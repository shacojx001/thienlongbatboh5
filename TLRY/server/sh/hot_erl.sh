#!/bin/bash

source `dirname $0`/head.sh
source `dirname $0`/common.sh

flag_file=${flag_path}/flag_compile_erl.txt
log_file=${log_path}/log_compile_erl.txt

cmark=`head -1 ${flag_file}`
if [ $cmark -eq 3 ]; then
    source $HOME/.bashrc
    echo "Hot Started at " `date +%Y-%m-%d" "%H:%M:%S` > ${log_file}

    cd ${game_path}
    sh ${game_path}/scripts/compile_game.sh

    # 游戏服热更
    for((i=0;i<${#SERVER_IDS[@]};i++))
    do
        echo "正在热更 ${SERVER_IDS[i]} 服 ..." >> $log_file
        NODE_NAME=${NODE_NAME_PREFIX}_s${SERVER_IDS[i]}@${DOMAIN}
        NODE_COOKIE=${COOKIE}_s${SERVER_IDS[i]}
        HOT_NODE_NAME=${NODE_NAME_PREFIX}_hot_s${SERVER_IDS[i]}@${DOMAIN}
	sh ${game_path}/scripts/hot.sh ${NODE_NAME} ${NODE_COOKIE} ${HOT_NODE_NAME} >> $log_file
    done

    # 跨服中心热更
    echo "正在热更 跨服中心 ..." >> $log_file
    KF_NODE_NAME=${NODE_NAME_PREFIX}_k100@${DOMAIN}
    KF_NODE_COOKIE=${COOKIE}_kf
    HOT_KF_NODE_NAME=${NODE_NAME_PREFIX}_hot_kf@${DOMAIN}
    sh ${game_path}/scripts/hot.sh ${KF_NODE_NAME} ${KF_NODE_COOKIE} ${HOT_KF_NODE_NAME} >> $log_file

    echo "Hot Finished at " `date +%Y-%m-%d" "%H:%M:%S` >> ${log_file}
    echo 0 > ${flag_file}
else
    echo "no change"
fi
