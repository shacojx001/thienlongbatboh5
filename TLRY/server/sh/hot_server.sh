#!/bin/sh

source `dirname $0`/${1}_head.sh
cd $SCRIPT_DIR

hot_mod=svr_hot_agent
hot_fun=hot

echo "========== start hot ... =========="
dt=`date +"%Y-%m-%d %H:%M:%S" `
echo `date +"%Y-%m-%d %H:%M:%S" `>> hot_server.log

ps aux | grep -Ew "100$" | grep -v stop | grep beam  | grep ${DOMAIN} | while read line 
do
    name=`echo ${line}|awk -F"-name " '{print $2}'|awk '{print $1}'`
    cookie=`echo ${line}|awk -F"-setcookie" '{print $2}'|awk '{print $1}'`
    echo "cast to node: ${name}"
    erl -noshell -name hot_${LOGO}_kf@${DOMAIN} -hidden -setcookie $cookie -eval "case net_adm:ping('$name') of pong -> rpc:cast('$name',$hot_mod, $hot_fun, []); _ -> hot_failed end" -s init stop & 
    echo "erl -detached -name hot_${LOGO}_kf@${DOMAIN} -setcookie $cookie -eval \"case net_adm:ping('$name') of pong -> rpc:cast('$name',$hot_mod, $hot_fun, []); _ -> hot_failed end\" -s init stop" >> hot_server.log
    
    sdomain=`echo ${name} |awk -F'@' '{print $2}'`
    lineNum=`echo ${name} |awk -F'@' '{print $1}'`
done    

echo "========== hot finished !!! =========="
