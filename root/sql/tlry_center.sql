-- MySQL dump 10.13  Distrib 5.6.50, for Linux (x86_64)
--
-- Host: localhost    Database: tlry_center
-- ------------------------------------------------------
-- Server version	5.6.50-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cdkey_list`
--

DROP TABLE IF EXISTS `cdkey_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdkey_list` (
  `card_no` char(40) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '卡号',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '媒体卡类型(0为默认)',
  `interval` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '同类型使用时间限制(小时)0为无限制',
  `card_expire` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '卡号失效时间，无截止时间置0',
  `card_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '卡号生成时间',
  `gift_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '礼包id,用于区分类型',
  `repeat_flag` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '同账号同类型是否可以重复领取1为可以，0为不可',
  `repeat_daily` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每天可领取个数',
  `lv_limit` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '等级限制',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '领取角色ID',
  `role_name` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '角色昵称',
  `role_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '使用日期',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，0未使用，1使用',
  `channel` varchar(1024) COLLATE utf8_unicode_ci NOT NULL COMMENT '单渠道礼包',
  `platf` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT '单专服礼包',
  `the_same_role_id` int(4) NOT NULL DEFAULT '0' COMMENT '同角色重复使用0为不可以,1为可以',
  `remark` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '关键字',
  `can_use_time` int(11) NOT NULL DEFAULT '0' COMMENT '卡号可以使用时间，无限制则为0',
  `cgift_type` int(11) NOT NULL COMMENT '生成礼包所属类型1:物品，2:掉落',
  UNIQUE KEY `card_no` (`card_no`) USING BTREE,
  KEY `card_time` (`card_time`) USING BTREE,
  KEY `gift_id` (`gift_id`) USING BTREE,
  KEY `card_expire` (`card_expire`) USING BTREE,
  KEY `channel` (`channel`(255)) USING BTREE,
  KEY `remark` (`remark`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdkey_list`
--

LOCK TABLES `cdkey_list` WRITE;
/*!40000 ALTER TABLE `cdkey_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdkey_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cdkey_log`
--

DROP TABLE IF EXISTS `cdkey_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cdkey_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `card_no` char(40) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '卡号',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '媒体卡类型(0为默认)',
  `interval` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '同类型使用时间限制(小时)0为无限制',
  `card_expire` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '卡号失效时间，无截止时间置0',
  `can_use_time` int(11) NOT NULL DEFAULT '0' COMMENT '卡号可以使用时间，无限制则为0',
  `card_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '卡号生成时间',
  `gift_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '礼包id,用于区分类型',
  `repeat_flag` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '同类型是否可以重复领取1为可以，0为不可',
  `lv_limit` int(4) unsigned NOT NULL DEFAULT '0' COMMENT '等级限制',
  `channel` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT '单渠道礼包',
  `platf` varchar(40) COLLATE utf8_unicode_ci NOT NULL COMMENT '单专服礼包',
  `the_same_role_id` int(4) NOT NULL DEFAULT '0' COMMENT '同角色重复使用0为不可以,1为可以',
  `sid` int(11) NOT NULL COMMENT '使用游戏服',
  `role_id` bigint(20) NOT NULL,
  `accname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL COMMENT '获取时间',
  `remark` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT '关键字',
  `cgift_type` int(11) NOT NULL COMMENT '生成礼包所属类型1:物品，2:掉落',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `card_time` (`card_time`) USING BTREE,
  KEY `gift_id` (`gift_id`) USING BTREE,
  KEY `card_expire` (`card_expire`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `channel` (`channel`) USING BTREE,
  KEY `accname` (`accname`) USING BTREE,
  KEY `card_no` (`card_no`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cdkey_log`
--

LOCK TABLES `cdkey_log` WRITE;
/*!40000 ALTER TABLE `cdkey_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `cdkey_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chat_game_rule`
--

DROP TABLE IF EXISTS `chat_game_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chat_game_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chat_game_rule`
--

LOCK TABLES `chat_game_rule` WRITE;
/*!40000 ALTER TABLE `chat_game_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat_game_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_loss`
--

DROP TABLE IF EXISTS `device_loss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_loss` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(128) NOT NULL DEFAULT '' COMMENT '渠道',
  `dev` varchar(64) NOT NULL DEFAULT '' COMMENT '设备号',
  `pos` tinyint(1) NOT NULL DEFAULT '1' COMMENT '位置',
  `time` int(11) NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `dev` (`dev`,`pos`,`channel`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='设备流失';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_loss`
--

LOCK TABLES `device_loss` WRITE;
/*!40000 ALTER TABLE `device_loss` DISABLE KEYS */;
/*!40000 ALTER TABLE `device_loss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_client_error`
--

DROP TABLE IF EXISTS `log_client_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_client_error` (
  `id` bigint(11) NOT NULL,
  `time` int(11) DEFAULT NULL COMMENT '上报时间',
  `records` text COLLATE utf8_unicode_ci COMMENT '错误信息',
  `channel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '平台号',
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '发送IP',
  `num` int(11) NOT NULL DEFAULT '1' COMMENT '报错次数',
  `server_id` int(11) NOT NULL DEFAULT '0' COMMENT '服id',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `nick_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '角色名',
  `level` int(11) NOT NULL DEFAULT '0' COMMENT '等级',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_client_error`
--

LOCK TABLES `log_client_error` WRITE;
/*!40000 ALTER TABLE `log_client_error` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_client_error` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ly_qq_vip_order`
--

DROP TABLE IF EXISTS `ly_qq_vip_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ly_qq_vip_order` (
  `order_id` varchar(64) NOT NULL DEFAULT '' COMMENT '订单号',
  `sid` int(11) NOT NULL DEFAULT '0' COMMENT '服ID',
  `accname` varchar(100) NOT NULL DEFAULT '' COMMENT '平台帐号',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '发货状态1为已发货',
  `ptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '付费时间',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '回调时间',
  `notify` text NOT NULL COMMENT '回调data',
  `email_result` varchar(256) NOT NULL DEFAULT '''''' COMMENT '发邮件返回信息',
  PRIMARY KEY (`order_id`) USING BTREE,
  KEY `ptime` (`ptime`) USING BTREE,
  KEY `accname` (`accname`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='手Q续费会员/年费会员订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ly_qq_vip_order`
--

LOCK TABLES `ly_qq_vip_order` WRITE;
/*!40000 ALTER TABLE `ly_qq_vip_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `ly_qq_vip_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ly_qq_vip_pre`
--

DROP TABLE IF EXISTS `ly_qq_vip_pre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ly_qq_vip_pre` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(64) DEFAULT '' COMMENT '订单号',
  `accname` varchar(100) NOT NULL DEFAULT '' COMMENT '平台帐号',
  `open_months` int(11) NOT NULL COMMENT '开通月数',
  `record` varchar(1024) DEFAULT '0' COMMENT '记录玩家信息JSON',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `accname` (`accname`) USING BTREE,
  KEY `ctime` (`ctime`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='手Q续费会员/年费会员记录角色信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ly_qq_vip_pre`
--

LOCK TABLES `ly_qq_vip_pre` WRITE;
/*!40000 ALTER TABLE `ly_qq_vip_pre` DISABLE KEYS */;
/*!40000 ALTER TABLE `ly_qq_vip_pre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice` (
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL,
  `gid` int(11) NOT NULL COMMENT '专区号',
  PRIMARY KEY (`gid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
INSERT INTO `notice` VALUES ('游戏公告','天下風雲出我輩，壹入江湖歲月催！由國內頂級遊戲制作團隊聯手暢遊打造的全新豎版天龍手遊——《天龍八部榮耀版》，11月8日早10點正式開啟!安卓玩家搶先體驗全新重制的天龍江湖 桃花影落，碧海潮生。曾在《天龍八部》網遊中，與喬峰並肩血戰聚賢莊的激情隨著歲月流逝而消退；與兄弟共戰宋遼戰場，大鬧縹緲峰的壯誌，也在工作與生活的重擔下漸漸消退。但隨著《天龍八部榮耀版》中壹曲《蒼山》的奏響，每位天龍少俠都會跟它的旋律，壹起哼著、回憶著當初與兄弟暢遊江湖的經歷。 ，牽動老玩家的情懷外。那些年，妳念念不忘的時裝坐騎、久打不通的副本、沒刷完的日常任務，都能在裏面找到。',1581246101,10000);
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `order_id` char(32) NOT NULL COMMENT '订单号',
  `pay_no` varchar(64) DEFAULT '' COMMENT '平台订单号',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '平台id',
  `sid` int(11) NOT NULL DEFAULT '0' COMMENT '服务器id',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `nickname` varchar(100) NOT NULL DEFAULT '' COMMENT '角色名',
  `lv` smallint(3) NOT NULL DEFAULT '0' COMMENT '人物等级',
  `accname` varchar(100) NOT NULL DEFAULT '' COMMENT '平台帐号',
  `product_id` varchar(100) NOT NULL DEFAULT '' COMMENT '商品id',
  `conf_id` int(11) NOT NULL DEFAULT '0' COMMENT '配置表base_recharge的id',
  `money` int(11) NOT NULL DEFAULT '0' COMMENT '金额:单位分',
  `gold` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值的元宝数',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '支付状态',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `ptime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '支付时间',
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1为测试充值',
  PRIMARY KEY (`order_id`) USING BTREE,
  KEY `pay_no` (`pay_no`) USING BTREE,
  KEY `ctime` (`ctime`,`sid`) USING BTREE,
  KEY `ptime` (`ptime`,`sid`) USING BTREE,
  KEY `gid` (`gid`,`sid`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='CP订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_key`
--

DROP TABLE IF EXISTS `order_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_key` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='创建订单时需要的id';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_key`
--

LOCK TABLES `order_key` WRITE;
/*!40000 ALTER TABLE `order_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player`
--

DROP TABLE IF EXISTS `player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `accname` varchar(50) NOT NULL DEFAULT '' COMMENT '平台账户',
  `reg_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后登陆时间',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道来源',
  `os` int(4) NOT NULL DEFAULT '0' COMMENT '0 android 1 ios 2 windows phone 3 other',
  `device_id` varchar(255) NOT NULL COMMENT '设备ID',
  `last_login_server` int(11) NOT NULL COMMENT '最后登录游戏服',
  `nickname` varchar(255) NOT NULL COMMENT '玩家名',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '平台id',
  `server_id` int(11) NOT NULL COMMENT '初始游戏服ID',
  `ip` varchar(50) NOT NULL COMMENT '玩家注册登录IP',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  `level` int(11) NOT NULL DEFAULT '1' COMMENT '等级',
  `last_login_channel` varchar(50) NOT NULL DEFAULT '' COMMENT '最后登录channel',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `player` (`role_id`) USING BTREE,
  KEY `reg_time` (`reg_time`) USING BTREE,
  KEY `last_login_time` (`last_login_time`) USING BTREE,
  KEY `ip` (`ip`) USING BTREE,
  KEY `accname` (`accname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=954 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='用户登陆信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player`
--

LOCK TABLES `player` WRITE;
/*!40000 ALTER TABLE `player` DISABLE KEYS */;
/*!40000 ALTER TABLE `player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_accname`
--

DROP TABLE IF EXISTS `player_accname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_accname` (
  `device_id` varchar(255) NOT NULL COMMENT '设备ID',
  `accname` varchar(50) NOT NULL DEFAULT '' COMMENT '平台账户',
  `reg_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道来源',
  `os` int(4) NOT NULL DEFAULT '0' COMMENT '0 android 1 ios 2 windows phone 3 other',
  `nickname` varchar(255) NOT NULL COMMENT '玩家名',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '平台id',
  `server_id` int(11) NOT NULL COMMENT '初始游戏服ID',
  `ip` varchar(50) NOT NULL COMMENT '玩家注册登录IP',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  PRIMARY KEY (`accname`) USING BTREE,
  KEY `reg_time` (`reg_time`) USING BTREE,
  KEY `server_id` (`server_id`,`role_id`) USING BTREE,
  KEY `ip` (`ip`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帐号表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_accname`
--

LOCK TABLES `player_accname` WRITE;
/*!40000 ALTER TABLE `player_accname` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_accname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `player_device`
--

DROP TABLE IF EXISTS `player_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `player_device` (
  `device_id` varchar(255) NOT NULL COMMENT '设备ID',
  `accname` varchar(50) NOT NULL DEFAULT '' COMMENT '平台账户',
  `reg_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `channel` varchar(50) NOT NULL DEFAULT '' COMMENT '渠道来源',
  `os` int(4) NOT NULL DEFAULT '0' COMMENT '0 android 1 ios 2 windows phone 3 other',
  `nickname` varchar(255) NOT NULL COMMENT '玩家名',
  `gid` int(11) NOT NULL DEFAULT '0' COMMENT '平台id',
  `server_id` int(11) NOT NULL COMMENT '初始游戏服ID',
  `ip` varchar(50) NOT NULL COMMENT '玩家注册登录IP',
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '角色id',
  PRIMARY KEY (`device_id`) USING BTREE,
  KEY `reg_time` (`reg_time`) USING BTREE,
  KEY `server_id` (`server_id`,`role_id`) USING BTREE,
  KEY `ip` (`ip`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='设备表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `player_device`
--

LOCK TABLES `player_device` WRITE;
/*!40000 ALTER TABLE `player_device` DISABLE KEYS */;
/*!40000 ALTER TABLE `player_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reward_audit_log`
--

DROP TABLE IF EXISTS `reward_audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reward_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `accname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `post` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '发送物品',
  `gold` int(11) NOT NULL,
  `begold` int(11) NOT NULL DEFAULT '0' COMMENT '绑元',
  `user_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '申请人',
  `ramk` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '备注',
  `auditer` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '发放者',
  `time` int(11) NOT NULL COMMENT '申请时间',
  `channel` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '渠道',
  `is_new` int(2) NOT NULL DEFAULT '0' COMMENT '是否新服奖金0否，1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reward_audit_log`
--

LOCK TABLES `reward_audit_log` WRITE;
/*!40000 ALTER TABLE `reward_audit_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_audit_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tlry_center'
--

--
-- Dumping routines for database 'tlry_center'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-16  8:57:21
