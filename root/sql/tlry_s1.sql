-- MySQL dump 10.13  Distrib 5.6.50, for Linux (x86_64)
--
-- Host: localhost    Database: tlry_s1
-- ------------------------------------------------------
-- Server version	5.6.50-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arena_rank`
--

DROP TABLE IF EXISTS `arena_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arena_rank` (
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排名',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  PRIMARY KEY (`rank`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='竞技场排名';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arena_rank`
--

LOCK TABLES `arena_rank` WRITE;
/*!40000 ALTER TABLE `arena_rank` DISABLE KEYS */;
/*!40000 ALTER TABLE `arena_rank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction` (
  `id` bigint(20) unsigned NOT NULL COMMENT '唯一ID',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '类型, 来源',
  `scope` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `target` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '目标',
  `items` text COMMENT '物品',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `roles` text COMMENT '分红玩家',
  `state` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '拍卖状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='拍卖行';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `auction` WRITE;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction_log`
--

DROP TABLE IF EXISTS `auction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `aid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '拍卖行ID',
  `uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '物品唯一ID',
  `cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配置ID(拍卖物品)',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `num` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '数量',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `scope` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '范围(购买时)',
  `target` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '目标(购买时)',
  `price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '成交价格',
  `deal` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '成交类型',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '成交时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='拍卖行日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction_log`
--

LOCK TABLES `auction_log` WRITE;
/*!40000 ALTER TABLE `auction_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `auction_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ban_info`
--

DROP TABLE IF EXISTS `ban_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ban_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '禁止序号',
  `type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '禁止类型(1:ip)',
  `value` varchar(20) NOT NULL COMMENT '禁止的值',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `value` (`value`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='封禁信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ban_info`
--

LOCK TABLES `ban_info` WRITE;
/*!40000 ALTER TABLE `ban_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `ban_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_dbver`
--

DROP TABLE IF EXISTS `base_dbver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_dbver` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `last_updated` varchar(50) NOT NULL COMMENT '上次更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='数据库脚本更新日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_dbver`
--

LOCK TABLES `base_dbver` WRITE;
/*!40000 ALTER TABLE `base_dbver` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_dbver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_game`
--

DROP TABLE IF EXISTS `base_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_game` (
  `cf_name` varchar(50) NOT NULL COMMENT '名',
  `cf_value` text NOT NULL COMMENT '值',
  PRIMARY KEY (`cf_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='游戏配置参数表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_game`
--

LOCK TABLES `base_game` WRITE;
/*!40000 ALTER TABLE `base_game` DISABLE KEYS */;
INSERT INTO `base_game` VALUES ('channel_list','vtnemo,chuxin'),('erl_cookie','tl_s10001.Jmt6w.ci8qw'),('erl_node','tl_s10001@117.4.244.81'),('game_domain','http://117.4.244.81'),('game_title','11111'),('gid','10000'),('ip','117.4.244.81'),('port','9000'),('push_url',''),('sdb','s10001_tl_gznemo'),('sid','1'),('version','2020-02-09-10-00-00');
/*!40000 ALTER TABLE `base_game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_kf`
--

DROP TABLE IF EXISTS `base_kf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_kf` (
  `node_id` smallint(5) unsigned NOT NULL COMMENT '节点ID',
  `name` varchar(100) NOT NULL COMMENT '节点名',
  `cookie` varchar(127) NOT NULL COMMENT '节点cookie',
  PRIMARY KEY (`node_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='跨服中心节点';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_kf`
--

LOCK TABLES `base_kf` WRITE;
/*!40000 ALTER TABLE `base_kf` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_kf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_kf_group`
--

DROP TABLE IF EXISTS `base_kf_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_kf_group` (
  `act_id` smallint(5) unsigned NOT NULL COMMENT '活动id(0为默认分组)',
  `group_id` int(10) unsigned NOT NULL COMMENT '分组id',
  `server_id` int(10) unsigned NOT NULL COMMENT '服id',
  `ctime` int(10) unsigned NOT NULL COMMENT '添加时间',
  UNIQUE KEY `act_id_server` (`act_id`,`server_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='活动分组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_kf_group`
--

LOCK TABLES `base_kf_group` WRITE;
/*!40000 ALTER TABLE `base_kf_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_kf_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_node_kf`
--

DROP TABLE IF EXISTS `base_node_kf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_node_kf` (
  `node_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '节点id',
  `node` varchar(100) NOT NULL COMMENT '节点名',
  `cookie` varchar(100) DEFAULT '' COMMENT 'cookie',
  `ip` varchar(127) NOT NULL DEFAULT '' COMMENT 'ip地址',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '节点启动时间',
  PRIMARY KEY (`node_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='跨服节点列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_node_kf`
--

LOCK TABLES `base_node_kf` WRITE;
/*!40000 ALTER TABLE `base_node_kf` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_node_kf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_white_accname`
--

DROP TABLE IF EXISTS `base_white_accname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_white_accname` (
  `accname` varchar(126) NOT NULL COMMENT '账号',
  `ip` varchar(31) NOT NULL DEFAULT 'all' COMMENT 'ip (all 表示不限制ip)',
  UNIQUE KEY `accname` (`accname`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='内测白名单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_white_accname`
--

LOCK TABLES `base_white_accname` WRITE;
/*!40000 ALTER TABLE `base_white_accname` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_white_accname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_word`
--

DROP TABLE IF EXISTS `base_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_word` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `word` varchar(500) NOT NULL COMMENT '关键字内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='关键字过滤';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_word`
--

LOCK TABLES `base_word` WRITE;
/*!40000 ALTER TABLE `base_word` DISABLE KEYS */;
/*!40000 ALTER TABLE `base_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `global_data`
--

DROP TABLE IF EXISTS `global_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_data` (
  `key` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '键',
  `value` text NOT NULL COMMENT '值',
  `daily_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '每日数据标识（0永久数据 1每日数据）',
  PRIMARY KEY (`key`) USING BTREE,
  KEY `daily_flag` (`daily_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='全局数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_data`
--

LOCK TABLES `global_data` WRITE;
/*!40000 ALTER TABLE `global_data` DISABLE KEYS */;
INSERT INTO `global_data` VALUES (6002,'[]',0),(6003,'[{1,1}]',0),(6004,'100',0),(6005,'2921690',0),(6006,'{1900,1629074873}',0),(6007,'[]',0);
/*!40000 ALTER TABLE `global_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '群组ID',
  `type` int(11) unsigned NOT NULL COMMENT '类型',
  `name` varchar(100) NOT NULL COMMENT '名字',
  `announce` varchar(500) NOT NULL COMMENT '公告',
  `owner` bigint(20) unsigned NOT NULL COMMENT '群主ID',
  `mem_list` text NOT NULL COMMENT '成员ID列表',
  `apply_list` text NOT NULL COMMENT '申请进群玩家ID列表',
  `last_update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次更新过操作时间戳',
  `last_chat_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次群组有人聊天的时间戳',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='好友群组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild`
--

DROP TABLE IF EXISTS `guild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild` (
  `guild_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '公会Id',
  `num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '编号',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `creator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '创始人ID',
  `creator_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创始人昵称',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '公会创建时间',
  `create_type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '创建类型',
  `chief_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '现任会长ID',
  `chief_name` varchar(50) NOT NULL DEFAULT '' COMMENT '现任会长昵称',
  `accept_type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '招人条件',
  `auto_accept` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自动接收',
  `recruit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '招募时间',
  `announce` varchar(500) NOT NULL DEFAULT '' COMMENT '公告',
  `level` smallint(3) unsigned NOT NULL DEFAULT '1' COMMENT '级别',
  `funds` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '资金',
  `c_rename` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '重命名标记',
  `logs` text COMMENT '帮派记录',
  `cook_val` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '烹饪值',
  `cook_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '烹饪人数(次数)',
  `cook_logs` text COMMENT '烹饪日志',
  `pause_denf_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '暂停维护结束时间戳',
  `recently_live` varchar(255) NOT NULL DEFAULT '[]' COMMENT '最近7天活跃值',
  `today_live` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '今日帮派成员活跃值',
  `bonus` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '分红进度',
  `task` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '帮派任务进度',
  `task_stage` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '帮派任务阶段',
  `build` varchar(500) NOT NULL DEFAULT '[]' COMMENT '建筑',
  `study` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '研究',
  `exploit` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '战绩',
  `battle` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '宣战值',
  `declare` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '宣战列表',
  `back` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '应战列表',
  `hostile` text COMMENT '敌对帮派',
  `rob` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '劫镖收益',
  `bless` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '帮派祝福',
  `sh_dung` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '少室山副本',
  `rob_list` text COMMENT '劫镖次数',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数据修改时间',
  PRIMARY KEY (`guild_id`) USING BTREE,
  UNIQUE KEY `index_guild_name` (`name`) USING BTREE,
  KEY `index_guild_chief_id` (`chief_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24242622849025 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild`
--

LOCK TABLES `guild` WRITE;
/*!40000 ALTER TABLE `guild` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_apply`
--

DROP TABLE IF EXISTS `guild_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_apply` (
  `guild_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '申请公会ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '申请角色ID',
  `create_time` int(11) unsigned DEFAULT '0' COMMENT '申请时间',
  `fight` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '战力',
  PRIMARY KEY (`guild_id`,`role_id`) USING BTREE,
  KEY `index_guild_apply_player_id` (`role_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派申请';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_apply`
--

LOCK TABLES `guild_apply` WRITE;
/*!40000 ALTER TABLE `guild_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_dismiss`
--

DROP TABLE IF EXISTS `guild_dismiss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_dismiss` (
  `guild_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '公会Id',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '名称',
  `creator_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '创始人ID',
  `creator_name` varchar(50) NOT NULL DEFAULT '' COMMENT '创始人昵称',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '公会创建时间',
  `create_type` tinyint(3) unsigned NOT NULL DEFAULT '2' COMMENT '创建类型',
  `chief_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '现任会长ID',
  `chief_name` varchar(50) NOT NULL DEFAULT '' COMMENT '现任会长昵称',
  `accept_type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '招人条件',
  `auto_accept` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '自动接收',
  `announce` varchar(500) NOT NULL DEFAULT '' COMMENT '公告',
  `level` smallint(3) unsigned NOT NULL DEFAULT '1' COMMENT '级别',
  `funds` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '资金',
  `c_rename` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '重命名标记',
  `members` text COMMENT '成员',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数据修改时间',
  `dismiss_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '解散时间',
  PRIMARY KEY (`guild_id`) USING BTREE,
  UNIQUE KEY `index_guild_name` (`name`) USING BTREE,
  KEY `index_guild_chief_id` (`chief_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=24241365725185 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派解散';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_dismiss`
--

LOCK TABLES `guild_dismiss` WRITE;
/*!40000 ALTER TABLE `guild_dismiss` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_dismiss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_member`
--

DROP TABLE IF EXISTS `guild_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_member` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `guild_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '帮派ID',
  `position` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '帮派职位',
  `cont` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '历史贡献',
  `last_leave` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次离开的时间',
  `last_join` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次加入帮派时间戳',
  `weekly_live` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '周活跃',
  `weekly_funds` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '周贡献帮派资金',
  `weekly_cont` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '周获得帮贡',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数据修改时间',
  PRIMARY KEY (`role_id`) USING BTREE,
  KEY `index_guild_member_guild_id` (`guild_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派成员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_member`
--

LOCK TABLES `guild_member` WRITE;
/*!40000 ALTER TABLE `guild_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guild_wine_act`
--

DROP TABLE IF EXISTS `guild_wine_act`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guild_wine_act` (
  `guild_id` bigint(20) unsigned NOT NULL COMMENT '公会ID',
  `dice_num_list` text NOT NULL COMMENT '骰子点数情况列表,[{role_id,num,time}]',
  `dice_max_role` text NOT NULL COMMENT '手气最好玩家信息,{role_id,role_name,career,gender,like_num,dislike_num,dice_num}',
  `dice_min_role` text NOT NULL COMMENT '手气最差玩家信息',
  PRIMARY KEY (`guild_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guild_wine_act`
--

LOCK TABLES `guild_wine_act` WRITE;
/*!40000 ALTER TABLE `guild_wine_act` DISABLE KEYS */;
/*!40000 ALTER TABLE `guild_wine_act` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_combat_power`
--

DROP TABLE IF EXISTS `log_combat_power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_combat_power` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'autoId',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `ori_power` int(10) unsigned NOT NULL COMMENT '旧战力',
  `power` int(10) unsigned NOT NULL COMMENT '新战力',
  `ctime` int(10) unsigned NOT NULL COMMENT '记录时间',
  `is_lower` tinyint(3) unsigned NOT NULL COMMENT '是否降低战力',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='战力日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_combat_power`
--

LOCK TABLES `log_combat_power` WRITE;
/*!40000 ALTER TABLE `log_combat_power` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_combat_power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_divorce`
--

DROP TABLE IF EXISTS `log_divorce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_divorce` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `mate_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '伴侣ID',
  `marry_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '婚姻ID',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '离婚时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='离婚日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_divorce`
--

LOCK TABLES `log_divorce` WRITE;
/*!40000 ALTER TABLE `log_divorce` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_divorce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_dungeon`
--

DROP TABLE IF EXISTS `log_dungeon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_dungeon` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一ID',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `dung_id` int(10) unsigned NOT NULL COMMENT '副本ID',
  `level` int(10) unsigned NOT NULL COMMENT '关卡',
  `mode` tinyint(3) unsigned NOT NULL COMMENT '模式',
  `action` tinyint(3) unsigned NOT NULL COMMENT '动作',
  `members` text COMMENT '成员',
  `time` int(10) unsigned NOT NULL COMMENT '时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=82475 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='副本日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_dungeon`
--

LOCK TABLES `log_dungeon` WRITE;
/*!40000 ALTER TABLE `log_dungeon` DISABLE KEYS */;
INSERT INTO `log_dungeon` VALUES (82473,24247800496128,3001,1,1,1,'[]',1624906011),(82474,24247800496128,3001,1,1,3,'[]',1624906041);
/*!40000 ALTER TABLE `log_dungeon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_login`
--

DROP TABLE IF EXISTS `log_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_login` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `career` tinyint(3) unsigned NOT NULL COMMENT '职业',
  `level` smallint(5) unsigned NOT NULL COMMENT '等级',
  `ip` varchar(62) NOT NULL COMMENT '登陆ip',
  `login_time` int(10) unsigned NOT NULL COMMENT '登陆时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4109 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='登陆日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_login`
--

LOCK TABLES `log_login` WRITE;
/*!40000 ALTER TABLE `log_login` DISABLE KEYS */;
INSERT INTO `log_login` VALUES (4108,24247800496128,4,1,'223.152.150.104',1624905873);
/*!40000 ALTER TABLE `log_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_logout`
--

DROP TABLE IF EXISTS `log_logout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_logout` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `career` tinyint(3) unsigned NOT NULL COMMENT '职业',
  `level` smallint(5) unsigned NOT NULL COMMENT '等级',
  `ip` varchar(62) NOT NULL COMMENT '登陆ip',
  `login_time` int(10) unsigned NOT NULL COMMENT '登陆时间',
  `logout_time` int(10) unsigned NOT NULL COMMENT '登出时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4096 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='登出日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_logout`
--

LOCK TABLES `log_logout` WRITE;
/*!40000 ALTER TABLE `log_logout` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_logout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_marriage`
--

DROP TABLE IF EXISTS `log_marriage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_marriage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `mate_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '伴侣ID',
  `marry_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '婚姻ID',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '结婚时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='结婚日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_marriage`
--

LOCK TABLES `log_marriage` WRITE;
/*!40000 ALTER TABLE `log_marriage` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_marriage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_marry_hall`
--

DROP TABLE IF EXISTS `log_marry_hall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_marry_hall` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `male_id` bigint(20) unsigned NOT NULL COMMENT '丈夫ID',
  `female_id` bigint(20) unsigned NOT NULL COMMENT '妻子ID',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开启时间戳',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='结婚礼堂日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_marry_hall`
--

LOCK TABLES `log_marry_hall` WRITE;
/*!40000 ALTER TABLE `log_marry_hall` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_marry_hall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_online`
--

DROP TABLE IF EXISTS `log_online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_online` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `source` varchar(126) NOT NULL COMMENT '渠道',
  `online` smallint(5) unsigned NOT NULL COMMENT '在线人数',
  `ctime` int(10) unsigned NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `source` (`source`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=583410 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='在线日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_online`
--

LOCK TABLES `log_online` WRITE;
/*!40000 ALTER TABLE `log_online` DISABLE KEYS */;
INSERT INTO `log_online` VALUES (583402,'vtnemo',1,1624905884),(583403,'vtnemo',1,1624905914),(583404,'vtnemo',1,1624905944),(583405,'vtnemo',1,1624905974),(583406,'vtnemo',1,1624906004),(583407,'vtnemo',1,1624906034),(583408,'vtnemo',1,1624906064),(583409,'vtnemo',1,1624906094);
/*!40000 ALTER TABLE `log_online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_rank_arena`
--

DROP TABLE IF EXISTS `log_rank_arena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_rank_arena` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排名',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='竞技场排行日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_rank_arena`
--

LOCK TABLES `log_rank_arena` WRITE;
/*!40000 ALTER TABLE `log_rank_arena` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_rank_arena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_rank_record`
--

DROP TABLE IF EXISTS `log_rank_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_rank_record` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排行榜类型',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排名',
  `data` text COMMENT '数据',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `type_rank` (`type`,`rank`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6852591 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='排行数据日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_rank_record`
--

LOCK TABLES `log_rank_record` WRITE;
/*!40000 ALTER TABLE `log_rank_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_rank_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_role_bags`
--

DROP TABLE IF EXISTS `log_role_bags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_role_bags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `bag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '背包ID',
  `op` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '操作',
  `gid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品数量',
  `opt` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作类型',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5958982 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家背包';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_role_bags`
--

LOCK TABLES `log_role_bags` WRITE;
/*!40000 ALTER TABLE `log_role_bags` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_role_bags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_role_level`
--

DROP TABLE IF EXISTS `log_role_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_role_level` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `old_level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '旧等级',
  `old_exp` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '旧经验',
  `amount` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '操作数量',
  `new_level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '新等级',
  `new_exp` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '新经验',
  `opt` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作类型',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11758 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家等级日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_role_level`
--

LOCK TABLES `log_role_level` WRITE;
/*!40000 ALTER TABLE `log_role_level` DISABLE KEYS */;
INSERT INTO `log_role_level` VALUES (11753,24247800496128,1,0,128,2,8,55,1624905901),(11754,24247800496128,2,108,296,3,241,55,1624905934),(11755,24247800496128,3,241,520,5,22,55,1624905940),(11756,24247800496128,5,496,800,6,371,55,1624905956),(11757,24247800496128,6,376,1000,7,197,55,1624906008);
/*!40000 ALTER TABLE `log_role_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_role_money`
--

DROP TABLE IF EXISTS `log_role_money`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_role_money` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `type` int(10) unsigned NOT NULL COMMENT '金钱类型',
  `op` tinyint(3) unsigned NOT NULL COMMENT '操作(获得,消耗)',
  `old` int(10) unsigned NOT NULL COMMENT '旧金钱',
  `new` int(10) unsigned NOT NULL COMMENT '新金钱',
  `amount` int(10) unsigned NOT NULL COMMENT '操作数量',
  `opt` int(10) unsigned NOT NULL COMMENT '消耗类型',
  `time` int(10) unsigned NOT NULL COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2917972 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家金钱日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_role_money`
--

LOCK TABLES `log_role_money` WRITE;
/*!40000 ALTER TABLE `log_role_money` DISABLE KEYS */;
INSERT INTO `log_role_money` VALUES (2917961,24247800496128,4,1,0,140,140,55,1624905901),(2917962,24247800496128,4,1,140,5140,5000,55,1624905908),(2917963,24247800496128,4,1,5140,15140,10000,141,1624905921),(2917964,24247800496128,4,1,15140,15470,330,55,1624905934),(2917965,24247800496128,4,1,15470,16070,600,55,1624905940),(2917966,24247800496128,4,2,16070,15270,800,18,1624905945),(2917967,24247800496128,4,2,15270,8470,6800,18,1624905946),(2917968,24247800496128,4,1,8470,8990,520,55,1624905953),(2917969,24247800496128,4,1,8990,9870,880,55,1624905956),(2917970,24247800496128,4,1,9870,10970,1100,55,1624906008),(2917971,24247800496128,2,1,0,8300000,8300000,71,1624906009);
/*!40000 ALTER TABLE `log_role_money` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_role_shop`
--

DROP TABLE IF EXISTS `log_role_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_role_shop` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `shop_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商店ID',
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '标签ID',
  `cate_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '目录ID',
  `item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '物品ID',
  `num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14376 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家商店日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_role_shop`
--

LOCK TABLES `log_role_shop` WRITE;
/*!40000 ALTER TABLE `log_role_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_role_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_attr`
--

DROP TABLE IF EXISTS `mail_attr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_attr` (
  `uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '唯一ID',
  `type` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '邮件类型',
  `op_type` int(11) NOT NULL DEFAULT '0' COMMENT '操作类型',
  `ver` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '时间',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '时间戳',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(1000) NOT NULL DEFAULT '' COMMENT '内容',
  `from_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '发送方ID',
  `from_name` varchar(50) NOT NULL DEFAULT '' COMMENT '发送方名字',
  `goods` text NOT NULL COMMENT '物品',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='邮件属性';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_attr`
--

LOCK TABLES `mail_attr` WRITE;
/*!40000 ALTER TABLE `mail_attr` DISABLE KEYS */;
INSERT INTO `mail_attr` VALUES (24247801057280,2,71,1,1624906009,'充值成功','恭喜您，您於<font color=\"#5fc934\">2021</font>年<font color=\"#5fc934\">6</font>月<font color=\"#5fc934\">29</font>日<font color=\"#5fc934\">2</font>時<font color=\"#5fc934\">46</font>分充值的[<font color=\"#fea21e\">8300000元寶</font>]已成功註入。',0,'系统','[]');
/*!40000 ALTER TABLE `mail_attr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market_item`
--

DROP TABLE IF EXISTS `market_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `market_item` (
  `uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '唯一ID',
  `cid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配置ID',
  `tag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '标签',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '类型',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名字',
  `owner` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '拥有者',
  `price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单价',
  `num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `stat` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `follower` text COMMENT '关注者',
  `property` text COMMENT '属性',
  PRIMARY KEY (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商会道具';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market_item`
--

LOCK TABLES `market_item` WRITE;
/*!40000 ALTER TABLE `market_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `market_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market_log`
--

DROP TABLE IF EXISTS `market_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `market_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '资质ID',
  `uid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '唯一ID',
  `buyer` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '购买方ID',
  `num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '购买数量',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '购买时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2866 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商会日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market_log`
--

LOCK TABLES `market_log` WRITE;
/*!40000 ALTER TABLE `market_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `market_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market_role`
--

DROP TABLE IF EXISTS `market_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `market_role` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `volume` int(10) unsigned NOT NULL,
  `turnover` int(10) unsigned NOT NULL,
  `use_gold` int(10) unsigned NOT NULL,
  `items` text,
  `follow` text,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商会玩家';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market_role`
--

LOCK TABLES `market_role` WRITE;
/*!40000 ALTER TABLE `market_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `market_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market_trade_log`
--

DROP TABLE IF EXISTS `market_trade_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `market_trade_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `action` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '动作',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名字',
  `num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `mtype` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '价格类型',
  `price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '成交价',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5666 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商会交易记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market_trade_log`
--

LOCK TABLES `market_trade_log` WRITE;
/*!40000 ALTER TABLE `market_trade_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `market_trade_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marriage`
--

DROP TABLE IF EXISTS `marriage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marriage` (
  `marry_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '婚姻ID',
  `male_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '男方ID',
  `female_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '女方ID',
  `marry_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结婚时间',
  `love_value` int(10) unsigned NOT NULL DEFAULT '0',
  `divorce_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '离婚时间',
  `is_open_hall` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否开过礼堂',
  `is_parade` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否游行过',
  PRIMARY KEY (`marry_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='婚姻';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marriage`
--

LOCK TABLES `marriage` WRITE;
/*!40000 ALTER TABLE `marriage` DISABLE KEYS */;
/*!40000 ALTER TABLE `marriage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marriage_role`
--

DROP TABLE IF EXISTS `marriage_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marriage_role` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `marry_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '婚姻ID',
  `mate_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '伴侣ID',
  `marry_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '结婚次数 {类型,次数}',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='婚姻玩家';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marriage_role`
--

LOCK TABLES `marriage_role` WRITE;
/*!40000 ALTER TABLE `marriage_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `marriage_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentor_person`
--

DROP TABLE IF EXISTS `mentor_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mentor_person` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `name` varchar(1024) NOT NULL COMMENT '名字',
  `lv` int(11) unsigned NOT NULL COMMENT '玩家等级',
  `career` int(11) unsigned NOT NULL COMMENT '职业',
  `icon` int(11) unsigned NOT NULL COMMENT '头像',
  `frame` int(11) unsigned NOT NULL COMMENT '头像框',
  `morality` int(11) unsigned NOT NULL COMMENT '师德',
  `mentor_lv` int(11) unsigned NOT NULL COMMENT '师父等级',
  `quiz_list` varchar(1024) NOT NULL COMMENT '问卷列表',
  `tudi_list` text NOT NULL COMMENT '徒弟信息列表',
  `reg` int(11) unsigned NOT NULL COMMENT '是否报名师父，0否，1是',
  `last_update_time` int(11) unsigned NOT NULL COMMENT '上次更新时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='师徒';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentor_person`
--

LOCK TABLES `mentor_person` WRITE;
/*!40000 ALTER TABLE `mentor_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `mentor_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merge_count`
--

DROP TABLE IF EXISTS `merge_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merge_count` (
  `id` int(11) unsigned NOT NULL COMMENT '唯一标识',
  `time` int(11) unsigned NOT NULL COMMENT '合服时间',
  `merge_count` tinyint(3) unsigned NOT NULL COMMENT '合服次数',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `time` (`time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='合服信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merge_count`
--

LOCK TABLES `merge_count` WRITE;
/*!40000 ALTER TABLE `merge_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `merge_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `operation_activity_schedule`
--

DROP TABLE IF EXISTS `operation_activity_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_activity_schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `act_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '活动ID',
  `timing` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '定时类型',
  `begin_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `index` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '索引',
  `valid` int(11) unsigned NOT NULL DEFAULT '1' COMMENT '是否有效',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='运营活动时间表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operation_activity_schedule`
--

LOCK TABLES `operation_activity_schedule` WRITE;
/*!40000 ALTER TABLE `operation_activity_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `operation_activity_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pet_hatch_list`
--

DROP TABLE IF EXISTS `pet_hatch_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pet_hatch_list` (
  `hatch_id` bigint(20) unsigned NOT NULL COMMENT '孵化id',
  `slots` varchar(127) NOT NULL COMMENT '孵化格子',
  `growup_lv_sum` tinyint(3) unsigned NOT NULL COMMENT '成长等级和',
  `growup_rate_sum` smallint(5) unsigned NOT NULL COMMENT '成长率和',
  `finish_time` int(10) unsigned NOT NULL COMMENT '完成时间',
  PRIMARY KEY (`hatch_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='珍兽孵化列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pet_hatch_list`
--

LOCK TABLES `pet_hatch_list` WRITE;
/*!40000 ALTER TABLE `pet_hatch_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `pet_hatch_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_log`
--

DROP TABLE IF EXISTS `questionnaire_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家id',
  `id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '问卷id',
  `server_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '服务器编号',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '玩家名',
  `lv` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '玩家等级',
  `power` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '玩家战力',
  `commit_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '提交时间',
  `star` smallint(4) unsigned NOT NULL DEFAULT '0' COMMENT '满意度1-5',
  `context` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '提交的内容',
  `reply` text COMMENT '回复',
  PRIMARY KEY (`log_id`) USING BTREE,
  KEY `role_id` (`role_id`,`id`,`server_num`,`nickname`,`commit_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_log`
--

LOCK TABLES `questionnaire_log` WRITE;
/*!40000 ALTER TABLE `questionnaire_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionnaire_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `raider_boss_log`
--

DROP TABLE IF EXISTS `raider_boss_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `raider_boss_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `boss` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Boss ID',
  `refresh` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '刷新时间',
  `kill` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '击杀时间',
  `killer` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '击杀者',
  `role_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总参加人数',
  `online_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '击杀时场景内人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='夺宝奇兵日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `raider_boss_log`
--

LOCK TABLES `raider_boss_log` WRITE;
/*!40000 ALTER TABLE `raider_boss_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `raider_boss_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rank_record`
--

DROP TABLE IF EXISTS `rank_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rank_record` (
  `type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排行榜类型',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排名',
  `data` text COMMENT '数据',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type`,`rank`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='排行数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rank_record`
--

LOCK TABLES `rank_record` WRITE;
/*!40000 ALTER TABLE `rank_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `rank_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recharge`
--

DROP TABLE IF EXISTS `recharge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recharge` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '唯一ID',
  `type` tinyint(4) NOT NULL DEFAULT '8' COMMENT '充值类型 1:gm 2:道具 8:常规',
  `product_id` tinyint(4) NOT NULL COMMENT '商品id',
  `pay_no` varchar(50) NOT NULL COMMENT '订单号',
  `accname` varchar(50) NOT NULL COMMENT '账号',
  `role_id` bigint(20) unsigned NOT NULL COMMENT '用户ID',
  `name` varchar(50) NOT NULL COMMENT '玩家名',
  `level` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `money` decimal(12,2) DEFAULT '0.00' COMMENT '金额',
  `gold` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '元宝',
  `state` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态 0:未处理 1:已处理 2:处理出错',
  `rmb` decimal(12,2) DEFAULT '0.00' COMMENT '人民币',
  `remark` varchar(255) NOT NULL DEFAULT '""' COMMENT '备注(客户端透传)',
  `pay_type` varchar(50) NOT NULL DEFAULT '""' COMMENT '支付类型',
  `source` varchar(50) NOT NULL DEFAULT '""' COMMENT '渠道',
  `cp_no` varchar(50) NOT NULL DEFAULT '""' COMMENT 'cp订单号',
  `ctime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '订单时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `type` (`type`) USING BTREE,
  KEY `state` (`state`) USING BTREE,
  KEY `lv` (`level`) USING BTREE,
  KEY `os_product_id` (`product_id`) USING BTREE,
  KEY `remark` (`remark`) USING BTREE,
  KEY `accname` (`accname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19004 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='充值订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recharge`
--

LOCK TABLES `recharge` WRITE;
/*!40000 ALTER TABLE `recharge` DISABLE KEYS */;
INSERT INTO `recharge` VALUES (19003,2,5,'24247800496128_recharge_at_1624906000','zxczxc',24247800496128,'大哥',1,0.00,1,1,1.00,'gm','0','\"\"','\"\"',1624906000);
/*!40000 ALTER TABLE `recharge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `renames`
--

DROP TABLE IF EXISTS `renames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renames` (
  `role_id` bigint(20) NOT NULL COMMENT '玩家id',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='合服重名玩家表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `renames`
--

LOCK TABLES `renames` WRITE;
/*!40000 ALTER TABLE `renames` DISABLE KEYS */;
/*!40000 ALTER TABLE `renames` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_achieve`
--

DROP TABLE IF EXISTS `role_achieve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_achieve` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `tasks` mediumtext COMMENT '成就任务',
  `types` text COMMENT '成就类型',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家成就';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_achieve`
--

LOCK TABLES `role_achieve` WRITE;
/*!40000 ALTER TABLE `role_achieve` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_achieve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_anqi`
--

DROP TABLE IF EXISTS `role_anqi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_anqi` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `id` int(11) unsigned NOT NULL COMMENT '暗器模型ID',
  `q_level` int(11) unsigned NOT NULL COMMENT '品阶值',
  `stren` int(11) unsigned NOT NULL COMMENT '强化等级',
  `stones` varchar(1024) NOT NULL COMMENT '镶嵌宝石',
  `practice_lv` int(11) unsigned NOT NULL COMMENT '修炼等级',
  `practice_exp` int(11) unsigned NOT NULL COMMENT '修炼进度经验',
  `cur_plan` tinyint(8) unsigned NOT NULL COMMENT '当前选择技能方案',
  `skill_plans` varchar(2048) NOT NULL COMMENT '技能方案',
  `poison_slots` text NOT NULL COMMENT '淬毒孔列表',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='暗器';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_anqi`
--

LOCK TABLES `role_anqi` WRITE;
/*!40000 ALTER TABLE `role_anqi` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_anqi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_arena`
--

DROP TABLE IF EXISTS `role_arena`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_arena` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `buy_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '购买次数',
  `left_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '剩余次数',
  `reward_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '定时奖励时间',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家竞技场数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_arena`
--

LOCK TABLES `role_arena` WRITE;
/*!40000 ALTER TABLE `role_arena` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_arena` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_artifact`
--

DROP TABLE IF EXISTS `role_artifact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_artifact` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `id` int(11) unsigned NOT NULL COMMENT '模型ID',
  `cur_avatar` int(11) unsigned NOT NULL COMMENT '当前的幻化ID',
  `avatars` varchar(100) NOT NULL COMMENT '拥有的幻化id列表',
  `stren` int(11) unsigned NOT NULL COMMENT '强化等级',
  `stones` varchar(1024) NOT NULL COMMENT '宝石',
  `old_attr` varchar(2048) NOT NULL COMMENT '历史阶级累计额外属性加成',
  `cur_attr` varchar(2048) NOT NULL COMMENT '当前阶级额外属性加成',
  `chips` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '碎片收集',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='神器';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_artifact`
--

LOCK TABLES `role_artifact` WRITE;
/*!40000 ALTER TABLE `role_artifact` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_artifact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_bags`
--

DROP TABLE IF EXISTS `role_bags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_bags` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `bag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '背包ID',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '名字',
  `goods` text NOT NULL COMMENT '物品',
  `cell_num` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '格子数量',
  PRIMARY KEY (`role_id`,`bag_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家背包';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_bags`
--

LOCK TABLES `role_bags` WRITE;
/*!40000 ALTER TABLE `role_bags` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_bags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_basic`
--

DROP TABLE IF EXISTS `role_basic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_basic` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `accname` varchar(126) NOT NULL COMMENT '平台账号',
  `name` varchar(126) NOT NULL COMMENT '角色名',
  `career` tinyint(3) unsigned NOT NULL COMMENT '职业',
  `gender` tinyint(3) unsigned NOT NULL COMMENT '性别',
  `level` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '等级',
  `exp` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '经验',
  `hp` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '血量',
  `mp` int(10) unsigned NOT NULL DEFAULT '100' COMMENT '蓝量',
  `murderous` tinyint(4) NOT NULL DEFAULT '0' COMMENT '杀气值',
  `clr_mur_counter` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '清除杀气计数器',
  `fight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '战力',
  `last_login_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次登陆时间',
  `last_logout_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次登出时间',
  `last_upgrade_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次升级时间',
  `first_recharge` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '首充时间',
  `total_online` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总在线时间',
  `login_days` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '总登陆天数',
  `state` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '角色状态',
  `is_online` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否在线',
  `is_gm` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否内玩',
  `server_id` int(11) unsigned NOT NULL COMMENT '注册服id',
  `last_login_ip` varchar(62) NOT NULL DEFAULT '""' COMMENT '上次登陆ip',
  `device` varchar(252) NOT NULL DEFAULT '""' COMMENT '设备id',
  `skill_list` text NOT NULL COMMENT '技能',
  `role_personal` text NOT NULL COMMENT '个性化数据',
  `role_scene` text NOT NULL COMMENT '场景数据',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE KEY `name` (`name`) USING BTREE,
  KEY `level` (`level`) USING BTREE,
  KEY `is_gm` (`is_gm`) USING BTREE,
  KEY `state` (`state`) USING BTREE,
  KEY `accname_server_id` (`accname`,`server_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色基础数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_basic`
--

LOCK TABLES `role_basic` WRITE;
/*!40000 ALTER TABLE `role_basic` DISABLE KEYS */;
INSERT INTO `role_basic` VALUES (24247800496128,'zxczxc','大哥',4,2,1,0,826,187,0,0,0,1624905873,1624905872,0,0,0,0,0,1,0,1,'223.152.150.104','dafdfaee3959503793955417d72ba511','[{skill,3,10040001,1,0,0},{skill,3,10040002,1,0,0},{skill,3,10040003,1,0,0},{skill,3,10040011,1,0,0}]','#{bubble=>0,fashion=>65601537,frame=>0,hair=>3372220416,icon=>3,title=>0,title_honor=>0,ver=>3}','{role_scene,2,10001,1,undefined,1,0,436,80,{},{},0}');
/*!40000 ALTER TABLE `role_basic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_channel`
--

DROP TABLE IF EXISTS `role_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_channel` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '经脉ID',
  `hero` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '英雄ID',
  `equips` text COMMENT '经脉装备',
  `potentials` text COMMENT '经脉潜能',
  PRIMARY KEY (`role_id`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家经脉';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_channel`
--

LOCK TABLES `role_channel` WRITE;
/*!40000 ALTER TABLE `role_channel` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_chat`
--

DROP TABLE IF EXISTS `role_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_chat` (
  `role_id` bigint(20) NOT NULL COMMENT '玩家ID',
  `channels` varchar(1000) NOT NULL DEFAULT '[]' COMMENT '聊天频道数据',
  `sensitive` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发送敏感内容次数',
  `ban_type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '禁言类型',
  `ban_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '被禁言次数',
  `ban_until` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '禁言有效期',
  `same_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '连续发送相同聊天内容的次数',
  `last_content` varchar(255) NOT NULL DEFAULT '' COMMENT '上次发言的内容',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数据时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家聊天信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_chat`
--

LOCK TABLES `role_chat` WRITE;
/*!40000 ALTER TABLE `role_chat` DISABLE KEYS */;
INSERT INTO `role_chat` VALUES (24247800496128,'[]',0,0,0,0,0,'',1624905872);
/*!40000 ALTER TABLE `role_chat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_chess`
--

DROP TABLE IF EXISTS `role_chess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_chess` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `star` int(10) unsigned NOT NULL DEFAULT '0',
  `state` int(10) unsigned NOT NULL DEFAULT '0',
  `chal_times` int(10) unsigned NOT NULL DEFAULT '0',
  `left_times` int(10) unsigned NOT NULL DEFAULT '0',
  `life_times` int(10) unsigned NOT NULL DEFAULT '0',
  `reward_time` int(10) unsigned NOT NULL DEFAULT '0',
  `time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家珍珑棋局';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_chess`
--

LOCK TABLES `role_chess` WRITE;
/*!40000 ALTER TABLE `role_chess` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_chess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_create`
--

DROP TABLE IF EXISTS `role_create`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_create` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `accname` varchar(126) NOT NULL COMMENT '平台账号',
  `source` varchar(126) NOT NULL COMMENT '渠道',
  `server_id` int(11) unsigned NOT NULL COMMENT '创角服id',
  `career` tinyint(3) unsigned NOT NULL COMMENT '职业',
  `gender` tinyint(3) unsigned NOT NULL COMMENT '性别',
  `name` varchar(126) NOT NULL COMMENT '名字',
  `ip` varchar(62) NOT NULL COMMENT '创角ip地址',
  `device` varchar(126) NOT NULL COMMENT '创角设备号',
  `ctime` int(10) unsigned NOT NULL COMMENT '创角时间',
  `vip` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='创角表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_create`
--

LOCK TABLES `role_create` WRITE;
/*!40000 ALTER TABLE `role_create` DISABLE KEYS */;
INSERT INTO `role_create` VALUES (24247800496128,'zxczxc','vtnemo',1,4,2,'大哥','223.152.150.104','dafdfaee3959503793955417d72ba511',1624905872,0);
/*!40000 ALTER TABLE `role_create` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_daily`
--

DROP TABLE IF EXISTS `role_daily`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_daily` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `data` mediumblob NOT NULL COMMENT '数据',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='每日统计数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_daily`
--

LOCK TABLES `role_daily` WRITE;
/*!40000 ALTER TABLE `role_daily` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_daily` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_daily_event`
--

DROP TABLE IF EXISTS `role_daily_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_daily_event` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家id',
  `lv` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '剑池等级',
  `exp` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '剑池经验',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `cur_list` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '当天进行的剑池功能任务',
  `last_list` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '昨天可找回的剑池任务次数',
  `huanhua_lv` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '剑池幻化形象阶数',
  `got_reward` varchar(2048) NOT NULL COMMENT '每日阶段奖励领取状态',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_daily_event`
--

LOCK TABLES `role_daily_event` WRITE;
/*!40000 ALTER TABLE `role_daily_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_daily_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_daily_gift`
--

DROP TABLE IF EXISTS `role_daily_gift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_daily_gift` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `list` varchar(1024) NOT NULL COMMENT '购买状态列表',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='每日礼包';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_daily_gift`
--

LOCK TABLES `role_daily_gift` WRITE;
/*!40000 ALTER TABLE `role_daily_gift` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_daily_gift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_daily_lively`
--

DROP TABLE IF EXISTS `role_daily_lively`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_daily_lively` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `reward` varchar(255) NOT NULL DEFAULT '[]' COMMENT '奖励领取列表',
  `completed` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '完成列表',
  `retrieve` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '找回列表',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='每日活跃';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_daily_lively`
--

LOCK TABLES `role_daily_lively` WRITE;
/*!40000 ALTER TABLE `role_daily_lively` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_daily_lively` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_daily_robber`
--

DROP TABLE IF EXISTS `role_daily_robber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_daily_robber` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `max_times` int(11) unsigned NOT NULL COMMENT '最大次数',
  `times` int(11) unsigned NOT NULL COMMENT '当天完成次数',
  `scene_id` int(11) unsigned NOT NULL COMMENT '场景id',
  `x` int(11) unsigned NOT NULL COMMENT 'x坐标',
  `y` int(11) unsigned NOT NULL COMMENT 'y坐标',
  `mon_id` int(11) unsigned NOT NULL COMMENT '怪物ID',
  `state` tinyint(1) unsigned NOT NULL COMMENT '状态，0未领取|1进行中|2已完成',
  `last_abandon_time` int(11) unsigned NOT NULL COMMENT '上次放弃任务时间戳',
  `last_accept_time` int(11) unsigned NOT NULL COMMENT '上次接取任务时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='逞凶打图';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_daily_robber`
--

LOCK TABLES `role_daily_robber` WRITE;
/*!40000 ALTER TABLE `role_daily_robber` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_daily_robber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_daily_thief`
--

DROP TABLE IF EXISTS `role_daily_thief`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_daily_thief` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `daily_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '今日完成次数',
  `exp_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '经验奇遇结束时间戳',
  `roraty_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '转盘奇遇结束时间戳',
  `get_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '领取马贼宝箱奖励次数',
  `index` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '转盘选择ID',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='马贼任务';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_daily_thief`
--

LOCK TABLES `role_daily_thief` WRITE;
/*!40000 ALTER TABLE `role_daily_thief` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_daily_thief` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_deed`
--

DROP TABLE IF EXISTS `role_deed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_deed` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `deed_target` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '誓约对象ID',
  `deed_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '誓约结束时间戳',
  `complete` varchar(2048) NOT NULL DEFAULT '' COMMENT '完成列表',
  `get_list` varchar(2048) NOT NULL DEFAULT '' COMMENT '领取列表',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='契约';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_deed`
--

LOCK TABLES `role_deed` WRITE;
/*!40000 ALTER TABLE `role_deed` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_deed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_delete`
--

DROP TABLE IF EXISTS `role_delete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_delete` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `accname` varchar(126) NOT NULL COMMENT '平台账号',
  `state` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '删除状态',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '操作时间',
  `server_id` int(11) unsigned NOT NULL COMMENT '注册服id',
  `level` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '等级',
  `career` tinyint(3) unsigned NOT NULL COMMENT '职业',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='删号表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_delete`
--

LOCK TABLES `role_delete` WRITE;
/*!40000 ALTER TABLE `role_delete` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_delete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_dividend`
--

DROP TABLE IF EXISTS `role_dividend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_dividend` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `lv_list` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '冲级大赛领取列表',
  `stone_list` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '点石成金领取列表',
  `lively_list` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '幸运抽奖活跃领取列表',
  `charge_list` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '幸运抽奖充值领取列表',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查数据时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='开服红利';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_dividend`
--

LOCK TABLES `role_dividend` WRITE;
/*!40000 ALTER TABLE `role_dividend` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_dividend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_dragon`
--

DROP TABLE IF EXISTS `role_dragon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_dragon` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `items` text COMMENT '龙元',
  `growth_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `growth_hole` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '孔',
  `refine_star` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '血炼星级',
  `refine_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '血炼等级',
  `refine_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '血炼次数',
  `refine_exp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '血炼经验',
  `refine_quality` text COMMENT '血炼资质',
  `refine_star_r` int(10) unsigned NOT NULL DEFAULT '0',
  `refine_lv_r` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '血炼等级',
  `refine_quality_r` text COMMENT '血炼资质',
  `condense_state` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '凝元状态',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家龙纹';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_dragon`
--

LOCK TABLES `role_dragon` WRITE;
/*!40000 ALTER TABLE `role_dragon` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_dragon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_dungeon`
--

DROP TABLE IF EXISTS `role_dungeon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_dungeon` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `dung_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '副本ID',
  `now_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天等级',
  `now_wave` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天波数',
  `max_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '历史最高等级',
  `max_wave` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '历史最高波数',
  `ymax_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '昨天历史最高等级',
  `ymax_wave` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '昨天历史最高波数',
  `chal_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天已挑战次数',
  `wipe_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天已扫荡次数',
  `assist_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天进入次数',
  `reset_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当天已重置次数',
  `life_his` text COMMENT '历史挑战记录，[{关卡,次数}]',
  `daily_his` text COMMENT '当天挑战记录，[{关卡,次数}]',
  `daily_wipe` text COMMENT '当天扫荡记录，[{关卡,次数}] ',
  `daily_reward` text COMMENT '当天奖励记录，[{关卡,波数}]',
  `star_info` text COMMENT '历史星级评价列表，[{关卡,星数}]',
  `his_reward` text COMMENT '历史奖励记录',
  `first_reward` text COMMENT '历史首通奖励情况，[{关卡, 波数}]',
  `chapter_reward` text COMMENT '历史章节奖励记录，[{章节,几星}]',
  `extra` text COMMENT '额外信息',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次重置时间戳',
  PRIMARY KEY (`role_id`,`dung_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家副本';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_dungeon`
--

LOCK TABLES `role_dungeon` WRITE;
/*!40000 ALTER TABLE `role_dungeon` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_dungeon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_equip`
--

DROP TABLE IF EXISTS `role_equip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_equip` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `pos` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '装备位置',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '装备ID',
  `stren` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '强化等级',
  `stones` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '宝石',
  `attr` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '属性(固定、随机)',
  `extra` text NOT NULL COMMENT '额外数据',
  PRIMARY KEY (`role_id`,`pos`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家装备';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_equip`
--

LOCK TABLES `role_equip` WRITE;
/*!40000 ALTER TABLE `role_equip` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_equip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_examine`
--

DROP TABLE IF EXISTS `role_examine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_examine` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '此轮答题题目ID',
  `help_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '求助次数',
  `answer_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '已回答题目数量',
  `right_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '答对题数',
  `acc_get` varchar(1024) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '[]' COMMENT '累计获得',
  `is_get` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否已领取',
  `begin_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '首次打开界面时间戳',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '答题完时间戳',
  `help_reward` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '获得帮助答题奖励次数',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='科举考试';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_examine`
--

LOCK TABLES `role_examine` WRITE;
/*!40000 ALTER TABLE `role_examine` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_examine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_fashion`
--

DROP TABLE IF EXISTS `role_fashion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_fashion` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时装ID',
  `dyeing` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '染色',
  `exp_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  `valid` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否有效',
  PRIMARY KEY (`role_id`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家时装';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_fashion`
--

LOCK TABLES `role_fashion` WRITE;
/*!40000 ALTER TABLE `role_fashion` DISABLE KEYS */;
INSERT INTO `role_fashion` VALUES (24247800496128,1001,1,0,1);
/*!40000 ALTER TABLE `role_fashion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_friend`
--

DROP TABLE IF EXISTS `role_friend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_friend` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `apply_list` text NOT NULL COMMENT '申请列表',
  `friend_list` text NOT NULL COMMENT '好友列表',
  `focus_list` text NOT NULL COMMENT '关注列表',
  `ban_list` text NOT NULL COMMENT '黑名单列表',
  `enemy_list` text NOT NULL COMMENT '仇人列表',
  `be_focused_list` text NOT NULL COMMENT '被关注列表',
  `block_list` text NOT NULL COMMENT '分组具体信息列表',
  `group_id_list` text NOT NULL COMMENT '群组ID列表',
  `own_group_id_list` varchar(1024) NOT NULL COMMENT '创建的群组ID列表',
  `offline_msg_f` text NOT NULL COMMENT '好友间离线聊天信息记录',
  `offline_msg_g` text NOT NULL COMMENT '群组间离线聊天信息记录',
  `nick_names` text NOT NULL COMMENT '好友备注名字列表',
  `last_update_time` int(11) unsigned NOT NULL COMMENT '上次修改过的时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家好友';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_friend`
--

LOCK TABLES `role_friend` WRITE;
/*!40000 ALTER TABLE `role_friend` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_friend` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_func`
--

DROP TABLE IF EXISTS `role_func`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_func` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `funcs` text NOT NULL COMMENT '功能开放数据([sys_id, ...])',
  `guide` text NOT NULL COMMENT '新手步骤数据([{id, num}, ...])',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家功能开放';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_func`
--

LOCK TABLES `role_func` WRITE;
/*!40000 ALTER TABLE `role_func` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_func` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_gather`
--

DROP TABLE IF EXISTS `role_gather`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_gather` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `vitality` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '活力',
  `reward_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上次回复时间',
  `skills` text COMMENT '采集技能',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='装备采集';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_gather`
--

LOCK TABLES `role_gather` WRITE;
/*!40000 ALTER TABLE `role_gather` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_gather` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_grow_fund`
--

DROP TABLE IF EXISTS `role_grow_fund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_grow_fund` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `grade` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '当前档次',
  `got_list` varchar(1024) NOT NULL COMMENT '已领取ID列表',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='成长基金';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_grow_fund`
--

LOCK TABLES `role_grow_fund` WRITE;
/*!40000 ALTER TABLE `role_grow_fund` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_grow_fund` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_growup`
--

DROP TABLE IF EXISTS `role_growup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_growup` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '成长线ID',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `image` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '幻化形象',
  `pgs_s` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '非限时进度',
  `pgs_t` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '限时进度',
  `expire` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  `skills` text COMMENT '技能列表',
  `equips` text COMMENT '装备列表',
  `skins` text NOT NULL COMMENT '皮肤列表',
  `skin` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '当前皮肤',
  `pill` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '属性丹',
  `achieve` text COMMENT '成就',
  `hide` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  PRIMARY KEY (`role_id`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='成长线';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_growup`
--

LOCK TABLES `role_growup` WRITE;
/*!40000 ALTER TABLE `role_growup` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_growup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_guild`
--

DROP TABLE IF EXISTS `role_guild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_guild` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `cook_type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '烹饪方式',
  `cook_reward` text COMMENT '烹饪奖励记录',
  `live_level` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '活跃等级',
  `live_exp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '活跃经验',
  `live_daily` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '每日活跃',
  `live_tasks` text COMMENT '活跃任务',
  `live_reward` text COMMENT '每日活跃奖励记录',
  `skills` text COMMENT '技能',
  `ex_goods` text COMMENT '兑换物品',
  `ex_refresh` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '兑换刷新时间',
  `ex_manual` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '手动刷新兑换次数',
  `practice_skill` text COMMENT '帮派修炼技能',
  `time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数据修改时间',
  `prac_max_lv` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '帮派修炼等级突破上限',
  `metall_lively` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '炼金活跃值',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家帮派数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_guild`
--

LOCK TABLES `role_guild` WRITE;
/*!40000 ALTER TABLE `role_guild` DISABLE KEYS */;
INSERT INTO `role_guild` VALUES (24247800496128,0,NULL,1,0,0,NULL,NULL,NULL,NULL,0,0,NULL,1624905872,0,0);
/*!40000 ALTER TABLE `role_guild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_guild_carry`
--

DROP TABLE IF EXISTS `role_guild_carry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_guild_carry` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `quality` tinyint(4) unsigned NOT NULL COMMENT '品质',
  `stat` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `carry_line` tinyint(4) unsigned NOT NULL COMMENT '镖车路线',
  `expire_time` int(11) unsigned NOT NULL COMMENT '过期时间',
  `scene_id` int(11) unsigned NOT NULL COMMENT '所在场景',
  `line_id` bigint(20) unsigned NOT NULL COMMENT '所在分线',
  `x` smallint(5) unsigned NOT NULL COMMENT 'x坐标',
  `y` smallint(5) unsigned NOT NULL COMMENT 'y坐标',
  `path_index` tinyint(4) unsigned NOT NULL COMMENT '路径索引',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派运镖';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_guild_carry`
--

LOCK TABLES `role_guild_carry` WRITE;
/*!40000 ALTER TABLE `role_guild_carry` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_guild_carry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_guild_task`
--

DROP TABLE IF EXISTS `role_guild_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_guild_task` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `flag` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '状态(0:未接取|1:正在进行)',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '正在进行时，接取的任务类型',
  `id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '该任务类型下的id',
  `finish_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '此次任务已完成次数',
  `drop_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次放弃任务时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派任务';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_guild_task`
--

LOCK TABLES `role_guild_task` WRITE;
/*!40000 ALTER TABLE `role_guild_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_guild_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_guild_wine_act`
--

DROP TABLE IF EXISTS `role_guild_wine_act`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_guild_wine_act` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `exp_get` int(11) unsigned NOT NULL COMMENT '累计获得的经验值',
  `quiz_info` varchar(200) NOT NULL COMMENT '问题回答情况[{id,quiz_id,answer,state}]',
  `dice_num` int(11) unsigned NOT NULL COMMENT '骰子总点数',
  `quiz_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '答问题的时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_guild_wine_act`
--

LOCK TABLES `role_guild_wine_act` WRITE;
/*!40000 ALTER TABLE `role_guild_wine_act` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_guild_wine_act` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_hero`
--

DROP TABLE IF EXISTS `role_hero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_hero` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '英雄ID',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `exp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '经验',
  `skill` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '指点技能',
  `legend` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '真武指定状态',
  PRIMARY KEY (`role_id`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家英雄';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_hero`
--

LOCK TABLES `role_hero` WRITE;
/*!40000 ALTER TABLE `role_hero` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_hero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_hero_charm`
--

DROP TABLE IF EXISTS `role_hero_charm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_hero_charm` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `hero` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '豪情值',
  `charm` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '魅力值',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='豪情魅力值';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_hero_charm`
--

LOCK TABLES `role_hero_charm` WRITE;
/*!40000 ALTER TABLE `role_hero_charm` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_hero_charm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_hero_guide`
--

DROP TABLE IF EXISTS `role_hero_guide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_hero_guide` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '方案ID',
  `name` varchar(50) DEFAULT NULL COMMENT '名字',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `plan` text COMMENT '指点方案',
  PRIMARY KEY (`role_id`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='英雄指点方案';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_hero_guide`
--

LOCK TABLES `role_hero_guide` WRITE;
/*!40000 ALTER TABLE `role_hero_guide` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_hero_guide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_kill_mon_exp`
--

DROP TABLE IF EXISTS `role_kill_mon_exp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_kill_mon_exp` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `have_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '天灵丹使用次数',
  `keep_exp` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '当前存储经验',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  `dl_keep_exp` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '地灵丹储存经验',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='江湖历练';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_kill_mon_exp`
--

LOCK TABLES `role_kill_mon_exp` WRITE;
/*!40000 ALTER TABLE `role_kill_mon_exp` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_kill_mon_exp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_kill_mon_retrieve`
--

DROP TABLE IF EXISTS `role_kill_mon_retrieve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_kill_mon_retrieve` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '杀怪数量统计',
  `flag` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否领取标识',
  `leave_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '昨天剩余数量',
  `last_check` int(11) unsigned NOT NULL COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历练补偿';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_kill_mon_retrieve`
--

LOCK TABLES `role_kill_mon_retrieve` WRITE;
/*!40000 ALTER TABLE `role_kill_mon_retrieve` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_kill_mon_retrieve` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_kv_data`
--

DROP TABLE IF EXISTS `role_kv_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_kv_data` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `data` mediumblob NOT NULL COMMENT '键值数据',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家键值数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_kv_data`
--

LOCK TABLES `role_kv_data` WRITE;
/*!40000 ALTER TABLE `role_kv_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_kv_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_mail`
--

DROP TABLE IF EXISTS `role_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_mail` (
  `role_id` bigint(20) NOT NULL COMMENT '玩家ID',
  `mail_id` bigint(20) NOT NULL COMMENT '邮件ID',
  `state` int(11) DEFAULT '0' COMMENT '邮件状态',
  `time` int(11) DEFAULT '0' COMMENT '时间戳',
  PRIMARY KEY (`role_id`,`mail_id`) USING BTREE,
  KEY `mail_id` (`mail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家邮件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_mail`
--

LOCK TABLES `role_mail` WRITE;
/*!40000 ALTER TABLE `role_mail` DISABLE KEYS */;
INSERT INTO `role_mail` VALUES (24247800496128,24247801057280,0,1624906009);
/*!40000 ALTER TABLE `role_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_marriage`
--

DROP TABLE IF EXISTS `role_marriage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_marriage` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `bless` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '祝福等级',
  `skills` text COMMENT '技能',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家婚姻';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_marriage`
--

LOCK TABLES `role_marriage` WRITE;
/*!40000 ALTER TABLE `role_marriage` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_marriage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_mentor`
--

DROP TABLE IF EXISTS `role_mentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_mentor` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `mentor_id` bigint(20) unsigned NOT NULL COMMENT '师父ID，0还没有师父',
  `mentor_quiz_list` varchar(1024) NOT NULL COMMENT '师父问卷列表',
  `tudi_quiz_list` varchar(1024) NOT NULL COMMENT '徒弟问卷列表',
  `registered` int(11) unsigned NOT NULL COMMENT '是否在平台报名，0否1是',
  `mentor_tasks` text NOT NULL COMMENT '师门任务进度列表',
  `taixue_tasks` text NOT NULL COMMENT '太学册任务进度列表',
  `mentor_taken` varchar(1024) NOT NULL COMMENT '师徒任务奖励领取情况',
  `morality` int(11) unsigned NOT NULL COMMENT '师德值',
  `mentor_lv` int(11) unsigned NOT NULL COMMENT '师父等级',
  `comment_pop` varchar(100) NOT NULL COMMENT '自动弹评价师父记录',
  `last_mentor_time` int(11) unsigned NOT NULL COMMENT '上次刷新师徒任务时间戳',
  `last_taixue_time` int(11) unsigned NOT NULL COMMENT '上次刷新太学册任务时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='师徒个人数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_mentor`
--

LOCK TABLES `role_mentor` WRITE;
/*!40000 ALTER TABLE `role_mentor` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_mentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_money`
--

DROP TABLE IF EXISTS `role_money`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_money` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `gold` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '元宝',
  `bgold` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '绑定元宝',
  `coin` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '金币',
  `fcoin` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '友情币',
  `cont` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '帮派贡献',
  `credit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '声望',
  `currency` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '帮派通宝',
  `forge_score` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '打造积分',
  `essence` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '百炼精华',
  `emprise` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '侠义值',
  `silver` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '银两',
  `sgold` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '储备元宝',
  `wfruit` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '许愿果',
  `tball` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '宝珠',
  `jifen` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `peach_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '桃李值',
  `love_value` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '历史恩爱值',
  `fate_token` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '天命印记',
  `song_liao` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '宋辽积分',
  `serum` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '龙元精粹',
  `history` text COMMENT '历史金钱',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家金钱';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_money`
--

LOCK TABLES `role_money` WRITE;
/*!40000 ALTER TABLE `role_money` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_money` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_online_reward`
--

DROP TABLE IF EXISTS `role_online_reward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_online_reward` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '已祈福次数',
  `list` varchar(255) NOT NULL COMMENT '已抽取ID列表',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='在线奖励';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_online_reward`
--

LOCK TABLES `role_online_reward` WRITE;
/*!40000 ALTER TABLE `role_online_reward` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_online_reward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_paladin`
--

DROP TABLE IF EXISTS `role_paladin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_paladin` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '侠客ID',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '等级',
  `exp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '经验',
  `star` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '星级',
  PRIMARY KEY (`role_id`,`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家侠客';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_paladin`
--

LOCK TABLES `role_paladin` WRITE;
/*!40000 ALTER TABLE `role_paladin` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_paladin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_paladin_info`
--

DROP TABLE IF EXISTS `role_paladin_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_paladin_info` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `battle` varchar(255) NOT NULL DEFAULT '[]' COMMENT '出战侠客',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家侠客信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_paladin_info`
--

LOCK TABLES `role_paladin_info` WRITE;
/*!40000 ALTER TABLE `role_paladin_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_paladin_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_pet`
--

DROP TABLE IF EXISTS `role_pet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_pet` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `in_hatching` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否正在孵化',
  `depot_size` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '开启的仓库格子',
  `lucky` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '幸运值',
  `pet_fight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '珍兽为玩家提供的战力',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='珍兽系统数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_pet`
--

LOCK TABLES `role_pet` WRITE;
/*!40000 ALTER TABLE `role_pet` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_pet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_pet_attachs`
--

DROP TABLE IF EXISTS `role_pet_attachs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_pet_attachs` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `attach_id` tinyint(3) unsigned NOT NULL COMMENT '阵法id',
  `internals` varchar(254) NOT NULL DEFAULT '[]' COMMENT '内丹数据',
  `fight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '内丹战力和',
  PRIMARY KEY (`role_id`,`attach_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='珍兽附体';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_pet_attachs`
--

LOCK TABLES `role_pet_attachs` WRITE;
/*!40000 ALTER TABLE `role_pet_attachs` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_pet_attachs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_pets`
--

DROP TABLE IF EXISTS `role_pets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_pets` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `grid` tinyint(3) unsigned NOT NULL COMMENT '格子',
  `cid` smallint(5) unsigned NOT NULL COMMENT '珍兽id',
  `stat` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `attach_id` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '附体id',
  `name` varchar(126) NOT NULL COMMENT '名字',
  `level` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT '等级',
  `exp` bigint(20) unsigned NOT NULL COMMENT '经验',
  `star` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '星级',
  `savvy_lv` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '悟性等级',
  `growup_lv` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '成长等级',
  `growup_rate` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '成长率',
  `awaken` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '觉醒次数',
  `potential` varchar(63) NOT NULL DEFAULT '{0,0,0,0,0}' COMMENT '资质',
  `init_attr` varchar(63) NOT NULL DEFAULT '[]' COMMENT '初始属性',
  `skills` varchar(1020) NOT NULL DEFAULT '[]' COMMENT '技能列表',
  `fight` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '战力',
  `sell_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出售时间',
  `sell_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出售次数',
  UNIQUE KEY `role_id_grid` (`role_id`,`grid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='珍兽';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_pets`
--

LOCK TABLES `role_pets` WRITE;
/*!40000 ALTER TABLE `role_pets` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_pets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_raider`
--

DROP TABLE IF EXISTS `role_raider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_raider` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `buy_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '购买次数',
  `left_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '剩余次数',
  `life_times` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '历史挑战次数',
  `reward_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '定时奖励时间',
  `set_list` text COMMENT '关注列表',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家夺宝奇兵数据';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_raider`
--

LOCK TABLES `role_raider` WRITE;
/*!40000 ALTER TABLE `role_raider` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_raider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_seven_login`
--

DROP TABLE IF EXISTS `role_seven_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_seven_login` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `login_day` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '登录天数',
  `list` varchar(1024) NOT NULL DEFAULT '[]' COMMENT '领取列表',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='七天登录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_seven_login`
--

LOCK TABLES `role_seven_login` WRITE;
/*!40000 ALTER TABLE `role_seven_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_seven_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_shop`
--

DROP TABLE IF EXISTS `role_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_shop` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `items` text COMMENT '物品',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家商店';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_shop`
--

LOCK TABLES `role_shop` WRITE;
/*!40000 ALTER TABLE `role_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_sign`
--

DROP TABLE IF EXISTS `role_sign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_sign` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `daily` text COMMENT '每日签到',
  `acc` text COMMENT '累计签到领取',
  `bq_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '今日已补签次数',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  `is_get` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否已签到',
  `times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '拥有补签次数',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='签到';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_sign`
--

LOCK TABLES `role_sign` WRITE;
/*!40000 ALTER TABLE `role_sign` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_sign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_society`
--

DROP TABLE IF EXISTS `role_society`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_society` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `star` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总星数',
  `tasks` text COMMENT '任务列表',
  `status` text COMMENT '星级奖励状态',
  `mails` text COMMENT '邮件',
  `open_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '开启时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='江湖之路';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_society`
--

LOCK TABLES `role_society` WRITE;
/*!40000 ALTER TABLE `role_society` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_society` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_task`
--

DROP TABLE IF EXISTS `role_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_task` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `tasks` text COMMENT '进行中任务',
  `last` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后一个任务',
  `finished` text COMMENT '已完成任务',
  `time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家任务';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_task`
--

LOCK TABLES `role_task` WRITE;
/*!40000 ALTER TABLE `role_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_task_his`
--

DROP TABLE IF EXISTS `role_task_his`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_task_his` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `task_id` int(10) unsigned NOT NULL COMMENT '任务id',
  `accept_time` int(10) unsigned NOT NULL COMMENT '接取时间',
  `finish_time` int(10) unsigned NOT NULL COMMENT '完成时间',
  KEY `role_id` (`role_id`) USING BTREE,
  KEY `task_id` (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='任务历史纪录(非log)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_task_his`
--

LOCK TABLES `role_task_his` WRITE;
/*!40000 ALTER TABLE `role_task_his` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_task_his` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_title_list`
--

DROP TABLE IF EXISTS `role_title_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_title_list` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `title_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '称号ID',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间',
  `valid` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否有效',
  `name` varchar(1024) NOT NULL DEFAULT '' COMMENT '专属称号名',
  PRIMARY KEY (`role_id`,`title_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家称号列表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_title_list`
--

LOCK TABLES `role_title_list` WRITE;
/*!40000 ALTER TABLE `role_title_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_title_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_treasure_map`
--

DROP TABLE IF EXISTS `role_treasure_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_treasure_map` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `is_trigger` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否已接任务',
  `task_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每日任务完成次数',
  `daily_rare_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每日珍惜使用次数',
  `daily_top_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每日绝世使用次数',
  `daily_times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '每日使用次数',
  `is_complete` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否已完成',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='藏宝图';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_treasure_map`
--

LOCK TABLES `role_treasure_map` WRITE;
/*!40000 ALTER TABLE `role_treasure_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_treasure_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_view`
--

DROP TABLE IF EXISTS `role_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_view` (
  `role_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `view_info` text COMMENT '玩家信息',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='玩家查看信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_view`
--

LOCK TABLES `role_view` WRITE;
/*!40000 ALTER TABLE `role_view` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_view` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_vip`
--

DROP TABLE IF EXISTS `role_vip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_vip` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '角色id',
  `level` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'vip等级',
  `exp` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'vip经验',
  `got_gifts` varchar(1022) NOT NULL DEFAULT '[]' COMMENT '已领取奖励',
  `last_update` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后更新时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色VIP';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_vip`
--

LOCK TABLES `role_vip` WRITE;
/*!40000 ALTER TABLE `role_vip` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_vip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_vow`
--

DROP TABLE IF EXISTS `role_vow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_vow` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `context` varchar(2048) NOT NULL DEFAULT '' COMMENT '许愿内容',
  `be_like_num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '被点赞数量',
  `like_list` varchar(2048) NOT NULL DEFAULT '' COMMENT '点赞列表',
  `show_list` varchar(2048) NOT NULL DEFAULT '' COMMENT '显示列表',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '当前的刷新类型',
  `is_post` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否许愿',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='许愿';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_vow`
--

LOCK TABLES `role_vow` WRITE;
/*!40000 ALTER TABLE `role_vow` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_vow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_wages`
--

DROP TABLE IF EXISTS `role_wages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_wages` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `stages` varchar(2048) NOT NULL DEFAULT '[]' COMMENT '完成进度',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='帮派工资';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_wages`
--

LOCK TABLES `role_wages` WRITE;
/*!40000 ALTER TABLE `role_wages` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_wages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_warrior_soul`
--

DROP TABLE IF EXISTS `role_warrior_soul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_warrior_soul` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `lv` int(11) unsigned NOT NULL COMMENT '精铸等级',
  `star_lv` int(11) unsigned NOT NULL COMMENT '星级',
  `skills` varchar(1024) NOT NULL COMMENT '武魂技能列表',
  `stren` int(11) unsigned NOT NULL COMMENT '强化等级',
  `stones` varchar(1024) NOT NULL COMMENT '宝石列表',
  `cur_avatar` int(11) unsigned NOT NULL COMMENT '当前的精魄幻化',
  `avatars` varchar(2048) NOT NULL COMMENT '拥有的精魄幻化列表',
  `add_succ_rate` int(11) unsigned NOT NULL COMMENT '精铸额外成功率',
  `conden_num` int(11) unsigned NOT NULL COMMENT '总凝魂次数',
  `soul_parts` text NOT NULL COMMENT '凝魂部位列表',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='武魂';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_warrior_soul`
--

LOCK TABLES `role_warrior_soul` WRITE;
/*!40000 ALTER TABLE `role_warrior_soul` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_warrior_soul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_weekly_consume`
--

DROP TABLE IF EXISTS `role_weekly_consume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_weekly_consume` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `weekly_consume` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '周消费元宝',
  `guard` varchar(1024) NOT NULL COMMENT '保底',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  `index` int(11) unsigned NOT NULL COMMENT '转盘已抽取ID',
  `got_list` varchar(255) NOT NULL COMMENT '已领取列表',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='周消费';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_weekly_consume`
--

LOCK TABLES `role_weekly_consume` WRITE;
/*!40000 ALTER TABLE `role_weekly_consume` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_weekly_consume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_weekly_month_card`
--

DROP TABLE IF EXISTS `role_weekly_month_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_weekly_month_card` (
  `role_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '玩家ID',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '类型(9周卡|10月卡)',
  `expire_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '过期时间戳',
  `flag` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '购买状态',
  `last_check` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上次检查时间戳',
  PRIMARY KEY (`role_id`,`type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='月卡';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_weekly_month_card`
--

LOCK TABLES `role_weekly_month_card` WRITE;
/*!40000 ALTER TABLE `role_weekly_month_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_weekly_month_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rumor`
--

DROP TABLE IF EXISTS `rumor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rumor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `type` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '方式',
  `timing` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '定时类型',
  `begin_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '开始时间',
  `end_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '结束时间',
  `interval` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '间隔',
  `times` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发送次数',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `valid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否有效',
  `scope` text NOT NULL COMMENT '频道',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='传闻';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rumor`
--

LOCK TABLES `rumor` WRITE;
/*!40000 ALTER TABLE `rumor` DISABLE KEYS */;
/*!40000 ALTER TABLE `rumor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sworn_group`
--

DROP TABLE IF EXISTS `sworn_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sworn_group` (
  `group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一ID',
  `name` varchar(100) NOT NULL COMMENT '名号',
  `quality` int(11) unsigned NOT NULL COMMENT '称号品质',
  `members` varchar(1024) NOT NULL COMMENT '成员列表',
  `tend_career` int(11) unsigned NOT NULL COMMENT '意向职业，1近战，2远程，3随意',
  `tend_lv` int(11) unsigned NOT NULL COMMENT '意向等级，1相同，2相差5级内，3随意',
  `tend_time` int(11) unsigned NOT NULL COMMENT '意向活跃时间，1|24小时，2|白天，3|晚上',
  `sworn_value` int(11) unsigned NOT NULL COMMENT '金兰值',
  `sworn_word` varchar(1024) NOT NULL COMMENT '金兰宣言',
  `params` varchar(1024) NOT NULL COMMENT '其他参数',
  `registered` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '是否已经在结拜平台登记，0否，1是',
  `last_update_time` int(11) unsigned NOT NULL COMMENT '上次更新时间戳',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='结拜组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sworn_group`
--

LOCK TABLES `sworn_group` WRITE;
/*!40000 ALTER TABLE `sworn_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `sworn_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sworn_person`
--

DROP TABLE IF EXISTS `sworn_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sworn_person` (
  `role_id` bigint(20) unsigned NOT NULL COMMENT '玩家ID',
  `name` varchar(1024) NOT NULL COMMENT '名字',
  `lv` int(11) unsigned NOT NULL COMMENT '等级',
  `icon` int(11) unsigned NOT NULL COMMENT '头像',
  `frame` int(11) unsigned NOT NULL COMMENT '头像框',
  `career` int(11) unsigned NOT NULL COMMENT '职业',
  `title_honor` int(11) unsigned NOT NULL COMMENT '头衔',
  `guild_name` varchar(1024) NOT NULL COMMENT '帮会名',
  `tend_career` int(11) unsigned NOT NULL COMMENT '意向职业，1近战，2远程，3随意',
  `tend_lv` int(11) unsigned NOT NULL COMMENT '意向等级，1相同，2相差5级内，3随意',
  `tend_time` int(11) unsigned NOT NULL COMMENT '意向活跃时间，1|24小时，2|白天，3|晚上',
  `last_update_time` int(11) unsigned NOT NULL COMMENT '上次更新时间戳',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='结拜个人';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sworn_person`
--

LOCK TABLES `sworn_person` WRITE;
/*!40000 ALTER TABLE `sworn_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `sworn_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `territory`
--

DROP TABLE IF EXISTS `territory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `territory` (
  `week` int(11) NOT NULL DEFAULT '0' COMMENT '周',
  `round` int(11) NOT NULL DEFAULT '0' COMMENT '回合',
  `territories` text COMMENT '领地',
  `matches` text COMMENT '匹配',
  PRIMARY KEY (`week`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='领地战';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `territory`
--

LOCK TABLES `territory` WRITE;
/*!40000 ALTER TABLE `territory` DISABLE KEYS */;
/*!40000 ALTER TABLE `territory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tlry_s1'
--

--
-- Dumping routines for database 'tlry_s1'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-16  8:57:09
