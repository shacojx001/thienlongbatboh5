<?php
function autoload($class_name)
{
    if (class_exists($class_name, false) || interface_exists($class_name, false)) {
        return;
    }
    if (strpos($class_name, 'Smarty') !== false || strpos($class_name , 'Hprose') !== false) {
        return;
    } else {
        $file = str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';
    }
    if (preg_match('/[^a-z0-9\\/\\\\_.-]/i', $file)) {
        return;
    }
    include_once $file;
}

/**
 * 调试
 *
 * @param mix $var
 *
 */
function d($var)
{
    echo "<pre>";
    print_r($var);
    echo "</pre>";
    exit;
}

/**
 * 随即密码
 *
 * @param int $len
 * @param string $format
 * @return string
 */
function string_mkPasswd($len = 6, $format = 'ALL')
{
    switch (strtolower($format)) {
        case 'all':
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-@#~';
            break;
        case 'char':
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
            break;
        case 'char2':
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-@#~';
            break;
        case 'num':
            $chars = '0123456789';
            break;
        default :
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-@#~';
            break;
    }

    mt_srand((double)microtime() * 1000000 * getmypid());
    $passwd = '';
    while (strlen($passwd) < $len) $passwd .= substr($chars, (mt_rand() % strlen($chars)), 1);
    return $passwd;
}

function cutstr($string, $length, $dot = ' ...')
{

    if (!defined('CHARSET')) {
        define('CHARSET', 'utf-8');
    }

    if (strlen($string) <= $length) {
        return $string;
    }

    $string = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'), array('&', '"', '<', '>'), $string);

    $strcut = '';
    if (strtolower(CHARSET) == 'utf-8') {

        $n = $tn = $noc = 0;
        while ($n < strlen($string)) {

            $t = ord($string [$n]);
            if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                $tn = 1;
                $n++;
                $noc++;
            } elseif (194 <= $t && $t <= 223) {
                $tn = 2;
                $n += 2;
                $noc += 2;
            } elseif (224 <= $t && $t < 239) {
                $tn = 3;
                $n += 3;
                $noc += 2;
            } elseif (240 <= $t && $t <= 247) {
                $tn = 4;
                $n += 4;
                $noc += 2;
            } elseif (248 <= $t && $t <= 251) {
                $tn = 5;
                $n += 5;
                $noc += 2;
            } elseif ($t == 252 || $t == 253) {
                $tn = 6;
                $n += 6;
                $noc += 2;
            } else {
                $n++;
            }

            if ($noc >= $length) {
                break;
            }

        }
        if ($noc > $length) {
            $n -= $tn;
        }

        $strcut = substr($string, 0, $n);

    } else {
        for ($i = 0; $i < $length; $i++) {
            $strcut .= ord($string [$i]) > 127 ? $string [$i] . $string [++$i] : $string [$i];
        }
    }

    $strcut = str_replace(array('&', '"', '<', '>'), array('&amp;', '&quot;', '&lt;', '&gt;'), $strcut);

    return $strcut . $dot;
}

//获取IP
function getIP()
{
    $cip = getenv('HTTP_CLIENT_IP');
    $xip = getenv('HTTP_X_FORWARDED_FOR');
    $rip = getenv('REMOTE_ADDR');
    $srip = $_SERVER ['REMOTE_ADDR'];
    if ($cip && strcasecmp($cip, 'unknown')) {
        $onlineip = $cip;
    } elseif ($xip && strcasecmp($xip, 'unknown')) {
        $onlineip = $xip;
    } elseif ($rip && strcasecmp($rip, 'unknown')) {
        $onlineip = $rip;
    } elseif ($srip && strcasecmp($srip, 'unknown')) {
        $onlineip = $srip;
    }

    preg_match("/[\d\.]{7,15}/", $onlineip, $match);
    return $match [0] ? $match [0] : 'unknown';
}

/**
 * safe_file_get_contents() 用共享锁模式打开文件并读取内容，可以避免在并发写入造成的读取不完整问题
 *
 * @param string $filename
 *
 */
function safe_file_get_contents($filename)
{
    if (!is_readable($filename)) {
        return false;
    }

    $filesize = filesize($filename);
    if ($filesize <= 0) {
        return false;
    }

    $fp = @fopen($filename, "rb");
    if ($fp) {
        flock($fp, LOCK_EX);
        $contents = @fread($fp, $filesize);
        flock($fp, LOCK_UN);
        fclose($fp);
        return $contents;
    } else {
        return false;
    }
}

/**
 * safe_file_put_contents() 一次性完成打开文件，写入内容，关闭文件三项工作，并且确保写入时不会造成并发冲突
 *
 * @param string $filename
 * @param string $contents
 *
 * @return boolean
 */
function safe_file_put_contents($filename, $contents)
{

    $fp = @fopen($filename, "wb");
    if ($fp) {
        flock($fp, LOCK_EX);
        @fwrite($fp, $contents);
        flock($fp, LOCK_UN);
        fclose($fp);
        return true;
    } else {
        return false;
    }
}

/**
 * safe_file_append_contents() 一次性完成打开文件，追加写入内容，关闭文件三项工作，并且确保追加写入时不会造成并发冲突
 *
 * @param string $filename
 * @param string $contents
 *
 * @return boolean
 */
function safe_file_append_contents($filename, $contents)
{

    $fp = @fopen($filename, "ab");
    if ($fp) {
        flock($fp, LOCK_EX);
        @fwrite($fp, $contents);
        flock($fp, LOCK_UN);
        fclose($fp);
        return true;
    } else {
        return false;
    }
}

/**
 * cache_read() 读取缓存 以return array();形式读取
 *
 * @param string $file
 * @param string $mode
 *
 * @return $data
 */
function cache_read($file, $mode = 1)
{
    if (!file_exists($file) || filesize($file) <= 0) {
        return false;
    }
    return $mode ? include($file) : file_get_contents($file);
}

/**
 * cache_write() 写入缓存  以return array();形式写入
 *
 * @param string $file
 * @param string $array
 *
 * @return boolean
 */
function cache_write($file, $array)
{
    if (is_array($array)) {
        $data = "<?php\n return " . var_export($array, true) . "\n?>";
        file_put_contents($file, $data);
        return true;
    } else {
        return false;
    }
}

/**
 * cache_delete() 删除缓存
 *
 * @param string $file
 *
 * @return boolean
 */
function cache_delete($file)
{
    if (file_exists($file)) {
        @unlink($file);
        return true;
    } else {
        return false;
    }
}

// ip库
function ip2addr($ip)
{
    //IP数据文件路径
    $dat_path = CACHE_DIR . 'QQWry.dat';

    //检查IP地址
    if (!preg_match("/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/", $ip)) {
        return 'IP Address Error';
    }
    //打开IP数据文件
    if (!$fd = @fopen($dat_path, 'rb')) {
        return 'IP date file not exists or access denied';
    }

    //分解IP进行运算，得出整形数
    $ip = explode('.', $ip);
    $ipNum = $ip [0] * 16777216 + $ip [1] * 65536 + $ip [2] * 256 + $ip [3];

    //获取IP数据索引开始和结束位置
    $DataBegin = fread($fd, 4);
    $DataEnd = fread($fd, 4);
    $ipbegin = implode('', unpack('L', $DataBegin));
    if ($ipbegin < 0)
        $ipbegin += pow(2, 32);
    $ipend = implode('', unpack('L', $DataEnd));
    if ($ipend < 0)
        $ipend += pow(2, 32);
    $ipAllNum = ($ipend - $ipbegin) / 7 + 1;

    $BeginNum = 0;
    $EndNum = $ipAllNum;

    $ip1num = 0;
    $ip2num = 0;
    $ipAddr1 = '';
    $ipAddr2 = '';
    //使用二分查找法从索引记录中搜索匹配的IP记录
    while ($ip1num > $ipNum || $ip2num < $ipNum) {
        $Middle = intval(($EndNum + $BeginNum) / 2);

        //偏移指针到索引位置读取4个字节
        fseek($fd, $ipbegin + 7 * $Middle);
        $ipData1 = fread($fd, 4);
        if (strlen($ipData1) < 4) {
            fclose($fd);
            return 'System Error';
        }
        //提取出来的数据转换成长整形，如果数据是负数则加上2的32次幂
        $ip1num = implode('', unpack('L', $ipData1));
        if ($ip1num < 0)
            $ip1num += pow(2, 32);

        //提取的长整型数大于我们IP地址则修改结束位置进行下一次循环
        if ($ip1num > $ipNum) {
            $EndNum = $Middle;
            continue;
        }

        //取完上一个索引后取下一个索引
        $DataSeek = fread($fd, 3);
        if (strlen($DataSeek) < 3) {
            fclose($fd);
            return 'System Error';
        }
        $DataSeek = implode('', unpack('L', $DataSeek . chr(0)));
        fseek($fd, $DataSeek);
        $ipData2 = fread($fd, 4);
        if (strlen($ipData2) < 4) {
            fclose($fd);
            return 'System Error';
        }
        $ip2num = implode('', unpack('L', $ipData2));
        if ($ip2num < 0)
            $ip2num += pow(2, 32);

        //没找到提示未知
        if ($ip2num < $ipNum) {
            if ($Middle == $BeginNum) {
                fclose($fd);
                return 'Unknown';
            }
            $BeginNum = $Middle;
        }
    }

    $ipFlag = fread($fd, 1);
    if ($ipFlag == chr(1)) {
        $ipSeek = fread($fd, 3);
        if (strlen($ipSeek) < 3) {
            fclose($fd);
            return 'System Error';
        }
        $ipSeek = implode('', unpack('L', $ipSeek . chr(0)));
        fseek($fd, $ipSeek);
        $ipFlag = fread($fd, 1);
    }

    if ($ipFlag == chr(2)) {
        $AddrSeek = fread($fd, 3);
        if (strlen($AddrSeek) < 3) {
            fclose($fd);
            return 'System Error';
        }
        $ipFlag = fread($fd, 1);
        if ($ipFlag == chr(2)) {
            $AddrSeek2 = fread($fd, 3);
            if (strlen($AddrSeek2) < 3) {
                fclose($fd);
                return 'System Error';
            }
            $AddrSeek2 = implode('', unpack('L', $AddrSeek2 . chr(0)));
            fseek($fd, $AddrSeek2);
        } else {
            fseek($fd, -1, SEEK_CUR);
        }

        while (($char = fread($fd, 1)) != chr(0))
            $ipAddr2 .= $char;

        $AddrSeek = implode('', unpack('L', $AddrSeek . chr(0)));
        fseek($fd, $AddrSeek);

        while (($char = fread($fd, 1)) != chr(0))
            $ipAddr1 .= $char;
    } else {
        fseek($fd, -1, SEEK_CUR);
        while (($char = fread($fd, 1)) != chr(0))
            $ipAddr1 .= $char;

        $ipFlag = fread($fd, 1);
        if ($ipFlag == chr(2)) {
            $AddrSeek2 = fread($fd, 3);
            if (strlen($AddrSeek2) < 3) {
                fclose($fd);
                return 'System Error';
            }
            $AddrSeek2 = implode('', unpack('L', $AddrSeek2 . chr(0)));
            fseek($fd, $AddrSeek2);
        } else {
            fseek($fd, -1, SEEK_CUR);
        }
        while (($char = fread($fd, 1)) != chr(0)) {
            $ipAddr2 .= $char;
        }
    }
    fclose($fd);

    //最后做相应的替换操作后返回结果
    if (preg_match('/http/i', $ipAddr2)) {
        $ipAddr2 = '';
    }
    $ipaddr = "$ipAddr1 $ipAddr2";
    $ipaddr = preg_replace('/CZ88.Net/is', '', $ipaddr);
    $ipaddr = preg_replace('/^s*/is', '', $ipaddr);
    $ipaddr = preg_replace('/s*$/is', '', $ipaddr);
    if (preg_match('/http/i', $ipaddr) || $ipaddr == '') {
        $ipaddr = 'Unknown';
    }

    //转成utf8
    return mb_convert_encoding($ipaddr, "utf-8", "gbk");
}


// 遍历所有目录文件
function my_scandir($path)
{
    system("find " . $path . " -type f | grep -v .svn > my_scandir.txt");
    $all = safe_file_get_contents("my_scandir.txt");
    cache_delete("my_scandir.txt");
    return explode("\n", $all);
}

/**
 * 获取GET/POST参数
 * @param $key
 * @param null $type
 * @param string $gp
 * @return int|string
 */
function request($key, $type = null, $gp = 'GP')
{
    $gp = strtoupper($gp);
    switch ($gp) {
        case 'G':
            $var = isset($_GET[$key]) ? $_GET[$key] : '';
            break;
        case 'P':
            $var = isset($_POST[$key]) ? $_POST[$key] : '';
            break;
        default:
            $var = isset($_GET[$key]) ? $_GET[$key] : (isset($_POST[$key]) ? $_POST[$key] : '');
    }
    if (null == $type) {
        return $var;
    }
    switch ($type) {
        case 'int':
            $var = intval($var);
            break;
        case 'str':
            if (!get_magic_quotes_gpc()) {
                $var = addslashes($var);
            }
            break;
        case 'arr': {
            foreach ($var as $key => $value) {
                $var[$key] = addslashes($value);
            }
            return $var;
        }
        default:
    }

    return trim($var);
}

/**
 * 去除xss
 * @param $string
 * @param bool $low
 * @return bool
 */
function clean_xss(&$string, $low = False)
{
    if (!is_array($string)) {
        $string = trim($string);
        $string = strip_tags($string);
        $string = htmlspecialchars($string);
        if ($low) {
            return True;
        }
        $string = str_replace(array('"', "\\", "'", "/", "..", "../", "./", "//"), '', $string);
        $no = '/%0[0-8bcef]/';
        $string = preg_replace($no, '', $string);
        $no = '/%1[0-9a-f]/';
        $string = preg_replace($no, '', $string);
        $no = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';
        $string = preg_replace($no, '', $string);
        return True;
    }
    $keys = array_keys($string);
    foreach ($keys as $key) {
        clean_xss($string [$key]);
    }
}

/**
 * 分页控制拦
 *
 * @param int $page 当前页
 * @param int $pagesize 每页记录数
 * @param int $count 记录总数
 * @param string $url 当前URL
 * @param string $orderby 排序字段
 * @param string $sort 排序方式
 * @return string
 */
function pagectrl($page = 1, $pagesize = 10, $count = 0, $url = '', $orderby = '', $sort = 'ASC')
{
    if (empty ($count) || empty ($pagesize)) {
        return '';
    } else {
        $pagecount = ceil($count / $pagesize);
    }

    $first = '首页';
    $prev = '上一页';
    $next = '下一页';
    $last = '末页';

    if (!empty ($orderby)) {
        $sort = in_array(strtoupper($sort), array('ASC', 'DESC')) ? $sort : 'ASC';
        $orderby = "&orderby=$orderby&sort=$sort";
    }

    $url = $url . (strstr($url, '?') ? '&' : '?');
    $ctrlbar = '';
    if ($pagecount > 1) {
        $ctrlbar = "共{$count}条记录，每页{$pagesize}条，当前第$page/{$pagecount}页 ";

        if ($page > 1) {
            $ctrlbar .= '<a href="' . $url . 'page=1' . $orderby . '">' . $first . '</a> ';
            $ctrlbar .= '<a href="' . $url . 'page=' . ($page - 1) . $orderby . '">' . $prev . '</a> ';
        }
        if ($pagecount < 7) {
            for ($pg = 1; $pg <= $pagecount; $pg++) {
                if ($page == $pg)
                    $ctrlbar .= '<span style="color:#FF6600;font-weight:bold;">' . $pg . '</span> ';
                else
                    $ctrlbar .= '<a href="' . $url . 'page=' . $pg . $orderby . '">' . $pg . '</a> ';
            }
        } else {
            if ($page < 6) {
                $pg = 1;
            } else {
                if ($pagecount - $page < 3)
                    $pg = $pagecount - 6;
                else
                    $pg = $page - 3;
            }
            for ($bg = $pg; $bg <= $pg + 6; $bg++) {
                if ($page == $bg)
                    $ctrlbar .= '<span style="color:#FF6600;font-weight:bold;">' . $bg . '</span> ';
                else
                    $ctrlbar .= '<a href="' . $url . 'page=' . $bg . $orderby . '">' . $bg . '</a> ';
            }
        }
        if ($page < $pagecount) {
            $ctrlbar .= '<a href="' . $url . 'page=' . ($page + 1) . $orderby . '">' . $next . '</a> ';
            $ctrlbar .= '<a href="' . $url . 'page=' . $pagecount . $orderby . '">' . $last . '</a> ';
        }
    }

    $option = '跳转<select onchange="location.href=\'' . $url . 'page=\'+this.value+\'' . $orderby . '\'">';
    for ($p = 1; $p <= $pagecount; $p++) {
        if ($p != $page)
            $option .= "<option value=\"$p\">$p/$pagecount</option>";
        else
            $option .= "<option value=\"$p\" selected=\"selected\">$p/$pagecount</option>";
    }
    $option .= '</select>';
    $ctrlbar .= $option;
    return $ctrlbar;
}

function get_ip()
{
    $cip = getenv('HTTP_CLIENT_IP');
    $xip = getenv('HTTP_X_FORWARDED_FOR');
    $rip = getenv('REMOTE_ADDR');
    $srip = $_SERVER ['REMOTE_ADDR'];
    if ($cip && strcasecmp($cip, 'unknown')) {
        $onlineip = $cip;
    } elseif ($xip && strcasecmp($xip, 'unknown')) {
        $onlineip = $xip;
    } elseif ($rip && strcasecmp($rip, 'unknown')) {
        $onlineip = $rip;
    } elseif ($srip && strcasecmp($srip, 'unknown')) {
        $onlineip = $srip;
    }
    preg_match("/[\d\.]{7,15}/", $onlineip, $match);
    return $match [0] ? $match [0] : 'unknown';
}

function getFileLines($filename, $startLine = 1, $count = 1000, $method = 'rb')
{
    $content = '';
    $line = count(file($filename));
    if ($startLine <= $line) {
        $fp = new SplFileObject($filename, $method);
        $fp->seek($startLine - 1); // 转到第N行, seek方法参数从0开始计数
        for ($i = 0; $i <= $count; ++$i) {
            $content .= $fp->current(); // current()获取当前行内容
            $fp->next(); // 下一行
        }
        $content .= '<br>';
        if ($line >= $startLine + $count) {
            $line = $startLine + $count;
        }
    }
    return array('line' => $line, 'content' => $content); // array_filter过滤：false,null,''
}

function get_file_for_one_line($filename, $startLine = 1,$method = 'rb')
{
    $data = array();
    $line = count(file($filename));
    $count = $line - $startLine;
    if ($startLine <= $line) {
        $fp = new SplFileObject($filename, $method);
        $fp->seek($startLine - 1); // 转到第N行, seek方法参数从0开始计数
        for ($i = 0; $i <= $count; ++$i) {
            $content = $fp->current(); // current()获取当前行内容
            array_push($data,$content);
            $fp->next(); // 下一行
        }
        if ($line >= $startLine + $count) {
            $line = $startLine + $count;
        }
    }
    return array('line' => $line, 'data' => $data); // array_filter过滤：false,null,''
}

function get_param_time($start_time,$end_time,$day_limit=1){

    if(empty($start_time)&&empty($end_time)){
        if($day_limit){
        $time = strtotime(date('Y-m-d'));
        $start_time = $time;
        $end_time = $time + $day_limit*86400;
        }else{

        }
    }else if($start_time&&empty($end_time)){
        $start_time = strtotime($start_time);
        $end_time = $start_time+ $day_limit*86400;
    }else if(empty($start_time)&&$end_time){
        $end_time = strtotime($end_time)+86400;
        $start_time = $end_time-$day_limit*86400;
    }else{
        $start_time = strtotime($start_time);
        $end_time = strtotime($end_time)+86400;
        if($start_time - $end_time>$day_limit*86400){
            $end_time = $start_time + $day_limit*86400;
        }
    }

    return  array($start_time,$end_time);
}

/**
 * 检查网络
 * @param $host
 * @param $port
 * @return array|bool
 */
function telnet($host,$port = 80)
{
    $fp = @fsockopen($host,$port,$errno,$errstr,1);
    if(!$fp) {
        return false;
    }
    @fclose($fp);

    return true;
}

//导出txt
function expTxt($list, $name = null, $typeList = array()){
    
    $line1 = array_keys($list[0]);
    $data .= implode("\t", $line1);
    $data .= "\r\n";
    foreach ($list as $key => $val) {
        $line = array();
        foreach ($val as $k => $v) {
            if(count($typeList)>0 && $typeList[$k] ){
                $line[] = "($v)".$typeList[$k][$v];
            }else{
                $line[] = $v;
            }
            
        }
        $data .= implode("\t", $line);
        $data .= "\r\n";
    }
    $name = $name ? $name : uniqid();
    header("content-type:application/octet-stream");
    header('Content-Disposition: attachment; filename=' . $name . '.txt');
    header("Content-Transfer-Encoding: binary");
    echo $data;
    exit;
}

/**
 * 导出csv
 * @param $list    array  导出data  
 * @param $name    string txt文件名字
 */
function expCsv($list, $name = null)
{
    ini_set('memory_limit', '2048M');
    $name = $name ? $name : uniqid();
    header("Content-type:application/vnd.ms-excel");
    header("Content-Disposition:filename=" . $name . ".csv");
    // 打开PHP文件句柄，php://output 表示直接输出到浏览器
    $fp = fopen('php://output', 'a');
    foreach (array_keys($list[0]) as $i => $v) {
        $headArr[$i] = iconv('utf-8', 'GB18030', $v);
    }
    //将标题名称通过fputcsv写到文件句柄
    fputcsv($fp, $headArr);
    foreach ($list as $item) {
        $rows = array();
        foreach ($item as $export_obj) {
            $rows[] = iconv('utf-8', 'GB18030', $export_obj);
        }
        fputcsv($fp, $rows);
    }
    ob_flush();
    flush();
    exit ();
}
