<?php
/**
 * $s = Ext_Template::getInstance();
 * $s是smarty的实例
 */

include("Classes/Smarty/Smarty.class.php");

!defined("DBEXPIRES")&&define("DBEXPIRES",3600);
!defined("DBISCACHE")&&define("DBISCACHE",0);

class Ext_Template extends Smarty {
	private static $instance = null;
	private static $lang_config = null;
	private static $use_lang = 'zh-cn';
	public static function getInstance(){
		$lang = 'zh-cn';
		self::$use_lang = isset($_SESSION['lang'])?$_SESSION['lang']:'zh-cn';
		if(!self::$instance) self::$instance = new self($lang);
		return self::$instance;
	}
	function __construct($lang){
//		parent::Smarty();
		parent::__construct();
		$this->compile_dir = LOG_DIR.'Templates_c/'.$lang.'/';
		//$this->config_dir = APP.'/Config';
		$this->cache_dir = LOG_DIR.'Cache/';
		$this->template_dir = TPL_DIR.$lang.'/';;
		$this->left_delimiter = "<{";
		$this->right_delimiter = "}>";
		$this->cache_lifetime = DBEXPIRES;
		$this->caching = DBISCACHE;
		$this->assign("PUBLICWEBROOT",PUBLICWEBROOT);
	}

	public static function lang($content){
		if(empty($content)){
			return $content;
		}elseif( !is_string($content) && empty($content['content']) ){
			if( is_array($content) ){
				foreach( $content as $k=>$v ){
					return $v;
				}
			}else{
				return $content;
			}
		}elseif( is_string($content) ){
			$content = array('content'=>$content);
		}
		if(!self::$lang_config){
			$lang_file = LIB_DIR.'Common/Language/language.php';
			self::$lang_config = include_once $lang_file;
		}
		return language::getLang($content['content'],self::$use_lang);
	}
}
