<?php
class Ext_Sqlite extends SQLite3{

	var $db_name; 
	var $error = "";
	
	private static $instance = array();
	

	public function __construct($db_name = "sqlite"){
		$this->db_name = $db_name;
		$this->open($this->db_name);
	}

	//构造函数

	public function __destruct(){
		$this->close();
	}

	/**
	 * 单例模式
	 * @param $name
	 */
	public static function getInstance($name=""){
		if(empty(self::$instance[$name])) {
			self::$instance[$name] = new self($name);
		}
		return self::$instance[$name];
	}

	//关闭数据库连接

	public function sqlite_close(){
		return $this->close();
	}

	//严重错误时停执行

	public function halt($msg){

		$this->close();

		//调试函数
		print("

		程序遇到一个严重而至命的错误，以至停止执行！

		错误描述：{$msg}

		");

		exit(1);
	}


	//执行对数据查询操作的SQL语句
	public function sqlite_query($sql){

		$result = $this->query($sql);
		return $result;

	}

	//执对数据库操作(create，update,delete,insert)的SQL语句
	public function sqlite_exec($sql){

		$result = $this->exec($sql);
		return $result;

	}


	//获取所有数据内容存入数组中
	public function sqlite_fetchrow($sql){

		$result = $this->query($sql);
		$row = array(); 
		while ($res = $result->fetchArray(SQLITE3_ASSOC)) {
	    	$row[] = $res;
		}
		return $row;

	}

	//获取最后增加记录的自动编号

	public function sqlite_insertid(){

		$insert_id = $this->lastInsertRowID($this->db_link);
		return $insert_id;
	}

	//执行除SELECT语句所影响的记录行数

	public function sqlite_changes(){

		$nums = $this->changes();
		return $nums;

	}



}

?>