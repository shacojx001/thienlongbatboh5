<?php
/**
 * memcache 使用单件模式
 * v1.2
 */

class Ext_Memcached {
	//单例
	private static $instance = array();
	//所有配置信息
	private static $dns = array();
	//mc连接
	private $connection;
	//获得当前配置信息
	private $config = array();

	private function __construct($name){
		//获取配置信息 
		if(!self::$dns){
			$conf = CONFIG_DIR . DIRECTORY_SEPARATOR ."memcached.php";
			if(!file_exists($conf)) die("配置文件Memcached.php不存在");
			self::$dns = include($conf);
		}
		$this->config = self::$dns[$name];
		if(!is_array($this->config)){
			die("Memcached.php没有{$name}配置项目");
		}

		//建立连接
		$this->connection = new Memcache();
		$this->connection->connect($this->config['host'],$this->config['port']);
	}

	/**
	 * 单例模式
	 * @param $name string
	 * @return Ext_Memcached
	 */
	public static function getInstance($name){
		if(empty(self::$instance[$name])) {
			self::$instance[$name] = new self($name);
		}
		return self::$instance[$name];
	}

	public function __destruct() {
		if ($this->connection) {
			$this->connection->close();
		}
	}

	/**
	 * 保存指定的KEY
	 * @param string $key
	 * @param array $data
	 * @param int $ttl
	 * @param boolean $compressed
	 */
	public function store($key, $data, $ttl = -1, $compressed = false) {
		if ( $ttl == -1) {
			$ttl = $this->config['ttl'];
			//设置最大缓存时间为2592000，超过则会出错
			if ($ttl > 2592000)
				$ttl = 2592000;
		}
		return $this->connection->set($key,$data, $compressed ,$ttl);
	}

	/**
	 * 获取指定key的信息
	 * @param string $key
	 */
	public function fetch($key) {
		return $this->connection->get($key);
	}

	/**
	 * 删除指定的KEY
	 * @param string $key
	 */
	public function delete($key) {
		$this->connection->delete($key);
	}

	/**
	 * 清理所有缓存
	 */
	public function flush()
	{
		return $this->connection->flush();
	}

	/**
	 * 获取当前mc连接
	 */
	public function getConn()
	{
		return $this->connection;
	}
}