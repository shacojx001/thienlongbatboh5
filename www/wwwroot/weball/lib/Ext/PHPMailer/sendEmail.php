<?php
/**
 * Created by PhpStorm.
 * User: Jc
 * Date: 2018/8/7
 * Time: 10:24
 */

class sendEmail{

	public function send($host,$port,$title,$content,$to_user,$from_user,$from_pw,$from_name,$SMTPSecure=''){
		header("content-type:text/html;charset=utf-8");
		ini_set("magic_quotes_runtime",0);
		require 'class.phpmailer.php';
		try {
			$mail = new PHPMailer(true);
			$mail->IsSMTP();
			$mail->CharSet    = 'UTF-8'; //设置邮件的字符编码，这很重要，不然中文乱码
			if(strtolower($SMTPSecure)=='ssl'){
				$mail->SMTPAuth   = true;                  //开启认证
			}else{
				$mail->SMTPAuth   = false;                  //开启认证
			}
			$mail->SMTPSecure = $SMTPSecure;//'ssl';
			$mail->Port       = $port;
			$mail->Host       = $host;
			$mail->Username   = $from_user;
			$mail->Password   = $from_pw;
			//$mail->IsSendmail(); //如果没有sendmail组件就注释掉，否则出现“Could  not execute: /var/qmail/bin/sendmail ”的错误提示
			$mail->AddReplyTo($from_user, $from_user);//回复地址
			$mail->From     = $from_user;
			$mail->FromName = $from_name;
			foreach($to_user as $val){
				$mail->AddAddress($val);
			}
			$mail->Subject  = $title;
			$mail->Body     = $content;
			$mail->AltBody  = "To view the message, please use an HTML compatible email viewer!"; //当邮件不支持html时备用显示，可以省略
			$mail->WordWrap = 80; // 设置每行字符串的长度
			//$mail->AddAttachment("f:/test.png");  //可以添加附件
			$mail->IsHTML(true);
			$ste = $mail->Send();
			if($ste){
				return array(true,'邮件已发送') ;
			}else{
				return array(false,'邮件发送失败') ;
			}
		} catch (phpmailerException $e) {
			return array(false,"邮件发送失败：".$e->errorMessage());
		}

	}

}