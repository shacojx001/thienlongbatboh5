<?php

/**
 * Class Ext_Mysql
 * 日期: 2017/11/24
 * author: wtf
 */
class Ext_Mysql{
	//配置信息
	private static $dns = array();
	//单例
	private static $instance = array();
	//获得当前配置信息
	private $config = array();
	//mc连接
	private $_conn = array();
	private $conn;
	//存放库操作后返回的资源句柄
	private $_tmpRes = null;
	
	
	private function __construct($name,$sdb){
        if($name == 'admin'){
            $db_file = 'db_login.php';
        }elseif($name == 'game'){
            $db_file = 'db_game.php';
        }elseif($name == 'allcenter'){
            $db_file = 'db_allcenter.php';
        }elseif($name == 'center'){       
            $db_file = 'db_admin.php';
            $name = $sdb;
        }elseif($name == 'kf'){
            $db_file = 'db_kf.php';
            $name = $sdb;
        }else{ 
            $db_file = 'db_admin.php';
            $name = $_SESSION['admin_db_link'];
        }

		//获取配置信息
		if(!isset(self::$dns[$db_file][$name])){
			$conf = CONFIG_DIR . DIRECTORY_SEPARATOR .$db_file;
			if(!file_exists($conf)){
				unset($_SESSION['admin_db_link']);
				Ext_Debug::log("配置文件$db_file 不存在",'error');
				return false;
				//die("配置文件$db_file 不存在");
			}
			self::$dns[$db_file] = include($conf);
		}
		$this->config = self::$dns[$db_file][$name];

		if(!is_array($this->config)){
			unset($_SESSION['admin_db_link']);
			Ext_Debug::log("$db_file 没有{$name}配置项目",'error');
			return false;
			//die("$db_file 没有{$name}配置项目");
		}
// print_r($this->config);exit;
		$this->conn = mysql_connect($this->config['host'].":".$this->config['port'],$this->config['user'],$this->config['pwd'],true);
		if(!$this->conn){
			Ext_Debug::log(mysql_error(),'error');
			return false;
		}
		if(!mysql_select_db($this->config['db'],$this->conn)){
			Ext_Debug::log(mysql_error(),'error');
			return false;
		}
		mysql_query("set names utf8");
	}

	public function __destruct()
	{
		if($this->conn) @mysql_close($this->conn);
	}

    /**
     * @param string $name
     * @param string $sdb
     * @return Ext_Mysql
     */
    public static function getInstance($name="",$sdb=""){
		if(empty(self::$instance[$name.$sdb])) {
			self::$instance[$name.$sdb] = new self($name,$sdb);
		}
		return self::$instance[$name.$sdb];
	}
	
	/**
	 * 获取连接
	 */
	public function getConn(){
		return $this->conn;
	}
	
	/**
	 * 执行指令
	 *
	 * @param string $sql
	 * @package string $dsn
	 */
	public function query($sql,$IgnoreFalse=FALSE){
		if(!$this->conn){
			Ext_Debug::log("数据库连接错误!",'error');
			return false;
		}
		$sTime = microtime(true);
		$result = mysql_query($sql,$this->conn);
		if(!$result && !$IgnoreFalse) {
			Ext_Debug::log("SQL：{$sql}(error)".mysql_error($this->conn).")",'error');
			return false;
			//die("SQL:{$sql} ".mysql_error($this->conn));
		}

		$eTime = microtime(true);
		$t = round($eTime-$sTime,3);
		Ext_Debug::log("SQL：{$sql}(OK:{$t})",'info');

		$this->_tmpRes = $result;
		return $result;
	}

	/**
	 * 执行指令(报错不打印记录)
	 *
	 * @param string $sql
	 * @package string $dsn
	 */
	public function query_error_no($sql,$IgnoreFalse=FALSE){
		$result = mysql_query($sql,$this->conn);
		
		$this->_tmpRes = $result;
		return $result;
	}
	
	/**
	 * 取一条记录
	 *
	 * @param string $sql
	 * @param string $cacheId
	 * @return array
	 */
	public function fetchOne($sql){
		$result = $this->query($sql);
		if(!$result) return array();
		return mysql_fetch_assoc($this->_tmpRes);
	}
	
	/**
	 * 取一条记录的一个字段
	 *
	 * @param string $sql
	 * @param string $cacheId
	 * @return array
	 */
	public function getOne($sql){
		$result = $this->query($sql);
		if(!$result){
			return '';
		}
		$row = array_values(mysql_fetch_assoc($this->_tmpRes));
		return $row[0];
	}
	
	/**
	 * 最后插入的ID
	 *
	 * @param string $sql
	 * @return array
	 */
	public function getLastInsertId(){
		if(!$this->conn){
			return 0;
		}
		return mysql_insert_id($this->conn);
	}
	
	/**
	 * 取记录 并根据自动hash结果集
	 *
	 * @param string $sql
	 * @param string $dsn 指定数据库，对于数据库配置的mark
	 * @param string $hashBy
	 * @return array
	 */
	public function fetchRow($sql,$hashBy=null){
		$return = array();
		$result = $this->query($sql);
		if(!$result){
			return array();
		}
		if(!$hashBy){
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[] = $res;
			}
		}else{
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[$res[$hashBy]] = $res;
			}
		}

		return $return;
	}
	
	public function getRes(){
		return $this->_tmpRes;
	}
	
	/**
	 * 影响记录数
	 *
	 * @param string $sql
	 * @return array
	 */
	public function getAffectRows(){
		if(!$this->conn) return 0;
		return mysql_affected_rows($this->conn);
	}
	
	public function getNumRows() {
		if(!$this->conn) return 0;
		return mysql_num_rows($this->_tmpRes);
	}

	/**
	 * 事务处理
	 * @param array $SqlList
	 * @return 1
	 */
    public function transaction($SqlList) {
		if(!$this->conn) return false;
        if(is_array($SqlList)){
			$sTime = microtime(true);
            $res = 1;
            mysql_query("BEGIN", $this->conn);
            foreach($SqlList as $sql){
                $result = mysql_query($sql, $this->conn);
                if($result === FALSE) {
                    $res = "SQL:{$sql} ".mysql_error($this->conn);
                    break;
                }
            }
            if($res == 1) {
				mysql_query("COMMIT", $this->conn);
				$eTime = microtime(true);
				$t = round($eTime-$sTime,3);
				Ext_Debug::log("SQL：{$sql}(OK:{$t})",'info');
			} else{
				mysql_query("ROLLBACK", $this->conn);
				Ext_Debug::log("SQL：{$sql}(error)" . mysql_error($this->conn) . ")", 'error');
			}
            return $res;
        }
        else die("操作错误");
    }


    public function insert($table,$arr, $replace=false){
		if(!$this->conn) return 0;
    	if (!empty($table) && !empty($arr) && is_array($arr)) {
			$fields = array();
			$values = array();
			foreach ($arr as $field=>$val){
				$fields[]="`$field`";
				$values[]="'$val'";
			}
			$fields = implode(',', $fields);
			$values = implode(',', $values);
			$sql = ($replace?'REPLACE':'INSERT').' INTO '.$table."($fields)VALUES($values);";
			if($this->query($sql)){
				return $this->getLastInsertId();
			}else{
				return 0;
			}
		}else{
			return 0;
		}
    }

	/**
	 * @param $table
	 * @param $arr
	 * @return bool|int|resource
	 */
	public function insertOrUpdate($table,$arr){
		if(!$this->conn) return 0;
		if (!empty($table) && !empty($arr) && is_array($arr)) {
			$fields = array();
			$values = array();
			$update = array();
			foreach ($arr as $field=>$val){
				$fields[]="`$field`";
				$values[]="'$val'";
				$update[] = "`$field`='$val'";
			}
			$fields = implode(',', $fields);
			$values = implode(',', $values);
			$update = implode(',',$update);
			$sql = 'INSERT INTO '.$table."($fields)VALUES($values) ON DUPLICATE KEY UPDATE $update";

			return $this->query($sql);
		}else{
			return 0;
		}
	}

	/**
	 * @param $table
	 * @param $where
	 * @param $data
	 * @return bool|resource
	 */
	public function update($table,$where,$data){
		if(!$this->conn) return false;
		if(!empty($where) && !empty($data) && is_array($where) &&is_array($data)){
			$fields = array();
			foreach ($data as $field=>$val){
				$fields[] =  "`$field`='$val'";
			}
			$fields = implode(',',$fields);
			$condition = array();
			foreach ($where as $field=>$val) {
				$condition[] = "`$field`='$val'";
			}
			$condition = ' WHERE '.implode(' and',$condition);
			$sql = "UPDATE `$table` SET ".$fields. $condition;
			return $this->query($sql);
		}
		return false;
	}


	public function fetchRow2($sql,$hashBy=null){
		$return = array();
		$result = $this->query2($sql);
		if(!$result){
			return array();
		}
		if(!$hashBy){
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[] = $res;
			}
		}else{
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[$res[$hashBy]] = $res;
			}
		}

		return $return;
	}

	public function query2($sql,$IgnoreFalse=FALSE){
		try{
			$result = mysql_query($sql,$this->conn);
			if(!$result){
				$err =  mysql_error($this->conn);
				file_put_contents('./chat_server_db.log',date('Y-m-d H:i:s').'|'.var_export($result,true).'|'.$err."\n",FILE_APPEND);
				self::reset_connect($err);
			}
		}catch(Exception $e){
			$msg = $e->getMessage();
			file_put_contents('./chat_server_db.log',date('Y-m-d H:i:s').'|'.$msg."\n");
			self::reset_connect($msg);
		}
		if(!$result && !$IgnoreFalse) {
			return false;
		}
		$this->_tmpRes = $result;
		return $result;
	}

	public static function reset_connect($err_msg){
		if(strpos($err_msg, 'MySQL server has gone away')!==false){
			self::$instance = null;
			return true;
		}
		return false;
	}

	public function get_error(){
		return mysql_error($this->conn);
	}
}