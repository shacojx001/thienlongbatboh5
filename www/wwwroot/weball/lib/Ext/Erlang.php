<?php
/**
 * PHP-Erlang 接口，需要先安装ped
 * @author 彭 qq:249828165
 */
/*
 * 使用：
   $erl = new Erlang();
   $erl -> connect('xge1@127.0.0.1','xge');
   $erl -> get('lib_mail', 'send', '[~s,~u]', array(array($msg, (int)$data['role_id'])));
   $erl -> close();
peb_encode:
format_string

    The format string is composed of one or more directives.

    	    ~a - an atom
    	    ~s - a string  字符串直接使用这个或 ~b都行
    	    ~b - a binary (contain 0x0 in string)
    	    ~i - an integer  使用数字类型记得要(int)$val, intval($val)
    	    ~l - a long integer
    	    ~u - an unsigned long integer
    	    ~f - a float
    	    ~d - a double float
    	    ~p - an erlang pid

data

    The data to send to Erlang node. Initial wrapped with an array, tuple and list data must be wrapped with extra dimension.

*/
class Ext_Erlang
{
	protected $link;
	public $error = array();
	public $conn_error = array();
	public $cfg = array();
	private $node = 0;
	private static $instance;
	
	private function __construct($isExit = true, $dbgame = false, $changedb = array())
	{
		if(empty($changedb)) // 如果不是切换数据请求
		{
			global $base_info;
			if(!$dbgame)
			{
				// 库表base_game中的配置
				if(!isset($base_info['erl_cookie']) || empty($base_info['erl_cookie']) || !isset($base_info['erl_node']) || empty($base_info['erl_node']))
				{
					alert('erlang配置出错');
				}
				$this->cfg = array(
					'cookie' => $base_info['erl_cookie'],
					'nodes'  => array(1 => $base_info['erl_node'])
				);
			}
			else
			{
				// 文件security.php中的配置
				$this->cfg = array(
					'cookie' => ERLANGCOOKIE,
					'nodes'  => array(1 => ERLANGNODE)
				);
			}
		}
		else // 如果是切换数据库请求
		{
			// 参数changedb中的配置
			$this->cfg = array(
				'cookie' => $changedb['erl_cookie'] ,
				'nodes'  => array(1 => $changedb['erl_node'])
			);
		}

		//避免重复读取，有时会读取失败，直接提示退出
		//if(!$_SESSION['admin_erlang_conn'])
		if(!is_resource($this->link))
		{
			// 连接节点
			$this->connect();
			
			//$zone = $this->get('svr_node', 'zone_list', '[~a]', array(array('admin'))); //[{zone,1,'xge1@192.168.5.144',{192,168,5,144},12345,1}]
			//print_r($zone);
            $zone = $this->tab2list("ets_node");
            if($this->getError() || peb_error() )
            {
				if($isExit)
				{
					exit("获得线路失败，请刷新页面重试！");
				}
				else
				{
					return false;
				}
			}

			if(is_array($zone))
			{
                ksort($zone);
                //print_r($this->cfg);
				$this->cfg['nodes'] = array(1 => ERLANGNODE);
                foreach($zone as $zval)
				{
					//if(!$zval[0])continue;
                    //$this->cfg['nodes'][$zval[0]] = $zval[1];
                    if($isExit === 99 && $zval[1] ==1)
                    {
                        $this->cfg['nodes'][$zval[1]] = $zval[4];
                    }
                    else 
                    {
					    if($zval[0]!='node' || !$zval[1] || !$zval[4] || $zval[1] < 10)
					    	continue;
                        $this->cfg['nodes'][$zval[1]] = $zval[4];
                    }
                }
				//print_r($this->cfg);
			}
			//$_SESSION['admin_erlang_conn'] = $this->cfg;
		}
		//else{
			//$this->cfg = $_SESSION['admin_erlang_conn'];
		//}
	}
	
	/**
	 * 单例模式
	 * @param $name
	 */
	public static function getInstance($isExit = true,$dbgame = false,$changedb = array(),$sid = ''){
		if($sid){
			if(!self::$instance[$sid]) {
				self::$instance[$sid] = new self($isExit,$dbgame,$changedb);
			}
			return self::$instance[$sid];
		}else{
			if(!self::$instance) {
				self::$instance = new self($isExit,$dbgame,$changedb);
			}
			return self::$instance;
		}
	}

	public function connect($node = 0, $cookie = '')
	{
		$this->close();
		$cfg = $this->cfg;
		if($node < 1)
		{
			$node = $cfg['nodes'][1];
		}
		else
		{
			$node = $cfg['nodes'][$node];
		}
        $this->node = $node = $node;
		if(!$cookie)
		{
			$cookie = $cfg['cookie'];
		}
		$this->link = peb_connect($node, $cookie, 5000);
		
		//echo $node."<br>";
		//echo $cookie."<br>";
		//echo $this->link."<br>";

		if(!$this->link)
		{
			$this->conn_error[$node] = 'could not connect['.peb_errorno().']:'  .  peb_error();
			$GLOBALS['erl_error'] .= "&nbsp;[$node]".$this->conn_error[$node];
		}
			
		return $this->link;
	}
	
	public function isActive(){
	}
	
	public function getError()
	{
		if($this->error)
		{
			$err = '';
			foreach($this->error as $node => $msg)
				$err .= "[{$node}]：{$msg}";
			
			
			return $err;
		}
		if(peb_error() == 'ei_connect error')
		{
			return '';
		}
		return peb_error();
	}
	
	public function getConnError()
	{
		if($this->conn_error)
		{
			$err = '';
			foreach($this->conn_error as $node => $msg)
				$err .= "[{$node}]：{$msg}";
				
			return $err;
		}
		return '';
	}
	
	private function rpc($M, $F, $A)
	{
		if(!is_resource($this->link))
		{
			$this->error[$this->node] = 'connect error!';
			return false;
		}
		try{
			$rpcRt = peb_rpc($M, $F, $A, $this->link);
			if(!$rpcRt)
				$rt = false;
			else{
				$rt = peb_decode($rpcRt);
			}
		}catch(Exception $e){
			$this->error[$this->node] .= $e -> getMessage();
			$rt = false;
		}
		return $rt;
	}

	public function newTable($name, $options)
	{
		$options = array_merge($options, array('named_table', 'public'));
		$x = peb_encode("[~a, [" . $this->repeatFormat('~a', $options) . "]]", array(array($name, $options)));
		return $this->rpc("ets", "new", $x);
	}

	/**
	 * 和erlang ets:insert使用一样
	 */
	public function insert($name, $key, $value)
	{
		$x = peb_encode("[~a, {~a, ~s}]", array(array($name, array($key, $value))));
		return $this->rpc("ets", "insert", $x);
	}

	public function bulkInsert($name, $values)
	{
		$x = peb_encode("[~a, [" . $this->repeatFormat('{~a, ~s}', count($values)) . "]]", array(array($name, $values)));
		return $this->rpc("ets", "insert", $x);
	}

	public function info($name, $item)
	{
		$x = peb_encode("[~a, ~a]", array(array($name, $item)));
		$rt = $this->rpc("ets", "info", $x);
		return isset($rt[1]) ? $rt : $rt[0];
	}

	public function delete_all_objects($name)
	{
		$x = peb_encode("[~a]", array(array($name)));
		return $this->rpc("ets", "delete_all_objects", $x);
	}

	/**
	 * 不建议使用，容易报错
	 * @param $name 表名
	 */
	public function tab2list($name)
	{
		$x = peb_encode("[~a]", array(array($name)));
		$rt = $this->rpc("ets", "tab2list", $x);
		return isset($rt[1]) ? $rt : $rt[0];
	}
	
	/**
	 * 调用某个模块的返回值,erlang端注意返回数字时要转换成list才能解包
	 * @param $model 如模块名：ets
	 * @param $func  如函数名：select
	 * @param $argk  peb_encode参数
	 * @param $argv  peb_encode参数值
	 */
	public function get($model, $func, $argk='[]', $argv=array())
	{
		$x = peb_encode($argk, $argv);
		$rt = $this->rpc($model, $func, $x);

		return isset($rt[1]) ? $rt : $rt[0];
	}
	
	/**
	 * 给所有线路发信息 循环调用get
	 * @return array(线路ID=>结果)
	 */
	public function getAll($model, $func, $argk='[]', $argv=array())
	{
		$rt = array();
//		foreach($this->cfg['nodes'] as $k => $v)
//		{
//			if($this->connect($k))
//			{
//				$rt[$v] = $this->get($model, $func, $argk, $argv);
//			}
//		}
		foreach($this->cfg['nodes'] as $k => $v)
		{
            $connect_result = $this->connect($k);
            $connect_error = peb_error();
			if($connect_result && !$connect_error)
			{
				//$rs = $this->get('lib_admin', 'rpc_all', "[~a, ~a, {$argk}]", array(array($model, $func, $argv)));
				//print_r($k);echo "  -----  ";
				$rs = $this->get($model, $func, $argk, $argv);
				/*if(is_array($rs))
				{
					foreach ($rs as $val)
					{
						$node = $val[0] == 0 ? $k : $val[0];
						$rt[$node] = $val[1];
					}
				}else{
					$rt = array();
				}*/
				$rt[$k] = $rs;
				//break;
			}
//            $rt[$k]['debug'] =  var_export($connect_result, true); // TEST.
//            $rt[$k]['debug2'] =  var_export($connect_error, true);
		}
		//d($rt);
		return $rt;
	}

	public function repeatFormat($format, $len)
	{
		if(is_array($len)){
			$len = count($len);
		}
		if($len < 1)return '';
		return implode(', ', array_fill(0, $len, $format));
	}
	
	public function close()
	{
		if(is_resource($this->link)) return peb_close($this->link);
	}
	
	public function __destruct()
	{
		return $this->close();
	}
}

