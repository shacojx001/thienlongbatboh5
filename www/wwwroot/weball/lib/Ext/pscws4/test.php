<?php
//test.php
//
// Usage on command-line: php test.php <file|textstring>
// Usage on web: 
error_reporting(E_ALL);

$text = "作为武侠小说大家,这个身份或许是他享誉最盛的。而另一个在金庸一生中不可磨灭的标签,便是报人,一个杰出的报人。

　　1947年,他进入上海《大公报》,从三千名投考者脱颖而出。第二年,《大公报》香港版创刊,金庸被派入香港,那年他24岁。当时的香港与上海相比,并不发达,但金庸说,“我一生很喜欢冒险,过一点新奇的生活。”

　　作家李敖曾在节目中批评金庸武侠小说“那写得什么玩意”,他说侠义部分金庸自己没一样做得到,“不讲真话、不做真事”。不过,作为报人金庸,他似乎并不是李敖所说的那般。

　　1959年,35岁的金庸创办《明报》。他说：“我办《明报》的时候,就是希望能够主持公正,把事实真相告诉给读者。”

　　他的社评文章,高峰期每日一篇,他的武侠小说,几乎也是以日更的节奏推进,数十年间无间断。

　　在这般工作状态下,还有个颇有趣的故事。当年《天龙八部》在《明报》连载时,金庸曾数次离港外游。小说连载不能断,他便请好友倪匡代笔。在小说第89回中,阿紫的双眼被丁春秋戳瞎,这个情节其实是倪匡写的。后来,金庸则以换眼治疗手段让阿紫复明了。";

// 
require 'pscws4.class.php';
$cws = new PSCWS4('utf8');
$cws->set_dict('etc/dict.utf8.xdb');
$cws->set_rule('etc/rules.ini');
 //是否复式分割,如“中国人”返回“中国＋人＋中国人”三个词。
$cws->set_multi(true);
//分词前去掉标点符号
$cws->set_ignore(true);

$cws->set_debug(true);
//设定将文字自动以二字分词法聚合
$cws->set_duality(true);
//要进行分词的语句
$cws->send_text($text);


$ret = array();
$ret = $cws->get_tops(50);
$cws->close();
print_r($ret);exit;


?>