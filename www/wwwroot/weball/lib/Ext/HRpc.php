<?php
/**
 * Class Ext_Hprose
 * 日期:2018年05月11日
 * author: wtf
 */
use Hprose\Client;
use Hprose\Http\Server;
class Ext_HRpc
{
    /**
     * client
     * @var
     */
    private static $cli_instance;

    /**
     * server
     * @var
     */
    private static $ser_instance;

    /**
     * 获取客户端PRC实例
     * @param $api
     * @return null
     * @throws Exception
     */
    public static function get_cli_instance($api){
        if(!$api){
            return null;
        }
        $key = md5($api);
        if (!isset(self::$cli_instance[$key])) {
            set_include_path( PATH_SEPARATOR . ROOT_DIR.'lib/Classes/Hprose' . PATH_SEPARATOR . get_include_path());
            require_once "Hprose.php";
            $client = Client::create($api, false);
            self::$cli_instance[$key] = $client;
        }

        return self::$cli_instance[$key];
    }

    /**
     * 获取服务端PRC实例
     * @return Server
     */
    public static function get_ser_instance(){
        if (!isset(self::$ser_instance)) {
            set_include_path( PATH_SEPARATOR . ROOT_DIR.'lib/Classes/Hprose' . PATH_SEPARATOR . get_include_path());
            require_once "Hprose.php";
            $server = new Server();
            self::$ser_instance = $server;
        }

        return self::$ser_instance;
    }

}