<?php
/**
 *
 * @author 逍遥重生
 * @package Core.DB 读写分离
 * @version 1.0 beat
 * @copyright 2008-5-8
 */

class Ext_Mysql_Ms{
	/**
	 * 单例模式
	 *
	 * @var resource
	 */
	private static $instance = null;
	
	/**
	 * 存放数据库单例的静态数组
	 *
	 * @var array
	 */
	private static $sqlsingle = array();

	/**
	 * 存放从库的ID,防止多次select的时候浪费联库资源 
	 *
	 * @var id
	 */
	private static $slaveId = null;
	/**
	 * 数据库执行记录
	 *
	 * @var array
	 */
	public static $querys = array("times"=>0,"sqls"=>array());
	
	/**
	 * 存放dsn的个数
	 *
	 * @var int
	 */
	private static $_dsns = 0;
	
	/**
	 * 存放dsn的数组
	 *
	 * @var array
	 */
	private static $_dsnc = array();
	
	/**
	 * 存放库操作后返回的资源句柄
	 *
	 * @var resource
	 */
	private $_tmpRes = null;
	
	/**
	 * 存放库操作后返回的句柄
	 *
	 * @var resource
	 */
	private $_tmpConn = null;
	/**
	 * 获取单例
	 *
	 * @return resource
	 */
	public static function getInstance(){
		if(!self::$instance){
			self::$instance = new self();
			if(!self::$_dsns){
				$conf = CONFIG_DIR . DIRECTORY_SEPARATOR . "db.php";
				if(!file_exists($conf)) throw new Mysql_Exception("配置文件db.php不存在");
				$dsn = include($conf);
				self::$_dsns = count($dsn);
				self::$_dsnc = $dsn;
			}
		}
		return self::$instance;
	}
	
	/**
	 * 构造函数 保留
	 *
	 */
	private function __construct(){
	}
	
	/**
	 * 取记录 并根据自动hash结果集
	 *
	 * @param string $sql
	 * @param string $dsn 指定数据库，对于数据库配置的mark
	 * @param string $hashBy
	 * @return array
	 */
	public function fetchRow($sql,$dsn=null,$hashBy=null){
		$return = array();
		$this->query($sql,$dsn);
		if(!$hashBy){
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[] = $res;
			}
		}else{
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[$res[$hashBy]] = $res;
			}
		}
		return $return;
	}
	
	public function fetchByField($sql,$field,$dsn=null){
		$return = array();
		$this->query($sql,$dsn);
		while ($res = mysql_fetch_assoc($this->_tmpRes)) {
			$return[] = $res[$field];
		}
		return $return;
	}
	
	public function getRes(){
		return $this->_tmpRes;
	}
	
	public function getConn(){
		return $this->_tmpConn;
	}
	
	/**
	 * 取记录 并根据by分组
	 *
	 * @param string $sql
	 * @param string $dsn 指定数据库，对于数据库配置的mark
	 * @param string $by
	 * @return string
	 */
	public function fetchGroupBy($sql,$dsn=null,$groupBy,$hashBy=null){
		$return = array();
		$this->query($sql,$dsn);
		if(!$hashBy){
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[$res[$groupBy]][] = $res;
			}
		}else{
			while ($res = mysql_fetch_assoc($this->_tmpRes)) {
				$return[$res[$groupBy]][$res[$hashBy]] = $res;
			}
		}
		
		return $return;
	}
	
	/**
	 * 取一条记录
	 *
	 * @param string $sql
	 * @param string $dsn 指定数据库，对于数据库配置的mark
	 * @param string $cacheId
	 * @return array
	 */
	public function fetchOne($sql,$dsn=null){
		$this->query($sql,$dsn);
		return mysql_fetch_assoc($this->_tmpRes);
	}
	
	/**
	 * 取一条记录
	 *
	 * @param string $sql
	 * @param string $dsn 指定数据库，对于数据库配置的mark
	 * @param string $cacheId
	 * @return array
	 */
	public function fetchNums($sql,$dsn=null){
		$this->query($sql,$dsn);
		return mysql_num_rows($this->_tmpRes);
	}
	
	/**
	 * 执行指令
	 *
	 * @param string $sql
	 * @package string $dsn
	 */
	public function query($sql,$dsn=null){
		self::$querys['times']++;
		if(!$dsn){
			$dsn = $this->_checkSql($sql);
		}
		$conn = $this->_getHelper($dsn);
		self::$querys['sqls'][] = array("sql"=>$sql,"type"=>$conn[1]);
		$result = mysql_query($sql,$conn[0]);
		if(!$result) throw new Mysql_Exception("SQL:{$sql} ".mysql_error());
		$this->_tmpRes = $result;
		$this->_tmpConn = $conn[0];
	}
	
	/**
	 * 分析SQL语句，自动分配到主库，从库
	 *
	 * @param string $sql
	 * @return array
	 */
	private function _checkSql($sql){
		$n = 0;
		if(self::$_dsns>1){//主从不一样
			$type = strtolower(substr(trim($sql),0,6));
			if ($type === 'select') {//读从库
				if(self::$slaveId){//如果存slave ID 那么就不再让机选库了，而去选择现成的，以免浪费资源
					$n = self::$slaveId;
				}else{
					if(self::$_dsns===2) $n = 1;
					else{//随即取一个从库
						$n = self::$slaveId = mt_rand(1,self::$_dsns-1);
					}
				}
			}
			
		}
		return array($n,self::$_dsnc[$n]['mark']);
	}
	
	/**
	 * 主从库助手函数
	 *
	 * @param array $dsn
	 * @return resource
	 */
	private function _getHelper($dsn){
		$name = '';
		${0} = is_array($dsn);
		if(${0}) $name=$dsn[1];
		else $name=$dsn; 
		if(!isset(self::$sqlsingle[$name])) {
			$n = -1;
			if(${0}){
				$n = $dsn[0];
			}else{
				foreach (self::$_dsnc as $k=>&$v){
					if ($v['mark']==$dsn) {
						$n = $k;
					}
				}
				if($n==-1) throw new Mysql_Exception("Db.php没有找到关于{$dsn}的配置信息");
			}
			self::$sqlsingle[$name] = new Helper(self::$_dsnc[$n]);
		}
		unset(${0});
		return array(self::$sqlsingle[$name]->getConn(),$name);
	}
	
	/**
	 * 执行存储过程 如果有out变量，自动查询出来
	 *
	 * @param string $sql
	 * @return array
	 */
	public function storePros($sql){
		$this->query($sql);
		preg_match_all('/,(@\w+)/', $sql,$matches);
		if(isset($matches[1])){
			return $this->fetchResult("select ".implode(",",$matches[1]));
		}
		return true;
	}
	
	/**
	 * 影响记录数
	 *
	 * @param string $sql
	 * @return array
	 */
	public function getAffectRows(){
		return mysql_affected_rows($this->_tmpConn);
	}
	
	public function getNumRows() {
		return mysql_num_rows($this->_tmpRes);
	}
	
	/**
	 * 最后插入的ID
	 *
	 * @param string $sql
	 * @return array
	 */
	public function getLastInsertId(){
		return mysql_insert_id($this->_tmpConn);
	}
}

class Helper{
	private $conn = null;
	public function __construct(&$dsn){
		//d($dsn['host'].":".$dsn['port'].$dsn['user'].$dsn['pwd']);
		$this->conn = mysql_connect($dsn['host'].":".$dsn['port'],$dsn['user'],$dsn['pwd']);
		mysql_select_db($dsn['db'],$this->conn) or die(mysql_error());
	}
	public function getConn(){
		mysql_query("set names utf8");
		return $this->conn;
	}
}

class Mysql_Exception extends Exception{
}