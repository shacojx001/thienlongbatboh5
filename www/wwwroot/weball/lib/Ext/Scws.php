<?php
/**
 * 分词库
 */

include('pscws4/pscws4.class.php');

class Ext_Scws{
	//配置信息
	private $cws;

	public function get_tops($text){
		//要进行分词的语句
		
		$this->cws->send_text($text);
			
		$ret = $this->cws->get_tops(50);
		
		return $ret;
	}
	public function __destruct()
	{
		$this->cws->close();
	}
	function __construct(){
		$this->cws = new PSCWS4('utf8');
		$this->cws->set_dict('pscws4/etc/dict.utf8.xdb');
		$this->cws->set_rule('pscws4/etc/rules.ini');

		$this->cws->set_charset('utf-8');
		 //是否复式分割,如“中国人”返回“中国＋人＋中国人”三个词。
		$this->cws->set_multi(true);
		//分词前去掉标点符号
		$this->cws->set_ignore(true);

		// $this->cws->set_debug(true);
		//设定将文字自动以二字分词法聚合
		$this->cws->set_duality(true);
	}
}
