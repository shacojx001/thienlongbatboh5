<?php

/**
 * Class Ext_Mysqli
 * 日期: 2017/11/24
 * author: wtf
 */
class Ext_Mysqli
{
    /**
     * 每个库对应一个实例
     * @var array
     */
    private static $instance = array();
    /**
     * 所有配置信息 属于类
     * @var array
     */
    private static $config = array();
    /**
     * 当前配置信息 属于对象
     * @var array
     */
    private $current_config = array();
    /**
     * 当前连接 属于对象
     * @var null
     */
    private $link = null;
    /**
     * 存放库操作后返回的资源句柄
     * @var null
     */
    private $_tmpRes = null;

    /**
     * Ext_Mysqli constructor.
     * @param $config
     */
    private function __construct($config)
    {
        $this->current_config = $config;
    }

    /**
     * connect
     * @return bool
     */
    private function connect()
    {
        $config = $this->current_config;
        if (!is_array($config) || empty($config)) {
            Ext_Debug::log('Ext_Mysqli: Db config error ', 'error');
            return false;
        }
        $this->link = @mysqli_connect($config['host'], $config['user'], $config['pwd'], '', $config['port']);
        if (!$this->link) {
            Ext_Debug::log('Ext_Mysqli: errno ' . mysqli_connect_errno() . 'errmsg ' . mysqli_connect_error(), 'error');
            return false;
        }
        if (!mysqli_select_db($this->link, $config['db'])) {
            Ext_Debug::log('Ext_Mysqli: errno ' . mysqli_errno($this->link) . 'errmsg ' . mysqli_error($this->link), 'error');
            mysqli_close($this->link);
            return false;
        }
        mysqli_query($this->link, "set names utf8");

        return true;
    }

    /**
     * @param string $name
     * @param bool $force
     * @param bool $conf
     * @return Ext_Mysqli
     */
    public static function getInstance($name = '', $force = false, $conf = array())
    {
        if (empty(self::$config)) {
            if(is_file(CONFIG_DIR . 'db_conf.php')){
                $config = include CONFIG_DIR . 'db_conf.php';
                if(!empty($config)){
                    self::$config = array_merge(self::$config,$config);
                }
            }
            if(is_file(CONFIG_DIR . 'db_conf_game.php')){
                $config_game = include CONFIG_DIR . 'db_conf_game.php';
                if(!empty($config_game)){
                    self::$config = array_merge(self::$config,$config_game);
                }
            }
            if(is_file(CONFIG_DIR . 'db_conf_kf.php')){
                $config_kf = include CONFIG_DIR . 'db_conf_kf.php';
                if(!empty($config_kf)){
                    self::$config = array_merge(self::$config,$config_kf);
                }
            }
        }
        if (!isset(self::$config[$name])) {
            if(!$conf[$name]){
                Ext_Debug::log('Ext_Mysqli: can not match config [' . $name . ']', 'error');
                exit('Sorry! Ext_Mysqli can not match config of ' . $name );
            }
            self::$config = array_merge(self::$config,$conf);
        }
        if (false != $force || !isset(self::$instance[$name])) {
            $conf = self::$config[$name];
            self::$instance[$name] = new self($conf);
        }

        return self::$instance[$name];
    }

    /**
     * @param $sql
     * @param bool $IgnoreFalse
     * @return bool|mysqli_result
     */
    public function query($sql, $IgnoreFalse = false)
    {
        if (!$this->link) {
            $this->connect();
        }
        try {
            $sTime = microtime(true);   
            $result = mysqli_query($this->link, $sql);  
            if (!$result && !$IgnoreFalse) {
                Ext_Debug::log("SQL：{$sql}(error)" . mysqli_error($this->link) . ")", 'error');
                return false;
            }
            $eTime = microtime(true);
            $t = round($eTime - $sTime, 3);
            Ext_Debug::log("SQL：{$sql}(OK:{$t})", 'info');
            $this->_tmpRes = $result;

            return $result;

        } catch (Exception $e) {
            $msg = $e->getMessage();
            Ext_Debug::log("Ext_Mysqli: $msg", 'error');
            return false;
        }
    }

    /**
     * 取一条记录
     * @param $sql
     * @return array|null
     */
    public function fetchOne($sql)
    {
        $result = $this->query($sql);
        if (!$result) return array();
        return mysqli_fetch_assoc($this->_tmpRes);
    }

    /**
     * 取记录 并根据自动hash结果集
     * @param $sql
     * @param null $hashBy
     * @return array
     */
    public function fetchRow($sql, $hashBy = null)
    {
        $return = array();
        $result = $this->query($sql);
        if (!$result) {
            return array();
        }
        if (!$hashBy) {
            while ($res = mysqli_fetch_assoc($this->_tmpRes)) {
                $return[] = $res;
            }
        } else {
            while ($res = mysqli_fetch_assoc($this->_tmpRes)) {
                $return[$res[$hashBy]] = $res;
            }
        }

        return $return;
    }

    /**
     * 事务处理
     * @param array $SqlList
     * @return 1
     */
    public function transaction($SqlList)
    {
        if (!$this->link) return false;
        if (is_array($SqlList)) {
            $sTime = microtime(true);
            $res = 1;
            mysqli_query($this->link, "BEGIN");
            foreach ($SqlList as $sql) {
                $result = mysqli_query($this->link, $sql);
                if ($result === FALSE) {
                    $res = "SQL:{$sql} " . mysqli_error($this->link);
                    break;
                }
            }
            if ($res == 1) {
                mysqli_query($this->link, "COMMIT");
                $eTime = microtime(true);
                $t = round($eTime - $sTime, 3);
                Ext_Debug::log("SQL：{$sql}(OK:{$t})", 'info');
            } else {
                mysqli_query($this->link, "ROLLBACK");
                Ext_Debug::log("SQL：{$sql}(error)" . mysqli_error($this->link) . ")", 'error');
            }
            return $res;
        } else {
            Ext_Debug::log("Ext_Mysqli: transaction sql list error", 'error');
            return false;
        }
    }

    /**
     * @param $table
     * @param $arr
     * @param bool $replace
     * @return int|string
     */
    public function insert($table, $arr, $replace = false)
    {
        if (!empty($table) && !empty($arr) && is_array($arr)) {
            $fields = array();
            $values = array();
            foreach ($arr as $field => $val) {
                $fields[] = "`$field`";
                $values[] = "'$val'";
            }
            $fields = implode(',', $fields);
            $values = implode(',', $values);
            $sql = ($replace ? 'REPLACE' : 'INSERT') . ' INTO ' . $table . "($fields)VALUES($values);";
            if ($this->query($sql)) {
                return $this->getLastInsertId();
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * @param $table
     * @param $arr
     * @return bool|int|resource
     */
    public function insertOrUpdate($table, $arr)
    {
        if (!empty($table) && !empty($arr) && is_array($arr)) {
            $fields = array();
            $values = array();
            $update = array();
            foreach ($arr as $field => $val) {
                $fields[] = "`$field`";
                $values[] = "'$val'";
                $update[] = "`$field`='$val'";
            }
            $fields = implode(',', $fields);
            $values = implode(',', $values);
            $update = implode(',', $update);
            $sql = 'INSERT INTO ' . $table . "($fields)VALUES($values) ON DUPLICATE KEY UPDATE $update";

            return $this->query($sql);
        } else {
            return 0;
        }
    }

    /**
     * @param $table
     * @param $where
     * @param $data
     * @return bool|resource
     */
    public function update($table, $where, $data)
    {
        if (!empty($where) && !empty($data) && is_array($where) && is_array($data)) {
            $fields = array();
            foreach ($data as $field => $val) {
                $fields[] = "`$field`='$val'";
            }
            $fields = implode(',', $fields);
            $condition = array();
            foreach ($where as $field => $val) {
                $condition[] = "`$field`='$val'";
            }
            $condition = ' WHERE ' . implode(' and', $condition);
            $sql = "UPDATE `$table` SET " . $fields . $condition;
            return $this->query($sql);
        }
        return false;
    }

    /**
     * @return int
     */
    public function getAffectRows()
    {
        if (!$this->link) return 0;
        return mysqli_affected_rows($this->link);
    }

    /**
     * @return int
     */
    public function getNumRows()
    {
        if (!$this->link) return 0;
        return mysqli_num_rows($this->_tmpRes);
    }

    /**
     * last insert id
     * @return int|string
     */
    public function getLastInsertId()
    {
        if (!$this->link) {
            return 0;
        }
        return mysqli_insert_id($this->link);
    }

    /**
     * @return null
     */
    public function getConn()
    {
        return $this->link;
    }

    /**
     *
     */
    public function __destruct()
    {
        if ($this->link) @mysqli_close($this->link);
    }

}