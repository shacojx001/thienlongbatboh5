<?php

/**
 * 数据库操作封装
 * @author lxr lxrmido@lxrmido.com
 */
class DB{
    /**
     * 返回对某SQL语句循环mysql_fetch_assoc后的结果
     * @param  string  $rs SQL语句
     * @return array()
     */
    public static function all($rs){
        return DB::fetch_all(DB::query($rs));
    }
    /**
     * 以$index字段为索引返回对某SQL语句循环mysql_fetch_assoc后的结果
     * @param  string $rs    SQL语句
     * @param  string $index 索引字段
     * @return array()
     */
    public static function map($rs, $index = 'id'){
        return DB::fetch_all_map(DB::query($rs), $index);
    }
    public static function map_one($rs, $index, $valkey){
        return DB::fetch_all_map_one(DB::query($rs), $index, $valkey);
    }
    /**
     * 返回对某SQL语句循环mysql_fetch_row后的结果
     * param  string  $rs SQL语句
     * return array()
     */
    public static function all_row($rs){
        return DB::fetch_all_row(DB::query($rs));
    }
    /**
     * 对某SQL语句的结果取每行的第一列拼合为一个数组返回
     * @param  string $rs SQL
     * @return array()     
     */
    public static function all_one($rs){
        return DB::fetch_all_one(DB::query($rs));
    }
    /**
     * 返回对某SQL语句mysql_fetch_assoc后的结果
     * @param  string  $rs SQL语句
     * @return array()
     */
    public static function assoc($rs){
        return mysql_fetch_assoc(DB::query($rs));
    }
    /**
     * 返回对某SQL语句mysql_fetch_row后的结果
     * @param  string  $rs SQL语句
     * @return array()
     */
    public static function row($rs){
        return mysql_fetch_row(DB::query($rs));
    }
    /**
     * 返回对某SQL语句mysql_fetch_row后的结果的第一列
     * @param  string  $rs SQL语句
     * @return any
     */
    public static function one($rs){
        $r = mysql_fetch_row(DB::query($rs));
        return isset($r[0]) ? $r[0] : null;
    }
    /**
     * array('a', 'b', 'c') -> "`a`,`b`,`c`"
     * @param  array() $keys
     * @return string
     */
    public static function group_up_keys($keys){
        $n = count($keys);
        if($n < 1){
            return '';
        }
        $str = "`{$keys[0]}`";
        for($i = 1; $i < $n; $i ++){
            $str .= ",`{$keys[$i]}`";
        }
        return $str;
    }
    /**
     * array('a', 'b', 'c') -> "'a','b','c'"
     * @param  array() $vals
     * @return string
     */
    public static function group_up_vals($vals){
        $n = count($vals);
        if($n < 1){
            return '';
        }
        $str = "'{$vals[0]}'";
        for($i = 1; $i < $n; $i ++){
            $str .= ",'{$vals[$i]}'";
        }
        return $str;
    }
    /**
     * array('a', 'b', 'c') -> "a|b|c"，相当于implode('|', array(...))
     * @param  array() $ors
     * @return string
     */
    public static function group_up_ors($ors){
        $n = count($ors);
        if($n < 1){
            return '';
        }
        $str = "{$ors[0]}";
        for($i = 1; $i < $n; $i ++){
            $str .= "|{$ors[$i]}";
        }
        return $str;
    }


    public static $connection;
    public static function instance($sql){
        // if(!DB::$connection){
            //DB::$connection = Ext_Mysql::getInstance("game");

        if(strpos($sql, '`base_log`')){//数据中心

           DB::$connection = Ext_Mysql::getInstance('allcenter');
        
        }else{//游戏服
        
           DB::$connection = Ext_Mysql::getInstance('center',$_SESSION['admin_db_link']);
        
        }   
            
            
        // }
        return DB::$connection;
    }
    public static function query($sql){
        // if(!DB::$connection){
            DB::instance($sql);
        // }
	
        $sTime = microtime(true);
        if($r = mysql_query($sql)){
            $eTime = microtime(true);
            $t = round($eTime-$sTime,3);
            Ext_Debug::log("SQL：{$sql}(OK:{$t})",'info');
            return $r;
        }else{
            Ext_Debug::log("SQL：{$sql}(error)".mysql_error(DB::$connection).")",'error');
            die("SQL:{$sql} ".mysql_error());
        }
    }
    public static function id($id = 'id'){
        $rs = DB::query('SELECT LAST_INSERT_ID()');
        if($r = mysql_fetch_row($rs)){
            return $r[0];
        }
        die('数据库插入ID查询失败');
    }
    public static function columns($db, $table){
        return DB::all_one(
            "SELECT `column_name`
             FROM `information_schema`.`columns` 
             WHERE table_schema='$db' 
             AND table_name='$table'"
        );
    }
    public static function columns_info($db, $table){
        return DB::all(
            "SELECT *
             FROM `information_schema`.`columns` 
             WHERE table_schema='$db' 
             AND table_name='$table'"
        );
    }
    public static function fetch_all($rs){
        $a = array();
        while($r = mysql_fetch_assoc($rs)){
            $a[] = $r;
        }
        return $a;
    }
    public static function fetch_all_map($rs, $index){
        $a = array();
        while($r = mysql_fetch_assoc($rs)){
            $a[$r[$index]] = $r;
        }
        return $a;
    }
    public static function fetch_all_map_one($rs, $index, $valkey){
        $a = array();
        while($r = mysql_fetch_assoc($rs)){
            $a[$r[$index]] = $r[$valkey];
        }
        return $a;
    }
    public static function fetch_all_row($rs){
        $a = array();
        while($r = mysql_fetch_row($rs)){
            $a[] = $r;
        }
        return $a;
    }
    public static function fetch_all_one($rs){
        $a = array();
        while($r = mysql_fetch_row($rs)){
            if(!empty($r[0])){
                $a[] = $r[0];
            }
        }
        return $a;
    }
}