<?php
/**
 * 数据模型.
 * User: fanweiting
 * Date: 2017/8/31
 * Time: 14:07
 */
Class Model{
    /**
     * @param $sid
     * @return array
     */
    public static function get_server_info_by($sid){
        $sql = "select * from adminserverlist where id = {$sid}";
        return Config::admindb()->fetchOne($sql);
    }

    /**
     * @param string $where
     * @return array
     */
    public static function get_server_power($where = '1'){
        $sql = "select * center_server_power where {$where} order by `value` desc";
        return Config::admindb()->fetchRow($sql,'sid');
    }

    /**
     * @param $gid
     * @return array
     */
    public static function get_server_list($field = '*',$where = '1',$orderBy = 'id desc'){
        $sql = "select {$field} from adminserverlist where {$where} order by {$orderBy}";
        return Config::admindb()->fetchRow($sql);
    }

    /**
     * @param string $field
     * @param string $where
     * @param string $orderBy
     * @return array
     */
    public static function get_kf_server_super_list($field = '*',$where = '1',$orderBy = 'pid asc',$limit = ''){
        $sql = "select {$field} from kf_server_super where {$where} and status = 1 order by {$orderBy} {$limit}";
        return Config::admindb()->fetchRow($sql);
    }

    /**
     * @param string $field
     * @param string $where
     * @return array|null
     */
    public static function get_kf_server_super($field = '*',$where = '1'){
        $sql = "select {$field} from kf_server_super where {$where}";
        return Config::admindb()->fetchOne($sql);
    }

    /**
     * @param string $field
     * @param string $where
     * @param string $orderBy
     * @return array
     */
    public static function get_platform($field = '*',$where = '1',$orderBy = 'gid asc'){
        $sql = "select {$field} from adminplatformlist where {$where} order by {$orderBy}";
        return Config::admindb()->fetchRow($sql);
    }

    /**
     * @param $sid
     * @param array $key
     * @return array
     */
    public static function get_game_base_info($sid,$key = array()){
        $info = array();
        $where = '1';
        if($key){
            $where .= " and cf_name in ('".implode("','",$key)."')";
        }
        $sql = "select cf_name,cf_value from base_game where {$where}";
        $res = Config::gamedb($sid)->fetchRow($sql);
        if($res){
            foreach ($res as $re) {
                 $info[$re['cf_name']] = $re['cf_value'];
            }
        }

        return $info;
    }

    /**
     * 通过主服获取从服
     * @param $mainId
     */
    public static function get_follow_by_main($field = '*',$mainId){
        if(is_array($mainId)){
            $mainId = " main_id in (" .implode(',',$mainId).")";
        }else{
            $mainId = " main_id = {$mainId}";
        }
        $sql = "select {$field} from adminserverlist where {$mainId}";
        return Config::admindb()->fetchRow($sql);
    }

}