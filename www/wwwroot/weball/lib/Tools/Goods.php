<?php
/**
 * 物品管理类
 */
class Goods
{
	//物品列表
	static $goods_list = array();
	static $goods_list_attr = array();

	static $ColorList = array( 0=>'', 1=>'#00ff00', 2=>'#0066ff', 3=>'#8E00D2', 4=>'#ffba00', 5=>'#FFE100' );

	/**
	 * 获取物品列表 (含属性)
	 * @return array
	 */
	public static function goods_list_attr($type = 1,$item_type = 0){
		if(self::$goods_list[$type.$item_type]){
			return self::$goods_list[$type.$item_type];
		}
		$goods_list = array();
		$admindb = Config::admindb();
		$where = '';
		if($type){
			$where .= " and type = ".(int)$type;
		}
		if($item_type){
			$where .= " and item_type = ".(int)$item_type;
		}
		$sql = "select id as goods_id,item as goods_name,attr from base_config where 1 {$where}";
		$res = $admindb->fetchRow($sql);
		foreach ($res as $key => $val) {
			$val['attr'] = json_decode($val['attr'],true);
			$goods_list[$val['goods_id']] = $val;
		}
		self::$goods_list[$type.$item_type] = $goods_list;

		return self::$goods_list[$type.$item_type];
	}

	/**
	 * 获取物品列表
	 * @return array
	 */
	public static function goods_list($type = 1,$item_type = 0){
		if(self::$goods_list[$type.$item_type]){
			return self::$goods_list[$type.$item_type];
		}
		$goods_list = array();
		$admindb = Config::admindb();
		$where = '';
		if($type){
			$where .= " and type = ".(int)$type;
		}
		if($item_type){
			$where .= " and item_type = ".(int)$item_type;
		}
		$sql = "select id as goods_id,item as goods_name from base_config where 1 {$where}";
		$res = $admindb->fetchRow($sql);
		foreach ($res as $key => $val) {
			$goods_list[$val['goods_id']] = $val['goods_name'];
		}
		self::$goods_list[$type.$item_type] = $goods_list;

		return self::$goods_list[$type.$item_type];
	}

	/**
	 * 解析erlang格式物品
	 */
	public static function explain_goods($goodStr, $goods_list){
		if($goodStr=='[]'){
                return '空';
        }

        $post = str_replace(array('[{5,[',']}]'), '', $goodStr);
        preg_match_all('/\\d+,\d,\d,\d+/',$post,$a);

        foreach ($a[0] as $v) {
            $tr = explode(',', $v);
            $name = $goods_list[$tr[0]] . '(' .$tr[0] . ')';
            $amount = '数量:' . $tr[1];
            $bd = $tr[2] ? '' : '不绑';
            $time =  ($tr[3] ? '过期:'.date('Y-m-d') : '');
            $goods[] = $name . ',' . $amount . ',' . $bd . ' ' . $time;
        }

        return @implode("<br>", $goods);
  
	}
}

?>