<?php

/**
 * 安全认证
 */
class Auth
{
    //后台管理相关
    static $USER_EDIT = 'USER_EDIT';                //用户编辑或添加
    static $USER_DEL = 'USER_DEL';                //用户删除

    static $MENU_ADD = 'MENU_ADD';                    //添加菜单
    static $MENU_EDIT = 'MENU_EDIT';                    //编辑菜单
    static $MENU_DEL = 'MENU_DEL';                    //删除菜单
    static $MENU_OPT_MANAGE = 'MENU_OPT_MANAGE';    //菜单-可操作项

    static $GROUP_ADD = 'GROUP_ADD';                //添加或编辑分组
    static $GROUP_DEL = 'GROUP_DEL';                //删除分组
    static $GROUP_VIEW = 'GROUP_VIEW';                //查看分组的权限
    static $GROUP_EDIT = 'GROUP_EDIT';                //设置分组的权限

    static $LOG_MAN = 'LOG_MAN';                    //日志-管理

    static $CHANNEL_ADD = 'CHANNEL_ADD';            //渠道添加
    static $CHANNEL_EDIT = 'CHANNEL_EDIT';            //渠道修改
    static $CHANNEL_DEL = 'CHANNEL_DEL';            //渠道修改

    static $PLATFORM_ADD = 'PLATFORM_ADD';            //专服添加
    static $PLATFORM_EDIT = 'PLATFORM_EDIT';        //专服修改
    static $PLATFORM_DEL = 'PLATFORM_DEL';            //专服删除

    static $SERVER_ADD = 'SERVER_ADD';              //添加服务器
    static $SERVER_EDIT = 'SERVER_EDIT';              //修改服务器
    static $SERVER_DEL = 'SERVER_DEL';              //删除服务器
    static $SERVER_CONFIG = 'SERVER_CONFIG';        //服务器列表生成配置

    static $SERVER_STOP = 'SERVER_STOP';//维护服务器

    static $CARD_MAN = 'CARD_MAN';//卡号生成、导出

    static $SERVER_APPLY = 'SERVER_APPLY';//申请开服
    static $SERVER_SH = 'SERVER_SH';//审核开服

    static $MAIL_APPLY = 'MAIL_APPLY'; //邮件申请
    static $MAIL_MAN = 'MAIL_MAN'; //邮件审核

    static $NOTICE_MAN = 'NOTICE_MAN';//添加删除平台公告

    static $RUMOR_MAN = 'RUMOR_MAN'; //传闻管理

    static $BAN_LOGIN = 'BAN_LOGIN'; // 封号操作
    static $BAN_TALK = 'BAN_TALK'; // 禁言

    static $SAVA_BASE_GAME = 'SAVA_BASE_GAME';//游戏配置保存
    static $SAVA_BASE_KEY = 'SAVA_BASE_KEY';//游戏配置保存

    static $HF_APPLY = 'HF_APPLY';//申请合服、取消合服
    static $HF_MAN = 'HF_MAN';//合服完成操作

    static $CACHE_GM = 'CACHE_GM'; //清除GM缓存
    static $CACHE_API = 'CACHE_API'; //清除API缓存

    static $SEARCH_ROLE_INFO = 'SEARCH_ROLE_INFO';//查询同帐号玩家

    static $BUILD_PRODUCT_CONFIG = 'BUILD_PRODUCT_CONFIG';//生成商品配置文件
    static $DEL_PRODUCT_CONFIG = 'DEL_PRODUCT_CONFIG';//删除商品配置文件
    static $ADD_PRODUCT_CONFIG = 'ADD_PRODUCT_CONFIG';//增加商品配置文件
    static $UPDATE_PRODUCT_CONFIG = 'UPDATE_PRODUCT_CONFIG';//更新商品配置文件

    static $COMMON_LOG_MAN = 'COMMON_LOG_MAN';//通用日志管理
    static $COMMON_LOG_ADD = 'COMMON_LOG_ADD';//通用日志添加
    static $COMMON_LOG_EDIT = 'COMMON_LOG_EDIT';//通用日志添加
    static $COMMON_LOG_DEL = 'COMMON_LOG_DEL';//通用日志删除
    static $COMMON_LOG_DOWN = 'COMMON_LOG_DOWN';//通用日志导出

    static $RESTART_GAME_SERVER = 'RESTART_GAME_SERVER';//开始重启游戏服
    static $RESTART_GAME_SERVER_ADD = 'RESTART_GAME_SERVER_ADD';//添加重启游戏服计划
    static $RESTART_GAME_SERVER_DEL = 'RESTART_GAME_SERVER_DEL';//删除重启游戏服计划
    static $RESTART_GAME_SERVER_STOP = 'RESTART_GAME_SERVER_STOP';//手动结束重启游戏服计划

    static $ZONE_MAN = 'ZONE_MAN';//分区操作
    static $ZONE_CONFIG = 'ZONE_CONFIG';//分区操作

    static $RUN_CUSTOM_DATA = 'RUN_CUSTOM_DATA'; //抓取自定义数据

    static $ACTIVITY = 'ACTIVITY'; //运营活动
    static $KF_ADD = 'KF_ADD'; //添加或编辑跨服中心
    static $KF_DEL = 'KF_DEL'; //删除跨服中心

    static $KF_GROUP_GM = 'KF_GROUP_GM'; //跨服分组刷新GM
    static $KF_GROUP_EDIT = 'KF_GROUP_EDIT'; //跨服分组添加编辑删除

    static $GUILD_POS = 'GUILD_POS';//更换军盟成员职位

    /**
     * 检查是否拥有可操作项的权限
     *
     * 在“菜单管理”下面，可以对每个菜单进行一个可操作项的编译
     * 然后在“权限分组”中，选择拥有的操作项，即表示该分组成员拥有的操作权限
     * 目前由于菜单接口文件管理比较混乱，没有通过简单的入口来作处理，因此先全手工添加权限
     *
     * @param string $actionType 操作类型
     * @param string $menu 菜单项
     * @return bool
     */
    public static function check_action($action)
    {
        return true;
        if (!isset($_SESSION['auth_action'])) self::show_no_power();
        if ($_SESSION['auth_action'] === 'ALL') return true;
        if (in_array($action, $_SESSION['auth_action'])) return true;
        self::show_no_power();
    }

    /**
     * 获取分组权限
     *
     * @param int $gid 分组id
     * @return array
     */
    public static function get_group_menu($gid)
    {
        $cache = Ext_Memcached::getInstance('user');

        //超级管理员，返回所有菜单
        if ($_SESSION['super'] == 'YES') {
            return self::get_all_menu();
        }

//        $key = self::get_menu_key($gid);
        $key = sprintf(Key::$gm_m['menu_gid'],$gid);
        $cache_data = $cache->fetch($key);

        if (0 && is_array($cache_data) && !empty($cache_data)) {
            return json_encode($cache_data);
        } else {
            $db = Ext_Mysql::getInstance('admin');
            $group_row = $db->fetchOne("SELECT menuids FROM admingroup WHERE gid={$gid}");
            if ($group_row['menuids']) {
                $menu = $db->fetchRow("SELECT * FROM `adminkind` WHERE kid IN({$group_row['menuids']}) AND `show`='YES' ORDER BY pid, ksort ,kid");
                $cache->store($key, $menu);
                return json_encode($menu);
            } else {
                $cache->store($key, array());
                return array();
            }
        }
    }

    /**
     * 获取日志
     *
     * @param int $gid 分组id
     * @return array
     */
    public static function get_menu_log()
    {
        $cache = Ext_Memcached::getInstance('user');

        $key = 'get_menu_log';
        $cache_data = $cache->fetch($key);

        if (0 && is_array($cache_data) && !empty($cache_data)) {
            return json_encode($cache_data);
        } else {
            $db = Ext_Mysql::getInstance('admin');
            
            $menu = $db->fetchRow("select CONCAT('8888',id) kid,title name,CONCAT('/action_gateway.php?ctl=log&act=tableShow&table_id=',id)filename from base_log");
            $cache->store($key, $menu);
            return $menu;
            
        }
    }

    /**
     * 取所有菜单
     */
    private static function get_all_menu()
    {
        $cache = Ext_Memcached::getInstance('user');
        $key = Key::$gm_m['menu_all'];
        $data = $cache->fetch($key);
        if (is_array($data)) return json_encode($data);

        $db = Ext_Mysql::getInstance('admin');

        $menu = $db->fetchRow("SELECT * FROM adminkind WHERE `show`='YES' ORDER BY pid, ksort ,kid");

        $cache->store('menu_all', $menu);


        return json_encode($menu);
    }

    /**
     * 根据文件名称 返回对应的分类信息
     *
     * @param string(要查找的文件名称) $filename
     * @return array
     */
    public static function MenuFindPidByFile($filename)
    {
        return Ext_Mysql::getInstance('admin')->fetchOne("SELECT * FROM `adminkind` WHERE `filename` = '" . mysql_escape_string(trim($filename)) . "'");
    }

    /**
     * 获取可操作项
     *
     * @param int $gid 分组id
     * @return array
     */
    public static function get_group_rights($gid)
    {
        $cache = Ext_Memcached::getInstance('user');

        //超级管理员，返回所有菜单
        if ($_SESSION['super'] == 'YES') {
            return 'ALL';
        }

        $key = sprintf(Key::$gm_m['rights_gid'],$gid);
        $cache_data = $cache->fetch($key);

        if (is_array($cache_data)) {
            return $cache_data;
        } else {
            $db = Ext_Mysql::getInstance('admin');
            $group_row = $db->fetchOne("SELECT rights FROM admingroup WHERE gid={$gid}");
            if ($group_row['rights']) {
                $rights_rows = $db->fetchRow("SELECT mark FROM adminrights WHERE id IN({$group_row['rights']})");
                $rights = array();
                foreach ($rights_rows as $v) {
                    $rights[] = $v['mark'];
                }
                $cache->store($key, $rights);
                return $rights;
            } else {
                $cache->store($key, array());
                return array();
            }
        }
    }

    /**
     * 提示没有权限操作
     */
    private static function show_no_power()
    {
        alert('您没有权限操作', '');
    }
}