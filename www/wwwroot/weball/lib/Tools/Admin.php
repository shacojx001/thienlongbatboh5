<?php
/**
 * 管理员类
 */

class Admin
{
    public static function get_parent($gid)
    {
  		$db = Ext_Mysql::getInstance('admin');
  		$sql = "select gid,pid from admingroup where gid = '$gid' ";
  		$ginfo = $db->fetchOne($sql);
  		if(!empty($ginfo))
        {
  			if($ginfo['pid'] == 0) // 已是最上层则返回自己组ID
            {
  				return $ginfo['gid'];
  			}
            else // 继续循环直到最上层的父ID
            {
  				return self::get_parent($ginfo['pid']);
  			}
  		}
        else
        {
  			return -1;
  		}
    }

    /**
	 * 检验用户是否合法
	 */
	public static function checkUser()
    {
        //判断是否存在session
        if (isset($_SESSION['uid']))
        {
        	//超级管理员，直接通过
        	if($_SESSION['super'] == 'YES'){
             	return true;
            }
            
            $rightsarray = self::getCurrentKind();
            if(empty($rightsarray)) {
                return true;
            }//找不到菜单的，默认为通过 可能是操作,另外用auth::check_action判断权限

            //有权限但为空的
            if(empty($_SESSION['grights'])){
             	alert('您没有任何权限');
            }elseif(!empty($_SESSION['grights'])){//有权限，菜单既存在时
             	$grights = explode(',',$_SESSION['grights']);
             	if(!in_array($rightsarray['kid'],$grights)){
             		alert('您没有权限');
             	}
            }

            return $rightsarray;
        }
        //用户没有登陆
        else
        {
            alert("请登录后再执行相关操作！", "./login.php");
            exit;
        }
    }

    /**
	 * 管理员登陆
	 *
	 * @param 用户数据 $array
	 */
    public static function adminlogin($array)
    {
   		$db = Ext_Mysql::getInstance('admin');
        // 禁止admin用户直接登陆
        if (trim($array['passport']) == "admin")
        {
            return false;
        }
        // 使用传来的用户名查询库表获得用户信息
        $adminInfo = $db->fetchOne("SELECT * FROM `adminuser` WHERE `passport` = '" . mysql_escape_string(trim($array['passport'])) . "'");
        // 该用户连续5次输错密码则保证封锁1小时
        if($adminInfo['status'] != 1){
            alert($array['passport'].'状态不可用,请联系负责人处理!','./login.php');
        }
        $now = time();
        if($adminInfo['error_num'] >= 5 && ($now - $adminInfo['error_time'] < 3600))
        {
            alert("你输入的错误次数过多, 系统已经禁止你的登录!", "./login.php");
            exit;
        }
        // 该用户不存在或者传来的密码错误
        if (!$adminInfo || $adminInfo['password'] != md5(trim($array['password'])))
        {
            // 之前已经连续5次输错密码
            if($adminInfo['error_num'] >= 5)
            {
                // 已经封锁超过1小时则错误次数清0
                if($now-$adminInfo['error_time'] > 3600)
                {
                    $db->query("update adminuser set error_time='{$now}', error_num='0' where `passport` = '" . mysql_escape_string(trim($array['passport'])) . "'");
                }
            }
            else
            {
                $db->query("update adminuser set error_time='{$now}', error_num=error_num+1 where `passport` = '" . mysql_escape_string(trim($array['passport'])) . "'");
            }
            alert("你输入的帐号密码错误!", "./login.php");
            exit;
        }

        if($adminInfo['super'] != 'YES' && $adminInfo['gid'] == 0){
            alert('你没有权限登陆!');
        }

        // 3- 保存会话信息
		$_SESSION['uid']      = $adminInfo['uid'];
		$_SESSION['gid']      = $adminInfo['gid'];
        $_SESSION['pid']    = $adminInfo['pid'];
        $_SESSION['username'] = $adminInfo['username'];
        $_SESSION['passport'] = $adminInfo['passport'];
        $_SESSION['super']    = $adminInfo['super'];
        $_SESSION['if_mail']    = $adminInfo['if_mail'];
        $_SESSION['if_develop']    = $adminInfo['if_develop'];

		// 根据不同用户组，获得当前用户可视菜单权限
		if ($adminInfo['super'] == 'YES') // 没有用户组但是超级管理员
		{
            //菜单权限
			$_SESSION['grights'] = "ALL";
            // 所属专服
            $_SESSION['gids'] = array();
            //所属渠道
            $_SESSION['channel'] = array();
		}
		else // 有用户组
		{
            // 菜单权限
			$groupInfo = Admin::getAdminGroup($adminInfo['gid']);
			$_SESSION['grights'] = $groupInfo['menuids'];
            // 所属专服
            $parentGidAndChanel = self::getParentGidAndChannel($adminInfo['pid']);
            if($adminInfo['gids']){
                $_SESSION['gids'] = explode(',',$adminInfo['gids']);
            }else{
                $_SESSION['gids'] = $parentGidAndChanel['gids']?explode(',',$parentGidAndChanel['gids']):array();
            }
            //所属渠道
            if($adminInfo['channel']){
                $_SESSION['channel'] = explode(',',$adminInfo['channel']);
            }else{
                $_SESSION['channel'] = $parentGidAndChanel['channel']?explode(',',$parentGidAndChanel['channel']):array();
            }
		}
		// 用户可操作权限
		$_SESSION['auth_action'] = Auth::get_group_rights($adminInfo['gid']);

        // 判断切换服列表
        if($adminInfo['super'] != 'YES')
        {
            $where = '';
            if($_SESSION['gids']){
                $where .= " and gid in (".implode(',',$_SESSION['gids']).")";
                if($_SESSION['channel']){
                    $res = $_SESSION['channel'];
                    foreach ($res as $key => $val) {
                        if (strpos($where, "FIND_IN_SET")) {
                            $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                        } else {
                            $where .= " AND  ( FIND_IN_SET('$val', channel_list) ";
                        }
                    }
                    $where .= ')';
                }
            }

            // 查询库表中的默认服信息
            $sql = "select `id`,`sdb` from `adminserverlist` where `default` in(0,1,4) $where order by gid,id asc limit 1";
            $admin_db_val = $db->fetchOne($sql);
            $_SESSION['admin_db_link'] = empty($admin_db_val['sdb']) ? '' : $admin_db_val['sdb'];
            $_SESSION['selected_sid'] = empty($admin_db_val['id']) ? '' : $admin_db_val['id'];
        }
        else
        {
            $sql = "select `id`,`sdb` from `adminserverlist` where `default` in(0,1,4) order by gid,id asc limit 1";
            $admin_db_val = $db->fetchOne($sql);
            $_SESSION['admin_db_link'] = empty($admin_db_val['sdb']) ? '' : $admin_db_val['sdb'];
            $_SESSION['selected_sid'] = empty($admin_db_val['id']) ? '' : $admin_db_val['id'];
        }


        Admin::adminLog($_GET['username']."登录成功",4);

        // 更新登陆记录
		$time = time();
		$db->query("UPDATE `adminuser` SET login_time='$time',error_num='0',`last_ip` = '" . $_SERVER['REMOTE_ADDR'] . "' WHERE `uid` = '" . $adminInfo['uid'] . "'");

		return $adminInfo;
    }
    /**
	 * 用户退出登陆
	 */
    public static function adminlogout()
    {
        unset($_SESSION['uid'], $_SESSION['grights'], $_SESSION['username'], $_SESSION['super'], $_SESSION['gid'], $_SESSION['grights'], $_SESSION['auth_action'],$_SESSION['gids'],$_SESSION['channel'],$_SESSION['admin_db_link'],$_SESSION['selected_sid'],$_SESSION['pid'],$_SESSION['if_develop']);
        session_destroy();

        alert("", "./login.php");
        exit;
    }

    /**
	 * 根据传来的GID获得对应小组的信息
	 *
	 * @param int $gid
	 * @return array
	 */
    public static function getAdminGroup($gid)
    {
        return Ext_Mysql::getInstance('admin')->fetchOne("SELECT * FROM `admingroup`  WHERE `gid` = '" . intval($gid) . "'");
    }

    /**
     * 添加管理员操作记录
     *
     * @param string $text
     * @param int $type
     */
    public static function adminLog($text, $type=0)
    {
        return Ext_Mysql::getInstance('admin')->query("INSERT INTO `adminlog` SET `uid` = '" . $_SESSION['uid'] . "', `username` = '" . $_SESSION['username'] . "', `ip` = '" . $_SERVER['REMOTE_ADDR'] . "', `ctime` = '" . time() . "', `text` = '" .mysql_escape_string($text) . "', `type`='{$type}'");
    }


    /**
	 * 根据文件名称返回对应的分类
	 *
	 * @return array
	 */
    private function getCurrentKind()
    {
        //分析当前文件的路径信息
        $pathInfo = pathinfo($_SERVER['PHP_SELF']);
        if(!empty($_SERVER['QUERY_STRING'])){
        	
            parse_str($_SERVER['QUERY_STRING'],$out);
            $realurl = array();
            $realurl['ctl'] = $out['ctl'];
            $realurl['act'] = $out['act'];
            $url = http_build_query($realurl);
            $query = "?".$url;
        }
        $result = Auth::MenuFindPidByFile(htmlspecialchars($pathInfo['basename'].$query));

        return $result;
    }

    /**
     * 获取用户的父级的所属专服和渠道
     * @param int $uid
     * @return bool
     */
    private static function getParentGidAndChannel($pid=0){
        if(!$pid) return array();
        $sql = "select gids,channel from adminuser where uid = {$pid} limit 1";

        return Ext_Mysql::getInstance('admin')->fetchOne($sql);
    }

}

/**
 * 弹出框
 */
function alert($content = false, $url = "")
{
    $javascript = "<script>";

    //链接警告窗口
    $javascript .= $content ? ("alert('" . $content . "');") : ("");

    //链接跳转信息
    $javascript .= ($url == "") ? ("history.back();") : ("document.location.href='" . $url . "'");

    //链接尾部信息
    $javascript .= "</script>";

    echo $javascript;
    exit;
}

