<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 17/12/2
 * Time: 下午3:02
 */
class Key {
    //接口key
    static $api = array(
        'server_list'=>'api|%s_server_list',//服务器列表
        'notice'=>'api|%s_notice',//公告
    );

    //gm菜单权限
    static $gm_m = array(
        'menu_all' => 'gm_m|menu_all',
        'menu_gid' => 'gm_m|menu_%s',
        'rights_gid' => 'gm_m|rights_%s',
    );

    static $real = array(
        'real_accname' => 'api|real_%s_%s',
    );

}