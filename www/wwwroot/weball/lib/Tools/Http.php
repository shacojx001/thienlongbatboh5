<?php
/**
 * 物品管理类
 * @author xhg
 *
 */

class Http
{

    //GET访问
    static function httpGet( $url ,$timeout = 7)
    {
        if ( !$url ) return 'http_url_error';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_BUFFERSIZE, 8094);
        curl_setopt($ch, CURLOPT_URL, $url);
        $RESPONSE = curl_exec($ch);
        if ( $ERRNUM=curl_errno($ch) ) {
            curl_close($ch);
            return 'curl_status_'.$ERRNUM;
        }
        else if ( ($ERRNUM=curl_getinfo($ch, CURLINFO_HTTP_CODE)) != 200 ) {
            curl_close($ch);
            return 'http_status_'.$ERRNUM;
        }
        curl_close($ch);
        return $RESPONSE;
    }

    //POST访问
    static function httpPost( $url, $content, $timeout = 7 )
    {
		if ( !$url ) return 'http_url_error';
		if ( !$content ) $content = '';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_BUFFERSIZE, 8094);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
		$RESPONSE = curl_exec($ch);
		if ( $ERRNUM=curl_errno($ch) ) {
			curl_close($ch);
			return 'curl_status_'.$ERRNUM;
		}
		else if ( ($ERRNUM=curl_getinfo($ch, CURLINFO_HTTP_CODE)) != 200 ) {
			curl_close($ch);
			return 'http_status_'.$ERRNUM;
		}
		curl_close($ch);
		return $RESPONSE;
    }

    //获取IP
    static function getIP() {
        $cip = getenv ( 'HTTP_CLIENT_IP' );
        $xip = getenv ( 'HTTP_X_FORWARDED_FOR' );
        $rip = getenv ( 'REMOTE_ADDR' );
        $srip = $_SERVER ['REMOTE_ADDR'];
        if ($cip && strcasecmp ( $cip, 'unknown' )) {
            $onlineip = $cip;
        } elseif ($xip && strcasecmp ( $xip, 'unknown' )) {
            $onlineip = $xip;
        } elseif ($rip && strcasecmp ( $rip, 'unknown' )) {
            $onlineip = $rip;
        } elseif ($srip && strcasecmp ( $srip, 'unknown' )) {
            $onlineip = $srip;
        }
        preg_match ( "/[\d\.]{7,15}/", $onlineip, $match );
        return $match [0] ? $match [0] : 'unknown';
    }


}

?>
