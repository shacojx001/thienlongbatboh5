<?php
/**
 * 订单类
 * User: fanweiting
 * Date: 2017/7/25
 * Time: 19:15
 */

class Order{
    /**
     * @var string
     */
    private static $tb_order = 'order';
    private static $tb_order_key = 'order_key';

    public static function updateOrder($orderId,$data=array()){
        $centerDb = Ext_Mysql::getInstance('allcenter');
        $table = self::$tb_order;
        $set = array();
        foreach ($data as $field=>$val){
            $set[] = "`$field`='$val'";
        }
        $sql = "UPDATE `{$table}` SET ".implode(',',$set)." WHERE (`order_id`='{$orderId}')";
        $centerDb->query($sql);

        return $centerDb->getAffectRows();
    }

    /**
     * @param $orderId
     * @param array $data
     * @return bool
     */
    public static function createOrder($orderId,$data = array()){
        if(empty($orderId)||empty($data)){
            return false;
        }
        $data['order_id'] = $orderId;
        $fields = array();
        $values = array();
        foreach ($data as $field=>$val){
            $fields[]="`$field`";
            $values[]="'$val'";
        }
        $fields = implode(',', $fields);
        $values = implode(',', $values);

        $table = self::$tb_order;
        $sql = "INSERT INTO `{$table}` ($fields)VALUES($values)";
        $re = Ext_Mysql::getInstance('allcenter')->query($sql);

        if($re){
            return $orderId;
        }
        return false;
    }

    /**
     * @return bool|string
     */
    public static function makeOrderNo($pre = 9){
        $time = explode('.',microtime(true));
        $time[1] = str_pad(substr($time[1],0,3),3,0,STR_PAD_RIGHT);
        $order = $pre;
        $order .= date('y',$time[0])*12+date('m',$time[0]);
        $order .= date('d',$time[0]);
        $order .= substr($time[0],5,5);
        $order .= $time[1];

        return $order.self::getOrderKey();
    }

    /**
     * @return string
     */
    public static function getOrderKey(){
        $id = Ext_Mysql::getInstance('allcenter')->insert(self::$tb_order_key,array('id'=>''));
        if(!$id){
            $id = mt_rand(1,9999);
        }
        if($id < 1000 ){
            $string = str_pad($id,4,'0',STR_PAD_LEFT);
        }else{
            $string = substr($id,-4);
        }
        return $string;
    }
}