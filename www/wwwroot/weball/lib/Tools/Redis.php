<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/26
 * Time: 17:23
 */

class Redis_Server{

    private static $ins=null;
    private $conn;
    //新 2018年06月05日
    private static $redis;

    public static function getInstance(){
        if (self::$ins == null){
            self::$ins = new self();;
        }
//        print_r("---------start-----redis-----------");
        return  self::$ins;
    }

    //新 2018年06月05日
    public static function getRedisInstance($name = 'default'){
        if(self::$redis){
            return self::$redis;
        }
        $config = include CONFIG_DIR . 'redis.php';
        $conf = $config[$name];
        self::$redis = new Redis();
        self::$redis->connect($conf['host'], $conf['port']);
        if($conf['auth']){
            self::$redis->auth($conf['auth']);
        }
        return self::$redis;
    }

    private function __construct()
    {
        $redis = new Redis();
        // $redis->connect('10.1.0.9',3666);
        $redis->connect('127.0.0.1',3666);//本地
        $redis->auth('a14ba758462ab1b6c1c808b90a03cb8d');
//        $redis->connect('localhost',6379);
//        $redis->auth('requirepass');
        $this->conn = $redis;
    }


    public function push($key,$data){
       $index =  $this->conn->lPush($key,$data);
        return $index;
    }

    public function pop($key){
        return $this->conn->rPop($key);
    }

    public function get_count($key){
        return $this->conn->lSize($key);
    }

    public function get_conn(){
        return $this->conn;
    }

}