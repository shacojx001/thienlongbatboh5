<?php

/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 17/11/21
 * Time: 上午9:27
 */
class Erlang
{

    const SUCCESS = 1;
    const FAIL = 0;

    

    /**
     * 封号
     * @param $ban_ids   array()
     * @return int
     */
    public static function limit_login($role_id, $sid)
    {

        $erl = Config::get_center_erl($sid);
        $res = $erl->get("lib_gm", "ban_role", "[~i]", array(array($role_id)));
 
        return $res;
    }

    /**
     * 解除封号
     * @param $ban_ids
     * @return int
     */
    public static function unlimit_login($ban_ids, $sid)
    {
       
        $erl = Config::get_center_erl($sid);
        $res = $erl->get("lib_gm", "unban_role", "[[" . $erl->repeatFormat('~i', count($ban_ids)) . "]]", array(array($ban_ids)));
       
        return $res;
    }


    /**
     * 禁言
     * @param $id
     * @param $time
     * @return int
     */
    public static function talk_limit($id, $time, $sid)
    {
        $erl = Config::get_center_erl($sid);
     
        $res = $erl->get("lib_gm", "ban_chat", "[~i,~i,~i]", array(array($id, 2, $time)));
        return $res;
    }

    /**
     * 解除禁言
     * @param $id
     * @return int
     */
    public static function unlimit_talk($id, $sid)
    {
        $erl = Config::get_center_erl($sid);
      
        $res = $erl->get("lib_gm", "unban_chat", "[~i]", array(array(intval($id))));
        return $res;
    }

   

    /**
     * 所有玩家发送邮件
     * @param $lv
     * @param $title
     * @param $content
     * @param $post
     * @param $source
     * @return mixed
     */
    public static function erl_send_mail_all_new($sid, $lv, $title, $content, $post, $source = '')
    {
        $erl = Config::get_center_erl($sid);
        $res = $erl->get("lib_gm", "sys2common", '[~b,~s,~s,~b]', array(
            array(
                $lv,
                $title,
                $content,
                $post,
            )
        ));
        usleep(50000);
        return $res;
    }

    /**
     * 指定玩家发送邮件
     * @param $id_list
     * @param $title
     * @param $content
     * @param $post
     * @return int
     */
    public static function erl_send_mail_new($sid, $id_list, $title, $content, $post)
    {
        $erl = Config::get_center_erl($sid);
        $res = $erl->get("lib_gm", "sys2select", '[~b,~s,~s,~b]', array(
            array(
                $id_list,
                $title,
                $content,
                $post,
            )
        ));
        usleep(50000);

        return $res;

    }

    /**
     * 在线玩家发邮件
     * @param $sid
     * @param $lv
     * @param $title
     * @param $content
     * @param $post
     * @param $source
     * @return bool|int
     */
    public static function erl_send_mail_online_new($sid, $lv, $title, $content, $post, $source = '')
    {
        $erl = Config::get_center_erl($sid);
        $res = $erl->get("lib_gm", "sys2online", '[~b,~s,~s,~b]', array(
            array(
                $lv,
                $title,
                $content,
                $post,
            )
        ));

        usleep(50000);

        return $res;

    }

    /**  发传闻
     */
    public static function erl_rumor_all($sid)
    {   
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "reinit_rumor");   
        usleep(50000);
        return is_array($res)?json_encode($res) : $res;
    }

    /**
     * svr_announcement:set_anno_invaild(AnnoId)
     * @param $sid
     * @param $post
     */
    public static function erl_rumor_del($sid)
    {   
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';
        $res = $erl->get("lib_gm", "reinit_rumor");
        usleep(50000);
        return is_array($res) ? json_encode($res) : $res;
    }

    /**
     * 充值通知接口
     * @param $sid
     * @param $pay_no
     * @return int|string
     */
    public static function erl_charge($sid, $pay_no)
    {
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "charge", "[~s]", array(
            array(
                $pay_no
            )
        ));
        usleep(50000);
        if ($res[0] == 'true') {
            return self::SUCCESS;
        }
        return self::FAIL;
    }


    /**
     * 开启运营活动 lib_gm:start_op_act_local($ThemeId, $ActId, $AwardIndex, $StartTime, $EndTime)
     * @param $sid
     * @param $post
     * @return int|string
     */
    public static function erl_start_activity($sid, $data)
    {
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "add_operation_activity", "[~i,~i,~i,~i,~i,~i,~i]", array(
            array(
                intval($data['id']),
                intval($data['act_id']),
                intval(1),
                intval($data['st']),
                intval($data['et']),
                intval($data['index']),
                intval(1),
            )
        ));

        usleep(50000);

        return $res;
    }

    //stop_op_act_local
    /**
     * 停止运营活动
     * @param $sid
     * @param $post
     * @return bool|string
     */
    public static function erl_stop_activity($sid, $id)
    {
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "remove_operation_activity", "[~i]", array(
            array(
                intval($id),
            )
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 刷新跨服配置
     *  svr_kf_group 模块
     * distribute_group 函数
     * 0 参数
     */
    public static function erl_flash_kf($kfCenter)
    {
        $erl = Config::get_kf_erl($kfCenter);

        if (!$erl) return $kfCenter . '服务器连接erl出错';
        $res = $erl->get("svr_kf_group", "distribute_group", "[]", array(
            array()
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 添加了新服的分组配置到kf_list之后，调用一下这个接口
     *  svr_kf_group 模块
     * distribute_group 函数
     * 0 参数
     */
    public static function erl_distribute_group_new_server($kfCenter,$post)
    {
        $erl = Config::get_kf_erl($kfCenter);

        if (!$erl) return $kfCenter . '服务器连接erl出错';
        $res = $erl->get("svr_kf_group", "distribute_group_new_server", "[~i]", array(
            array(
                intval($post['sid']),
            )
        ));

        usleep(50000);

        return $res;
    }

    //开始跨服运营活动：
    //lib_gm:start_op_act，参数：主题id，活动id，奖励index，开始时间，结束时间，跨服分组id
    //结束活动：lib_gm:stop_op_act，参数：主题id，跨服分组id
    //消息发给100节点
    public static function erl_start_activity_operation_kf($kfCenter, $post)
    {
        $erl = Config::get_kf_erl($kfCenter);
        if (!$erl) return $kfCenter . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "start_op_act", "[~i,~i,~i,~i,~i,~i]", array(
            array(
                intval($post['theme_id']),
                intval($post['act_id']),
                intval($post['index']),
                intval($post['st']),
                intval($post['et']),
                intval($post['group_id']),
            )
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 停止跨服运营活动
     * @param $kfCenter
     * @param $post
     * @return bool|string
     */
    public static function erl_stop_activity_operation_kf($kfCenter, $post)
    {
        $erl = Config::get_kf_erl($kfCenter);
        if (!$erl) return $kfCenter . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "stop_op_act", "[~i,~i]", array(
            array(
                intval($post['info_id']),
                intval($post['group_id']),
            )
        ));

        usleep(50000);

        return $res;
    }

    

    /**
     * 单服日常开启 发10节点：lib_gm，start_daily_act_local，参数：活动id
     * @param $sid
     * @param $post
     * @return bool|string
     */
    public static function erl_start_daily_activity($sid, $post)
    {
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "start_daily_act_local", "[~i]", array(
            array(
                intval($post['act_id']),
            )
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 单服日常关闭 发10节点：lib_gm，stop_daily_act_local，参数：活动id
     * @param $sid
     * @param $post
     * @return bool|string
     */
    public static function erl_stop_daily_activity($sid, $post)
    {
        $erl = Config::get_center_erl($sid);
        if (!$erl) return $sid . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "stop_daily_act_local", "[~i]", array(
            array(
                intval($post['act_id']),
            )
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 跨服日常开启，发100节点：lib_gm，start_daily_act，参数：分组id，活动id
     * @param $kfCenter
     * @param $post
     * @return bool|string
     */
    public static function erl_start_daily_activity_kf($kfCenter, $post)
    {
        $erl = Config::get_kf_erl($kfCenter);
        if (!$erl) return $kfCenter . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "start_daily_act", "[~i]", array(
            array(
                intval($post['act_id']),
                intval($post['group_id']),
            )
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 跨服日常关闭，发100节点：lib_gm，stop_daily_act，参数：分组id，活动id
     * @param $kfCenter
     * @param $post
     * @return bool|string
     */
    public static function erl_stop_daily_activity_kf($kfCenter, $post)
    {
        $erl = Config::get_kf_erl($kfCenter);
        if (!$erl) return $kfCenter . '服务器连接erl出错';

        $res = $erl->get("lib_gm", "stop_daily_act", "[~i]", array(
            array(
                intval($post['act_id']),
                intval($post['group_id']),
            )
        ));

        usleep(50000);

        return $res;
    }

    /**
     * 通知游戏服更新聊天屏蔽词
     * @param $sid
     * @return bool
     */
    public static function erl_update_chat_word($sid)
    {
        $erl = Config::get_center_erl($sid);
        if( empty($erl) ){
            return false;
        }
        $res = $erl->get("svr_filter", "update_all_words", '[]', array(
            array()
        ));
        usleep(50000);
        return $res;
    }

    /**
     * 添加服务端白名单
     * @param $sid
     * @param accname
     * @param ip
     */
    public static function add_white_accname($sid, $accname, $ip = 'all')
    {
        $erl = Config::get_center_erl($sid);
        if( empty($erl) ){
            return false;
        }
        if($ip != 'all'){
            $ip = str_replace(',', '.', $ip);
            list($ip1, $ip2, $ip3, $ip4) = explode('.', $ip);
        }
      
        if($ip != 'all'){
            $res = $erl->get("lib_gm", "add_white_accname", '[~s,~i,~i,~i,~i]', array(
                array($accname, intval($ip1), intval($ip2), intval($ip3), intval($ip4))
            ));
        }else{
            $res = $erl->get("lib_gm", "add_white_accname", '[~s]', array(
                array($accname)
            ));
        }
        
        usleep(50000);
        return $res;
    }

    /**
     * 添加服务端白名单
     * @param $sid
     * @param $accname
     * @return bool
     */
    public static function del_white_accname($sid, $accname)
    {
        $erl = Config::get_center_erl($sid);
        if( empty($erl) ){
            return false;
        }
        $res = $erl->get("lib_gm", "del_white_accname", '[~s]', array(
            array($accname)
        ));
        usleep(50000);
        return $res;
    }

    /**
     * 修改开服时间
     * @param $sid
     * @param $accname
     * @return bool
     */
    public static function set_open_time($sid, $time)
    {
        $erl = Config::get_center_erl($sid);
        if( empty($erl) ){
            return false;
        }
        $time = intval($time);
        $res = $erl->get("lib_gm", "set_open_time", '[~i]', array(
            array($time)
        ));
        usleep(50000);
        return $res;
    }    




}