<?php
/**
 * 配置工具.
 * User: fanweiting
 * Date: 2017/8/31
 * Time: 14:07
 */
Class Config{
    /**
     * @var $erlconfig
     */
    protected static $erlconfig;
    /**
     * @var
     */
    protected static $erlkfconfig;
    /**
     * @var null
     */
    protected static $obj = null;

    /**
     * @return Ext_Mysqli|null
     */
    public static function admindb()
    {
        return Ext_Mysqli::getInstance('admin');
    }

    /**
     * 连接center数据库
     * @param $sid
     * @return bool|Ext_Mysqli
     */
    public static function centerdb()
    {
        return Ext_Mysqli::getInstance('center');
    }

    /**
     * 连接各服后台数据库
     * @param $sid
     * @return bool|Ext_Mysqli
     */
    public static function gamedb($sid){
        global $all_server_bysid;

        if (!isset($all_server_bysid[$sid])) {
            $info = Model::get_server_info_by($sid);
            if($info){
                $conf[$info['sdb']] = array(
                    'host'=>$info['db_host'],
                    'user'=>$info['db_user'],
                    'pwd'=>$info['db_pwd'],
                    'port'=>$info['db_port'],
                    'db'=>$info['sdb'],
                    'charset'=>'utf8',
                );
                return Ext_Mysqli::getInstance($info['sdb'],false,$conf);
            }else{
                die('Sid['.$sid.'] is not Found In System! Plz check!');
            }
        }
        return Ext_Mysqli::getInstance($all_server_bysid[$sid]);
    }

    /**
     * 连接跨服
     * @param $sever
     * @return Ext_Mysqli
     */
    public static function kfdb($sever)
    {
        return Ext_Mysqli::getInstance($sever);
    }


    /**跨服连接ERLANG
     * @param $sid
     * @return Ext_Erlang
     */
    public static function get_center_erl($sid)
    {
        if (empty(self::$erlconfig[$sid])) {
            $sql = "select * from base_game";
            $erlinfo = self::gamedb($sid)->fetchRow($sql);
            $rows = array();
            foreach ($erlinfo as $key => $val) {
                if ($val['cf_name'] == 'erl_cookie') {
                    $rows['erl_cookie'] = $val['cf_value'];
                }
                if ($val['cf_name'] == 'erl_node') {
                    $rows['erl_node'] = $val['cf_value'];
                }
            }
            if (!empty($rows)) {
                self::$erlconfig[$sid] = Ext_Erlang::getInstance(false, false, $rows, $sid);
                return self::$erlconfig[$sid];
            } else {
                return false;
            }
        } else {
            return self::$erlconfig[$sid];
        }
    }

    /**
     * Erlang连接跨服
     */
    public static function get_kf_erl($kfCenter){
        $system_config = self::system_config();
        if(empty(self::$erlkfconfig[$kfCenter])){
            $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
            $re = $HRpc_client->getServerInfoByNode($kfCenter);
            if(!$re){
                //重试一次
                $re = $HRpc_client->getServerInfoByNode($kfCenter);
            }
            if($re){
                $rows = array('erl_node'=>$re['node'],'erl_cookie'=>$re['cookie']);
                self::$erlkfconfig[$kfCenter] = Ext_Erlang::getInstance(false,false,$rows,$re['node']);
                return self::$erlkfconfig[$kfCenter];
            }else{
                return false;
            }
        }else{
            return self::$erlkfconfig[$kfCenter];
        }
    }

    /**
     * 生成配置文件 仅仅重新生成db_admin.php servers.cfg.php
     * @return bool
     */
    public static function config_wx()//武侠游戏配置
    {
        $admindb = Ext_Mysql::getInstance('admin');
        $sql = "SELECT
                    a.id,
                    a.gid,
                    a.sname,
                    a.description,
                    a.sdb,
                    a.db_host,
                    a.db_user,
                    a.db_pwd,
                    a.url,
                    a.`default`,
                    a.otime,
                    a.db_port,
                    a.srv_num,
                    a.hot_state,
                    a.recom_state,
                    a.game_ip,
                    a.game_port,
                    a.channel_list,
                    a.stop_service_describe,
                    a.visible_time,
                    a.main_id,
                    a.state
                  FROM
                    adminserverlist a
                  ORDER BY
                    otime ASC";
        $res = $admindb->fetchRow($sql);

        $list = array();
        $all_server_bysid = array();
        if($res)
        foreach ($res as $key => $val) {
            $list[$val['id']] = $val;
            $all_server_bysid[$val['id']] = $val['sdb'];
        }

        $db_config = "";
        $db_conf_game = array();
        foreach ($list as $key => $val) {
            if ($val['main_id']) {
                $sid = $val['main_id'];
                $db_config .= "
                \"{$val['sdb']}\" => array(
                  \"host\"=>\"{$list[$sid]['db_host']}\",
                  \"user\"=>\"{$list[$sid]['db_user']}\",
                  \"pwd\"=>\"{$list[$sid]['db_pwd']}\",
                  \"port\"=>{$list[$sid]['db_port']},
                  \"db\"=>\"{$list[$sid]['sdb']}\",
                  \"charset\"=>\"utf8\",
                ),\r\n
            ";
                $db_conf_game[$val['sdb']] = array(
                    'host'=>$list[$sid]['db_host'],
                    'user'=>$list[$sid]['db_user'],
                    'pwd'=>$list[$sid]['db_pwd'],
                    'port'=>$list[$sid]['db_port'],
                    'db'=>$list[$sid]['sdb'],
                    'charset'=>'utf8',
                );

            } else {
                $db_config .= "
                \"{$val['sdb']}\" => array(
                  \"host\"=>\"{$val['db_host']}\",
                  \"user\"=>\"{$val['db_user']}\",
                  \"pwd\"=>\"{$val['db_pwd']}\",
                  \"port\"=>{$val['db_port']},
                  \"db\"=>\"{$val['sdb']}\",
                  \"charset\"=>\"utf8\",
                ),\r\n
            ";

                $db_conf_game[$val['sdb']] = array(
                    'host'=>$val['db_host'],
                    'user'=>$val['db_user'],
                    'pwd'=>$val['db_pwd'],
                    'port'=>$val['db_port'],
                    'db'=>$val['sdb'],
                    'charset'=>'utf8',
                );
            }

        }

        // 生成配置db_admin.php
        $db_temp = file_get_contents(CONFIG_DIR . 'db_admin_template.php');
        $db_temp = str_replace('{DB_TEMP}', "//此文件即将废弃\r\n".$db_config, $db_temp);
        $size = file_put_contents(CONFIG_DIR . 'db_admin.php', $db_temp);
        if (!$size) {
            alert('DB_ADMIN配置生成失败');
        }

        //生成配置db_conf_game.php
        $_db_conf_game = "<?php\r\nreturn ".var_export($db_conf_game,true).";";
        $size = file_put_contents(CONFIG_DIR . 'db_conf_game.php', $_db_conf_game);
        if (!$size) {
            alert('生成db_conf_game.php失败');
        }

        //渠道平台列表
        $sql = "SELECT
                    a.gid,
                    channel_str AS channel,
                    c.pay_key,
                    c.ios_version,
                    c.is_pay,
                    c.game_id,
                    c.sdk_mark
                  FROM
                    channel_list c
                  LEFT JOIN adminplatformlist a ON c.gid = a.gid
                  ORDER BY gid desc";
        $channel_list = self::returnK($admindb->fetchRow($sql), "channel");
        foreach ($channel_list as $key => $val) {
            $channel_list[$key]['ios_version'] = explode(',', $val['ios_version']);
        }    
        //平台列表
        $sql = "select * from adminplatformlist order by id desc";
        $gid_res = self::returnK($admindb->fetchRow($sql), "gid");
       

        $all_server_bysid = var_export($all_server_bysid, true);
        $server_list_info = var_export($list,true);
        $channel_list = var_export($channel_list, true);
    
        $gid_list_info = var_export($gid_res, true);

        $config = <<<CODE
<?php

\$all_server_bysid = $all_server_bysid;

\$server_list_info = $server_list_info;

\$channel_list = $channel_list;

\$gid_list_info = $gid_list_info;

?>
CODE;

        //加锁
        $file = CONFIG_DIR . 'servers.cfg.php';
        $fp = fopen($file, 'w');
        if (flock($fp, LOCK_EX)) {//加锁
            fwrite($fp, $config);
            flock($fp, LOCK_UN);//解锁
        }
        fclose($fp);

        alert('生成成功');
    }

    /**
     * 生成白名单，黑名单配置
     * @return bool
     */
    public static function create_ip_white_list()
    {
        $admindb = Ext_Mysql::getInstance('admin');
        //IP白名单列表
        $sql = "SELECT
                    ip,
                    channel,
                    `type`
                  FROM
                    ip_white_list where 1";
        $res = $admindb->fetchRow($sql);

        $accname_white_list = array();
        $ip_white_list = array();
        $ip_ban_list = array();
        $device_ban_list = array();
        foreach ($res as $key => $val) {
            if($val['type']==1){
                $accname_white_list[$val['channel']][] = $val['ip'];
            }elseif($val['type']==2){
                $ip_white_list[] = $val['ip'];
            }elseif($val['type']==3){
                $ip_ban_list[] = $val['ip'];
            }elseif($val['type']==4){
                $device_ban_list[] = $val['ip'];
            }
        }

        $accname_white_list = var_export($accname_white_list, true);
        $ip_white_list = var_export($ip_white_list, true);
        $ip_ban_list = var_export($ip_ban_list, true);
        $device_ban_list = var_export($device_ban_list, true);
    
        $config = <<<CODE
<?php

\$accname_white_list = $accname_white_list;

\$ip_white_list = $ip_white_list;

\$ip_ban_list = $ip_ban_list;

\$device_ban_list = $device_ban_list;

?>
CODE;
        //加锁
        $file = CONFIG_DIR . 'ban.cfg.php';
        $fp = fopen($file, 'w');
        if (flock($fp, LOCK_EX)) {//加锁
            fwrite($fp, $config);
            flock($fp, LOCK_UN);//解锁
        }
        fclose($fp);

        alert('生成成功');
    }


    /**
     * 将数据字段替换键值方法
     * @param $date
     * @param $k
     * @return array
     */
    public static function returnK($date, $k)
    {
        $return = array();
        foreach ($date as $key => $val) {
            $return[$val[$k]] = $val;
        }
        ksort($return);
        return $return;
    }

    //检查数据库连接
    public static function mysql_line_check($data){
        if(!$data['host']||!$data['port']){
            return 0;
        }
        $conn = mysql_connect($data['host'].":".$data['port'],$data['user'],$data['pwd'],true);
        if(!$conn){
            return 1;
        }
        if(!mysql_select_db($data['sdb'],$conn)){
            return 2;
        }
        mysql_close($conn);

        return true;
    }

    /**
     * 获取系统配置
     */
    public static function system_config(){
        static $sys_conf;
        if($sys_conf){
            return $sys_conf;
        }
        global $system_config;
        if(empty($system_config)){
            $system_config = array();
            $sys_conf = Config::admindb()->fetchRow("select * from system_config");
            $system_config = array();
            foreach($sys_conf as $val){
                $system_config[$val['name']] = $val['value'];
            }
        }
        $sys_conf = $system_config;

        return $sys_conf;
    }

    /**
     * 处理合服
     * @param $hefuId
     * @return bool
     */
    public static function deal_hefu($hefuId){
        if(!$hefuId){
            return false;
        }
        $sql = "select * from hefu_list where id = {$hefuId} and status = 0";
        $info = Config::admindb()->fetchOne($sql);
        if(!$info){
            return false;
        }
        $mainId = $info['sid'];
        $followIds = explode(',',$info['sid_list']);
        $key = array_search($mainId ,$followIds);
        unset($followIds[$key]);
        //获取从服的从服
        $rst = Model::get_follow_by_main('id',$followIds);
        if($rst){
            foreach ($rst as $v) {
                $followIds[] = $v['id'];
            }
        }

        //获取主服信息
        $mainInfo = Model::get_server_info_by($mainId);

        //更改从服 字段:sdb,db_host,db_port,url,game_ip,game_port,main_id
        $followInfos = array();
        foreach ($followIds as $followId) {
            $followInfo = Model::get_server_info_by($followId);
            $followInfos[$followId] = array(
                'sdb'=>$followInfo['sdb'],
                'db_host'=>$followInfo['db_host'],
                'db_port'=>$followInfo['db_port'],
                'url'=>$followInfo['url'],
                'game_ip'=>$followInfo['game_ip'],
                'game_port'=>$followInfo['game_port'],
                'main_id'=>$followInfo['main_id'],
                'srv_num'=>$followInfo['srv_num'],
            );
        }

        //记录合服前信息 record_for_roll_back
        $val = json_encode($followInfos);
        $sql = "update hefu_list set record_for_roll_back = '{$val}' where id = {$hefuId}";
        Config::admindb()->query($sql);

        //更新游戏服合服信息
        $sql = "update adminserverlist set `srv_num`=`srv_num`+1 where id = {$mainId}";
        Config::admindb()->query($sql);
        $sql = "update adminserverlist set `srv_num`=`srv_num`+1,sdb='{$mainInfo['sdb']}',db_host='{$mainInfo['db_host']}',db_port='{$mainInfo['db_port']}',url='{$mainInfo['url']}',game_ip='{$mainInfo['game_ip']}',game_port='{$mainInfo['game_port']}',main_id='{$mainInfo['id']}'
where id in (".implode(',',$followIds).")";
        $rst = Config::admindb()->query($sql);
        if($rst){
            //更新hefu表状态
            $sql = "update hefu_list set status = 9 where id = {$hefuId}";
            Config::admindb()->query($sql);
            return true;
        }else{
            return false;
        }
    }

    /**
     * 通过合服任务回滚操作
     * @param $hefuId
     * @return bool
     */
    public static function deal_hefu_roll_back($hefuId){
        if(!$hefuId){
            return false;
        }
        $sql = "select * from hefu_list where id = {$hefuId} and status = 9";
        $info = Config::admindb()->fetchOne($sql);
        if(!$info){
            return false;
        }
        $followInfos = json_decode($info['record_for_roll_back'],true);
        if(!is_array($followInfos)) return false;
        foreach ($followInfos as $sid => $followInfo) {
            Config::admindb()->update('adminserverlist',array('id'=>$sid),$followInfo);
        }
        $sql = "update hefu_list set status = 0 where id = {$hefuId}";
        Config::admindb()->query($sql);
        return true;
    }

    //把0活动写入跨服中心
    public static function kf_config($sid){
        $sql = "select kf_center from adminplatformlist p LEFT JOIN adminserverlist s on p.gid=s.gid where s.id=$sid";
        $res =  Config::admindb()->fetchOne($sql);
        $kf_center = $res['kf_center'];
        if(!$kf_center) {
            alert("跨服中心为空！");
        }
        $max_group_id = Kf::get_max_group_num($kf_center);
        $time = time();
        $sql = "INSERT IGNORE INTO base_kf_group(`act_id`,`group_id`,`server_id`,`ctime`)VALUES('0','$max_group_id','$sid',$time);";
        Config::kfdb($kf_center)->query($sql);

        return true;     
    }

}