<?php
/**
 * 玩家管理类
 * @author xhg
 *
 */
 
class User
{
    /**
     * IP限制/IP白名单 对应ban_info表, base_game内ip_ban 数据
     * @param $ip_list
     * @param $process      string 操作       ban_ip:添加IP黑名单, unban_ip:删除IP黑名单,
     *                                       bai_ip:添加IP白名单, unbai_ip:删除白名单
     * @param $process_name string 操作解释
     * @param bool|Ext_Erlang $erl  同步时,传入erlang对象
     * @param bool|Ext_Mysql $current_db    同步时,传入DB对象
     * @return string
     */
    static function ipLimit($ip_list, $process, $process_name, $erl =false, $current_db =false) {
		if(!$ip_list){
			return false;
		}
		foreach ($ip_list as $v) {
			$ip_list_str = trim(preg_replace("/[^0-9\,\.]+/", "", $v));
			if (strlen($ip_list_str) < 5) return 'IP地址长度有误';
		}
		switch($process){
			case 'ban_ip':{
				$res = Erlang::limit_ip($erl,$ip_list);
				if($res != 1){
					return "封禁ip：".implode(',',$ip_list).'失败！';
				}
				$type=1;
				break;
			}
			case 'unban_ip':{
				$res = Erlang::unlimit_ip($erl,$ip_list);
				if($res != 1){
					return "解禁ip：".implode(',',$ip_list).'失败！';
				}
				$type=7;
				break;
			}
		}

		Admin::adminLog("$process_name 成功(".implode(',',$ip_list).")",2);
        $msg = "对同IP(".implode(',',$ip_list).")$process_name 成功！  ";

		$time = time();
		$user = $_SESSION['username']?$_SESSION['username']:'系统';
		$db = Ext_Mysql::getInstance();
		$db->query("insert into log_ban(type,object,description,time,admin)values($type,'".implode(',',$ip_list)."','{$msg}','{$time}','{$user}')");

        return $msg;
    }

	/*
	封禁/解封角色
	svr_ban:ban_role(RoleIDList) RoleIDList格式：[1, 2, 3]
	svr_ban:unban_role(RoleIDList)
	封禁设备ID
	svr_ban:ban_device(DeviceList) DeviceList格式：["test", "test2"]
	svr_ban:unban_device(DeviceList)
	封禁IP
	svr_ban:ban_ip(IPList)   IPList格式：["192.168.1.45"]
	svr_ban:unban_ip(IPList)
	*/
	/**
	 * 封号
	 */
	static function user_limit($process,$roleIds=array(),$description='',$sid){
		// print_r(11111111);exit;
		$time = time();
		$db = Config::gamedb($sid);
		switch ($process){
			case 'limit_login':
				
				if($roleIds){
					$sql = "select role_id,`name` nick_name from role_basic where role_id = $roleIds";
					$rows = $db->fetchOne($sql);
					if($rows)
						$ban_ids = intval($rows['role_id']);
						$log_arr = '('.$rows['role_id'].')'.$rows['nick_name'];
				}
	
				$res = 'false';
				
				if($ban_ids){
					$res = Erlang::limit_login($ban_ids,$sid);
					Ext_Debug::log('---------封号erlang返回结果--------'.$res);
					Admin::adminLog("对玩家($log_arr)进行封号[svr_ban:ban_role],操作结果：{$res}",2);
				}
				
				$description = "对玩家封号，原因: ".$description;
				$db->query("insert into log_ban(type,object,description,time,admin)values(0,'" . $log_arr . "','$description','$time','{$_SESSION['username']}')");
				return array('state'=>1,'msg'=>"对玩家($ban_ids)封号成功.$res");
				break;
			case 'unlimit_login':
				$ban_ids = array();
				$log_arr = array();
				if($roleIds){
					$sql = "select role_id,`name` nick_name from role_basic where role_id in (".implode(',',$roleIds).")";
					$rows = $db->fetchRow($sql);
					if($rows)
						foreach ($rows as $row) {
							$ban_ids[] = intval($row['role_id']);
							$log_arr[] = '('.$row['role_id'].')'.$row['nick_name'];
						}
				}
				
				$res = 'false';
				$ban_ids = array_unique($ban_ids);
				if($ban_ids){
					$res = Erlang::unlimit_login($ban_ids,$sid);
					Admin::adminLog("对玩家(" . implode(',', $log_arr) . ")进行解除封号[svr_ban:unban_role],操作结果：{$res}",2);
				}
				if ($res != 'ok') {
					return array('state'=>0,'msg'=>"解除封号失败：".implode(',',$ban_ids));
				}
				//封号日志
				$description = "对玩家解除封号:".$description;
				$db->query("insert into log_ban(type,object,description,time,admin)values(2,'" . implode(',', $log_arr) . "','$description','$time','{$_SESSION['username']}')");
				return array('state'=>1,'msg'=>"对玩家(" . implode(',', $ban_ids) . ")解除封号成功");
				break;
		}

	}

	/**
	 * 禁言
	 */
	static function talk_limit($process,$role_id,$second,$description='',$sid){
		
		$t = time();
		$db = Config::gamedb($sid);
		switch ($process){
			case 'limit_talk'://禁言
				$time = $second;
				$SuccIds = array();
				$FailIds = array();
				$log_arr = array();
				
				$row = $db->fetchOne("select role_id,name from role_basic where role_id = '$role_id' limit 1");

				if (!$row['role_id']) continue;
				$id = intval($row['role_id']);
			
				if (!$id) continue;
				$res = Erlang::talk_limit($id,$time,$sid);
			
				Ext_Debug::log('---------禁言erlang返回结果--------'.$id.'--'.$sid.'--'.$time . '--' .$res);
				if ($res == 'ok') {
					$SuccIds[] = intval($id);
					$log_arr[] = '('.$row['role_id'].')'.$row['name'];
				}
				else $FailIds[] = $id;
				

				$min = floor(($second - time()) / 60);
				Admin::adminLog("对玩家(" . implode(',', $log_arr) . ")禁言{$min}分钟成功",3);
				//封号日志
				$description = "对玩家禁言{$min}分钟:".$description;
				$db->query("insert into log_ban(type,object,description,time,admin)values(3,'" . implode(',', $log_arr) . "','$description','$t','{$_SESSION['username']}')");
				return array('state'=>1,'msg'=>"对玩家(" . implode(',', $log_arr) . ")禁言{$min}分钟成功$res");
				break;
			case 'unlimit_talk'://解禁言
				$SuccIds = array();
				$FailIds = array();
				$log_arr = array();
				
				$row = $db->fetchOne("select role_id,name from role_basic where role_id = '$role_id' limit 1");

				if (!$row['role_id']) continue;
				$id = $row['role_id'];
				$res = Erlang::unlimit_talk($id,$sid);
				if ($res == 'ok') {
					$SuccIds[] = intval($id);
					$log_arr[] = '('.$row['role_id'].')'.$row['name'];
				}
				else $FailIds[] = $id;
					
				
				Admin::adminLog("对玩家(" . implode(',', $log_arr) . ")解除禁言成功",3);
				//封号日志
				$description = "解除禁言成功:".$description;
				$db->query("insert into log_ban(type,object,description,time,admin)values(4,'" . implode(',', $log_arr) . "','$description','$t','{$_SESSION['username']}')");
				return array('state'=>1,'msg'=>"对玩家(" . implode(',', $SuccIds) . ")解除禁言成功$res");
				break;
		}
	}


	/**
	 * 封禁设备号
	 */
	static function dev_limit($process,$devs=array(),$description='',$sid){
		$time = time();
		$db = Ext_Mysql::getInstance();
		switch ($process){
			case 'limit_device'://封设备
				if(!$devs){
					return false;
				}
				$ban_devs = array();
				foreach ($devs as $dev) {
					$ban_devs[] =  (string)$dev;
				}
				$res = Erlang::limit_device($ban_devs,$sid);
				if($res != 1){
					return array('state'=>0,'msg'=>"对设备号(" . implode(',', $ban_devs) . ")封禁失败");
				}
				Admin::adminLog("对设备号(" . implode(',', $ban_devs) . ")封禁成功",2);
				//封号日志
				$description = "封禁设备号成功:".$description;
				$db->query("insert into log_ban(type,object,description,time,admin)values(5,'" . implode(',', $ban_devs) . "','$description','$time','{$_SESSION['username']}')");
				return array('state'=>1,'msg'=>"对设备号(" . implode(',', $ban_devs) . ")封禁成功");
				break;
			case 'unlimit_device'://解封封设备
				if(!$devs){
					return false;
				}
				$ban_devs = array();
				foreach ($devs as $dev) {
					$ban_devs[] =  (string)$dev;
				}
				$res = Erlang::unlimit_device($ban_devs,$sid);
				if($res != 1){
					return array('state'=>0,'msg'=>"对设备号(" . implode(',', $ban_devs) . ")解封失败");
				}
				Admin::adminLog("对设备号(" . implode(',', $ban_devs) . ")解封成功",2);
				//封号日志
				$description = "解禁设备号成功:".$description;
				$db->query("insert into log_ban(type,object,description,time,admin)values(6,'" . implode(',', $ban_devs) . "','$description','$time','{$_SESSION['username']}')");
				return array('state'=>1,'msg'=>"对设备号(" . implode(',', $ban_devs) . ")解禁成功");
				break;
		}
	}
}
