<?php
/**
 * 跨服操作类
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/14
 * Time: 下午7:05
 */
Class Kf{

    /**
     * 跨服分组操作【运营活动类】
     * @param $gid
     * @param $config
     * @return bool
     */
    public static function dispatch_group_opt($gid,$config){
        //获取当前专服对应的跨服中心
        //获取当前跨服中心指定活动的所有游戏服
        //按策略分组
    // print_r($config);exit;    
    //     exit('此方法需要重写');
        $kfCenter = self::kf_node_by($gid);
        $group = array();

        $limit_time = strtotime(date('Y-m-d',strtotime('-6 day')));

        if(1 == $config['type']){
            //按开服时间
            if( !empty($config) && !empty($config['new_server_limit']) && $config['new_server_limit']!=1 ){
                $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description',' otime<'.$limit_time);
            }else{
                $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
            }
            if(!$serList||!is_array($serList)){
                die('此跨服中心没有游戏服!');
            }    
  
            $gSte = self::strategy_fixed_group_by_otime($kfCenter,$config['act_id'],$serList,$config['group_num']);
            if( empty($gSte[0]) || $gSte[0]!==true ){
                $group = null;
            }else{
                $group = $gSte[1];
            }

    // print_r($group);exit;        
            // $group = self::strategy_group_by_otime($kfCenter,$config['act_id'],$serList,$config['group_num']);
        }elseif(2 == $config['type']){
            //按战力
            //按开服时间
            if( !empty($config) && !empty($config['new_server_limit']) && $config['new_server_limit']!=1 ){
                $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description',' otime<'.$limit_time);
            }else{
                $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
            }
            if(!$serList||!is_array($serList)){
                die('此跨服中心没有游戏服!');
            }
            $powList = self::rpc_get_server_power_by_kf($kfCenter);

            foreach ($serList as $k=>$v) {
                if($powList[$v['id']]){
                    $serList[$k]['power'] = $powList[$v['id']]['value'];
                }else{
                    $serList[$k]['power'] = 0;
                }
            }

            $group = self::strategy_group_by_power($kfCenter,$config['act_id'],$serList,$config['group_num']);

        }else{
            $serList = array();
        }

        if(!$group){
            die('没生成分组!');
        }
        if(!self::del_group_by_act($kfCenter,$config['act_id'])){
            die("删除分组失败:{$kfCenter},{$config['act_id']}");
        }
        self::insertKfGroup($kfCenter,$group,100);

        return true;
    }
    /**
     * 执行分组操作 【玩法类】
     * @param $kfCenter
     * @param $config
     * @return bool
     */
    public static function dispatch_group($kfCenter,$config){
        //删除所有当前活动ID下的分组
        //获取当前跨服中心指定活动的所有游戏服
        //判断开服时间
        //按策略分组

        $group = $list = array();
        if(1 == $config['type']){
            //按开服时间
            $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
            if(!$serList||!is_array($serList)){
                return array(false,'此跨服中心没有游戏服!');
            }
            $now = strtotime(date('Y-m-d'));
            $open_days = ($config['open_days']) * 86400;
            foreach ($serList as $v) {
                $lim = strtotime(date('Y-m-d',$v['otime']));
                //开服时间大于几天
                if( ($now - $lim + 86400)< $open_days){
                    continue;
                }
                $list[] = $v;
            }
            $group = array();
            $serList = self::k_sort($list);
            $maxGroupId = self::get_max_group_num($kfCenter);
            $rule = json_decode($config['rule'],true);
            $serList = self::strategy_group_by_otime($group,$maxGroupId,$serList,$config['act_id'],$rule['4s'],10000,$rule['4n']);
            $serList = self::strategy_group_by_otime($group,$maxGroupId,$serList,$config['act_id'],$rule['3s'],$rule['3e'],$rule['3n']);
            $serList = self::strategy_group_by_otime($group,$maxGroupId,$serList,$config['act_id'],$rule['2s'],$rule['2e'],$rule['2n']);
            $serList = self::strategy_group_by_otime($group,$maxGroupId,$serList,$config['act_id'],$rule['1s'],$rule['1e'],$rule['1n']);
        }
        elseif(2 == $config['type']){
            //按战力
            $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
            if(!$serList||!is_array($serList)){
                return array(false,'此跨服中心没有游戏服!');
            }
            $powList = self::rpc_get_server_power_by_kf($kfCenter);
            foreach ($serList as $k=>$v) {
                if($powList[$v['id']]){
                    $serList[$k]['power'] = $powList[$v['id']]['value'];
                }else{
                    $serList[$k]['power'] = 0;
                }
            }
            $now = strtotime(date('Y-m-d'));
            $open_days = ($config['open_days']) * 86400;
            foreach ($serList as $v) {
                $lim = strtotime(date('Y-m-d',$v['otime']));
                //开服时间大于几天
                if( ($now - $lim + 86400 )< $open_days){
                    continue;
                }
                $list[] = $v;
            }
            $group = self::strategy_group_by_power($kfCenter,$config['act_id'],$list,$config['group_num']);
        }
        elseif(4 == $config['type']){
            //按战力（新）
            $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
            if(!$serList||!is_array($serList)){
                return array(false,'此跨服中心没有游戏服!');
            }
            $powList = self::rpc_get_server_power_new_by_kf($kfCenter);
            foreach ($serList as $k=>$v) {
                if($powList[$v['id']]){
                    $serList[$k]['power'] = $powList[$v['id']]['value'];
                }else{
                    $serList[$k]['power'] = 0;
                }
            }
            $now = strtotime(date('Y-m-d'));
            $open_days = ($config['open_days']) * 86400;
            foreach ($serList as $v) {
                $lim = strtotime(date('Y-m-d',$v['otime']));
                //开服时间大于几天
                if( ($now - $lim + 86400 )< $open_days){
                    continue;
                }
                $list[] = $v;
            }
            $group = self::strategy_group_by_power_new($kfCenter,$config['act_id'],$list,$config['group_num']);

        }else{
            $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
            if(!$serList||!is_array($serList)){
                return array(false,'此跨服中心没有游戏服!');
            }
            $gSte = self::strategy_fixed_group_by_otime($kfCenter,$config['act_id'],$serList,$config['group_num']);
            if( empty($gSte[0]) || $gSte[0]!==true ){
                $group = null;
            }elseif( is_array($gSte[1]) ){
                $group = $gSte[1];
            }else{
                return $gSte;
            }
        }
        if(!$group){
            return array(false,'没生成分组!');
        }
        if(!self::del_group_by_act($kfCenter,$config['act_id'])){
            return array(false,"删除分组失败:{$kfCenter},{$config['act_id']}");
        }     
        self::insertKfGroup($kfCenter,$group,100);

        file_put_contents('./dispatch_group_single_cron.log',date('Y-m-d H:i:s')."|dispatch_group|$kfCenter|{$config['act_id']}\n",FILE_APPEND);
        if(empty($config['act_id'])){
            return array(false,'活动id空!');
        } ;
      
        return array(true,'分组成功!');
    }


    /**
     * 玩法类跨服自身一服一组
     */
    public static function dispatch_group_single_cron($kfCenter,$actId){
        static $kfSerList = array();
        $g_list = self::get_super_kf_group_list($kfCenter,"act_id=$actId");
    
        $in_group_ids = array();
        if($g_list){
            foreach ($g_list as $v) {
                $in_group_ids[] = $v['server_id'];
            }
        }

        $serInfo = array();
        if(!isset($kfSerList[$kfCenter])){
            $serList = self::rpc_get_server_by_kf($kfCenter,'id,otime,description');
        }else{
            $serList = $kfSerList[$kfCenter];
        }
        if(!$serList){
            return true;
        }
        $all_ids = array();
        $list = self::k_sort($serList,'otime');
        foreach ($list as $v) {
            $all_ids[] = $v['id'];
            $serInfo[$v['id']] = $v;
        }
        $not_in_group_ids = array_diff($all_ids,$in_group_ids);
        $time = time();
        foreach ($not_in_group_ids as $sid) {
            $maxGroupId = self::get_max_group_num($kfCenter);
            $group = array();
            $group[] = array(
                'group_id'=>$maxGroupId,
                'server_id'=>$serInfo[$sid]['id'],
                'act_id'=>$actId,
                'server_node'=>$serInfo[$sid]['server_node'],
                'server_cookies'=>$serInfo[$sid]['server_cookies'],
                'server_name'=>$serInfo[$sid]['description'],
                'time'=>$time,
                'step'=>0,
            );
            self::insertKfGroup($kfCenter,$group,100);
            //调erlang通知服务端
            file_put_contents('./dispatch_group_single_cron.log',date('Y-m-d H:i:s')."|dispatch_group|$kfCenter|{$sid}\n",FILE_APPEND);
            // Erlang::erl_distribute_group_new_server($kfCenter,array('sid'=>$sid));
        }
    }

    /**
     * 分组策略:按开服时间，多少服一组
     * @param $kfCenter
     * @param $act_id
     * @param $serList
     * @param $s
     * @param $e
     * @param $n
     * @return array
     */
    public static function strategy_group_by_otime(&$group,&$maxGroupId,$serList,$act_id,$s,$e,$n){
        $each = 1;
        $time = time();
        if(!$s || !$e || !$n){
            return $serList;
        }
        foreach ($serList as $k => $v) {
            $now = strtotime(date('Y-m-d'));
            $otime = strtotime(date('Y-m-d',$v['otime']));
            $od = ($now - $otime )/86400 + 1;
            //var_dump($v['id'],$od,$s,$e); echo "<br>";

            if( $od >= $s && $od <= $e){
                if ($each > $n) {
                    $maxGroupId++;
                    $each = 1;
                }
                $group[]= array(
                    'group_id'=>$maxGroupId,
                    'server_id'=>$v['id'],
                    'act_id'=>$act_id,
                    'server_node'=>$v['server_node'],
                    'server_cookies'=>$v['server_cookies'],
                    'server_name'=>$v['description'],
                    'ctime'=>$time,
                    'step'=>1,
                );
                $each++;
                unset($serList[$k]);
            }
        }

        //不满$n数的补全
        if(($each-1) != $n){
            $i = 1;
            $last = $n - ($each - 1);
            foreach ($serList as $k => $v) {
                if($i>$last){
                    break;
                }
                $group[]= array(
                    'group_id'=>$maxGroupId,
                    'server_id'=>$v['id'],
                    'act_id'=>$act_id,
                    'server_node'=>$v['server_node'],
                    'server_cookies'=>$v['server_cookies'],
                    'server_name'=>$v['description'],
                    'ctime'=>$time,
                    'step'=>1,
                );
                $i++;
                unset($serList[$k]);
            }
        }
        $maxGroupId++;

        return $serList;
    }

    /**
     * 分组策略:按战力--新
     * @param $kfCenter
     * @param $act_id
     * @param $serList
     * @param $group_num
     * @return array
     */
    public static $newPowerGroup = array();//战力分组后的分组数组
    public static $newPowerGroupNum = 0;
    public static $newPowerEach = 1;
    public static $newPowerMaxGroupId = 0;
    public static $newPowerAct_id = 0;
    public static $newPowerSerListSurplus = array();
    public static function strategy_group_by_power_new($kfCenter,$act_id,$serList,$group_num){
        $serList = self::k_sort($serList,'power');
        self::$newPowerMaxGroupId = self::get_max_group_num($kfCenter);
        self::$newPowerEach = 1;
        self::$newPowerGroup = array();
        self::$newPowerGroupNum = $group_num;
        self::$newPowerAct_id = $act_id;
        self::$newPowerSerListSurplus = $serList;
        self::eachStepGroupPower();
        return self::$newPowerGroup;
    }
    //递归判断
    public static function eachStepGroupPower(  ){
        if( count(self::$newPowerSerListSurplus)<=0 ){
            return false;
        }
        $serList = self::$newPowerSerListSurplus;
        //如果整个专区内未分组服数≤X个，则直接分1组完毕。不再执行后面。
        if( count($serList)<= self::$newPowerGroupNum ){
            self::pushToGroupSer($serList);
        }
        //如果整个专区内未分组服数≤X*2个，则跳过step4，直接执行step5
        elseif( count($serList)<= self::$newPowerGroupNum*2 ){
            self::eachStepTwoGroupPower($serList);
        }
        //整个专区内未分组服数＞X*2
        elseif( count($serList) > self::$newPowerGroupNum*2 ){
            //获取前后5天相邻开服的服列表
            $serListOneArr = array_slice($serList,0,1,false);
            self::handleOTimeBetween( $serList, $serListOneArr[0]['otime']);
        }
        return true;
    }
    //把战力前3的服匹配，另外（X-3）个服从库中随机抽取。一共得出X个服凑成一个小组，库剩余的服放回去
    public static function eachStepTwoGroupPower($serList){
        $serListThr = array_slice($serList,0,3);//战力前3的服
        $serListOrder    = array_slice($serList,3);//另外（X-3）个服
        $serListOrderTwo = array();
        $serListOne    = array_rand($serListOrder,self::$newPowerGroupNum-3);//另外（X-3）个服从库中随机抽取
        $serListOneTwo = array();
        foreach( $serListOrder as $k=>$v ){
            if( in_array($k,$serListOne) ){
                $serListOneTwo[] = $v;
            }else{
                $serListOrderTwo[] = $v;
            }
        }
        $serListSurePush = array_merge($serListThr,$serListOneTwo);
        self::pushToGroupSer($serListSurePush);
        self::eachStepGroupPower( );
    }
    //获取前后5天相邻开服的服列表
    public static function handleOTimeBetween($sList,$otime,$plus_day=0){
        $start_time = $otime-86400*($plus_day+5);
        $end_time   = $otime+86400*($plus_day+5);
        $arr = array();
        foreach( $sList as $k=>$v ){
            if( $start_time <= $v['otime'] && $v['otime']<$end_time  ){
                $arr[] = $v;
            }
        }

        if( count($arr)>= self::$newPowerGroupNum*2 ){
            self::eachStepTwoGroupPower($arr);
        }else{
            self::handleOTimeBetween($sList,$otime,$plus_day+5);
        }
    }
    //把服务器数组，推入分组里，返回分组数据
    public static function pushToGroupSer( $serList ){
        foreach ($serList as $k=>$v) {
            if ( self::$newPowerEach > self::$newPowerGroupNum ) {
                self::$newPowerMaxGroupId++;
                self::$newPowerEach = 1;
            }
            self::$newPowerGroup[]= array(
                'group_id' => self::$newPowerMaxGroupId,
                'server_id' => $v['id'],
                'act_id' => self::$newPowerAct_id,
                'server_node' => $v['server_node'],
                'server_cookies' => $v['server_cookies'],
                'server_name' => $v['description'],
                'time' => time(),
                'step' => 1,
            );
            self::$newPowerEach++;
            unset(self::$newPowerSerListSurplus[$k]);
        }
    }

    /**
     * 分组策略:按战力
     * @param $kfCenter
     * @param $act_id
     * @param $serList
     * @param $group_num
     * @return array
     */
    public static function strategy_group_by_power($kfCenter,$act_id,$serList,$group_num){
        $serList = self::k_sort($serList,'power');
        $maxGroupId = self::get_max_group_num($kfCenter);
        $each = 1;
        $group = array();
        $time = time();
        foreach ($serList as $v) {
            if ($each > $group_num) {
                $maxGroupId++;
                $each = 1;
            }
            $group[]= array(
                'group_id'=>$maxGroupId,
                'server_id'=>$v['id'],
                'act_id'=>$act_id,
                'server_node'=>$v['server_node'],
                'server_cookies'=>$v['server_cookies'],
                'server_name'=>$v['description'],
                'time'=>$time,
                'step'=>1,
            );
            $each++;
        }

        return $group;
    }

    /**
     * 分组策略:按开服时间固定分组
     * @param $kfCenter
     * @param $act_id
     * @param $serList
     * @param $group_num
     * @return array
     */
    public static function strategy_fixed_group_by_otime($kfCenter,$act_id,$serList,$group_num){
        $serList = self::k_sort($serList);
        $sids = self::check_in_group($kfCenter,$act_id);
        if(PHP_SAPI == 'cli' && $sids){
            if($sids){
                //把未加入分组的服分配组
                $time = time();
                foreach ($serList as $v) {
                    if(!in_array($v['id'],$sids)){
                        $group = array();
                        $groupId = self::check_not_full_group_num($kfCenter,$act_id,$group_num);
                        $group[]= array(
                            'group_id'=>$groupId,
                            'server_id'=>$v['id'],
                            'act_id'=>$act_id,
                            'server_node'=>$v['server_node'],
                            'server_cookies'=>$v['server_cookies'],
                            'server_name'=>$v['description'],
                            'time'=>$time,
                            'step'=>1,
                        );
                        self::insertKfGroup($kfCenter,$group,100);
                        //file_put_contents('./strategy_fixed_group_by_otime.log',date('Y-m-d H:i:s')."|dispatch_group|$kfCenter|{$act_id}\n",FILE_APPEND);
                        #Erlang::erl_distribute_group_new_server($kfCenter,array('sid'=>$v['id']));
                    }
                }
                return array(true,'分组成功');
//                exit('分组完成#!');//必须程序终止
            }
        }else{
            $group = array();
            $maxGroupId = self::get_max_group_num($kfCenter);
            $each = 1;
            $time = time();
            foreach ($serList as $v) {
                if ($each > $group_num) {
                    $maxGroupId++;
                    $each = 1;
                }
                $group[]= array(
                    'group_id'=>$maxGroupId,
                    'server_id'=>$v['id'],
                    'act_id'=>$act_id,
                    'server_node'=>$v['server_node'],
                    'server_cookies'=>$v['server_cookies'],
                    'server_name'=>$v['description'],
                    'time'=>$time,
                    'step'=>1,
                );
                $each++;
            }
            return array(true,$group);
//            return $group;
        }
    }

    /**
     * 删除超级跨服中心下的指定活动分组
     * @param $kfCenter
     * @param $actId
     * @return bool|mysqli_result
     */
    public static function del_group_by_act($kfCenter,$actId,$sids=array()){
        $sql = "delete from base_kf_group where act_id = {$actId}";
        if($sids){
            $sql .= " and server_id in (".implode(',',$sids).')';
        }
        return self::get_super_kf_db($kfCenter)->query($sql);
    }

    /**
    * 获取玩法类跨服活动 (假的活动ID)
    */
    public static function get_act_wf(){
        $list = array();
        $sql = "select * from base_config where `type`= 4";
        $re = Config::admindb()->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['id']] = $v;
            }
        }

        return $list;
    }

    /**
     * 获取跨服活动 运营活动类 (假的活动ID)
     */
    public static function get_act_opt(){
        $list = array();
        $sql = "select * from base_config where `type`= 6 ";
        $re = Config::admindb()->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['id']] = $v;
            }
        }

        return $list;
    }

    /**
     * 真实活动ID映射base_kf_sys的假ID
     * @param $type
     * @param $realActId
     * @return mixed
     */
    public static function get_act_map($type,$realActId){
        $sql = "select `id` from base_config where `type`= 16 and item_type = '{$type}' and attr = '{$realActId}'";
        $re = Config::admindb()->fetchOne($sql);
        return $re['id']?$re['id']:false;
    }


    /**
     * 获取发行信息
     * @return array
     */
    public static function get_pid(){
        $system_config = Config::system_config();

        $info = array();
        $pid_info = $system_config['pid_info'];
        foreach (explode('|',$pid_info) as $v) {
            list($pid,$name) = explode('=',$v);
            $info[$pid]=$name;
        }

        return $info;
    }


    /**
     * 获取跨服中心下的所有游戏服
     * @param $kfCenter
     * @return array
     */
    public static function rpc_get_server_by_kf($kfCenter,$fields='*',$where = '1'){
        $system_config = Config::system_config();

        $pids = self::get_pid();
     
        if(!$pids){
            return false;
        }
        $pid_info = array();
        foreach ($pids as $pid=>$name) {
            $key = 'rpc_kf_server_'.$pid;
            if(!$system_config[$key]){
                //alert('请检查发行ID配置:'.$key);
                return false;
            }
            $pid_info[] = array(
                'api'=>$system_config[$key],
                'pid'=>$pid,
                'name'=>$name,
            );
        }

        $servers = array();
        foreach ($pid_info as $v) {
            $HRpc_client = Ext_HRpc::get_cli_instance($v['api']);
            $re = $HRpc_client->getServerList($kfCenter,$fields,$where);
            if($re){
                foreach ($re as $k=>$ser) {
                    $re[$k]['pid'] = $v['pid'];
                    $re[$k]['pname'] = $v['name'];
                }

                $servers = array_merge($servers,$re);
            }
        }

        return $servers;
    }

    //获取 新战力 的数据 ，，通过跨服去调用那个数据
    public static function rpc_get_server_power_new_by_kf($kfCenter){
        $system_config = Config::system_config();

        $pids = self::get_pid();
        if(!$pids){
            return false;
        }
        $pid_info = array();
        foreach ($pids as $pid=>$name) {
            $key = 'rpc_kf_server_'.$pid;
            if(!$system_config[$key]){
                //alert('请检查发行ID配置:'.$key);
                return false;
            }
            $pid_info[] = array(
                'api'=>$system_config[$key],
                'pid'=>$pid,
                'name'=>$name,
            );
        }

        $servers = array();
        foreach ($pid_info as $v) {
            $HRpc_client = Ext_HRpc::get_cli_instance($v['api']);
            // \php\admin\api\kf_sub_server_manage.php
            $re = $HRpc_client->getServerPowerNew($kfCenter);
            if($re){
                foreach ($re as $k=>$ser) {
                    $re[$k]['pid'] = $v['pid'];
                    $re[$k]['pname'] = $v['name'];
                }

                $servers = array_merge($servers,$re);
            }
        }

        $list = array();
        foreach ($servers as $v) {
            $list[$v['sid']] = $v;
        }

        return $list;
    }

    public static function rpc_get_server_power_by_kf($kfCenter){
        $system_config = Config::system_config();

        $pids = self::get_pid();
        if(!$pids){
            return false;
        }
        $pid_info = array();
        foreach ($pids as $pid=>$name) {
            $key = 'rpc_kf_server_'.$pid;
            if(!$system_config[$key]){
                //alert('请检查发行ID配置:'.$key);
                return false;
            }
            $pid_info[] = array(
                'api'=>$system_config[$key],
                'pid'=>$pid,
                'name'=>$name,
            );
        }

        $servers = array();
        foreach ($pid_info as $v) {
            $HRpc_client = Ext_HRpc::get_cli_instance($v['api']);
            $re = $HRpc_client->getServerPower($kfCenter);
            if($re){
                foreach ($re as $k=>$ser) {
                    $re[$k]['pid'] = $v['pid'];
                    $re[$k]['pname'] = $v['name'];
                }

                $servers = array_merge($servers,$re);
            }
        }
        $list = array();
        foreach ($servers as $v) {
            $list[$v['sid']] = $v;
        }

        return $list;
    }

    /**
     * 获取跨服中心下的所有游戏服base_kf信息
     * @param $kfCenter
     * @return array
     */
    public static function rpc_get_server_base_kf($kfCenter){
        $system_config = Config::system_config();

        $pids = self::get_pid();
        if(!$pids){
            return false;
        }
        $pid_info = array();
        foreach ($pids as $pid=>$name) {
            $key = 'rpc_kf_server_'.$pid;
            if(!$system_config[$key]){
                //alert('请检查发行ID配置:'.$key);
                return false;
            }
            $pid_info[] = array(
                'api'=>$system_config[$key],
                'pid'=>$pid,
                'name'=>$name,
            );
        }

        $servers = array();
        foreach ($pid_info as $v) {
            $HRpc_client = Ext_HRpc::get_cli_instance($v['api']);
            $re = $HRpc_client->getServerKf($kfCenter);
            if($re){
                $servers = array_merge($servers,$re);
            }
            continue;
        }

        return $servers;
    }

    /**
     * 获取超级跨服中心数据库
     * @param $kfCenter
     * @return Ext_Mysqli
     */
    public static function get_super_kf_db($kfCenter){
        $config = self::get_kf_config($kfCenter);
        return Ext_Mysqli::getInstance($kfCenter,false,$config);
    }


    /**
     * 获取跨服中心信息
     * @param $kfCenter
     * @return mixed
     */
    public static function get_kf_config($kfCenter){
        static $kf = array();
        if(!isset($kf[$kfCenter])){
            $info = Model::get_kf_server_super('*',"node = '{$kfCenter}'");
            $kf[$kfCenter] = array(
                'host'=>$info['db_host'],
                'user'=>$info['db_user'],
                'pwd'=>$info['db_pwd'],
                'port'=>$info['db_port'],
                'db'=>$info['db_name'],
                'charset'=>'utf8',
            );
        }
        $conf[$kfCenter] = $kf[$kfCenter];

        return $conf;
    }

    /**
     * 获取最大分组ID
     * @param $kfCenter
     * @return int
     */
    public static function get_max_group_num($kfCenter){
        preg_match_all("/\d+/",$kfCenter,$res);
        $group_id_pre = (int)($res[0][1] . "000");
        if(!$group_id_pre) $group_id_pre = 10000000;//兼容开服服
        $group_id_end = $group_id_pre + 10000000;

        $sql = "select max(group_id) as mx from base_kf_group where group_id >= {$group_id_pre} and group_id <{$group_id_end}";    
        $re = self::get_super_kf_db($kfCenter)->fetchOne($sql);
        if($re['mx']) {
            return $re['mx'] + 1;
        }
        return $group_id_pre + 1;
    }

    /**
     * 分组入库
     * @param $kfCenter
     * @param $list
     * @param int $lim
     */
    public static function insertKfGroup($kfCenter,$list,$lim = 100){
        $time = time();
        
        if(is_array($list)&&$list){
            $i = $j = 0;
            $insert_pre = "insert into base_kf_group (`act_id`,`group_id`,`server_id`,`ctime`) values";
            $tmp = array();
            foreach ($list as $v) {
                if($i >= $lim){
                    $j++;
                    $i = 0;
                }
                $tmp[$j][] = "('{$v['act_id']}','{$v['group_id']}','{$v['server_id']}','{$time}')";
                $i++;
            }

            foreach ($tmp as $v) {
                $insert_sql = $insert_pre . implode(',',$v);
                self::get_super_kf_db($kfCenter)->query($insert_sql);
            }
        }
    }

    /**
     * 获取超级跨服下的分组
     * @param $kfCenter
     * @param $where
     * @return array
     */
    public static function get_super_kf_group_list($kfCenter,$where){
        $sql = "select * from base_kf_group where {$where} order by group_id,server_id";
        return Kf::get_super_kf_db($kfCenter)->fetchRow($sql);
    }

    /**
     * 检查未满分组 返回此分组ID
     * @param $kfCenter
     * @param $actId
     * @param $groupNum
     * @return int
     */
    public static function check_not_full_group_num($kfCenter,$actId,$groupNum){
        $sql = "select group_id,count(1) as c from base_kf_group where `step` = 1 and act_id= {$actId} group by group_id having c < {$groupNum}";
        $re = self::get_super_kf_db($kfCenter)->fetchOne($sql);
        if($re){
            return $re['group_id'];
        }else{
            return self::get_max_group_num($kfCenter);
        }
    }

    /**
     * 检查已加入分组的游戏服
     * @param $kfCenter
     * @param $actId
     * @param $sids
     */
    public static function check_in_group($kfCenter,$actId){
        $sql = "select server_id from base_kf_group where act_id= {$actId}";
        $re = self::get_super_kf_db($kfCenter)->fetchRow($sql);
        $sids = array();
        if($re){
            foreach ($re as $v) {
                $sids[] = $v['server_id'];
            }
        }

        return $sids;
    }

    /*
      * 获取跨服中心节点
      */
    public static function kf_node_by($gid){
        $sql = "select kf_center from adminplatformlist where gid = {$gid} limit 1";
        $kfCenter = Config::admindb()->fetchOne($sql);
        if(!$kfCenter || empty($kfCenter['kf_center'])){
            return false;
        }
        return $kfCenter['kf_center'];
    }

    /**
     *
     * 获取跨服中心下所有分组
     * @param $gid
     * @param $type //类型1-系统 2-日常活动 3-运营活动
     * @param $actId //假的ID
     * @param string $like
     * @return array
     */
    public static function kf_group_by($gid,$type,$actId,$like = ''){
        $actId = self::get_act_map($type,$actId);
        if(!$actId){
            return array();
        }
        $kfCenter = self::kf_node_by($gid);
        if(!$kfCenter){
            return array();
        }
        $where = '1';
        if($actId){
            $where .= " and act_id = {$actId}";
        }
        
        $sql = "select group_id,server_id,`server_name` from base_kf_group where {$where} order by group_id asc";
        $re = self::get_super_kf_db($kfCenter)->fetchRow($sql);
        if(!$re){
            return array();
        }

        return $re;
    }


    /**
     * 排序
     * @param $list
     * @param string $type
     */
    public static function k_sort($list,$type = 'otime'){
        if($type == 'otime'){
            $sort = array();
            $ids = array();
            foreach ($list as $v) {
                $sort[] = $v['otime'];
                $ids[] = $v['id'];
            }
            array_multisort($sort, SORT_ASC, $ids, SORT_ASC, $list);
        }elseif($type == 'power'){
            $sort = array();
            $ids = array();
            foreach ($list as $v) {
                $sort[] = $v['power'];
                $ids[] = $v['id'];
            }
            array_multisort($sort, SORT_DESC, $ids, SORT_ASC, $list);
        }

        return $list;
    }
}