<?php

/**
 * Created by PhpStorm.
 * User: zwj
 * Date: 19/02/13
 * Time: 上午11:18
 */
class Mail
{
     /**
     * 指定玩家邮件
     * @param $gid int
     * @param $sid int
     * @param $lv str
     * @return int
     */
     public static function check_audit($type){
        $code = false;
        if ($_SESSION['if_mail'] != 1 || $_POST['time_type']) {
            if ($_POST['time_type']) {
                $type = $type+10;
                if ($_POST['timing']) {
                    $timing = strtotime($_POST['timing']);
                    if ($timing <= time()) { echo "请输入发送时间"; exit; }
                } else { echo "请输入发送时间";exit; }
            }
        } else {
            $code = true;
        }
        return array($code, $type, $timing);
     }

    /**
     * 单服邮件
     * @param $gid int
     * @param $sid int
     * @param $lv str
     * @param $id_list str
     * @param $title str
     * @param $content str
     * @param $post str
     * @param $source str
     * @param $timing int
     * @return int
     */
    public static function send_mail_sid($type, $gid, $sid, $lv, $id_list, $title, $content, $post, $source = '', $timing = 0)
    {   
        $time = time();
        $sql = "INSERT INTO audit_mail (
                        type,
                        gid,
                        sid,
                        source,
                        lv,
                        id_list,
                        title,
                        content,
                        post,
                        submiter,
                        submitimte,
                        timing
                    )
                    VALUES
                        (
                            '$type',
                            '$gid',
                            '$sid',
                            '$source',
                            '$lv',
                            '$id_list',
                            '$title',
                            '$content',
                            '$post',
                            '{$_SESSION['username']}',
                            '$time',
                            '$timing'
                        )";       
        $admindb = Ext_Mysql::getInstance('admin');                
        $return = $admindb->query($sql);

        if( $type===0 || $type == 10) $code = "单服邮件:申请指定玩家发邮件。";
        if( $type===1 || $type == 11) $code = "单服邮件:申请给全部玩家发邮件。";
        if( $type===2 || $type == 12) $code = "单服邮件:申请给在线玩家发邮件。";
        Admin::adminLog($code, 1);

        return $return;
    }

    /**
     * 专服邮件
     * @param $gid int
     * @param $sid_list str
     * @param $lv str
     * @param $title str
     * @param $content str
     * @param $post str
     * @param $source str
     * @param $timing int
     * @return int
     */
    public static function send_mail_gid($type, $gid, $sid_list, $lv, $title, $content, $post, $source = '', $timing = 0)
    {   
        $time = time();
        $sql = "INSERT INTO audit_mail (
                        type,
                        gid,
                        sid_list,
                        source,
                        lv,
                        title,
                        content,
                        post,
                        submiter,
                        submitimte,
                        timing
                    )
                    VALUES
                        (
                            '$type',
                            '$gid',
                            '$sid_list',
                            '$source',
                            '$lv',
                            '$title',
                            '$content',
                            '$post',
                            '{$_SESSION['username']}',
                            '$time',
                            '$timing'
                        )";       
        $admindb = Ext_Mysql::getInstance('admin');                
        $return = $admindb->query($sql);

        if( $type===3 || $type == 13) $code = "专服邮件:全部玩家。";
        if( $type===4 || $type == 14) $code = "专服邮件:在线玩家";
        
        Admin::adminLog($code, 1);

        return $return;
    }

    

}