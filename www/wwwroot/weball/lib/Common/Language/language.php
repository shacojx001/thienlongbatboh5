<?php
/**
 * Created by PhpStorm.
 * User: Jc
 * Date: 2019/2/22
 * Time: 15:58
 */
class language{

    private static $config = array();
    private $language = array();

    public function __construct($content='',$language){
        if( empty($content) || empty($language) ){
            return false;
        }
        if( empty(self::$config[$language]) ){
            $file_config = dirname(__FILE__).DIRECTORY_SEPARATOR.'pack'.DIRECTORY_SEPARATOR.$language.'.php';
            if( file_exists($file_config) ){
                self::$config[$language] = include($file_config);
            }
        }

        if( empty(self::$config[$language]) ){
            return false;
        }
        $this->language = self::$config[$language];
    }


    public static function getLang($content='',$language){
        if( empty(self::$config[$language]) ){
            self::$config[$language] = new self($content,$language);
        }
        if( empty(self::$config[$language]) ){
            return $content;
        }
        if( empty(self::$config[$language]->language) ){
            return $content;
        }
        if( empty(self::$config[$language]->language[$content]) ){
            return $content;
        }
        return self::$config[$language]->language[$content];
    }

}