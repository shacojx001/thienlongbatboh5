<?php

$all_server_bysid = array (
  1 => 'tlry_s1',
);

$server_list_info = array (
  1 => 
  array (
    'id' => '1',
    'gid' => '10000',
    'sname' => 'tl_s10001',
    'description' => 'Kieu Phong',
    'sdb' => 'tlry_s1',
    'db_host' => '127.0.0.1',
    'db_user' => 'root',
    'db_pwd' => '123456',
    'url' => 'http://s10001.gznemo.chuxinhd.com/',
    'default' => '1',
    'otime' => '1581213600',
    'db_port' => '3306',
    'srv_num' => '0',
    'hot_state' => '0',
    'recom_state' => '1',
    'game_ip' => '117.4.244.81',
    'game_port' => '9000',
    'channel_list' => 'vtnemo,chuxin',
    'stop_service_describe' => '',
    'visible_time' => '1579795200',
    'main_id' => '0',
    'state' => '0',
  ),
  2 => 
  array (
    'id' => '2',
    'gid' => '10000',
    'sname' => 'tl_s10002',
    'description' => 'Chua Ra',
    'sdb' => 'tlry_s2',
    'db_host' => '127.0.0.1',
    'db_user' => 'root',
    'db_pwd' => '123456',
    'url' => 'http://s10002.gznemo.chuxinhd.com/',
    'default' => '1',
    'otime' => '1581323219',
    'db_port' => '3306',
    'srv_num' => '0',
    'hot_state' => '0',
    'recom_state' => '0',
    'game_ip' => '117.4.244.81',
    'game_port' => '9001',
    'channel_list' => 'vtnemo,chuxin',
    'stop_service_describe' => '',
    'visible_time' => '1580486400',
    'main_id' => '0',
    'state' => '0',
  ),
);

$channel_list = array (
  'chuxin' => 
  array (
    'gid' => '10000',
    'channel' => 'chuxin',
    'pay_key' => '',
    'ios_version' => 
    array (
      0 => '',
    ),
    'is_pay' => '1',
    'game_id' => '111',
    'sdk_mark' => '',
  ),
  'vtnemo' => 
  array (
    'gid' => '10000',
    'channel' => 'vtnemo',
    'pay_key' => '',
    'ios_version' => 
    array (
      0 => '10',
      1 => '2.3',
      2 => '4.5',
    ),
    'is_pay' => '1',
    'game_id' => '111',
    'sdk_mark' => 'chuxin',
  ),
);

$gid_list_info = array (
  10000 => 
  array (
    'id' => '1',
    'platform_name' => '第一专服',
    'platform_str' => 'dev',
    'gid' => '10000',
    'platform_rate' => '1.0000',
    'res_url' => 'http://117.4.244.81:82/dev/',
    'version_url' => 'http://117.4.244.81:82/dev/',
    'advance_time' => '2',
    'kf_center' => 'tl_k10001100@k10001.gznemo.chuxinhd.com',
    'max_reg_num' => '0',
    'reward_rate' => '100',
    'reward_today' => '200',
  ),
);

?>