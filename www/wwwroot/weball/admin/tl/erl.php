<?php
class Erls
{
	protected $link;
	public $error = array();
	public $conn_error = array();
	public $cfg = array();
	private $node = 0;
	private static $instance;
	
	private function __construct($isExit = true, $dbgame = false, $changedb = array())
	{
		if(empty($changedb)) 
		{
			global $base_info;
			if(!$dbgame)
			{
				if(!isset($base_info['erl_cookie']) || empty($base_info['erl_cookie']) || !isset($base_info['erl_node']) || empty($base_info['erl_node']))
				{
					alert('配置错误');
				}
				$this->cfg = array(
					'cookie' => $base_info['erl_cookie'],
					'nodes'  => array(1 => $base_info['erl_node'])
				);
			}
			else
			{
				$this->cfg = array(
					'cookie' => ERLANGCOOKIE,
					'nodes'  => array(1 => ERLANGNODE)
				);
			}
		}
		else 
		{
			$this->cfg = array(
				'cookie' => $changedb['erl_cookie'] ,
				'nodes'  => array(1 => $changedb['erl_node'])
			);
		}


		if(!is_resource($this->link))
		{
			$this->connect();
            $zone = $this->tab2list("ets_node");
            if($this->getError() || peb_error() )
            {
				if($isExit)
				{
					exit;
				}
				else
				{
					return false;
				}
			}

			if(is_array($zone))
			{
                ksort($zone);
				$this->cfg['nodes'] = array(1 => ERLANGNODE);
                foreach($zone as $zval)
				{

                    if($isExit === 99 && $zval[1] ==1)
                    {
                        $this->cfg['nodes'][$zval[1]] = $zval[4];
                    }
                    else 
                    {
					    if($zval[0]!='node' || !$zval[1] || !$zval[4] || $zval[1] < 10)
					    	continue;
                        $this->cfg['nodes'][$zval[1]] = $zval[4];
                    }
                }
			}
		}
	}
	

	public static function getInstance($isExit = true,$dbgame = false,$changedb = array(),$sid = '0'){
		if($sid){
			if(!self::$instance[$sid]) {
				self::$instance[$sid] = new self($isExit,$dbgame,$changedb);
			}
			return self::$instance[$sid];
		}else{
			if(!self::$instance) {
				self::$instance = new self($isExit,$dbgame,$changedb);
			}
			return self::$instance;
		}
	}

	public function connect($node = 0, $cookie = '')
	{
		$this->close();
		$cfg = $this->cfg;
		if($node < 1)
		{
			$node = $cfg['nodes'][1];
		}
		else
		{
			$node = $cfg['nodes'][$node];
		}
        $this->node = $node = $node;
		if(!$cookie)
		{
			$cookie = $cfg['cookie'];
		}
		$this->link = peb_connect($node, $cookie, 5000);
		
		if(!$this->link)
		{
			$this->conn_error[$node] = 'could not connect['.peb_errorno().']:'  .  peb_error();
			$GLOBALS['erl_error'] .= "&nbsp;[$node]".$this->conn_error[$node];
		}
			
		return $this->link;
	}
	
	public function isActive(){
	}
	
	public function getError()
	{
		if($this->error)
		{
			$err = '';
			foreach($this->error as $node => $msg)
				$err .= "[{$node}]：{$msg}";
			
			
			return $err;
		}
		if(peb_error() == 'ei_connect error')
		{
			return '';
		}
		return peb_error();
	}
	
	public function getConnError()
	{
		if($this->conn_error)
		{
			$err = '';
			foreach($this->conn_error as $node => $msg)
				$err .= "[{$node}]：{$msg}";
				
			return $err;
		}
		return '';
	}
	
	private function rpc($M, $F, $A)
	{
		if(!is_resource($this->link))
		{
			$this->error[$this->node] = 'connect error!';
			return false;
		}
		try{
			$rpcRt = peb_rpc($M, $F, $A, $this->link);
			if(!$rpcRt)
				$rt = false;
			else{
				$rt = peb_decode($rpcRt);
			}
		}catch(Exception $e){
			$this->error[$this->node] .= $e -> getMessage();
			$rt = false;
		}
		return $rt;
	}

	public function newTable($name, $options)
	{
		$options = array_merge($options, array('named_table', 'public'));
		$x = peb_encode("[~a, [" . $this->repeatFormat('~a', $options) . "]]", array(array($name, $options)));
		return $this->rpc("ets", "new", $x);
	}

	public function insert($name, $key, $value)
	{
		$x = peb_encode("[~a, {~a, ~s}]", array(array($name, array($key, $value))));
		return $this->rpc("ets", "insert", $x);
	}

	public function bulkInsert($name, $values)
	{
		$x = peb_encode("[~a, [" . $this->repeatFormat('{~a, ~s}', count($values)) . "]]", array(array($name, $values)));
		return $this->rpc("ets", "insert", $x);
	}

	public function info($name, $item)
	{
		$x = peb_encode("[~a, ~a]", array(array($name, $item)));
		$rt = $this->rpc("ets", "info", $x);
		return isset($rt[1]) ? $rt : $rt[0];
	}

	public function getid($argk='[]', $argv=array())
	{
		$x = peb_encode($argk, $argv);
		$rt = $this->rpc('lib_role_db', 'get_role_id_by_name', $x);

		return isset($rt[1]) ? $rt : $rt[0];
	}  
  
	public function delete_all_objects($name)
	{
		$x = peb_encode("[~a]", array(array($name)));
		return $this->rpc("ets", "delete_all_objects", $x);
	}

	public function tab2list($name)
	{
		$x = peb_encode("[~a]", array(array($name)));
		$rt = $this->rpc("ets", "tab2list", $x);
		return isset($rt[1]) ? $rt : $rt[0];
	}
	
	public function get($model, $func, $argk='[]', $argv=array())
	{
		$x = peb_encode($argk, $argv);
		$rt = $this->rpc($model, $func, $x);

		return isset($rt[1]) ? $rt : $rt[0];
	}
	
	public function mails($argk='[]', $argv=array())
	{
		$x = peb_encode($argk, $argv);
		$rt = $this->rpc("lib_gm", "sys2select", $x);

		return isset($rt[1]) ? $rt : $rt[0];
	}
  
	public function getAll($model, $func, $argk='[]', $argv=array())
	{
		$rt = array();
		foreach($this->cfg['nodes'] as $k => $v)
		{
            $connect_result = $this->connect($k);
            $connect_error = peb_error();
			if($connect_result && !$connect_error)
			{
				$rs = $this->get($model, $func, $argk, $argv);
				$rt[$k] = $rs;
			}
		}
		return $rt;
	}

	public function repeatFormat($format, $len)
	{
		if(is_array($len)){
			$len = count($len);
		}
		if($len < 1)return '';
		return implode(', ', array_fill(0, $len, $format));
	}
	
	public function close()
	{
		if(is_resource($this->link)) return peb_close($this->link);
	}
	
	public function __destruct()
	{
		return $this->close();
	}
}

