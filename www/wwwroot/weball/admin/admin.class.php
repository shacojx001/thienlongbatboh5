<?php
/**
 * 后台
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_admin extends action_superclass
{
    static $sname_by_sids_data = array();

    public function server_list()
    {
        $this->db = Ext_Mysql::getInstance('admin');
        $where = " WHERE 1 ";

        $start_time = $_GET['start_time'];
        $end_time = $_GET['end_time'];
        if (!empty($start_time)) {
            $starttime = strtotime($start_time) - 86400;
            $where .= " AND a.time >= '$starttime' ";
        }
        if (!empty($end_time)) {
            $endtime = strtotime($end_time . "23:59:59");
            $where .= " AND a.time <= '$endtime' ";
        }

        $sql = "select gid,platform_name from adminplatformlist order by id desc";
        $presult = $this->admindb->fetchRow($sql);
        if($presult){
            foreach ($presult as $v) {
                $glist[$v['gid']] = $v['platform_name'];
            }
        }

        $orderby = " order by gid asc,id desc ";
        $sql = "select a.*,b.platform_name from adminserverlist a left join adminplatformlist b on a.gid = b.gid $where $orderby";

        $res = $this->db->fetchRow($sql);
        $list = array();
        if (!empty($res)) {
            foreach ($res as $key => $val) {
                $list[$val['gid']]['platform_name'] = $val['platform_name'] ;
                $list[$val['gid']]['slist'][] = $val;
            }
        }
        foreach ($list as $key => $val) {
            foreach ($val['slist'] as $k => $v) {
                if ($v['visible_time']) {
                    $list[$key]['slist'][$k]['visible_time'] = date('Y-m-d H:i:s', $v['visible_time']);
                }
                if ($v['repair_ctime']) {
                    $list[$key]['slist'][$k]['repair_ctime'] = date('Y-m-d H:i:s', $v['repair_ctime']);
                }
                if ($v['repair_etime']) {
                    $list[$key]['slist'][$k]['repair_etime'] = date('Y-m-d H:i:s', $v['repair_etime']);
                }
                if ($v['otime']) {
                    $list[$key]['slist'][$k]['old_otime'] = $v['otime'];
                    $list[$key]['slist'][$k]['otime'] = date('Y-m-d H:i:s', $v['otime']);
                }

            }
        }

        $assign['status'] = array(0 => '测试服', 1 => '正式服', 2 => '计划开服', 3 => '计划封测', 4 => '维护中', 5 => '关服', 6 => '合服', 7 => '装服完成');
        $assign['color'] = array(0 => '#ffba00', 1 => '#0066ff', 2 => '#8E00D2', 3 => '#00ff00', 4 => '#aaE100', 7 => '#FF0000');
        $assign['hot_state'] = array(0 => '火爆', 1 => '繁忙', 3 => '空闲');
        $assign['recom_state'] = array(0 => '否', 1 => '是');
        $assign['glist'] = $glist;
        $assign['list'] = $list;
        $assign['time'] = time();

        $this->display('admin_server_list.shtml', $assign);
    }

    public function server_list_action()
    {   
        global $system_config;
        //接收参数、过滤参数
        $gid = request('gid','int');
        $sname = trim($_POST['sname']);
        $description = trim($_POST['description']);
        $sdb = trim($_POST['sdb']);
        $db_port = intval($_POST['db_port']);
        $s_num = intval(trim($_POST['s_num']));
        $url = trim($_POST['url']);
        $otime = strtotime($_POST['otime']);
        $visible_time = strtotime($_POST['visible_time']);//可视时间
        $hot_state = trim($_POST['hot_state']);
        $recom_state = trim($_POST['recom_state']);
        $repair_ctime = strtotime($_POST['repair_ctime']);
        $repair_etime = strtotime($_POST['repair_etime']);
        $game_ip = trim($_POST['game_ip']);
        $game_port = trim($_POST['game_port']);
        $channel_list = implode(",", $_POST['channel']);
        $stop_service_describe = trim($_POST['stop_service_describe']);
        $db_host = trim($_POST['db_host']);

        if (empty($gid)) alert('平台不能为空');
        if(!$s_num){
            alert('服ID必须填写!');
        }
        //可视时间不能小于装服时间
        if ($visible_time >= $otime) {
            alert("可视时间不能小于装服时间!");
        }

        //重启标识
        $restart_flag = false;
        $old_otime = strtotime(request('old_otime'));

        $default = $_POST['def'];
        $this->db = Ext_Mysql::getInstance('admin');
        $id = intval($_GET['id']);
        if ($id > 0) {
            Auth::check_action(Auth::$SERVER_EDIT);
            $sql = "update adminserverlist set `gid`='$gid',`sname`='$sname',`s_num`='$s_num',`description`='$description',`sdb`='$sdb',`db_port`='$db_port',`url`='$url',`otime`='$otime',`default`='$default',`hot_state`='$hot_state',`recom_state`='$recom_state',`repair_ctime`='$repair_ctime',`repair_etime`='$repair_etime',`game_ip`='$game_ip',`game_port`='$game_port',`channel_list`='$channel_list',`stop_service_describe`='$stop_service_describe',`visible_time`='$visible_time',`db_host`='$db_host' where id = '$id'";
            //修改从服的IP、端口、及状态
            if($default==1){//合服状态
               $sql_main = "update adminserverlist set `game_ip`='$game_ip',`game_port`='$game_port',`default`=6 where main_id = '$id'";
               $this->db->query($sql_main);
            }elseif($default==4){//维护状态
               $sql_main = "update adminserverlist set `game_ip`='$game_ip',`game_port`='$game_port',`default`=4 where main_id = '$id'";
               $this->db->query($sql_main);
            }

            //判断是否自动重启游戏服
            if($old_otime > time() && $old_otime != $otime){
                //重启
                $restart_flag = true;

            }
        } else {
            Auth::check_action(Auth::$SERVER_ADD);
            $sql = "select count(1) as c from adminserverlist where id = {$s_num}";
            $cnt = $this->db->fetchOne($sql);
            if(!empty($cnt['c'])){
                alert('服ID已经存在! '.$s_num);
            }
            $sql = "insert into adminserverlist (`id`,`gid`,`sname`,`s_num`,`description`,`sdb`,`db_port`,`url`,`otime`,`default`,`hot_state`,`recom_state`,`repair_ctime`,`repair_etime`,`game_ip`,`game_port`,`channel_list`,`stop_service_describe`,`visible_time`,`db_host`)values('$s_num','$gid','$sname','$s_num','$description','$sdb','$db_port','$url','$otime','$default','$hot_state','$recom_state','$repair_ctime','$repair_etime','$game_ip','$game_port','$channel_list','$stop_service_describe','$visible_time','$db_host')";
        }
        $this->db->query($sql);
        // 查询修改的数据库，修改相应游戏服开服时间/跨服中心
        $sql = "SELECT
                        s.id,
                        p.platform_str,
                        from_unixtime(otime, '%Y-%m-%d-%H-%i-%S') AS version,
                        main_id
                      FROM
                        adminserverlist s
                      LEFT JOIN adminplatformlist p ON s.gid = p.gid
                      WHERE
                        s.id = '$s_num'";
        $res = $this->db->fetchOne($sql);
        if( !empty($res) && $res['main_id']=='0' ){
            $push_url = $system_config['push_url']?$system_config['push_url']:'';
            $sql = "REPLACE INTO base_game (cf_name, cf_value)
                      VALUES
                        ('channel_list','$channel_list'),
                        ('game_domain', '$url'),
                        ('game_title', '$description'),
                        ('gid', '$gid'),
                        ('sid', '{$res['id']}'),
                        ('sdb', '$sdb'),
                        ('ip', '$game_ip'),
                        ('port', '$game_port'),
                        ('version', '{$res['version']}'),
                        ('push_url','{$push_url}')";

            Config::gamedb($res['id'])->query($sql);
        }

        //加入跨服
        Config::kf_config($res['id']);

        //重启
        if($restart_flag){
            $otime_res = Erlang::set_open_time($res['id'], $otime);
            // print_r($otime_res);exit;
            Admin::adminLog('保存游戏服修改了开服时间自动重启服!'.$otime_res);
        }

        Admin::adminLog("编辑游戏服:{$s_num}|{$_POST['otime']}|{$description}|{$sdb}|{$db_host}|{$db_port}|{$game_ip}|{$game_port}|{$default}");

        alert('操作成功!请记得填写生成配置！！！！！！'.$otime_res );
    }

    public function handle_server_base_info_error(){
        $sql = "SELECT
                        id,
                        from_unixtime(otime, '%Y-%m-%d-%H-%i-%S') AS version,
                        main_id,
                        description
                      FROM
                        adminserverlist
                      WHERE
                        `default` in (1,4,6)";
        $res = $this->admindb->fetchRow($sql);

        if( !empty($res) ){
            $res_key = array();
            foreach( $res as $k=>$v ){
                $res_key[$v['id']] = $v;
            }


            foreach( $res as $k=>$v ){
                if( $v['main_id']=='0' ){

                    $sql = "update base_game set cf_value='".$v['description']."' where cf_name='game_title' ";
                    Config::gamedb($v['id'])->query($sql);
                    $sql = "update base_game set cf_value='".$v['id']."' where cf_name='sid' ";
                    Config::gamedb($v['id'])->query($sql);

                }elseif( $v['main_id']>0 ){
                    if( isset($res_key[$v['main_id']]) ){

                        $sql = "update base_game set cf_value='".$res_key[$v['main_id']]['description']."' where cf_name='game_title' ";
                        Config::gamedb($v['id'])->query($sql);
                        $sql = "update base_game set cf_value='".$res_key[$v['main_id']]['id']."' where cf_name='sid' ";
                        Config::gamedb($v['id'])->query($sql);

                    }

                }
            }

        }
        echo 'ok';
    }

    public function create_server_config_wx()//武侠游戏配置
    {
        Admin::adminLog("生成配置");
        Auth::check_action(Auth::$SERVER_CONFIG);
        Config::config_wx();
    }



    //审核服配置
    public function sh_server_config_list(){
        $data = $this->sh_server_config_get();
        if(!empty($data['sh_server'])){
            foreach( $data['sh_server'] as $k=>$v ){
                foreach( $v as $k1=>$v1 ){
                    if( strpos($k1,'time')!==false ){
                        $data['sh_server'][$k][$k1] = date('Y-m-d H:i:s',$v1);
                    }
                }
            }
        }
        $info = $this->sh_server_base_field();
        $assign['data'] = json_encode($data);
        $assign['info'] = json_encode($info);
//        echo $assign['data'];die;
        $this->display('sh_server_config_list.shtml', $assign);
    }
    public function sh_server_config_add(){
        $params = $_POST;
        if(empty($params)){
            exit(json_encode(array('code'=>50001,'msg'=>'参数错误')));
        }
        $config_field = $this->sh_server_base_field();
        $info = array();
        foreach($config_field as $k=>$v){
            if(!isset($params[$k])){
                exit(json_encode(array('code'=>50001,'msg'=>'参数错误')));
            }else{
                if( strpos($k,'time')!==false ){
                    $info[$k] = strval(strtotime($params[$k]));
                }else{
                    $info[$k] = $params[$k];
                }
            }
        }
        $data = $this->sh_server_config_get();
        if(!empty($params['now_server_id'])){
            unset($data['sh_server'][$params['now_server_id']]);
            $data['ver'] = $this->sh_server_del_ver_by_s( $data['ver'] , $params['now_server_id'] );
        }
        if(empty($data['sh_server'])){
            $data['sh_server'] = array($info['server_id']=>$info);
        }else{
            $data['sh_server'][$info['server_id']] = $info;
        }
        $ste = $this->sh_server_config_create($data);
        if(!$ste){
            exit(json_encode(array('code'=>50002,'msg'=>'失败，生成配置失败')));
        }
        exit(json_encode(array('code'=>200,'msg'=>'保存成功')));
    }
    public function sh_server_config_add_ver(){
        $params = $_POST;
        if( empty($params) || empty($params['sh_server_select2']) || empty($params['name_ver']) ){
            exit(json_encode(array('code'=>50001,'msg'=>'参数错误')));
        }
        $data = $this->sh_server_config_get();
        if(empty($data['ver'])){
            $data['ver'] = array($params['name_ver']=>$params['sh_server_select2']);
        }else{
            $data['ver'][$params['name_ver']] = $params['sh_server_select2'];
        }
        $ste = $this->sh_server_config_create($data);
        if(!$ste){
            exit(json_encode(array('code'=>50002,'msg'=>'失败，生成配置失败')));
        }
        exit(json_encode(array('code'=>200,'msg'=>'保存成功')));
    }
    public function sh_server_config_del_s(){
        $params = $_POST;
        if( empty($params) || empty($params['server_id']) ){
            exit(json_encode(array('code'=>50001,'msg'=>'参数错误')));
        }
        $data = $this->sh_server_config_get();
        if(!empty($data['sh_server'])){
            unset($data['sh_server'][$params['server_id']]);
            $data['ver'] = $this->sh_server_del_ver_by_s( $data['ver'] , $params['server_id'] );
        }else{
            exit(json_encode(array('code'=>200,'msg'=>'删除成功')));
        }
        $ste = $this->sh_server_config_create($data);
        if(!$ste){
            exit(json_encode(array('code'=>50002,'msg'=>'失败，生成配置失败')));
        }
        exit(json_encode(array('code'=>200,'msg'=>'删除成功')));
    }
    public function sh_server_config_del_ver(){
        $params = $_POST;
        if( empty($params) || empty($params['ver']) ){
            exit(json_encode(array('code'=>50001,'msg'=>'参数错误')));
        }
        $data = $this->sh_server_config_get();
        if(!empty($data['ver'])){
            unset($data['ver'][$params['ver']]);
        }else{
            exit(json_encode(array('code'=>200,'msg'=>'删除成功')));
        }
        $ste = $this->sh_server_config_create($data);
        if(!$ste){
            exit(json_encode(array('code'=>50002,'msg'=>'失败，生成配置失败')));
        }
        exit(json_encode(array('code'=>200,'msg'=>'删除成功')));
    }
    protected function sh_server_del_ver_by_s($ver,$del_server_id){
        $info = array();
        foreach($ver as $k=>$v){
            if( $del_server_id!=$v ){
                $info[$k] = $v;
            }
        }
        return $info;
    }
    protected function sh_server_base_field(){
        $config_field = array(
            "server_id" => "服ID",
            "description" => "服描述",
            "game_ip" => "游戏服IP",
            "game_port" => "游戏服端口",
            "otime" => "开服时间",
            "visible_time" => "可视时间",
            "hot_state" => "火爆状态,0火爆,1繁忙,3空闲",
            "recom_state" => "推荐状态,0否,1是",
            "default" => "服状态,0测试服,1正式服,2计划开服,3计划封测,4维护中,5关服,6合服",
            "repair_ctime" => "维护开始时间",
            "repair_etime" => "维护结束时间",
            "stop_service_describe" => "停服描述",
            "shenhe" => "是否审核服，1是");
        return $config_field;
    }
    protected function sh_server_config_create($data){
        $info = "<?php \r\n return ";
        $info .= var_export($data,true);
        $info .= ';';
        $path = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'sh_server_config.php';
        return file_put_contents($path,$info);
    }
    protected function sh_server_config_get(){
        $path = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'sh_server_config.php';
        if( !file_exists($path) ){
            return array();
        }
        $data = require $path;
        return $data;
    }


    public function export()
    {
        $sql = "select * from merged_suggest";
        $list = Config::centerdb()->fetchRow($sql);
        if (!empty($list)) {
            foreach ($list as $key => $val) {
                if (!empty($val['data'])) {
                    $val['data'] = json_decode($val['data'], true);
                    if (is_array($val['data'])) {
                        foreach ($val['data'] as $k => $v) {
                            $list[$key][$k] = $v;
                        }
                    }
                }
            }
        }
        $glist = $this->get_allgroup_list();
        $slist = $this->get_allserver_list_open();

        require_once("Classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        foreach ($list as $key => $val) {
            foreach ($val as $v) {
                $val['pname'] = $glist[$val['gid']];
                $val['sname'] = $slist[$val['sid']];
                $val['time'] = date("Y-m-d H:i:s", $val['otime']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $key + 1, $val['pname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $key + 1, $val['sname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $key + 1, $val['time']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $key + 1, $val['tavg0']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $key + 1, $val['tavg1']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 1, $val['tavg2']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $key + 1, $val['tavg3']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $key + 1, $val['tavg4']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $key + 1, $val['tavg5']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, $key + 1, $val['tavg6']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, $key + 1, $val['tavg7']);
            }
        }
        $objPHPExcel->getActiveSheet()->setTitle('合服建议');
        $excelName = 'merged_' . date("YmdHis") . '.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $excelName);
        header('Cache-Control: max-age=0');
        header("Pragma: no-cache");
        $objWriter->save('php://output');
    }


    public function platform_list()
    {
        $this->db = Ext_Mysql::getInstance('admin');

        $gid_list = $this->get_allgroup_list();
        $where = $this->get_where_g(0,$gid_list);
        $sql = "select * from adminplatformlist where 1 $where order by gid";
        $res = $this->db->fetchRow($sql);
        $list = array();
        if (!empty($res)) {
            foreach ($res as $key => $val) {
                $list[$key] = $val;
            }
        }

        //跨服中心
        $sql = "select node as node_name from kf_server";
        $kf_center_list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['kf_center_list'] = $kf_center_list;
        $this->display('admin_platform_list.shtml', $assign);
    }

    public function del_platform()
    {
        Auth::check_action(Auth::$PLATFORM_DEL);
        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) {
            alert('参数错误', '');
        }

        $this->db = Ext_Mysql::getInstance('admin');
        $info = $this->db->fetchOne("select * from adminplatformlist where id ={$id}");
        if(!$info){
            alert('专服不存在','');
        }
        $this->db->query("delete from adminplatformlist where id={$id}");
        Admin::adminLog("删除专服:{$info['platfrom_name']}({$id})");
        alert('删除成功', '');
    }


    public function platform_list_action()
    {
        $platform_id = request('platform_id','int');
        $platform_name = request('platform_name','str');
        $platform_gid = request('platform_str','int');
        $platform_rate = request('platform_rate','str');
        $advance_time = request('advance_time','int');
        //$kf_center = request('kf_center');
        $res_url = request('res_url');
        $version_url = request('version_url');
        $max_reg_num = request('max_reg_num','int');
        $this->db = Ext_Mysql::getInstance('admin');
        if (empty($platform_id)) {
            //检查合法性
            if(($platform_gid%10000)!=0){
                alert('平台代码不合符!');
            }
            //检查是否存在
            $sql = "select count(1) as t from adminplatformlist where gid = {$platform_gid} limit 1";
            $re = $this->admindb->fetchOne($sql);
            if($re['t']){
                alert('此平台代码已经存在!');
            }

            Admin::adminLog("添加专服:$platform_name($platform_gid)");
            Auth::check_action(Auth::$PLATFORM_ADD);
            $sql = "insert into adminplatformlist set platform_name='{$platform_name}', platform_rate='{$platform_rate}', gid={$platform_gid}, res_url='{$res_url}', version_url='{$version_url}', advance_time='{$advance_time}',max_reg_num='{$max_reg_num}'";

            $this->db->query($sql);
            $id = $this->db->getLastInsertId();
            Admin::adminLog("添加专服:{$platform_name}({$id})");
        } else {
            Admin::adminLog("修改专服:$platform_name($platform_gid)");
            Auth::check_action(Auth::$PLATFORM_EDIT);
            $sql = "update adminplatformlist set platform_name='{$platform_name}', platform_rate='{$platform_rate}', res_url='{$res_url}', version_url='{$version_url}', advance_time='{$advance_time}',max_reg_num='{$max_reg_num}' where id={$platform_id}";
            $this->db->query($sql);
            Admin::adminLog("修改专服:{$platform_name}({$platform_id})");
        }

        alert('操作成功');

    }

    public function channel_list()
    {
        $uid_gid = empty($_SESSION['gids'])?'':$_SESSION['gids'];

        $channel_list = $this->get_channel_list();
        $where = $this->get_where_c(0,$channel_list,'channel_str');
        //渠道列表
        $sql = "select * from channel_list where 1 $where";
        $slist = $this->admindb->fetchRow($sql);

        //专区列表
        $gid_list = $this->get_allgroup_list();
        $where = $this->get_where_g(0,$gid_list);

        $where_group = '';
        if(!empty($uid_gid)){
            $where_group = ' and gid in ('.(implode(',',$uid_gid)).')';
        }
        $sql = "select gid as id,platform_name from adminplatformlist where 1 $where_group";
        $glist = $this->admindb->fetchRow($sql);

        foreach ($glist as $key => $val) {
            $res[$val['id']] = $val['platform_name'];
        }

        $assign['slist'] = $slist;
        $assign['glist'] = $glist;
        $assign['res'] = $res;
        $this->display('admin_channel_list.shtml', $assign);

    }

    public function channel_list_add()
    {
        $id = $_REQUEST['id'];
        $gid = $_POST['gid'];
        $channel_name = trim($_POST['channel_name']);
        $channel_str = trim($_POST['channel_str']);
        $pay_key = trim($_POST['pay_key']);
        $sdk_mark = trim($_POST['sdk_mark']);
        $ios_version = trim($_POST['ios_version']);
        $is_pay = $_POST['is_pay'];
        $game_id = trim($_POST['game_id']);

        if($ios_version){
            if(strpos($ios_version,'，') !== false){ 
                alert("不可有中文字符，");
            }
        }
        if (empty($id)) {
            Admin::adminLog("添加渠道:$channel_str");
            Auth::check_action(Auth::$CHANNEL_ADD);
            $sql = "insert into channel_list(`gid`,`channel_name`,`channel_str`,`pay_key`,`ios_version`,`is_pay`,`game_id`,`sdk_mark`) values('$gid','$channel_name','$channel_str','$pay_key','$ios_version','$is_pay','$game_id','$sdk_mark')";
            $this->admindb->query($sql);
        } elseif ($id && $gid && $channel_name && $channel_str) {
            Admin::adminLog("编辑渠道:$channel_str");
            Auth::check_action(Auth::$CHANNEL_EDIT);
            $sql = "update channel_list set gid='$gid',channel_name='$channel_name',channel_str='$channel_str',pay_key='$pay_key',ios_version='$ios_version',is_pay='$is_pay',game_id='$game_id',sdk_mark='$sdk_mark' where id=$id";
            $this->admindb->query($sql);
        } elseif ($id && empty($gid) && empty($channel_name) && empty($channel_str)) {
            Admin::adminLog("删除渠道:$channel_str");
            Auth::check_action(Auth::$CHANNEL_DEL);
            $sql = "delete from channel_list where id='$id' ";
            $this->admindb->query($sql);
        }

        alert("更新成功！");
        exit;

    }

    public function zone_list()
    {
        $list = array();
        $channel = request('channel');
        $addDiv = "display:none";
        $addDiv2 = "display:none";
        $changeType_1 = $changeType_2 = 1;
        if($channel){
            $server_list = $this->channel_allserver_list($channel);
          
            $admin_db = Ext_Mysql::getInstance('admin');
            $sql = "select * from adminzonelist where channel='{$channel}'";
            $list = $this->admindb->fetchOne($sql);
            
            if($list['data']){
                $list['data'] = json_decode($list['data'],true);
                $list['typeList'] = array();   
                foreach ($list['data'] as $key => $val) {
                    $list['typeList'][$key] = array(
                        'serverNum'=> is_array($val) ? "display: none;":"",  
                        'serverSelect'=> is_array($val) ? "":"display: none;", 
                        'type'=> is_array($val) ? "array":"num",       
                    );
                }
            }
            
            if($list['type']==1){
                $changeType_1 = count($list['data']);
                $addDiv = "";
            }elseif($list['type']==2){
                $addDiv2 = "";
                $changeType_2 = count($list['data']);
            }
            $zone_list = $channel!='tpl' ? $this->zone_list_show($channel, $server_list) : '';
        // print_r($zone_list);
        }
        $clist = array('tpl'=>'默认模板')+$this->get_channel_list();
 
        $assign['channel'] = $channel;
        $assign['server_list'] = $server_list;
        $assign['list'] = $list;
        $assign['clist'] = $clist;
        $assign['addDiv'] = $addDiv;
        $assign['addDiv2'] = $addDiv2;
        $assign['changeType_1'] = $changeType_1;
        $assign['changeType_2'] = $changeType_2;
        $assign['zone_list'] = $zone_list;
    // print_r($list);exit;    
        $this->display('admin_zone_list.shtml',$assign);
    }

    //分区预览效果
    public function zone_list_show($channel, $sid_list){

        $config_result = $this->zone_config_file(true);
        $config = $config_result[$channel];

        $zone_list_data = $config_result[$channel] ? $config_result[$channel] : $config_result['tpl'];
        // print_r($zone_list_data);exit;    
        $zone_list = array();
        if($zone_list_data){
            //过滤已选中结果
            $server_key_list = $this->get_zone_array($zone_list_data, $sid_list);
            // print_r($server_key_list);exit;   
            $num_key = 1;
            foreach ($zone_list_data as $key => $val) {
                if(is_array($val)) continue;
                if(count($server_key_list)==0){
                    unset($zone_list_data[$key]);
                    continue;
                }
                
                if(strstr($key, '%')){//%拆分区
                    foreach (array_chunk($server_key_list, $val) as $kk => $vv) {
                        $num_key_tk = str_replace('%', $num_key, $key);
                        $zone_list_data[$num_key_tk] = $vv;
                        $num_key = $num_key + 1;
                    }
                    unset($server_key_list);
                    unset($zone_list_data[$key]);
                }else{

                    $zone_list_data[$key] = array_slice($server_key_list, 0 , $val, true);
                    array_splice($server_key_list, 0, $val);
                }
                
            }
            
            //为了后台界面显示用
            
            //上颜色
            foreach ($zone_list_data as $key => $val) {
                foreach ($val as $kk => $vv) {
                    $kt = $sid_list[$vv];
                    $color = "color:#3cf33c;";
                    if( $kt['hot_state'] === '0' ){
                    
                        $color = "color:red;";
                    }
                    if( $kt['default'] == 4 ){
                        $color = "color:#FFFF;";
                    }
                    $zone_list_data[$key][$kk] = array('id'=>$vv, 'color'=>$color);
                }
                
                
            }
            foreach ($zone_list_data as $key => $val) {
                $zone_list_data[$key] = array_chunk($val, 2);
            }
            //为了后台界面显示用
            $zone_list = $zone_list_data;   
          // print_r($zone_list_data);exit;  
        }
        return $zone_list;
    }

    public function zone_list_action(){
        Auth::check_action(Auth::$ZONE_MAN);

        $channel = $_REQUEST['channel'];
        $name = $_REQUEST['name'];
        $num = $_REQUEST['num'];
        $type = $_REQUEST['changeType'];
        $sidList = $_REQUEST['sidList'];
        $changeServer = $_REQUEST['changeServer'];

        if(empty($channel)){
            alert('请填好参数');
        }
        if(empty($name)){
            alert('请填好参数');
        }
        if(empty($num)){
            alert('请填好参数');
        }
        $data = array();
        foreach ($name as $key => $val) {
            $data[$val] = $changeServer[$key] == 1 ? $sidList[$key] : $num[$key];
        }
 // print_r($changeServer);       

        $data = addslashes(json_encode($data));
        Admin::adminLog('修改分区:'.$channel);
        $sql = "replace into adminzonelist(channel,type,data)VALUES('{$channel}',$type,'{$data}');";
 
        $admin_db = Ext_Mysql::getInstance('admin');
        $re = $admin_db->query($sql);
        if($re){
            alert('操作成功',"/action_gateway.php?ctl=admin&act=zone_list&channel=".$channel);
        }
        alert('操作失败');
    }

    public function del_zone(){
        Auth::check_action(Auth::$ZONE_MAN);
        $channel = request('channel');
        if(!$channel){
            alert('操作失败');
        }
        $admin_db = Ext_Mysql::getInstance('admin');
        $sql = "delete from adminzonelist where channel = '{$channel}'";
        $re = $admin_db->query($sql);
        if($re){
            Admin::adminLog('删除分区:'.$channel);
            alert('操作成功');
        }
        alert('操作失败');
    }

    public function zone_config_file($use = false){
        Auth::check_action(Auth::$ZONE_CONFIG);
        $list = array();
        $admin_db = Ext_Mysql::getInstance('admin');
        $sql = "select * from adminzonelist";
        $re = $admin_db->fetchRow($sql);       
        $list = array();
        if($re){
            foreach($re as $v){
                $data = json_decode($v['data'],true);
                foreach ($data as $key => $val) {
                    if(is_array($val)) {
                        foreach ($val as $kk => $vv) {
                            $data[$key][$kk] = intval($vv);
                        }
                        
                    }
                }
                $list[$v['channel']] = $data;  
            }
            if($use == true) return $list;
        }       
 // print_r($list);exit;   
        $zone_file = CONFIG_DIR.'zone.cfg.php';
        $cont = "<?php\r\n/** 此文件系统自动生成 ".date('Y-m-d H:i:s')." **/\r\nreturn ".var_export($list,true).";";
        $result = file_put_contents($zone_file,$cont);
        if($result){
            Admin::adminLog('生成分区配置文件');
            alert('生成完成！');
        }else{
            alert('生成失败！');
        }
    }

    //抽出分组里边的数组
    public function get_zone_array($zoneData, $sid_list){
        $filter_array = array();
        foreach ($zoneData as $key => $val) {
            if(is_array($val)){
                $filter_array = array_merge($filter_array,$val);
            }
            
        }
        if(count($filter_array)>0){
            foreach ($filter_array as $key => $val) {
                unset($sid_list[$val]);
            }
        }
        return array_keys($sid_list);
    }
    
    public function server_list_del(){
        Auth::check_action(Auth::$SERVER_DEL);
      	$this->db = Ext_Mysql::getInstance('admin');
      	$id = $_GET['id'];
      	if(empty($id)){
      		alert('ID异常');
      	}
      	$sql = "delete from adminserverlist where id = '$id'";
      	$this->db->query($sql);
        $log = "删除服务器列表ID:{$id}";
        Admin::adminLog($log);
      	alert('删除成功');
    }
      
    public function merged_list_del(){
      	$this->db = Ext_Mysql::getInstance('admin');
      	$id = $_GET['id'];
      	if(empty($id)){
      		alert('ID异常');
      	}
      	$sql = "delete from mergedlist where id = '$id'";
      	$this->db->query($sql);
        $log = "删除合服列表ID:{$id}";
        Admin::adminLog($log);
      	alert('删除成功');
    }


    public function new_server_limit(){
        $sql = "select channel_name,channel_str from channel_list ";
        $channel_list = $this->admindb->fetchRow($sql);
        $sql = "select * from new_server_limit";
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['channel_list'] = $channel_list;
        $this->display(CENTER_TEMP . 'new_server_limit.shtml',$assign);
    }
    public function new_server_limit_insert(){
        if(empty($_POST['channel']) || empty($_POST['limit_day'])){
            alert('有参数为空');exit;
        }
        $channel = $_POST['channel'];
        $limit_day = $_POST['limit_day'];
        $sql = "REPLACE INTO new_server_limit
                VALUES
                    ('{$channel}', $limit_day);";
        $this->admindb->query($sql);
        $this->new_server_limit_txt();
        alert('更新成功');
    }
    public function new_server_limit_delete(){
        $channel = $_GET['channel'];
        $sql = "DELETE FROM new_server_limit WHERE channel='{$channel}'";
        $this->admindb->query($sql);
        $this->new_server_limit_txt();
        alert('删除成功');
    }
    public function new_server_limit_txt(){
        $sql = "select * from new_server_limit";
        $list = $this->admindb->fetchRow($sql,'channel');
        if(count($list)==0){
            $list = array();
        }
        $list = var_export($list,true);
        $config = $this->create_config_base($list);
        file_put_contents(CONFIG_DIR.'new_server_limit_txt.php', $config);
        return true;
    }


    public function reg_ban (){
        $assign = $this->get_server_rights();
        $assign['glist'] = $assign['srv_group'];

        $time = time();
        if($_GET['gid']){
            $gid = $_GET['gid'];
            $sql = "SELECT
                            id sid,
                            gid,
                            s_num,
                            sname,
                            description,
                            FROM_UNIXTIME(otime) otime,
                            1 as statu
                        FROM
                            adminserverlist
                        WHERE
                            gid = $gid
                        AND otime<$time";
            $slist = $this->admindb->fetchRow($sql);
        }
        $path = CONFIG_DIR.'reg_ban_list.php';
        if(!file_exists($path)) {//不存在
            $list = var_export(array(),true);
            $config = $this->create_config_base($list);
            file_put_contents($path,$config);
            $reg_ban_list = array();
        }else {
            $reg_ban_list = include($path);
        }
        foreach ($slist as $key => $val) {
            if(in_array($val['sid'],$reg_ban_list)) {
                $slist[$key]['statu'] = 0;
            }
        }
        $assign['slist'] = $slist;
        $this->display(CENTER_TEMP . 'reg_ban.shtml',$assign);
    }
    public function reg_ban_save (){
        $sid_list = $_POST['sid_list'];
        $action = $_POST['action'];

        if(count($sid_list)<1) {
            alert('请选择游戏服');exit;
        }

        $path = CONFIG_DIR.'reg_ban_list.php';
        $list = include($path);

        if($action=='批量修改为关闭'){
            foreach ($sid_list as $key => $val) {
                if(!in_array($val, $list))
                    $list[$key]= $val;
            }
        }else {
            foreach ($sid_list as $key => $val) {
                unset($list[$key]);
            }
        }
        $list = var_export($list,true);
        $config = $this->create_config_base($list);
        file_put_contents($path, $config);
        alert('操作成功！');
    }





    public function get_game_ip_port(){
        $host = urldecode(request('host'));
        $game_ip = 0;
        $game_port = 0;
        $db_port = 3306;
        $db_host = '';
        if($host){
            $host .= 'game_ip_port.txt';
            $myfile = Http::httpGet($host);
            $myfile = json_decode($myfile,true);
            if ($myfile) {
                $game_ip = $myfile['game_ip'];
                $game_port = $myfile['game_port'];
                $db_port = $myfile['db_port'];
                $db_host = $myfile['db_host'];
            }
        }
        $data = array(
            'game_ip'=>$game_ip,
            'game_port'=>$game_port,
            'db_port'=>$db_port,
            'db_host'=>$db_host,
        );
        exit(json_encode($data));

    }


}
