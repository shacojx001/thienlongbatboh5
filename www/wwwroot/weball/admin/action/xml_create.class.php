<?php
/**
 * Excel 配置生成
 * @author
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_xml_create extends action_superclass
{

    //导入到稳定服
    public function export_base(){
        // print_r($_REQUEST);exit;
        $ctime = $_REQUEST['ctime'];
        if(empty($ctime)){
            alert("请选择日志添加时间！");
        }
        $ctime = strtotime($ctime);
        $sql = "delete from vtnemo_admin_base.base_xml;";
        $this->admindb->query($sql);
        $sql = "insert into vtnemo_admin_base.base_xml select * from tlry_admin.base_xml where ctime<=$ctime";
        // print_r($sql);exit;
        $this->admindb->query($sql);

        alert("导入到稳定服成功！");
    }

    //特殊处理的xml
    public function special_xml($xml)
    {
        $xml_list = array(
            // 'skill',
            );
        if(!in_array($xml, $xml_list)){
            return false;
        }
        $list = array();
        switch ($xml) {
            // case 'skill':
            //     $list = array(
            //         'config_skill_0.lua',
            //         'config_skill_1.lua',
            //         'config_skill_2.lua',
            //         'config_skill_3.lua',
            //         'config_skill_4.lua',
            //         'config_skill_5.lua',
            //         'config_skill_6.lua',
            //         'config_skill_7.lua',
            //         'config_skill_8.lua',
            //         'config_skill_9.lua',
            //         );
            //     break;
            
            default:
                # code...
                break;
        }
        return $list;
    }

    public function base_xml_show()
    {
        $sql = "select aid,comment from base_xml where pid=0 ";
        foreach($this->admindb->fetchRow($sql) as $k=>$v) {
            $pid_list[$v['aid']] = $v['comment'];
        }

        $sql = "select x.*,xp.`comment` as ks from base_xml x LEFT JOIN base_xml xp on x.pid=xp.aid where x.pid!=0 and x.is_del!=1 and x.`show`=1 order by convert(xp.`comment` using gbk) asc,convert(x.comment using gbk) asc";
        $res = $this->admindb->fetchRow($sql);
        
        $list = $this->check_file_update($res);
// print_r($list);exit;
        $this->smarty->assign('pid_list', $pid_list);
        $this->smarty->assign('list', $list);
        $this->smarty->display("./exceloption.shtml");
    }

    //检测文件是否有更新
    public function check_file_update($res)
    {
        foreach($res as $k=>$v) {
            $ltime = $v['ltime'];
            $xmlFile = strtoupper(substr(PHP_OS,0,3))==='WIN'?iconv("utf-8","gbk",XML_FILES_PATH.$v['xmlfile']) : XML_FILES_PATH.$v['xmlfile'];
            
            $hrlFile = SERVER_HEADER_PATH."/{$v['template']}.hrl";
            $erlFile = SERVER_DATA_PATH."/conf_{$v['template']}.erl";
            $luaFile = CLIENT_DATA_PATH."/config_{$v['template']}.lua"; 
            $msg = array();
            $timeList = array();

            list($msg[], $timeList[]) = $this->check_file_info('xml', $xmlFile, $ltime);
            list($msg[], $timeList[]) = $this->check_file_info('hrl', $hrlFile, $ltime);
            list($msg[], $timeList[]) = $this->check_file_info('erl', $erlFile, $ltime);
            list($msg[], $timeList[]) = $this->check_file_info('lua', $luaFile, $ltime);
            
            $msg[] = '<b>生成：'.date("Y-m-d H:i:s",$ltime).'</b>';
            $msg = implode("</br>", array_filter($msg));

            $list[$v['pid']][$v['aid']] = $v;
            $list[$v['pid']][$v['aid']]['updateTime'] = $msg;
          
            //勾选中未生成的，xml有更新的
            if($_REQUEST['update']==1) {
                foreach ($timeList as $key => $val) {
                    if($val && $val > $ltime) {
                        $list[$v['pid']][$v['aid']]['checked'] = 'checked';
                    }
                }
            }
        }
        return $list;
    }

    //检测文件情况
    public function check_file_info($type, $xmlFile, $ltime)
    {
        if(file_exists($xmlFile)) {
            $updateTime2 = $updateTime = filemtime($xmlFile);
            if($updateTime2) {
                
                if($updateTime2 > $ltime){
                   
                    $updateTime = "<b style=color:red>{$type}：".date("Y-m-d H:i:s",$updateTime)."</b>";
                }else{
                    $updateTime = $type.'：'.date("Y-m-d H:i:s",$updateTime);
                }

            }else {
                $updateTime = "<b style=color:red;>获取失败！</b>";
            }
        }elseif($type == 'xml') {
            $updateTime = "<b style=color:red;>{$type}文件不存在！</b>";
        }

        return array($updateTime, $updateTime2);
    }

    public function add_config()
    {
        $aid = $_POST['aid'];
        $pid = $_POST['pid'];
        $comment = trim($_POST['comment']);
        $xmlfile = trim($_POST['xmlfile']);
        $template = trim($_POST['template']);
        $subtab = trim($_POST['subtab']);
        $table_name = trim($_POST['table_name']);
        $primary_key = trim($_POST['primary_key']);
        $show = trim($_POST['show']);
        $ctime = time();
        if($pid == 0) {
            $sql = "INSERT INTO base_xml(pid,comment,ctime)VALUES($pid,'{$comment}','{$ctime}');";
        } elseif($aid) {
            $sql = "UPDATE base_xml SET pid=$pid,comment='{$comment}',xmlfile='{$xmlfile}',template='{$template}',subtab='{$subtab}',ctime='{$ctime}',table_name='{$table_name}',primary_key='{$primary_key}',`show`='{$show}' WHERE aid=$aid;";
            
        }else{
            $sql = "INSERT INTO base_xml(pid,comment,xmlfile,template,subtab,ctime,table_name,primary_key) VALUES($pid,'{$comment}','{$xmlfile}','{$template}','{$subtab}','{$ctime}','{$table_name}','{$primary_key}');";
        }

        $this->admindb->query($sql);
        alert("添加成功！");
    }

    public function option_action()
    {
        $stime = microtime(true);
        $aid = $_REQUEST['type'];

        if(empty($aid)) {
            alert('请选择生成配置！');
        }else {
            if($aid == 'all') {//全部生成
                $sql = "select * from base_xml where pid != 0 and `show`=1 and is_del!=1";
            }else {
                $aid_str = is_array($aid) ? implode(",", $aid) : $aid;
                $sql = "select * from base_xml where aid in($aid_str)";
            }

            $data = $this->admindb->fetchRow($sql);

            include(XML_PARSE_PATH."common.class.php");
            ini_set('memory_limit', '3072M');
            //缓冲输出
            header('X-Accel-Buffering: no');
            ini_set('max_execution_time', '0');

            //定时往下拉JS
            if(!$_GET['print']) {
                echo "<script>var ClentTimer = setInterval('get_compile_log()',500);function get_compile_log(){window.scrollBy(0,+100000);}</script>";
            }
            foreach ($data as $key => $val) {
                $st = microtime(true);
                $this->generate($val);
                $this->get_cost_time($st, '——总耗时');   
            }
        }
        $et = round(microtime(true)-$stime,5);  
        echo "</br></br><b style='color:red;'>————————————执行结束，总耗时：$et 秒————————————</b>";
        //定时下拉JS结束
        echo "<script>setTimeout(function(){clearInterval(ClentTimer)},2000);</script>";

        if($_GET['print']==1){//打印data不提交
            exit;
        }

        if(strtoupper(substr(PHP_OS,0,3))!='WIN') {//本地调试不用提交
            $st = microtime(true);
            $this->svn_commit_all_config();
            $this->get_cost_time($st, '</br>——SVN提交耗时');   
        }

    }

    //更新最后生成时间
    public function get_cost_time($st, $str)
    {
        $et = round(microtime(true)-$st,5);  
        $et = $et > 1 ? "<b style='color:red;'>$et</b>" : $et; 
        echo "{$str}：$et 秒</br>";   
    }

    //更新最后生成时间
    public function insert_time($aid)
    {
        $time = time();
        $sql = "UPDATE base_xml SET ltime='$time' WHERE aid=$aid ";
        $this->admindb->query($sql);
        return true;
    }

    //执行函数
    public function generate($confArgs)
    {
        echo "{$confArgs['comment']} 模块：</br>";
        $xmlFile = $confArgs['xmlfile'];
        $subTable = $confArgs['subtab'];
        $primaryKey = $confArgs['primary_key'];
        $aid = $confArgs['aid'];
        $st = microtime(true);
        $data = $this->parse_xml($xmlFile, $subTable, $primaryKey);
        $this->get_cost_time($st, "xml解析时间 "); 
        //输出内容
        ob_flush();
        flush();

        if(!is_array($data)){
            return false;
        }
        if(count($data)<1){
            $this->echo_error("空数组"); 
        }

        $serverTpl = XML_PARSE_PATH."{$confArgs['template']}.svr.tpl.php";
        if ($confArgs['template'] && file_exists($serverTpl)) {
            $serverFile = SERVER_DATA_PATH."/conf_{$confArgs['template']}.erl";
            $serverSplit = SERVER_DATA_PATH."/{$confArgs['template']}/conf_{$confArgs['template']}_IndexNum.erl";
            $st = microtime(true);
            $res = $this->gen_data_confs($serverFile, $serverTpl, $data, $serverSplit);
            if($res>0){
                $this->get_cost_time($st, "——服务端文件: $serverFile ok 耗时"); 
                $this->insert_time($aid);
            } else {
                $this->echo_error("服务端文件: $serverFile 写入失败"); 
            }
        }
        //输出内容
        ob_flush();
        flush();

        $headerTpl = XML_PARSE_PATH."{$confArgs['template']}.hrl.tpl.php";
        if ($confArgs['template'] && file_exists($headerTpl)) {
            $headerFile = SERVER_HEADER_PATH."/{$confArgs['template']}.hrl";
            $st = microtime(true);
            $res = $this->gen_data_confs($headerFile, $headerTpl, $data, '');
            if($res>0){
                $this->get_cost_time($st, "——服务端头文件: $headerFile ok 耗时"); 
                $this->insert_time($aid);
            } else {
                $this->echo_error("服务端头文件: $headerFile 写入失败"); 
            }
        }
        //输出内容
        ob_flush();
        flush();

        $clientTpl = XML_PARSE_PATH."{$confArgs['template']}.cli.tpl.php";
        if ($confArgs['template'] && file_exists($clientTpl)) {
            $clientFile = CLIENT_DATA_PATH."/config_{$confArgs['template']}.lua"; 
            $clientSplit = CLIENT_DATA_PATH."/config_{$confArgs['template']}_IndexNum.lua"; 
           
            $st = microtime(true);
            $res = $this->gen_data_confs($clientFile, $clientTpl, $data, $clientSplit);
          
            if($res>0){
                //汉彬加的东西
                // $list = $this->special_xml($confArgs['template']);
                $work_dir = ROOT_PATH.'sh/optimizer';
                // if(is_array($list)){
                //     foreach ($list as $key => $val) {
                //         $stk = microtime(true);
                //         $clientFileNum = CLIENT_DATA_PATH.'/'.$val;
                //         $opt_cmd = ROOT_PATH."sh/optimizer/optimizer {$work_dir} {$clientFileNum}";
                //         exec($opt_cmd, $output);
                //         $this->get_cost_time($stk, "——客户端压缩文件: $clientFileNum ok 耗时"); 
                //         //输出内容
                //         ob_flush();
                //         flush();
                //     }

                // }else{
                    $stk = microtime(true);
                    $opt_cmd = ROOT_PATH."sh/optimizer/optimizer {$work_dir} {$clientFile}";
                    exec($opt_cmd, $output);
                    $this->get_cost_time($stk, "——客户端压缩文件: $clientFile ok 耗时"); 
                // }
                
                // print_r("<br>");
                // print_r("opt_cmd：".$opt_cmd);
                // print_r("<br>");
                // print_r($output);
                // print_r("<br>");
                //汉彬加的东西
                $this->get_cost_time($st, "——客户端文件: $clientFile ok 耗时"); 
                $this->insert_time($aid);
            } else {
                $this->echo_error("客户端文件: $clientFile 写入失败"); 
            }
        }

        //输出内容
        ob_flush();
        flush();

        return true;
    }


    // 解析xml函数
    public function parse_xml($xmlFile, $tableFlag,  $primaryKey = null, $indexBeg = 0, $indexignore = 1)
    {
       
        // echo "开始解析XML文件：$xmlFile 的子表: $tableFlag</br>";
        // $xmlFile = XML_FILES_PATH.$xmlFile;
        $xmlFile = strtoupper(substr(PHP_OS,0,3))==='WIN'?iconv("utf-8","gbk",XML_FILES_PATH.$xmlFile) : XML_FILES_PATH.$xmlFile; 
        // $xmlFile = iconv("utf-8","gbk",XML_FILES_PATH.$xmlFile);
        if (file_exists($xmlFile)) {
            $xmlData = simplexml_load_file($xmlFile);
            $sheetNum = count($xmlData->Worksheet);
            $tableIndex = $sheetNum;

            if($sheetNum==0) {
                $this->echo_error($xmlFile." XML文件格式有问题，解析不出来，请重新生成！"); 
                return false;
            }

            //获取所有子表信息
            $tableList = array();
            for($i = 0; $i < $sheetNum; $i++) {
                $tableFlagCur = $xmlData->Worksheet[$i]->attributes('ss', TRUE)->Name;
                $tableFlagCur = $this->object_to_array($tableFlagCur);
                $tableList[$i] = $tableFlagCur[0];
            }


            // 定位子表索引
            $tableList = array();
            if (is_int($tableFlag)) {//子表是数字
                // $tableIndex = $tableFlag;
                $tableList[$tableFlag] = $tableFlag;
            } else {
                //筛选出需要的子表
                $tableFlag = explode(",", $tableFlag); 
                //获取所有子表信息
                for($i = 0; $i < $sheetNum; $i++) {
                    $tableFlagCur = $xmlData->Worksheet[$i]->attributes('ss', TRUE)->Name;
                    $tableFlagCur = $this->object_to_array($tableFlagCur);
                    if(!in_array($tableFlagCur[0], $tableFlag)) continue;
                    $tableList[$i] = $tableFlagCur[0];
                }
            }
           
            $res = array();
            $notesList = array();//注释项
            foreach ($tableList as $tableIndex => $Name) {
                if ($tableIndex < $sheetNum) {
                    // 成功定位子表索引
                    $index = 0;
                    $keys = array();
                    $table = $xmlData->Worksheet[$tableIndex]->Table;

// file_put_contents('cs.php', print_r($table,true));
// print_r('ok');exit;                    
                    // 定位主键索引
                    foreach ($table->Row[$indexBeg] as $Cell) {
                        if (preg_match("/[A-Za-z0-9_]+/", $Cell->Data, $matchs) > 0) {
                            $keys[$matchs[0]] = $index;
                        }
                        $index++;
                    }
                    $index = 0;
                    $rows = array();
                    // 复制表数据
                    foreach ($table->Row as $row) {
                        if($index == $indexignore) {//忽略备注项
                            //注释项目
                            for ($i = 0; $i < count($row->Cell); $i++) {
                                $val = (string)$row->Cell[$i]->Data;
                                if ($val == null) {//如果值为空时候,默认为0
                                    $val = '';
                                }
                                $notesList[] = trim($val);
                            }
                            $index++;
                            continue;
                        }
                        if ($index++ <= $indexBeg) {
                            continue;
                        }

                        $tmp = array();
                        $buff = array();
                        $offset = 0;

                        for ($i = 0; $i < count($row->Cell); $i++) {
                            $cellIndex = $row->Cell[$i]->attributes("ss", TRUE)->Index;
                            if ($cellIndex > 0) {
                                $offset = ($cellIndex - 1) - $i;
                            }
                            $val = (string)$row->Cell[$i]->Data;

                            if ($val == null) {//如果值为空时候,默认为0
                                $val = '';
                            }
                            $buff[$i + $offset] = trim($val);
                        }
                        
                        if(empty($buff[0]) && !is_numeric($buff[0]) ) {//第一个值为空数据，去掉
                            continue;
                        }    
                        // 格式化表数据
                        foreach ($keys as $key => $col) {
                            $tmp[$key] = isset($buff[$col])?$buff[$col]:"";
                        }
                        $rows[] = $tmp;
                    }
   
                } else {
                    // 定位子表索引失败，中止配置文件生成
                    $this->echo_error("XML文件: ".basename($xmlFile)." 不存在子表: $tableFlag ！"); 
                    return false;
                }
                //主键为键
                $res[] = $this->array_to_hashmap2($rows,$primaryKey);
            }
      
            //合并数组         
            $data = $res[0];
            //补充子键...联表才需要
            $key_list = array();
            foreach ($res as $key => $val) {
                $key_list[$key] = array_fill_keys(array_keys(current($val)),'');
            }
            for ($i=1; $i < count($res); $i++) { 
                
                foreach ($data as $key => $val) {
                    if($res[$i][$key]) {
                        $data[$key] = $val + $res[$i][$key];
                    }else{
                        $data[$key] = $val + $key_list[$i];
                    }
                }
            } 
    // print_r($data);exit;     
            //打印data
            if($_GET['print']==1){
                    
                    $list = var_export($data,true);
                    echo "<style>code{font-size: 15px; font-family: initial;}</style>";
                    highlight_string($list);
                    exit;
            }
            //导表
            if($_GET['print'] == 'import_mysql') {
                $key_list = array_keys(current($data));
                $notesList = array_values(array_unique($notesList));

                $header = array_combine($notesList, $key_list);
                //导表
                $this->import_mysql($header, $data);

                alert('导表成功');
                
            }else {//服务端data
                unset($xmlData);
                unset($res);
                unset($tableList);
                
                return $data;
            }

        } else{
            $this->echo_error($xmlFile." XML文件不存在！"); 
            return false;
        }
    }

    // 输出配置表函数
    public function gen_data_confs($fileName, $tpl, $data, $fileSplit)
    {
        $content = $this->parse_tpl($tpl, $data, $fileSplit);
      
        $res = file_put_contents($fileName, str_replace("\r", "", $content));
        return $res;
        // echo "生成 ".basename($fileName)." 配置文件完毕</br>前往: ".dirname($fileName)." 检查！ </br></br>";
    }

    // 解析模板文件函数
    public function parse_tpl($tpl, $data, $fileSplit)
    {
        ob_start();
        extract(array("data" => $data, "fileSplit"=>$fileSplit));

        include($tpl);//执行模板php   检测php语法错误：PHP_check_syntax
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    //通过模版名，获取内容
    public function getData($template){
        $sql = "select * from base_xml where template = '$template'";         
        $confArgs = $this->admindb->fetchOne($sql);
        $data = $this->parse_xml($confArgs['xmlfile'], $confArgs['subtab'], $confArgs['primary_key']);
        return $data;
    }

    //SVN提交
    public function svn_commit_all_config()
    {
        include_once 'action/svn.class.php';
        $svn_class = new action_svn();
        echo "<br><br>erl文件正在提交svn...<br>";
        $svn_class->svn_commit_erl();
        // echo "<br><br>xml文件正在提交svn...<br>";
        // $svn_class->svn_commit_xml();
        echo "<br><br>lua文件正在提交svn...<br>";
        $svn_class->svn_commit_lua();
        echo "<br><br>hrl文件正在提交svn...<br>";
        $svn_class->svn_commit_hrl();
    }

    //  删除配置
    public function del_config()
    {
        $aid = $_REQUEST['aid'];
        if ($aid == '') alert('ID为空');
        $this->admindb->query("update base_xml set is_del=1 where aid = '$aid'");
        // $this->admindb->query("delete from base_xml where aid = '$aid'");
        alert("删除成功");
    }

    //xml导出
    public function export_tpl_excel()
    {
        $aid = $_GET['type'];
        if(empty($aid)) {
            alert('请选择xml模版导出！');
        }
        $sql = "select * from base_xml where aid=$aid";
        $info = $this->admindb->fetchOne($sql);

        if(empty($info['xmlfile'])) {
            alert('该配置没有填写xml路径！');
        }

        $xmlFile = XML_FILES_PATH.$info['xmlfile'];

        if (file_exists($xmlFile)) {//文件下载
            ob_start();
            $filename=$xmlFile;
            $date=date("Ymd-H:i:m");
            header( "Content-type:  application/octet-stream ");
            header( "Accept-Ranges:  bytes ");
            header( "Content-Disposition:  attachment;  filename= {$info['xmlfile']}");
            $size=readfile($filename);
            header( "Accept-Length: " .$size);
             exit;
        } else {
            alert('xml文件不存在！');
        }
    }
    
    //输出错误
    public function echo_error($msg)
    {   
        echo "<b style=color:red;>——".$msg."</b></br></br>";
    }

    //导表入库
    public function import_mysql($header, $data)
    {
        $aid = $_REQUEST['type'];

        if(empty($aid)) {
            alert('请选择生成配置！');
        }else {
            
            $aid_str = is_array($aid) ? implode(",", $aid) : $aid;
            $sql = "select * from base_xml where aid in($aid_str)";
            
            $confArgs = $this->admindb->fetchOne($sql);

            $xmlFile = $confArgs['xmlfile'];
            $subTable = $confArgs['subtab'];
            $primaryKey = $confArgs['primary_key'];

            $table = "base_table_".$confArgs['template'];

            $str_array = array();
            foreach ($header as $key => $val) {
                $str_array[] = "`{$val}` varchar(255) NOT NULL COMMENT '{$key}'";
            }
            $n = 0;
            foreach ($header as $key => $val) {
                if($n > 20) break;
                $str_array[] = "KEY `{$val}` (`{$val}`)";
                $n++;
            }
          
            $insert_array = array();      
            // "INSERT INTO `{$table}` VALUES ";

            foreach ($data as $key => $val) {

                $res = array();
                foreach ($val as $kk => $vv) {
                    if(in_array($kk, $header)){
                        $res[] = '"'.$vv.'"';
                    }
                }
                $insert_array[] =  stripcslashes(addslashes("(".implode(",", $res).")"));
            }     
 
            $insert = "INSERT INTO `{$table}` VALUES ".implode(",", $insert_array);

            $str = implode(",", $str_array);

            $str = "CREATE TABLE `{$table}` ($str) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
// print_r($insert);exit;
            $drop_sql = "DROP TABLE IF EXISTS `{$table}`;";
           
            $this->admindb->query($drop_sql);
            $this->admindb->query($str);
            $this->admindb->query($insert);

            alert("导入成功");
            // "CREATE TABLE `ban_info` (
            //   `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '禁止序号',
            //   `type` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '禁止类型(1:ip)',
            //   `value` varchar(20) NOT NULL COMMENT '禁止的值',
            //   `last_updated` varchar(50) NOT NULL COMMENT '上次更新时间',
            //   PRIMARY KEY (`id`),
            //   KEY `type` (`type`),
            //   KEY `value` (`value`)
            // ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"

            
        }
    }

    /**
     * 对象 转 数组
     *
     * @param object $obj 对象
     * @return array
     */
    function object_to_array($obj) 
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }
     
        return $obj;
    }

    //将一个二维数组转换为 hashmap
    function array_to_hashmap2(& $arr, $keyField, $valueField = null)
    {
        $ret = array();
        
        foreach ($arr as $row) {
            $key_array = explode(",", $keyField);
            $key = '';
            foreach ($key_array as $k => $v) {
                $key = $key == '' ? (string)$row[$v] : $key .'_'.(string)$row[$v];
            }    
            if($ret[$key]){//检测主键重复值
                echo  $this->echo_error("主键 $keyField 有重复值：".$key); 
            }
            $ret[$key] = $row;
        }
       
        return $ret;
    }

}
