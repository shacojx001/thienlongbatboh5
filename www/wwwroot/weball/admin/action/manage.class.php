<?php
/**
 * 后台管理
 *
 * 管理菜单，分组，用户，后台操作日志等
 *
 * @author:    calvin
 * @email:    calvinzhong888@gmail.com
 */

require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_manage extends action_superclass
{
    public static $child_id = array();
    public static $sync_child_id = array();

    /**
     * 菜单列表页
     */
    public function menu()
    {
        //权限判断
        $db = Ext_Mysql::getInstance('admin');

        $parent_rows = $db->fetchRow("SELECT kid, name, `show` FROM adminkind WHERE pid=0");
        $arr_parent = array();
        $arr_parent[0] = '';
        foreach ($parent_rows as $row) {
            if ($row['show'] == "NO")
                $arr_parent[$row['kid']] = $row['name'] . "(隐藏)";
            else
                $arr_parent[$row['kid']] = $row['name'];
        }

        $kw_pid = isset($_REQUEST['kw_pid']) ? (int)$_REQUEST['kw_pid'] : 0;
        $kw_name = isset($_REQUEST['kw_name']) ? htmlspecialchars(trim($_REQUEST['kw_name'])) : 0;
        $kw_show = isset($_REQUEST['kw_show']) ? htmlspecialchars(trim($_REQUEST['kw_show'])) : 0;

        $sql = '1=1';
        if ($kw_pid > 0) $sql .= " AND pid='{$kw_pid}'";
        if (!empty($kw_name)) $sql .= " AND name LIKE '%{$kw_name}%'";
        if (!is_numeric($kw_show)) {
            $sql .= " AND `show`='{$kw_show}'";
        }

        $rows = $db->fetchRow("SELECT * FROM adminkind WHERE {$sql} ORDER BY pid, ksort");
        $data = array();
        foreach ($rows as $row) {
            $row['pname'] = '';
            if (isset($arr_parent[$row['pid']])) $row['pname'] = $arr_parent[$row['pid']];
            $data[] = $row;
        }

        $this->smarty->assign(array(
            'kw_pid' => $kw_pid,
            'kw_name' => $kw_name,
            'kw_show' => $kw_show,
            'arr_parent' => $arr_parent,
            'arr_show' => array(0 => '', 'YES' => '显示', 'NO' => '隐藏'),
            'arr_show_radio' => array('YES' => '显示', 'NO' => '隐藏'),
            'data' => $data,
        ));
        $this->smarty->display('manage_menu.shtml');
    }

    /**
     * 菜单-添加或编辑
     */
    public function edit_menu()
    {
        $db = Ext_Mysql::getInstance('admin');
        $old_id = isset($_REQUEST['old_id']) ? (int)$_REQUEST['old_id'] : 0;
        if ($old_id > 0) {
            $row = $db->fetchOne("SELECT kid,`system` FROM adminkind WHERE kid={$old_id}");
            if (empty($row)) alert("原记录不存在", '');
            if ($row['system'] == 1) alert("内置菜单不能编辑", '');
        }

        if (!isset($_REQUEST['name']) || empty($_REQUEST['name'])) {
            alert('菜单名称错误', '');
        }
        $name = htmlspecialchars(trim($_REQUEST['name']));

        if (!isset($_REQUEST['pid']) || !is_numeric($_REQUEST['pid']) || $_REQUEST['pid'] < 0 || $_REQUEST['pid'] > 1000) {
            alert('父级菜单错误', '');
        }
        $pid = (int)$_REQUEST['pid'];


        if (!isset($_REQUEST['filename'])) {
            alert('菜单名称错误', '');
        }
        $filename = htmlspecialchars(trim($_REQUEST['filename']));

        if (!isset($_REQUEST['show']) || !in_array($_REQUEST['show'], array('YES', 'NO'))) {
            alert('显示或隐藏错误', '');
        }
        $show = $_REQUEST['show'];

        if (!isset($_REQUEST['ksort']) || !is_numeric($_REQUEST['ksort']) || $_REQUEST['ksort'] < 0 || $_REQUEST['ksort'] > 255) {
            alert('排序错误', '');
        }
        $ksort = (int)$_REQUEST['ksort'];

        //添加
        if (empty($old_id)) {
            Admin::adminLog("添加菜单:$name");
            Auth::check_action(Auth::$MENU_ADD);
            $time = $_SERVER['REQUEST_TIME'];
            $sql = "INSERT INTO adminkind SET name='{$name}',pid={$pid},filename='{$filename}',
			`show`='{$show}',ctime={$time},mtime={$time},ksort={$ksort}";
        } //编辑
        else {
            if ($pid == $old_id) {
                alert('父级菜单不能选自己错误', '');
            }
            Admin::adminLog("编辑菜单:$name");
            Auth::check_action(Auth::$MENU_EDIT);
            $time = $_SERVER['REQUEST_TIME'];
            $sql = "UPDATE adminkind SET name='{$name}',pid={$pid},filename='{$filename}',
			`show`='{$show}',mtime={$time},ksort={$ksort}
			WHERE kid={$old_id}";
        }

        $db->query($sql);
        if ($db->getAffectRows() > 0) {
            alert('添加或修改成功', 'action_gateway.php?ctl=manage&act=menu');
        } else {
            alert('添加或修改失败', '');
        }
    }

    /**
     * 菜单-删除
     */
    public function delete_menu()
    {
        //权限判断
        Auth::check_action(Auth::$MENU_DEL);

        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) {
            alert('参数错误', '');
        }

        $db = Ext_Mysql::getInstance('admin');
        $row = $db->fetchOne("SELECT `system` FROM adminkind WHERE kid={$id}");
        if (empty($row)) alert("原记录不存在", '');
        if ($row['system'] == 1) alert("内置菜单不能删除", '');
        Admin::adminLog("删除菜单:$id");
        $db->query("DELETE FROM adminkind WHERE kid={$id}");
        $db->query("DELETE FROM adminrights WHERE kid={$id}");

        alert('删除记录成功', '');
    }

    /**
     * 菜单-设置可操作项
     */
    public function set_menu_opt()
    {
        //权限判断
        Auth::check_action(Auth::$MENU_OPT_MANAGE);

        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) {
            alert('参数错误', '');
        }

        $db = Ext_Mysql::getInstance('admin');
        $menu_row = $db->fetchOne("SELECT * FROM adminkind WHERE kid={$id}");
        if (empty($menu_row)) {
            alert('参数错误', '');
        }
        if ($menu_row['pid'] == 0) {
            alert('一级菜单不需要设置可操作项', '');
        }

        $rows = $db->fetchRow("SELECT * FROM adminrights WHERE kid={$id}");
        $data = array();
        foreach ($rows as $row) {
            $data[] = $row;
        }

        $this->smarty->assign(array(
            'kid' => $menu_row['kid'],
            'name' => $menu_row['name'],
            'data' => $data,
        ));

        $this->smarty->display('manage_menu_opt.shtml');
    }

    /**
     * 菜单-设置可操作项-添加或编辑
     */
    public function edit_menu_opt()
    {
        //权限判断
        Auth::check_action(Auth::$MENU_OPT_MANAGE);

        $db = Ext_Mysql::getInstance('admin');
        $old_id = isset($_REQUEST['old_id']) ? (int)$_REQUEST['old_id'] : 0;
        $old_kid = isset($_REQUEST['old_kid']) ? (int)$_REQUEST['old_kid'] : 0;
        $old_mark = isset($_REQUEST['old_mark']) ? htmlspecialchars(trim($_REQUEST['old_mark'])) : '';
        if ($old_id > 0) {
            $row = $db->fetchOne("SELECT id FROM adminrights WHERE id={$old_id}");
            if (empty($row)) alert("原记录不存在", '');
            unset($row);
        }

        if (!isset($_REQUEST['name']) || empty($_REQUEST['name'])) {
            alert('操作名称错误', '');
        }
        $name = htmlspecialchars(trim($_REQUEST['name']));

        if (!isset($_REQUEST['mark']) || empty($_REQUEST['mark'])) {
            alert('操作标识错误', '');
        }
        $mark = htmlspecialchars(trim($_REQUEST['mark']));

        //添加
        if (empty($old_id)) {
            $oldrow = $db->fetchOne("SELECT id FROM adminrights WHERE kid={$old_kid} AND mark='{$mark}'");
            if ($oldrow) {
                alert('该菜单下相同的标识已经存在', '');
            }
            Admin::adminLog("添加菜单操作项:$name($mark)");
            $sql = "INSERT INTO adminrights SET kid={$old_kid}, name='{$name}', mark='{$mark}'";
        } //编辑
        else {
            if ($old_mark != $mark) {
                $oldrow = $db->fetchOne("SELECT id FROM adminrights WHERE kid={$old_kid} AND mark='{$mark}' AND mark<>'{$old_mark}'");
                if ($oldrow) {
                    alert('该菜单下相同的标识已经存在', '');
                }
            }
            Admin::adminLog("编辑菜单操作项:$name($mark)");
            $sql = "UPDATE adminrights SET name='{$name}', mark='{$mark}'
			WHERE id={$old_id}";
        }

        $db->query($sql);
        if ($db->getAffectRows() > 0) {
            alert('添加或修改成功', 'action_gateway.php?ctl=manage&act=set_menu_opt&id=' . $old_kid);
        } else {
            alert('添加或修改失败', '');
        }
    }

    /**
     * 菜单-设置可操作项-删除
     */
    public function delete_menu_opt()
    {
        //权限判断
        Auth::check_action(Auth::$MENU_OPT_MANAGE);

        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) {
            alert('参数错误', '');
        }
        Admin::adminLog("编辑菜单操作项:$id");
        Ext_Mysql::getInstance('admin')->query("DELETE FROM adminrights WHERE id={$id}");
        alert('删除记录成功', 'action_gateway.php?ctl=manage&act=set_menu_opt&id=' . $_REQUEST['kid']);
    }

    /**
     * 权限分组列表页
     */
    public function group()
    {
        $where = ' where 1 ';
        // 用户组名
        $gname = $_GET['gname'];
        if (!empty($gname)) {
            $where .= " and gname like '%$gname%'";
        }
        if($_SESSION['super'] != 'YES'){
            $child_gids = self::get_children($_SESSION['gid']);
            $child_gids[] = $_SESSION['gid'];
            $where .= ' and gid in ('.implode(',',$child_gids).')';
        }
        $sql = "SELECT * FROM admingroup $where order by pid,gid";
        $db = Ext_Mysql::getInstance('admin');
        $data = $db->fetchRow($sql);

        $glist = array();
        if (!empty($data)) {
            foreach ($data as $key => $val) {
                $glist[$val['gid']] = $val['gname'];
            }
        }
        $this->smarty->assign(array(
            'data' => $data,
            'glist' => $glist,
        ));

        $this->smarty->display('manage_group.shtml');
    }

    /**
     * 权限分组-添加或编辑
     */
    public function edit_group()
    {
        Auth::check_action(Auth::$GROUP_ADD);

        $db = Ext_Mysql::getInstance('admin');
        $old_id = request('old_id','int');
        if ($old_id > 0) {
            $row = $db->fetchOne("SELECT gid FROM admingroup WHERE gid={$old_id}");
            if (empty($row)) alert("原记录不存在", '');
            unset($row);
        }
        $pid = request('pid','int');
        if($_SESSION['super'] != 'YES' && !$pid){
            alert('请选项父分组!');
        }
        $name = htmlspecialchars(trim($_REQUEST['name']));
        if (!isset($_REQUEST['name']) || empty($_REQUEST['name'])) {
            alert('名称错误', '');
        }

        if (!isset($_REQUEST['desc'])) {
            alert('名称错误', '');
        }
        $desc = htmlspecialchars(trim($_REQUEST['desc']));
        //添加
        if (empty($old_id)) {
            $time = time();
            Admin::adminLog("添加分组:$name");
            $sql = "INSERT INTO admingroup SET `pid`='{$pid}',gname='{$name}',`else`='{$desc}',`ctime`='{$time}',mtime={$time}";
            $db->query($sql);
        } //编辑
        else{
            Admin::adminLog("编辑分组:$name");
            $sql = "update admingroup set gname='{$name}',`else`='{$desc}' where gid = {$old_id}";
            $db->query($sql);
        }

        if ($db->getAffectRows() > 0) {
            alert('添加或修改成功', 'action_gateway.php?ctl=manage&act=group');
        } else {
            alert('添加或修改失败', '');
        }
    }

    /**
     * 权限分组-删除
     */
    public function delete_group()
    {
        //权限判断
        Auth::check_action(Auth::$GROUP_DEL);

        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) {
            alert('参数错误', '');
        }
        $db = Ext_Mysql::getInstance('admin');
        $db->query("DELETE FROM admingroup WHERE gid={$id}");
        $db->query("DELETE FROM adminuser WHERE gid={$id}");
        Admin::adminLog("删除分组:$id");
        alert('删除记录成功', '');
    }

    /**
     * 权限分组-设置权限
     */
    public function set_group_setopt()
    {
        //权限判断
        Auth::check_action(Auth::$GROUP_VIEW);

        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) {
            alert('参数错误', '');
        }

        $db = Ext_Mysql::getInstance('admin');
        $group_row = $db->fetchOne("SELECT gname, menuids, rights FROM admingroup WHERE gid={$id}");
        if (empty($group_row)) {
            alert('参数错误', '');
        }


        //------------ 处理菜单项 ------------//
        if($_SESSION['super'] == 'YES'){
            $sync_group = $db->fetchRow("SELECT gid,gname FROM admingroup WHERE gid != {$id} and pid = 0 order by gid asc");
            $this->smarty->assign('sync_group', $sync_group);
            $condition = '';
        }else{
            if(!$_SESSION['gid']){
                alert('our group is not exist!!!');
            }
            $rights_group = $db->fetchOne("SELECT menuids FROM admingroup WHERE gid={$_SESSION['gid']}");
            $rights_menu = $rights_group['menuids'];
            $condition = " AND kid in ($rights_menu)";
        }
        $menuids = $group_row['menuids'];
//        if ($_SESSION['super'] != 'YES' && $_SESSION['gid']>0) {
//            $rights_group = $db->fetchOne("SELECT menuids FROM admingroup WHERE gid={$_SESSION['gid']}");
//            $rights_menu = $rights_group['menuids'];
//            $condition = " AND kid in ($rights_menu)";
//        } else {
//            $sync_group = $db->fetchRow("SELECT gid,gname FROM admingroup WHERE groupbackground='cooperate' order by gid asc");
//            $this->smarty->assign('sync_group', $sync_group);
//            $condition = '';
//        }

        $arr_menuids = explode(",", $menuids);
        $rows = $db->fetchRow("SELECT kid, name, pid FROM adminkind WHERE `show`='YES' $condition ORDER BY pid,ksort ");
        $data = array();
        foreach ($rows as $row) {
            if ($row['pid'] == 0) {
                $data[$row['kid']] = array(
                    'parent_id' => $row['kid'],
                    'parent_name' => $row['name'],
                    'sonlist' => array(),
                );
            } else {
                $data[$row['pid']]['sonlist'][] = array(
                    'son_id' => $row['kid'],
                    'son_name' => $row['name'],
                    'checked' => (in_array($row['kid'], $arr_menuids) ? 1 : 0),
                );
            }
        }

        //------------ 处理可操作项 ------------//
        $rights = explode(",", $group_row['rights']);

        //取出权限数据
        $arr_rights = array();
        if ($_SESSION['super'] == 'YES') {
            $rwhere = "";
        } else {
            $sql = "select rights from admingroup where gid = '{$_SESSION['gid']}'";
            $rightss = $db->fetchOne($sql);
            if ($rightss['rights']) {
                $rwhere = " where id in ({$rightss['rights']}) ";
            } else {
                $rwhere = " id = 0 ";
            }
        }
        $rights_rows = $db->fetchRow("SELECT * FROM adminrights $rwhere");
        foreach ($rights_rows as $rights_row) {
            if (!isset($arr_rights[$rights_row['kid']])) {
                $arr_rights[$rights_row['kid']] = array();
            }
            $arr_rights[$rights_row['kid']][] = array(
                'id' => $rights_row['id'],
                'name' => $rights_row['name'],
                'mark' => $rights_row['mark'],
                'checked' => (in_array($rights_row['id'], $rights) ? 1 : 0),
            );
        }

        //取出所有菜单
        //$rows = $db->fetchRow("SELECT kid, name, pid FROM adminkind WHERE `show`='YES' $condition ORDER BY pid,ksort");
        $data2 = array();
        foreach ($rows as $row) {
            if ($row['pid'] == 0) {
                $data2[$row['kid']] = array(
                    'parent_id' => $row['kid'],
                    'parent_name' => $row['name'],
                    'sonlist' => array(),
                );
            } else {
                $data2[$row['pid']]['sonlist'][] = array(
                    'son_id' => $row['kid'],
                    'son_name' => $row['name'],
                    'rights' => (isset($arr_rights[$row['kid']]) ? $arr_rights[$row['kid']] : array()),
                );
            }
        }
        $this->smarty->assign(array(
            'id' => $id,
            'group_name' => $group_row['gname'],
            'data' => $data,
            'data2' => $data2,
        ));

        $this->smarty->display('manage_group_setopt.shtml');
    }

    /**
     * 权限分组-确定设置权限
     */
    public function set_group_setopt_submit()
    {
        //权限判断
        Auth::check_action(Auth::$GROUP_EDIT);

        $gid = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($gid <= 0) {
            alert('参数错误', '');
        }

        $db = Ext_Mysql::getInstance('admin');
        $appendids = array();
        $reduceids = array();
        $ids = array();
        foreach ($_REQUEST as $key => $data) {
            if (substr($key, 0, 4) == 'chk_') {
                $parent_id = substr($key, 4, strlen($key));
                $ids[$parent_id] = $parent_id;
                foreach ($data as $id) {
                    $ids[$id] = $id;
                }
            }
            if (substr($key, 0, 10) == 'appendchk_') {
                $appendparent_id = substr($key, 10, strlen($key));
                $appendids[$appendparent_id] = $appendparent_id;
                foreach ($data as $id) {
                    $appendids[$id] = $id;
                }
            }
            if (substr($key, 0, 10) == 'reducechk_') {
                $reduceparent_id = substr($key, 10, strlen($key));
                $reduceids[$reduceparent_id] = $reduceparent_id;
                foreach ($data as $id) {
                    $reduceids[$id] = $id;
                }
            }

        }
        $menu_ids = implode(",", $ids);


        $rights_ids = '';
        if (isset($_REQUEST['chkids']) && count($_REQUEST['chkids']) > 0) {
            $rights_ids = implode(",", $_REQUEST['chkids']);
        }

        $append_rights_ids = array();
        if (isset($_REQUEST['appendchkids']) && count($_REQUEST['appendchkids']) > 0) {
            $append_rights_ids = $_REQUEST['appendchkids'];
        }

        $reduce_rights_ids = array();
        if (isset($_REQUEST['reducechkids']) && count($_REQUEST['reducechkids']) > 0) {
            $reduce_rights_ids = $_REQUEST['reducechkids'];
        }

        $ch_group = self::get_children($gid);
        //如果修改的组存在子组
        if (!empty($ch_group)) {
            foreach ($ch_group as $val) {
                $sql = "select gid,menuids,rights from admingroup where gid = '$val'";
                $chinfo = $db->fetchOne($sql);
                //菜单权限子级更改
                if (!empty($chinfo['menuids'])) {
                    $chd_menu_arr = explode(',', $chinfo['menuids']);
                    $chd_menu_inster = array_intersect($ids, $chd_menu_arr);//求交集 父组没有的权限子组要删掉
                    if (!empty($chd_menu_inster)) {
                        $chd_menu_ids = join(',', $chd_menu_inster);
                    } else {
                        $chd_menu_ids = '';
                    }
                } else {
                    $chd_menu_ids = '';
                }
                //操作权限子组更改
                if (!empty($chinfo['rights'])) {
                    $chd_rights_arr = explode(',', $chinfo['rights']);
                    $chd_rights_inster = array_intersect($_REQUEST['chkids'], $chd_rights_arr);
                    if (!empty($chd_rights_inster)) {
                        $chd_rights_ids = join(',', $chd_rights_inster);
                    } else {
                        $chd_rights_ids = '';
                    }
                } else {
                    $chd_rights_ids = '';
                }
                $db->query("UPDATE admingroup SET menuids='{$chd_menu_ids}',rights='{$chd_rights_ids}' WHERE gid='{$val}'");
                Admin::adminLog($_SESSION['username'] . "修改相应子分组{$val}的权限：{$chd_menu_ids}，操作权限:{$chd_rights_ids}");
            }
        }
        $db->query("UPDATE admingroup SET menuids='{$menu_ids}',rights='{$rights_ids}' WHERE gid='{$gid}'");
        Admin::adminLog($_SESSION['username'] . "修改分组{$gid}的权限：{$menu_ids}，操作权限:{$rights_ids}");
        //权限同步，同时也删除子平台中的权限
        if ($_SESSION['super'] == 'YES') {
            $sync_group = $_POST['sync_group'];
            if (!empty($sync_group)) {
                foreach ($sync_group as $val) {
                    if (!empty($appendids) || !empty($append_rights_ids)) {//追加
                        $appendmerged = array();
                        $new_arr = array();
                        $appendrightsmerged = array();
                        $sql = "select menuids,rights from admingroup where gid='$val'";
                        $source = $this->admindb->fetchOne($sql);
                        //追加权限
                        if (!empty($source['menuids']) && !empty($appendids)) {
                            $sourcearr = explode(',', $source['menuids']);
                            foreach ($sourcearr as $av) {
                                $appendmerged[$av] = $av;
                            }
                            $new_arr = array_unique(array_merge($appendmerged, $appendids));
                            $appendmenuids = join(',', $new_arr);
                            $db->query("UPDATE admingroup SET menuids='{$appendmenuids}' WHERE gid='{$val}'");
                            Admin::adminLog($_SESSION['username'] . "追加分组{$val}的权限：{$appendmenuids}");
                        }
                        //追加执行权限
                        if (!empty($append_rights_ids)) {
                            if (!empty($source['rights'])) {
                                $sourcearr = explode(',', $source['rights']);
                                foreach ($sourcearr as $av) {
                                    $appendrightsmerged[$av] = $av;
                                }
                            }
                            $new_arr = array_unique(array_merge($appendrightsmerged, $append_rights_ids));
                            $appendrightsids = join(',', $new_arr);
                            $db->query("UPDATE admingroup SET rights='{$appendrightsids}' WHERE gid='{$val}'");
                            Admin::adminLog($_SESSION['username'] . "追加分组{$val}的操作权限：{$appendrightsids}");
                        }

                    } elseif (!empty($reduceids) || !empty($reduce_rights_ids)) {//减少
                        $reduccemerged = array();
                        $new_arr = array();
                        $reducerightsmerged = array();
                        $sql = "select menuids,rights from admingroup where gid='$val'";
                        $source = $this->admindb->fetchOne($sql);
                        if (!empty($source['menuids']) && !empty($reduceids)) {
                            $sourcearr = explode(',', $source['menuids']);
                            foreach ($sourcearr as $av) {
                                $reduccemerged[$av] = $av;
                            }
                            $new_arr = array_diff($reduccemerged, $reduceids);
                            $reducemenuids = join(',', $new_arr);
                            $db->query("UPDATE admingroup SET menuids='{$reducemenuids}' WHERE gid='{$val}'");
                            Admin::adminLog($_SESSION['username'] . "追减分组{$val}的权限：{$reducemenuids}");
                        }
                        //追减执行权限
                        if (!empty($reduce_rights_ids)) {
                            if (!empty($source['rights'])) {
                                $sourcearr = explode(',', $source['rights']);
                                foreach ($sourcearr as $av) {
                                    $reducerightsmerged[$av] = $av;
                                }
                            }
                            $new_arr = array_diff($reducerightsmerged, $reduce_rights_ids);
                            $reducerightsids = join(',', $new_arr);
                            $db->query("UPDATE admingroup SET rights='{$reducerightsids}' WHERE gid='{$val}'");
                            Admin::adminLog($_SESSION['username'] . "追减父分组{$val}的操作权限：{$reducerightsids}");
                        }
                        //如果修改的组存在子组,继续追减子组
                        $ch_group_sync = self::get_children($val);
                        if (!empty($ch_group_sync)) {
                            foreach ($ch_group_sync as $v) {
                                $sql = "select gid,menuids,rights from admingroup where gid = '$v'";
                                $chinfo = $db->fetchOne($sql);
                                if (!empty($chinfo['menuids']) && !empty($reduceids)) {
                                    $chd_menu_arr = explode(',', $chinfo['menuids']);
                                    $chd_menu_diff = array_diff($chd_menu_arr, $reduceids);
                                    if (!empty($chd_menu_diff)) {
                                        $chd_menu_ids = join(',', $chd_menu_diff);
                                    } else {
                                        $chd_menu_ids = '';
                                    }
                                    $db->query("UPDATE admingroup SET menuids='{$chd_menu_ids}' WHERE gid='{$v}'");
                                    Admin::adminLog($_SESSION['username'] . "追减子分组{$v}的权限：{$chd_menu_ids}");
                                }
                                //操作权限子组更改
                                if (!empty($chinfo['rights']) && !empty($reduce_rights_ids)) {
                                    $chd_rights_arr = explode(',', $chinfo['rights']);
                                    $chd_rights_diff = array_diff($chd_rights_arr, $reduce_rights_ids);
                                    if (!empty($chd_rights_diff)) {
                                        $chd_rights_ids = join(',', $chd_rights_diff);
                                    } else {
                                        $chd_rights_ids = '';
                                    }
                                    $db->query("UPDATE admingroup SET rights='{$chd_rights_ids}' WHERE gid='{$v}'");
                                    Admin::adminLog($_SESSION['username'] . "追减子分组{$v}的操作权限：{$chd_rights_ids}");
                                }

                            }
                        }
                    } else {
                        $ch_group_sync = self::get_children($val);
                        //如果修改的组存在子组
                        if (!empty($ch_group_sync)) {
                            foreach ($ch_group_sync as $v) {
                                $sql = "select gid,menuids,rights from admingroup where gid = '$v'";
                                $chinfo = $db->fetchOne($sql);
                                //菜单权限子级更改
                                if (!empty($chinfo['menuids'])) {
                                    $chd_menu_arr = explode(',', $chinfo['menuids']);
                                    $chd_menu_inster = array_intersect($ids, $chd_menu_arr);
                                    if (!empty($chd_menu_inster)) {
                                        $chd_menu_ids = join(',', $chd_menu_inster);
                                    } else {
                                        $chd_menu_ids = '';
                                    }
                                } else {
                                    $chd_menu_ids = '';
                                }
                                //操作权限子组更改
                                if (!empty($chinfo['rights'])) {
                                    $chd_rights_arr = explode(',', $chinfo['rights']);
                                    $chd_rights_inster = array_intersect($_REQUEST['chkids'], $chd_rights_arr);
                                    if (!empty($chd_rights_inster)) {
                                        $chd_rights_ids = join(',', $chd_rights_inster);
                                    } else {
                                        $chd_rights_ids = '';
                                    }
                                } else {
                                    $chd_rights_ids = '';
                                }
                                $db->query("UPDATE admingroup SET menuids='{$chd_menu_ids}',rights='{$chd_rights_ids}' WHERE gid='{$v}'");
                                Admin::adminLog($_SESSION['username'] . "修改子分组{$v}的权限：{$chd_menu_ids}，操作权限：{$chd_rights_ids}");
                            }
                        }
                        $db->query("UPDATE admingroup SET menuids='{$menu_ids}',rights='{$rights_ids}' WHERE gid='{$val}'");
                        Admin::adminLog($_SESSION['username'] . "同步修改分组{$val}的权限：{$menu_ids}，操作权限：{$rights_ids}");
                    }
                }
            }
        }

        $cache = Ext_Memcached::getInstance('user');
        $menu_key = sprintf(Key::$gm_m['menu_gid'],$gid);
        $rights_key = sprintf(Key::$gm_m['rights_gid'],$gid);
        $cache->delete($menu_key);
        $cache->delete($rights_key);

        alert('设置权限成功', '');
    }

    /**
     * 获取所有子组ID
     *
     * @param unknown_type $gid
     * @return unknown
     */
    private static function get_children($gid)
    {
        $arr = array();
        $db = Ext_Mysql::getInstance('admin');
        $sql = "select gid from admingroup where pid = '$gid'";
        $ginfo = $db->fetchRow($sql);
        if (!empty($ginfo)) {
            foreach ($ginfo as $val) {
                $arr[] = $val['gid'];
                $arr = array_merge($arr,self::get_children($val['gid']));
            }
        }
        return $arr;
    }

    /**
     * 用户列表页
     */
    public function user()
    {
        $db = Ext_Mysql::getInstance('admin');
        $where = "1";
        if($_SESSION['super'] != 'YES' ){
            $child_gids = self::get_children($_SESSION['gid']);
            $where .= ' and gid in ('.implode(',',$child_gids).')';
        }

        $username = request('username','str');
        if ($username != '') {
            $where .= " and username = '$username'";
        }

        //获取分组
        $group_rows = $db->fetchRow("SELECT gid,gname FROM admingroup where $where");
        $arr_group = array();
        foreach ($group_rows as $row) {
            $arr_group[$row['gid']] = $row['gname'];
        }

        //获取子分组所有成员
        $sql = "SELECT * FROM adminuser WHERE $where";
        $rows = $db->fetchRow($sql);
        $data = array();
        foreach ($rows as $row) {
            $row['group_name'] = isset($arr_group[$row['gid']]) ? $arr_group[$row['gid']] : '';
            $data[] = $row;
        }

        $groupsql = "select gid,platform_name as gname from adminplatformlist  order by gid";
        $grouplist = $this->admindb->fetchRow($groupsql);
        $glist = array();
        if (!empty($grouplist)) {
            foreach ($grouplist as $key => $val) {
                $glist[$val['gid']] = $val['gname'];
            }
        }

        $this->smarty->assign(array(
            'arr_group' => $arr_group,//分组权限
            'data' => $data, //用户列
            'group_list' => $glist,//$this->get_allgroup_list(),//专服列
            'super'=> $_SESSION['super']
        ));
// print_r($_SESSION);exit;
        $this->smarty->display('manage_user.shtml');
    }

    /**
     * 用户-添加或编辑
     */
    public function edit_user()
    {
        Auth::check_action(Auth::$USER_EDIT);

        $db = Ext_Mysql::getInstance('admin');
        $old_id = request('old_id','int');
        if ($old_id > 0) {
            $row = $db->fetchOne("SELECT uid FROM adminuser WHERE uid={$old_id}");
            if (empty($row)) alert("原记录不存在", '');
            unset($row);
        }

        $username = htmlspecialchars(request('username','str'));
        if (!$username) {
            alert('用户名称错误', '');
        }

        $passport = htmlspecialchars(request('passport','str'));
        if (!$passport) {
            alert('登录账号错误', '');
        }

        $if_mail = request('if_mail','int');
        $channel = request('channel')?request('channel','arr'):request('channel');
        if ($channel) {
            sort($channel);
            $channel = array_filter($channel);
            $channel = implode(",", $channel);
        } else {
            $channel = "";
        }

        $gid = request('gid','int');
        if ($_SESSION['super']!='YES' && !$gid) {
            alert('请选择权限分组', '');
        }elseif($gid){
            //获取分组
            $group_rows = $db->fetchOne("SELECT count(1) as c FROM admingroup where gid = $gid");
            if(empty($group_rows['c'])){
                alert('权限分组错误', '');
            }
        }

        $platform = request('platform')?request('platform','arr'):request('platform');
        if ($platform) {
            sort($platform);
            $platform = array_filter($platform);
            $platform = implode(",", $platform);
        } else {
            $platform = "";
        }

        $else = htmlspecialchars(request('desc','str'));
        if (!$else) {
            alert('备注错误', '');
        }
        $if_develop = $_REQUEST['if_develop']?$_REQUEST['if_develop']:0;

        //添加
        if (empty($old_id)) {
            if (!isset($_REQUEST['pass1']) || !isset($_REQUEST['pass2']) || $_REQUEST['pass1'] != $_REQUEST['pass2']) {
                alert('密码错误', '');
            }
            if (strlen($_REQUEST['pass1']) < 8) {
                alert('密码长度必须8位以上');
            }

            if (preg_match("/^[0-9]{1,}$/", $_REQUEST['pass1'])) {
                alert('密码必须由数字和字母组成');
            }
            if (preg_match("/^[a-zA-Z]{1,}$/", $_REQUEST['pass1'])) {
                alert('密码必须由数字和字母组成');
            }

            $password = md5($_REQUEST['pass1']);

            $time = $_SERVER['REQUEST_TIME'];
            $pid = $_SESSION['super']=='YES'?0:$_SESSION['uid'];


            $sql = "INSERT INTO adminuser SET pid='{$pid}',passport='{$passport}', `password`='{$password}',username='{$username}',
			gid={$gid},ctime={$time},mtime={$time},`else`='{$else}',last_ip='{$_SERVER['REMOTE_ADDR']}',gids='{$platform}',channel='{$channel}',if_mail='{$if_mail}',if_develop='{$if_develop}'";
            Admin::adminLog("添加用户:$passport");
        } //编辑
        else {
            if (!empty($_REQUEST['pass1']) && !empty($_REQUEST['pass2'])) {
                if ($_REQUEST['pass1'] != $_REQUEST['pass2']) {
                    alert('密码错误', '');
                }
                if (strlen($_REQUEST['pass1']) < 8) {
                    alert('密码长度必须8位以上');
                }

                if (preg_match("/^[0-9]{1,}$/", $_REQUEST['pass1'])) {
                    alert('密码必须由数字和字母组成');
                }
                if (preg_match("/^[a-zA-Z]{1,}$/", $_REQUEST['pass1'])) {
                    alert('密码必须由数字和字母组成');
                }
                $password = md5($_REQUEST['pass1']);
                $time = $_SERVER['REQUEST_TIME'];

                $sql = "UPDATE adminuser SET passport='{$passport}', `password`='{$password}',username='{$username}',
				gid={$gid},mtime={$time},`else`='{$else}',password='{$password}',gids='{$platform}',channel='{$channel}',if_mail='{$if_mail}',if_develop='{$if_develop}'
				WHERE uid={$old_id}";
            } else {
                $time = $_SERVER['REQUEST_TIME'];
                $sql = "UPDATE adminuser SET passport='{$passport}',username='{$username}',
				gid={$gid},mtime={$time},`else`='{$else}',gids='{$platform}',channel='{$channel}',if_mail='{$if_mail}',if_develop='{$if_develop}'
				WHERE uid={$old_id}";
            }
            Admin::adminLog("编辑用户:$passport");
        }

        $db->query($sql);
        if ($db->getAffectRows() > 0) {
            alert('添加或修改成功', 'action_gateway.php?ctl=manage&act=user');
        } else {
            alert('添加或修改失败', '');
        }
    }

    /**
     * 用户-删除
     */
    public function delete_user()
    {
        //权限判断
        Auth::check_action(Auth::$USER_DEL);

        $id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : 0;
        if ($id <= 0) alert('参数错误', '');

        $db = Ext_Mysql::getInstance('admin');
        $row = $db->fetchOne("SELECT uid FROM adminuser WHERE uid={$id}");
        if (empty($row)) alert("原记录不存在", '');
        if ($row['super'] == 'YES') alert("不能删除超级管理员", '');
        $db->query("DELETE FROM adminuser WHERE uid={$id}");
        Admin::adminLog("删除用户:$id");
        alert('删除记录成功', '');
    }

    /**
     * 日志列表
     */
    public function log()
    {
        $arr_type = array(
            '-1' => '',
            0 => '常规',
            1 => '邮件',
            2 => '封号',
            3 => '禁言',
            4 => '登陆',
            5 => '其他',
        );
        $db = Ext_Mysql::getInstance('admin');

        $kw_type = isset($_REQUEST['kw_type']) ? (int)$_REQUEST['kw_type'] : 0;
        $kw_info = isset($_REQUEST['kw_info']) ? (int)$_REQUEST['kw_info'] : 0;
        $kw_keyword = isset($_REQUEST['kw_keyword']) ? htmlspecialchars(trim($_REQUEST['kw_keyword'])) : '';
        $kw_start_time = isset($_REQUEST['kw_start_time']) ? $_REQUEST['kw_start_time'] : 0;
        $kw_end_time = isset($_REQUEST['kw_end_time']) ? $_REQUEST['kw_end_time'] : 0;
        $this->smarty->assign(array(
            'kw_type' => $kw_type,
            'kw_info' => $kw_info,
            'kw_keyword' => $kw_keyword,
            'kw_start_time' => $kw_start_time,
            'kw_end_time' => $kw_end_time,
        ));

        $sql = '1=1';
        if ($kw_type > -1) $sql .= " AND type={$kw_type}";
        if ($kw_start_time > 0) {
            try {
                $start_time = strtotime($kw_start_time);
                $sql .= " AND ctime>={$start_time}";
            } catch (Exception $e) {

            }
        }
        if ($kw_end_time > 0) {
            try {
                $end_time = strtotime($kw_end_time);
                $sql .= " AND ctime<={$end_time}";
            } catch (Exception $e) {

            }
        }
        if ($kw_keyword != '' && $kw_info > 0) {
            switch ($kw_info) {
                //管理员id
                case 1:
                    $uid = (int)$kw_keyword;
                    $sql .= " AND uid={$uid}";
                    break;

                //管理员名称
                case 2:
                    $sql .= " AND username='{$kw_keyword}'";
                    break;

                //操作IP
                case 3:
                    $sql .= " AND ip='{$kw_keyword}'";
                    break;
            }
        } elseif ($kw_keyword != '') {
            $sql .= " and `text` like '%$kw_keyword%'";
        }
        $total_record = $db->fetchOne("SELECT COUNT(id) AS total FROM adminlog WHERE {$sql}");
        $total_record = $total_record['total'];
        if ($total_record > 0) {
            $url = "action_gateway.php?ctl=manage&act=log&kw_type={$kw_type}&kw_info={$kw_info}&kw_keyword={$kw_keyword}&kw_start_time={$kw_start_time}&kw_end_time={$kw_end_time}";

            $per_page = 50;
            $total_page = ceil($total_record / $per_page);
            $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
            $cur_page = $cur_page < 1 ? 1 : $cur_page;
            $limit_start = ($cur_page - 1) * $per_page;

            $fetch_rows = $db->fetchRow("SELECT * FROM adminlog WHERE {$sql} order by ctime desc LIMIT {$limit_start}, $per_page");

            $assign_records = array();
            if ($fetch_rows) {
                foreach ($fetch_rows as $fetch_row) {
                    $fetch_row['action_name'] = isset($arr_type[$fetch_row['type']]) ? $arr_type[$fetch_row['type']] : '--';
                    $assign_records[] = $fetch_row;
                }
            }

            $this->smarty->assign(
                array(
                    "pages" => array(
                        "curpage" => $cur_page,
                        "totalpage" => $total_page,
                        "data" => $assign_records,
                        "totalnumber" => $total_record,
                        "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
                    )
                )
            );

        }

        $this->smarty->assign(array(
            'arr_type' => $arr_type,
            'arr_info' => array(0 => '', 1 => '管理员ID', 2 => '管理员名称', 3 => '操作IP'),
        ));
        $this->smarty->display('manage_log.shtml');
    }

    public function cleantimes()
    {
        Auth::check_action(Auth::$USER_EDIT);
        $this->admin_db = Ext_Mysql::getInstance('admin');
        $id = $_GET['id'];
        if (empty($id)) {
            alert('用户错误');
        }
        $sql = "update adminuser set error_num = 0 where uid = '$id'";
        $this->admindb->query($sql);
        Admin::adminLog("清除用户错误密码次数:$id");
        alert('清除错误密码次数成功');
    }

    /**
     * 用户-删除
     */
    public function delete_log()
    {
        //权限判断
        Auth::check_action(Auth::$LOG_MAN);
        //假如是代理平台查看。则只能查看属于他们添加的服

        $time = strtotime("-30 days");
        Ext_Mysql::getInstance('admin')->query("DELETE FROM adminlog WHERE ctime<{$time}");
        Admin::adminLog("删除后台操作日志30天");
        alert('删除记录成功', '');
    }

    /**
     * 后台操作日志
     */
    function log_debug()
    {
        Auth::check_action(Auth::$LOG_MAN);
        $this->display(CENTER_TEMP . 'log_debug.shtml', array());
    }

    /**
     * 动态获取日志
     */
    function getLogAjax()
    {
        Auth::check_action(Auth::$LOG_MAN);
        $file = LOG_DIR . 'admin/' . date('Ym') . '/' . date('Ymd') . '.log';
        if (!is_file($file)) {
            return exit(json_encode(array('state' => 0, 'msg' => '日志文件不存在！')));
        }
        $point = intval(request('point'));
        $endLine = count(file($file));
        if (!$point) {
            $startLine = $endLine - 500;
            if ($startLine <= 0) $startLine = 1;
        } else {
            $startLine = $point;
            if ($startLine == $endLine) {
                exit(json_encode(array('state' => 1, 'cont' => '', 'point' => $endLine)));
            }
        }
        $content = $this->getFileLines($file, $startLine, $endLine);

        exit(json_encode(array('state' => 1, 'cont' => $content, 'point' => $endLine)));
    }

    /**
     * 通过行号获取日志
     * @param $filename
     * @param int $startLine
     * @param int $endLine
     * @param string $method
     * @return array|mixed|string
     */
    private function getFileLines($filename, $startLine = 1, $endLine = 50, $method = 'rb')
    {
        $content = array();

        if (version_compare(PHP_VERSION, '5.1.0', '>=')) { // 判断php版本（因为要用到SplFileObject，PHP>=5.1.0）
            $count = $endLine - $startLine;
            $fp = new SplFileObject($filename, $method);
            $fp->seek($startLine - 1); // 转到第N行, seek方法参数从0开始计数
            for ($i = 0; $i <= $count; ++$i) {
                $content[] = $fp->current(); // current()获取当前行内容
                $fp->next(); // 下一行
            }
        } else { //PHP<5.1
            $fp = fopen($filename, $method);
            if (!$fp)
                return 'error:can not read file';
            for ($i = 1; $i < $startLine; ++$i) { // 跳过前$startLine行
                fgets($fp);
            }

            for ($i; $i <= $endLine; ++$i) {
                $content[] = fgets($fp); // 读取文件行内容
            }
            fclose($fp);
        }
        $content = array_filter($content); // array_filter过滤：false,null,''
        $content = implode('<br>', $content);
        $content = str_replace('Error', '<span style="color:red;font-weight: bold;">Error</span>', $content);

        return $content;
    }

    public function get_user_info(){

        $uid = $_SESSION['uid'];
        $sql = "select uid,passport,username,gid,gids,last_ip,`else`,FROM_UNIXTIME(login_time) as login_time,FROM_UNIXTIME(ctime) as ctime,FROM_UNIXTIME(mtime) as mtime,channel,status from adminuser where uid = $uid";
        $res = $this->admindb->fetchOne($sql);

        $gid = $res['gid'];
        $sql="select gname from admingroup Where gid = $gid";
        $group_res = $this->admindb->fetchOne($sql);
        $assign['user'] = $res;
        $assign['group'] = $group_res;

        $this->display('user_info.shtml',$assign);

    }

    public function update_user_password(){

        $old_pwd = request('old_pwd','str');
        $new_pwd = request('new_pwd','str');
        $new_pwd_re = request('new_pwd_re','str');

        if(empty($old_pwd)||empty($new_pwd)||empty($new_pwd_re)){
            alert('密码不能为空！');
        }

        if($new_pwd != $new_pwd_re){
            alert('两次密码不一致！');
        }

        if(strlen($new_pwd) <8){
            alert('新密码长度必须大于8');
        }

        if (preg_match("/^[0-9]{1,}$/", $new_pwd)) {
            alert('密码必须由数字和字母组成');
        }
        if (preg_match("/^[a-zA-Z]{1,}$/", $new_pwd)) {
            alert('密码必须由数字和字母组成');
        }

        $old_encrypt = md5($old_pwd);
        $new_encrypt = md5($new_pwd);

        $uid = $_SESSION['uid'];
        $sql = "select 1 from adminuser where uid=$uid and password='$old_encrypt'";
        $isExit = $this->admindb->fetchOne($sql);
        if(empty($isExit)){
            alert('原密码错误！');
        }
        $time = time();
        $update_sql = "update adminuser set password = '$new_encrypt',mtime = $time where uid = $uid";
        $code = $this->admindb->query($update_sql);
        if($code >=0){
            unset($_SESSION['uid'], $_SESSION['grights'], $_SESSION['username'], $_SESSION['super'], $_SESSION['gid'], $_SESSION['grights'], $_SESSION['auth_action'],$_SESSION['gids'],$_SESSION['channel'],$_SESSION['admin_db_link'],$_SESSION['selected_sid']);
            session_destroy();
            alert('更新成功！请重新登陆！','./login.php');
        }else{
            alert('更新失败！');
        }
    }

    /**
     * 禁用解禁用户
     */
    public function change_user_state(){
        if($_SESSION['super'] != 'YES'){
            alert('您没权限操作!');
        }
        $uid = request('uid','int');
        $state = request('state','int');
        if(!$uid || !$state || !in_array($state,array(1,2))){
            alert('非法请求!');
        }
        $sql = "select status from adminuser where uid={$uid}";
        $info = $this->admindb->fetchOne($sql);
        if(!$info){
            alert('找不到用户');
        }
        $sql = "update adminuser set status = $state where uid = $uid";
        $re = $this->admindb->query($sql);
        if($re){
            Admin::adminLog("禁用用户$uid");
            alert('success');
        }
        alert('fail !');
    }
}