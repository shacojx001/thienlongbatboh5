<?php
/**
 * 留存
 * User: wtf
 * Date: 17/11/21
 * Time: 下午5:04
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_center_remain extends action_superclass
{
    /**
     * 付费帐号留存
     */
    public function paid_accname()
    {
        $type = request('type','int');
        
        $name = array(
            0=>Ext_Template::lang('付费帐号数'),
            1=>Ext_Template::lang('新增付费帐号数'),
        );
        $table = array(
            0=>'center_remaining_paid_accname',
            1=>'center_remaining_newly_paid_accname',
        );
        $sd = $_POST['sd'] ? $_POST['sd'] : $_GET['sd'];
        $ed = $_POST['ed'] ? $_POST['ed'] : $_GET['ed'];

        if (!$sd) $sd = date('Y-m') . '-01';
        if (!$ed) $ed = date('Y-m-d');
        $st = strtotime($sd);
        $et = strtotime($ed);

        $sql = "select * from {$table[$type]} where (time>=$st and time<$et+86400) order by time desc";
        $re = $this->admindb->fetchRow($sql);
        $list = array();
        $pay_total = 0;
        $total = array();
        foreach ($re as $v) {
            $login_data = json_decode($v['login_data'],true);
            $pay_total += $v['pay_num'];
            foreach ($login_data as $day => $lg) {
                $total[$day] += $lg;
                $login_data[$day] = round($lg/$v['pay_num'],4) *100 .'%';
            }
            $list[] = array(
                'ymd'=> date('Y-m-d',$v['time']),
                'pay_num'=>$v['pay_num'],
                'login_data'=> $login_data,
            );
        }

        foreach ($total as $day=>$v) {
            $total[$day]= round($v/$pay_total,4) *100 .'%';
        }

        //前一个月
        $last_sd = date("Y-m-01", strtotime("-1 months", strtotime($sd)));
        $last_ed = date("Y-m-d", strtotime("+1 months", strtotime($last_sd)) - 86400);

        //后一个月
        $next_sd = date("Y-m-01", strtotime("+1 months", strtotime($sd)));
        $next_ed = date("Y-m-d", strtotime("+1 months", strtotime($next_sd)) - 86400);

        $assign['last_ed'] = $last_ed;
        $assign['last_sd'] = $last_sd;
        $assign['next_sd'] = $next_sd;
        $assign['next_ed'] = $next_ed;
        $assign['sd'] = $sd;
        $assign['ed'] = $ed;
        $assign['list'] = $list;
        $assign['total'] = $total;
        $assign['pay_total'] = $pay_total;
        $assign['name']=$name[$type];
        $this->display('center_remain.paid_accname.shtml', $assign);
    }



}