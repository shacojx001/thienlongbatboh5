<?php
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_chat extends action_superclass
{   
    public function cs(){
        include_once(LIB_DIR . 'Tools/Redis.php');
        $redis = Redis_Server::getInstance();
  
        $redis->push('key','测试');
        $data = $redis->pop('key');
print_r($data);exit;
    }
    /**
     * 聊天监控关键词规则
     */
    public function rule(){
        $assign = array();
        $keywords = request('keywords');
        if($keywords){
            $lvs = request('lvs','int');
            $lve = request('lve','int');
            $pays = request('pays','int');
            $paye = request('paye','int');
            $num = request('num','int');
            $ban_time = request('ban_time','int');
            $info = compact('lvs','lve','pays','paye','num','ban_time','ban_time');
            $json = json_encode($info);
            $sql = "delete from chat_rule";
            Config::centerdb()->query($sql);
            $sql = "insert into chat_rule (`rule`,`keywords`) values ('{$json}','{$keywords}')";
            $re = Config::centerdb()->query($sql);
            if(!$re){
                alert(Ext_Template::lang('操作失败'));
            }
            include_once(LIB_DIR . 'Tools/Redis.php');
            $redis = Redis_Server::getRedisInstance();
            $redis->set(REDIS_CHAT_RULE_KEY,json_encode($info));
            $redis->set(REDIS_CHAT_KEYWORD,$keywords);
            $assign['info'] = $info;
            $assign['keywords'] = $keywords;
        }else{
            $re = Config::centerdb()->fetchOne("select * from chat_rule");
            if($re){
                $assign['info'] = json_decode($re['rule'],true);
                $assign['keywords'] = $re['keywords'];
            }
        }

        $this->display('chat_rule.shtml', $assign);
    }

    /**
     * 监控页
     */
    public function index(){
        $assign = $this->get_server_rights();
        $assign['plat'] = json_encode($assign['srv_group']);
        $assign['servers'] = json_encode($assign['srv_server']);

        $assign['chat_channel'] = Goods::goods_list(9);
        $assign['chat_channel_json'] = json_encode($assign['chat_channel']);

        $this->display('chat.shtml', $assign);
    }

    /**
     * 聊天记录
     */
    public function chat_log(){
        $assign = $this->get_server_rights();
        $where = ' where 1';

        $st = request('st');
        $et = request('et');
        $keyword = request('keyword','str');
        if($keyword){
            $where .= " and message like '%{$keyword}%'";
        }

        if($st){
            $st = strtotime($st);
            $where .= " and log_time>=".$st;
        }else{
            $st = strtotime(date('Y-m-d'));
        }

        if($et){
            $et = strtotime($et)+86400;
            $where .= " and log_time<".$et;
        }else{
            $et = $st+86400;
        }
        $gid = request('gid','int');
        $sid = request('sid','int');
        $chat_channel = request('chat_channel','int');
        $role_id = request('role_id','str');
        if($chat_channel){
            $where .= " and channel_id=".$chat_channel;
        }
//        if($gid){
//            $where .= " and gid = $gid";
//        }
//        if($sid){
//            $where .= " and sid = $sid";
//        }
        $where .= $this->get_where_g($gid,$this->get_allgroup_list());
        $where .= $this->get_where_s($sid,$this->get_allserver_list());
        if($role_id){
            $where .= " and send_role_id ='".$role_id."'";
        }

        $assign['chat_channel'] = Goods::goods_list(9);

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from chat $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from chat $where order by log_time desc $limit";
        $list = $this->admindb->fetchRow($sql);
        if($list){

        }
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );
        $_GET['st'] = date('Y-m-d',$st);
        $_GET['et'] = date('Y-m-d',$et-86400);

        $this->display('chat_log.shtml', $assign);
    }

    //聊天屏蔽词
    public function chat_game_rule(){
        $re = Config::centerdb()->fetchOne("select * from chat_game_rule");
        $assign = array();
        if(!empty($re)){
            $assign = $re;
        }

        $this->display('chat/chat_rule.shtml', $assign);
    }

    public function chat_game_rule_action(){
        $word = request('word');
        if(empty($word)){
            exit(json_encode(array('state'=>400,'msg'=>'参数不能空')));
        }
        $sql = "delete from chat_game_rule";
        Config::centerdb()->query($sql);
        $sql = "insert into chat_game_rule (`word`) values ('{$word}')";
        $re = Config::centerdb()->query($sql);
        Admin::adminLog("修改服务端聊天屏蔽词:".$re);
        if(empty($re)){
            exit(json_encode(array('state'=>400,'msg'=>Ext_Template::lang('保存错误'))));
        }
        exit(json_encode(array('state'=>200,'msg'=>Ext_Template::lang('保存成功'))));
    }

    public function chat_game_rule_server(){
        $re = Config::centerdb()->fetchOne("select * from chat_game_rule");
        $assign = array();
        if(!empty($re)){
            $assign = array_filter(explode("\n",$re['word']));
        }

        $arr = array();
        foreach($assign as $k=>$v){
            if(!empty($v)){
                $arr[] = "('".addcslashes($v,"'")."')";
            }
        }

        if( !empty($arr) ){
            $arr = implode(',',$arr);
            $sql = 'insert into base_word (`word`) VALUES  '.$arr.' ';
        }

        $serverSql = "select gid,sname,id,sdb from adminserverlist WHERE `default` in (1,4) and main_id=0 ";
        $serverlist = $this->admindb->fetchRow($serverSql);
        $game_erlang_ste = array();
        if(!empty($serverlist)){
            Admin::adminLog("聊天屏蔽词同步到服务端");
            foreach( $serverlist as $k1=>$v1 ){
                Config::gamedb($v1['id'])->query('delete from base_word');
                if( !empty($arr) ) {
                    Config::gamedb($v1['id'])->query($sql);
                }
                $ste = Erlang::erl_update_chat_word($v1['id']);
            
                if ($ste != 'ok') {
                    $game_erlang_ste[] = '服务器' . $v1['id'] . '同步失败：' . json_decode($ste);
                }

            }
            if(!empty($game_erlang_ste)){
                exit(json_encode(array('state'=>400,'msg'=>implode('<br/>',$game_erlang_ste))));
            }
            exit(json_encode(array('state'=>200,'msg'=>'同步成功') ));
        }
        exit(json_encode(array('state'=>400,'msg'=>'要同步的服务器列表为空') ));
    }



}