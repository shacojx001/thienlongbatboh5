<?php
/**
 * author fan
 * LTV 生命周期价值
 */
require_once dirname(__FILE__) . '/base.php';

class ltv extends base
{
    public function handle()
    {
        exit('迷路了啊 小伙子@ !!!!');
    }

    /**
     * 分渠道 、 统计帐号ltv
     */
    public function accname_channel()
    {
        $type = request('type','int');
        $type = $type?$type:0;

        $table = array(
            0=>'center_ltv_accname_channel',
            1=>'center_ltv_role_channel',
        );

        $where = "1";
        $sd = $_POST['sd'] ? $_POST['sd'] : $_GET['sd'];
        $ed = $_POST['ed'] ? $_POST['ed'] : $_GET['ed'];

        if (!$sd) $sd = date('Y-m') . '-01';
        if (!$ed) $ed = date('Y-m-d');
        $st = strtotime($sd);
        $et = strtotime($ed);
        if($st){
            $where .= " and `time` >= ".$st;
        }
        if($et){
            $where .= " and `time` < ". ($et + 86400);
        }

        $channel = request('channel','str');
        if($channel){
            $where .= " and `group` = {$channel}";
        }

        $sql = "select * from {$table[$type]} where {$where} order by `time` desc";
        $re = $this->admindb->fetchRow($sql);
        $re = $this->merge_row($re);
        $list = $time_daily_pay_num = $daily_total_money = $daily_total_reg = array();
        $total_reg = 0;
        foreach ($re as $v) {
            //所有日期总注册
            $total_reg += $v['reg_num'];
            $tmp_data = $v['data'];
            foreach ($v['data'] as $day => $money) {
                //汇总第N天总注册
                $daily_total_reg[$day] += $v['reg_num'];
                //汇总每N天总充值
                $daily_total_money[$day] += $money[2];
                //汇总每日所有充值人数
                $time_daily_pay_num[$v['time']] += $money[0];
                $tmp_data[$day] = array(
                    'money' => '¥'.$money[2],
                    'ltv' => sprintf('%.2f',$money[2]/$v['reg_num']),
                    //'pay_num'=> $money[0],
                    //'daily_money'=> '¥'.$money[1],
                );
            }
            $list[$v['time']] = array(
                'ymd'=> date('Y-m-d',$v['time']),
                'reg_num'=>$v['reg_num'],
                'data'=> $tmp_data,
            );
        }
        foreach ($list as $time=>$v) {
            $list[$time]['rate'] = sprintf('%.4f',$time_daily_pay_num[$time] / $v['reg_num'],4) * 100 . '%';
        }

        $daily_total_ltv = array();
        foreach ($daily_total_money as $day=>$v) {
            $daily_total_ltv[$day] = sprintf('%.2f',$v/$daily_total_reg[$day]);
        }

        //前一个月
        $last_sd = date("Y-m-01", strtotime("-1 months", strtotime($sd)));
        $last_ed = date("Y-m-d", strtotime("+1 months", strtotime($last_sd)) - 86400);

        //后一个月
        $next_sd = date("Y-m-01", strtotime("+1 months", strtotime($sd)));
        $next_ed = date("Y-m-d", strtotime("+1 months", strtotime($next_sd)) - 86400);

        $assign['last_ed'] = $last_ed;
        $assign['last_sd'] = $last_sd;
        $assign['next_sd'] = $next_sd;
        $assign['next_ed'] = $next_ed;
        $assign['sd'] = $sd;
        $assign['ed'] = $ed;

        $assign['list'] = $list;
        $assign['daily_total_ltv'] = $daily_total_ltv;
        $assign['daily_total_money'] = $daily_total_money;
        $assign['total_reg'] = $total_reg;
        $assign['channel_list'] = $this->get_channel_list();
        $assign['channel'] = $channel;

        $this->display('center/ltv_accname_channel.shtml', $assign);
    }

    /**
     * 合并list
     * @param $list
     * @return array
     */
    public function merge_row($list){
        $rows = array();
        foreach ($list as $v) {
            $json = json_decode($v['data'],true);
            $v['data'] = $json;
            $rows[$v['time']][] = $v;
        }

        $return = array();
        foreach ($rows as $time=>$row) {
            if(count($row)>1){
                $tmp = array();
                $tmp[$time]['time'] = $time;
                $tmp_data = array();
                foreach ($row as $v) {
                    $tmp[$time]['reg_num'] += $v['reg_num'];
                    foreach ($v['data'] as $day => $val) {
                        $tmp_data[$day][0] += $val[0];
                        $tmp_data[$day][1] += $val[1];
                        $tmp_data[$day][2] += $val[2];
                    }
                }
                $tmp[$time]['data'] = $tmp_data;
                $return = array_merge($return,$tmp);
            }else{
                $return = array_merge($return,$row);
            }
        }

        ksort($return);
        return $return;
    }
}




































































































