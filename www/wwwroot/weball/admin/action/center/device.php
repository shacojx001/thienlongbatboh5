<?php
/**
 * author fan
 * LTV 生命周期价值
 */
require_once dirname(__FILE__) . '/base.php';

class device extends base
{
    public function handle()
    {
        exit('迷路了啊 小伙子@ !!!!');
    }

    /**
     * 设备流失统计
     */
    public function loss()
    {
        $where = "1";
        $sd = $_POST['sd'] ? $_POST['sd'] : $_GET['sd'];
        $ed = $_POST['ed'] ? $_POST['ed'] : $_GET['ed'];

        if (!$sd) $sd = date('Y-m') . '-01';
        if (!$ed) $ed = date('Y-m-d');
        $st = strtotime($sd);
        $et = strtotime($ed);
        if($st){
            $where .= " and `time` >= ".$st;
        }
        if($et){
            $where .= " and `time` < ". ($et + 86400);
        }

        $channel = request('channel','str');
        if($channel){
            $where .= " and `channel` = {$channel}";
        }

        $list = array();
        $sql = "select channel,pos,count(1) as c from device_loss where {$where} group by channel,pos";
        $re = $this->centerdb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['channel']][] = $v;
            }
        }

        $_list = array();
        foreach ($list as $channel => $v) {
            $sort = array();
            foreach ($v as $val) {
                $sort[$val['pos']] = $val['c'];
            }

            $_list[$channel][1] = $this->loss_count($sort[1],$sort[2]);
            $_list[$channel][2] = $this->loss_count($sort[2],$sort[3]);
            $_list[$channel][3] = $this->loss_count($sort[3],$sort[4]);
            $_list[$channel][4] = (int)$sort[4];

            $_list[$channel][0] = 0;
            foreach ($_list[$channel] as $val) {
                $_list[$channel][0] += $val;
            }
        }

        //前一个月
        $last_sd = date("Y-m-01", strtotime("-1 months", strtotime($sd)));
        $last_ed = date("Y-m-d", strtotime("+1 months", strtotime($last_sd)) - 86400);

        //后一个月
        $next_sd = date("Y-m-01", strtotime("+1 months", strtotime($sd)));
        $next_ed = date("Y-m-d", strtotime("+1 months", strtotime($next_sd)) - 86400);

        $assign['last_ed'] = $last_ed;
        $assign['last_sd'] = $last_sd;
        $assign['next_sd'] = $next_sd;
        $assign['next_ed'] = $next_ed;
        $assign['sd'] = $sd;
        $assign['ed'] = $ed;

        $assign['list'] = $_list;
        $assign['channel_list'] = $this->get_channel_list();
        $assign['channel'] = $channel;

        $this->display('center/device_loss.shtml', $assign);
    }

    public function loss_count($pre,$next){
        $loss = $pre - $next;
        if($loss<0 || !$loss) $loss = 0;

        return $loss;
    }

}




































































































