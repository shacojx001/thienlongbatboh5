<?php
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_feedback extends action_superclass
{
    // 意见收集(全专服)
    function role_questionnaire_all()
    {
        //获取服务器权限
        $assign = $this->get_server_rights();
        $gid_list = $assign['srv_group'];
        $sid_list = $assign['srv_server'];      //获取服务器列表
        //关键词保存文本

        //参数
        $gid = request('gid', 'int');
        $sid = request('sid', 'int');


        $type = request('type', 'int');
        $begin_time = request('begin_time', 'str');
        $end_time = request('end_time', 'str');
        $key_word = request('key_word', 'str');
        $per_page = request('per_page', 'int');
        $page = request('page', 'int');
        $otime = request('otime', 'str');
        $role_id = request('role_id', 'str');
        //获取平台，服务器sql
        $where = "WHERE 1 ";
        $where .= $this->get_where_g($gid, $gid_list);
        $where .= $this->get_where_s($sid, $sid_list);

        if ($type == 1) {        //未回复
            $where .= " AND  context!='' and state = 0 ";
        } elseif ($type == 2) {     //已回复
            $where .= " AND  context!='' and reply!='' and state =1 ";
        } elseif ($type == 3) {    //全部
            $where .= " ";
        } elseif ($type == 4) {    //看空白
            $where = " WHERE 1 AND  context='' ";
        }

        //日期晒选
        if ($begin_time && empty($end_time)) {
            $begin_time = strtotime($begin_time);
            $where .= " and commit_time>=$begin_time ";

        } elseif ($begin_time && $end_time) {
            $begin_time = strtotime($begin_time);
            $end_time = strtotime($end_time);
            $where .= " and commit_time between $begin_time and $end_time ";
        } else {
            $begin_time = strtotime(date("Y-m-d"));
            $end_time = $begin_time + 86400;

            $where .= " and commit_time between $begin_time and $end_time ";

        }

        //角色ID
        if(!empty($role_id)){
            $where .= " and role_id = '$role_id'";
        }

        //关键词
        if ($key_word && $key_word != 1) {
            $str = $key_word;
            $where .= " AND content REGEXP '$str'";

            $assign['key_word'] = $key_word;
        }

        $url = "action_gateway.php?";

        //查询分页总数
        $sql = "SELECT
                        count(*) AS num
                FROM
                        player_suggestion
                $where
                        ";
        $res = $this->admindb->fetchOne($sql);

        $total_record = $res['num'];
        $per_page = $per_page ? $per_page : 20;

        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($page) ? $page : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $startRow = ($cur_page - 1) * $per_page;


        $sql = "SELECT
                        id as log_id,
                        gid,
                        sid,
                        role_id,
                        nickname,
                        role_lv as lv,
                        combat_power as power,
                        FROM_UNIXTIME(commit_time) AS commit_time,
                        star,
                        state,
                        content as context,
                        reply,
                         FROM_UNIXTIME(reply_time) AS reply_time
                      FROM
                        player_suggestion
                      $where
                      ORDER BY
                        commit_time DESC
                      LIMIT $startRow,$per_page
                      ";
        $res = $this->admindb->fetchRow($sql);

        if (empty($sid_list)) {
            alert("暂无数据");
            exit;
        }

        foreach ($res as $key => $value) {       //加入平台信息，服务器信息
            $res[$key]['sname'] = $sid_list[$value['sid']];
            $res[$key]['gname'] = $gid_list[$value['gid']];
        }

        $assign['res'] = $res;

        $assign['content'] = array(
            1 => "谢谢参与！",
            2 => "非常感谢您的参与！",
        );

        $assign['pages'] = array(
            "per_page" => $per_page,
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_REQUEST) . "&page=",
        );


        $this->display('feedback_all.shtml', $assign);
    }


    function file_put_save()
    {

        if (empty($_POST['str']) || $_POST['str'] == 1 || empty($_POST['txt_name'])) {
            echo '内容为空！';
            exit;
        } else {
            $file = $_POST['txt_name'] . '.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个

            $content = $_POST['str'];
            file_put_contents($file, $content);// 这个函数支持版本(PHP 5)
            echo 'ok';
            exit;
        }
    }

//回复
    function question_reply_all()
    {
        //参数
        $ids = request('id', 'arr');
        $allreply = request('allreply', 'str');
        $alltemp = request('alltemp', 'str');

        $reply = $allreply ? $allreply : $alltemp;

        $title = "GM回复";


        $sid_list = array();
        $id_list = array();
        foreach ($ids as $key => $val) {
            $arr = explode('_', $val);      //前端传过来的参数是  id_sid_roleid
            if (!isset($sid_list[$arr[1]])) {
                $sid_list[$arr[1]] = array();
            }
            array_push($sid_list[$arr[1]], $arr[2]);  //将role_id根据sid分开
            array_push($id_list, $arr[0]);            //记录反馈id

        }
        if (empty($sid_list)) {
            alert("请选择回复对象！");
            exit;

        }
        if (empty($reply)) {
            alert("请输入回复内容！");
            exit;
        }

        // 发邮件给玩家
        foreach ($sid_list as $key => $val) {
            $id_list_str = "[" . implode(",", $val) . "]";      //将role_id拼接
            $result = Erlang::erl_send_mail_new($key, $id_list_str, $title, $reply, '[]');
        }

        // 更新回复状态
        $time = time();
        foreach ($id_list as $key => $val) {
            $sql = "UPDATE questionnaire_log
                    SET reply = '$reply'
                    WHERE
                      log_id IN ($val)";

            $this->admindb->query($sql);
        }

        alert('发送成功！');
    }

    // 意见收集(本服)
    function role_questionnaire_server()
    {
        //参数
        $type = request('type', 'int');
        $begin_time = request('begin_time', 'str');
        $end_time = request('end_time', 'str');
        $key_word = request('key_word', 'str');
        $per_page = request('per_page', 'int');
        $page = request('page', 'int');
        $sid = $_SESSION['selected_sid'];
        $role_id = request('role_id', 'str');

        $where = "WHERE 1 ";

        if ($type == 1) {         //未回复
            $where .= " AND  context!='' and reply is null ";
        } elseif ($type == 2) {          //已回复
            $where .= " AND  context!='' and reply is null ";
        } elseif ($type == 3) {       //全部
            $where .= " ";
        } elseif ($type == 4) {
            $where = " WHERE 1 AND  context='' ";//看空白
        }

        if ($begin_time && empty($end_time)) {
            $begin_time = strtotime($begin_time);
            $where .= " and commit_time>=$begin_time ";

        } elseif ($begin_time && $end_time) {
            $begin_time = strtotime($begin_time);
            $end_time = strtotime($end_time);
            $where .= " and commit_time between $begin_time and $end_time ";
        } else {
            $begin_time = strtotime(date("Y-m-d"));
            $end_time = $begin_time + 86400;

            $where .= " and commit_time between $begin_time and $end_time ";

        }

        //角色ID
        if(!empty($role_id)){
            $where .= " and role_id = '$role_id'";
        }

        //关键词查找
        if ($key_word){
            $str = $key_word;
            $where .= " AND content REGEXP '$str'";

            $assign['key_word'] = $key_word;
        }

        $url = "action_gateway.php?";

        //查询分页总数
        $sql = "SELECT
                        count(*) AS num
                FROM
                        questionnaire_log
                $where
                        ";
        $res = $this->admindb->fetchOne($sql);

        // 分页
        $total_record = $res['num'];
        $per_page = $per_page ? $per_page : 20;

        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($page) ? $page : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $startRow = ($cur_page - 1) * $per_page;


        $sql = "SELECT
                        log_id,
                        role_id,
                        id,
                        server_num,
                        nickname,
                        lv,
                        power,
                        FROM_UNIXTIME(commit_time) commit_time,
                        star,
                        context
                    FROM
                        questionnaire_log
                      $where
                      ORDER BY
                        commit_time DESC
                      LIMIT $startRow,$per_page
                      ";
        $res = $this->db->fetchRow($sql);

// print_r($res);exit;
        //加入游戏服信息，平台信息 
        foreach ($res as $key => $value) {
            $res[$key]['sid'] = $_SESSION['selected_sid'];
        }
        //结果
        $assign['res'] = $res;


        $assign['content'] = array(
            1 => "谢谢参与！",
            2 => "非常感谢您的参与！",
        );
        $assign['sid'] = $sid;
        $assign['pages'] = array(
            "per_page" => $per_page,
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_REQUEST) . "&page=",
        );

        $this->display('feedback_server.shtml', $assign);
    }
}