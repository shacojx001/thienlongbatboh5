<?php
/**
 * 游戏配置
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_game extends action_superclass
{
    public function base_game()
    {

        //管理员合法性判断
        $sql = "select * from `base_game` ";
        $this->db->query($sql);
        $result = $this->db->getRes();

//平台列表
        $sql = "select gid,platform_str,platform_name from adminplatformlist ";
        $list = $this->admindb->fetchRow($sql);


        $data = array(
            'version' => '',//开服时间
            'game_domain' => '',//域名
            'game_title' => '',//游戏服名
            'erl_cookie' => '',
            'erl_node' => '',
            'sdb' => '',//数据库连接
            'sid' => '',//服务器列表自增ID
            'gid' => '',//平台ID
            'channel_list' => '',//渠道列
            'ip'=>'',
            'port'=>'',
        );

        while ($row = mysql_fetch_assoc($result)) {
            if (is_numeric($row['cf_value']) == FALSE) $data[$row['cf_name']] = $row['cf_value'];
            else $data[$row['cf_name']] = $row['cf_value'];
        }


        $assign['link_sign'] = $_SESSION['admin_db_link'];
        $assign['data'] = $data;
        $assign['list'] = $list;
        $this->display("base_game.shtml", $assign);
    }

    public function base_game_add()
    {
        $game_data = array(
            'version' => '',//开服时间
            'game_domain' => '',//域名
            'game_title' => '',//游戏服名
            'sdb' => '',//数据库连接
            'sid' => '',//服务器列表自增ID
            'gid' => '',//平台ID
            'channel_list' => '',//渠道列
            'push_url' => '',//推送url
        );
        $key_data = array(
            'erl_cookie' => '',
            'erl_node' => '',

        );
        $link_sign = request('link_sign', 'str');
        if ($_SESSION['admin_db_link'] != $link_sign) {
            alert('修改内容与当前服不对应，请检查服务器是否正确');
        }

        switch ($_REQUEST['ac']) {
            //修改参数
            case "edit_game":
                //用户保存游戏配置权限
                Auth::check_action(Auth::$SAVA_BASE_GAME);
                foreach ($game_data as $k => $v) {
                    $val = trim(preg_replace("/[\'\"]+/", "", $_REQUEST[$k]));
                    $sql = "replace into `base_game` set cf_name='$k', cf_value='$val' ";
                    $this->db->query($sql);
                }

                Admin::adminLog("修改游戏参数 js");
                alert("保存成功！");
                break;
            case "edit_key":
                //用户保存游戏配置权限
                Auth::check_action(Auth::$SAVA_BASE_KEY);
                foreach ($key_data as $k => $v) {
                    $val = trim(preg_replace("/[\'\"]+/", "", $_REQUEST[$k]));
                    $sql = "replace into `base_game` set cf_name='$k', cf_value='$val' ";
                    $this->db->query($sql);
                }
                Admin::adminLog("修改服务器参数 php");
                alert("保存成功！");
                break;
            default:
                break;
        }

    }

    public function product_config()
    {

        $assign = $this->get_server_rights();
        $c_list = $assign['channel_list'];

        $channel = request('channel', 'str');
        $per_page = request('per_page', 'int');
        $page = request('page', 'int');
        $where = " Where 1 ";
        $where .= $this->get_where_c($channel, $c_list, 'channel');

        $url = "action_gateway.php?";

        //查询分页总数
        $sql = "SELECT
                        count(*) AS num
                FROM
                        product_conf
                $where
                and is_del = 0
                        ";
        $res = $this->admindb->fetchOne($sql);

        $total_record = $res['num'];
        $per_page = $per_page ? $per_page : 20;

        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($page) ? $page : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $startRow = ($cur_page - 1) * $per_page;

        $sql = "select * from product_conf $where and is_del = 0 order by channel asc ,gold asc  LIMIT $startRow,$per_page";
        $res = $this->admindb->fetchRow($sql);
        $assign['list'] = $res;

        $assign['pages'] = array(
            "per_page" => $per_page,
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_REQUEST) . "&page=",
        );


        $this->display("product_config.shtml", $assign);
    }

    public function add_product_config()
    {
        Admin::adminLog("增加商品配置");
        Auth::check_action(Auth::$ADD_PRODUCT_CONFIG);

        $product_id = request('product_id', 'str');
        $gold = request('gold', 'int');
        $money = request('money', 'int');
        $channel = request('channel', 'str');

        if (empty($channel)) {
            alert("请选择一个渠道!");
            exit();
        }

        $sql = "select 1 from product_conf where channel = '$channel' and product_id = '$product_id'";
        $res = $this->admindb->fetchOne($sql);
        if ($res) {
            alert("商品id重复！");
            exit();
        }
        $sql = "insert into product_conf(channel,product_id,gold,money,is_del) values('$channel','$product_id',$gold,$money,0)";
        $res = $this->admindb->query($sql);
        if ($res >= 1) {
            alert("添加成功！");
            exit();
        } else {
            alert("添加失败！");
            exit();
        }

    }

    public function update_product_config()
    {
        Admin::adminLog("更新商品配置");
        Auth::check_action(Auth::$UPDATE_PRODUCT_CONFIG);

        $product_id = request('product_id', 'str');
        $gold = request('gold', 'int');
        $money = request('money', 'int');
        $channel = request('channel', 'str');

        if (empty($channel)) {
            alert("请选择一个渠道!");
            exit();
        }

        $sql = "update product_conf set gold = $gold ,money=$money  where channel = '$channel' and product_id = '$product_id'";
        $res = $this->admindb->query($sql);
        if ($res >= 1) {
            alert("更新成功！");
            exit();
        } else {
            alert("更新失败！");
            exit();
        }

    }

    public function del_product_config()
    {
        Admin::adminLog("删除商品配置");
        Auth::check_action(Auth::$DEL_PRODUCT_CONFIG);

        $product_id = request('product_id', 'str');
        $channel = request('channel', 'str');

        if (empty($channel)) {
            alert("请选择一个渠道!");
            exit();
        }

        $sql = "update product_conf set is_del = 1 where channel = '$channel' and product_id = '$product_id'";
        $res = $this->admindb->query($sql);
        if ($res >= 1) {
            alert("删除成功！");
            exit();
        } else {
            alert("删除失败！");
            exit();
        }
    }

    public function build_product_config()
    {

        Admin::adminLog("生成商品配置");
        Auth::check_action(Auth::$BUILD_PRODUCT_CONFIG);

        $sql = "select * from product_conf where is_del = 0  order by channel asc,gold asc";
        $res = $this->admindb->fetchRow($sql);
        $last_channel = '';
        $channel_config = array();
        foreach ($res as $key => $value) {
            if ($last_channel != $value['channel']) {
                $channel_config[$value['channel']] = array();
            }
            $config = array(
                'gold' => $value['gold'],
                'money' => $value['money']
                );
            $channel_config[$value['channel']][$value['product_id']] = $config;
            $last_channel = $value['channel'];
        }
        $product_list = var_export($channel_config,true);


        $file_export = <<<CODE
<?php

\$product_list = $product_list;

?>
CODE;

        //加锁
        $file = CONFIG_DIR . 'product_cfg.php';
        $fp = fopen($file, 'w');
        if (flock($fp, LOCK_EX)) {//加锁
            fwrite($fp, $file_export);
            flock($fp, LOCK_UN);//解锁
        }
        fclose($fp);
        alert("生成成功！");
    }


}
