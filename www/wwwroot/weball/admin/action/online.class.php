<?php
/**
 * 在线
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_online extends action_superclass
{
	public function online_list(){
		$start_time = empty($_GET['start_time']) ? date('Y-m-d') : $_GET['start_time'];
		$_REQUEST['start_time'] = $start_time;
		$begtime = strtotime($start_time);
		$todayStartTime = strtotime(date('Y-m-d',time()));
		if( $begtime==$todayStartTime ){
			$endtime = time()-120;
		}else{
			$endtime = $begtime+86399;
		}

		$tmp = $this->admindb->fetchRow("select id,time,sum(online) as total from center_online_channel where time >= '{$begtime}' and time <= '{$endtime}' group by time order by time asc");
		if(!empty($tmp)){
			$online = array();
			$chartkeys = array();
			$chartval = array();
			foreach ($tmp as $key => $val){
				$online[date('H:i',$val['time'])] = $val['total'];
				$charkeys[] = date('H:i',$val['time']);
				$chartval[] = intval($val['total']);
			}
			$max = max($chartval);
			krsort($online);
		}
		$assign['dtime'] = $start_time;
		$assign['max'] = $max;
		//$assign['online'] = $online;
		$assign['charkeys'] =  json_encode($charkeys);
		$assign['chartval'] =  json_encode($chartval);
		$this->display('online_list.shtml',$assign);
	}
	
	public function ostatis_list(){
		if($_GET['start_month']&&!$_GET['start_day']){
			$month_or_day = 'month';
		}
		switch($month_or_day){
			case 'month': $assign = $this->get_online_for_month();break;
			default:$assign = $this->get_online_for_day();break;
		}
		$this->display('ostatis_list.shtml',$assign);
	}

	function get_online_for_month(){
		$start_time = empty($_GET['start_month'])? date('Y-m') : request('start_month','str','G');
		$_REQUEST['start_month'] = $start_time;
		$begtime = strtotime($start_time.'-01');
		$nextmonth = strtotime('+1 month',$begtime);
		$endtime = strtotime(date('Y-m-01',$nextmonth))-1;
		$sql = "SELECT ctime as timestamp,avg(total ) as avgnum,max(total ) as maxnum FROM (select ctime,sum(online) as total from log_online  group by ctime) as a WHERE ctime >= '$begtime' and ctime <= '$endtime' group by DATE_FORMAT(FROM_UNIXTIME(`ctime`),'%m-%d') order by ctime asc";

		$res = $this->db->fetchrow($sql);
		if(!empty($res)){
			$list = array();
			$chartavg = array();
			$chartmax = array();
			foreach ($res as $key => $val) {
				$list[date('m-d',$val['timestamp'])]['avg'] = floor($val['avgnum']);
				$list[date('m-d',$val['timestamp'])]['max'] = intval($val['maxnum']);
				$chartavg[date('m-d',$val['timestamp'])] = floor($val['avgnum']);
				$chartmax[date('m-d',$val['timestamp'])] = intval($val['maxnum']);
			}
		}
		$tlist = array();
		$tchartavg = array();
		$tchartmax = array();
		for($i=$begtime;$i<$endtime;$i=$i+86400){
			$tlist[] = date('d日',$i);
			$tchartavg[] = isset($chartavg[date('m-d',$i)]) ? $chartavg[date('m-d',$i)] : 0 ;
			$tchartmax[] = isset($chartmax[date('m-d',$i)]) ? $chartmax[date('m-d',$i)] : 0 ;
		}

		$assign['dtime'] = $start_time;
		$assign['list'] = $list;         //一个月每天的数据
		$assign['tlist'] = json_encode($tlist);
		$assign['tchartavg'] = json_encode($tchartavg);
		$assign['tchartmax'] = json_encode($tchartmax);
		$assign['month_or_day'] = 'month';
		return $assign;
	}
	function get_online_for_day(){
		$start_time = empty($_GET['start_day']) ? date('Y-m-d') : request('start_day','str','G');
		$_REQUEST['start_day'] = $start_time;
		Ext_Debug::log($_GET);
		$begtime = strtotime($start_time);
		$endtime = $begtime+86399;
		$tmp = $this->db->fetchRow("select ctime as timestamp,id,sum(online) as total from log_online where ctime >= '{$begtime}' and ctime <= '{$endtime}' group by ctime order by ctime asc");

		if(!empty($tmp)){
			$online = array();
			$chartkeys = array();
			$chartval = array();
			foreach ($tmp as $key => $val){
				$online[date('H:i',$val['timestamp'])] = $val['total'];
				$charkeys[] = date('H:i',$val['timestamp']);
				$chartval[] = intval($val['total']);
			}
			$max = max($chartval);
			krsort($online);
		}
		$assign['dtime'] = $start_time;
		$assign['max'] = $max;
		//$assign['online'] = $online;
		$assign['charkeys'] =  json_encode($charkeys);
		$assign['chartval'] =  json_encode($chartval);
		$assign['month_or_day'] = 'day';
        return $assign;
	}
}