<?php
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_card extends action_superclass
{
    /**
     * 生成卡号
     */
    public function create_card_list()
    {
        $url = "?ctl=card&act=create_card_list";
        $where = " WHERE 1 ";
        $cardno = request('cardno', 'str');
        $remark = request('remark', 'str');
        $channel = request('channel', 'str');
        if (!empty($cardno)) {
            $where .= " and card_no = '$cardno' ";
            $url .= "&cardno=$cardno";
        }
        $giftid = $_GET['gift_id'];
        if ($giftid != 0) {
            $where .= " and gift_id = '$giftid' ";
            $url .= "&gift_id=$giftid";
        }
        if ($channel) {
            $sub = array();
            foreach ($channel as $v) {
                $sub[] = " find_in_set('$v',`channel`) ";
            }
            $where .= ' and (' . implode('or', $sub) . ')';
        }
        if ($remark) {
            $where .= " and remark = '$remark' ";
            $url .= "&remark=$remark";
        }


        $sql = "select channel_name,channel_str from channel_list ";
        $assign['channel_list'] = $this->admindb->fetchRow($sql);

        $sql = "select platform_name,platform_str from adminplatformlist";
        $assign['plat_list'] = $this->admindb->fetchRow($sql);


        $total_record = Config::centerdb()->fetchOne("SELECT COUNT(*) as total from cdkey_list $where");
        $total_record = $total_record['total'];

        $per_page = 50;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by card_time desc ";

        $sql = "SELECT
              if(`type`=1,'通用礼包','普通礼包') as `type`,
              card_no,
              if(`interval`,CONCAT(`interval`,'小时'),'无限制') as `interval`,
              if(card_expire,FROM_UNIXTIME(card_expire),'无') as card_expire,
              if(can_use_time,FROM_UNIXTIME(can_use_time),'无') as can_use_time,
              FROM_UNIXTIME(card_time) as card_time,
              gift_id,
              repeat_flag,
              lv_limit,
              channel,
              platf,
              if(the_same_role_id=0,'不可以','可以') as the_same_role_id,
              remark,
              cgift_type
            FROM
              cdkey_list $where $orderby $limit";

        $list = Config::centerdb()->fetchRow($sql);
        $assign['list'] = $list;
        $assign['cardtype1'] = $this->cardtype(" and  type = 13 ");
   
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_GET) . "&page=",
        );
        $this->display(CENTER_TEMP . 'create_card_list.shtml', $assign);
    }

    public function cardtype($rights = '')
    {
        $where = " where 1 " . $rights;

        $sql = "select id,item from base_config where type=3";
        $glist = $this->admindb->fetchRow($sql);
        $cardtype = array();
        foreach ($glist as $val) {
            $cardtype[$val['id']] = $val['item'];
        }
        return $cardtype;
    }

    ///生成卡号
    public function create_card_list_action()
    {
        Auth::check_action(Auth::$CARD_MAN);
        set_time_limit(0);

        $type = request('type', 'int');
        $gift_id = request('giftid', 'str');
        $repeat_flag = request('repeat_flag', 'str');
        $num = request('num', 'str');
        $interval = request('interval', 'str');
        $can_use_time = $_POST['can_use_time'] ? strtotime(request('can_use_time', 'str')) : 0;
        $cardexpire = request('card_expire', 'str');
        $key = request('key', 'str');
        $lv_limit = request('lv_limit', 'str');
        $channel = is_array($_POST['channel']) ? implode(',', array_unique(array_filter(request('channel', 'arr')))) : request('channel', 'str');
        $platf = request('platf', 'str');
        $the_same_role_id = request('the_same_role_id', 'str');

        $remark = request('remark', 'str');
        $card_time = time();

        if (empty($gift_id)) alert('礼包类型为空');
        if (!in_array($type, array(0, 1))) alert('种类错误');
        if ($type == 1) $num = 1;
        if (empty($num)) alert('生成数量不能为空');
        if ($num > 20000) alert('生成数量不要超过20000');


        if (!empty($cardexpire)) {
            $card_expire = strtotime($cardexpire);
        } else {
            $card_expire = 0;
        }


        $base_sql = 'insert into cdkey_list (`card_no`,`type`,`interval`,`card_expire`,`can_use_time`,`card_time`, `gift_id`,`repeat_flag`,`lv_limit`,`channel`,`platf`,`the_same_role_id`,`remark`,`cgift_type`) values ';

        $gen_start_time = intval(microtime(true) * 10);
        $insert_unit = 100;
        $insert_num = 0;
        $values = array();
        for ($i = 1; $i <= $num; $i++) {

            $card_no = strtoupper($key . $this->__num_convert(($gen_start_time + $i), 36) . $this->__num_convert(rand(1679616, 1679616 * 36 - 1), 36));

            $values[] = "('$card_no','$type','$interval','$card_expire','$can_use_time','$card_time','$gift_id','$repeat_flag','$lv_limit','$channel','$platf','$the_same_role_id','$remark',2)";

            if ($i % $insert_unit == 0) {
                //达到插入数量，则插入一次sql
                $implode = implode(',', $values);
                $insert_sql = $base_sql . $implode;

                $result = Config::centerdb()->query($insert_sql, true);
                if ($result == true) {
                    $insert_num += $insert_unit;
                }
                //插入完成后，将数组清空，并休眠1秒
                $values = array();
            }

        }

        //最后剩下没插入的，也插入一次sql
        if (!empty($values)) {
            $insert_sql = $base_sql . implode(',', $values);
            //echo $insert_sql . "<br />";
            $result = Config::centerdb()->query($insert_sql, true);
            if ($result == true) {
                $insert_num += count($values);
            }
        }
        Admin::adminLog("生成礼包号:$gift_id;数量:$num");
        alert('成功生成' . $insert_num . '个卡号');

    }


    public function card_export()
    {
        Auth::check_action(Auth::$CARD_MAN);
        ini_set('memory_limit', '1024M');
        $num = request('cardno', 'str', 'P');
        $giftid = request('gift_id', 'int', 'P');
        $channel = request('channel', 'arr', "P");
        $remark = request('remark', 'str', 'P');
        $where = " WHERE 1 ";
        if ($num) {
            $where .= " and  card_no='$num'";
        }

        if ($giftid) {
            $where .= " and  gift_id='$giftid' and cgift_type=2";
        }
        if ($channel) {
            $sub = array();
            foreach ($channel as $v) {
                $sub[] = " find_in_set('$v',`channel`) ";
            }
            $where .= ' and (' . implode('or', $sub) . ') ';
        }

        if ($remark) {
            $where .= " and remark='$remark'";
        }
        Admin::adminLog("导出礼包号:$giftid");
        $sql = "select card_no from cdkey_list $where order by card_time desc ";
        $list = Config::centerdb()->fetchRow($sql);
        
        $data .= "\r\n";
        foreach ($list as $key => $val) {
              $data .= $val['card_no'];
              $data .= "\r\n";
        }
        
        header("content-type:application/octet-stream");
        header('Content-Disposition: attachment; filename=' ."礼包_".date("Y-m-d H:i:s"). '.txt');
        header("Content-Transfer-Encoding: binary");
        echo $data;
        exit;
    }

    //礼包领取日志
    public function card_log()
    {
        $url = "?ctl=card&act=card_log";

        $sid = request('sid','int');
        $assign = $this->get_server_rights();
        $s_list = $assign['srv_server'];
        $where = " WHERE 1 ";
        $where .= $this->get_where_s($sid,$s_list);

        if ($_GET['card_no']) {
            $where .= " and card_no ='" . $_GET['card_no'] . "'";
            $url .= "&card_no=" . $_GET['card_no'];
        }
        if ($_GET['role_id']) {
            $where .= " and role_id =" . $_GET['role_id'];
            $url .= "&role_id=" . $_GET['role_id'];
        }

        if ($_GET['accname']) {
            $where .= " and accname ='" . $_GET['accname'] . "'";
            $url .= "&accname=" . $_GET['accname'];
        }

        if ($_GET['nickname']) {
            $where .= " and nickname ='" . $_GET['nickname'] . "'";
            $url .= "&nickname=" . $_GET['nickname'];
        }
        if ($_GET['start_time'] && $_GET['end_time']) {
            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']);

            $where .= " and time between $start_time and $end_time ";
            $url .= "&start_time=" . $_GET['start_time'] . "&end_time=" . $_GET['end_time'];
        }

        $giftid = $_GET['gift_id'];
        if ($giftid != 0) {
            $arr = explode('_',$giftid);
            $giftid = $arr[0];
            $gift_type = $arr[1];
            $where .= " and gift_id = '$giftid' and cgift_type = $gift_type";
            $url .= "&gift_id=$giftid";
        }

        //查询游戏服
        $sql = "select id,gid,description from adminserverlist";
        $sid_list = $this->returnK($this->admindb->fetchRow($sql), 'id');

        $total_record = Config::centerdb()->fetchOne("SELECT COUNT(*) as total from cdkey_log $where");
        $total_record = $total_record['total'];

        $per_page = 50;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by card_time desc ";

        $sql = "SELECT
              card_no,
              if(`interval`,CONCAT(`interval`,'小时'),'永久') as `interval`,
              if(card_expire,FROM_UNIXTIME(card_expire),'无') as card_expire,
              if(can_use_time,FROM_UNIXTIME(can_use_time),'无') as can_use_time,
              FROM_UNIXTIME(card_time) as card_time,
              gift_id,
              if(repeat_flag=1,'可以','不可以') as repeat_flag,
              lv_limit,
              channel,
              platf,

              if(the_same_role_id=0,'不可以','可以') as the_same_role_id,
              sid,
              role_id,
              accname,
              nickname,
              from_unixtime(time) as time,
              cgift_type
            FROM
              cdkey_log $where $orderby $limit";

        $list = Config::centerdb()->fetchRow($sql);


        $assign['sid_list'] = $sid_list;
        $assign['list'] = $list;
        $assign['cardtype'] = $this->cardtype();  //type =1 AND item_type=80是为了适配最初生成的卡号类型有问题
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_GET) . "&page=",
        );
        $this->display(CENTER_TEMP . 'card_log.shtml', $assign);

    }
}