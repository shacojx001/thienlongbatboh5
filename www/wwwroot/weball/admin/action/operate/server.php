<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/25
 * Time: 上午9:31
 */
require_once dirname(__FILE__) . '/base.php';

class server extends base
{
    public function handle()
    {

    }

    /**
     * 合服数据分析
     */
    public function hefu_analysis(){
        global $server_list_info;
        $assign = $this->get_server_rights();
        $sids = request('sids');
        if(!is_array($sids)){
            $sids = explode(',',$sids);
        }

        $list = array();
        $charge = $this->get_3day_charge($sids);
        $online = $this->get_3day_online($sids);

        foreach ($server_list_info as $v) {
            if(in_array($v['id'],$sids)){
                $power = $this->get_role_power($v['id']);
                $list[$v['id']]['srv_num'] = $v['srv_num'];
                $list[$v['id']]['charge'] = $charge[$v['id']] ? implode("&nbsp;&nbsp;&nbsp;",$charge[$v['id']]):'';
                $list[$v['id']]['online'] = $online[$v['id']] ? implode("&nbsp;&nbsp;&nbsp;",$online[$v['id']]):'';
                $list[$v['id']]['power'] = $power ? implode("&nbsp;&nbsp;&nbsp;",$power) :'';
            }
        }
        $assign['list'] = $list;
        $this->display('operate/hefu_analysis.shtml', $assign);
        //三天充值

    }

    /**
     * 获取游戏服前几名战力
     * @param $sid
     * @return array
     */
    private function get_role_power($sid){
        $list = array();
        $sql = "select fight `combat_power` from role_basic order by fight desc limit 10";
        $rst = Config::gamedb($sid)->fetchRow($sql);

        if($rst){
            foreach ($rst as $v) {
                $list[] = $v['combat_power'];
            }
        }

        return $list;
    }

    /**
     * 获取三天充值
     */
    private function get_3day_charge($sids){
        $list = array();
        if($sids){
            $et = strtotime(date('Y-m-d'));
            $st = $et - 86400*3;
            $sids = implode(',',$sids);
          
            $sql = "select sid,from_unixtime(`ctime`,'%m/%d') as d,sum(money) as m from center_charge where ctime>=$st and ctime<$et and sid in ($sids) group by sid,d";
            $rst = Config::admindb()->fetchRow($sql); 
            if($rst){
                foreach ($rst as $v) {
                    $list[$v['sid']][] = $v['d'].':<span style="color:red;">'.intval($v['m']/100).'</span>';
                }
            }
        }
        return $list;
    }

    /**
     * 获取3天最高在线
     */
    private function get_3day_online($sids){
        $list = array();
        if($sids){
            $et = strtotime(date('Y-m-d'));
            $st = $et - 86400*3;
            $sids = implode(',',$sids);
            $sql = "select from_unixtime(`time`,'%m/%d') as d,sid,`online` from center_total_online where `time`>=$st and `time`<$et and sid in ($sids) group by sid,d";
            $rst = Config::admindb()->fetchRow($sql);
           
            if($rst){
                foreach ($rst as $v) {
                    $list[$v['sid']][] = $v['d'].':<span style="color:red;">'.$v['online'].'</span>';
                }
            }
        }

        return $list;
    }


    /**
     * 开服操作
     */
    public function open()
    {
        $assign = array();
        $assign['platform'] = $this->get_allgroup_list();
        $gid = request('gid','int');
        $assign['list'] = $this->get_server_reg($gid);
        $assign['power'] = $this->get_server_power($gid);
        $assign['online'] = $this->get_server_online($gid);
        $num = 0;
        $slist =  $this->get_not_open_servers($gid);
        if($slist){
            $num = count($slist);
            $assign['slist'] = $slist;
        }
        $assign['num'] = $num;
        $assign['max_reg'] = $this->get_max_reg_num($gid);

        $this->display('operate/open_server.shtml', $assign);
    }

    public function open_action(){
        Auth::check_action(Auth::$SERVER_CONFIG);
        $gid = request('gid','int');
        if(!$gid) exit('请选择专服!');

        $time = time();
        $sql = "select * from adminserverlist where otime > {$time} and gid = {$gid} order by otime asc limit 1";
        $info = Config::admindb()->fetchOne($sql);
        if(!$info){
            exit('没有游戏服可开启!');
        }
        //更新server
        Config::admindb()->update('adminserverlist',array('id'=>$info['id']),array('otime'=>$time));
        //更新base_game表
        $date = date('Y-m-d-H-i-s',$time);
        $sql = "REPLACE INTO base_game (cf_name, cf_value) VALUES ('version', '{$date}')";
        Config::gamedb($info['id'])->query($sql);
        //重启游戏服
        $save = array(
            'gid'=>$gid,
            'sid'=>$info['id'],
            'url'=>trim(substr($info['url'],6),'/'),
            'ip'=>$info['game_ip'],
            'addtime'=>time(),
        );
        //外网打开注释
        $restart_id = $this->restart_server_add($save);
        if($restart_id){
            $this->restart_server_action($restart_id);
        }

        //生成配置
        Config::config_wx();
        Admin::adminLog("快速开启游戏服:$gid|{$info['sid']}");

        exit('正在开启中...<br>游戏服重启中...<br>');
    }

    /**
     * 获取最大注册数
     * @param $gid
     * @return int
     */
    private function get_max_reg_num($gid){
        $sql = "select max_reg_num from adminplatformlist where gid='{$gid}'";
        $rst = Config::admindb()->fetchOne($sql);
        return $rst['max_reg_num']?:0;
    }

    /**
     * 获取未开服的游戏服
     * @param $gid
     * @return array
     */
    private function get_not_open_servers($gid){
        $time = time();
        $sql = "select id,description from adminserverlist where gid = '{$gid}' and otime > {$time} and `default` = 1";

        return Config::admindb()->fetchRow($sql);
    }

    /**
     * 获取每个服注册数
     * @param $gid
     * @return array
     */
    private function get_server_reg($gid){
        $list = array();
        if(!$gid){
            return $list;
        }
        $sql = "select a.*,b.description from center_server_reg a LEFT join adminserverlist b on a.sid = b.id where a.gid = {$gid} order by a.sid desc";
        return Config::admindb()->fetchRow($sql,'sid');
    }

    /**
     * 获取每个服注册数
     * @param $gid
     * @return array
     */
    private function get_server_power($gid){
        $list = array();
        if(!$gid){
            return $list;
        }
        $sql = "select * from center_server_power where gid = {$gid} order by sid desc";
        $rst = Config::admindb()->fetchRow($sql);
        if($rst){
            foreach ($rst as $v) {
                $list[$v['sid']] = $v['value'];
            }
        }
        return $list;
    }

    /**
     * 获取每个游戏服在线数
     * @param $gid
     * @return array
     */
    private function get_server_online($gid){
        $list = array();
        if(!$gid){
            return $list;
        }
        $t = strtotime(date('Y-m-d H:i')) - 2*60;
        $sql = "select sid,sum(`online`) as s from center_online_channel where gid= {$gid} and `time` = {$t} group by sid;";
        $rst = Config::admindb()->fetchRow($sql);
        if($rst){
            foreach ($rst as $v) {
                $list[$v['sid']] = $v['s'];
            }
        }
        return $list;
    }
}