<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/2
 * Time: 上午10:15
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';
abstract class base extends action_superclass{

    public $gamedb;
    public $admindb;
    public $centerdb;

    function __construct()
    {
        parent::__construct();
        $sid = $_SESSION['selected_sid'];
        $this->gamedb = Config::gamedb($sid);
        $this->admindb = Config::admindb();
        $this->centerdb = Config::centerdb();
    }
    /**
     * 抽象方法由子类实现
     * @return mixed
     */
    abstract function handle();

    /**
     * 获取活动base 信息
     */
    public function get_act($type = 6){
        $activity = $index = array();
        $sql = "select * from base_config where type = $type";
        $re = $this->admindb->fetchRow($sql);
   
        if($re){
            foreach ($re as $v) {
                $t = json_decode($v['attr'],true);
                if($t){
                    foreach ($t as &$in) {
                        $in['desc'] = urldecode($in['desc']);
                    }
                }
                
                //$t['theme_name'] = urldecode($t['theme_name']);
                //$t['act_name'] = urldecode($t['act_name']);
                $index[$v['id']] = $t;
                $v['attr'] = $t;
                $activity[$v['id']] = array('id'=>$v['id'],'name'=>$v['item']);
            }
        }
// print_r($index);
// print_r($activity);
// exit;
        return array('activity'=>$activity,'index'=>$index);
    }

    /**
     * 获取日常活动
     */
    public function get_daily_act($is_cross = 0){
        $sql = "select id,item as `name` from base_config where `type` = 5 and item_type={$is_cross}";
        return $this->admindb->fetchRow($sql,'id');
    }

    /**
     * 开启单服运营活动
     * 写入游戏服，通知erlang
     */
    public function open_activity($sid, $save){
        $callback = Erlang::erl_start_activity($sid, $save);    
        return $callback;
    }

    /**
     * 关闭单服运营活动
     */
    public function off_activity($sid, $id){
        $callback = Erlang::erl_stop_activity($sid, $id);    
        return $callback;
    }



}