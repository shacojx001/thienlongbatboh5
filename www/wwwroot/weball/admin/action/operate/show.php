<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/25
 * Time: 上午9:31
 */
require_once dirname(__FILE__) . '/base.php';

class show extends base
{
    public function handle()
    {

    }

    public function activity_show()
    {
        
        $y = $_REQUEST['time'] ? date("Y", strtotime($_REQUEST['time'])) :'';
        $m = $_REQUEST['time'] ? date("m", strtotime($_REQUEST['time'])) :'';
        list($start, $end) = $this->mFristAndLast($y, $m);
        $list = array();
        $toDay = strtotime(date('Y-m-d', time()));
        $toMon = $_REQUEST['time'] ? $_REQUEST['time'] : date('Y-m', time());   

        for( $i = $start; $i < $end; $i += 86400 ){
            
            $list[] = array(
                        'time' => $i,
                        'date' => date('Y-m-d', $i),
                        'week' => $this->getTimeWeek($i),
                        'weekKey' => $this->getTimeWeek($i, false),
                        'data' => array(),

                    );
        }

        $where = " where 1 ";
        if($_REQUEST['act_id']){
            $where .= " and act_id in({$_REQUEST['act_id']}) ";
        }

        $sql = "select act_id,begin_time begin,end_time end,valid from operation_activity_schedule $where";
       
        $act_list = $this->db->fetchRow($sql);
        $act_name_list = $this->get_config_list(5);

        foreach ($list as $key => $val) {
            $begin = $val['time'];
            $end = $val['time']+86400;
            foreach ($act_list as $kk => $vv) {
                $list[$key]['data'][] = $this->create_li($begin, $end, $vv, $act_name_list);
            }
            $list[$key]['data'] = implode("", $list[$key]['data']);
        }
        $num = $list[0]['weekKey'] == 7 ? 0 : $list[0]['weekKey'];
        if($num>0){
            $addArray = array_fill(0,$num,array());
            $list = array_merge($addArray, $list);
        }         
        
        $list = array_chunk($list, 7);

        foreach ($list as $key => $val) {
            foreach ($val as $kk => $vv) {
                if($toDay == $vv['time']){
                    $eq = $kk+1;
                    $style = "tr[data-index='$key'] td:nth-child($eq)";
                }
            }
        }

        $assign['list'] = $list;
        $assign['eq'] = $eq;
        $assign['style'] = $style;
        $assign['toMon'] = $toMon;
        $assign['act_name_list'] = $act_name_list;
    
        $this->display('operate/activity_show.shtml', $assign);
    }

    //
    public function create_li($begin, $end, $vv, $act_name_list)
    {
        if( $vv['begin']>=$begin && $vv['begin']<$end){ //开始当天
            $str = "<li style='color:#1E9FFF;'>{$vv['act_id']} {$act_name_list[$vv['act_id']]}</li>";
        }elseif($vv['end']>=$begin && $vv['end']<$end){ //结束当天
            $str = "<li style='color:#FF5722;'>{$vv['act_id']} {$act_name_list[$vv['act_id']]}</li>";
        }elseif($vv['begin']<=$begin && $vv['end']>$begin){ //活动期间
            $str = "<li style='color:#5fc934;'>{$vv['act_id']} {$act_name_list[$vv['act_id']]}</li>";
        }
        return $str;
    }

    //当天星期几
    public function getTimeWeek($time = '', $key = true)
    {
        $time = $time ? $time : time();
        $weekday = array('星期日','星期一','星期二','星期三','星期四','星期五','星期六'); 
        $weekdayKey = array(7,1,2,3,4,5,6); 
        return $key ? $weekday[date('w', $time)] : $weekdayKey[date('w', $time)] ;
    }

    public function mFristAndLast($y = "", $m = ""){
        if ($y == "") $y = date("Y");
        if ($m == "") $m = date("m");
        $m = sprintf("%02d", intval($m));
        $y = str_pad(intval($y), 4, "0", STR_PAD_RIGHT);
     
        $m>12 || $m<1 ? $m=1 : $m=$m;
        $firstday = strtotime($y . $m . "01000000");
        $firstdaystr = date("Y-m-01", $firstday);
        $lastday = strtotime(date('Y-m-d 23:59:59', strtotime("$firstdaystr +1 month -1 day")));
     
        return array($firstday, $lastday);
    }
    
}