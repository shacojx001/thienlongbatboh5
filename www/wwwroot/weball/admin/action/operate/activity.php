<?php
/**
 * 活动
 */
require_once dirname(__FILE__) . '/base.php';

class activity extends base
{
    //开启：lib_gm:start_op_act_local($ThemeId, $ActId, $AwardIndex, $StartTime, $EndTime)
    //结束：lib_gm:stop_op_act_local($ThemeId, $ActId)
    public function handle()
    {

    }

    /**
     * 日常活动停开 单服
     */
    public function daily_activity()
    {
        $assign = $this->get_server_rights();

        $activity = $this->get_daily_act();
        $assign['activity'] = $activity;

        $where = 'where 1';
        $act_id = request('act_id', 'int');
        if ($act_id) {
            $where .= ' and act_id=' . $act_id;
        }
        $gid = request('gid', 'int');
        if ($gid) {
            $where .= ' and gid=' . $gid;
        }
        $sid = request('sid', 'int');
        if ($sid) {
            $where .= ' and sid = ' . $sid;
        }

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from operate_daily_activity $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from operate_daily_activity $where order by id desc $limit";
        $list = $this->admindb->fetchRow($sql);

        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );

        $this->display('operate/daily_activity.shtml', $assign);
    }

    /**
     * 日常活动停开 跨服
     */
    public function daily_activity_kf()
    {
        $assign = $this->get_server_rights();

        $activity = $this->get_daily_act(1);
        $assign['activity'] = $activity;

        $where = 'where 1';
        $act_id = request('act_id', 'int');
        if ($act_id) {
            $where .= ' and act_id=' . $act_id;
        }
        $gid = request('gid', 'int');
        if ($gid) {
            $where .= ' and gid=' . $gid;
        }
        $sid = request('sid', 'int');
        if ($sid) {
            $where .= ' and sid = ' . $sid;
        }

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from operate_daily_activity $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from operate_daily_activity_kf $where order by id desc $limit";
        $list = $this->admindb->fetchRow($sql);

        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );

        $this->display('operate/daily_activity_kf.shtml', $assign);
    }

    /**
     * 日常活动停启 提交操作
     */
    public function daily_activity_action()
    {
        Auth::check_action(Auth::$ACTIVITY);
        $gid = request('gid', 'int');
        $sids = request('sids');
        $activity = request('activity');
        $switch = request('switch', 'int');

        if (empty($gid) || empty($sids) || empty($activity) || !is_array($activity) || !is_array($sids)) {
            alert(Ext_Template::lang('请选择好参数再提交'));

        }
        $t = time();
        $act_list = $this->get_daily_act();
        foreach ($sids as $sid) {
            foreach ($activity as $act_id) {
                $res = "ok";
                if (1 == $switch) {
                    $res = Erlang::erl_start_daily_activity($sid,array('act_id'=>$act_id));
                } elseif (2 == $switch) {
                    $res = Erlang::erl_stop_daily_activity($sid,array('act_id'=>$act_id));
                }

                $callback = is_array($res) ? json_encode($res) : $res;
                $act_name = $act_list[$act_id]['name'];
                $save = array('gid' => $gid, 'sid' => $sid, 'callback' => $callback, 'act_id' => $act_id, 'act_name' => $act_name, 'ct' => $t);
                $this->admindb->insert('operate_daily_activity', $save);
                Admin::adminLog('停止日常活动:' . implode('|', $save));
            }
        }
        alert('Finish all commit!');
    }

    /**
     * 日常跨服活动停启 提交操作
     */
    public function daily_activity_kf_action()
    {
        Auth::check_action(Auth::$ACTIVITY);
        $gid = request('gid', 'int');
        $groups = request('group_ids');
        $activity = request('activity');
        $switch = request('switch', 'int');

        if (empty($gid) || empty($groups) || empty($activity) || !is_array($activity) || !is_array($groups)) {
            alert(Ext_Template::lang('请选择好参数再提交'));
        }
        $t = time();
        $act_list = $this->get_daily_act(1);
        foreach ($groups as $group_id) {
            foreach ($activity as $act_id) {
                $res = "ok";
                if (1 == $switch) {
                    $res = Erlang::erl_start_daily_activity_kf($gid,array('act_id'=>$act_id,'group_id'=>$group_id));
                } elseif (2 == $switch) {
                    $res = Erlang::erl_stop_daily_activity_kf($gid,array('act_id'=>$act_id,'group_id'=>$group_id));
                }

                $callback = is_array($res) ? json_encode($res) : $res;
                $act_name = $act_list[$act_id]['name'];
                $save = array('gid' => $gid, 'group_id' => $group_id, 'callback' => $callback, 'act_id' => $act_id, 'act_name' => $act_name, 'ct' => $t);
                $this->admindb->insert('operate_daily_activity_kf', $save);
                Admin::adminLog('停止跨服日常活动:' . implode('|', $save));
            }
        }
        alert('Finish all commit!');
    }

    /*
     * 跨服运营活动
     */
    public function operate_activity_kf()
    {
        $assign = $this->get_server_rights();
        $where = ' where 1';

        $state = array(
            1 => Ext_Template::lang('未开启'),
            2 => Ext_Template::lang('进行中'),
            3 => Ext_Template::lang('已结束'),
            4 => Ext_Template::lang('活动停止'),
            5 => Ext_Template::lang('开启失败'),
        );
        $assign['state'] = $state;

        $gid = request('gid', 'int');
        $group_id = request('group_id', 'int');

        $theme_id = $act_id = 0;
        $s_act_key = request('s_act_key');
        if ($s_act_key) {
            list($theme_id, $act_id) = explode('_', $s_act_key);
        }

        if ($gid) {
            $where .= " and gid = $gid";
        }
        if ($group_id) {
            $where .= " and group_id = {$group_id}";
        }
        if ($act_id) {
            $where .= " and theme_id = $theme_id and act_id=$act_id";
        }
        if ($group_id) {
            $where .= " and group_id = $group_id";
        }

        $t = time();
        $state = request('state', 'int');
        if ($state == 1) {
            $where .= " and st>{$t}";
        } elseif ($state == 2) {
            $where .= " and st<{$t} and et>{$t}";
        } elseif ($state == 3) {
            $where .= " and et<{$t}";
        } elseif ($state == 4) {
            $where .= " and is_del = 1";
        } elseif ($state == 5) {
            $where .= " and is_del = 2";
        }

        $act_info = $this->get_act(6);
        $assign['activity'] = $act_info['activity'];
        $assign['index'] = $act_info['index'];

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from activity_operation_kf $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from activity_operation_kf $where order by is_del asc,ct desc $limit";
        $list = $this->admindb->fetchRow($sql);
        if ($list) {
            $now = time();
            foreach ($list as $k => $v) {
                $state = '<span style="color:blue;">'.Ext_Template::lang('未开启').'</span>';
                if ($v['st'] < $now && $v['et'] > $now) {
                    $state = '<span style="color:red;">'.Ext_Template::lang('进行中').'</span>';
                } elseif ($v['et'] < $now) {
                    $state = '<span style="color:dimgrey">'.Ext_Template::lang('已结束').'</span>';
                }
                if ($v['is_del'] == 1) {
                    $state = '<span style="color:grey">'.Ext_Template::lang('活动停止').'</span>';
                }
                if ($v['is_del'] == 2) {
                    $state = '<span style="color:grey">'.Ext_Template::lang('开启失败').'</span>';
                }
                $list[$k]['state'] = $state;
            }
        }
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );


        $this->display('operate/operate_activity_kf.shtml', $assign);
    }

    public function operate_activity_kf_action()
    {
        Auth::check_action(Auth::$ACTIVITY);

        $gid = request('gid', 'int', 'P');
        $group_ids = request('group_ids');
        $activity_key = request('activity');
        if (!$gid || !$group_ids || !$activity_key) {
            alert(Ext_Template::lang('信息不完整!'));
        }

        $data = array();
        foreach ($activity_key as $key) {
            $index = request($key . '_index', 'int');
            $st = request($key . '_st', 'str');
            $et = request($key . '_et', 'str');
            if (!$index || !$st || !$et) {
                alert(Ext_Template::lang('活动配置信息不完整!'));
            }

            $st = strtotime($st);
            $et = strtotime($et);
            if ( $et < $st) {
                alert(Ext_Template::lang('结束时间必须大于开始时间!'));
            }
            list($theme_id, $act_id) = explode('_', $key);
            $data[$key] = array(
                'theme_id' => $theme_id,
                'act_id' => $act_id,
                'index' => $index,
                'st' => $st,
                'et' => $et,
            );
        }

        $kfCenter = Kf::kf_node_by($gid);

        $act_info = $this->get_act(11);
        $activity = $act_info['activity'];
        $index = $act_info['index'];
        foreach ($group_ids as $group_id) {
            foreach ($data as $v) {
                $send = array(
                    'theme_id' => $v['theme_id'],
                    'act_id' => $v['act_id'],
                    'index' => $v['index'],
                    'st' => $v['st'],
                    'et' => $v['et'],
                    'group_id' => $group_id,
                );
                $merge_key = $v['theme_id'] . '_' . $v['act_id'];
                $save = array(
                    'gid' => $gid,
                    'group_id' => $group_id,
                    'act_name' => $activity[$merge_key]['name'],
                    'index_desc' => $index[$v['act_id']][$v['index']]['desc'] . '(' . $index[$v['act_id']][$v['index']]['detail'] . ')',
                    'callback' => '',
                    'ct' => time(),
                );
                $save = array_merge($save, $send);
                $re = $this->admindb->insert('activity_operation_kf', $save);
                if ($re) {
                    //此处可以放队列做异步
                    $insert_id = $this->admindb->getLastInsertId();
                    $callback = Erlang::erl_start_activity_operation_kf($kfCenter, $send);
                    $is_del = 0;
                    if ($callback[0] != 'ok') {
                        $is_del = 2;
                    }
                    $_callback = json_encode($callback);
                    $sql = "update activity_operation_kf set `callback` = '$_callback',`is_del`='$is_del' where id = '$insert_id'";
                    $this->admindb->query($sql);
                    $save['id'] = $insert_id;
                    $save['callback'] = is_array($callback) ? json_encode($callback) : $callback;
                    Admin::adminLog('配置跨服运营活动:' . implode('|', $save) . "###send:" . implode('|', $send));
                }
            }
        }

        alert(Ext_Template::lang('已提交!'));
    }

    /**
     * 删除跨服运营活动
     */
    public function operate_activity_kf_del()
    {
        $id = request('id', 'int');
        if (!$id) {
            alert(Ext_Template::lang('缺少参数!'));
        }

        $sql = "select * from activity_operation_kf where id = {$id}";
        $re = $this->admindb->fetchOne($sql);
        if (!$re) {
            alert(Ext_Template::lang('找不到活动开启记录!'));
        }
        if ($re['st'] < (time() + 360)) {
            //alert('当前时间不能停止活动');
        }
        if ($re['callback']) {
            $info = json_decode($re['callback'], true);
            //lib_gm:stop_op_act_local($ThemeId, $ActId)
            $kfCenter = Kf::kf_node_by($re['gid']);
            $del_call = Erlang::erl_stop_activity_operation_kf($kfCenter, array('info_id' => $info[1]));
            $_del_call = is_array($del_call) ? json_encode($del_call) : $del_call;
            $sql = "update activity_operation_kf set is_del=1,del_callback = '{$_del_call}' where id = $id";
            $this->admindb->query($sql);
            Admin::adminLog('停止跨服运营活动:' . $id . '|' . $_del_call . "|分组{$re['group_id']}");

            if ($del_call[0] == 'ok') {
                alert(Ext_Template::lang('停止跨服运营活动成功!'));
            } else {
                alert(Ext_Template::lang('停止跨服运营活动失败!'));
            }
        }
        alert(Ext_Template::lang('停止跨服运营活动失败!'));

    }

    /**
     * 单服运营活动
     */
    public function operate_activity()
    {
        $assign = $this->get_server_rights();
        $where = ' where 1';

        $state = array(
            1 => Ext_Template::lang('未开启!'),
            2 => Ext_Template::lang('进行中!'),
            3 => Ext_Template::lang('已结束!'),
            4 => Ext_Template::lang('活动停止!'),
            5 => Ext_Template::lang('开启失败!'),
        );
        $assign['state'] = $state;

        $gid = request('gid', 'int');
        $sid = request('sid', 'int');

        $theme_id = $act_id = 0;
        $s_act_key = request('s_act_key');
        if ($s_act_key) {
            list($theme_id, $act_id) = explode('_', $s_act_key);
        }

        if ($gid) {
            $where .= " and gid = $gid";
        }
        if ($sid) {
            $where .= " and sid = $sid";
        }
        if ($act_id) {
            $where .= " and theme_id = $theme_id and act_id=$act_id";
        }

        $t = time();
        $state = request('state', 'int');
        if ($state == 1) {
            $where .= " and st>{$t}";
        } elseif ($state == 2) {
            $where .= " and st<{$t} and et>{$t}";
        } elseif ($state == 3) {
            $where .= " and et<{$t}";
        } elseif ($state == 4) {
            $where .= " and is_del = 1";
        } elseif ($state == 5) {
            $where .= " and is_del = 2";
        }

        $act_info = $this->get_act(5);
        $assign['activity'] = $act_info['activity'];
        $assign['index'] = $act_info['index'];

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from activity $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from activity $where order by is_del asc,ct desc $limit";
        $list = $this->admindb->fetchRow($sql);
        if ($list) {
            $now = time();
            foreach ($list as $k => $v) {
                $state = '<span style="color:blue;">'.Ext_Template::lang('未开启').'</span>';
                if ($v['st'] < $now && $v['et'] > $now) {
                    $state = '<span style="color:red;">'.Ext_Template::lang('进行中').'</span>';
                } elseif ($v['et'] < $now) {
                    $state = '<span style="color:dimgrey">'.Ext_Template::lang('已结束').'</span>';
                }
                if ($v['is_del'] == 1) {
                    $state = '<span style="color:grey">'.Ext_Template::lang('活动停止').'</span>';
                }
                if ($v['is_del'] == 2) {
                    $state = '<span style="color:grey">'.Ext_Template::lang('开启失败').'</span>';
                }
                $list[$k]['state'] = $state;
            }
        }
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );

        $this->display('operate/operate_activity.shtml', $assign);
    }

    /**
     * 单服运营活动提交
     */
    public function operate_activity_action()
    {
        Auth::check_action(Auth::$ACTIVITY);

        $gid = request('gid', 'int', 'P');
        $sids = request('sids');
        $activity_key = request('activity');
        if (!$gid || !$sids || !$activity_key) {
            alert(Ext_Template::lang('信息不完整!'));
        }

        $data = array();
        foreach ($activity_key as $key) {
            $index = request($key . '_index', 'int');
            $st = request($key . '_st', 'str');
            $et = request($key . '_et', 'str');
            if (!$index || !$st || !$et) {
                alert(Ext_Template::lang('活动配置信息不完整!'));
            }

            $st = strtotime($st);
            $et = strtotime($et);
            if ($et < $st) {
                alert(Ext_Template::lang('结束时间必须大于开始时间!'));
            }
            $data[$key] = array(
                'act_id' => $key,
                'index' => $index,
                'st' => $st,
                'et' => $et,
            );
        }

        $act_info = $this->get_act(5);
        $activity = $act_info['activity'];
        $index = $act_info['index'];
        foreach ($sids as $sid) {
            foreach ($data as $v) {
                $send = array(
                    'act_id' => $v['act_id'],
                    'index' => $v['index'],
                    'st' => $v['st'],
                    'et' => $v['et'],
                );
                $act_id = $v['act_id'];
                $save = array(
                    'gid' => $gid,
                    'sid' => $sid,
                    'act_name' => $activity[$act_id]['name'],
                    'index_desc' => $index[$v['act_id']][$v['index']]['desc'] . '(' . $index[$v['act_id']][$v['index']]['detail'] . ')',
                    'callback' => '',
                    'ct' => time(),
                );
                $save = array_merge($save, $send);

                $re = $this->admindb->insert('activity', $save);
                if ($re) {
                    //此处可以放队列做异步
                    $insert_id = $this->admindb->getLastInsertId();
                    $send['id'] = $insert_id;
                    $callback = $this->open_activity($sid, $send);  
                    
                    $is_del = 0;
                    if ($callback != 'ok') {
                        $is_del = 2;
                    }
                    $_callback = json_encode($callback);
                    $sql = "update activity set `callback` = '$_callback',`is_del`='$is_del' where id = '$insert_id'";
                    $this->admindb->query($sql);
                    $save['id'] = $insert_id;
                    $save['callback'] = is_array($callback) ? json_encode($callback) : $callback;
                    Admin::adminLog('配置活动:' . $insert_id . '|' . implode('|', $save) . "###send:" . implode('|', $send));
                }
            }
        }

        alert(Ext_Template::lang('已提交!'));
    }

    /**
     * 单服运营活动删除
     */
    public function operate_activity_del()
    {
        $id = request('id', 'int');
        if (!$id) {
            alert(Ext_Template::lang('缺少参数'));
        }

        $sql = "select * from activity where id = {$id}";
        $re = $this->admindb->fetchOne($sql);
        if (!$re) {
            alert(Ext_Template::lang('找不到活动开启记录!'));
        }
        if ($re['st'] < (time() + 360)) {
            //alert('当前时间不能停止活动');
        }      
        if ($re['callback']) {
            $info = json_decode($re['callback'], true);
            //lib_gm:stop_op_act_local($ThemeId, $ActId)
            $del_call = $this->off_activity($re['sid'], $re['id']);
            $_del_call = is_array($del_call) ? json_encode($del_call) : $del_call;
            $sql = "update activity set is_del=1,del_callback = '{$_del_call}' where id = $id";
            $this->admindb->query($sql);
            Admin::adminLog('停止活动:' . $id . '|' . $_del_call);

            if ($del_call == 'ok') {
                alert(Ext_Template::lang('停止活动成功!'));
            } else {
                alert(Ext_Template::lang('停止活动失败!'));
            }
        }
        alert(Ext_Template::lang('停止活动失败!'));

    }

    /**
     * 获取服列表
     */
    public function ajax_allserver_list()
    {
        $gid = request('gid', 'int');
        $re = parent::ajax_allserver_list($gid, true);
        exit($re);
    }

    /**
     * 获取跨服分组
     */
    public function ajax_kf_group()
    {
        global $system_config;
        $gid = request('gid', 'int');
        $act_id = request('act_id','str');
        list($theme_id,$real_act_id) = explode('_',$act_id);
        $type = request('type','int');
        $kf_group = Kf::kf_group_by($gid,$type,$real_act_id,$system_config['pid_like']);
        if (!$kf_group) {
            exit(json_encode(array('flag' => 0, 'json' => Ext_Template::lang('此专服没有配置分组'))));
        }
        $group = array();
        foreach ($kf_group as $v) {
            $group[$v['group_id']][] = $v['server_id'];
        }
        $html = '';
        foreach ($group as $group_id => $v) {
            ksort($v);
            $sids = '['.implode(',',$v).']';
            $html .= "<option value='{$group_id}'>{$group_id}组:$sids</option>";
        }
        exit(json_encode(array('flag' => 1, 'json' => $html)));
    }


    /**
     * 跨服分组配置
     */
    public function kf_group()
    {
        global $system_config;
        $assign = $this->get_server_rights();

        $activity = Kf::get_act_opt();
        $assign['act_list'] = $activity;
        $act_ids = array_keys($activity);

        $gid = request('gid', 'int');
        $act_id = request('act_id','int');
        if ($gid) {
            $kfCenter = kf::kf_node_by($gid);
            if (!$kfCenter) {
                alert(Ext_Template::lang('该专服未配置跨服中心'));
            }
            $where = '1';
            if($act_id){
                $where .= " and act_id = {$act_id}";
            }else{
                $where .= " and act_id in (".implode(',',$act_ids).")";
            }

            $groupIds = array();
            $sql = "select group_id from base_kf_group where {$where} group by group_id";
            $re = Kf::get_super_kf_db($kfCenter)->fetchRow($sql);
            if ($re) {
                foreach ($re as $v) {
                    $groupIds[] = $v['group_id'];
                }
            }
            sort($groupIds);
            $assign['group_ids'] = $groupIds;

            $sid = request('sid', 'int');
            $group_id = request('group_id', 'int');
            $where = "where 1";
            if ($sid) {
                $where .= ' and server_id=' . $sid;
            }
            if ($group_id) {
                $where .= ' and group_id=' . $group_id;
            }
            if($act_id){
                $where .= " and act_id = {$act_id}";
            }else{
                $where .= " and act_id in (".implode(',',$act_ids).")";
            }

            $total_record = Kf::get_super_kf_db($kfCenter)->fetchOne("SELECT COUNT(1) as total from base_kf_group $where");
            $total_record = $total_record['total'];
            $per_page = 30;
            $total_page = ceil($total_record / $per_page);
            $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
            $cur_page = $cur_page < 1 ? 1 : $cur_page;
            $limit_start = ($cur_page - 1) * $per_page;
            $limit = " LIMIT {$limit_start}, $per_page ";

            $sql = "select * from base_kf_group $where order by group_id asc,server_id asc $limit";
            $re = Kf::get_super_kf_db($kfCenter)->fetchRow($sql);
            $list = $sids = array();
            foreach ($re as $v) {
                $sids[$v['group_id']][] = $v['server_id'];
            }
            foreach ($re as $v) {
                sort($sids[$v['group_id']]);
                $v['sids'] = '['.implode(',',$sids[$v['group_id']]).']';
                $v['act_name'] = '['.$v['act_id'].']'.$activity[$v['act_id']]['item'];
                $list[] = $v;
            }

            $assign['list'] = $list;
            $assign['pages'] = array(
                "curpage" => $cur_page,
                "totalpage" => $total_page,
                "totalnumber" => $total_record,
                "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
            );
        }

        $this->display('operate/kf_group.shtml', $assign);
    }

    /**
     * 跨服分组配置
     */
    public function kf_group_config(){
        $assign = $this->get_server_rights();
        $act_list = Kf::get_act_opt();
        $assign['act_list'] = $act_list;

        $gid = request('gid','int');
        $act_id = request('act_id','int');
        if($gid && $act_id){
            $sql = "select * from kf_group_config where gid = '{$gid}' and act_id='{$act_id}'";
            $info = Config::admindb()->fetchOne($sql);
            $assign['info'] = $info;
        }

        $this->display('operate/kf_group_config.shtml', $assign);
    }

    /**
     * 提交跨服分组配置
     */
    public function kf_group_config_action(){
        $data['gid']= request('gid','int');
        $data['act_id'] = request('act_id','int');
        $data['type'] = request('type','int');
        $data['group_num'] = request('group_num','int');
        if(!$data['gid']||!$data['act_id']||!$data['type']||! $data['group_num']){
            alert(Ext_Template::lang('请填写完整配置!'));
        }

        $sql = "select group_num from kf_group_config where gid = '{$data['gid']}' and act_id='{$data['act_id']}'";
        $re = Config::admindb()->fetchOne($sql);
        if($re['group_num'] && $re['group_num'] != $data['group_num'] && !request('run_group')){
            //检测 更改分组服数时必须要执行重新分组
            alert(Ext_Template::lang('更改分组服数时必须要执行重新分组!'));
        }

        $re = Config::admindb()->insertOrUpdate('kf_group_config',$data);
        if($re){
            if(request('run_group')){
                Kf::dispatch_group_opt($data['gid'],$data);
                Admin::adminLog('提交跨服中心分组配置[运营活动类]并进行分组操作:'.implode('|',$data));
                alert('完成分组操作!请查看分组列表!');
            }
            Admin::adminLog('提交跨服中心分组配置[运营活动类]:'.implode('|',$data));
            alert(Ext_Template::lang('提交成功!'));
        }else{
            alert(Ext_Template::lang('提交失败!'));
        }
    }

    /**
     * 删除kf
     */
    public function kf_group_del()
    {
        alert(Ext_Template::lang('暂时关闭此功能!'));
        Auth::check_action(Auth::$KF_GROUP_EDIT);
        $serverid = request('serverid', 'int');
        $groupid = request('groupid', 'int');
        $gid = request('gid', 'int');
        if (!$serverid || !$groupid || !$gid) {
            alert(Ext_Template::lang('参数错误!'));
        }
        $sql = "select kf_center from adminplatformlist where gid = {$gid} limit 1";
        $kfCenter = $this->admindb->fetchOne($sql);
        if (!$kfCenter || empty($kfCenter['kf_center'])) {
            alert('该专服未配置跨服中心');
        }
        $kfCenter = $kfCenter['kf_center'];
        $re = Config::kfdb($kfCenter)->query("delete from base_kf_group where server_id={$serverid} and group_id={$groupid}");
        if ($re) {
            Admin::adminLog("删除跨服分组:分组ID({$groupid},服ID({$serverid})");
            alert('删除成功!');
        }
        alert('删除失败!');
    }

    /**
     * 刷新分组
     */
    public function kf_group_flash()
    {
        //还有要接个GM
        //svr_kf_group 模块
        //distribute_group 函数
        //0 参数
        alert('暂时关闭此功能');
        Auth::check_action(Auth::$KF_GROUP_GM);
        $gid = request('gid', 'int');
        if (!$gid) {
            alert('请选择指定平台!');
        }
        $kfCenter = Kf::kf_node_by($gid);
        if (!$kfCenter) {
            alert('该专服未配置跨服中心');
        }
        $res = Erlang::erl_flash_kf($kfCenter);

        Admin::adminLog("刷新跨服分组GM:平台({$gid})");
        alert($res);
    }


}