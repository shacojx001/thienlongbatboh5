<?php
/**
 * 客服
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_service extends action_superclass
{
    public function ajax_group_byserver()
    {
        $gid = $_REQUEST['gid'];
        $json = $this->ajax_allserver_list($gid);
        echo $json;
        exit;
    }

    public function ajax_group_by_server(){
        $gid = request('gid','int');
        exit($this->server_list_for_header($gid));
    }


    /**
     * 开服计划
     */
    public function apply_plan_list()
    {
        $where = " where 1 ";
        $url = "?ctl=service&act=apply_plan_list";

        $assign['glist'] = $this->get_allgroup_list();
        $assign['slist'] = $this->get_allserver_list();

        $assign['super'] = $_SESSION['super'] == "YES" ? 1 : 0;
        $assign['srv_list_bygroup'] = $this->get_allserver_bygid();

        $gid = !isset($_GET['gid']) ? 0 : $_GET['gid'];
        $where .= $this->get_where_g($gid,$assign['glist']);
        if (!empty($gid)) {
            //$where .= " and gid = '$gid'";
            $url .= "&gid=$gid";
            $assign['gid'] = $gid;
        } else {
            $url .= "&gid=";
            $assign['gid'] = '';
        }

        $state = $_GET['state'];
        if ($state != "") {
            $where .= " AND state = '$state' ";
            $url .= "&state=$state";
        }

        $total_record = $this->admindb->fetchOne("SELECT COUNT(*) as total from adminserverlist_plan $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by state,gid,sid";

        $sql = "select * from adminserverlist_plan $where $orderby $limit";
        // echo $sql;
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );
        $this->display('apply_plan_list.shtml', $assign);
    }


    /**
     * 提开服申请
     */
    public function apply_plan_action()
    {
        global $system_config;
        Auth::check_action(Auth::$SERVER_APPLY);
        if (empty($_POST['channel'])) {
            alert(Ext_Template::lang("请选择渠道"));
        }

        $channel_list = implode(",", $_POST['channel']);
        $otime = strtotime($_POST['otime']);
        if ($otime < time()) {
            alert(Ext_Template::lang("开服时间已过"));
        }
        $title = trim($_POST['title']);
        $gid = intval($_POST['gid2']);
        $num = intval($_POST['num']);

        //$domain = addslashes(trim($_POST['domain']));
        $time = time();
        if (!empty($num) && !empty($otime) && $gid != '' && !empty($title)) {
            if(empty($system_config['game_url'])){
                alert(Ext_Template::lang("系统未配置game_url,请联系负责人处理!"));
            }
            $domain = sprintf($system_config['game_url'],$num);
            $sql = "select count(1) as t from adminserverlist plan where num = $num and state != 2";
            $res = $this->admindb->fetchOne($sql);
            if (!empty($res['t'])) {
                alert(Ext_Template::lang("申请失败,该服ID已存在审核列表"));
            }
            $sql = "select count(1) as t from adminserverlist where id = $num";
            $res = $this->admindb->fetchOne($sql);
            if (!empty($res['t'])) {
                alert(Ext_Template::lang("申请失败,该服ID已存在服列表"));
            }

            //检查服id范围是否合法
            $gids_f = $this->check_server_id($gid);
            if(!$gids_f){
                alert(Ext_Template::lang("申请失败,专服配置错误!"));
            }
            if($num<$gids_f['min'] || $num>$gids_f['max']){
                alert(Ext_Template::lang("游戏服ID范围不合法!"));
            }


            $sql = "INSERT INTO adminserverlist_plan (
                  gid,
                  otime,
                  title,
                  num,
                  domain,
                  time,
                  channel_list
                )
                VALUES
                  (
                    '$gid',
                    '$otime',
                    '$title',
                    '$num',
                    '$domain',
                    '$time',
                    '$channel_list'
                  )";
            $this->admindb->query($sql);
            Admin::adminLog("开服申请:$title($num);GID:$gid");
            alert(Ext_Template::lang("申请成功"),'action_gateway.php?ctl=service&act=apply_plan_list');
        } else {
            alert(Ext_Template::lang("提交异常"));
        }
    }

    /**
     * 开服计划审核列表
     */
    public function review_plan()
    {
        $where = " where 1 ";
        $url = "action_gateway.php?ctl=service&act=review_plan";
        $assign['srv_group'] = $assign['glist'] = $this->get_allgroup_list();
        $assign['slist'] = $this->get_allserver_list();
        $gid = $_GET['gid'];
        if (!empty($gid)) {
            $where .= " and gid = '$gid'";
            $url .= "&gid=$gid";
            $assign['gid'] = $gid;
        } else {
            $url .= "&gid=";
            $assign['gid'] = '';
        }

        $sid = !isset($_GET['sid']) ? 0 : $_GET['sid'];
        if (!empty($sid)) {
            $where .= " and sid = '$sid'";
            $url .= "&sid=$sid";
            $assign['sid'] = $sid;
        }

        $state = $_GET['state'];
        $state = $state == "" ? 0 : $state;
        $where .= " AND state = '$state' ";
        $url .= "&state=$state";

        $start_time = $_GET['start_time'];
        if (!empty($start_time)) {
            $starttime = strtotime($start_time);
            $where .= " AND otime >= '$starttime' ";
            $url .= "&start_time=$start_time";
        }

        $end_time = $_GET['end_time'];
        if (!empty($end_time)) {
            $endtime = strtotime($end_time);
            $where .= " AND otime <= '$endtime' ";
            $url .= "&end_time=$end_time";
        }

        $total_record = $this->admindb->fetchOne("SELECT COUNT(*) as total from adminserverlist_plan $where");
        if (!empty($total_record)) {
            $total_record = $total_record['total'];
        } else {
            $total_record = 0;
        }

        $per_page = 1500;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by id ASC";

        $sql = "select * from adminserverlist_plan $where $orderby $limit";
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;

        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );

        $this->display('review_plan.shtml', $assign);
    }

    /**
     * 审核开服操作
     */
    public function review_plan_action()
    {
        global $system_config;

        Auth::check_action(Auth::$SERVER_SH);
        $idarr = $_REQUEST['id'];
        $state = $_REQUEST['state'] == 1 ? 1 : 2;
        if (!empty($idarr)) {
            $ids = join(',', $idarr);
            if ($state == 1) {
                $sql = "select p.*,a.advance_time,a.kf_center,a.platform_str from adminserverlist_plan p LEFT JOIN adminplatformlist a on p.gid=a.gid where p.id in ($ids)";

                $info = $this->admindb->fetchRow($sql);

                if (!empty($info)) {
                    foreach ($info as $key => $val) {
                        if ($val['state'] != 0) continue;
                        // 参数检查
                        if ($val['title'] == "")
                            alert("请配置标题");
                        if ($val['domain'] == "")
                            alert("请配置【{$val['title']}】的域名");

                        ///查询是否有未审核的编码更前的服
                        $sql = "SELECT min(num) as res FROM adminserverlist_plan WHERE gid = {$val['gid']} AND state = 0 AND num < {$val['num']}";
                        $cs = $this->admindb->fetchOne($sql);
                        if ($cs['res']) {
                            alert("请把该游戏服{$cs['res']}审核");
                        }

                        if(empty($system_config['game_db'])||empty($system_config['db_user'])||empty($system_config['db_pwd'])){
                            alert('游戏服数据库名等信息未配置,请联系负责人处理!');
                        }

                        $sdb = sprintf($system_config['game_db'],$val['num']);
                        $sname = array_shift(explode('_',$sdb));

                        $s_num = "{$val['num']}";
                        $sql = "select count(*) as t from adminserverlist where id='{$val['num']}'";
                        $ainfo = $this->admindb->fetchOne($sql);
                        if ($ainfo['t'] > 0) {
                            alert('失败,该服已存在');
                        }

                        $visible_time = $val['otime'] - $val['advance_time'] * 3600;

                        $url = $val['domain'] . "js/game_ip_port.txt";
                        $myfile = Http::httpGet($url);
                        $myfile = json_decode($myfile,true);
                        if ($myfile) {
                            $game_ip = $myfile['game_ip'];
                            $game_port = $myfile['game_port'];
                            $db_port = $myfile['db_port'];
                            $db_host = $myfile['db_host'];
                        } else {
                            $game_ip = 0;
                            $game_port = 0;
                            $db_port = 3306;
                            $db_host = '';
                        }
                        $db_user = $system_config['db_user'];
                        $db_pwd = $system_config['db_pwd'];

                        $sql = "INSERT INTO adminserverlist (`id`,`gid`,`sname`,`description`,`sdb`,`url`,`otime`,`visible_time`,`default`,`channel_list`,`game_ip`,`game_port`,`db_port`,`db_host`,`s_num`)
                        VALUES ('{$val['num']}','{$val['gid']}','$sname','{$val['title']}','$sdb','{$val['domain']}','{$val['otime']}','{$visible_time}','1',
                          '{$val['channel_list']}','$game_ip','$game_port','$db_port','$db_host','$s_num')";

                        $this->admindb->query($sql);
                        $insertid = $this->admindb->getLastInsertId();
                        if ($insertid) {
                            $sql = "update adminserverlist_plan set state=1,sid='$insertid' where id='{$val['id']}'";
                            $this->admindb->query($sql);
                            $log = "{$val['title']},服名:{$sdb},时间:{$val['otime']}";
                            Admin::adminLog($log);
                        } else {
                            alert("{$sdb}审核失败");
                        }

                        $data = array(
                            'db_host'=>$db_host,
                            'db_port'=>$db_port,
                            'db_user'=>$db_user,
                            'db_pwd'=>$db_pwd,
                        );

                        //$data = $this->admindb->fetchOne("select id,gid,sdb,db_host,db_port,db_user,db_pwd,channel_list,url,description,otime from adminserverlist where id = $insertid");
                        $conn = mysql_connect($data['db_host'].":".$data['db_port'],$data['db_user'],$data['db_pwd'],true);
                        if($conn){
                            if(mysql_select_db($sdb,$conn)){
                                mysql_query("set names utf8");
                                $push_url = $system_config['push_url']?$system_config['push_url']:'';
                                $version = date('Y-m-d-H-i-s',$val['otime']);
                                $sql = "REPLACE INTO base_game (cf_name, cf_value)
                                  VALUES
                                    ('channel_list','{$val['channel_list']}'),
                                    ('game_domain', '{$val['domain']}'),
                                    ('game_title', '{$val['title']}'),
                                    ('gid', '{$val['gid']}'),
                                    ('sid', '{$val['num']}'),
                                    ('sdb', '{$sdb}'),
                                    ('ip', '$game_ip'),
                                    ('port', '$game_port'),
                                    ('version', '{$version}'),
                                    ('push_url','{$push_url}')";
                                mysql_query($sql);
                                mysql_close();
                            }
                        }

                    }
                }
                Admin::adminLog("审核开服通过:$ids");
                alert('审核成功');

            } else {
                $sql = "update adminserverlist_plan set state=2 where id in($ids)";
                $this->admindb->query($sql);
                Admin::adminLog("审核开服拒绝:$ids");
                alert('拒绝成功');
            }
        } else {
            alert('提交为空');
        }
    }

    public function review_plan_edit()
    {
        Auth::check_action(Auth::$SERVER_SH);
        $id = request('id','int');
        $title = request('title','str');
        $domain = request('domain','str');
        if (!empty($id)) {
            $sql = "update adminserverlist_plan set title='{$title}', domain='{$domain}' where id={$id}";
            $this->admindb->query($sql);
            Admin::adminLog("审核开服编辑:$id;$title;$domain");
            alert('操作成功');
        } else {
            alert('参数错误');
        }
    }

    public function apply_plan_del()
    {
        Auth::check_action(Auth::$SERVER_APPLY);
        $id = $_GET['id'];
        if (empty($id)) {
            alert('ID为空');
        }

        $sql = "delete from adminserverlist_plan where id='$id' ";

        $this->admindb->query($sql);
        Admin::adminLog("审核开服删除:$id");
        alert('取消成功');
    }

    public function review_plan_del(){
        Auth::check_action(Auth::$SERVER_SH);
        $id = $_GET['id'];
        if (empty($id)) {
            alert('ID为空');
        }

        $sql = "delete from adminserverlist_plan where id='$id' ";

        $this->admindb->query($sql);
        Admin::adminLog("审核开服删除:$id");
        alert('取消成功');
    }

    public function get_merged_suggest()
    {
        $sql = "select gid,data from merged_suggest";
        $info = Config::centerdb()->fetchRow($sql);
        $list = array();
        if (!empty($info)) {
            foreach ($info as $key => $val) {
                if (!empty($val['data'])) {
                    $data = json_decode($val['data'], true);
                }
                if ($data['tavg7'] < 10) {
                    $list[$val['gid']]['not']++;
                }
                $sql = "select count(*) as t from adminserverlist where gid='{$val['gid']}' and `default`=1";
                $tinfo = $this->admindb->fetchOne($sql);
                $list[$val['gid']]['total'] = $tinfo['t'];
            }
        }
        if (!empty($list)) {
            foreach ($list as $key => $val) {
                $list[$key]['rate'] = round($val['not'] / $val['total'] * 100, 2);
            }
        }
        return $list;
    }


    /**
     * 平台公告
     */
    public function notice()
    {
        $where = '1';
        if($_SESSION['gids']){
            $where .= " and gid in (".implode(',',$_SESSION['gids']).") ";
        }

        $sql = " SELECT title, content, time,gid FROM notice where $where";
        $list = Config::centerdb()->fetchRow($sql);

        //专区列表
        $glist = $this->get_allgroup_list();

        $assign['list'] = $list;
        $assign['glist'] = $glist;

        $this->display('service_notice.shtml', $assign);
    }

    /**
     * 添加平台公告
     */
    public function add_notice()
    {
        Auth::check_action(Auth::$NOTICE_MAN);
        $glist = $this->get_allgroup_list();
        $assign['glist'] = $glist;

        $this->display('service_notice_add.shtml', $assign);
    }

    /**
     * 添加平台公告
     */
    public function insert_notice()
    {
        //查询平台列表
        Auth::check_action(Auth::$NOTICE_MAN);
        $title = request('title','str');
        $content = request('content','str');
        $gid = request('gid','int');
        if (empty($title) || empty($content) || empty($gid)) {
            alert(Ext_Template::lang("不能为空！"));
        }

        $sql = "REPLACE INTO `notice` (
                `gid`,
                `title`,
                `content`,
                `time`
              )
              VALUES
                (
                  '$gid',
                  '$title',
                  '$content',
                  unix_timestamp(now())
                );";

        Config::centerdb()->query($sql);
        $this->clear_cache($gid);
        Admin::adminLog("平台公告添加:$title");

        echo '<script>alert("添加成功!");window.location.href="/action_gateway.php?ctl=service&act=notice";</script>';

    }


    /**
     * 删除平台公告
     */
    public function delete_notice()
    {
        Auth::check_action(Auth::$NOTICE_MAN);
        //查询平台列表
        $gid = request('gid','int');
        if (empty($gid)) {
            alert("gid不能为空！");
        }
        $sql = "DELETE FROM notice WHERE gid = $gid";
        Config::centerdb()->query($sql);
        $this->clear_cache($gid);
        Admin::adminLog("平台公告删除:$gid");
        echo '<script>alert("删除成功!");window.location.href="/action_gateway.php?ctl=service&act=notice";</script>';
    }

    /**
     * 清公告缓存
     */
    public function clear_cache($gid)
    {
        
        //查询平台下所有渠道
        $gid = request('gid','int');
        
        $sql = "select * from channel_list where gid=$gid";
        $res = $this->admindb->fetchRow($sql);
        foreach ($res as $key => $val) {
            $cache_key = sprintf(Key::$api['notice'],$val['channel_str']);
            $cache = Ext_Memcached::getInstance("api");
            $cache->delete($cache_key);
        }
        return true;
    }


    /**
     * 合服管理
     */
    function hefu_list(){
        $url = '?ctl=service&act=hefu_list';
        $srv_group = $this->get_allgroup_list();
        $assign['srv_group'] = $srv_group;

        $gid = request('gid','int');
        $status = request('status','int');
        $time = request('time','str');

        $where = ' status = '.$status;
        if ($time) {
            $where .= " and  date(from_unixtime(start_time))= '$time' ";
        }
        if ($gid) {
            $where .= " and platform='$gid'";
        }

        global $server_list_info;
        $res2 = array();
        foreach ($server_list_info as $v) {
            $res2[$v['id']] = $v['description'];
        }

        $total_record = 0;
        $sql = "select count(1) as c from hefu_list where $where";
        $total = $this->admindb->fetchOne($sql);
        if($total['c']){
            $total_record = $total['c'];
        }
        $per_page = 50;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $list = array();
        if($total_record){
            $sql = "select id,platform,main_server,REPLACE(REPLACE(`server`,'[',''),']','') as `server`,start_time,`status`,sid_list,sid from hefu_list where $where $limit";
            $list = $this->admindb->fetchRow($sql);
            foreach ($list as $key => $val) {
                $csss = array();
                $css = explode(',', $val['sid_list']);
                foreach ($css as $k => $v) {
                    $csss[] = $res2[$v];
                }
                $list[$key]['str'] = '[' . implode(',', $csss) . ']';
            }
        }
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query(array_filter($_REQUEST)) . "&page=",
        );

        $assign['list'] = $list;
        $assign['status'] = $status;
        $export = $_GET['export'];
        if ($export == '导出Excel') {
            $elist = $list;
            require_once("Classes/PHPExcel.php");
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, 1, '序号');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, 1, '平台');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, 1, '主服');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, 1, '合服游戏服');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, 1, '合服时间');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, 1, '状态');
            foreach ($elist as $key => $val) {
                foreach ($val as $v) {
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $key + 2, ' ' . $key + 1);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $key + 2, $val['platform']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $key + 2, ' ' . $val['main_server']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $key + 2, $val['server']);

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $key + 2, date('Y-m-d H:i:s', $val['start_time']));
                    if ($val['status'] == 0) {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 2, '等待合服');
                    } else {
                        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 2, '合服完成');
                    }
                }

            }
            $objPHPExcel->getActiveSheet()->setTitle('合服列表');
            if (empty($platform)) {
                $platform = '全平台';
            }
            $excelName = 'all_combine_list_' . date("Ymd", time()) . '.xls';
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            header('Content-Type: application/vnd.ms-excel; charset=utf-8');
            header("Content-Disposition: attachment; filename=" . $excelName);
            header('Cache-Control: max-age=0');
            header("Pragma: no-cache");
            $objWriter->save('php://output');
        }

        $this->display(CENTER_TEMP . 'hefu_list.shtml', $assign);
    }

    /**
     * 合服申请
     */
    public function hefu_apply(){
        Auth::check_action(Auth::$HF_APPLY);
        $gid = request('gids','int');
        $hf_time = request('hf_time');
        $sids = request('sids','arr');
        $sids = array_filter($sids);
        $sids_arr = array();
        foreach ($sids as $v) {
            $sids_arr[] = intval($v);
        }
        $main_sid = request('main_sid','int');
        if(!$gid){
            alert(Ext_Template::lang("请选择专服"));
        }
        if(!$sids){
            alert(Ext_Template::lang("请选择要合服的游戏服"));
        }
        if(!$main_sid){
            alert(Ext_Template::lang("请选择主服"));
        }
        if(count($sids_arr)<=1){
            alert(Ext_Template::lang("至少选择两个游戏服进行合服操作"));
        }
        if(!in_array($main_sid,$sids_arr)){
            alert(Ext_Template::lang("主服不在合服列表中"));
        }
        $hf_time = strtotime($hf_time);
        if ($hf_time < time()) {
            alert(Ext_Template::lang("合服时间必须大于当前时间!"));
        }
        //TODO
        //查询是否在合服列表中，还未合服
        foreach ($sids_arr as $v) {
            $sql = "select count(1) as c from hefu_list where FIND_IN_SET({$v},sid_list) and status=0";
            $c = $this->admindb->fetchOne($sql);
            if(!empty($c['c'])){
                alert(Ext_Template::lang("服:{$v}已申请合服,请重新选择!"));
            }
        }
        $sids_str = implode(',',$sids_arr);
        $sql = "select id,sdb,description,main_id from adminserverlist where id in ({$sids_str})";
        $re = $this->admindb->fetchRow($sql);
        if($re){
            $main_server = '';
            $server = array();
            $str = '';
            foreach ($re as $v) {
                if($v['main_id']!=0){
                    $str.= "{$v['description']}[{sdb}];";
                }
                if($v['id'] == $main_sid){
                    $main_server = $v['sdb'];
                }
                $server[] = $v['sdb'];
            }
            $server = implode(',',$server);
            if($str){
                alert(Ext_Template::lang("以下游戏服{$str}已经是从服!"));
            }
            //插入一条数据
            $sql = "insert into hefu_list(platform,main_server,server,start_time,sid_list,sid) values('$gid','$main_server','$server','$hf_time','$sids_str','$main_sid') ";
            $re = $this->admindb->query($sql);
            if($re){
                Admin::adminLog("合服申请:[$main_sid][$sids_str]");
                alert(Ext_Template::lang("合服申请成功!"));
            }

        }
        alert(Ext_Template::lang("合服申请失败!"));
    }

    /**
     * 通过gid sids获取游戏服
     * 合服管理用
     */
    public function ajax_server_list_to_option(){
        Auth::check_action(Auth::$HF_APPLY);
        $gid = request('gid','int');
        $sids = request('sids','str');
        $str = '';
        $flag = 0;
        if($gid){
            $time = time();
            $sql = "select id,gid,sdb as sname,description from adminserverlist where gid={$gid} and  `default` =1 and otime<$time order by gid,id";
            $slist = $this->admindb->fetchRow($sql);
            if($slist){
                $flag = 1;
                foreach ($slist as $v) {
                    $str .= "<option value='{$v['id']}' >{$v['description']}[{$v['sname']}]</option>";
                }
            }
        }
        if($sids){
            $time = time();
            $sql = "select id,gid,sdb as sname,description from adminserverlist where id in ({$sids}) and  `default` =1 and otime<$time order by gid,id";
            $slist = $this->admindb->fetchRow($sql);
            if($slist){
                $flag = 1;
                foreach ($slist as $v) {
                    $str .= "<option value='{$v['id']}' >{$v['description']}[{$v['sname']}]</option>";
                }
            }
        }
        exit(json_encode(array('flag'=>$flag,'json'=>$str)));
    }

    /**
     * 合服完成操作
     */
    public function hefu_list_post()
    {
        //使用方法
        $ids = request('ids','str');
        $status = request('status','int');
        if(!$ids){
            alert("参数错误!");
        }
        $ids_arr = array_filter(explode(',',$ids));
        if(count($ids_arr)<1){
            alert('至少选择一个进行操作');
        }
        foreach ($ids_arr as &$id) {
            $id = intval($id);
        }
        $ids_str = implode(',',$ids_arr);
        //check
        $rets = array();
        if ($status == 9) {
            Auth::check_action(Auth::$HF_MAN);
            //处理合服 adminserlist
            foreach ($ids_arr as $hefuId) {
                $rst = Config::deal_hefu($hefuId);
                $rets[] = "任务{$hefuId}:".($rst?'成功!':'失败!');
            }
            Admin::adminLog("合服完成操作:[$ids_str]:".implode('|'.$rets));
        } else {
            Auth::check_action(Auth::$HF_APPLY);
            $sql = "delete from hefu_list where id in ($ids_str)";
            $this->admindb->query($sql);
            Admin::adminLog("合服取消操作:[$ids_str]");
        }
        alert('操作成功！'.implode('|',$rets));
    }

    // 停更列表
    public function stop_update_server_list()
    {
        //游戏服列表
        $sql = " SELECT id AS sid, gid,sname,description,IF(`default` = 1,'正式服','维护中') AS `default` FROM adminserverlist WHERE  `default` IN (1, 4) AND main_id = 0";
        $res = $this->admindb->fetchRow($sql);

        foreach ($res as $key => $val) {
            $server_list[$val['gid']][$val['sid']]['sname'] = $val['sname'];
            $server_list[$val['gid']][$val['sid']]['description'] = $val['description'];
            $server_list[$val['gid']][$val['sid']]['default'] = $val['default'];
        }

        // 专服列表
        $sql = "SELECT platform_name,platform_str,gid FROM adminplatformlist";
        $res = $this->admindb->fetchRow($sql);

        foreach ($res as $key => $val) {
            $platform_list[$val['gid']]['platform_name'] = $val['platform_name'];
            $platform_list[$val['gid']]['platform_str'] = $val['platform_str'];
        }

        $assign['server_list'] = $server_list;
        $assign['platform_list'] = $platform_list;

        $this->display(CENTER_TEMP . 'stop_update_server_list.shtml', $assign);

    }

    // 停更列表
    public function stop_update_server_list_update()
    {
        Auth::check_action(Auth::$SERVER_STOP);
        $sids= request('sids');
        $default = request('default','int');;
        $stop_service_describe = request('stop_service_describe','str');;
        $list =array();
        foreach ($sids as $key => $val) {
            $list[$key] = implode(',', $val);
        }

        if ($default == 1) {//正式服
            foreach ($list as $key => $val) {
                $sql = "UPDATE adminserverlist SET `default` = '$default', `stop_service_describe` = '' WHERE id IN ($val)";
                $this->admindb->query($sql);
                //合服的子服，也设定为合服状态
                $sql = "UPDATE adminserverlist SET `default` = '6', `stop_service_describe` = '' WHERE main_id IN ($val) ";
                $this->admindb->query($sql);
            }
            Admin::adminLog("停更维护-修改服务器状态为正式服:".implode(',',$list));
        } else {//维护中
            foreach ($list as $key => $val) {
                $sql = "UPDATE adminserverlist SET `default` = '$default',`stop_service_describe` = '$stop_service_describe' WHERE id IN ($val)";
                $this->admindb->query($sql);
                //合服的子服，也设定为维护中
                $sql = "UPDATE adminserverlist SET `default` = '$default', `stop_service_describe` = '$stop_service_describe' WHERE main_id IN ($val) ";
                $this->admindb->query($sql);
            }
            Admin::adminLog("停更维护-修改服务器状态为维护中:".implode(',',$list));
        }
        alert('生成成功');
    }

    /**
     * 合服失败回滚操作
     */
    public function hefu_roll_back_action(){
        Auth::check_action(Auth::$HF_MAN);
        $hefuId = request('hefu_id','int');
        if($hefuId){
            $rst = Config::deal_hefu_roll_back($hefuId);
            if($rst){
                alert('操作成功!');
            }
        }
        alert('操作失败');
    }


    // 合服停更列表
    public function hefu_stop_update_server_list()
    {
        //查询需要合服的列表
        $sql = "select sid_list from hefu_list where `status`=0";
        $cs = $this->admindb->fetchRow($sql);
        $css = array();
        foreach ($cs as $key => $val) {
            $css[] = $val['sid_list'];
        }
        $str = implode(',', $css);
        if ($str) {
            //游戏服列表
            $sql = " SELECT id AS sid,gid,sname,description,
                  IF (`default` = 1,'正式服','维护中') AS `default`
                  FROM adminserverlist
                  WHERE id in($str)
                  AND `default` in (1,4) ";
            $res = $this->admindb->fetchRow($sql);
            foreach ($res as $key => $val) {
                $server_list[$val['gid']][$val['sid']]['sname'] = $val['sname'];
                $server_list[$val['gid']][$val['sid']]['description'] = $val['description'];
                $server_list[$val['gid']][$val['sid']]['default'] = $val['default'];
            }
        }

        // 专服列表
        $sql = "SELECT platform_name,platform_str,gid FROM adminplatformlist";
        $res = $this->admindb->fetchRow($sql);

        foreach ($res as $key => $val) {
            $platform_list[$val['gid']]['platform_name'] = $val['platform_name'];
            $platform_list[$val['gid']]['platform_str'] = $val['platform_str'];
        }

        $assign['server_list'] = $server_list;
        $assign['platform_list'] = $platform_list;
        $assign['hefu'] = 1;

        $this->display(CENTER_TEMP . 'stop_update_server_list.shtml', $assign);
    }

    /**
     * 获取开服申请的服id
     */
    public function ajax_check_server_id(){
        $gid = request('gid','int');
        if(!$gid){
            exit(json_encode(array('flag'=>0,'json'=>'')));
        }
        $gids_f = $this->check_server_id($gid);
        $msg = "当前专服下游戏服ID取值范围:".$gids_f['min']."~".$gids_f['max'];
        $sql = "select max(num) as num from adminserverlist_plan where gid={$gid} and state != 2 limit 1";
        $re = $this->admindb->fetchOne($sql);
        if($re['num']){
            exit(json_encode(array('flag'=>1,'json'=>$re['num']+1,'msg'=>$msg)));
        }
        $sql = "select max(id) as num from adminserverlist where gid={$gid} limit 1";
        $re = $this->admindb->fetchOne($sql);
        if($re['num']){
            exit(json_encode(array('flag'=>1,'json'=>$re['num']+1,'msg'=>$msg)));
        }
        exit(json_encode(array('flag'=>1,'json'=>$gid + 1,'msg'=>$msg)));
    }

    /**
     * 检查服id范围是否合法
     */
    private function check_server_id($gid){
        if(!$gid){
            return false;
        }
        if(($gid % 10000)!= 0){
            return false;
        }
        return array(
            'min'=> $gid+1,
            'max'=> $gid+10000,
        );
    }

}
