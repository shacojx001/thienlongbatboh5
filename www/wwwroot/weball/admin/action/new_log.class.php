<?php
/**
 * 新日志功能
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_new_log extends action_superclass
{
    public function log_show()
    {
        $id = $_GET['id'];
        $sql = "SELECT * FROM base_new_log";
        $res = $this->admindb->fetchRow($sql, 'id');

        $base_log = $res[$id];
        $find_array = unserialize($base_log['find_array']);
        $relation_array = unserialize($base_log['relation_array']);

        //数据库切换
        $db_res = $this->db_change($base_log['find_db'], $_GET['kf_center']);
        $this->db = $db_res['db'];

        //如果有传入sql_orders，则用传入的
        if ($_GET['sql_submit'] == 1) {
            //传入值过滤
            $SQL = $_GET['sql_orders'];
            //拼接SQL-where
            // $SQL_WHERE = " WHERE 1 ";
            foreach ($find_array as $key => $val) {
                $SQL_WHERE .= $this->sql_where($key, $val);
            }
        } else {
            $SQL = $base_log['sql'];
            //拼接SQL-where
            $SQL_WHERE = " WHERE 1 ";
            foreach ($find_array as $key => $val) {
                $SQL_WHERE .= $this->sql_where($key, $val);
            }
        }
        //如果原始SQL存在$where 则用$SQL_WHERE替换
        if (strpos($SQL, '$where') !== false) {
            //原始SQL
            $sql = str_replace('$where', $SQL_WHERE, $SQL);
        } else {
            //原始SQL
            $sql = $SQL . $SQL_WHERE;
        }

        //SQL排序
        if ($_GET['DESC']) {
            $sort = $_GET['DESC'];
            $SQL_ORDER = " ORDER BY `$sort` DESC ";
        } elseif ($_GET['ASC']) {
            $sort = $_GET['ASC'];
            $SQL_ORDER = " ORDER BY `$sort` ASC ";
        } else {
            $SQL_ORDER = " " . $base_log['order_by_sql'] . " ";
        }

        //导表
        if ($_GET['submit'] == '导出') {
            $this->export_xls($_GET['sql_orders'], $this->db, $relation_array, $base_log['title']);
        }
        $sql = str_replace(PHP_EOL, ' ', $sql);  
        $sql = preg_replace("/[\s]+/is"," ",$sql);
  
      
        
        //分页SQL
        $sql_page = preg_replace("/^(select) (.*?) (from) (.*?)/i", '$1 count(1) as num $3 $4', $sql);
   
        //$sql_page = "SELECT count(1) AS num  FROM($sql) a ";
        $total = $this->db->fetchOne($sql_page);
   
   // print_r($sql_page);exit;   
        $p = array(
            'count' => $total['num'],       //总条数
            'showPages' => 8,   //需要显示的页数
            'currPage' => $_GET['page'] ? $_GET['page'] : 1,     //当前页
            'subPages' => $_GET['subPages'] ? $_GET['subPages'] : $base_log['default_page'],   //每页显示条数
            // 'href' =>$total['num'],         //连接 
        );
        $page = new Page($p['count'], $p['showPages'], $_GET['page'] ? $_GET['page'] : 1, $p['subPages']);
        $limit = $page->getLimit();

        //显示数据SQL
        if ($_GET['sql_submit'] == 1) {
            $sql_end = $sql . $limit;
            $assign['sql_orders'] = $sql;
        } else {
            $sql_end = $sql . $SQL_ORDER . $limit;
            $assign['sql_orders'] = $sql . $SQL_ORDER;
        }
        $res = $this->db->fetchRow($sql_end);
        //字段替换
        $res = $this->relation_fun($relation_array, $res);

        if ($_GET['request']) {
            print_r(array_merge($_GET, $_POST));
        }
        if ($_GET['sql'] && empty($_GET['sql_submit'])) {
            print_r('<br/><k id="show_sql">' . $sql_end . '</k>');
        } elseif ($_GET['sql'] && $_GET['sql_submit'] == 1) {
            print_r('<br/><k id="show_sql">' . $sql . '</k>');
        }

        $assign['page'] = $page->showPages();
        $assign['list'] = $res;
        $assign['superscript'] = array_keys($res[0]);

        $assign['find_array'] = $find_array;
        $assign['title'] = $base_log['title'];
        $assign['id'] = $id;
        $assign['find_db'] = $base_log['find_db'];

        $assign['sql_power'] = $_GET['sql_power'];

        if ($find_array)
            foreach ($find_array as $v) {
                if ($v['items']) {
                    $assign['items'] = $this->get_list('items');
                }
                if ($v['goods']) {
                    $assign['goods'] = $this->get_list('goods');
                }
                if ($v['out_in']) {
                    $assign['out_in'] = $this->get_list('out_in');
                }
                if ($v['opt']) {
                    $assign['opt'] = $this->get_list('opt');
                }
                if ($v['shop']) {
                    $assign['shop'] = $this->get_list('shop');
                }
            }

        unset($_GET['page']);
        $assign['url'] = htmlentities($_SERVER['PHP_SELF']) . '?' . http_build_query($_GET);

        $this->display(CENTER_TEMP . 'new_log.shtml', $assign);
    }


    //拼接SQL-where
    function sql_where($key, $val)
    {
        //判断 如果带(. `)的，不用加
        if (strpos($val['sql_id'], '.') !== false || strpos($val['sql_id'], '`') !== false) {
            $sql_id = $val['sql_id'];
        } else {
            $sql_id = '`' . $val['sql_id'] . '`';
        }
        if ($val['time'] == 1 && ($_GET[$key . '_start'] || $_GET[$key . '_end'])) {//时间
            //时间区间转换函数
            $time_result = $this->time_translate('time', $_GET[$key . '_start'], $_GET[$key . '_end']);
            if ($time_result) {
                $SQL_WHERE = " AND " . $sql_id . $time_result;
            }
        } elseif ($val['range'] == 1 && ($_GET[$key . '_start'] || $_GET[$key . '_end'])) {//区间
            //时间区间转换函数
            $time_result = $this->time_translate('range', $_GET[$key . '_start'], $_GET[$key . '_end']);
            if ($time_result) {
                $SQL_WHERE = " AND " . $sql_id . $time_result;
            }
        } elseif ($val['vague'] == 1 && $_GET[$key]) {//模糊查询
            $vague = trim($_GET[$key]);
            $SQL_WHERE = " AND " . $sql_id . " LIKE '%$vague%'";
        } elseif ($_GET[$key]) {
            $value = trim($_GET[$key]);
            $SQL_WHERE = " AND " . $sql_id . " = '$value'";
        } else {
            $SQL_WHERE = " ";
        }
        return $SQL_WHERE;
    }

    //数据库切换
    function db_change($find_db, $kf_center = "")
    {
        switch ($find_db) {
            case '1'://游戏服
                $this->db = $this->db;
                break;
            case '2'://center
                $this->db = Config::centerdb();
                break;
            case '3'://admin
                $this->db = $this->admindb;
                break;
            case '4'://跨服中心
                $this->db = '';
                break;
            case '5'://游戏服、跨服中心
                break;
        }

        $return = array(
            'db' => $this->db,
            'kf_center_list' => array(),
        );
        return $return;
    }

    //导表
    function export_txt($sql, $db, $relation_array, $title)
    {
        Auth::check_action(Auth::$COMMON_LOG_DOWN);
        Admin::adminLog("导出日志:$sql");
        ini_set('memory_limit', '2048M');
        $list = $db->fetchRow($sql);
        $list = $this->relation_fun($relation_array, $list);
        $line1 = array_keys($list[0]);
        $data .= implode("\t", $line1);
        $data .= "\r\n";
        foreach ($list as $key => $val) {
            $line = array();
            foreach ($val as $k => $v) {
                $line[] = $v;
            }
            $data .= implode("\t", $line);
            $data .= "\r\n";
        }

        header("content-type:application/octet-stream");
        header('Content-Disposition: attachment; filename=' . $title . "_" . date("Y-m-d H:i:s") . '.txt');
        header("Content-Transfer-Encoding: binary");
        echo $data;
        exit;
    }

    //导表
    function export_xls($sql, $db, $relation_array, $title)
    {
        Auth::check_action(Auth::$COMMON_LOG_DOWN);
        Admin::adminLog("导出日志:$sql");
        ini_set('memory_limit', '2048M');
        $list = $db->fetchRow($sql);
        $list = $this->relation_fun($relation_array, $list);
        $headerArray = array_keys($list[0]);
        $dataArray = array();
        foreach ($list as $key => $val) {
            $line = array();
            foreach ($val as $k => $v) {
                $line[] = $v;
            }
            $dataArray[] = $line;
        }

        $this->export_to_excel($headerArray,$dataArray,$title);
    }

    //获取常用列表
    function get_list($type)
    {
        $list = array();
        switch ($type) {
            case 'items'://物品
                $list = Goods::goods_list(1);
                break;
            case 'goods'://商品
                $list = Goods::goods_list(2);
                break;
            case 'out_in'://消耗产出
                $list = array(
                    1 => '产出',
                    2 => '消耗',
                );
                break;
            case 'opt'://商品
                $list = Goods::goods_list(3);
                break;
            case 'shop'://商店
                $list = Goods::goods_list(4);
                break;
            case 'pet'://商店
                $list = Goods::goods_list(6);
                break;
            case 'mount'://商店
                $list = Goods::goods_list(7);
                break;
            default://空
                break;
        }

        return $list;
    }

    //字段替换
    function relation_fun($relation_array, $res)
    {
        $relation_list = array();
        $relation_key_base = array();
        //替换字段
        if (!$relation_array) {
            return $res;
        }
        foreach ($relation_array as $key => $val) {
            //type 1:自定义类型
            //type 2:物品表类型
            //type 3:商品表类型
            //type 4:变动原因表类型
            //type 5:商店表类型
            switch ($val['type']) {
                case 1:
                    $relation_list[$val['relation_val']] = $val['relation_list'];
                    break;
                case 2:
                    $relation_list[$val['relation_val']] = $this->get_list('items');
                    break;
                case 3:
                    $relation_list[$val['relation_val']] = $this->get_list('goods');
                    break;
                case 4:
                    $relation_list[$val['relation_val']] = $this->get_list('out_in');
                    break;
                case 5:
                    $relation_list[$val['relation_val']] = $this->get_list('opt');
                    break;
                case 6:
                    $relation_list[$val['relation_val']] = $this->get_list('shop');
                    break;
                case 7:
                    $relation_list[$val['relation_val']] = $this->get_list('pet');
                    break;
                case 8:
                    $relation_list[$val['relation_val']] = $this->get_list('mount');
                    break;

                default://空
                    break;
            }

            $relation_key_base[$val['relation_val']] = $val['relation_key'];
        }

        //字段替换执行
        foreach ($res as $key => $val) {
            foreach ($val as $kk => $vv) {
                //判断key是否存在
                if (array_key_exists($kk, $relation_key_base)) {
                    //关联key
                    $base_key = $val[$relation_key_base[$kk]];
                    $res[$key][$kk] = $relation_list[$kk][$base_key] ? $relation_list[$kk][$base_key] : $res[$key][$kk];
                }
            }
        }

        return $res;
    }//fun end

    //获取跨服中心
    function get_kf_list()
    {
        $sql = "SELECT
                  platform_name,
                  kf_center
                FROM
                  adminplatformlist
                WHERE
                  kf_center != ''";
        $res = $this->admindb->fetchRow($sql);
        $list = array();
        foreach ($res as $key => $val) {
            $list[$val['kf_center']] = $val['platform_name'];
        }

        return $list;
    }

    //传入时间转化
    function time_translate($type, $kstart_time, $kend_time)
    {
        //不存在
        if (empty($kstart_time) && empty($kend_time)) {
            return false;
        }
        if ($type == 'time') {//时间类型转时间戳  区间类型则不需要
            //时间转时间戳
            $start_time = strtotime($kstart_time);
            $end_time = strtotime($kend_time) + 86400 - 1;
        } else {
            $start_time = $kstart_time;
            $end_time = $kend_time;
        }
        if ($kstart_time && $kend_time) {
            $str = " BETWEEN " . $start_time . " AND " . $end_time;
        } elseif ($kstart_time) {
            $str = " >=$start_time ";
        } elseif ($kend_time) {
            $str = " <=$end_time ";
        }
        return $str;
    }

    //日志列
    function new_log_list_show()
    {
        //检查查看权限
        Auth::check_action(Auth::$COMMON_LOG_MAN);
        $assign = $this->get_server_rights();

        if ($assign['channel']) {
            $assign['power'] = 0;
        } else {
            $assign['power'] = 1;
        }
        $sql = "SELECT
                `id`,
                `title`,
                CASE `find_db`
              WHEN 1 THEN
                '游戏服'
              WHEN 2 THEN
                'center'
              WHEN 3 THEN
                'admin'
              WHEN 4 THEN
                '跨服中心'
              WHEN 5 THEN
                '混合'                  
              END AS find_db,
               `sql`,
              `ordinary` 
              FROM
                base_new_log";
        $res = $this->admindb->fetchRow($sql);

        $list = array();
        foreach ($res as $key => $val) {
            $list[$val['ordinary']][] = $val;
        }

        $assign['log_array'] = array(
            '1' => '通用日志列',
            '2' => '数据统计列',
            '3' => '服务器信息',
            '4' => '特别日志',
        );

        $assign['list'] = $list;
        $this->display(CENTER_TEMP . 'new_log_list_show.shtml', $assign);
    }

    //日志添加
    function log_add()
    {
        Auth::check_action(Auth::$COMMON_LOG_MAN);
        $id = $_GET['id'];
        if ($id) {
            $sql = "SELECT * FROM base_new_log WHERE id = $id  ";
            $res = $this->admindb->fetchOne($sql);
            $res['find_array'] = unserialize($res['find_array']);
            $res['relation_array'] = unserialize($res['relation_array']);
            $assign['list'] = $res;
        }
        $this->display(CENTER_TEMP . 'new_log_add.shtml', $assign);
    }

    //日志保存
    function log_save()
    {
        $id = (int)$_POST['id'];
        //id不存在则最大值+1
        if (empty($id)) {
            Auth::check_action(Auth::$COMMON_LOG_ADD);
            $sql = "SELECT IFNULL(max(id), 0) + 1 AS id FROM base_new_log";
            $res = $this->admindb->fetchOne($sql);
            $id = $res['id'];
        } else {
            Auth::check_action(Auth::$COMMON_LOG_EDIT);
        }
        $sql_str = $_POST['sql'];
        if (empty($sql_str)) {
            alert("sql不能为空！");
        }
        $title = trim($_POST['title']);
        $find_db = $_POST['find_db'];//数据库类型

        /*
         * 搜索字段设置
         */
        $sql_id = $_POST['sql_id'];//需要搜索字段
        $name_list = $_POST['name_list'];//需要搜索字段名
        $sort_list = $_POST['sort_list'];//搜索字段的排序
        $where_type = $_POST['where_type'];//搜索字段类型

        //排序设置
        $order_by_sql = $_POST['order_by_sql'];

        /*
         * 替换字段设置
         */
        $relation_list = $_POST['relation_list'];//被替换字段
        $replace_list = $_POST['replace_list'];//替换成目标字段
        $relation_type = $_POST['relation_type'];//替换字段来源表
        $relation_key = $_POST['relation_key'];//自定义-被替换字段
        $relation_val = $_POST['relation_val'];//自定义-替换成目标字段

        /*
         * 显示行设置
         */
        $default_page = $_POST['default_page'];

        /*
         * 日志类型设置
         */
        $ordinary = $_POST['ordinary'];

        /*
         * 处理搜索字段
         */
        $find_array = array();
        foreach ($sql_id as $key => $val) {
            $accurate = $where_type[$key] == 1 ? 1 : 0;//精确查询
            $vague = $where_type[$key] == 2 ? 1 : 0;//模糊查询
            $time = $where_type[$key] == 3 ? 1 : 0;//时间类型
            $range = $where_type[$key] == 4 ? 1 : 0;//区间类型
            $items = $where_type[$key] == 5 ? 1 : 0;//物品
            $goods = $where_type[$key] == 6 ? 1 : 0;//商品
            $out_in = $where_type[$key] == 7 ? 1 : 0;//产出消耗
            $opt = $where_type[$key] == 8 ? 1 : 0;//变动原因
            $shop = $where_type[$key] == 9 ? 1 : 0;//变动原因
            //排序存在则读排序，不存在可以不读
            $sort = $sort_list[$key];
            $find_array[] = array(
                'sql_id' => trim($val),
                'sort' => trim($sort),
                'name' => trim($name_list[$key]),
                'accurate' => $accurate,//精确
                'vague' => $vague,//模糊
                'time' => $time,//时间
                'range' => $range,//区间
                'items' => $items,//区间
                'goods' => $goods,//区间
                'out_in' => $out_in,//区间
                'opt' => $opt,//区间
                'shop' => $shop,//区间
            );
        }
        $find_array = $this->my_sort($find_array, 'sort', SORT_ASC);
        $find_array = serialize($find_array);

        //替换字段(需要时候)
        $relation_array = array();
        if ($_POST['relation_show'] == 2) {
            foreach ($relation_list as $key => $val) {
                $relation_array[$val] = array(
                    'key' => $key,
                    'relation_key' => $val,
                    'relation_val' => $replace_list[$key],
                    'type' => $relation_type[$key],
                    'relation_list' => array_combine($relation_key[$key], $relation_val[$key]),
                );
            }
        }
        $relation_array = serialize($relation_array);

        $sql_str = addslashes($sql_str);
        $sql = "REPLACE INTO base_new_log (
                `id`,
                `title`,
                `find_db`,
                `sql`,
                `find_array`,
                `order_by_sql`,
                `relation_array`,
                `default_page`,
                `ordinary`
              )
              VALUES
                (
                  '$id',
                  '$title',
                  '$find_db',
                  '$sql_str',
                  '$find_array',
                  '$order_by_sql',
                  '$relation_array',
                  '$default_page',
                  '$ordinary'
                )";
// print_r($sql);exit;       
        $this->admindb->query($sql);

        Admin::adminLog("编辑通过日志:$id");
        alert('添加成功！', "/action_gateway.php?ctl=new_log&act=log_add&id=$id");
    }

    //删除日志
    function log_delete()
    {
        Auth::check_action(Auth::$COMMON_LOG_DEL);
        $id = (int)$_GET['id'];
        if (!$id) alert('非法');
        $sql = "DELETE FROM base_new_log WHERE id = $id";
        $this->admindb->query($sql);
        Admin::adminLog("删除通过日志:$id");
        alert('删除成功！');
    }

    //检查sql有没问题
    function check_sql()
    {
        $sql = $_POST['sql'];
        if (empty($sql)) {
            echo 'sql不能为空！';
        }
        $sql = " EXPLAIN " . $sql;
        $find_db = $_POST['find_db'];
        $db_res = $this->db_change($find_db);
        $this->db = $db_res['db'];
        $sql_res = $this->db->fetchOne($sql);
        echo "ok";
    }

    //返回替换值
    function return_keys()
    {
        $sql = $_POST['sql'];
        $find_db = $_POST['find_db'];
        $db_res = $this->db_change($find_db);
        $this->db = $db_res['db'];
        $res = $this->db->fetchOne($sql);
        $res = array_keys($res);
        echo json_encode(array('data' => $res));
    }

    /**
     * 通用排行榜
     */
    public function show_universal_list()
    {

        $type = request('show_type', 'int');
        $export = request('export', 'int');
        if (empty($type)) {
            $type = 1;
        }
        switch ($type) {
            case 1:      //等级排行
                $fields = array('排名', '等级', '角色名', '职业', 'vip等级', '角色id');  //顺序需与foreach中$data数组顺序保持一致
                $id = 200;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);           //获取游戏服等级排行榜
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);  //解析数据
                    $data = array();
                    $role_ids = array();;
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];        //获取排名
                        $data[$v[1]]['lv'] = $v[2];           //获取等级
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';             //拼凑role_ids
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);             //查询所有的角色
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];    //角色名
                        $data[$val['role_id']]['career'] = $val['career'];           //职业
                        $data[$val['role_id']]['vip_lv'] = $val['vip_lv'];           //vip等级
                        $data[$val['role_id']]['role_id'] = $val['role_id'];         //角色id
                    }
                }
                break;
            case 2:          //战力排行
                $fields = array('排名', '战力', '等级', '角色名', '职业', 'vip等级', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 100;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['power'] = $v[2];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['level'] = $val['level'];
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['career'] = $val['career'];
                        $data[$val['role_id']]['vip_lv'] = $val['vip_lv'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
            case 3:   //坐骑排行
                $fields = array('排名', '阶位', '坐骑等级', '经验', '角色名', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 300;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['lv'] = $v[2];
                        $data[$v[1]]['mount_lv'] = $v[3];
                        $data[$v[1]]['mount_exp'] = $v[4];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
            case 4:   //宠物排行
                $fields = array('排名', '阶位', '星级', '角色名', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 500;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['lv'] = $v[2];
                        $data[$v[1]]['star_lv'] = $v[3];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
            case 5:   //铭文塔排行
                $fields = array('排名', '副本ID', '副本关卡', '完成时间', '角色名', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 600;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['fuben_id'] = $v[2];
                        $data[$v[1]]['fuben'] = $v[3];
                        $data[$v[1]]['finish_time'] = $v[4];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
            case 6:   //脱机效率排行
                $fields = array('排名', '每分钟经验', '角色名', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 700;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['min_exp'] = $v[2];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
            case 7:   //成就排行
                $fields = array('排名', '成就点数', '角色名', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 800;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['count'] = $v[2];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
            case 8:   //充值排行
                $fields = array('排名', '充值数', '角色名', '角色id');//顺序需与foreach中$data数组顺序保持一致
                $id = 900;
                $sql = "select infos from server_rank where id = $id";
                $res = $this->db->fetchOne($sql);
                if (count($res) > 0) {
                    $arr_data = $this->rank_data_to_array($res['infos']);
                    $data = array();
                    $role_ids = array();
                    foreach ($arr_data as $v) {
                        $data[$v[1]]['rank'] = $v[0];
                        $data[$v[1]]['charge'] = $v[2];
                        array_push($role_ids, $v[1]);
                    }
                    $where_role_ids = '(' . implode(',', $role_ids) . ')';
                    $res = $this->get_nick_names_by_role_ids($where_role_ids);
                    foreach ($res as $key => $val) {
                        $data[$val['role_id']]['nick_name'] = $val['nick_name'];
                        $data[$val['role_id']]['role_id'] = $val['role_id'];
                    }
                }
                break;
        }
        if ($export) {       //如果是导出则进行导出
            $this->out_put_excel($fields, $data);
        } else {
            $html = $this->out_put_html($fields, $data);
            $assign['fields'] = $html['fields'];
            $assign['values'] = $html['values'];
            $this->display('show_universal_list.shtml', $assign);
        }
    }


    /**
     * 组装输出表格html
     * @param $fields
     * @param $values
     * @return array
     */
    private function out_put_html($fields, $values)
    {
        $out_put_fields = '';
        $out_put_value = '';
        foreach ($fields as $v) {   //表头
            $out_put_fields .= "<td bgcolor='#dde7ee' nowrap='nowrap' align='middle'><b>$v</b></td>";
        }
        foreach ($values as $key => $val) {
            $out_put_value .= ' <tr>';
            foreach ($val as $key => $v) {
                if ($key == 'career') {
                    switch ($v) {
                        case 1:
                            $v = '战士';
                            break;
                        case 2:
                            $v = '射手';
                            break;
                        case 3:
                            $v = '法师';
                            break;
                    }
                }
                $out_put_value .= '<td bgcolor="#f7f8f9" nowrap="nowrap" align="middle">' . $v . '</td>';    //各字段对应的表头
            }
            $out_put_value .= '</tr>';
        }
        return array('fields' => $out_put_fields, 'values' => $out_put_value);   //返回表头与内容
    }

    /**
     * 组装导出excel
     * @param $fields
     * @param $values
     */
    private function out_put_excel($fields, $values)
    {
        require_once("Classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        foreach ($fields as $key => $val) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($key, 1, $val);   //excel表头
        }
        $i = 0;
        foreach ($values as $key => $val) {
            $index = 0;
            foreach ($val as $k => $v) {
                if ($k == 'career') {
                    switch ($v) {
                        case 1:
                            $v = '战士';
                            break;
                        case 2:
                            $v = '射手';
                            break;
                        case 3:
                            $v = '法师';
                            break;
                    }
                }
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($index, $i + 2, $v . ' ');          //内容
                $index++;
            }
            $i++;
        }
        $server_info = $this->serverinfo();

        $objPHPExcel->getActiveSheet()->setTitle('排行榜');

        $excelName = $server_info['description'] . '-排行榜' . '.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $excelName);
        header('Cache-Control: max-age=0');
        header("Pragma: no-cache");
        $objWriter->save('php://output');
    }

    private function rank_data_to_array($data)
    {
        $temp_res = str_replace('[', '', $data);
        $temp_res = str_replace(']', '', $temp_res);
        $temp_arr = explode('},', $temp_res);
        $arr_data = array();
        foreach ($temp_arr as $v) {
            $v = str_replace('{', '', $v);
            $v = str_replace('}', '', $v);
            $v_arr = explode(',', $v);
            array_push($arr_data, $v_arr);
        }
        return $arr_data;
    }

    private function get_nick_names_by_role_ids($where_role_ids)
    {
        $sql = "select nick_name,career,vip_lv,level,role_id from role_base where role_id in $where_role_ids ";
        $res = $this->db->fetchRow($sql);
        return $res;
    }

    public function center_market_notice()
    {

        //获取用户的平台，服务器
        $assign = $this->get_server_rights();
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $seller_id = request('seller_id', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $cur_page = empty($_GET['page']) ? 1 : request('page', 'int');

        //分页跳转的url
        $url = "action_gateway.php?ctl=new_log&act=center_market_notice&sid=$sid&gid=$gid&seller_id=$seller_id&start_time=$start_time&end_time=$end_time";

        $where = "WHERE 1 ";
        //获取平台，服务器sql
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);

        if ($seller_id) {
            $where .= " AND seller_id = $seller_id";
        }

        //日期晒选
        if ($start_time && empty($end_time)) {
            $start_time = strtotime($start_time);
            $where .= " and time>=$start_time ";

        } elseif ($start_time && $end_time) {
            $start_time = strtotime($start_time);
            $end_time = strtotime($end_time);
            $where .= " and time >= $start_time and time < $end_time ";
        } else {
            $start_time = strtotime(date("Y-m-d"));
            $end_time = $start_time + 86400;

            $where .= " and time >= $start_time and time < $end_time ";

        }

        //获取记录总数
        $per_page = 20;
        $sql = "select count(*) as total from center_market_notice $where";
        $total_record = $this->admindb->fetchOne($sql);
        $total = $total_record['total'];
        $total_page = ceil($total / $per_page);
        $start_row = ($cur_page - 1) * $per_page;

        //获取数据
        $sql = "select gid,sid,seller_id,seller_name,price,FROM_UNIXTIME(time) as time from center_market_notice $where order by time desc limit $start_row,$per_page ";
        $res = $this->admindb->fetchRow($sql);
        foreach ($res as $key => $val) {
            $res[$key]['sname'] = $s_list[$val['sid']];  //加入平台名称
            $res[$key]['gname'] = $g_list[$val['gid']];  //加入服务器名称
        }

        $assign['list'] = $res;


        $this->smarty->assign(
            array(
                "pages" => array(
                    "curpage" => $cur_page,
                    "totalpage" => $total_page,
                    "totalnumber" => $total,
                    "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
                )
            )
        );
        $this->display(CENTER_TEMP . "market_notice.shtml", $assign);
    }

}//class end