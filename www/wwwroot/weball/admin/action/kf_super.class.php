<?php
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';
ini_set('always_populate_raw_post_data',-1);
class action_kf_super extends action_superclass
{
    /**
     * 所有专服页面
     */
    public function platform(){
        global $system_config;

        $pids = Kf::get_pid();
        if(!$pids){
            alert('未配置发行ID信息');
        }
        $platform = array();
        $pid_info = array();
        foreach ($pids as $pid=>$name) {
            $key = 'rpc_kf_server_'.$pid;
            if(!$system_config[$key]){
                alert('请检查发行ID配置:'.$key);
            }
            $pid_info[] = array(
                'api'=>$system_config[$key],
                'pid'=>$pid,
                'name'=>$name,
            );
        }

        foreach ($pid_info as $v) {
            $HRpc_client = Ext_HRpc::get_cli_instance($v['api']);
            $re = $HRpc_client->platform();
            if($re){
                foreach ($re as $k=>$plat) {
                    $re[$k]['pid'] = $v['pid'];
                    $re[$k]['pname'] = $v['name'];
                }

                $platform = array_merge($platform,$re);
            }
        }

        $_platform = array();
        $i = $j = 1;
        foreach ($platform as $v) {
            $_platform[$i][] = $v;
            if($j%3==0){
                $i++;
            }
            $j++;
        }

        $kfcenter = Model::get_kf_server_super_list('node');

        $assign['kfcenter'] = $kfcenter;
        $assign['platforms'] = $_platform;
        $this->display('kf_super_plat.shtml', $assign);
    }

    /**
     * 设置专服使用指定跨服中心
     */
    public function platform_action(){
        //alert('暂时关闭');
        global $system_config;
        $gid = request('gid','int');
        $kfCenter = request('kf_center','str');
        $old_kfCenter = request('old_kf_center','str');

        if(!$gid||!$kfCenter){
            alert('选择项不能为空');
        }

        $kfCenterInfo = Config::admindb()->fetchOne("select node,cookie,pid from kf_server_super where node = '{$kfCenter}'");
        $key = 'rpc_kf_server_'.$kfCenterInfo['pid'];
        if(!$system_config[$key]){
            alert('请检查发行ID配置:'.$key);
        }

        //新的跨服中心
        $new_kfcenter = Model::get_kf_server_super_list('node,cookie',' node="'.$kfCenter.'"');
        
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config[$key]);
        $re = $HRpc_client->setPlatformKf($gid,array('node'=>$new_kfcenter[0]['node'],'cookie'=>$new_kfcenter[0]['cookie']));

        if(false == $re && !is_array($re) ){
            alert('更改失败!#1'.$re);
        }
        $callback = '';
        if(is_array($re)){
            $callback = implode(',',$re);
        }
        admin::adminLog("更改专服的跨服中心:[gid:$gid;kf:$kfCenter]".(!$callback?'成功':'失败:'.$callback));
        if($callback){
            alert("以下服操作失败,请检查错误信息!".$callback);
        }
        alert('操作成功!');
    }

    /**
     * 跨服分组列表--只显示麟游--麟游专用
     */
    public function kf_group_linyou(){
        $assign = array();

        global $system_config;
        $assign = $this->get_server_rights();

        if(!$system_config['rpc_kf_server']){
            alert('未配置:rpc_kf_server');
        }
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
        $assign['kf_list'] = $HRpc_client->getServerByPid($system_config['my_pid']);

        $act_list = Kf::get_act_wf();
        $assign['act_list'] = $act_list;
        $list = $sids = array();
        $kfCenter = request('kf_center','str');
        if($kfCenter){
            $act_id = request('act_id','int');
            $where = $act_id?"act_id = {$act_id}":'';
            $re = Kf::get_super_kf_group_list($kfCenter,$where);
            if($re){
                foreach ($re as $v) {
                    $sids[$v['group_id']][] = $v['server_id'];
                }
                foreach ($re as $v) {
                    sort($sids[$v['group_id']]);
                    $v['sids'] = '['.implode(',',$sids[$v['group_id']]).']';
                    $v['act_name'] = '['.$v['act_id'].']'.$act_list[$v['act_id']]['item'];
                    $list[] = $v;
                }
            }
        }
        $assign['list'] = $list;

        $this->display('kf_super_group_linyou.shtml', $assign);
    }

    /**
     * 跨服分组配置
     */
    public function kf_group(){
        $assign = array();
        $kf_list = Model::get_kf_server_super_list('node');
        $assign['kf_list'] = $kf_list;

        $act_list = Kf::get_act_wf();
        $assign['act_list'] = $act_list;
        $list = $sids = array();
        $kfCenter = request('kf_center','str');
        if($kfCenter){
            $act_id = request('act_id','int');
            $where = $act_id?"act_id = {$act_id}":'';
            $re = Kf::get_super_kf_group_list($kfCenter,$where);
            if($re){
                foreach ($re as $v) {
                    $sids[$v['group_id']][] = $v['server_id'];
                }
                foreach ($re as $v) {
                    sort($sids[$v['group_id']]);
                    $v['sids'] = '['.implode(',',$sids[$v['group_id']]).']';
                    $v['act_name'] = '['.$v['act_id'].']'.$act_list[$v['act_id']]['item'];
                    $list[] = $v;
                }
            }
        }
        $assign['list'] = $list;

        $this->display('kf_super_group.shtml', $assign);
    }

    /**
     * 跨服分组配置
     */
    public function kf_group_config(){
        $assign = array();

        $kf_list = array_merge( array(array('node'=>'tpl')) , Model::get_kf_server_super_list('node'));
        $assign['kf_list'] = $kf_list;

        $act_list = Kf::get_act_wf();
        $assign['act_list'] = $act_list;

        $kfCenter = request('kf_center','str');
        $act_id = request('act_id','int');
        if($kfCenter && $act_id){
            $sql = "select * from kf_super_group_config where node = '{$kfCenter}' and act_id='{$act_id}'";
            $info = Config::admindb()->fetchOne($sql);
            $info['rule'] = json_decode($info['rule'],true);
            $assign['info'] = $info;
        }
                                                                
        $this->display('kf_super_group_config.shtml', $assign);
    }

    /**
     * 提交跨服分组配置
     */
    public function kf_group_config_action(){
        $data['node']= request('kf_center','str');
        $data['act_id'] = request('act_id','int');
        $data['new_server'] = request('new_server','int');
        $data['single_server'] = request('single_server','int');
        $data['type'] = request('type','int');
        $data['stop_op'] = request('stop_op','int');

        if(!$data['node']||!$data['act_id']||!$data['new_server']||!$data['single_server']||!$data['type']){
            alert('请填写完整配置!');
        }

        if($data['type'] ==1){
            $rule = array(
                '1s'=>request('1s','int'),
                '1e'=>request('1e','int'),
                '1n'=>request('1n','int'),
                '2s'=>request('2s','int'),
                '2e'=>request('2e','int'),
                '2n'=>request('2n','int'),
                '3s'=>request('3s','int'),
                '3e'=>request('3e','int'),
                '3n'=>request('3n','int'),
                '4s'=>request('4s','int'),
                '4n'=>request('4n','int'),
            );
            $data['open_days'] = request('1s','int');
            $data['group_num'] = request('1n','int');
        }elseif($data['type']== 2){
            $rule = array(
                'p1s'=>request('p1s','int'),
                'p1n'=>request('p1n','int'),
            );
            $data['open_days'] = request('p1s','int');
            $data['group_num'] = request('p1n','int');
        }elseif($data['type'] == 3){
            $rule = array(
                'f1s'=>request('f1s','int'),
            );
            $data['open_days'] = 1;
            $data['group_num'] = request('f1s');
        }elseif($data['type']== 4){
            //按战力（新）
            $rule = array(
                'f1s_new'=>request('f1s_new','int'),
            );
            $data['open_days'] = 0;
            $data['group_num'] = request('f1s_new','int');
        }

        if($data['type']==1 && (!$rule['1s'] || !$rule['1n'])){
            alert('请把分组规则填写完整');
        }
        if($data['type']==2 && (!$rule['p1s'] || !$rule['p1n'])){
            alert('请把分组规则填写完整');
        }
        //按战力（新）
        if($data['type']==4 && !$rule['f1s_new'] ){
            alert('请把分组规则填写完整');
        }

        $data['rule'] = json_encode($rule);

        $sql = "select * from kf_super_group_config where node = '{$data['node']}' and act_id='{$data['act_id']}'";
        $re = Config::admindb()->fetchOne($sql);
        if($re['rule'] && md5($re['rule']) != md5($data['rule'])){
            //检测 更改分组服数时必须要执行重新分组
            //alert('更改分组服数时必须要执行重新分组');
        }


        $re = Config::admindb()->insertOrUpdate('kf_super_group_config',$data);
        if($re){
            if(request('run_group')){
                $ste = Kf::dispatch_group($data['node'],$data);
                Admin::adminLog('提交超级跨服中心分组配置并进行分组操作:'.implode('|',$data));
                if(empty($ste[0])||$ste[0]!==true){
                    alert($ste[1]);
                }
                alert('完成分组操作!请查看分组列表!');
            }
            Admin::adminLog('提交超级跨服中心分组配置:'.implode('|',$data));
            alert('提交成功');
        }else{
            alert('提交失败');
        }
    }

    /**
     * 跨服中心
     */
    public function kf_server_list()
    {
        $assign = $this->get_server_rights();

        $where = ' where 1';

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from kf_server_super $where ");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from kf_server_super $where order by pid $limit";
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );

        $assign['pids'] = Kf::get_pid();

        $this->display('kf_super.shtml', $assign);
    }

    public function kf_action()
    {
        Auth::check_action(Auth::$KF_ADD);
        $data['node'] = request('node', 'str');
        $data['cookie'] = request('cookie', 'str');
        $data['db_host'] = request('db_host', 'str');
        $data['db_name'] = request('db_name', 'str');
        $data['db_pwd'] = request('db_pwd', 'str');
        $data['db_port'] = request('db_port', 'str');
        $data['db_user'] = request('db_user', 'str');
        $data['pid'] = request('pid', 'int');

        if (!$data['node']) {
            alert('节点名必须');
        }

        $this->admindb->insert('kf_server_super', $data, true);
        Admin::adminLog('编辑跨服中心:' . json_encode($data));
        alert('已提交');
    }

    /**
     * 删除跨服中心
     */
    public function del()
    {
        Auth::check_action(Auth::$KF_DEL);
        $node = request('node', 'str');
        if (!$node) {
            alert('缺少参数');
        }

        $sql = "select * from kf_server_super where node = '{$node}'";
        $re = $this->admindb->fetchOne($sql);
        if (!$re) {
            alert('找不到此跨服!');
        }
        $sql = "delete from kf_server_super where node = '{$node}'";
        $this->admindb->query($sql);
        Admin::adminLog('删除跨服:' . $node);
        alert('删除成功!');

    }

    /**
     * 刷新分组
     */
    public function kf_group_flash()
    {
        alert('关闭');
        Auth::check_action(Auth::$KF_GROUP_GM);

        $kfCenter = request('kf_center','str');
        if(!$kfCenter){
            alert('请选择一个跨服中心');
        }
        $res = Erlang::erl_flash_kf($kfCenter);

        Admin::adminLog("刷新跨服分组GM:平台($kfCenter)");
        alert($res);
    }
    /**
     * 刷新分组
     */
    public function kf_group_flash_shenlong()
    {
        alert('关闭');
        Auth::check_action(Auth::$KF_GROUP_GM);

        $kfCenter = request('kf_center','str');
        if(!$kfCenter){
            alert('请选择一个跨服中心');
        }
        $res = Erlang::erl_refresh_world_server_boss($kfCenter);

        Admin::adminLog("刷新跨服分组GM:平台($kfCenter) [神龙岛]");
        alert(is_array($res)?implode('|',$res):$res);
    }

    /**
     * 专服玩法不齐，补上模版玩法
     */
    public function add_tpl($list){
        $sql = "select * from kf_super_group_config where new_server = 1 and node ='tpl' ";
        $tpl_res = $this->admindb->fetchRow($sql);
        $tpl_list = array();
        foreach ($tpl_res as $key => $val) {
            $tpl_list[$val['act_id']] = $val;
        }

        $data = array();
        foreach ($list as $key => $val) {
            $data[$val['node']][$val['act_id']] = $val;
        }

        foreach ($data as $key => $val) {
            foreach (array_diff_key($tpl_list, $val) as $k => $v) {
                $data[$key][$k] = $v;
            }
        }

        $list = array();
        foreach ($data as $key => $val) {
            foreach ($val as $kk => $vv) {
                $vv['node'] = $key;
                $list[] = $vv;
            }
        }

        return $list;
    }


    /**
     * 玩法类 停服维护时操作分组
     */
    public function dispatch_group_when_on_stop_server(){
        $sql = "select * from kf_super_group_config where new_server = 2 and node !='tpl' ";
        $list = $this->admindb->fetchRow($sql);
       
        $list = $this->add_tpl($list);
     // print_r($list);exit;
        if($list){
            $str = '';

            foreach ($list as $v) {
                if(!$v['node']||!$v['act_id']||!$v['type']||!$v['open_days']||!$v['new_server']||!$v['group_num']){
                    continue;
                }
                $ste = Kf::dispatch_group($v['node'],$v);
                if( empty($ste[0]) || $ste[0]!==true ){
                    $str .= $v['node'].' | '.$v['act_id'].' | '.(empty($ste[1])?'error':$ste[1]).'<br/>' ;
                }
            }
            if( $str=='' ){
                alert( $str );
            }
        }else{
            alert( "没有活动可以分组!" );
        }
        alert('分组完成!');
    }

    /**
     * 玩法类 手动一服一组
     */
    public function dispatch_group_single(){
        $allKfCenter = Model::get_kf_server_super_list();
   
        $activity = Kf::get_act_wf();
        if($activity && $allKfCenter){
            foreach ($allKfCenter as $v) {
                foreach ($activity as $act) {
                    $sql = "select `single_server` from kf_super_group_config where `node` = '{$v['node']}' and act_id='{$act['id']}'";
                    $re = Config::admindb()->fetchOne($sql);
                    //如果设置了 不自动一服一组,则忽略
                    //print($v['node'].','.$act['id'].','.$re['single_server']."\n");
                    if($re['single_server'] && $re['single_server'] == 2){
                        continue;
                    }
                    Kf::dispatch_group_single_cron($v['node'],$act['id']);
                }
            }
        }
        alert('分组完成');
    }

}