<?php
/**
 * SVN
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_svn extends action_superclass
{
	public function ajax_svn_up(){
		$dir = scandir(SERVER_DATA_PATH);
		$list = array();
		foreach ($dir as $key => $val) {
			if($val == '.' || $val == '..' || $val == '.svn') continue;
			if(strpos($val, '.xml')) continue;
			$list[] = $val;
		}
		$count = count($list);
		echo json_encode(array('count'=>$count,'list'=>$list));
		exit;
	}

	public function ajax_svn_commit(){
		$file = $_POST['file'];
		system("/usr/bin/svn add ".SERVER_DATA_PATH.$file,$reval);
		$res = system("/usr/bin/svn commit ".SERVER_DATA_PATH.$file.' -m \"提交生成配置\"',$reval);
		echo $res.'<br>';
		exit;
	}

	public function svn_commit_hrl(){
		/*system("/usr/bin/svn add ".SERVER_DATA_PATH.'*.erl',$reval);
		$res = system("/usr/bin/svn ci ".SERVER_DATA_PATH."*.erl -m \"提交生成配置\"",$reval);
		echo $res.'<br>'.$reval;
		exit;*/
		//新
		$version =  svn_update(SERVER_HEADER_PATH);
		echo '更新版本：'.$version.'<br>';
		echo '<pre>';
		$list = svn_status(SERVER_HEADER_PATH);
		// print_r($list);
		echo '</pre>';
		echo '<br>';
		if(!empty($list)){
			foreach ($list as $key => $val) {
				if($val['text_status'] == 2){
					echo $val['path'].' Add '.var_dump(svn_add($val['path'])).'<br>';
				}elseif($val['text_status'] == 8){
					echo $val['path'].' Edit <br>';
				}
			}
		}
		echo '<br>';

		$res = svn_commit('提交生成配置', array(SERVER_HEADER_PATH));
		print_r($res);
		if($res[0] == -1){
			echo '无内容提交';
		}else{
			echo '提交版本号：'.$res[0];
		}
	}

	public function svn_commit_erl(){
		/*system("/usr/bin/svn add ".SERVER_DATA_PATH.'*.erl',$reval);
		$res = system("/usr/bin/svn ci ".SERVER_DATA_PATH."*.erl -m \"提交生成配置\"",$reval);
		echo $res.'<br>'.$reval;
		exit;*/
		//新
		$dir = dirname(SERVER_DATA_PATH).'/';
	    $version =  svn_update(SERVER_DATA_PATH);
	    echo '更新版本：'.$version.'<br>';
	    echo '<pre>';
	    $list = svn_status(SERVER_DATA_PATH);
	    // print_r($list);
	    echo '</pre>';
	    echo '<br>';
	    if(!empty($list)){
	        foreach ($list as $key => $val) {
	            if($val['text_status'] == 2){
	               echo $val['path'].' Add '.var_dump(svn_add($val['path'])).'<br>';
	            }elseif($val['text_status'] == 8){
	               echo $val['path'].' Edit <br>';
	            }
	        }
	    }
	    echo '<br>';
	    
	    $res = svn_commit('提交生成配置', array(SERVER_DATA_PATH));
	    print_r($res);
	    if($res[0] == -1){
	        echo '无内容提交';
	    }else{
	        echo '提交版本号：'.$res[0];
	    }
	}

	public function svn_commit_xml(){
		//提交
		/*system("/usr/bin/svn add ".XML_DATA_DIR.'*.xml',$reval);
		$res = system("/usr/bin/svn ci ".XML_DATA_DIR."*.xml -m \"提交各个xml生成配置\"",$reval);
		echo $res.'<br>'.$reval;
		exit;*/
		$dir = dirname(XML_DATA_DIR).'/';
	    $version =  svn_update(XML_DATA_DIR);
	    echo '更新版本：'.$version.'<br>';
	    echo '<pre>';
	    $list = svn_status(XML_DATA_DIR);
	    //print_r($list);
	    echo '</pre>';
	    echo '<br>';
	    if(!empty($list)){
	        foreach ($list as $key => $val) {
	            if($val['text_status'] == 2){
	               echo $val['path'].' Add '.var_dump(svn_add($val['path'])).'<br>';
	            }elseif($val['text_status'] == 8){
	               echo $val['path'].' Edit <br>';
	            }
	        }
	    }
	    echo '<br>';
	    
	    $res = svn_commit('提交各个xml生成配置', array(XML_DATA_DIR));
	    print_r($res);
	    if($res[0] == -1){
	        echo '无内容提交';
	    }else{
	        echo '提交版本号：'.$res[0];
	    }
	}

	public function svn_commit_lua(){
		//提交
		/*system("/usr/bin/svn add ".XML_DATA_DIR.'*.xml',$reval);
		$res = system("/usr/bin/svn ci ".XML_DATA_DIR."*.xml -m \"提交各个xml生成配置\"",$reval);
		echo $res.'<br>'.$reval;
		exit;*/
		$dir = dirname(CLIENT_DATA_PATH).'/';
	    $version =  svn_update(CLIENT_DATA_PATH);
	    echo '更新版本：'.$version.'<br>';
	    echo '<pre>';
	    $list = svn_status(CLIENT_DATA_PATH);
	    //print_r($list);
	    echo '</pre>';
	    echo '<br>';
	    if(!empty($list)){
	        foreach ($list as $key => $val) {
	            if($val['text_status'] == 2){
	               echo $val['path'].' Add '.var_dump(svn_add($val['path'])).'<br>';
	            }elseif($val['text_status'] == 8){
	               echo $val['path'].' Edit <br>';
	            }
	        }
	    }
	    echo '<br>';
	    $res = svn_commit('提交各个lua生成配置', array(CLIENT_DATA_PATH));
	    print_r($res);
	    if($res[0] == -1){
	        echo '无内容提交';
	    }else{
	        echo '提交版本号：'.$res[0];
	    }
	}

	public function ajax_svn_commit_all(){
		$stype = $_POST['stype'];
		if($stype == 'erl'){
			$this->svn_commit_erl();
		    exit;
		    
		}elseif($stype == 'lua'){//提交LUA场景
			system("/usr/bin/svn add ".LUA_CONFIG_DIR.'config_scene.lua',$reval);
			$res = system("/usr/bin/svn ci ".LUA_CONFIG_DIR."config_scene.lua -m \"提交LUA场景生成配置\"",$reval);
			echo $res.'<br>'.$reval.'<br>';
			//提交各个场景LUA
			system("/usr/bin/svn add ".LUA_SCRIPT_DIR.'*.lua',$reval);
			$res = system("/usr/bin/svn ci ".LUA_SCRIPT_DIR."*.lua -m \"提交各个LUA场景生成配置\"",$reval);
			echo $res.'<br>'.$reval;
			exit;
		}elseif($stype == 'sqlite'){//提交Sqlite
			//提交
			system("/usr/bin/svn add ".LUA_SQLITE_DIR.'*.db',$reval);
			$res = system("/usr/bin/svn ci ".LUA_SQLITE_DIR."*.db -m \"提交各个Sqlite生成配置\"",$reval);
			echo $res.'<br>'.$reval;
			exit;
		}elseif($stype == 'xml'){//提交xml
			$this->svn_commit_xml();
		    exit;
		}
		
	}
	
	
	
}