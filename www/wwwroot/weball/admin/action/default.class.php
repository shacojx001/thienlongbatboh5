<?php
/**
 * 默认首页
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_default extends action_superclass
{
    public function index()
    {
        $this->smarty->assign("phptime", date("D M j G:i:s T Y"));
        $this->display('default.shtml',array());
    }
}