<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 17/11/21
 * Time: 下午5:04
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_center_data extends action_superclass
{
    /**
     * 汇总分析
     */
    public function center_total()
    {
        $sd = $_POST['sd'] ? $_POST['sd'] : $_GET['sd'];
        $ed = $_POST['ed'] ? $_POST['ed'] : $_GET['ed'];

        if (!$sd) $sd = date('Y-m') . '-01';
        if (!$ed) $ed = date('Y-m-d');
        $st = strtotime($sd);
        $et = strtotime($ed);

        //日期数组
        $date = $this->to_date_arr($st, $et);
        $tmp = $data = array();
        //汇总数组
        $t = array('T_DAU_R' => 0, 'T_DAO_R' => 0, 'T_DNU_R' => 0, 'T_DAU' => 0, 'T_DAO' => 0, 'T_DNV' => 0, 'T_DNU' => 0,
            'T_DPU' => 0, 'T_DPU_R' => 0, 'T_DPUS' => 0, 'T_DPT' => 0, 'T_DNPU' => 0, 'T_DNPT' => 0, 'T_DNPU_R' => 0, 'T_DNPT_R' => 0,
            'T_DPU_DAU' => 0, 'T_DPT_DAU' => 0, 'T_DPT_DPU' => 0, 'T_DNPT_DNU' => 0, 'T_DNPT_DNPU' => 0, 'T_ONLINE' => 0,
        );
        $sql = "select FROM_UNIXTIME(`time`, '%Y-%m-%d') as ymd,a.* from center_total a where `time`>={$st} and `time`<={$et} order by `time` desc";
        $result = $this->admindb->fetchRow($sql);
        if ($result) {
            //获取最高在线 注意: 所有服最高在线相加
            $online = array();
            $sql = "SELECT FROM_UNIXTIME(`time`, '%Y-%m-%d') as ymd,sum(`online`) as s FROM `center_total_online` where `time`>={$st} and `time`<={$et} group by `time`;";
            $re = $this->admindb->fetchRow($sql);
            if ($re)
                foreach ($re as $v) {
                    $online[$v['ymd']] = $v['s'];
                }
            foreach ($result as $v) {
                if ($v['DPT']) $v['DPT'] = $v['DPT'] / 100;
                if ($v['DNPT']) $v['DNPT'] = $v['DNPT'] / 100;
                if ($v['DNPT_R']) $v['DNPT_R'] = $v['DNPT_R'] / 100;
                $v['DPU_DAU'] = 0;//登陆付费率
                if ($v['DPU'] && $v['DAU']) $v['DPU_DAU'] = (round($v['DPU'] / $v['DAU'], 4) * 100) . '%';
                $v['DPT_DAU'] = 0;//ARPU
                if ($v['DPT'] && $v['DAU']) $v['DPT_DAU'] = round($v['DPT'] / $v['DAU'], 2);
                $v['DPT_DPU'] = 0;//ARPPU
                if ($v['DPT'] && $v['DPU']) $v['DPT_DPU'] = round($v['DPT'] / $v['DPU'], 2);
                $v['DNPT_DNU'] = 0;//新增ARPU
                if ($v['DNPT'] && $v['DNU']) $v['DNPT_DNU'] = round($v['DNPT'] / $v['DNU'], 2);
                $v['DNPT_DNPU'] = 0;//新增ARPPU
                if ($v['DNPT'] && $v['DNPU']) $v['DNPT_DNPU'] = round($v['DNPT'] / $v['DNPU'], 2);
                //汇总
                $t['T_DAU_R'] += $v['DAU_R'];
                $t['T_DAO_R'] += $v['DAO_R'];
                $t['T_DNU_R'] += $v['DNU_R'];
                $t['T_DAU'] += $v['DAU'];
                $t['T_DAO'] += $v['DAO'];
                $t['T_DNV'] += $v['DNV'];
                $t['T_DNU'] += $v['DNU'];
                $t['T_DPU'] += $v['DPU'];
                $t['T_DPU_R'] += $v['DPU_R'];
                $t['T_DPUS'] += $v['DPUS'];
                $t['T_DPT'] += $v['DPT'];
                $t['T_DNPU'] += $v['DNPU'];
                $t['T_DNPT'] += $v['DNPT'];
                $t['T_DNPU_R'] += $v['DNPU_R'];
                $t['T_DNPT_R'] += $v['DNPT_R'];

                $tmp[$v['ymd']] = $v;
            }
            //汇总比率
            if ($t['T_DPU'] && $t['T_DAU']) $t['T_DPU_DAU'] = (round($t['T_DPU'] / $t['T_DAU'], 4) * 100) . '%';
            if ($t['T_DPT'] && $t['T_DAU']) $t['T_DPT_DAU'] = round($t['T_DPT'] / $t['T_DAU'], 2);
            if ($t['T_DPT'] && $t['T_DPU']) $t['T_DPT_DPU'] = round($t['T_DPT'] / $t['T_DPU'], 2);
            if ($t['T_DNPT'] && $t['T_DNU']) $t['T_DNPT_DNU'] = round($t['T_DNPT'] / $t['T_DNU'], 2);
            if ($t['T_DNPT'] && $t['T_DNPU']) $t['T_DNPT_DNPU'] = round($t['T_DNPT'] / $t['T_DNPU'], 2);

            foreach ($date as $day) {
                if (!$tmp[$day]) {
                    $data[$day] = array(
                        'ymd' => $day,
                        'DAU_R' => 0, 'DAO_R' => 0, 'DNU_R' => 0,
                        'DAU' => 0, 'DAO' => 0, 'DNV' => 0, 'DNU' => 0,
                        'DPU' => 0, 'DPU_R' => 0, 'DPUS' => 0, 'DPT' => 0,
                        'DNPU' => 0, 'DNPT' => 0, 'DNPU_R' => 0, 'DNPT_R' => 0,
                        'DPU_DAU' => 0,//登陆付费率
                        'DPT_DAU' => 0,//ARPU
                        'DPT_DPU' => 0,//ARPPU
                        'DNPT_DNU' => 0,//新增ARPU
                        'DNPT_DNPU' => 0,//新增ARPPU
                        'ONLINE' => $online[$day] ? $online[$day] : 0,
                    );
                } else {
                    $tmp[$day]['ONLINE'] = $online[$day] ? $online[$day] : 0;
                    $data[$day] = $tmp[$day];
                }
                $t['T_ONLINE'] += $data[$day]['ONLINE'];
            }
            //按日期倒序
            krsort($data);
        }

        //前一个月
        $last_sd = date("Y-m-01", strtotime("-1 months", strtotime($sd)));
        $last_ed = date("Y-m-d", strtotime("+1 months", strtotime($last_sd)) - 86400);

        //后一个月
        $next_sd = date("Y-m-01", strtotime("+1 months", strtotime($sd)));
        $next_ed = date("Y-m-d", strtotime("+1 months", strtotime($next_sd)) - 86400);

        $assign['last_ed'] = $last_ed;
        $assign['last_sd'] = $last_sd;
        $assign['next_sd'] = $next_sd;
        $assign['next_ed'] = $next_ed;
        $assign['data'] = $data;
        $assign['t'] = $t;
        $assign['sd'] = $sd;
        $assign['ed'] = $ed;
        $this->display('center_total.shtml', $assign);
    }

    /**
     * 分渠道汇总
     */
    public function center_total_channel()
    {
        $channel = request('channel', 'str');
        $channel_list = $this->get_channel_list();
        $where = $this->get_where_c($channel, $channel_list, 'channel');
        $sd = $_POST['sd'] ? $_POST['sd'] : $_GET['sd'];
        $ed = $_POST['ed'] ? $_POST['ed'] : $_GET['ed'];
        if (!$sd) {
            $sd = date('Y-m') . '-01';
        }
        if (!$ed) {
            $ed = date('Y-m-d');
        }
        $st = strtotime($sd);
        $et = strtotime($ed);

        //汇总数组
        $t = array('T_DAU_R' => 0, 'T_DAO_R' => 0, 'T_DNU_R' => 0, 'T_DAU' => 0, 'T_DAO' => 0, 'T_DNV' => 0, 'T_DNU' => 0,
            'T_DPU' => 0, 'T_DPU_R' => 0, 'T_DPUS' => 0, 'T_DPT' => 0, 'T_DNPU' => 0, 'T_DNPT' => 0, 'T_DNPU_R' => 0, 'T_DNPT_R' => 0,
            'T_DPU_DAU' => 0, 'T_DPT_DAU' => 0, 'T_DPT_DPU' => 0, 'T_DNPT_DNU' => 0, 'T_DNPT_DNPU' => 0, 'T_ONLINE' => 0,
        );
        //日期数组
        $date = $this->to_date_arr($st, $et);
        $data = $data = array();
        $sql = "select FROM_UNIXTIME(`time`, '%Y-%m-%d') as ymd,a.* from center_total_channel a where `time`>={$st} and `time`<={$et} $where order by `time` desc,channel asc";
        $result = $this->admindb->fetchRow($sql);
        if ($result) {
            //获取最高在线
            $online = array();
            $sql = "SELECT FROM_UNIXTIME(`time`, '%Y-%m-%d') as ymd,sum(`online`) as online,channel FROM `center_total_online_channel` where `time`>={$st} and `time`<={$et} $where group by `time`,channel;";
            $re = $this->admindb->fetchRow($sql);
            if ($re) {
                foreach ($re as $v) {
                    $online[$v['ymd']][$v['channel']] = $v['online'];
                }
            }
            $tmp = array();
            foreach ($result as $v) {
                $tmp['channel'][] = $v['channel'];
                $tmp['ymd'][] = $v['ymd'];
            }
            array_multisort($tmp['ymd'], SORT_DESC, $tmp['channel'], SORT_ASC, $result);
            foreach ($result as $k => $v) {
                if ($v['DPT']) $v['DPT'] = $v['DPT'] / 100;
                if ($v['DNPT']) $v['DNPT'] = $v['DNPT'] / 100;
                if ($v['DNPT_R']) $v['DNPT_R'] = $v['DNPT_R'] / 100;
                $v['DPU_DAU'] = 0;//登陆付费率
                if ($v['DPU'] && $v['DAU']) $v['DPU_DAU'] = (round($v['DPU'] / $v['DAU'], 4) * 100) . '%';
                $v['DPT_DAU'] = 0;//ARPU
                if ($v['DPT'] && $v['DAU']) $v['DPT_DAU'] = round($v['DPT'] / $v['DAU'], 2);
                $v['DPT_DPU'] = 0;//ARPPU
                if ($v['DPT'] && $v['DPU']) $v['DPT_DPU'] = round($v['DPT'] / $v['DPU'], 2);
                $v['DNPT_DNU'] = 0;//新增ARPU
                if ($v['DNPT'] && $v['DNU']) $v['DNPT_DNU'] = round($v['DNPT'] / $v['DNU'], 2);
                $v['DNPT_DNPU'] = 0;//新增ARPPU
                if ($v['DNPT'] && $v['DNPU']) $v['DNPT_DNPU'] = round($v['DNPT'] / $v['DNPU'], 2);
                $data[$v['ymd']][$v['channel']] = $v;

                //汇总
                $t['T_DAU_R'] += $v['DAU_R'];
                $t['T_DAO_R'] += $v['DAO_R'];
                $t['T_DNU_R'] += $v['DNU_R'];
                $t['T_DAU'] += $v['DAU'];
                $t['T_DAO'] += $v['DAO'];
                $t['T_DNV'] += $v['DNV'];
                $t['T_DNU'] += $v['DNU'];
                $t['T_DPU'] += $v['DPU'];
                $t['T_DPU_R'] += $v['DPU_R'];
                $t['T_DPUS'] += $v['DPUS'];
                $t['T_DPT'] += $v['DPT'];
                $t['T_DNPU'] += $v['DNPU'];
                $t['T_DNPT'] += $v['DNPT'];
                $t['T_DNPU_R'] += $v['DNPU_R'];
                $t['T_DNPT_R'] += $v['DNPT_R'];
            }
            //汇总比率
            if ($t['T_DPU'] && $t['T_DAU']) $t['T_DPU_DAU'] = (round($t['T_DPU'] / $t['T_DAU'], 4) * 100) . '%';
            if ($t['T_DPT'] && $t['T_DAU']) $t['T_DPT_DAU'] = round($t['T_DPT'] / $t['T_DAU'], 2);
            if ($t['T_DPT'] && $t['T_DPU']) $t['T_DPT_DPU'] = round($t['T_DPT'] / $t['T_DPU'], 2);
            if ($t['T_DNPT'] && $t['T_DNU']) $t['T_DNPT_DNU'] = round($t['T_DNPT'] / $t['T_DNU'], 2);
            if ($t['T_DNPT'] && $t['T_DNPU']) $t['T_DNPT_DNPU'] = round($t['T_DNPT'] / $t['T_DNPU'], 2);
            foreach ($date as $day) {
                if ($data[$day]) {
                    foreach ($data[$day] as $_channel => $cv) {
                        $data[$day][$_channel]['ONLINE'] = $online[$day][$_channel] ? $online[$day][$_channel] : 0;
                        $t['T_ONLINE'] += $data[$day][$_channel]['ONLINE'];
                    }
                }
            }
        }
        //前一个月
        $last_sd = date("Y-m-01", strtotime("-1 months", strtotime($sd)));
        $last_ed = date("Y-m-d", strtotime("+1 months", strtotime($last_sd)) - 86400);

        //后一个月
        $next_sd = date("Y-m-01", strtotime("+1 months", strtotime($sd)));
        $next_ed = date("Y-m-d", strtotime("+1 months", strtotime($next_sd)) - 86400);

        $assign['channel_list'] = $channel_list;
        $assign['channel'] = $channel;
        $assign['last_ed'] = $last_ed;
        $assign['last_sd'] = $last_sd;
        $assign['next_sd'] = $next_sd;
        $assign['next_ed'] = $next_ed;
        $assign['data'] = $data;
        $assign['t'] = $t;
        $assign['sd'] = $sd;
        $assign['ed'] = $ed;
        $this->display('center_total_channel.shtml', $assign);
    }

    /**
     * 分专服汇总
     */
    public function center_total_gid()
    {
        $gids = $this->get_allgroup_list();

        $gid = request('gid', 'int');
        $where = $this->get_where_g($gid, $gids);

        $sd = $_POST['sd'] ? $_POST['sd'] : $_GET['sd'];
        $ed = $_POST['ed'] ? $_POST['ed'] : $_GET['ed'];
        if (!$sd) {
            $sd = date('Y-m') . '-01';
        }
        if (!$ed) {
            $ed = date('Y-m-d');
        }
        $st = strtotime($sd);
        $et = strtotime($ed);

        //汇总数组
        $t = array('T_DAU_R' => 0, 'T_DAO_R' => 0, 'T_DNU_R' => 0, 'T_DAU' => 0, 'T_DAO' => 0, 'T_DNV' => 0, 'T_DNU' => 0,
            'T_DPU' => 0, 'T_DPU_R' => 0, 'T_DPUS' => 0, 'T_DPT' => 0, 'T_DNPU' => 0, 'T_DNPT' => 0, 'T_DNPU_R' => 0, 'T_DNPT_R' => 0,
            'T_DPU_DAU' => 0, 'T_DPT_DAU' => 0, 'T_DPT_DPU' => 0, 'T_DNPT_DNU' => 0, 'T_DNPT_DNPU' => 0, 'T_ONLINE' => 0,
        );
        //日期数组
        $date = $this->to_date_arr($st, $et);
        $data = $data = array();
        $sql = "select FROM_UNIXTIME(`time`, '%Y-%m-%d') as ymd,a.* from center_total_gid a where `time`>={$st} and `time`<={$et} $where order by `time` desc,gid asc";
        $result = $this->admindb->fetchRow($sql);
        if ($result) {
            //获取最高在线
            $online = array();
            $sql = "SELECT FROM_UNIXTIME(`time`, '%Y-%m-%d') as ymd,sum(`online`) as online,gid FROM `center_total_online_channel` where `time`>={$st} and `time`<={$et} $where group by `time`,gid;";
            $re = $this->admindb->fetchRow($sql);
            if ($re) {
                foreach ($re as $v) {
                    $online[$v['ymd']][$v['gid']] = $v['online'];
                }
            }
            $tmp = array();
            foreach ($result as $v) {
                $tmp['gid'][] = $v['gid'];
                $tmp['ymd'][] = $v['ymd'];
            }
            array_multisort($tmp['ymd'], SORT_DESC, $tmp['channel'], SORT_ASC, $result);
            foreach ($result as $v) {
                if ($v['DPT']) $v['DPT'] = $v['DPT'] / 100;
                if ($v['DNPT']) $v['DNPT'] = $v['DNPT'] / 100;
                if ($v['DNPT_R']) $v['DNPT_R'] = $v['DNPT_R'] / 100;
                $v['DPU_DAU'] = 0;//登陆付费率
                if ($v['DPU'] && $v['DAU']) $v['DPU_DAU'] = (round($v['DPU'] / $v['DAU'], 4) * 100) . '%';
                $v['DPT_DAU'] = 0;//ARPU
                if ($v['DPT'] && $v['DAU']) $v['DPT_DAU'] = round($v['DPT'] / $v['DAU'], 2);
                $v['DPT_DPU'] = 0;//ARPPU
                if ($v['DPT'] && $v['DPU']) $v['DPT_DPU'] = round($v['DPT'] / $v['DPU'], 2);
                $v['DNPT_DNU'] = 0;//新增ARPU
                if ($v['DNPT'] && $v['DNU']) $v['DNPT_DNU'] = round($v['DNPT'] / $v['DNU'], 2);
                $v['DNPT_DNPU'] = 0;//新增ARPPU
                if ($v['DNPT'] && $v['DNPU']) $v['DNPT_DNPU'] = round($v['DNPT'] / $v['DNPU'], 2);
                $data[$v['ymd']][$v['gid']] = $v;

                //汇总
                $t['T_DAU_R'] += $v['DAU_R'];
                $t['T_DAO_R'] += $v['DAO_R'];
                $t['T_DNU_R'] += $v['DNU_R'];
                $t['T_DAU'] += $v['DAU'];
                $t['T_DAO'] += $v['DAO'];
                $t['T_DNV'] += $v['DNV'];
                $t['T_DNU'] += $v['DNU'];
                $t['T_DPU'] += $v['DPU'];
                $t['T_DPU_R'] += $v['DPU_R'];
                $t['T_DPUS'] += $v['DPUS'];
                $t['T_DPT'] += $v['DPT'];
                $t['T_DNPU'] += $v['DNPU'];
                $t['T_DNPT'] += $v['DNPT'];
                $t['T_DNPU_R'] += $v['DNPU_R'];
                $t['T_DNPT_R'] += $v['DNPT_R'];

            }
            //汇总比率
            if ($t['T_DPU'] && $t['T_DAU']) $t['T_DPU_DAU'] = (round($t['T_DPU'] / $t['T_DAU'], 4) * 100) . '%';
            if ($t['T_DPT'] && $t['T_DAU']) $t['T_DPT_DAU'] = round($t['T_DPT'] / $t['T_DAU'], 2);
            if ($t['T_DPT'] && $t['T_DPU']) $t['T_DPT_DPU'] = round($t['T_DPT'] / $t['T_DPU'], 2);
            if ($t['T_DNPT'] && $t['T_DNU']) $t['T_DNPT_DNU'] = round($t['T_DNPT'] / $t['T_DNU'], 2);
            if ($t['T_DNPT'] && $t['T_DNPU']) $t['T_DNPT_DNPU'] = round($t['T_DNPT'] / $t['T_DNPU'], 2);
            foreach ($date as $day) {
                if ($data[$day]) {
                    foreach ($data[$day] as $_gid => $cv) {
                        $data[$day][$_gid]['ONLINE'] = $online[$day][$_gid] ? $online[$day][$_gid] : 0;
                        $t['T_ONLINE'] += $data[$day][$_gid]['ONLINE'];
                    }
                }
            }
        }
        //前一个月
        $last_sd = date("Y-m-01", strtotime("-1 months", strtotime($sd)));
        $last_ed = date("Y-m-d", strtotime("+1 months", strtotime($last_sd)) - 86400);

        //后一个月
        $next_sd = date("Y-m-01", strtotime("+1 months", strtotime($sd)));
        $next_ed = date("Y-m-d", strtotime("+1 months", strtotime($next_sd)) - 86400);

        $assign['gids'] = $gids;
        $assign['gid'] = $gid;

        $assign['last_ed'] = $last_ed;
        $assign['last_sd'] = $last_sd;
        $assign['next_sd'] = $next_sd;
        $assign['next_ed'] = $next_ed;
        $assign['data'] = $data;
        $assign['t'] = $t;
        $assign['sd'] = $sd;
        $assign['ed'] = $ed;
        $this->display('center_total_gid.shtml', $assign);
    }

    /**
     * 返回日期数组
     * @param $st
     * @param $et
     * @return array
     */
    public function to_date_arr($st, $et)
    {
        $data = array();
        for ($i = $st; $i <= $et; $i += 86400) {
            $data[] = date('Y-m-d', $i);
        }
        return $data;
    }

    /**
     * 等级流失率
     */
    function level_rate()
    {
        //获取服务器列表权限
        $assign = $this->get_server_rights();

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $date = request('time', 'str');
        $where = "where 1";
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);

        if ($date) {
            $time = strtotime($date);
        } else {
            $time_str = date('Y-m-d');
            $time = strtotime($time_str) - 86400;
            $_GET['time'] = date('Y-m-d', $time);
        }
        $where .= " AND time = " . $time;

        $sql = "SELECT
                sum(total) AS total
              FROM
                center_role_level_retain
              $where
              GROUP BY
                time
                 ";
        $res = $this->admindb->fetchRow($sql);
        $total = 0;
        foreach ($res as $key => $val) {
            $total += $val['total'];
        }
        $total_list['all'] = $total;
        $sql = "SELECT
                time,
                sum(total) as total,
                sum(one_not_login) as one_not_login,
                sum(three_not_login) as three_not_login,
                sum(seven_not_login) as seven_not_login,
                level_range
              FROM
                center_role_level_retain
              $where
              GROUP BY
              level_range
                 ";
        $res = $this->admindb->fetchRow($sql);
        $list = array();
        if (!empty($res)) {
            foreach ($res as $key => $val) {
                $list[$val['level_range']]['name'] = $val['level_range'];
                $list[$val['level_range']]['total'] = $val['total'];
                $list[$val['level_range']]['total_rate'] = round($val['total'] / $total * 100, 2) . '%';
                $list[$val['level_range']]['one_not_login'] = $val['one_not_login'];
                $list[$val['level_range']]['one_total_rate'] = round($val['one_not_login'] / $val['total'] * 100, 2) . '%';
                $list[$val['level_range']]['three_not_login'] = $val['three_not_login'];
                $list[$val['level_range']]['three_total_rate'] = round($val['three_not_login'] / $val['total'] * 100, 2) . '%';
                $list[$val['level_range']]['seven_not_login'] = $val['seven_not_login'];
                $list[$val['level_range']]['seven_total_rate'] = round($val['seven_not_login'] / $val['total'] * 100, 2) . '%';
                $total_list['one'] += $val['one_not_login'];
                $total_list['three'] += $val['three_not_login'];
                $total_list['seven'] += $val['seven_not_login'];
            }
        }
//        Ext_Debug::log($list);
        $total_list['one_rate'] = $total ? round($total_list['one'] / $total * 100, 2) : 0;
        $total_list['three_rate'] = $total ? round($total_list['three'] / $total * 100, 2) : 0;
        $total_list['seven_rate'] = $total ? round($total_list['seven'] / $total * 100, 2) : 0;
        $assign['list'] = $list;
        $assign['gid_list'] = $gid_list;
        $assign['sid_list'] = $sid_list;
        $assign['total_list'] = $total_list;

        $this->display(CENTER_TEMP . 'center_role_level_retain.shtml', $assign);
    }


    /**
     * 获取指定专服下的游戏服列表
     */
    public function ajax_group_byserver()
    {
        $gid = intval($_POST['gid']);
        $selecthtml = $this->ajax_allserver_list($gid);
        echo $selecthtml;
        exit;
    }

    /**
     * 获取指定专服下的渠道列表
     */
    public function ajax_channel()
    {
        $selecthtml = $this->ajax_channel_list_select_by_gid();
        echo $selecthtml;
        exit;
    }

    //留存数据查询（分渠道）
    function role_retain_channel()
    {
        //获取服务器列表权限
        $assign = $this->get_server_rights();


        $where = " WHERE 1 AND channel != 'fynemo' ";
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];
        $c_list = $assign['channel_list'];

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $channel = request('channel', 'str');
        //获取平台，服务器，渠道sql
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);
        $where .= $this->get_where_c($channel, $c_list, 'channel');

        if ($_GET['start_time'] && $_GET['end_time']) {
            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']) + 86400;
            $where .= " and time >= '$start_time' and time < '$end_time' ";

        } else {
            $start_time = date('Y-m-01', strtotime(date("Y-m-d")));
            $end_time = date('Y-m-d', strtotime("$start_time +1 month -1 day"));
            $_GET['start_time'] = date('Y-m-01', strtotime(date("Y-m-d")));
            $_GET['end_time'] = $end_time;

            $where .= " and time >= " . strtotime($start_time) . " and time <" . (strtotime($end_time) + 86400);

        }

        $sql = "SELECT
                gid,
                sid,
                channel,
                date(from_unixtime(time)) AS time,
                sum(reg_num) AS reg_num,
                sum(one_login) AS one_login,
                sum(two_login) AS two_login,
                sum(three_login) AS three_login,
                sum(four_login) AS four_login,
                sum(five_login) AS five_login,
                sum(six_login) AS six_login,
                sum(seven_login) AS seven_login,
                sum(fourteen_login) AS fourteen_login,
                sum(thirty_login) AS thirty_login,
                sum(fortyfive_login) AS fortyfive_login,
                sum(sixty_login) AS sixty_login,
                sum(seventyfive_login) AS seventyfive_login,
                sum(ninety_login) AS ninety_login
              FROM
                center_role_retain_channel
                $where
              GROUP BY
                time   ";
        $info = $this->admindb->fetchRow($sql);
        $list = array();
        if (!empty($info)) {
            foreach ($info as $key => $val) {
                $list[$val['time']]['reg_num'] += $val['reg_num'];
                $list[$val['time']]['one_login'] += $val['one_login'];
                $list[$val['time']]['two_login'] += $val['two_login'];
                $list[$val['time']]['three_login'] += $val['three_login'];
                $list[$val['time']]['four_login'] += $val['four_login'];
                $list[$val['time']]['five_login'] += $val['five_login'];
                $list[$val['time']]['six_login'] += $val['six_login'];
                $list[$val['time']]['seven_login'] += $val['seven_login'];
                $list[$val['time']]['fourteen_login'] += $val['fourteen_login'];
                $list[$val['time']]['thirty_login'] += $val['thirty_login'];
                $list[$val['time']]['fortyfive_login'] += $val['fortyfive_login'];
                $list[$val['time']]['sixty_login'] += $val['sixty_login'];
                $list[$val['time']]['seventyfive_login'] += $val['seventyfive_login'];
                $list[$val['time']]['ninety_login'] += $val['ninety_login'];
            }

            foreach ($list as $key => $val) {
                $list[$key]['one_login'] = $val['one_login'] . "(" . round($val['one_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['two_login'] = $val['two_login'] . "(" . round($val['two_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['three_login'] = $val['three_login'] . "(" . round($val['three_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['four_login'] = $val['four_login'] . "(" . round($val['four_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['five_login'] = $val['five_login'] . "(" . round($val['five_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['six_login'] = $val['six_login'] . "(" . round($val['six_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['seven_login'] = $val['seven_login'] . "(" . round($val['seven_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['fourteen_login'] = $val['fourteen_login'] . "(" . round($val['fourteen_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['thirty_login'] = $val['thirty_login'] . "(" . round($val['thirty_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['fortyfive_login'] = $val['fortyfive_login'] . "(" . round($val['fortyfive_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['sixty_login'] = $val['sixty_login'] . "(" . round($val['sixty_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['seventyfive_login'] = $val['seventyfive_login'] . "(" . round($val['seventyfive_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['ninety_login'] = $val['ninety_login'] . "(" . round($val['ninety_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
            }

        }

        $assign['list'] = $list;
        $this->display(CENTER_TEMP . 'role_retain_channel.shtml', $assign);
    }

    /**
     * 等级流失
     */
    public function lv_statis()
    {

        //获取服务器列表权限
        $assign = $this->get_server_rights();
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        $where = " ";
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);

//		$start_time = empty($_GET['start_time']) ? strtotime(date('Y-m-d')) : strtotime(request('start_time','str'));
//		$end_time = empty($_GET['end_time']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime(request('end_time','str').'23:59:59');
        if (!$start_time) {
            $start_time = strtotime(date('Y-m-d')) - 86400 * 6;
            $_REQUEST['start_time'] = date('Y-m-d', $start_time);
        } else {
            $start_time = strtotime($start_time);
        }

        if (!$end_time) {
            $end_time = strtotime(date('Y-m-d')) + 86400;
            $_REQUEST['end_time'] = date('Y-m-d');
        } else {
            $end_time = strtotime($end_time) + 86400;
        }

        if ($end_time - $start_time > 86400 * 31) {
            alert(Ext_Template::lang('时间范围不能大于一个月'));
        }
        $sql = "select count(distinct(a.role_id)) as t,a.level from center_role_login a where a.time>='$start_time' and a.time <'$end_time' $where group by a.level order by a.level asc";
        $info = $this->admindb->fetchRow($sql);
        $list = array();
        $lvlist = array();
        $range = $this->check_range($info[0]['level']);
        $last_range = $range;
        $total = 0;
        if (!empty($info)) {
            foreach ($info as $key => $val) {
                $range = $this->check_range($val['level']);
                if ($last_range != $range) {
                    $lvlist[] = $last_range;
                    $list[] = $total;
                    $total = 0;
                }
                $total += intval($val['t']);
                $last_range = $range;
            }
            $lvlist[] = $range;
            $list[] = $total;
        }

        $assign['jsdate'] = json_encode($lvlist);
        $assign['lcount'] = json_encode($list);
        $this->display('lv_statis.shtml', $assign);
    }

    private function check_range($index)
    {
        if ($index == 0) {
            return "0";
        }
        $a = 1;
        if ($index % 20 == 0) {
            $a = $index / 20;
            if ($a == 1) {
                $b = 1;
            } else {
                $b = ($a - 1) * 20 + 1;
            }
        } else {
            $a = ($index - $index % 20) / 20;
            $b = $a * 20 + 1;
        }
        $c = $b + 19;
        return $b . "~" . $c;
    }

    /**
     * 渠道回本统计 LTV（旧）
     */
    public function channel_return_money1()
    {
        //获取服务器列表权限
        $assign = $this->get_server_rights();
        $gid_list = $assign['srv_group'];
        $sid_list = $assign['srv_server'];      //获取服务器列表
        $c_list = $assign['channel_list'];

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $channel = request('channel', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        $where = " WHERE 1 ";
        $where .= $this->get_where_g($gid, $gid_list);
        $where .= $this->get_where_s($sid, $sid_list);
        $where .= $this->get_where_c($channel, $c_list, 'channel');

        if ($channel) {
            $where .= "AND channel!='fynemo' ";
        }

        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND reg_time >= '$start_time'";
        } else {
            $start_time = $_GET['start_time'] = date('Y-m-01');
            $start_time = strtotime($start_time);
            $where .= " AND reg_time >= '$start_time'";
        }

        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND reg_time < '$end_time'";
        } else {
            $end_time = strtotime(date('Y-m-d')) + 86400;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND reg_time < '$end_time'";
        }

        $sql = "select sum(one_pay_num) as one_pay_num,
                      sum(one_money) as one_money,
                      sum(two_pay_num) as two_pay_num,
                      sum(two_money) as two_money,
                      sum(three_pay_num) as three_pay_num,
                      sum(three_money) as three_money,
                      sum(four_pay_num) as four_pay_num,
                      sum(four_money) as four_money,
                      sum(five_pay_num) as five_pay_num,
                      sum(five_money) as five_money,
                      sum(six_pay_num) as six_pay_num,
                      sum(six_money) as six_money,
                      sum(seven_pay_num) as seven_pay_num,
                      sum(seven_money) as seven_money,
                      sum(two_week_pay_num) as two_week_pay_num,
                      sum(two_week_money) as two_week_money,
                      sum(three_week_pay_num) as three_week_pay_num,
                      sum(three_week_money) as three_week_money,
                      sum(four_week_pay_num) as four_week_pay_num,
                      sum(four_week_money) as four_week_money,
                      sum(four_week_later_money) as four_week_later_money,
                      sum(four_week_later_pay_num) as four_week_later_pay_num,
                      sum(reg_num) as reg_num,
                      reg_time
              from center_channel_return_money $where group by reg_time order by reg_time";
        $res = $this->admindb->fetchRow($sql);
        foreach ($res as $key => $val) {
            $time = $val['reg_time'];
            $now = strtotime(date('Y-m-d'));
            $day = array(
                'one_day' => array($time + 86400, $time + 86400 * 2),
                'two_day' => array($time + 86400 * 2, $time + 86400 * 3),
                'three_day' => array($time + 86400 * 3, $time + 86400 * 4),
                'four_day' => array($time + 86400 * 4, $time + 86400 * 5),
                'five_day' => array($time + 86400 * 5, $time + 86400 * 6),
                'six_day' => array($time + 86400 * 6, $time + 86400 * 7),
                'seven_day' => array($time + 86400 * 7, $time + 86400 * 8),
                'two_week' => array($time + 86400 * 8, $time + 86400 * 15),
                'three_week' => array($time + 86400 * 15, $time + 86400 * 22),
                'four_week' => array($time + 86400 * 22, $time + 86400 * 29),
                'last' => array($time + 86400 * 29, 99999999999)
            );

            $res[$key]['reg_time'] = date('Y-m-d', $val['reg_time']);
            $count_money = $val['one_money'];
            $res[$key]['one_ltv'] = round($count_money / $val['reg_num'], 2);

            if ($now >= $day['two_day'][0] && $val['two_money'] != 0) {
                $count_money = $count_money + $val['two_money'];
                $res[$key]['two_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['two_ltv'] = 0;
            }

            if ($now >= $day['three_day'][0] && $val['three_money'] != 0) {
                $count_money = $count_money + $val['three_money'];
                $res[$key]['three_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['three_ltv'] = 0;
            }

            if ($now >= $day['four_day'][0] && $val['four_money'] != 0) {
                $count_money = $count_money + $val['four_money'];
                $res[$key]['four_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['four_ltv'] = 0;
            }

            if ($now >= $day['five_day'][0] && $val['five_money'] != 0) {
                $count_money = $count_money + $val['five_money'];
                $res[$key]['five_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['five_ltv'] = 0;
            }


            if ($now >= $day['six_day'][0] && $val['six_money'] != 0) {
                $count_money = $count_money + $val['six_money'];
                $res[$key]['six_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['six_ltv'] = 0;
            }


            if ($now >= $day['seven_day'][0] && $val['seven_money'] != 0) {
                $count_money = $count_money + $val['seven_money'];
                $res[$key]['seven_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['seven_ltv'] = 0;
            }


            if ($now >= $day['two_week'][0] && $val['two_week_money'] != 0) {
                $count_money = $count_money + $val['two_week_money'];
                $res[$key]['two_week_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['two_week_ltv'] = 0;
            }


            if ($now >= $day['three_week'][0] && $val['three_week_money'] != 0) {
                $count_money = $count_money + $val['three_week_money'];
                $res[$key]['three_week_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['three_week_ltv'] = 0;
            }


            if ($now >= $day['four_week'][0] && $val['four_week_money'] != 0) {
                $count_money = $count_money + $val['four_week_money'];
                $res[$key]['four_week_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['four_week_ltv'] = 0;
            }

            if ($now >= $day['four_week'][0] && $val['four_week_later_money'] != 0) {
                $count_money = $count_money + $val['four_week_later_money'];
                $res[$key]['four_week_later_ltv'] = round($count_money / $val['reg_num'], 2);
            } else {
                $res[$key]['four_week_later_ltv'] = 0;
            }

            $res[$key]['pay_money'] = $count_money;
            $res[$key]['all_ltv'] = round($count_money / $val['reg_num'], 2);
        }
        $assign['list'] = $res;

        $this->display(CENTER_TEMP . 'channel_return_money1.shtml', $assign);
    }

    /**
     * 渠道回本统计 LTV（新）
     */
    public function channel_return_money()
    {
        //获取服务器列表权限
        $assign = $this->get_server_rights();
        $gid_list = $assign['srv_group'];
        $sid_list = $assign['srv_server'];      //获取服务器列表
        $c_list = $assign['channel_list'];

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $channel = request('channel', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        //平台，服务器，渠道的选择
        $where = " WHERE 1 ";
        $where .= $this->get_where_g($gid, $gid_list);           
        $where .= $this->get_where_s($sid, $sid_list);
        $where .= $this->get_where_c($channel, $c_list, 'channel');

        if ($channel) {
            $where .= "AND channel!='fynemo' ";
        }

        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND reg_time >= '$start_time'";
        } else {
            $start_time = $_GET['start_time'] = date('Y-m-01');  //默认当月第一天开始
            $start_time = strtotime($start_time);
            $where .= " AND reg_time >= '$start_time'";
        }

        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND reg_time < '$end_time'";
        } else {
            $end_time = strtotime(date('Y-m-d')) + 86400;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND reg_time < '$end_time'";
        }

        $sql = "select   reg_num,
                          from_unixtime(reg_time,'%Y-%m-%d') as reg_time,
                          day_money,
                          all_money
              from center_channel_return_money $where order by reg_time";
        $res = $this->admindb->fetchRow($sql);
        $period_ltv = array();       //每一段的ltv
        $period_money = array();      //每一段的总金额
        foreach($res as $key => $val){
            $reg_time = $val['reg_time'];
            $day_money = $val['day_money'];
            $period_money[$reg_time]["reg_num"] += $val['reg_num'];
            $period_money[$reg_time]['all_money'] += $val['all_money'];
            $day_money_arr = json_decode($day_money,true);        //解析json
            if($day_money_arr == null){
                continue;
            }
            ksort($day_money_arr);           //排序
            foreach($day_money_arr as $k => $v) { //将天数对应金额
                $period_money[$reg_time][$k] += $v['money'];
            }
        }
//        Ext_Debug::log($period_money);
        foreach($period_money as $key => $val){        //计算ltv
            $time = strtotime(date('Y-m-d'))-86400;
            $day = ($time - strtotime($key)) / 86400;
            $count_money = 0;
            for($i=1;$i<=90;$i++){
                if($val[$i] == 0 && $i>$day){           //如果某一天的充值金额为0,并还没到。则直接跳过。若已经到了，则跟上一天的相同
                    continue;
                }
                $count_money +=$val[$i];
                $period_ltv[$key][$i] = round($count_money/$val['reg_num'],2);
            }
            $period_ltv[$key]["reg_num"] = $val['reg_num']; //总的注册人数
            $period_ltv[$key]["reg_time"] = $key;            //注册时间
            $period_ltv[$key]["money"] = $val['all_money'];    //金额
            $period_ltv[$key]["ltv"] = round($count_money/$val['reg_num'],2);   //ltv
        }
        $assign['list'] = $period_ltv;

        $this->display(CENTER_TEMP . 'channel_return_money.shtml', $assign);
    }

   //财务对账
    public function center_money_statis()
    {
        //获取所有的平台
        $sql = "select platform_name,gid from adminplatformlist";
        $platform_res = $this->admindb->fetchRow($sql);
        $platform_list = array();
        foreach ($platform_res as $k => $v) {
            $platform_list[$v['gid']]['name'] = $v['platform_name'];
        }
        $gid = request('gid', 'int');
        $channels = request('channel') ? request('channel', 'arr') : request('channel');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $where = " Where 1 ";
        if ($gid) {
            $where .= " and gid = $gid";
            $g_where = "where gid = $gid";
        }
        if ($channels) {
            $where .= ' and source in(';
            $arr = array();
            foreach ($channels as $k => $v) {
                $str = "'$v'";
                array_push($arr, $str);
            }
            $where .= implode(',', $arr);
            $where .= ')';
        }
        if ($start_time && $end_time) {          //给出的时间不得超过一个月
            $start_time = strtotime($start_time);
            $end_time = strtotime($end_time) + 86400;
            if ($end_time - $start_time > 86400 * 31) {
                $end_time = $start_time + 86400 * 31;
            }
            $where .= " and ctime >=$start_time and ctime < $end_time";
        } elseif (!$start_time && !$end_time) {       //默认给一个月的时间
            $start_time = strtotime(date('Y-m'));
            $end_time = strtotime(date('Y-m-d')) + 86400;
            $_GET['start_time'] = date('Y-m-01');
            $_GET['end_time'] = date('Y-m-d');
            $where .= " and ctime >=$start_time and ctime < $end_time";
        } else {
            alert(Ext_Template::lang('请选择正确的时间！')); 
        }
        //获取gid下的所有渠道
        $sql = "select gid,channel_name,channel_str from channel_list $g_where";
        $channel_res = $this->admindb->fetchRow($sql);
        $channel_list = array();
        foreach ($channel_res as $k => $v) {
            $channel_list[$v['channel_str']] = $v['channel_name'];
        }


        $sql = "select sum(money) as total_money ,gid,source as sign,count(*) as total_order,count(distinct role_id) as total_role from center_charge $where group by source,gid ";
        $res = $this->admindb->fetchRow($sql);
        $all_res = array();
        foreach ($res as $key => $v) {
            $res[$key]['gname'] = $platform_list[$v['gid']]['name'];          //平台名
            $res[$key]['cname'] = $channel_list[$v['sign']] . "(" . $v['sign'] . ")";         //渠道名
            $res[$key]['total_money'] = round($v['total_money'] / 100, 2);        //渠道总金额
            $all_res['total_money'] += round($v['total_money'] / 100, 2);           //所有渠道总金额
            $all_res['total_order'] += $v['total_order'];                          //所有渠道的订单数
            $all_res['total_role'] += $v['total_role'];                             //所有渠道的总充值人数

        }
        $assign['platform_list'] = $platform_list;
        $assign['channel_list'] = $channel_list;
        $assign['list'] = $res;
        $assign['all'] = $all_res;
        $this->display(CENTER_TEMP . 'center_money_statis.shtml', $assign);
    }

    public function ajax_checkbox_channel()
    {
        $selecthtml = $this->ajax_channel_list_checkbox_by_gid();
        echo $selecthtml;
        exit;
    }

    //LTV（账号）
    public function accname_return_money()
    {
        exit('废弃');
        $accname = request('accname', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $where = " WHERE 1 ";

        if($accname){
            $where .= " AND accname  = '$accname'";
        }

        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND reg_time >= '$start_time'";
        } else {
            $start_time = $_GET['start_time'] = date('Y-m-01');
            $start_time = strtotime($start_time);
            $where .= " AND reg_time >= '$start_time'";
        }

        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND reg_time < '$end_time'";
        } else {
            $end_time = strtotime(date('Y-m-d')) + 86400;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND reg_time < '$end_time'";
        }

        $sql = "select   reg_num,
                          from_unixtime(reg_time,'%Y-%m-%d') as reg_time,
                          day_money,
                          accname,
                          all_money
              from center_accname_return_money $where order by reg_time";
        $res = $this->admindb->fetchRow($sql);
        $period_ltv = array();
        $period_money = array();
        foreach($res as $key => $val){
            $reg_time = $val['reg_time'];
            $day_money = $val['day_money'];
            $period_money[$reg_time]["reg_num"] += $val['reg_num'];
            $period_money[$reg_time]['all_money'] += $val['all_money'];
            $day_money_arr = json_decode($day_money,true);
            if($day_money_arr == null){
                continue;
            }
            ksort($day_money_arr);
            foreach($day_money_arr as $k => $v) {
                $period_money[$reg_time][$k] += $v['money'];
            }
        }
//        Ext_Debug::log($period_money);
        foreach($period_money as $key => $val){
            $time = strtotime(date('Y-m-d'))-86400;
            $day = ($time - strtotime($key)) / 86400;
            $count_money = 0;
            for($i=1;$i<=90;$i++){
                if($val[$i] == 0 && $i > $day){
                    continue;
                }
                $count_money +=$val[$i];
                $period_ltv[$key][$i] = round($count_money/$val['reg_num'],2);
            }
            $period_ltv[$key]["reg_num"] = $val['reg_num'];
            $period_ltv[$key]["reg_time"] = $key;
            $period_ltv[$key]["money"] = $val['all_money'];
            $period_ltv[$key]["ltv"] = round($val['all_money']/$val['reg_num'],2);
            Ext_Debug::log($val['all_money']."-----------".$count_money);
        }
        $assign['list'] = $period_ltv;

        $this->display(CENTER_TEMP . 'accname_return_money.shtml', $assign);
    }

    function accname_retain_channel()
    {
        $where = " WHERE 1  ";
        $accname = request('accname', 'str');

        if($accname){
            $where .= " and accname = '$accname' ";
        }
        if ($_GET['start_time'] && $_GET['end_time']) {
            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']) + 86400;
            $where .= " and time >= '$start_time' and time < '$end_time' ";

        } else {
            $start_time = date('Y-m-01', strtotime(date("Y-m-d")));
            $end_time = date('Y-m-d', strtotime("$start_time +1 month -1 day"));
            $_GET['start_time'] = date('Y-m-01', strtotime(date("Y-m-d")));
            $_GET['end_time'] = $end_time;

            $where .= " and time >= " . strtotime($start_time) . " and time <" . (strtotime($end_time) + 86400);

        }

        $sql = "SELECT
                accname,
                date(from_unixtime(time)) AS time,
                sum(reg_num) AS reg_num,
                sum(one_login) AS one_login,
                sum(two_login) AS two_login,
                sum(three_login) AS three_login,
                sum(four_login) AS four_login,
                sum(five_login) AS five_login,
                sum(six_login) AS six_login,
                sum(seven_login) AS seven_login,
                sum(fourteen_login) AS fourteen_login,
                sum(thirty_login) AS thirty_login
              FROM
                center_accname_retain
                $where
              GROUP BY
                time   ";
        $info = $this->admindb->fetchRow($sql);
        $list = array();
        if (!empty($info)) {
            foreach ($info as $key => $val) {
                $list[$val['time']]['reg_num'] += $val['reg_num'];
                $list[$val['time']]['one_login'] += $val['one_login'];
                $list[$val['time']]['two_login'] += $val['two_login'];
                $list[$val['time']]['three_login'] += $val['three_login'];
                $list[$val['time']]['four_login'] += $val['four_login'];
                $list[$val['time']]['five_login'] += $val['five_login'];
                $list[$val['time']]['six_login'] += $val['six_login'];
                $list[$val['time']]['seven_login'] += $val['seven_login'];
                $list[$val['time']]['fourteen_login'] += $val['fourteen_login'];
                $list[$val['time']]['thirty_login'] += $val['thirty_login'];
            }

            foreach ($list as $key => $val) {
                $list[$key]['one_login'] = $val['one_login'] . "(" . round($val['one_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['two_login'] = $val['two_login'] . "(" . round($val['two_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['three_login'] = $val['three_login'] . "(" . round($val['three_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['four_login'] = $val['four_login'] . "(" . round($val['four_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['five_login'] = $val['five_login'] . "(" . round($val['five_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['six_login'] = $val['six_login'] . "(" . round($val['six_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['seven_login'] = $val['seven_login'] . "(" . round($val['seven_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['fourteen_login'] = $val['fourteen_login'] . "(" . round($val['fourteen_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
                $list[$key]['thirty_login'] = $val['thirty_login'] . "(" . round($val['thirty_login'] / (empty($val['reg_num']) ? 1 : $val['reg_num']) * 100, 2) . "%)";
            }

        }

        $assign['list'] = $list;
        $this->display(CENTER_TEMP . 'accname_retain_channel.shtml', $assign);
    }

    /**
     * 离线等级流失率
     */
    public function center_role_level_retain_new(){
        //获取服务器列表权限
        $assign = $this->get_server_rights();

        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $date = request('time', 'str');
        $type = request('type','int');
        $isExport = request('isExport','str');
        $where = "where 1";
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);

        if ($date) {
            $time = strtotime($date);
        } else {
            $time_str = date('Y-m-d');
            $time = strtotime($time_str) - 86400;
            $_GET['time'] = date('Y-m-d', $time);
        }
        $date = date('Y-m-d', $time);
        if($type){
            $where .=" and type=".$type;
        }
        $where .= " AND time = " . $time;

        $sql = "select  date(from_unixtime(time)) AS time, gid,sid,one_day,three_day,sevent_day,all_count,lv_all_count from center_lv_retain $where";
        $res = $this->admindb->fetchRow($sql);
        $list = array();
        $all_count = array();
        foreach($res as $ke => $va){

            $one_day = json_decode($va['one_day'],true);        //一天未登录的数据
            $three_day = json_decode($va['three_day'],true);    //三天未登陆的数据
            $sevent_day = json_decode($va['sevent_day'],true);   //七天未登陆的数据
            $lv_all_count = json_decode($va['lv_all_count'],true);  //等级所有的数据
            $all_count['all']['num'] += $va['all_count'];          //全服的人数
//            $list[]
            foreach($one_day as $k => $v){
                $list[$k]['one']['num'] += $v;                      //一天未登陆的人数，按等级划分
                $all_count['one']['num'] +=$v;                      //一天未登陆的人数
            }
            foreach($three_day as $k => $v){
                $list[$k]['three']['num'] += $v;                    //三天未登陆的人数，按等级划分
                $all_count['three']['num'] +=$v;                    //三天未登陆的人数
            }
            foreach($sevent_day as $k => $v){
                $list[$k]['sevent']['num'] += $v;                  //七天未登陆的人数，按等级划分
                $all_count['sevent']['num'] +=$v;                   //七天未登陆的人数
            }

            foreach($lv_all_count as $k => $v){
                $list[$k]['all']['num'] += $v;                      //全部人数，按等级划分
                $all_count['all_num']['num'] +=$v;                  //全部人数
            }
        }
        foreach($list as $k=>$v){
            $list[$k]['one']['rate'] = round($v['one']['num']/$v['all']['num'],4)*100;  //一天的流失率
            $list[$k]['three']['rate'] = round($v['three']['num']/$v['all']['num'],4)*100;//三天的流失率
            $list[$k]['sevent']['rate'] = round($v['sevent']['num']/$v['all']['num'],4)*100;//七天的流失率
            $list[$k]['all']['rate'] = round($v['all']['num']/$all_count['all']['num'],4)*100;//全部的流失率
        }

        $all_count['one']['rate'] = $all_count['all']['num'] ? round($all_count['one']['num']/$all_count['all']['num'],4)*100 : 0;  //总的一天的流失率
        $all_count['three']['rate'] = $all_count['all']['num'] ? round($all_count['three']['num']/$all_count['all']['num'],4)*100: 0;//总的三天的流失率
        $all_count['sevent']['rate'] = $all_count['all']['num'] ? round($all_count['sevent']['num']/$all_count['all']['num'],4)*100: 0;//总的七天的流失率

        if($isExport){
            $this->export_lv_retain($list,$all_count,$date);      //导出表格
        }else{
            $assign['list'] = $list;
            $assign['all_count'] = $all_count;
            $this->display(CENTER_TEMP . 'center_role_level_retain_new.shtml', $assign);
        }
    }

    /**
     * 离线等级流失率导出表格
     */
    private function export_lv_retain($res,$all_count,$date){
        require_once("Classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, 1, '等级');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, 1, '总数('.$all_count['all']['num'].')');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, 1, '占总比率');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, 1, '1天未登录数('.$all_count['one']['num'].')');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, 1, '1天流失率('.$all_count['one']['rate'].'%)');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, 1, '3天未登录数('.$all_count['three']['num'].')');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, 1, '3天流失率('.$all_count['three']['rate'].'%)');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, 1, '7天未登录数('.$all_count['sevent']['num'].')');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, 1, '7天流失率('.$all_count['sevent']['rate'].'%)');
        foreach ($res as $key => $val) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $key + 1, $key);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $key + 1, $val['all']['num']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $key + 1, $val['all']['rate'].'%');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $key + 1, $val['one']['num']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $key + 1, $val['one']['rate'].'%');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 1, $val['three']['num']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $key + 1, $val['three']['rate'].'%');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $key + 1, $val['sevent']['num']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $key + 1, $val['sevent']['rate'].'%');
        }
        $objPHPExcel->getActiveSheet()->setTitle("离线等级流失");
        $excelName = $date . '离线等级流失.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $excelName);
        header('Cache-Control: max-age=0');
        header("Pragma: no-cache");
        $objWriter->save('php://output');
    }

}