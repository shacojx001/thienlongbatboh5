<?php
/**
 * 接口api
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_api_show extends action_superclass
{


///目录文件可视化
    function file_show() {
        
        if($_GET['url']) {
           $dir = $_GET['url'];
        } else {
           $dir = dirname(__FILE__);
        }
        //清文件缓存
        clearstatcache();
        $files = scandir($dir);
        $url = '"'.$dir.'"';
        $add_file = "<a id='add' href='#' onclick='fun(this)' value='$dir'>添加文件</a>";
          
        echo "<table border=1 borderColor=red cellpadding=6>";
        echo "<tr>
            <th>文件名</th>
            <th>大小</th>
            <th>类型</th>
            <th ondblclick='delete_show()'>修改日期</th>
            <th>权限</th>
            <th>$add_file</th>
            </tr>";
        foreach ($files as $file) {
            $file_url = $dir."/".$file;
            if($file=='.') {
                continue;
            }
            $str = $_GET['show']==1?"<a class='delete_a' href='/action_gateway.php?ctl=api_show&act=delete_file&file_name=".$file_url."' >删除</a></td>":"";
            if(is_dir($file_url)) {//如果是文件夹\
                if($file == '..') {
                   $file = '上级目录';
                }
                echo "<tr><td>
                    <a href='/action_gateway.php?ctl=api_show&act=file_show&url=".$file_url."'>
                    $file
                    </a></td>";
            } else {
                echo "<tr><td>
                    <a href='/action_gateway.php?ctl=api_show&act=file_info&url=".$file_url."'>
                    $file
                    </a></td>";
            }
            echo "<td>" . filesize($file_url) . "KB</td>";
            echo "<td>" . filetype($file_url) . "</td>";
            echo "<td>" . date('Y-m-d H:i:s', filemtime($file_url)) . "</td>";
            echo "<td>" . substr(sprintf("%o",fileperms($file_url)),-4) . "</td>";
            if(is_dir($file_url)) {//如果是文件夹\
                echo "<td></td></tr>";
            } else {
                echo "<td ondblclick='ondblclick_f(obj)'>
                   <a href='/action_gateway.php?ctl=api_show&act=file_modify&file_name=".$file_url."'>修改</a>$str</tr>";
            }  
        }

        echo "</table>";
        echo "<script>
            function fun(obj) {
                var url = obj.getAttribute('value');
                var file_name = prompt('输入文件名(后缀也要)');
                if(!file_name) {
                    alert('文件名不能为空');
                }
                else {
                    location.href='/action_gateway.php?ctl=api_show&act=create_file&file_name='+file_name+'&url='+url;
                }
            }
            function delete_show() {
              window.location.href=window.location.href+'&show=1';
            }     
        </script>";
  
    }

/////文件修改
    function file_modify() {
   
        $file_name = $_GET['file_name'];
        if(empty($file_name)) {
            alert('文件名为空！');
        }
        $content = file_get_contents($file_name);
        echo
            "<form method='POST' action='/action_gateway.php?ctl=api_show&act=file_save'>".
            "文件名：$file_name".
            "<input type='hidden' name='file_name' value=$file_name>".
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".
            "<input type='submit' value='保存'>".
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".
            "<input type='button' value='上一步' onclick='window.history.back(-1);'>".
            "<textarea name='file_contents' style='width: 100%;
                   height: 100%;'>".
            $content.
            "</textarea>".
            "</form>";
        exit;
    }  

//文件删除
    function delete_file() {
        
        $file = $_GET['file_name'];
        unlink($file);
        alert('删除成功！');
    }

//////文件保存
    function file_save() {
    
        $file_name = $_POST['file_name'];
        $file_contents = $_POST['file_contents'];
        if (empty($file_contents)) {
            alert("内容不能为空");
            exit;
        }
        if (empty($file_name)) {
            alert("文件名不能为空");
            exit;
        }
        file_put_contents($file_name, $file_contents);
        alert('文件保存成功！');
    }



////文件查看
    function file_info() {
        
        $file_path = $_GET['url'];
        if(file_exists($file_path)) {
            $fp = fopen($file_path,"r");
            $str = fread($fp,filesize($file_path));//指定读取大小，这里把整个文件内容读取出来
        }
        echo "<pre style=' font-size: 18px;'>";print_r(str_replace('<?php','',$str));echo "<pre>"; exit;
    }
        ////创建文件
    function create_file() {
        
        $file_name = $_GET['file_name'];
        $url = $_GET['url']; 
        $file_url = $url."\\".$file_name;
        if(file_exists($file_url)) {
            alert('文件已存在');exit;
        }
        if(!strstr($file_url,".")) {//如果是文件夹
            mkdir($file_url);
        } else {//如果是文件
            $myfile = fopen($file_url,'w');
            fclose($myfile);
        }
        alert('文件创建成功!');
    }




    function allrun_show() {
        $url = dirname(dirname(dirname(__FILE__))) . "/cron/data_mining.php";
        $res = file_get_contents($url);

        $assign['filename'] = $filename;
        $assign['res'] = $res;

        $this->display(CENTER_TEMP . 'allrun_show.shtml', $assign);
    }

    function allrun_show_save() {
        $content = $_REQUEST['content'];
        if (empty($_REQUEST['content'])) {
            alert("内容不能为空");exit;
        }

        $url = dirname(dirname(dirname(__FILE__))) . "/cron/data_mining.php";
        if(stripos($content,"system(")!== false) {
            echo "有敏感字词！";exit;
        }
        if(stripos($content,"passthru(")!== false) {
            echo "有敏感字词！";exit;
        }
        if(stripos($content,"shell_exec(")!== false) {
            echo "有敏感字词！";exit;
        }
        if(stripos($content,"popen(")!== false) {
            echo "有敏感字词！";exit;
        }
        if(stripos($content,"proc_open(")!== false) {
            echo "有敏感字词！";exit;
        }
        file_put_contents($url, $content);
        alert('生成成功');
    }


    function allrun() {
        $url = dirname(dirname(dirname(__FILE__))) . "/cron/data_mining.php";
        //检测php语法错误
        $res = shell_exec("php -l $url");
        if(!strpos($res,'syntax errors')) {
            echo 'fail';exit;
        }
        //检测脚本是否在执行中
        $res = shell_exec("ps -ef|grep -v grep | grep 'data_mining.php'");
        if($res) {
            echo 'runing';exit;
        }
        pclose(popen("php ".$url." &", 'r'));
        echo 'ok';exit;
    }

    function check_allrun() {
        $url = dirname(dirname(dirname(__FILE__))) . "/cron/data_mining.php";

        //检测php语法错误
        $res = shell_exec("php -l $url");
        if(!strpos($res,'syntax errors')) {
            echo 'fail';exit;
        }
        $res = shell_exec("ps -ef|grep -v grep | grep 'data_mining.php'");
        if($res) {
            echo $res;exit;
        } else {
            echo 'ok';exit;
        }
    }

    function kill_allrun() {
        shell_exec("ps -ef | grep data_mining.php | awk '{print $2}' | xargs kill -9");

        alert('进程已经停止！','/action_gateway.php?ctl=api_show&act=allrun_show');exit;
    }

    //资源下载
    function run_download() {
        $url = dirname(dirname(dirname(__FILE__))) . "/cron/data_mining.txt";
        $data = file_get_contents($url, $data);

        header("content-type:application/octet-stream");
        header('Content-Disposition: attachment; filename=' . '数据抓取.txt');
        header("Content-Transfer-Encoding: binary");
        echo $data;

        unlink($url);
        exit;
    }



}