<?php
/**
 * 新日志功能
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_log extends action_superclass
{   
    public function get_server_list()
    {   
        $gid_list = $this->get_allgroup_list('admindb', 'adminplatformlist', 'gid', 'platform_name');
        
        $time = time();
        $server_list = $this->get_adminserver_list('gid,id', 'description'," where 1 and otime<$time ");
        
        return array($gid_list, $server_list);
    }

    //日志分类 
    public function logType()
    {  
        $this->display(CENTER_TEMP . 'tableLogType.shtml', $assign);
    }

    //日志分类读取
    public function logTypeAjax()
    {
        $sql = "select * from base_log_type";
        $res = $this->admindb->fetchRow($sql);

        echo $this->echo_lay_json(0, 'ok', $res);
    }

    //日志分类操作
    public function logTypeAction()
    {
        $type = $_REQUEST['type'];
        if( $type == 'add' ){
            $name = $_REQUEST['name'];
            $sql = "INSERT INTO base_log_type(`name`)value('{$name}');";
        }elseif( $type == 'update' ){
            $id = $_REQUEST['id'];
            $name = $_REQUEST['name'];
            $sql = "UPDATE base_log_type SET `name`='{$name}' WHERE id = $id;";
        }elseif( $type == 'delete' ){
            $id = $_REQUEST['id'];
            $sql = "DELETE FROM base_log_type WHERE id = $id;";
        }

        $this->admindb->query($sql);

        echo $this->echo_lay_json(0, 'ok');
        // print_r($_REQUEST);exit;
    }

    //获取baseLog
    public function getBaseLog($id = '')
    {   
        $cache = Ext_Memcached::getInstance('user');
        if(!$list = $cache->fetch('base_log_list')){
            $sql = "select * from base_log";  
            $res = $this->admindb->fetchRow($sql);
            $list = array();
            foreach ($res as $key => $val) {
                $list[$val['id']] = $val;
            }
            $cache->store('base_log_list',$list,300);
        }
        return $id ? $list[$id] : $list;
    }

    //清除cachae
    public function clear_cache(){
        $cache = Ext_Memcached::getInstance('user');
        $cache->delete('base_log_list');
        $cache->delete('logTableList');
        return true;
    }

     //日志展示
    public function tableShow()
    {          

        list($list, $cols, $selectList, $replaceList, $post, $extrList) = $this->tableShowFun();

        $assign['list'] = $list;
        $assign['cols'] = json_encode($cols);
        $assign['page'] = $list['page'];
        $assign['searchList'] = $list['searchList'];
        $assign['replaceList'] = $replaceList;
        $assign['extrList'] = $extrList;
        $assign['selectList'] = json_encode($selectList);
        $assign['url'] = '/action_gateway.php?ctl=log&act=tableShowAjax&'.http_build_query($post);
        $assign['post'] = json_encode($post);
        $assign['goodList'] = $this->get_config_list(1);

        $assign['selectListPost'] = $selectList;
        if($_REQUEST['expTxt'] || $_REQUEST['expCsv']){
            $type = $_REQUEST['expTxt'] ? 'txt' : 'csv';
            $this->tableShowAjax($type, $replaceList);
        }  
        
        $assign['selectListStyle'] = array(
                'role_list' => "width: 280px;",
            );
        // print_r($list['searchList']);exit;
        $this->display(CENTER_TEMP . 'tableShow.shtml', $assign);
    }

    public function tableShowAjax($type = '', $replaceList = array())
    {   
        
        $id = $_REQUEST['table_id'];
        // $list = $this->admindb->fetchOne("select * from base_log where id = $id");  
        $list = $this->getBaseLog($id);
      
        $list['searchList'] = unserialize($list['searchList']);

        //获取搜索条件
        $where = $this->tableWhereCreat($list['searchList']);
// print_r($where);

        //排序
        $order = $_REQUEST['tableField'] ? ' ORDER BY '.$_REQUEST['tableField'].' '.$_REQUEST['tableOrder'] : $list['order'];

        //limit 拼接
        if($type){
            $limit = ' ';
        }else{
            $num = $_REQUEST['limit'];
            $page = $_REQUEST['page'];
            $limit = " LIMIT ". ($page-1) * $num .",".$num;
        }
        

        //分页SQL
        $sql = str_replace(';', ' ', $list['sql']);//去掉结束分号;
        $sql = str_ireplace('from', " from", $sql);//加个空格;
        $sql = str_ireplace('left', " left ", $sql);//加个空格;
        $sql = str_ireplace('where', " where ", $sql);//加个空格;
        $sql = str_ireplace('right', " right ", $sql);//加个空格;
        //去掉
        $sql = ltrim(rtrim(preg_replace(array("/\r\n/","/\t/","/\s(?=\s)/"),array('',' ',"\\1"),$sql)));
        if(count($cs = explode(')', $sql))>1){
            if(stripos($cs[count($cs)-1],"where")){
                $where = str_replace('WHERE', ' AND ', $where);//where替换成and
            }
        }elseif(stripos($sql,"WHERE")){
            $where = str_replace('WHERE', ' AND ', $where);//where替换成and
        }   

        $sql_page = preg_replace("/^(select) (.*?) (from) (.*?)/i", '$1 count(1) as num $3 $4', $sql.$where);
// print_r($sql_page);exit;
        $count = $this->db->fetchOne($sql_page);

        $sql = $sql.$where.$order.$limit;

        $data = $this->db->fetchRow($sql);
        
        //打印
        $this->export($type, $data, $list['title'], $replaceList);

        $echo = array();
        if($_REQUEST['sqlEcho'] == 1) $echo['sql'] = $sql;

        echo $this->echo_lay_json(0, 'ok', $data, $count['num'], true, $echo);
    }

    public function tableList(){
        
        $cache = Ext_Memcached::getInstance('user');
        if(!$list = $cache->fetch('logTableList')){   
            $sql = "select b.id,title,pid,`name`,`sql`,searchList,replaceList,`order`,page from base_log b LEFT JOIN base_log_type t on b.pid=t.id order by pid asc,id asc";
            $list = $this->admindb->fetchRow($sql);
         
            foreach ($list as $key => $val) {
                $rgb = $this->color_return($val['pid']);
                $color = implode(',', $rgb);
                
                $list[$key]['name'] = "<span style='color : rgb($color)'>".$val['name']."</span>";
            }
   // print_r($list);exit;
            $cache->store('logTableList',$list,300);
        }

        $assign['list'] = json_encode($list);

        $this->display(CENTER_TEMP . 'tableList.shtml', $assign);
    }

    public function tableDelete(){
        $id = $_REQUEST['id'];
        $sql = "delete from base_log where id=$id";
        $this->admindb->query($sql);
        $this->clear_cache();
        alert("删除成功！");
    }

    public function tableEdit(){

        if($_REQUEST['id']){
            $list = $this->fieldInput();
            $list['searchList'] = json_encode($list['searchList']); 
            $list['replaceList'] = json_encode($list['replaceList']); 
        }

        $searchSelect = array(
                        'role_list'=>'◆角色列表',
                        'guild_list'=>'◆公会列表',
                        'eq'=>'★精确查询',
                        'like'=>'★模糊查询',
                        'between'=>'★区间类型',
                        'in'=>'★in类型',
                        'time'=>'★时间类型',
                        'key_val'=>'★key_val',
                        'sql'=>'★sql'
                        );

        $replaceSelect = array('to_day'=>'★日期高亮','key_val'=>'★key_val','sql'=>'★sql');
        
        $extr = $this->get_config_list();
        $assign['searchSelect'] = json_encode($searchSelect + $extr);
        $assign['replaceSelect'] = json_encode(array('role_list'=>'◆角色列表','guild_list'=>'◆公会列表') + $replaceSelect + $extr);
        $assign['list'] = $list;

        $assign['base_log_type'] = $this->get_list('admin','base_log_type','id','name');

        $this->display(CENTER_TEMP . 'tableEdit.shtml', $assign);
    }

    //日志保存
    public function tableSave()
    {   

        //把数组里的参数，全赋值
        
        foreach ($_REQUEST as $key => $val) {
            if(is_array($val)){
                eval("\$$key = \$val;");
            }else{
                eval("\$$key = trim(\$val);");
            }
            
        }
// print_r($_REQUEST);exit;    
        //获取处理后的searchList
        $searchList = $this->searchFieldCreate();

        //获取处理后的replaceList
        $replaceList = $this->replaceFieldCreate();
        if(!$id){
            $res = $this->admindb->fetchOne("select max(id) as id from base_log");
            $id = $res['id'] + 1;
        }    
       
        $data = array(
                'id'=>$id,
                'title'=>$title,
                'pid' =>$pid,
                'sql'=>addslashes($sql),
                'order'=>addslashes($order),
                'page'=>$page,
                'searchList'=>addslashes(serialize($searchList)),
                'replaceList'=>addslashes(serialize($replaceList)),
                );    

        $this->admindb->insert('base_log', $data, true);
        $this->clear_cache();

        alert("操作成功","/action_gateway.php?ctl=log&act=tableEdit&id=".$id);
    }

    //tableShow
    public function tableShowFun()
    {   
        $id = $_REQUEST['table_id'];
        // $list = $this->admindb->fetchOne("select * from base_log where id = $id");  
        $list = $this->getBaseLog($id);

        $list['searchList'] = unserialize($list['searchList']);
        $list['replaceList'] = unserialize($list['replaceList']);
     // print_r($list['searchList']);exit;    
        $selectList = array();
        if($list['searchList'] && !$_REQUEST['expTxt'] && !$_REQUEST['expCsv']){
            foreach ($list['searchList'] as $key => $val) {
                
                if(strpos($val['field'], '.')){
                    $list['searchList'][$key]['field'] = $val['field'] = str_replace('.','__',$val['field']);
                }
                $list['searchList'][$key]['val'] = $_REQUEST[$val['field']];
                
                if($val['type'] == 'sql'){
                    $sql = $val['vice_sql'];
                    $res = $this->db->fetchRow($sql);  
                    list($lKey,$lVal) = array_keys($res[0]);
                    foreach ($res as $kk => $vv) {
                        $list['searchList'][$key]['list'][$vv[$lKey]] = $vv[$lVal];
                    }
                    if($_REQUEST[$val['field']] || is_numeric($_REQUEST[$val['field']]) ){
                        $selectList[$val['field']] = explode(',', ($_REQUEST[$val['field']]));
                    } 

                }elseif($val['type'] == 'key_val'){
                    $res = $val['vice_key_val'];
                    $res = explode("\r", $res);
                    
                    foreach ($res as $kk => $vv) {
                        list($lKey,$lVal) = explode(' ', $vv);
                        $lKey = trim($lKey);
                        $lVal = trim($lVal);
                        $list['searchList'][$key]['list'][$lKey] = $lVal;
                    }
                    
                    if($_REQUEST[$val['field']] || is_numeric($_REQUEST[$val['field']]) ){
                        $selectList[$val['field']] = explode(',', ($_REQUEST[$val['field']]));
                    } 
                }elseif($val['type']=='between'){
                    // print_r(I());exit;
                    if($_REQUEST[$val['field']]){
                        $list['searchList'][$key]['vice_between'] = implode('-',$_REQUEST[$val['field']]);
                    }     
                    
                }elseif($val['type'] == 'role_list'){

                    $list['searchList'][$key]['list'] = $this->get_config_list($val['type'], false);
// print_r($list['searchList'][$key]['list']);exit;
                    if($_REQUEST[$val['field']] || is_numeric($_REQUEST[$val['field']]) ){
                        $selectList[$val['field']] = explode(',', ($_REQUEST[$val['field']]));
                    } 

                }elseif($val['type'] == 'guild_list'){

                    $list['searchList'][$key]['list'] = $this->get_config_list($val['type'], false);

                    if($_REQUEST[$val['field']] || is_numeric($_REQUEST[$val['field']]) ){
                        $selectList[$val['field']] = explode(',', ($_REQUEST[$val['field']]));
                    } 

                }else{
                    $list['searchList'][$key]['list'] = $val['type']==1 ? array() : $this->get_config_list($val['type'], false);
                    if(is_numeric($val['type'])){
                        $selectList[$val['field']] = explode(',', ($_REQUEST[$val['field']]));
                    }
                }
            }
        }
// print_r($list['searchList']);exit;        
        //替换条件
        $replaceList = array();
        $extrList = array();
        $replacewith = array();
        if($list['replaceList']){
            foreach ($list['replaceList'] as $key => $val) {
                $replacewith[$key] = '';
                if($val['type'] == 'sql'){
                    $sql = $val['vice_sql'];
                    $res = $this->db->fetchRow($sql);    
                    list($lKey,$lVal) = array_keys($res[0]);
                    
                    foreach ($res as $kk => $vv) {
                        $replaceList[$val['field']][$vv[$lKey]] = $vv[$lVal];  
                    }
                      
                }elseif($val['type'] == 'key_val'){
                    $res = $val['vice_key_val'];
                    $res = explode("\r", $res);
                   
                    foreach ($res as $kk => $vv) {
                        list($lKey, $lVal, $color) = explode(' ', $vv);
                        $lKey = trim($lKey);
                        $lVal = trim($lVal);
                        $color = trim($color);
                        $str =  $color ? "<span style='color:$color'>$lVal</span>" : $lVal;
                        $replaceList[$val['field']][$lKey] = $str;
                    }
                    $replacewith[$val['field']] = 90;
                }elseif($val['type'] == 'to_day'){
                    
                    $extrList[$val['field']] = array(
                                            'type'  =>'to_day',
                                            'val'   => array(
                                            'tomorrow'  =>(strtotime(date('Y-m-d'))+86400)*1000,
                                            'today'     =>(strtotime(date('Y-m-d')))*1000,
                                            'yesterday' =>(strtotime(date('Y-m-d'))-86400)*1000,
                                            'befday'    =>(strtotime(date('Y-m-d'))-86400*2)*1000,
                                            'threeday'  =>(strtotime(date('Y-m-d'))-86400*3)*1000,
                                            ),
                                                );
                    $replacewith[$val['field']] = 180;
                }elseif($val['type'] == 'role_list'){
                    $attr = true;
                    if($_REQUEST['expTxt'] || $_REQUEST['expCsv']){
                        $attr = false;
                    }

                    $replaceList[$val['field']] = $this->get_config_list($val['type'], $attr);
// print_r($replaceList[$val['field']]);exit;
                    $replacewith[$val['field']] = 300;
                }elseif($val['type'] == 'guild_list'){

                    $replaceList[$val['field']] = $this->get_config_list($val['type']);
                    $replacewith[$val['field']] = 230;
                }else{
                    $attr = true;
                    if(($_REQUEST['expTxt'] || $_REQUEST['expCsv'])){
                        $attr = false;
                    }

                    $replaceList[$val['field']] = $this->get_config_list($val['type'], $attr);
                    $replacewith[$val['field']] = $val['type']==1 ? 200 : 120;
                }
            }
            //格式化
            foreach ($replaceList as $key => $val) {
                $replaceList[$key] = json_encode($val);
            }
            
        }

        //获取表格头
        $sql = $list['sql']." LIMIT 1";

        $data = $this->db->fetchOne($sql);
      
        $cols = array();
        if($data){
            foreach (array_keys($data) as $key => $val) {
                if ($replaceList[$val]) {
                    $cols[] = array('field'=> $val, 'title'=> $val, 'sort'=>true, 'templet'=>"#{$val}", 'width'=>$replacewith[$val]);
                }elseif($extrList[$val]){
                    $cols[] = array('field'=> $val, 'title'=> $val, 'sort'=>true, 'templet'=>"#{$val}", 'width'=>$replacewith[$val]);
                }else{
                    $cols[] = array('field'=> $val, 'title'=> $val, 'sort'=>true);
                }
            }
        }   
        //请求参数处理
        // between特殊处理
        $post = array();
        foreach ($_REQUEST as $key => $val) {
            if(is_array($val)){
                
                $post[$key] = $val;
                $post[$key] = implode('-', $val);
            }else{
                $post[$key] = $val;
            } 
        }
        unset($post['ctl']);
        unset($post['act']);
        return array($list, $cols, $selectList, $replaceList, $post, $extrList);
    }

    //输出搜索字体副属性显示
    public function fieldInput()
    {
        $id = $_REQUEST['id'];
        // $list = $this->admindb->fetchOne("select * from base_log where id = $id");   
        $list = $this->getBaseLog($id);
        $list['searchList'] = unserialize($list['searchList']);
        $list['replaceList'] = unserialize($list['replaceList']);
        $list = $this->typeReturn($list);
        return $list;
    }

    //对字段处理
    public function typeReturn($data)
    {
        foreach ($data['searchList'] as $key => $val) {
            $mark = '';
            switch ($val['type']) {
                case 'key_val':
                    $mark = $val['vice_key_val'];
                    break;
                case 'sql':
                    $mark = $val['vice_sql'];
                    break;    
                default:
                    # code...
                    break;
            }
            $data['searchList'][$key]['mark'] = $mark;
        }
        foreach($data['replaceList'] as $key => $val) {
            $mark = '';
            switch ($val['type']) {
                case 'key_val':
                    $mark = $val['vice_key_val'];
                    break;
                case 'sql':
                    $mark = $val['vice_sql'];
                    break;    
                default:
                    # code...
                    break;
            }
            $data['replaceList'][$key]['mark'] = $mark;
        }
        return $data;
    }


    //对传入的搜索值处理
    public function searchFieldCreate()
    {
        foreach ($_REQUEST as $key => $val) {
            if(is_array($val)){
                eval("\$$key = \$val;");
            }else{
                eval("\$$key = trim(\$val);");
            }
        }
        $searchList = array();
        if($searchField){
            foreach ($searchField as $key => $val) {
                $searchList[$key] = array(
                                'name'=>$searchName[$key],
                                'field'=>trim($searchField[$key]),
                                'type'=>trim($searchType[$key]),
                                'sort'=>$searchSort[$key],
                                'vice_sql'=>$searchSql[$key],
                                'vice_key_val'=>$searchkey_val[$key],
                                    );
            }
            // $searchList = A('Common/FunArray')->my_sort($searchList,'sort',SORT_ASC);
            $searchList = array_column_sort($searchList,'sort','SORT_ASC');
            // $searchList = serialize($searchList);
        }
     
        return $searchList;
    }

    public function export($type, $data, $name, $replaceList){
        
        if(empty($type)) return false;
        if(count($replaceList)>0){
            foreach ($replaceList as $key => &$val) {
                $val = json_decode($val, true);
            }
        }
        if($type == 'txt') expTxt($data, $name, $replaceList);
        if($type == 'csv') expCsv($data, $name, $replaceList);
        return true;
    }

    //对传入的替换值处理
    public function replaceFieldCreate()
    {   
        foreach ($_REQUEST as $key => $val) {
            if(is_array($val)){
                eval("\$$key = \$val;");
            }else{
                eval("\$$key = trim(\$val);");
            }
        }
        $replaceList = array();
        if($replaceField){
            foreach ($replaceField as $key => $val) {
                $replaceList[$key] = array(
                                'field'=>$replaceField[$key],
                                'type'=>$replaceType[$key],
                                'vice_sql'=>$replaceSql[$key],
                                'vice_key_val'=>$replacekey_val[$key],
                                    );
            }
            // $replaceList = serialize($replaceList);
        }
        
        return $replaceList;
    }

    //对查询传入的搜索值处理
    public function tableWhereCreat($searchList)
    {
        $post = $_REQUEST;

        $whereList = array();        
        foreach ($searchList as $key => $val) {
            //传入值
            $sKey_mapping = strpos($val['field'], '.') ? str_replace('.','__',$val['field']) : $val['field'];
            $sKey = $val['field'];
            $sVal = $post[$sKey_mapping] || is_numeric($post[$sKey_mapping]) ? $post[$sKey_mapping] : null;
         
            switch ($val['type']) {
                case 'eq':
                    if(empty($sVal) && !is_numeric($sVal)) continue;

                    $whereList[] = " {$sKey} = '{$sVal}' ";
                    break;
                case 'like':
                    if(empty($sVal) && !is_numeric($sVal)) continue;

                    $whereList[] = " {$sKey} like '%{$sVal}%' ";
                    break;
                case 'between':
                
                    if(is_array($sVal)){
                        $min = $sVal['min'];
                        $max = $sVal['max'];
                    }else{
                        list($min, $max) = explode('-', $sVal);
                    }
                    
                    if ( $min && $max ) {
                        $whereList[] = " {$sKey} between '{$min}' AND '{$max}' ";
                    } elseif ( $min && empty($max) ) {
                        $whereList[] = " {$sKey} >= '{$min}' ";
                    } elseif ( empty($min) && $max ) {
                        $whereList[] = " {$sKey} <= '{$max}' ";
                    }
                    break;
                case 'in':
                    if(empty($sVal) && !is_numeric($sVal)) continue;

                    //逗号替换
                    $sVal = str_replace('，', ',', $sVal);
                    $str = array();
                    foreach(explode(",", $sVal) as $kk => $vv) {
                        $str[] = "'".$vv."'";
                    }
                    $str = implode(",", $str);

                    $whereList[] = " {$sKey} in({$str}) ";
                    break;
                case 'time':
                    if(empty($sVal) && !is_numeric($sVal)) continue;

                    list($st, $et) = explode(' - ', $sVal);
                    $st = strtotime($st);
                    $et = strtotime($et);
                    $whereList[] = " {$sKey} between '{$st}' AND '{$et}' ";
                    break;            
                case 'key_val':
                
                    if(empty($sVal) && !is_numeric($sVal)) continue;
  
                    //逗号替换
                    $sVal = str_replace('，', ',', $sVal);
                    $str = array();
                    foreach(explode(",", $sVal) as $kk => $vv) {
                        $str[] = "'".$vv."'";
                    }
                    $str = implode(",", $str);

                    $whereList[] = " {$sKey} in({$str}) ";
                    break;   
                case 'sql':
                    if(empty($sVal) && !is_numeric($sVal)) continue;

                    //逗号替换
                    $sVal = str_replace('，', ',', $sVal);
                    $str = array();
                    foreach(explode(",", $sVal) as $kk => $vv) {
                        $str[] = "'".$vv."'";
                    }
                    $str = implode(",", $str);
                    
                    $whereList[] = " {$sKey} in({$str}) ";
                    break;       
                default:
                    if(empty($sVal) && !is_numeric($sVal)) continue;

                    //逗号替换
                    $sVal = str_replace('，', ',', $sVal);
                    $str = array();
                    foreach(explode(",", $sVal) as $kk => $vv) {
                        $str[] = "'".$vv."'";
                    }
                    $str = implode(",", $str);
                    
                    $whereList[] = " {$sKey} in({$str}) ";
                    break;       
            }
        }
  
        $where = " ";
        if(count($whereList)>0){
            $where .= " WHERE ".implode(" AND ", $whereList);
        }
     
        return $where;
    }

    /** 
     * Json数据格式化
     * @param  array $data 数据
     * @param  string  array|json   默认是array
     * @param  String $indent 缩进字符，默认4个空格
     * @return JSON
     */
    public function jsonFormat($data, $type = 'array', $indent = null)
    {
        if (phpversion() >= 5.4) {
            return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        }
        if ($type == 'array') {
            // 对数组中每个元素递归进行urlencode操作，保护中文字符
            array_walk_recursive($data, 'jsonFormatProtect');
            // json encode
            $data = json_encode($data);
        }
        // 将urlencode的内容进行urldecode
        $data = urldecode($data);
        // 缩进处理
        $ret = '';
        $pos = 0;
        $length = strlen($data);
        $indent = isset($indent) ? $indent : '    ';
        $newline = "\n";
        $prevchar = '';
        $outofquotes = true;
        for ($i = 0; $i <= $length; $i++) {
            $char = substr($data, $i, 1);
            if ($char == '"' && $prevchar != '\\') {
                $outofquotes = !$outofquotes;
            } elseif (($char == '}' || $char == ']') && $outofquotes) {
                $ret .= $newline;
                $pos--;
                for ($j = 0; $j < $pos; $j++) {
                    $ret .= $indent;
                }
            }
            $ret .= $char;
            if (($char == ',' || $char == '{' || $char == '[') && $outofquotes) {
                $ret .= $newline;
                if ($char == '{' || $char == '[') {
                    $pos++;
                }
                for ($j = 0; $j < $pos; $j++) {
                    $ret .= $indent;
                }
            }
            $prevchar = $char;
        }
        $ret = $this->my_json_decode($ret);
        return $ret;
    }

    /** 
     * 将元素进行urlencode
     * @param $val String 传入元素
     */
    public function jsonFormatProtect(&$val)
    {
        if ($val !== true && $val !== false && $val !== null) {
            $val = urlencode($val);
        }
    }

    /** 
     * 去掉JSON里边key的引号
     * @param $string $val
     * @return json
     */
    function my_json_decode($str) {
        $str = preg_replace('/"(\w+)"(\s*:\s*)/is', '$1$2', $str);   //去掉key的双引号
        return $str;
    }

    //物品搜索
    public function findGoods()
    {
        $name = $_REQUEST['keyword'];
        $returnData = array('code'=>-1, 'msg'=>'关键字为空');
     
        if(empty($name)){
            echo json_encode($returnData);exit;
        } 
     
        $sql = "select id,item,attr from base_config where type=1 and (item like '%{$name}%' or id='{$name}')";
        $res = $this->admindb->fetchRow($sql);
       
        if($res){
            foreach ($res as $key => $val) {
                $attr = json_decode($val['attr'], true);
                $color = $this->goods_color[$attr['color']];
                $data[] = array(
                            'name'=>"({$val['id']})"."<t style=color:$color>".$val['item']."</t>",
                            // 'name'=>"({$val['id']})".$val['item'],
                            'value'=>$val['id'],
                            );
            }
            $returnData['msg'] = 'success';
            echo json_encode(array('code'=>0, 'msg'=>'success', 'data'=>$data));
        }else{
            $returnData['code'] = -1;
            $returnData['msg'] = '搜索结果为空';
            echo json_encode($returnData);exit;
        }
        
    }
}//class end