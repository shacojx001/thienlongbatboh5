<?php
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_kf extends action_superclass
{
    /**
     * 跨服中心
     */
    public function kf_server_list(){
        global $system_config;
        $assign = $this->get_server_rights();

        if(!$system_config['rpc_kf_server']){
            alert('未配置:rpc_kf_server');
        }
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
        $assign['list'] = $HRpc_client->getServerByPid($system_config['my_pid']);
//        $where = ' where 1';
//
//        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from kf_server $where");
//        $total_record = $total_record['total'];
//
//        $per_page = 30;
//        $total_page = ceil($total_record / $per_page);
//        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
//        $cur_page = $cur_page < 1 ? 1 : $cur_page;
//        $limit_start = ($cur_page - 1) * $per_page;
//        $limit = " LIMIT {$limit_start}, $per_page ";
//
//        $sql = "select * from kf_server $where $limit";
//        $list = $this->admindb->fetchRow($sql);
//        $assign['list'] = $list;
//        $assign['pages'] = array(
//            "curpage" => $cur_page,
//            "totalpage" => $total_page,
//            "totalnumber" => $total_record,
//            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
//        );

        $this->display('kf.shtml', $assign);
    }

    public function kf_action(){
        alert('已关闭添加功能');
        global $system_config;
        Auth::check_action(Auth::$KF_ADD);
        $data['node'] = request('node','str');
        $data['cookie'] = request('cookie','str');
        $data['db_host'] = request('db_host','str');
        $data['db_name'] = request('db_name','str');
        $data['db_pwd'] = request('db_pwd','str');
        $data['db_port'] = request('db_port','str');
        $data['db_user'] = request('db_user','str');
        $data['pid'] = 1;

        if(!$data['node']){
            alert('节点名必须');
        }
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
        $HRpc_client->editServer($data);

        //$this->admindb->insert('kf_server',$data,true);
        Admin::adminLog('编辑跨服中心:'.json_encode($data));
        alert('已提交');
    }

    /**
     * 删除跨服中心
     */
    public function del(){
        alert('已关闭删除功能!');
        Auth::check_action(Auth::$KF_DEL);
        $node = request('node','str');
        if(!$node){
            alert('缺少参数');
        }

        $sql = "select * from kf_server where node = '{$node}'";
        $re = $this->admindb->fetchOne($sql);
        if(!$re){
            alert('找不到此跨服!');
        }
        $sql = "delete from kf_server where node = '{$node}'";
        $this->admindb->query($sql);
        Admin::adminLog('删除跨服:'.$node);
        alert('删除成功!');

    }

    /**
     * 获取服列表
     */
    public function ajax_allserver_list(){
        $gid = request('gid','int');
        $re = parent::ajax_allserver_list($gid,true);
        exit($re);
    }

    /**
     * 生成跨服配置
     */
    public function kf_db_config(){
        global $system_config;
        error_reporting(0);
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
        $list = $HRpc_client->getServerByPid($system_config['my_pid']);
        //$sql = "select * from kf_server";
        //$list = $this->admindb->fetchRow($sql);
        $tmp = array();
        if($list){
            foreach ($list as $v){
                $tmp[$v['node']] = array(
                    'host'=> $v['db_host'],
                    'user'=> $v['db_user'],
                    'pwd'=> $v['db_pwd'],
                    'port'=> $v['db_port'],
                    'db'=> $v['db_name'],
                    'charset'=> 'utf8',
                    'cookie'=> $v['cookie'],
                );
            }
        }else{
            alert('生成失败');
        }
        $size = file_put_contents(CONFIG_DIR.'db_kf.php',"<?php\nreturn ".var_export($tmp,true).';');
        if(!$size){
            alert('生成配置失败!');
        }
        $size = file_put_contents(CONFIG_DIR.'db_conf_kf.php',"<?php\nreturn ".var_export($tmp,true).';');
        if(!$size){
            alert('生成配置失败!');
        }

        echo '<script>alert("生成成功!");window.location.href="/action_gateway.php?ctl=kf_super&act=kf_server_list";</script>';
        exit;
    }



}