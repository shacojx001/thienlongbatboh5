<?php
/**
 * 武侠游戏项
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_wx_info extends action_superclass
{
    //错误上报日志
    function log_client_error()
    {

        $where = " WHERE 1 ";

        $url = "action_gateway.php?ctl=wx_info&act=log_client_error";

        if ($_GET['start_time'] && $_GET['end_time']) {
            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']);

            $where = $where . " and time>$start_time and  time <$end_time";
        }

        if($_GET['records']){
            $where = $where . " and records like '%".$_GET['records']."%'";
        }
// print_r($where);
        $total_record = Config::centerdb()->fetchOne("SELECT COUNT(*) as total from log_client_error $where ");

        $total_record = $total_record['total'];


        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;

        $limit = " LIMIT {$limit_start}, $per_page ";


        // $sql = "select * from log_client_error  $orderby $limit";
        $sql = "SELECT id, time,  REPLACE (REPLACE (records,'script',''),'refresh','') as records, channel, ip,num,role_id,nick_name,level FROM log_client_error $where ORDER BY time DESC $limit  ";

        $list = Config::centerdb()->fetchRow($sql);
// print_r($list);exit;
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );

        $this->display(CENTER_TEMP . 'log_client_error.shtml', $assign);

    }


    //错误上报日志
    function delet_log_client_error()
    {
        $id = $_GET['id'];
        $sql = "SELECT
                      group_concat(id) as id_list
                  FROM
                    log_client_error
                  WHERE
                    records IN (
                      SELECT
                        records
                      FROM
                        log_client_error
                      WHERE
                        id = $id
                    )";
        $delete_list = Config::centerdb()->fetchOne($sql);

        $id_list = $delete_list['id_list'];

        $sql = " DELETE
                  FROM
                    log_client_error
                  WHERE
                    id in ($id_list)";
        Config::centerdb()->query($sql);

        alert('删除记录成功！');
    }


    //错误上报日志
    function delet_all()
    {

        $sql = "TRUNCATE log_client_error";
        Config::centerdb()->query($sql);

        alert('删除记录成功！');

    }

    // 获取渠道列表select
    public function ajax_channel_list()
    {

        if (empty($_SESSION['channel'])) {
            $str = "<option value=''>全部</option>";
        }

        $gid = $_POST['gid'];

        if (!empty($gid)) {
            if ($_SESSION['channel']) {
                $channel = $_SESSION['channel'];
                $where_channel = " and channel_str='$channel' ";
            }
            $sql = "SELECT
              gid,
              channel_name,
              channel_str
            FROM
              channel_list
            WHERE
              gid = {$gid}
              $where_channel ";
            $slist = $this->admindb->fetchRow($sql);

            if (!empty($slist)) {

                foreach ($slist as $key => $val) {
                    $str .= "<option value='{$val['channel_str']}'>{$val['channel_name']}</option>";
                }
                $json = array('flag' => 1, 'json' => $str);
                echo json_encode($json);
            }
        } else {
            $json = array('flag' => 0, 'json' => $str);
            echo json_encode($json);
        }
    }

    // 获取渠道列表Checkbox(区分权限)
    public function ajax_channel_list_checkbox()
    {
        parent::ajax_channel_list_checkbox();
    }

    // 获取渠道列表Checkbox(用户管理用的)
    public function ajax_user_channel_list_checkbox()
    {
        parent::ajax_user_channel_list_checkbox();
    }

    // 获取渠道列表radio(区分权限)
    public function ajax_channel_list_radio()
    {
        parent::ajax_channel_list_radio();
    }

    public function ips_white_list()
    {
    
        $sql = "SELECT
                ip,
                id,
                channel
              FROM
                ip_white_list where type =2 order by id desc";
        $list = $this->admindb->fetchRow($sql);

        $assign['list'] = $list;

        $this->display(CENTER_TEMP . 'ips_white_list.shtml', $assign);
    }


// 测试服IP白名单
    public function ip_white_list()
    {
        $type = $_GET['type'] ? $_GET['type'] : 1;
        $header = array();
        switch ($type) {
            case 1://账号
                $header = array('ID','账号','渠道','操作');
                $fields = 'id,ip,channel';
                $sql = "SELECT
                        channel_str,
                        channel_name
                      FROM
                        channel_list";
                $channel_list = $this->admindb->fetchRow($sql);
                break;
            case 2://IP白名单
                $header = array('ID','白名单IP','操作');
                $fields = 'id,ip';
                break;  
            case 3://IP黑名单
                $header = array('ID','黑名单IP','操作');
                $fields = 'id,ip';
                break;  
            case 4://设备黑名单
                $header = array('ID','黑名单设备','操作');
                $fields = 'id,ip';
                break;            
        }
        $typeList = array(
                    1=>'账号白名单',
                    2=>'IP白名单',
                    3=>'IP黑名单',
                    4=>'设备黑名单',
                    );

        $sql = "SELECT
                $fields
              FROM
                ip_white_list  where type = $type order by id desc ";
        $list = $this->admindb->fetchRow($sql);

        $assign['type'] = $type;
        $assign['header'] = $header;
        $assign['typeList'] = $typeList;
        $assign['list'] = $list;
        $assign['channel_list'] = $channel_list;

        $this->display(CENTER_TEMP . 'ip_white_list.shtml', $assign);
    }

// 添加IP白名单
    public function add_ip_white_list()
    {
        $type = request('type');
        $ip = trim($_REQUEST['ip']);
        $channel = $_REQUEST['channel'] ? $_REQUEST['channel'] : '';
        $type = $_REQUEST['type'];
        
        if(empty($ip)){
            alert("输入值不能为空！");
            exit;
        }
        if($type==1){
            if(empty($channel)){
                alert("必须选择渠道！");
                exit;
            }
        }

        $sql = "INSERT INTO
              ip_white_list (ip,channel,type)
            VALUES
              ('$ip','$channel',$type) ";
        $this->admindb->query($sql);

        alert("添加成功，请生成配置才能生效！");
        exit;
    }

    // 删除IP白名单
    public function detele_ip_white_list()
    {

        if (empty($_GET['id'])) {
            alert("ID不能为空");
            exit;
        }

        $id = $_GET['id'];
        $sql = "DELETE
              FROM
                ip_white_list
              WHERE
                id = $id ";
        $this->admindb->query($sql);

        alert("删除成功，请生成配置才能生效！");
        exit;
    }

    // 生成IP白名单配置
    public function create_ip_white_list()
    {
        Config::create_ip_white_list();
    }

    /////监控
    function log_monitor()
    {
        //求每个主机IP上的游戏服

        $where = " where 1 and ip!=''";

        //默认时间为当前时间到12小时前
        if ($_GET['start_time'] && $_GET['end_time']) {

            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']);

        } else {
            $start_time = time();
            $end_time = time() - 43200;

            $_GET['start_time'] = date('Y-m-d H:i:s', $start_time);
            $_GET['end_time'] = date('Y-m-d H:i:s', $end_time);
        }


        $where .= " AND time between $end_time  and $start_time ";

        $sql = "SELECT
                    server_num,
                    all_server_num,
                    ip
                  FROM
                    (
                      SELECT
                        *
                      FROM
                        log_monitor
                      $where
                      ORDER BY
                        id DESC
                    ) AS a
                  GROUP BY
                    ip";
        $res1 = $this->admindb->fetchRow($sql);

        $server_num = array();
        $all_server_num = array();

        foreach ($res1 as $key => $val) {
            $server_num[$val['ip']] = $val['server_num'];
            $all_server_num[$val['ip']] = $val['all_server_num'];
        }

        $sql = "SELECT
                  ip,
                  FROM_UNIXTIME(time, '%H:%i') AS time,
                  `load`,
                  online_num,
                  online_top_num,
                  server_num,
                  all_server_num
                FROM
                  log_monitor
                $where
                GROUP BY
                  ip,
                  time
                ORDER BY
                  id ASC";
        $res = $this->admindb->fetchRow($sql);

        $list = array();

        foreach ($res as $key => $val) {
            $list[$val['ip'] . '(' . $server_num[$val['ip']] . '服数)(' . $all_server_num[$val['ip']] . ')']['load'][] = (float)$val['load'];
            $list[$val['ip'] . '(' . $server_num[$val['ip']] . '服数)(' . $all_server_num[$val['ip']] . ')']['online_num'][] = (int)$val['online_num'];
            $list[$val['ip'] . '(' . $server_num[$val['ip']] . '服数)(' . $all_server_num[$val['ip']] . ')']['online_top_num'][] = (int)$val['online_top_num'];
            $list[$val['ip'] . '(' . $server_num[$val['ip']] . '服数)(' . $all_server_num[$val['ip']] . ')']['server_num'][] = $val['server_num'];
            $list[$val['ip'] . '(' . $server_num[$val['ip']] . '服数)(' . $all_server_num[$val['ip']] . ')']['time'][] = $val['time'];

            $list[$val['ip'] . '(' . $server_num[$val['ip']] . '服数)(' . $all_server_num[$val['ip']] . ')']['stime'][] = $val['stime'];

        }

        $ip_list = array();
        foreach ($list as $key => $val) {
            $ip_list[] = $key;
            $list[$key]['load'] = json_encode($val['load']);
            $list[$key]['online_num'] = json_encode($val['online_num']);
            $list[$key]['online_top_num'] = json_encode($val['online_top_num']);
            if (empty($time)) {
                $time = json_encode($val['time']);
            }

        }


        $assign['list'] = $list;
        $assign['time'] = $time;
        $assign['start_time'] = date('Y-m-d H:i:s', $start_time);
        $assign['end_time'] = date('Y-m-d H:i:s', $end_time);

        $this->display(CENTER_TEMP . 'log_monitor.shtml', $assign);

    }

    /**
     * 服务端错误日志下载
     */
    function erl_err_log()
    {
        global $base_info;

        $game_domain = $base_info['game_domain'];
        if (!$game_domain) {
            exit("<h1>base_game表配置错误!请联系php管理员!</h1>");
        }

        $game_domain = rtrim($game_domain, '/');
        $game_domain .= '/erl_err_log.php';
// print_r($game_domain);exit;
        $gets = Http::httpGet($game_domain);
// print_r($gets);exit;
        if (substr($gets, 0, 12) == 'curl_status_' || substr($gets, 0, 12) == 'http_status_') {
            exit($gets);
        }
        $gets = json_decode($gets, true);
        if (empty($gets)) exit("<h1>no err log files exist!</h1>");

        $files = array();
        $data = array();
        foreach ($gets as $get) {
            $files[] = $get['file'];
            $data[$get['file']] = $get;
        }
        sort($files);
        $post = array();
        foreach ($files as $key => $val) {
            $post[$key] = array(
                    'id' => $key + 1,
                    'file'=>$val,
                    'size'=>sprintf('%.2f',$data[$val]['size']/1024).'kb',
                        );
        }
        // print_r($post);exit;
        $assign['post'] = json_encode($post);
// print_r($post);exit;        
        // foreach ($files as $v) {
        //     $size = sprintf('%.2f',$data[$v]['size']/1024);
        //     echo "<a href='?ctl=wx_info&act=erl_err_log_down&file={$data[$v]['file']}'>{$data[$v]['file']}</a>&nbsp; {$size}KB<br>";
        // }

        $this->display('wx_info/erl_err_log.shtml', $assign);
    }

    /**
     * 服务端错误日志下载
     */
    public function erl_err_log_down()
    {
        global $base_info;
        $file = request('file', 'str');
        if (!$file) alert('非法!');
        $game_domain = $base_info['game_domain'];
        $game_domain = rtrim($game_domain, '/');

        $api = $game_domain . '/erl_err_log.php?file=' . $file;
        $re = Http::httpGet($api);
        if ($re != 'success') {
            exit($re);
        }
        $log_file = $game_domain . '/tmp/' . $file;

        if(request('show')==1){
            $assign['title'] = $file;
            $assign['code'] = file_get_contents($log_file);
            $this->display('pre.shtml', $assign);
            exit;
        }
        
        //打开文件
        $filesize = @filesize($log_file);
    
        $handle = fopen($log_file, "rb");
        if(!$handle) exit('文件不可读:'.$log_file);
        //输入文件标签
        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length: " . $filesize);
        Header("Content-Disposition: attachment; filename=" . $file);
        //输出文件内容
        //读取文件内容并直接输出到浏览器
        while (!feof($handle)) {
            $contents = fread($handle, 8192);
            echo $contents;
            @ob_flush();  //把数据从PHP的缓冲中释放出来
            flush();      //把被释放出来的数据发送到浏览器
        }
        fclose($handle);
        exit();
    }

    /**
     * 重启游戏服
     */
    public function restart_game_server()
    {

        //获取服务器列表权限
        $assign = $this->get_server_rights();


        $url = "?ctl=wx_info&act=restart_game_server";
        $where = " WHERE 1 ";

        $total_record = $this->admindb->fetchOne("SELECT COUNT(*) as total FROM auto_server_start $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by a.id desc ";

        $sql = "SELECT
                a.*, l.description,
                l.sname
              FROM
                auto_server_start a
              LEFT JOIN adminserverlist l ON a.sid = l.id
              $where $orderby $limit";
        $list = $this->admindb->fetchRow($sql);

        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );

        $this->display('restart_game_server.shtml', $assign);
    }

    // 新增重启游戏服
    function restart_game_server_add()
    {
        Auth::check_action(Auth::$RESTART_GAME_SERVER_ADD);
        $gid = request('gid', 'int');
        $sid = request('sid', 'int');

        if (empty($gid) || empty($sid)) {

            alert("请选择游戏服");
            exit;

        }//查询游戏服数据
        $sql = "SELECT
                id AS sid,
                REPLACE (
                  LEFT (url, length(url)-1),
                  'http://',
                  ''
                ) AS url,
                game_ip AS ip,
                otime
              FROM
                adminserverlist
              WHERE
                id = $sid ";
        $info = $this->admindb->fetchOne($sql);

        $time = time();

        //如果游戏服时间大于当前时间则不允许执行
        if ($info['otime'] <= $time) {
            alert("该游戏服已开服，不允许执行此操作，请找运维！");
            exit;
        }

        $save = array(
            'gid'=>$gid,
            'sid'=>$sid,
            'url'=>$info['url'],
            'ip'=>$info['ip'],
            'addtime'=>$time,
        );

        $res = $this->restart_server_add($save);
        if (!$res) {//有未执行，或者未跑完的
            alert("添加失败! 可能仍然有未执行或者未跑完的任务！");
            exit;
        }

        alert("操作成功");
    }

    //删除游戏服定时重启游戏服
    function delete_restart_game_server()
    {
        Auth::check_action(Auth::$RESTART_GAME_SERVER_DEL);
        $id = request('id', 'int');

        if (empty($id)) {
            alert("请选择选项");
            exit;
        }

        $sql = "DELETE from auto_server_start where id=$id;";
        $this->admindb->query($sql);

        alert("删除成功！");
    }

    //停止游戏服定时重启游戏服
    function stop_restart_game_server()
    {

        Auth::check_action(Auth::$RESTART_GAME_SERVER_STOP);
        $id = request('id', 'int');

        if (empty($id)) {
            alert("请选择选项");
            exit;
        }

        $sql = "update auto_server_start set finishtime =1  where id=$id;";
        $this->admindb->query($sql);

        alert("手动结束成功！");
    }

    // 定时重启游戏服定时重启游戏服
    function create_server_start()
    {
        Auth::check_action(Auth::$RESTART_GAME_SERVER);     //验证权限

        $id = request('id', 'int');
        if (empty($id)) {
            alert('ID为空');
            exit;
        }

        $re = $this->restart_server_action($id);
        if(!$re){
            alert('重启失败');
        }

        alert('重启中，5分钟后再进行下一次重启');

    }

    //重启游戏服日志
    public function get_restart_game_server_log()
    {

        $start_line = request('start_line', 'int');
        //erl编译标识文件 和 编译日志文件
        $log_dir = LOG_DIR . 'restart_game_server_log.txt';

        if (is_file($log_dir)) {
            $arr = getFileLines($log_dir, $start_line, 1000);
            $line = $arr['line'];
            $serverlog = $arr['content'];
        }
        echo json_encode(array('serverlog' => nl2br($serverlog), 'line' => $line));
        exit;
    }

    //服务端白名单
    public function server_ip_white(){
        
        $this->display('wx_info/server_ip_white.shtml', $assign);
    }

    public function server_ip_white_ajax(){
        $sid = $_SESSION['selected_sid'];
        $sql = "select * from base_white_accname order by accname asc";
        $list = Config::gamedb($sid)->fetchRow($sql);
        echo $this->echo_lay_json(0, 'ok', $list);
    }

    public function server_ip_white_add(){
// print_r($_REQUEST);exit;        
        $accname = trim($_REQUEST['accname']);
        $ip = trim($_REQUEST['ip']);
        if(empty($accname) || empty($ip)){
            echo '有参数为空！';exit;
        }
        $sid = $_SESSION['selected_sid'];
        $res = Erlang::add_white_accname($sid, $accname, $ip);
        echo $res;exit;
    }

    public function server_ip_white_delete(){
        $accname = trim($_REQUEST['accname']);
        // $ip = $_REQUEST['ip'];
        if(empty($accname) ){
            echo '有参数为空！';exit;
        }
        $sid = $_SESSION['selected_sid'];
        $res = Erlang::del_white_accname($sid, $accname);
        echo $res;exit;
    }

    public function server_ip_white_delete_all(){
        $data = json_decode($_REQUEST['data'],true);
        $sid = $_SESSION['selected_sid'];
        foreach ($data as $key => $val) {
            $res = Erlang::del_white_accname($sid, $val['accname'], $val['ip']);
        }
        echo $res;exit;
    }


}