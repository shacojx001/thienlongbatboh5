<?php
/**
 * 头像审核
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_show extends action_superclass
{
    
//头像审核功能(专服)
    public function check_head_all_plaft()
    {

        $gid_list = $this->get_allgroup_list();

        $sql = "select id,description from adminserverlist";
        $res = $this->admindb->fetchRow($sql);
        $sid_list = array();
        foreach ($res as $key => $val) {
            $sid_list[$val['id']] = $val['description'];
        }
        $sid = $_POST['sid'] ? implode(',', $_POST['sid']) : '';
        $state = empty($_REQUEST['state']) ? 2 : $_REQUEST['state'];
        $gid = $_REQUEST['gid'];

        $str = "&gid=$gid&sid=$sid&state=$state&role_id=".$_REQUEST['role_id'];

        $assign['gid_list'] = $gid_list;
        $assign['state'] = $state;
        
        $assign['url'] = 'action_gateway.php?ctl=show&act=get_data&'.$str;
        $assign['get_slist'] = $gid ? $this->get_slist(true) : '';
        $assign['web'] = PIC_SHOW;

        $this->display('check_head_all_group.shtml',$assign);
    }

    //erlang处理
    public function photo_erlang()
    {
        // print_r($_REQUEST);exit;
        $type = $_REQUEST['type'];
        $role_id = explode(',', $_REQUEST['role_id']);
        $str = $_REQUEST['role_id'];
        $sql = "select sid,role_id,url from center_role_photo where role_id in($str) ";
        $res = $this->admindb->fetchRow($sql);
        $result = array();
        foreach ($res as $key => $val) {
            //通知服务端
            $msg = Erlang::erl_user_icon($val['sid'], $val['role_id'], $type);  
            $result[$val['role_id']] = $msg == 0 ? 'ok' : json_encode($msg);
    // print_r($result[$val['role_id']]);exit;  
            // $result[$val['role_id']] = 'ok';
        }
        $when = array();
        foreach ($result as $key => $val) {
            $when[] = "WHEN '{$key}' THEN '{$val}' ";
        }

        $whereIn = implode(',', array_keys($result));
        $when = implode(' ', $when);

        // $state = 
        $sql = "UPDATE center_role_photo SET msg = CASE role_id $when END,state = $type WHERE role_id in($whereIn)";
     
        $this->admindb->query($sql);
        return $msg;
    }

    //获取数据
    public function get_data()
    {
       
        $gid = $_REQUEST['gid'];
        if($gid){
            $where = " where gid=".$gid." and state=".$_REQUEST['state'];
            
            if($_REQUEST['sid']){
                $sidStr = $_REQUEST['sid'];
                $where .= " and sid in($sidStr)";
            }
            if($_REQUEST['role_id']){
                $role_id = $_REQUEST['role_id'];
                $where .= " and role_id = $role_id";
            }

            $num = $_REQUEST['limit'];
            $page = $_REQUEST['page'];
            $limit = " LIMIT ". ($page-1) * $num .",".$num;

            $sql = "select count(1) num from center_role_photo $where ";
            $num = $this->admindb->fetchOne($sql);
            $sql = "select gid,sid,role_id,state,FROM_UNIXTIME(ctime) ctime,url,msg from center_role_photo $where order by ctime desc $limit ";
            $data = $this->admindb->fetchRow($sql);

        }else{
            $data = array();
        }

        $data = array('code'=>0, 'msg'=>'ok', 'count'=>$num['num'], 'data'=>$data);
        echo json_encode($data);
    }

    public function get_slist($type = false)
    {
        $gid = $_REQUEST['gid'];

        $sql = "select id,s_num from adminserverlist where gid=$gid";
        $res = $this->admindb->fetchRow($sql);

        $str = array();

        foreach ($res as $key => $val) {
            $checked = $_REQUEST['sid'] && in_array($val['id'], $_REQUEST['sid']) ? 'checked' : '';
            $str[] = "<label><input type='checkbox' name='sid[]' value='{$val['id']}' $checked>  {$val['s_num']}</label>";
        }
        $str = implode(' ', $str);
        if($type == true){
            return $str;
        }else{
            echo $str;
        }
        
    }



}