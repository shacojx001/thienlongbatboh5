<?php

class action_superclass
{
    public $smarty = null;
    public $db = null;

    /**
     * @var Ext_Mysql
     */
    public $admindb = null;
    public static $erlconfig;
    public static $auto_rights;
    public static $server_by_gid_data;
    public static $all_server_list_data;

    public $goldtype = array();

    public $server_color = array(
        0 => '#e2a5a5',
        1 => '#5cb85c',
        4 => '#f0ad4e',
        6 => '#b9b5b5',
        10=> '#ffffff',
    );

    public $goods_color = array(
        1 => '#0003',  //白
        2 => '#5fc934',//绿
        3 => '#3171f5',//蓝
        4 => '#a230e3',//紫
        5 => '#d5722d',//橙
        6 => '#db4734',//红
    );

    public $base_config_list = array(
        '1' => '1物品表',
        '2' => '2操作类型表',
        '3' => '3礼包表',
        '4' => '4跨服活动',
        '5' => '5运营活动',
        '6' => '6跨服运营活动',
        '7' => '7日志列表',
        '8' => '8 GM工具',
        '9' => '9 称号列表',
        '10' => '10 商店类型',
        '11' => '11 货币类型',
        '12' => '12 充值元宝类型',
        '13' => '13 背包类型',
    );

    public function __construct()
    {
        global $smarty;
        $this->db = Ext_Mysql::getInstance();
        $this->admindb = Ext_Mysql::getInstance('admin');
        $this->smarty = $smarty;
    }

    public function display($temp, $assign)
    {
        $this->smarty->assign($assign);
        $this->smarty->display($temp);
    }

    /**
     * 连接sqlite3
     */
    public function sqlitedb($db = 'Mysqlite.db')
    {
        $db = LUA_SQLITE_DIR . $db . '.db';
        return Ext_Sqlite::getInstance($db);
    }

    /**
     * 获取服务器组权限
     * @return array
     */
    protected function get_server_rights()
    {
        $sid = request('sid', 'int');
        $gid = request('gid', 'int');

        $assign = array();
        //获取查询的平台ID
        $assign['gid'] = $gid;
        //获取查询的服务器ID
        $assign['sid'] = $sid;
        //获取当前渠道
        $assign['channel'] = $_SESSION['channel'];

        //返回所有平台
        $assign['srv_group'] = $this->get_allgroup_list();
        //返回：服务器ID=>服务器名
        $assign['srv_server'] = $this->get_allserver_list($gid);
        //返回 渠道列表
        $assign['channel_list'] = $this->get_channel_list($gid);
        return $assign;
    }

    /**
     * 获取渠道列表
     *
     * @param string $gid
     * @return array
     */
    public function get_channel_list($gid = '')
    {
        $channel_list = array();

        $where = ' where 1';
        if ($_SESSION['channel']) {
            $channel_arr = array();
            foreach ($_SESSION['channel'] as $v) {
                $channel_arr[] = "'" . $v . "'";
            }
            $str = implode(",", $channel_arr);
            $where .= " and channel_str in($str) ";
        }
        if ($gid) {
            $where .= " and gid in ({$gid})";
        }

        $sql = "SELECT gid, channel_name, channel_str FROM channel_list $where group by channel_str";
        $res = $this->admindb->fetchRow($sql);
        foreach ($res as $key => $val) {
            $channel_list[$val['channel_str']] = $val['channel_name'];
        }

        return $channel_list;

    }


    /**
     * 获取专服列表
     * @param string $gid
     * @param bool $not
     * @param bool $all
     * @return array
     */
    public function get_allgroup_list($all = false)
    {
        $where = " where 1 ";

        if ($_SESSION['gids']) {
            $where .= " and gid in (" . implode(',', $_SESSION['gids']) . ")";
            if ($_SESSION['channel']) {
                $channel_arr = array();
                foreach ($_SESSION['channel'] as $v) {
                    $channel_arr[] = "'" . $v . "'";
                }
                $where .= " and channel_str in(" . implode(',', $channel_arr) . ") ";
            }
        }

        $sql = "SELECT gid FROM channel_list $where order by id asc";
        $res = $this->admindb->fetchRow($sql);

        if ($res)
            foreach ($res as $key => $val) {
                $gids[$val['gid']] = $val['gid'];
            }

        if ($gids) {
            $str = implode(',', $gids);
            $condition = " and gid in ($str) ";
        } else {
            $condition = " ";
        }

        $sql = "select gid,platform_name as gname from adminplatformlist where 1 $condition order by gid";

        $grouplist = $this->admindb->fetchRow($sql);

        $glist = array();
        if ($all) {
            $glist[0] = '全部';
        }
        if (!empty($grouplist)) {
            foreach ($grouplist as $key => $val) {
                $glist[$val['gid']] = $val['gname'];
            }
        }
        return $glist;
    }

    /**
     * 通过渠道获取游戏服
     * @param string $orderby
     * @param string $type
     * @param bool $flag
     * @return mixed
     */
    public function get_allserver_bygid($orderby = 'DESC', $type = 'sname', $flag = false)
    {
        $where = " where 1 ";
        if ($flag) {
            $where .= " and `default` in (0,1,4) ";
        }

        if ($_SESSION['gids']) {
            $where .= " and gid in (" . implode(',', $_SESSION['gids']) . ")";
            if ($_SESSION['channel']) {
                foreach ($_SESSION['channel'] as $key => $val) {
                    if (strpos($where, "FIND_IN_SET")) {
                        $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                    } else {
                        $where .= " AND  (FIND_IN_SET('$val', channel_list) ";
                    }
                }
                $where .= ')';
            }
        }

        $sql = "select id,gid,sname,id,sdb,description from adminserverlist $where order by id $orderby";
        $serverlist = Config::admindb()->fetchRow($sql);
        $slist = array();
        if (!empty($serverlist)) {
            foreach ($serverlist as $key => $val) {
                $slist[$val['gid']][$val['id']] = $val[$type];
            }
        }
        return $slist;
    }


    /**
     * 获取服务器列表
     * 返回 id->name
     * @param string $gid
     * @param string $sid
     * @param string $status
     * @return mixed
     */
    public function get_allserver_list($gid = 0, $whereExt = '')
    {
        $where = " WHERE `default` in (0,1,4) ". $whereExt;
        if ($_SESSION['channel']) {
            foreach ($_SESSION['channel'] as $key => $val) {
                if (strpos($where, "FIND_IN_SET")) {
                    $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                } else {
                    $where .= " AND  (FIND_IN_SET('$val', channel_list) ";
                }
            }
            $where .= ')';
        }

        if ($gid) {
            $where .= " and gid in ({$gid})";
        }
        $sql = "select gid,sname,id,sdb from adminserverlist $where order by gid,otime asc";
        $serverlist = $this->admindb->fetchRow($sql);
        $slist = array();
        if (!empty($serverlist)) {
            foreach ($serverlist as $key => $val) {
                $slist[$val['id']] = $val['sname'];
            }
        }
        return $slist;
    }

    public function server_list_for_header($gid,$flag = false){
        $html = '';
        $where = '1';
        if($gid){
            $where .= ' and gid = '.$gid;
        }
        if ($_SESSION['channel'] && $_SESSION['super'] != 'YES') {
            foreach ($_SESSION['channel'] as $key => $val) {
                if (strpos($where, "FIND_IN_SET")) {
                    $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                } else {
                    $where .= " AND ( FIND_IN_SET('$val', channel_list) ";
                }
            }
            $where .= ')';
        }
        $rst = Model::get_server_list('id,sname,description,main_id,srv_num,`default`,otime',$where,'id desc');
        
        $today = strtotime(date("Y-m-d"));
        $tomorrow = $today + 86400;
        $yesterday = $today - 86400;
        $postnatal = $today + 86400*2;

        if($rst){
            foreach ($rst as $v) {
                $color = '';
                if($this->server_color[$v['default']]){
                    $color = $this->server_color[$v['default']];
                }
                if($v['otime']>time()){//未到开服时间
                    $color = 10;
                }

                $isTodya = '';
                if($v['otime']>$today && $v['otime']<$tomorrow){//今天新服
                    $isTodya = "<b style='color:red;'>今</b>";
                }elseif($v['otime']>$yesterday && $v['otime']<$today){
                    $isTodya = "<b style='color:#ef6565;'>昨</b>";
                }elseif($v['otime']>$tomorrow && $v['otime']<$postnatal){
                    $isTodya = "<b style='color:#efc2c2;'>明</b>";
                }

                $main = '';
                $hefu = '';
                if($v['main_id'] >0 && $v['srv_num'] > 0){
                    $hefu = "[合{$v['main_id']}]";
                    $main = $v['main_id'];
                }
                if($v['main_id'] == 0 && $v['srv_num'] > 0){
                    $hefu = '[合]';
                }

                $class = $_SESSION['selected_sid']==$v['id'] ? 'bg-blue' : '';

                $html .= "<button data-main='{$main}' data-sid='{$v['id']}' type='button' class='js-sid btn btn-default btn-xs $class' style='background-color: {$color};float:left;margin-right:10px;margin-bottom: 4px;'>{$v['id']}{$hefu} {$isTodya}</button>";

                $html2 .= "<button data-main='{$main}' data-sid='{$v['id']}' type='button' class='js-sid btn btn-default btn-xs $class' style='background-color: {$color};float:left;margin-right:10px;margin-bottom: 4px;'>{$v['description']}{$hefu} {$isTodya}</button>";
            }
        }
        if($flag){
            return array('sidList'=>$html, 'nameList'=>$html2);
        }
        return json_encode(array('flag'=>true,'json'=>$html,'json2'=>$html2));
    }

    /**
     * AJAX接口返回前端option
     * @param $gid
     * @param string $status
     * @return string
     */
    public function ajax_allserver_list($gid,$flag=false)
    {
        $str = "<option value=''>全部</option>";
        if($flag) $str = "";
        if ($gid > 0) {
            $condition = " and `default` in(0,1,4) ";
            if ($_SESSION['channel']) {
                $where = '';
                foreach ($_SESSION['channel'] as $key => $val) {
                    if (strpos($where, "FIND_IN_SET")) {
                        $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                    } else {
                        $where .= " AND ( FIND_IN_SET('$val', channel_list) ";
                    }
                }
                $where .= ')';
            }

            $sql = "select sname,id from adminserverlist where gid = '$gid' $condition $where order by gid,otime asc";
            // print_r($sql);
            $slist = $this->admindb->fetchRow($sql);
            if (!empty($slist)) {
                foreach ($slist as $key => $val) {
                    $str .= "<option value='{$val['id']}'>{$val['sname']}</option>";
                }
                $json = array('flag' => 1, 'json' => $str);
                return json_encode($json);
            } else {
                $json = array('flag' => 0, 'json' => $str);
                return json_encode($json);
            }
        } else {
            $json = array('flag' => 0, 'json' => $str);
            return json_encode($json);
        }
    }

    /**
     * 用户管理
     * 获取渠道列表Checkbox
     */
    public function ajax_user_channel_list_checkbox()
    {
        $str = '';
        $gid = request('gid', 'str');
        $uid = request('uid', 'int');
        if ($gid) {
            $slist = $this->get_channel_list($gid);
            $channel_list = array();
            if ($uid) {
                $sql = "SELECT channel FROM adminuser WHERE uid = $uid";
                $res = $this->admindb->fetchOne($sql);
                $res2 = explode(',', $res['channel']);
                foreach ($res2 as $key => $val) {
                    $channel_list[$val] = $val;
                }
            }
            if ($slist) {
                foreach ($slist as $key => $val) {
                    if (in_array($key, $channel_list)) {
                        $str .= "<label><input name='channel[]' type='checkbox' value='{$key}' checked  />&nbsp;&nbsp;{$val}({$key})&nbsp;&nbsp;</label>";
                    } else {
                        $str .= "<label><input name='channel[]' type='checkbox' value='{$key}' />&nbsp;&nbsp;{$val}({$key})&nbsp;&nbsp;</label>";
                    }
                }
                $json = array('flag' => 1, 'json' => $str);
                exit(json_encode($json));
            }

        } else {
            $json = array('flag' => 0, 'json' => $str);
            exit(json_encode($json));
        }
    }

    /**
     * 游戏传闻用
     * 获取渠道列表Checkbox
     */
    public function ajax_rumor_channel_list_checkbox()
    {
        $str = '';
        $gid = request('gid', 'str');
        if ($gid) {
            $slist = $this->get_channel_list($gid);
            if ($slist) {
                foreach ($slist as $key => $val) {
                    $str .= "<input name='channel[]' type='checkbox' value='{$key}' />{$val}({$key})&nbsp;&nbsp;";
                }
                $json = array('flag' => 1, 'json' => $str);
                exit(json_encode($json));
            }

        } else {
            $json = array('flag' => 0, 'json' => $str);
            exit(json_encode($json));
        }
    }

    /**
     * 发邮件用
     * 获取渠道列表radio(区分权限)
     */
    public function ajax_channel_list_radio()
    {
        $gid = request('gid', 'int');
        $sid = request('sid', 'int');
        $str = "";
        if ($gid && !$sid) {
            $slist = $this->get_channel_list($gid);
            if ($slist) {
                foreach ($slist as $k => $v) {
                    $str .= "<input name='channel' type='radio' value='{$k}' />{$v}&nbsp;&nbsp;";
                }
            }
            $json = array('flag' => 1, 'json' => $str);
            exit(json_encode($json));
        } elseif ($gid && $sid) {
            if ($_SESSION['channel']) {
                $channel_arr = array();
                foreach ($_SESSION['channel'] as $v) {
                    $channel_arr[] = "'" . $v . "'";
                }
                $channel = implode(",", $channel_arr);
                $where_channel = " and channel_str in($channel) ";
            } else {
                $str = "<label><input name='channel' type='radio' value='all' />&nbsp;&nbsp;全部&nbsp;&nbsp;</label>";
            }
            $sql = "SELECT gid, channel_name, channel_str FROM channel_list WHERE gid = {$gid} $where_channel ";
            $slist = $this->admindb->fetchRow($sql);
            $sql = "SELECT channel_list FROM adminserverlist WHERE id = $sid";
            $res = $this->admindb->fetchOne($sql);
            $channel_list = explode(",", $res['channel_list']);
            foreach ($slist as $key => $val) {
                if (in_array($val['channel_str'], $channel_list)) {
                    $str .= "<label><input name='channel' type='radio' value='{$val['channel_str']}' />&nbsp;&nbsp;{$val['channel_name']}({$val['channel_str']})&nbsp;&nbsp;</label>";
                }
            }
            $json = array('flag' => 1, 'json' => $str);
            exit(json_encode($json));
        } else {
            $json = array('flag' => 0, 'json' => $str);
            exit(json_encode($json));
        }
    }

    /**
     * 获取渠道可见的服务器列表
     * @param 
     * @param string $status
     * @return string
     */
    public function channel_allserver_list($channel = '')
    {
        $data = array();
        if ($channel) {
            $condition = " and `default` in(1,4,6) ";
            if ($channel) {
                $where = " AND FIND_IN_SET('$channel', channel_list) ";
            }

            $sql = "select * from adminserverlist where 1 $condition $where order by otime asc,id asc";
            // print_r($sql);exit;
            $data = $this->admindb->fetchRow($sql, 'id');   
        }
        return $data;
    }

    /**
     * 服务器列表用、开服申请用
     * 获取渠道列表Checkbox
     */
    public function ajax_channel_list_checkbox()
    {
        $gid = request('gid', 'int');
        $sid = request('sid', 'int');
        $str = " ";
        if ($gid && $sid) {
            $sql = "SELECT gid, channel_name, channel_str FROM channel_list WHERE gid = {$gid}";
            $slist = $this->admindb->fetchRow($sql);
            $sql = "SELECT channel_list FROM adminserverlist WHERE id = $sid";
            $res = $this->admindb->fetchOne($sql);
            $channel_list = explode(",", $res['channel_list']);
            foreach ($slist as $key => $val) {
                if (in_array($val['channel_str'], $channel_list)) {
                    $str .= "<label><input name='channel[]' type='checkbox' value='{$val['channel_str']}' checked /> {$val['channel_name']}({$val['channel_str']})&nbsp;&nbsp;</label>";
                } else {
                    $str .= "<label><input name='channel[]' type='checkbox' value='{$val['channel_str']}' /> {$val['channel_name']}({$val['channel_str']})&nbsp;&nbsp;</label>";
                }
            }
            $json = array('flag' => 1, 'json' => $str);
            exit(json_encode($json));
        } elseif ($gid && !$sid) {
            $sql = "SELECT gid, channel_name, channel_str FROM channel_list WHERE gid = {$gid}";
            $slist = $this->admindb->fetchRow($sql);
            foreach ($slist as $key => $val) {
                $str .= "<label><input name='channel[]' type='checkbox' value='{$val['channel_str']}' /> {$val['channel_name']}({$val['channel_str']})&nbsp;&nbsp;</label>";
            }
            $json = array('flag' => 1, 'json' => $str);
            exit(json_encode($json));
        } else {
            $json = array('flag' => 0, 'json' => $str);
            exit(json_encode($json));
        }
    }

    public function get_allserver_list_open($gid = '', $not = false, $sid = false)
    {
        if ($gid != '') {
            if ($not) {
                $condition = " and gid not in($gid) ";
            } else {
                $condition = " and gid in($gid) ";
            }
        } elseif ($sid) {
            $condition = " and id in($sid) ";
        } else {
            $condition = "";
        }
        if (!empty($_SESSION['sub_pid'])) {
            $condition .= " and sub_pid = '{$_SESSION['sub_pid']}'";
        } else {
            $condition .= " and sub_pid = 0";
        }
        $sql = "select gid,sname,id,sdb from adminserverlist where `default` = 1 $condition order by gid,otime asc";
        $serverlist = $this->admindb->fetchRow($sql);
        $slist = array();
        if (!empty($serverlist)) {
            foreach ($serverlist as $key => $val) {
                $slist[$val['id']] = $val['sname'];
            }
        }
        return $slist;
    }

    public function get_server_bydb($sdb, $status = 5)
    {
        $sql = "select id as sid,gid,otime from adminserverlist where `default`<$status and sdb='$sdb'";
        return Ext_Mysql::getInstance('admin')->fetchOne($sql);
    }

    public function get_server_byid($id, $status = 5)
    {
        $sql = "select id as sid,gid,otime from adminserverlist where `default`<$status and id='$id'";
        return Ext_Mysql::getInstance('admin')->fetchOne($sql);
    }

    /**
     * 获取监控配置
     */
    public function get_monitor_config($type = "all")
    {
        $where = " where 1";
        $sql = "select * from monitor_config $where";
        $info = $this->admindb->fetchRow($sql);
        $clist = array();
        if (!empty($info)) {
            foreach ($info as $key => $val) {
                if (strpos($val['data'], ',') !== false) {
                    $darr = explode(',', $val['data']);
                    foreach ($darr as $k => $v) {
                        $clist[$val['name'] . '_' . $k] = $v;
                    }
                } else {
                    $clist[$val['name']] = $val['data'];
                }
            }
        }
        if ($type == "all") {
            return $clist;
        } else {
            return $clist[$type];
        }
    }


    public function get_sign_type_for_count($server_id)
    {
//        $sql = "select source from log_login group by source";
//        $sinfo = $this->db->fetchRow($sql);
//        $signarr = array();
//        foreach ($sinfo as $key => $val) {
//            if ($val['source'] == '') continue;
//            $signarr[$val['source']] = $val['source'];
//        }
//        return $signarr;
        $where = 'id =' . $server_id;
        $sql = "select channel_list from adminserverlist where " . $where;
        $res = $this->admindb->fetchOne($sql);
        Ext_Debug::log($res);
        $sinfo = explode(',', $res['channel_list']);
        Ext_Debug::log($sinfo);
        $signarr = array();
        foreach ($sinfo as $key => $val) {
            $signarr[$val] = $val;
        }
        return $signarr;
    }


    protected function create_lua_array2($DataArr)
    {
        if ($DataArr == "[]") {
            return "{}";
        } else {
            return "{" . str_replace(";", ",", $DataArr) . "}";
        }
    }

    // [{A, B}, {C, D}] -> {{A, B}, {C, D}}
    protected function create_lua_array3($DataArr)
    {
        if (empty($DataArr)) {
            return "{}";
        }
        return str_replace("[", "{", str_replace("]", "}", $DataArr));
    }

    protected function create_erl_list($DataArr)
    {
        if ($DataArr == "[]") {
            return "[]";
        } else {
            return "[" . str_replace(";", ",", $DataArr) . "]";
        }
    }

    // 将格式为123;1,3;456的转换为[{123},{1,3},{456}]
    function create_erl_list2($str)
    {
        if ($str == "") {
            return "[]";
        } else {
            $elem = explode(";", $str);
            $temp = "[";
            foreach ($elem as $val) {
                $temp .= "{{$val}},";
            };
            $temp = chop($temp, ",") . "]";
            return $temp;
        }
    }

    /**转XML
     *
     **/
    protected function create_xml_by_sql($sql, $filename, $sql_flag = true)
    {
        if ($sql_flag) {
            $arr_xml = array();
            $arr_xml[] = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            $arr_xml[] = "<root>";

            $rows = $this->db->fetchRow($sql);

            foreach ($rows as $row) {
                $arr_xml[] = "<item>";
                foreach ($row as $key => $val) {
                    $arr_xml[] = "<{$key}>{$val}</{$key}>";
                }
                $arr_xml[] = "</item>";
            }
            $arr_xml[] = "
            </root>";

            $xml = implode("\n", $arr_xml);
        } else {
            $xml = $sql;
        }
        $file = XML_DATA_DIR . $filename . '.xml';
        return $this->write_file($file, $xml);
    }

    // 激活码
    public function __num_convert($num, $jin)
    {
        $dic = array(
            0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9,
            10 => 'a', 11 => 'b', 12 => 'c', 13 => 'd', 14 => 'e', 15 => 'f', 16 => 'g',
            17 => 'h', 18 => 'i', 19 => 'j', 20 => 'k', 21 => 'l', 22 => 'm', 23 => 'n',
            24 => 'o', 25 => 'p', 26 => 'q', 27 => 'r', 28 => 's', 29 => 't',
            30 => 'u', 31 => 'v', 32 => 'w', 33 => 'x', 34 => 'y', 35 => 'z',
//            36 => 'A', 37 => 'B', 38 => 'C', 39 => 'D', 40 => 'E', 41 => 'F', 42 => 'G',
//            43 => 'H', 44 => 'I', 45 => 'J', 46 => 'K', 47 => 'L', 48 => 'M', 49 => 'N',
//            50 => 'O', 51 => 'P', 52 => 'Q', 53 => 'R', 54 => 'S', 55 => 'T',
//            56 => 'U', 57 => 'V', 58 => 'W', 59 => 'X', 60 => 'Y', 61 => 'Z',
//            62 => '=', 63 => '-', 64 => '+', 65 => '*', 66 => '/', 67 => '!', 68 => '#', 69 => '?'
        );
        $shang = $num;
        $res = '';
        do {
            $yu = $shang % $jin;
            $res = (string)(($dic[$yu]) . $res);
            $shang = floor($shang / $jin);
        } while ($shang >= $jin);
        $res = (string)(($dic[$yu]) . $res);
        return $res;
    }

    /**
     * 生成文件
     *
     * @param string $fileName 文件路径
     * @param string $content 文件内容
     * @return bool
     */
    public function write_file($fileName, $content)
    {
        $fp = fopen($fileName, 'w');
        if ($fp) {
            fwrite($fp, $content);
            fclose($fp);
            return true;
        }
        return false;
    }


    /**
     * 将数据字段替换键值方法
     */
    public function returnK($date, $k)
    {
        $return = array();
        foreach ($date as $key => $val) {
            $return[$val[$k]] = $val;
        }
        ksort($return);
        return $return;
    }

    /**
     *  返回当前游戏服数据
     */
    public function serverinfo()
    {
        $sql = " select * from adminserverlist where  sdb='" . $_SESSION['admin_db_link'] . "'";
        $sever = $this->admindb->fetchOne($sql);

        return $sever;
    }

    //排序
    public function my_sort($arrays, $sort_key, $sort_order = SORT_DESC, $sort_type = SORT_NUMERIC)
    {
        if (is_array($arrays)) {
            foreach ($arrays as $array) {
                if (is_array($array)) {
                    $key_arrays[] = $array[$sort_key];
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
        array_multisort($key_arrays, $sort_order, $sort_type, $arrays);
        return $arrays;
    }

    /**
     * 获取某个时间内开服的服务器列表
     * 返回 id->name
     * @param string $gid
     * @param string $sid
     * @param string $status
     * @return mixed
     */
    public function get_allserver_list_for_otime($otime_sql_str)
    {
        $where = " WHERE `default` in (0,1,4) ";
        if ($_SESSION['channel']) {
            foreach ($_SESSION['channel'] as $key => $val) {
                if (strpos($where, "FIND_IN_SET")) {
                    $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                } else {
                    $where .= " AND  (FIND_IN_SET('$val', channel_list) ";
                }
            }
            $where .= ')';
        }
        $where .= $otime_sql_str;
        $sql = "select gid,sname,id,sdb from adminserverlist $where order by gid,otime asc";
        $serverlist = $this->admindb->fetchRow($sql);
        $slist = array();
        if (!empty($serverlist)) {
            foreach ($serverlist as $key => $val) {
                $slist[$val['id']] = $val['sname'];
            }
        }
        return $slist;
    }

    public function get_where_g($gid, $g_list)
    {
        $where = " ";
        if (!$gid && $_SESSION['super'] != 'YES') {
            $where .= " AND gid in (";
            $all_gid = array();
            foreach ($g_list as $key => $val) {
                array_push($all_gid, $key);
            }
            $where .= join(',', $all_gid) . ") ";
        } else if (!$gid && $_SESSION['super'] == 'YES') {
            $where .= " ";
        } else {        //单个平台
            $where .= " AND gid =" . $gid;
        }
        return $where;
    }

    public function get_where_s($sid,$s_list)
    {
        $where = " ";
        if (!$sid && $_SESSION['super'] != 'YES') {
            $where .= " AND sid in (";
            $all_sid = array();
            foreach ($s_list as $key => $val) {
                array_push($all_sid, $key);
            }
            $where .= join(',', $all_sid) . ") ";
        } else if (!$sid && $_SESSION['super'] == 'YES') {
            $where .= " ";
        } else {        //单个平台
            $where .= " AND sid =" . $sid;
        }
        return $where;
    }

    public function get_where_c($channel,$c_list,$sql_channel='source')
    {
        $where = " ";
        if (!$channel && $_SESSION['super'] != 'YES') {
            $where .= " AND $sql_channel in (";
            $all_sid = array();
            foreach ($c_list as $key => $val) {
                array_push($all_sid, "'".$key."'");
            }
            $where .= join(',', $all_sid) . ") ";
        } else if (!$channel && $_SESSION['super'] == 'YES') {
            $where .= " ";
        } else {        //单个平台
            $where .= " AND $sql_channel ='" . $channel."'";
        }
        return $where;
    }

    /**
     * 获取平台下的渠道
     *
     */
    public function ajax_channel_list_select_by_gid()
    {
        $str = '';
        $gid = request('gid', 'str');
        if ($gid) {
            $slist = $this->get_channel_list($gid);
            if ($slist) {
                $str = '<option value="" checked>全部</option>';
                foreach ($slist as $key => $val) {
                    $str .= "<option value='{$key}' >{$val}({$key})</option>";
                }
                $json = array('flag' => 1, 'json' => $str);
                exit(json_encode($json));
            }

        } else {
            $json = array('flag' => 0, 'json' => $str);
            exit(json_encode($json));
        }
    }

    /**
     * 获取平台下的渠道
     *
     */
    public function ajax_channel_list_checkbox_by_gid()
    {
        $str = '';
        $gid = request('gid', 'str');
        if ($gid) {
            $sql = "select gid,channel_name,channel_str from channel_list where gid = $gid";
            $channel_res = $this->admindb->fetchRow($sql);
            $channel_list = array();
            foreach ($channel_res as $k => $v) {
                $channel_list[$v['channel_str']] = $v['channel_name'];
            }
            if ($channel_list) {
                foreach ($channel_list as $key => $val) {
                    $str .= "<input name='channel[]' type='checkbox' value='{$key}' >{$val}({$key})&nbsp;";
                }
                $json = array('flag' => 1, 'json' => $str);
                exit(json_encode($json));
            }

        } else {
            $json = array('flag' => 0, 'json' => $str);
            exit(json_encode($json));
        }
    }

    /**
     * 导出excel
     * @param $headerArray
     * @param $bodyArray
     * @param $name
     */
    public function export_to_excel($headerArray,$bodyArray,$name=''){
        require_once("Classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        foreach ($headerArray as $col=>$v) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, 1, $v);
        }
        $line = 2;
        foreach ($bodyArray as $lines) {
            $col = 0;
            foreach ($lines as $v) {
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($col, $line, $v);
                $col++;
            }
            $line++;
        }
        $objPHPExcel->getActiveSheet()->setTitle($name);
        $excelName = $name.date('Ymd').'.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $excelName);
        header('Cache-Control: max-age=0');
        header("Pragma: no-cache");
        $objWriter->save('php://output');
    }

    /**
     * 添加一条重启游戏服记录
     * @param $save
     */
    public function restart_server_add($save){
        // 查询数据库，如果有未执行操作，或者未完成操作
        $sql = "select count(*) as num from auto_server_start where finishtime=0 or runtime=0";
        $res = $this->admindb->fetchOne($sql);

        if ($res['num'] > 0) {//有未执行，或者未跑完的
            return false;
        }

        return $this->admindb->insert('auto_server_start',$save);
    }

    /**
     * 重启操作
     * @param $id
     */
    public function restart_server_action($id){
        if (empty($id)) {
            return false;
        }

        $sql = "select * from auto_server_start where id='$id'";
        $info = $this->admindb->fetchOne($sql);
        if (empty($info)) {
            return false;
        }
        $runner = $_SESSION['username'];

        file_put_contents(LOG_DIR . 'restart_game_server_log.txt', "正在等待重启，1分钟内将执行，请稍等...");            //写入等待文字
        system("echo {$info['url']} {$info['ip']} $id >" . LOG_DIR . "restart_game_server.txt", $ret);               //将服务器的url，ip，id写入到txt文件中，运维那边有脚本实时监控这个文件，有内容则进行重启

        $time = time();
        return $this->admindb->query("update auto_server_start set runtime = '$time',runner='$runner' where id='{$id}'");
    }

    //服务器列表查询
    public function get_adminserver_list($akey = 'id', $aval = 'sdb' , $where = ' where 1 ' ,$orderby = ' ORDER BY id ASC',$field = '*',  $flag = false)
    {
        $where = " where 1 ";
        if ($flag) {
            $where .= " and `default` in (0,1,4) ";
        }

        if ($_SESSION['gids']) {
            $where .= " and gid in (" . implode(',', $_SESSION['gids']) . ")";
            if ($_SESSION['channel']) {
                foreach ($_SESSION['channel'] as $key => $val) {
                    if (strpos($where, "FIND_IN_SET")) {
                        $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                    } else {
                        $where .= " AND  (FIND_IN_SET('$val', channel_list) ";
                    }
                }
                $where .= ')';
            }
        }

        $sql = " select $field from adminserverlist $where $orderby ";
     
        $serverlist = Config::admindb()->fetchRow($sql);
        $slist = array();
        if (!empty($serverlist)) {
            $str = explode(',', $akey);
            $str2 = $aval ? '$val[\''.$aval.'\']' : '$val';
            foreach ($str as $kk => $vv) {
                $str[$kk] = '[$val[\''.$vv.'\']]';
            }
            $str = "\$slist".implode('', $str)." = $str2;";
        
        // print_r($str);exit;
            foreach ($serverlist as $key => $val) {
                // $slist[$val['gid']][$val['id']] = $val[$val];
                eval($str);
            }
        }

        return $slist;
    }

    //简易查询
    public function get_list($db, $table, $key = null, $val = null, $sql = null)
    {
        if($db == 'admin') {
            $mysql = Config::admindb();
        } elseif($db == 'center') {
            $mysql = Config::centerdb();
        } elseif(is_numeric($db)) {
            $mysql = Config::gamedb($db);
        } else {
            return '请输入正确的数据库连接';
        }

        if($sql) {
            $sql = $sql;
        } elseif($table) {
            $sql = " SELECT * FROM $table ";
        } else{
            return '请输入表名或者sql语句';
        }
        $list = $mysql->fetchRow($sql);
        if($key) {
            $list = array_to_hashmap($list, $key, $val);
        } 
        return $list;
    }

    /**
     * 输出lay_json格式(专门给lay后台模版用的)
     * @param $code    int    错误代码  
     * @param $msg     string 错误信息 ，如果msg为空，则取code去Error获取错误信息
     * @param $data    array  需要返回的信息 
     * @param $count   int  数据总记录数
     * @param $paramet string 传入信息 
     * @param $ext array 额外参数
     * @return string
     */
    public function echo_lay_json($code = 0, $msg = '', $data = array(), $count = 0, $paramet = true, $ext = array())
    {
        $res = array(
            'code' => $code,//状态码
            'msg' => $msg,//报错信息
            'paramet' => $paramet == true ? $_REQUEST : array(),//传入参数
            'count' => $count,//总条数
            'ext' => $ext,//额外参数
            'data' => $data,//返回值
        );
        return json_encode($res);
    }

    //基础配置列表
    public function get_config_list($type = 0, $attr = true, $sid = false){      
        $config_list = $this->base_config_list;
        //过滤要元素
        $delete_config = array(7,8);
        foreach ($delete_config as $key => $val) {
            unset($config_list[$val]);
        }

        if(empty($type)) return  $config_list;
        $cache = Ext_Memcached::getInstance("user");
         //存在sid时用sid，否则拿当前游戏服
        $sid = $sid ? $sid : $_SESSION['selected_sid'];
        if(is_bool($attr)){
            $attrSign = $attr?'true':'false';
        }else{
            $attrSign = $attr;
        }

        $cache_sid_key = "log_{$type}_{$sid}_{$attrSign}";
        $cache_key = "log_{$type}_{$attrSign}";

        $data = array();
        switch ($type) {
            case '1':
                if (!$data = $cache->fetch($cache_key)) {
                    $sql = "select * from base_config where type = $type ";
                    $res = $this->admindb->fetchRow($sql);

                    if($attr){
                        foreach ($res as $key => $val) {
                            $attr = json_decode($val['attr'], true);
                            $color = $this->goods_color[$attr['color']];
                            $data[$val['id']] = "<span style='color:$color'>".$val['item']."</span>";
                        }
                    // print_r($data);exit;
                    }else{
                        foreach ($res as $key => $val) {
                            $data[$val['id']] = $val['item'];
                        }
                    }
                    $cache->store($cache_key, $data, 300);
                }    
            break;
            case 'role_list':
                if (!$data = $cache->fetch($cache_sid_key)) {
                    
                        $tomorrow   = (strtotime(date('Y-m-d'))+86400);
                        $today      = (strtotime(date('Y-m-d')));
                        $yesterday  = (strtotime(date('Y-m-d'))-86400);
                        $befday     = (strtotime(date('Y-m-d'))-86400*2);
                        $threeday   = (strtotime(date('Y-m-d'))-86400*3);
            
                    $sql = "select role_id,concat(`name`,'(',`level`,'级)') `name`,last_login_time from role_basic";
                    $res = Config::gamedb($sid)->fetchRow($sql);
                   
                    foreach ($res as $key => $val) {
                        if($attr){
                            $time = $val['last_login_time'];
                            if($time > $tomorrow){
                                $data[$val['role_id']] = "<t style=color:#d9e426;>".$val['name']."</t>";
                            }elseif($time > $today && $time < $tomorrow){
                                $data[$val['role_id']] = "<t style=color:red;>".$val['name']."</t>";
                            }elseif($time > $yesterday && $time < $today){
                                $data[$val['role_id']] = "<t style=color:blue;>".$val['name']."</t>";
                            }elseif($time > $befday && $time < $yesterday){
                                $data[$val['role_id']] = "<t style=color:#00d0ff;>".$val['name']."</t>";
                            }elseif($time > $threeday && $time < $befday){
                                $data[$val['role_id']] = "<t style=color:#17923d;>".$val['name']."</t>";
                            }else{
                                $data[$val['role_id']] = $val['name'];
                            }    
                        }else{
                            $data[$val['role_id']] = $val['name'];
                        }
                          
                    }

                    $cache->store($cache_sid_key, $data, 60);
                }
            break; 
            case 'guild_list':
                if (!$data = $cache->fetch($cache_sid_key)) {
                    $sql = "select guild_id,`name` from guild";
                    $res = Config::gamedb($sid)->fetchRow($sql);
                    foreach ($res as $key => $val) {
                        $data[$val['guild_id']] = $val['name'];
                    }
                    $cache->store($cache_sid_key, $data, 60);
                }
            break;   
            case 'gid_list':
                if (!$data = $cache->fetch($cache_key)) {
                    $sql = "select gid,platform_name from adminplatformlist";
                    $res = $this->admindb->fetchRow($sql);
                    foreach ($res as $key => $val) {
                        $data[$val['gid']] = $val['platform_name'];
                    }
                    $cache->store($cache_key, $data, 10);
                }
            break;
            case 'sid_list':
                $attr = $attr ? 'description' : $attr;
                if (!$data = $cache->fetch($cache_key)) {
                    $sql = "select id,$attr from adminserverlist";
                    $res = $this->admindb->fetchRow($sql);    
                    foreach ($res as $key => $val) {
                        $data[$val['id']] = $val[$attr];
                    }
                    $cache->store($cache_key, $data, 10);
                }
            break;
            default://通用类型
                if (!$data = $cache->fetch($cache_key)) {
                    $sql = "select * from base_config where type = $type ";
                    $res = $this->admindb->fetchRow($sql);
                    $rgb = array(0,0,0);
                    // print_r($rgb);exit;
                    if($attr == false){
                        foreach ($res as $key => $val) {
                            $data[$val['id']] = $val['item'];
                        }
                    }else{
                        foreach ($res as $key => $val) {
                            // $rgb[0] = $val['id'] < 80 ? $val['id'] * 3 : $val['id'] % 240 ;
                            // $rgb[1] = $val['id'] % 120 ;
                            // $rgb[2] = $val['id'] % 80 ;
                            $rgb = $this->color_return($val['id']);
                            // print_r($rgb);
                            $color = implode(',', $rgb);
                            $data[$val['id']] = "<span style='color : rgb($color)'>".$val['item']."</span>";
                        }
                    }
                    $cache->store($cache_key, $data, 300);
                }
            break;
        }
// exit;
        return $data;
    }

    //颜色配置返回
    public function color_return($id){
        //黑色 0,0,0
        //白色 255,255,255
        //红色 255,0,0
        //绿色 0,255,0
        //蓝色 0,0,255
        //黄色 255,255,0
        //青色 0,255,255
        
        $r = ($id % 10) * 10 * 3 ;
        $g = $id % 100 * 2;
        $b = round($id % 150 * 1.5) ;
        
        if(($r+$g+$b)<90){
            $r = $r * 2;$g = $g * 30;$b = $b * 90;
        }
        return array($r, $g, $b);
        
    }

}