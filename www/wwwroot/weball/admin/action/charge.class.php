<?php
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_charge extends action_superclass
{
     //全部订单
    public function charge_list_all()
    {
        $assign = $this->get_server_rights();
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];
        $c_list = $assign['channel_list'];
        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $pay_no = request('pay_no', 'str');
        $cp_no = request('cp_no', 'str');
        $accname = request('accname', 'str');
        $role_id = request('role_id', 'str');
        $nickname = request('nickname', 'str');
        $channel = request('channel', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $cur_page = $cur_page = empty($_GET['page']) ? 1 : request('page', 'int');
        $per_page = request('per_page', 'int');

        $url = "action_gateway.php?";

        $where = "WHERE 1 ";
        //获取平台，服务器sql
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);
        $where .= $this->get_where_c($channel, $c_list, 'source');

        if ($pay_no) {
            $where .= " AND pay_no = '$pay_no'";
        }
        if ($cp_no) {
            $where .= " AND cp_no = '$cp_no'";
        }
        if ($accname) {
            $where .= " AND accname = '$accname'";
        }
        if ($role_id) {
            $where .= " AND role_id = $role_id";
        }
        if ($nickname) {
            $where .= " AND nickname = '$nickname'";
        }
        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND ctime >= $start_time";
        } else {
            $start_time = strtotime(date('Y-m-d')) - 7 * 86400;  //前7天
            $_GET['start_time'] = date('Y-m-d', $start_time);
            $where .= " AND ctime >= $start_time";
        }
        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND ctime < $end_time";
        } else {
            $end_time = $start_time + 86400 * 8;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND ctime < $end_time";
        }

        $per_page = $per_page ? $per_page : 20;
        $count_sql = "select count(*) as total from center_charge $where";
        $total_record = $this->admindb->fetchOne($count_sql);
        $start_row = ($cur_page - 1) * $per_page;
        $total = $total_record['total'];
        $total_page = ceil($total / $per_page);


        $sql = "select gid,sid,pay_no,cp_no,accname,role_id,nickname,
              lv,source,money,gold,FROM_UNIXTIME(ctime) as ctime,type,product_id,ext
              from center_charge $where order by ctime desc limit $start_row,$per_page";
        $res = $this->admindb->fetchRow($sql);
        foreach ($res as $key => $val) {
            $res[$key]['sname'] = $s_list[$val['sid']];  //加入平台名称
            $res[$key]['gname'] = $g_list[$val['gid']];  //加入服务器名称
            $res[$key]['money'] = round($val['money'] / 100, 2);
        }

        $assign['list'] = $res;
        $assign['pages'] = array(
            "per_page" => $per_page,
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total,
            "url" => $url . http_build_query($_REQUEST) . "&page=",
        );
        $this->display(CENTER_TEMP . "charge_list_all.shtml", $assign);

    }

    //单服全部订单
    public function charge_list()
    {

        $pay_no = request('pay_no', 'str');
        $cp_no = request('cp_no', 'str');
        $accname = request('accname', 'str');
        $role_id = request('role_id', 'str');
        $nickname = request('nickname', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $state = request('state', 'int');
        $cur_page = $cur_page = empty($_GET['page']) ? 1 : request('page', 'int');
        $per_page = request('per_page', 'int');

        $url = "action_gateway.php?";
        $where = "WHERE 1 ";

        if ($pay_no) {
            $where .= " AND pay_no = '$pay_no'";
        }
        if ($cp_no) {
            $where .= " AND cp_no = '$cp_no'";
        }
        if ($accname) {
            $where .= " AND accname = '$accname'";
        }
        if ($role_id) {
            $where .= " AND role_id = $role_id";
        }
        if ($nickname) {
            $where .= " AND name = '$nickname'";
        }
        if ($state) {
            if ($state == 1) {
                $where .= " AND state = 0";
            }
            if ($state == 2) {
                $where .= " AND state = 1";
            }
        }

        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND ctime >= $start_time";
        } else {
            $start_time = strtotime(date('Y-m-d')) - 7 * 86400;
            $_GET['start_time'] = date('Y-m-d', $start_time);
            $where .= " AND ctime >= $start_time";
        }
        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND ctime < $end_time";
        } else {
            $end_time = $start_time + 86400 * 8;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND ctime < $end_time";
        }

        $per_page = $per_page ? $per_page : 20;
        $count_sql = "select count(*) as total from recharge $where";
        $total_record = $this->db->fetchOne($count_sql);
        $start_row = ($cur_page - 1) * $per_page;
        $total = $total_record['total'];
        $total_page = ceil($total / $per_page);


        $sql = "select pay_no,cp_no,accname,role_id,name nickname,
              level lv,source,rmb money,gold,FROM_UNIXTIME(ctime) as ctime,state,type,product_id,remark ext
              from recharge $where order by ctime desc limit $start_row,$per_page";
        $res = $this->db->fetchRow($sql);
        $gid_list = $this->get_allgroup_list();
        $server_res = $this->serverinfo();
        $gid = $server_res['gid'];
        foreach ($res as $key => $val) {
            $res[$key]['sname'] = $server_res['sname'];  //加入平台名称
            $res[$key]['gname'] = $gid_list[$gid];  //加入服务器名称
            $res[$key]['money'] = round($val['money']);
        }

        $assign['list'] = $res;
        $assign['pages'] = array(
            "per_page" => $per_page,
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total,
            "url" => $url . http_build_query($_REQUEST) . "&page=",
        );
        $this->display("charge_list_server.shtml", $assign);

    }

    //全部订单导出
    public function charge_list_all_export()
    {
        ini_set('memory_limit', '1024M');
        $assign = $this->get_server_rights();
        $s_list = $assign['srv_server'];
        $g_list = $assign['srv_group'];
        $c_list = $assign['channel_list'];
        $sid = request('sid', 'int');
        $gid = request('gid', 'int');
        $pay_no = request('pay_no', 'str');
        $cp_no = request('cp_no', 'str');
        $accname = request('accname', 'str');
        $role_id = request('role_id', 'str');
        $nickname = request('nickname', 'str');
        $channel = request('channel', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        $where = "WHERE 1 ";
        $where .= $this->get_where_g($gid, $g_list);
        $where .= $this->get_where_s($sid, $s_list);
        $where .= $this->get_where_c($channel, $c_list, 'source');

        if ($pay_no) {
            $where .= " AND pay_no = '$pay_no'";
        }
        if ($cp_no) {
            $where .= " AND cp_no = '$cp_no'";
        }
        if ($accname) {
            $where .= " AND accname = '$accname'";
        }
        if ($role_id) {
            $where .= " AND role_id = $role_id";
        }
        if ($nickname) {
            $where .= " AND nickname = '$nickname'";
        }
        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND ctime >= $start_time";
        } else {
            $start_time = strtotime(date('Y-m-d')) - 7 * 86400;
            $_GET['start_time'] = date('Y-m-d', $start_time);
            $where .= " AND ctime >= $start_time";
        }
        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND ctime < $end_time";
        } else {
            $end_time = $start_time + 86400 * 8;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND ctime < $end_time";
        }

        $sql = "select count(*) as total
              from center_charge $where order by ctime desc";
        $res = $this->admindb->fetchOne($sql);
        if ($res['total'] >= 8000) {
            alert(Ext_Template::lang('数据量过大，请再缩小范围')); 
            exit();
        }

//        Admin::adminLog("游戏服充值日志:$giftid");
        $sql = "select gid,sid,pay_no,cp_no,accname,role_id,nickname,
              lv,source,money,gold,FROM_UNIXTIME(ctime) as ctime,type,product_id,ext
              from center_charge $where order by ctime desc ";
        $res = $this->admindb->fetchRow($sql);

        require_once("Classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, 1, '平台名称');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, 1, '服务器');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, 1, '平台订单号');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, 1, 'CP订单号');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, 1, '账号');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, 1, '用户id');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, 1, '昵称');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, 1, '等级');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, 1, '渠道');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, 1, '金额（单位：元）');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, 1, '游戏币');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, 1, '订单时间');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, 1, '充值类型');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, 1, '商品id');
        foreach ($res as $key => $val) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $key + 2, $g_list[$val['gid']]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $key + 2, $s_list[$val['sid']]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $key + 2, $val['pay_no'] . " ");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $key + 2, $val['cp_no'] . " ");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $key + 2, $val['accname']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 2, $val['role_id'] . " ");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $key + 2, $val['nickname']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $key + 2, $val['lv']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $key + 2, $val['source']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, $key + 2, $val['money'] / 100);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, $key + 2, $val['gold']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, $key + 2, $val['ctime']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, $key + 2, $val['type']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, $key + 2, $val['product_id']);
        }
        $objPHPExcel->getActiveSheet()->setTitle("charge_list_all");
        $excelName = 'charge_list_all_' . date("YmdHis") . '.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $excelName);
        header('Cache-Control: max-age=0');
        header("Pragma: no-cache");
        $objWriter->save('php://output');
    }

    //单服订单导出
    public function charge_list_export()
    {

        ini_set('memory_limit', '1024M');
        $pay_no = request('pay_no', 'str');
        $cp_no = request('cp_no', 'str');
        $accname = request('accname', 'str');
        $role_id = request('role_id', 'str');
        $nickname = request('nickname', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $state = request('state', 'int');
        $where = "WHERE 1 ";

        if ($pay_no) {
            $where .= " AND pay_no = '$pay_no'";
        }
        if ($cp_no) {
            $where .= " AND cp_no = '$cp_no'";
        }
        if ($accname) {
            $where .= " AND accname = '$accname'";
        }
        if ($role_id) {
            $where .= " AND role_id = $role_id";
        }
        if ($nickname) {
            $where .= " AND nickname = '$nickname'";
        }
        if ($state) {
            if ($state == 1) {
                $where .= " AND state = 0";
            }
            if ($state == 2) {
                $where .= " AND state = 1";
            }
        }
        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " AND ctime >= $start_time";
        } else {
            $start_time = strtotime(date('Y-m-d')) - 7 * 86400;
            $_GET['start_time'] = date('Y-m-d', $start_time);
            $where .= " AND ctime >= $start_time";
        }
        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " AND ctime < $end_time";
        } else {
            $end_time = $start_time + 86400 * 8;
            $_GET['end_time'] = date('Y-m-d', $end_time - 86400);
            $where .= " AND ctime < $end_time";
        }

        $sql = "select count(*) as total
              from charge $where order by ctime desc";
        $res = $this->db->fetchOne($sql);
        if ($res['total'] >= 8000) {
            alert(Ext_Template::lang('数据量过大，请再缩小范围'));
            exit();
        }

//        Admin::adminLog("游戏服充值日志:$giftid");
        $sql = "select pay_no,cp_no,accname,role_id,nickname,
              lv,source,money,gold,FROM_UNIXTIME(ctime) as ctime,state,type,product_id,ext
              from charge $where order by ctime desc";
        $res = $this->db->fetchRow($sql);
        require_once("Classes/PHPExcel.php");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, 1, '平台订单号');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, 1, 'CP订单号');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, 1, '账号');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, 1, '用户id');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, 1, '昵称');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, 1, '等级');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, 1, '渠道');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, 1, '金额（单位：元）');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, 1, '游戏币');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, 1, '订单时间');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, 1, '领取状态');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, 1, '充值类型');
        $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, 1, '商品id');
        foreach ($res as $key => $val) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $key + 2, $val['pay_no'] . " ");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $key + 2, $val['cp_no'] . " ");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $key + 2, $val['accname']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $key + 2, $val['role_id'] . " ");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $key + 2, $val['nickname']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 2, $val['lv']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $key + 2, $val['source']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $key + 2, $val['money'] / 100);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $key + 2, $val['gold']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, $key + 2, $val['ctime']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, $key + 2, $val['state'] == 0 ? '未领取' : '已领取');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, $key + 2, $val['type']);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, $key + 2, $val['product_id']);
        }
        $objPHPExcel->getActiveSheet()->setTitle("charge_list");
        $excelName = 'charge_list_' . date("YmdHis") . '.xls';
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-Disposition: attachment; filename=" . $excelName);
        header('Cache-Control: max-age=0');
        header("Pragma: no-cache");
        $objWriter->save('php://output');
    }

    /**
     * RMB玩家管理
     */
    public function charge_rmb()
    {
        $where = " where 1 ";
        $url = "?ctl=charge&act=charge_rmb";
//        $orderby = " order by totalmoney desc ";
        $is_inner = request('is_inner','int');
        if($is_inner == 1){
            $where .= ' and is_inner=1';
        }elseif($is_inner == 2){
            $where .= ' and is_inner!=1';
        }

        $accname = request('accname', 'str');
        if (!empty($accname)) {
            $where .= " and accname = '$accname' ";
            $url .= "&accname=$accname";
        }

        $role_id = request('role_id', 'str');
        if (!empty($role_id)) {
            $where .= " and a.role_id = '$role_id' ";
            $url .= "&role_id=$role_id";
        }

        //背包库存彩钻
        $gold_min = request('gold_min', 'int');
        $gold_max = request('gold_max', 'int');
        if ($gold_min) {
            $where .= " and yuanbao >= $gold_min";
            $url .= "&gold_min=$gold_min";
        }
        if ($gold_max) {
            $where .= " and yuanbao <= $gold_max";
            $url .= "&gold_max=$gold_max";
        }

        //首充等级
        $first_pay_lv_min = request('first_pay_lv_min', 'int');
        $first_pay_lv_max = request('first_pay_lv_max', 'int');
        if ($first_pay_lv_min) {
            $where .= " and first_pay_lv >= $first_pay_lv_min";
            $url .= "&first_pay_lv_min=$first_pay_lv_min";
        }
        if ($first_pay_lv_max) {
            $where .= " and first_pay_lv <= $first_pay_lv_max";
            $url .= "&first_pay_lv_max=$first_pay_lv_max";
        }


        $nickname = request('nickname', 'str');
        if (!empty($nickname)) {
            $where .= " and nick_name = '$nickname' ";
            $url .= "&nickname=$nickname";
        }

        $moneysort = request('moneysort', 'int');
        if ($moneysort == 1) {
            $orderby = " order by totalmoney asc ";
            $assign['moneysort'] = 2;
            $url .= "&moneysort=$moneysort";
        } elseif ($moneysort == 2) {
            $orderby = " order by totalmoney desc ";
            $assign['moneysort'] = 1;
            $url .= "&moneysort=$moneysort";
        }

        //玩家等级
        $role_lv = request('role_lv', 'int');
        if (!empty($role_lv)) {
            $where .= " and level = $role_lv";
            $url .= "&role_lv=$role_lv";
        }

        //充值总额
        $totalmoney_min = request('totalmoney_min', 'int');
        $totalmoney_max = request('totalmoney_max', 'int');

        if ($totalmoney_min) {
            $where .= " and total_pay_money >= $totalmoney_min";
            $url .= "&totalmoney_min=$totalmoney_min";
        }
        if ($totalmoney_max) {
            $where .= " and total_pay_money<=$totalmoney_max";
            $url .= "&totalmoney_max=$totalmoney_max";
        }

        //充值次数
        $totalnum_min = request('totalnum_min', 'int');
        $totalnum_max = request('totalnum_max', 'int');
        if ($totalnum_min) {
            $where .= " and total_pay_cnt >= $totalnum_min";
            $url .= "&totalnum_min=$totalnum_min";
        }
        if ($totalnum_max) {
            $where .= " and total_pay_cnt <= $totalnum_max";
            $url .= "&totalnum_max=$totalnum_max";
        }

        //停充天数
        $stop_pay_days_min = request('stop_pay_days_min', 'int');
        $stop_pay_days_max = request('stop_pay_days_max', 'int');
        if ($stop_pay_days_min) {
            $where .= " and to_days(now()) -  to_days(FROM_UNIXTIME(last_pay_time)) >=$stop_pay_days_min";
            $url .= "&stop_pay_days_min=$stop_pay_days_min";
        }
        if ($stop_pay_days_max) {
            $where .= " and to_days(now()) -  to_days(FROM_UNIXTIME(last_pay_time)) <=$stop_pay_days_max";
            $url .= "&stop_pay_days_max=$stop_pay_days_max";
        }

        //未登陆天数
        $stop_login_days_min = request('stop_login_days_min', 'int');
        $stop_login_days_max = request('stop_login_days_max', 'int');
        if ($stop_login_days_min) {
            $where .= " and to_days(now()) - to_days(FROM_UNIXTIME(last_login_time)) >= $stop_login_days_min";
            $url .= "&stop_login_days_min=$stop_login_days_min";
        }
        if ($stop_login_days_max) {
            $where .= " and to_days(now()) -  to_days(FROM_UNIXTIME(last_login_time)) <=$stop_login_days_max";
            $url .= "&stop_login_days_max=$stop_login_days_max";
        }

        //平均充值
        $avgmoney_min = request('avgmoney_min', 'int');
        $avgmoney_max = request('avgmoney_max', 'int');
        if ($avgmoney_min) {
            $where .= " and  total_pay_money/total_pay_cnt >= $avgmoney_min";
        }
        if ($avgmoney_max) {
            $where .= " and  total_pay_money/total_pay_cnt <= $avgmoney_max";
        }

        $total_record = $this->db->fetchOne("SELECT COUNT(distinct(a.role_id)) AS total from role_base a left join role_currency b on a.role_id = b.role_id $where  and a.total_pay_money > 0 ");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select a.role_id,nick_name as nickname,acc_name as accname,level,total_pay_money as totalmoney,total_pay_cnt as total_num,first_pay_lv as min_lv,
              to_days(now()) - to_days(FROM_UNIXTIME(last_login_time)) as stop_login_time,
              to_days(now()) - to_days(FROM_UNIXTIME(last_pay_time)) as stop_pay_time,
              FROM_UNIXTIME(reg_time) as reg_time,
              FROM_UNIXTIME(first_pay_time) as ctime,
              FROM_UNIXTIME(last_login_time) as last_login_time,
              (total_pay_money/total_pay_cnt) as avgmoney,
              b.yuanbao as gold,b.byuanbao as bgold,
              a.is_inner
              from role_base a left join role_currency b on a.role_id = b.role_id
              $where  and a.total_pay_money > 0 $orderby $limit ";
        $list = $this->db->fetchRow($sql);

        $sql = "select sum(total_pay_money) as total from role_base";
        $total_money = $this->db->fetchOne($sql);

        foreach ($list as $key => $value) {
            $list[$key]['totalsmoneyrate'] = round($value['totalmoney'] / $total_money['total'], 4) * 100;

            if ($value['ctime'] == '1970-01-01 08:00:00') {
                $list[$key]['ctime'] = Ext_Template::lang('无记录');
            }
        }

        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );

        if ($_GET['export'] == '导出') {
            $elist = $list;
            require_once("Classes/PHPExcel.php");
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, 1, '角色ID');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, 1, '角色名');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, 1, '账号');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, 1, '等级');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, 1, '充值总额(%)');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, 1, '充值次数');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, 1, '平均充值');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, 1, '背包库存彩钻');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, 1, '停充天数');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, 1, '首充等级');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, 1, '注册时间');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, 1, '首充时间');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, 1, '未登录天数');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, 1, '最后登陆时间');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(14, 1, '是否内玩');

            foreach ($elist as $key => $val) {
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(0, $key + 2, ' ' . $val['role_id']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, $key + 2, ' ' . $val['nickname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, $key + 2, ' ' . $val['accname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, $key + 2, ' ' . $val['level']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, $key + 2, ' ' . $val['totalmoney']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, $key + 2, ' ' . $val['total_num']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(6, $key + 2, ' ' . $val['avgmoney']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(7, $key + 2, ' ' . $val['gold']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(8, $key + 2, ' ' . $val['stop_pay_time']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(9, $key + 2, ' ' . $val['min_lv']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(10, $key + 2, ' ' . $val['reg_time']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(11, $key + 2, ' ' . $val['ctime']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(12, $key + 2, ' ' . $val['stop_login_time']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(13, $key + 2, ' ' . $val['last_login_time']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow(14, $key + 2, ' ' . $val['is_inner']);

            }

            $server_info = $this->serverinfo();

            $objPHPExcel->getActiveSheet()->setTitle('RMB玩家管理');

            $excelName = $server_info['description'] . '-RMB玩家管理' . '.xls';
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            header('Content-Type: application/vnd.ms-excel; charset=utf-8');
            header("Content-Disposition: attachment; filename=" . $excelName);
            header('Cache-Control: max-age=0');
            header("Pragma: no-cache");
            $objWriter->save('php://output');
        }
        // print_r($list);exit;
        $this->display('charge_rmb.shtml', $assign);
    }

    /**
     * 计算ARPU值
     *
     * @param int $totals
     * @param int $num
     * @param int $format
     */
    public static function arpu($totals, $num, $format = 0)
    {
        return round($totals / (empty($num) ? 1 : $num), $format);
    }

    //单服充值统计
    public function charge_statis()
    {
        //参数部分
        $find_type = request('find_type', 'int');  //区分汇总或分渠道
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $source = request('source', 'str');
        $pstart = request('pstart', 'int');   //用于标红
        $pend = request('pend', 'int');  //用于标红

        if (!$find_type) {      //默认为汇总统计
            $find_type = 1;
        }
        $where = ' Where 1';

        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " And ctime >=$start_time";
        } else {
            $start_time = date('Y-m-01');
            $_GET['start_time'] = $start_time;
            $start_time = strtotime($start_time);
            $where .= " And ctime >= $start_time";
        }

        if ($end_time) {
            $end_time = strtotime($end_time) + 86400;
            $where .= " And ctime < $end_time";
        } else {
            $end_time = date('Y-m-d');
            $_GET['end_time'] = $end_time;
            $end_time = strtotime($end_time) + 86400;
            $where .= " And ctime < $end_time";
        }

        //用于标红
        if (!$pstart) {
            $pstart = 3000;   //大于三千的标红
        }
        if (!$pend) {
            $pend = 999999999;
        }
        //查询汇率
        $sql = "select platform_rate as rate from adminplatformlist a LEFT JOIN adminserverlist s on a.gid=s.gid where s.sdb='$_SESSION[admin_db_link]'";
        $rate_res = $this->admindb->fetchone($sql);
        if ($rate_res['rate']) {
            $rate = $rate_res['rate'];
        } else {
            $rate = 1;
        }


        if ($find_type == 1) {
            //查询目前服务器的总人数
            $sql = "select count(*) as total from role_basic;";
            $s_count_res = $this->db->fetchOne($sql);
            $s_count = $s_count_res['total'];

            //统计充值的计数。
            $sql = "select source,sum(rmb) as total_money,sum(gold) as total_gold,count(distinct role_id) as total_role,count(*) as total_count from recharge";
            $total_res = $this->db->fetchOne($sql);
            $total_res['s_count'] = $s_count;
            $total_res['total_money'] = round($total_res['total_money'] / 100 * $rate, 2);
            $total_res['pay_rate'] = (round($total_res['total_role'] / $s_count, 4) * 100);
            $total_res['arppu'] = round($total_res['total_money'] / $total_res['total_role'], 2);

            //每天的统计
            //查询活跃人数
            $sql = "select count(distinct role_id)as act_role ,date(FROM_UNIXTIME(time)) as login_day  from log_login where time>= $start_time and time <$end_time group by login_day";
            $login_res = $this->db->fetchRow($sql);
            $login_data = array();
            foreach ($login_res as $k => $v) {
                $login_data[$v['login_day']] = $v['act_role'];
            }

            //查询时间范围内的充值统计
            $sql = "select sum(rmb) as total_money,sum(gold) as total_gold,count(distinct role_id) as total_role,count(*) as total_count from recharge $where";
            $time_total_res = $this->db->fetchOne($sql);
            $time_total_res['total_money'] = $time_total_res['total_money'] / 100;

            //查询单个日期的充值统计
            $sql = "select sum(rmb) as total_money,sum(gold) as total_gold,count(distinct role_id) as total_role,count(*) as total_count,date(FROM_UNIXTIME(ctime)) as charge_time from recharge $where group by charge_time";
            $charge_day_res = $this->db->fetchRow($sql);

            foreach ($charge_day_res as $k => $v) {
                $charge_day_res[$k]['total_money'] = round($v['total_money'] / 100 * $rate, 2);
                $charge_day_res[$k]['s_count'] = empty($login_data[$v['charge_time']]) ? 0 : $login_data[$v['charge_time']];  //活跃人数
                $charge_day_res[$k]['arpu'] = self::arpu($charge_day_res[$k]['total_money'], $login_data[$v['charge_time']], 2);
                $charge_day_res[$k]['pay_rate'] = $login_data[$v['charge_time']] ? (round($v['total_role'] / $login_data[$v['charge_time']], 4) * 100) : 0;
                $charge_day_res[$k]['arppu'] = round($charge_day_res[$k]['total_money'] / $v['total_role'], 2);

            }
        } else {
            //查询目前服务器的总人数分渠道
            $sql = "select count(*) as total,source from role_basic group by source;";
            $s_count_res = $this->db->fetchRow($sql);
            $channel_all = array();
            foreach ($s_count_res as $k => $v) {
                $channel_all[$v['source']] = $v['total'];
            }

            //统计充值的计数分渠道
            $sql = "select source,sum(rmb) as total_money,sum(gold) as total_gold,count(distinct role_id) as total_role,count(*) as total_count from recharge group by source";
            $total_res = $this->db->fetchRow($sql);
            foreach ($total_res as $k => $v) {
                $total_res[$k]['all'] = $channel_all[$v['source']];
                $total_res[$k]['total_money'] = round($total_res[$k]['total_money'] / 100 * $rate, 2);  //计算汇率
                $total_res[$k]['pay_rate'] = $channel_all[$v['source']] ? (round($total_res[$k]['total_role'] / $channel_all[$v['source']], 4) * 100) : 0;
                $total_res[$k]['arppu'] = $total_res[$k]['total_role'] ? round($total_res[$k]['total_money'] / $total_res[$k]['total_role'], 2) : 0;
            }

            //查询活跃人数 分渠道
            $sql = "select count(distinct role_id)as act_role ,date(FROM_UNIXTIME(time)) as login_day,source  from log_login where time>= $start_time and time <$end_time group by source,login_day";
            $login_res = $this->db->fetchRow($sql);
            $login_data = array();
            foreach ($login_res as $k => $v) {
                $login_data[$v['source']][$v['login_day']] = $v['act_role'];
            }

            //查询时间范围内的充值统计
            $sql = "select sum(rmb) as total_money,sum(gold) as total_gold,count(distinct role_id) as total_role,count(*) as total_count,source from recharge $where";
            $time_total_res = $this->db->fetchOne($sql);
            $time_total_res['total_money'] = $time_total_res['total_money'] / 100;

            //查询单个日期的充值统计
            $sql = "select source,sum(rmb) as total_money,sum(gold) as total_gold,count(distinct role_id) as total_role,count(*) as total_count,date(FROM_UNIXTIME(ctime)) as charge_time from recharge $where group by source,charge_time";
            $charge_day_res = $this->db->fetchRow($sql);
            foreach ($charge_day_res as $k => $v) {
                $charge_day_res[$k]['total_money'] = round($v['total_money'] / 100 * $rate, 2);
                $charge_day_res[$k]['s_count'] = empty($login_data[$v['source']][$v['charge_time']]) ? 0 : $login_data[$v['source']][$v['charge_time']];
                $charge_day_res[$k]['arpu'] = self::arpu($charge_day_res[$k]['total_money'], $login_data[$v['source']][$v['charge_time']], 2);
                $charge_day_res[$k]['pay_rate'] = (round($v['total_role'] / $login_data[$v['source']][$v['charge_time']], 4) * 100);
                $charge_day_res[$k]['arppu'] = round($charge_day_res[$k]['total_money'] / $v['total_role'], 2);

            }
        }

        //单天的充值记录
        $tdate = $_GET['tdate'];
        if (!empty($tdate)) {

            //各种判断条件
            $stime = strtotime($tdate);
            $etime = $stime + 86400;
            $url = "?ctl=charge&act=charge_statis";
            if ($source) {
                $where .= " AND source = '$source' ";
                $url .= "&source=$source";
            }
            $where .= " and ctime >= '$stime' and ctime < $etime ";
            $start_date = date('Y-m-d', $start_time);
            $end_date = date('Y-m-d', $end_time - 86400);
            $url .= "&tdate=$tdate&start_time=$start_date&end_time=$end_date&find_type=$find_type&pstart=$pstart&pend=$pend";
            $total_record = $this->db->fetchOne("SELECT COUNT(*) AS total from recharge $where ");
            $total_record = $total_record['total'];

            $per_page = 30;
            $total_page = ceil($total_record / $per_page);
            $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
            $cur_page = $cur_page < 1 ? 1 : $cur_page;
            $limit_start = ($cur_page - 1) * $per_page;
            $limit = " LIMIT {$limit_start}, $per_page ";

            //查询
            $sql = "select accname,role_id,name nickname,pay_no,level,FORMAT(rmb*$rate,2) as money,gold,ctime from recharge $where  order by ctime desc $limit";
            $plist = $this->db->fetchRow($sql);
     
            $assign['plist'] = $plist;
            $assign['pages'] = array(
                "curpage" => $cur_page,
                "totalpage" => $total_page,
                "totalnumber" => $total_record,
                "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
            );
        }

        $assign['total_list'] = $total_res;
        $assign['day_list'] = $charge_day_res;
        $assign['find_type'] = $find_type;
        $assign['time_total_res'] = $time_total_res;
        $assign['pstart'] = $pstart;
        $assign['pend'] = $pend;
        $this->display('charge_statis.shtml', $assign);
    }


    /**
     * 充值等级分布
     */
    public function lv_region()
    {
        $where = " WHERE 1 ";

        //获取服务器列表权限
        $assign = $this->get_server_rights();

        $gid = request('gid', 'int');
        $sid = request('sid', 'int');
        $channel = request('channel', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        if ($gid) {
            $where .= " And gid = $gid";
        }
        if ($sid) {
            $where .= " And sid = $sid";
        }
        $where .= $this->get_where_c($channel, $assign['channel_list'], 'source');
        if ($start_time) {
            $stime = strtotime($start_time);
        } else {
            $stime = strtotime(date('Y-m-d')) - 6 * 86400;
        }
        $_GET['start_time'] = date('Y-m-d', $stime);
        $where .= " AND ctime>=$stime ";
        if ($end_time) {
            $etime = strtotime($end_time) + 86400;
            if ($etime - $stime > 6 * 86400) {
                $etime = $stime + 7 * 86400;
            }
        } else {
            $etime = $stime + 7 * 86400;
        }
        $_GET['end_time'] = date('Y-m-d', $etime - 86400);
        $where .= " AND ctime<$etime ";

        $sql = "select sum(money) as total_money,count(lv) as total_lv,lv from center_charge $where group by lv order by lv asc";
        $tmp = $this->admindb->fetchRow($sql);
        if (!empty($tmp)) {
            $lv_list = array();
            $lv_money = array();
            $chartkeys = array();
            $chartval = array();
            foreach ($tmp as $key => $val) {
                $charkeys[$val['lv']] = $val['lv'] . '级';
                $lv_list[$val['lv']] = intval($val['total_lv']);
                $lv_money[$val['lv']] = intval($val['total_money']) / 100;
            }
            $max = max($lv_list);
            $avg = round(array_sum($lv_list) / count($lv_list), 2);

            $maxgold = max($lv_money);
            $avggold = round(array_sum($lv_money) / count($lv_money), 2);
        }
        if (!empty($charkeys)) {
            $charkeys = array_values($charkeys);
        }
        if (!empty($lv_list)) {
            $lv_list = array_values($lv_list);
        }
        if (!empty($lv_money)) {
            $lv_money = array_values($lv_money);
        }

        $assign['max'] = $max;
        $assign['avg'] = $avg;
        $assign['maxgold'] = $maxgold;
        $assign['avggold'] = $avggold;

        $assign['charkeys'] = json_encode($charkeys);
        $assign['lvlist'] = json_encode($lv_list);
        $assign['lvmoney'] = json_encode($lv_money);
        $this->display(CENTER_TEMP . 'charge_lv_region.shtml', $assign);
    }

    /**
     * 充值金额分布
     */
    public function money_region()
    {

        $assign = $this->get_server_rights();
        $gid = request('gid', 'int');
        $sid = request('sid', 'int');
        $channel = request('channel', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $where = ' Where 1';
        if ($gid) {
            $where .= " AND gid = $gid";
        }
        if ($sid) {
            $where .= " AND sid = $sid";
        }
        $where .= $this->get_where_c($channel, $assign['channel_list'], 'source');
        if ($start_time) {
            $stime = strtotime($start_time);
        } else {
            $stime = strtotime(date('Y-m-d')) - 6 * 86400;
        }
        $_GET['start_time'] = date('Y-m-d', $stime);
        $where .= " AND ctime>=$stime ";
        if ($end_time) {
            $etime = strtotime($end_time) + 86400;
            if ($etime - $stime > 6 * 86400) {
                $etime = $stime + 7 * 86400;
            }
        } else {
            $etime = $stime + 7 * 86400;
        }
        $_GET['end_time'] = date('Y-m-d', $etime - 86400);
        $where .= " AND ctime<$etime ";

        $sql = "select FROM_UNIXTIME(ctime,'%Y-%m-%d') as time ,count(distinct role_id) as total from center_charge $where group by time";
        $man_res = $this->admindb->fetchRow($sql);
        $list = array();
        foreach ($man_res as $k => $v) {
            $list[$v['time']]['total'] = $v['total'];
            $charkeys[] = $v['time'];
        }
        $sql = "select FROM_UNIXTIME(ctime,'%Y-%m-%d') as time ,sum(money) as total_money,count(distinct role_id) as total_role from center_charge $where group by time,role_id order by time";
        $res = $this->admindb->fetchRow($sql);
        $charval = array();
        $charvals = array();
        foreach ($res as $k => $v) {
            $sort = $this->check_limit($v['total_money'] / 100);
            $list[$v['time']]['money'] += $v['total_money'] / 100;
            $list[$v['time']][$sort] += $v['total_role'];
            $charval[$sort][$v['time']] += $v['total_role'];

        }
        $i = 0;
        foreach ($charval as $key => $val) {
            $charvals[$i]['name'] = $this->check_limit(null, $key, false);
            foreach ($charkeys as $k => $v) {
                if (empty($val[$v])) {
                    $val[$v] = 0;
                }
            }
            ksort($val);
            $charvals[$i++]['data'] = array_values($val);
        }
        ksort($list);
        $assign['list'] = $list;
        $assign['charkeys'] = json_encode($charkeys);
        $assign['charvals'] = json_encode($charvals);
        $this->display(CENTER_TEMP . 'charge_money_region.shtml', $assign);

    }

    /**
     * 查看金额所在的范围
     * @param $num
     */
    private function check_limit($num, $sort = null, $name = true)
    {

        $limit_arr = array(
            '10_sort' => array(0, 10, '[0,10)'),
            '100_sort' => array(10, 100, '[10,100)'),
            '200_sort' => array(100, 200, '[100,200)'),
            '500_sort' => array(200, 500, '[200,500)'),
            '1000_sort' => array(500, 1000, '[500,1000)'),
            '1500_sort' => array(1000, 1500, '[1000,1500)'),
            '2000_sort' => array(1500, 2000, '[1500,2000)'),
            '2500_sort' => array(2000, 2500, '[2000,2500)'),
            '3000_sort' => array(2500, 2147483647, '[2500,∞)')
        );

        if ($name) {
            foreach ($limit_arr as $k => $v) {
                if ($num >= $v[0] && $num < $v[1]) {
                    return $k;
                }
            }
        } else {
            return $limit_arr[$sort][2];
        }
    }

    //货币消耗统计
    public function Output_statistics(){
        $this->consumptionOutput_statistics(2);
    }
    //货币产出统计
    public function Input_statistics(){
        $this->consumptionOutput_statistics(1);
    }

    //消耗统计
    public function consumptionOutput_statistics($op = 2)
    {
        $role_id = request('role_id', 'str');
        $acc_name = request('acc_name', 'str');
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        $where = 'Where 1';
        $log_shop_where = 'Where 1';
        if ($role_id) {
            $where .= " and role_id = '$role_id' ";
            $log_shop_where .= " and role_id = '$role_id' ";
        }
        if ($acc_name) {
            $where .= " and accname = '$acc_name'";
            $sql = "select role_id from role_basic where accname = '$acc_name'";
            $role_res = $this->db->fetchRow($sql);
            $role_ids = array();
            foreach($role_res as $k => $v){
                array_push($role_ids,$v['role_id']);
            }
            $log_shop_where .= ' and role_id in ('.implode(',',$role_ids).')';
        }

        if ($start_time) {
            $stime = strtotime($start_time);
        } else {
            $start_time = date('Y-m-d', strtotime("-10 day")); //默认拿10天的数据
            $stime = strtotime($start_time);
        }
        if ($end_time) {
            $etime = strtotime($end_time) + 86400;
            if ($etime - $stime > 86400 * 10) {
                $stime = $etime - 86400 * 10;
                $start_time = date('Y-m-d', $stime);
            }
        } else {
            $end_time = date('Y-m-d', $stime + 10 * 86400);
            $etime = strtotime($end_time) + 86400;
        }
        $_GET['start_time'] = $start_time;
        $where .= " and time >= $stime";
        $log_shop_where .=" and time >= $stime";

        $_GET['end_time'] = $end_time;
        $where .= " and time < $etime";
        $log_shop_where .=" and time < $etime";

        //获取元宝消耗的类型名
        $type_sql = "select id,item from base_config  where type = 2 ";
        $type_res = $this->admindb->fetchRow($type_sql);
        $type_data = array();
        foreach ($type_res as $key => $val) {
            $type_data[$val['id']]['name'] = '('.$val['id'].')'.$val['item'];
            $type_data[$val['id']]['flag'] = 0;
        }

        //获取绑定元宝的消耗类型名
        $shop_goods_type = $this->get_config_list(1);

        //需要查询类型
        $gold_type = $_GET['gold_type'] ? $_GET['gold_type'] : 2;
        //货币对应商店类型
        $sql = "select item_type,id from base_config where type=10";
        $shop_cur_res = $this->admindb->fetchRow($sql);
        $shop_cur_list = array();
        foreach ($shop_cur_res as $key => $val) {
            if($shop_cur_list[$val['item_type']]){
                $shop_cur_list[$val['item_type']] .= ','.$val['id'];
            }else{
                $shop_cur_list[$val['item_type']] = $val['id'];
            }
            
        }
    
        $shop_cur_id = $shop_cur_list[$gold_type];
        if($op ==2 && $shop_cur_id){
            $total_log_shop_sql = "select sum(num) as total from log_role_shop a $log_shop_where and a.shop_id in($shop_cur_id)";

            $total_log_shop_res = $this->db->fetchOne($total_log_shop_sql);
            $log_shop_sql = "select sum(num) as total,a.item_id from log_role_shop a $log_shop_where and a.shop_id in($shop_cur_id) group by a.item_id order by total desc";
            $log_shop_res = $this->db->fetchRow($log_shop_sql);
            foreach($log_shop_res as $key=>$val){
                $log_shop_res[$key]['name'] = '('.$val['item_id'].')'.$shop_goods_type[$val['item_id']];
                $log_shop_res[$key]['rate'] = round($val['total']/$total_log_shop_res['total'],4)*100;
            }
        }
        
// print_r($log_shop_sql);exit;        
        $total_sql = "select sum(amount) as total_gold from log_role_money $where and op=$op and type=$gold_type";
// print_r($total_sql);        
        $total_res = $this->db->fetchOne($total_sql);

        $sql = "select sum(amount) as gold,opt from log_role_money $where and op=$op and type=$gold_type  group by opt order by gold desc";
        $res = $this->db->fetchRow($sql);

        $data_list = array();
        $all_yuanbao = $total_res['total_gold'];
        foreach ($res as $key => $val) {
            $data = array();
            $data['num'] = $val['gold'];
            $data['id'] = $val['opt'];
            $data['name'] = $type_data[$val['opt']]['name'];
            $data['rate'] = round($val['gold'] / $all_yuanbao, 4) * 100;
            $type_data[$val['opt']]['flag'] = 1;
            array_push($data_list, $data);
        }
        //所有类型补全
        // foreach ($type_data as $key => $val) {
        //     if ($val['flag'] == 0) {
        //         $data = array();
        //         $data['num'] = 0;
        //         $data['id'] = $key;
        //         $data['name'] = $val['name'];
        //         $data['rate'] = 0;
        //         array_push($data_list, $data);
        //     }
        //     $type_data[$key]['flag'] = 0;
        // }

        $day_total_sql = "select sum(amount) as total_gold ,FROM_UNIXTIME(time,'%Y-%m-%d') as time  from log_role_money $where and op=$op and type=$gold_type group by FROM_UNIXTIME(time,'%Y-%m-%d') order by time desc";
       
        $day_total_res = $this->db->fetchRow($day_total_sql);

        $day_all_data = array();

        foreach ($day_total_res as $key => $val) {
            $day_all_data[$val['time']]['num'] = $val['total_gold'];
        }
       
        $day_sql = "select sum(amount) as gold,opt,FROM_UNIXTIME(time,'%Y-%m-%d') as time from log_role_money $where and op=$op and type=$gold_type group by FROM_UNIXTIME(time,'%Y-%m-%d'),opt order by time desc,gold desc";
        $day_res = $this->db->fetchRow($day_sql);
        $all_gold_sql = "select opt,sum(amount) as gold from log_role_money where op=$op and type=$gold_type group by opt order by gold";
        $all_gold_res = $this->db->fetchRow($all_gold_sql);
        $all_gold_data = array();
        foreach ($all_gold_res as $key => $val) {
            $all_gold_data[$val['opt']] = $val['gold'];
        }

        foreach ($day_res as $key => $val) {
            $day_all_data[$val['time']][$val['opt']]['name'] = $type_data[$val['opt']]['name'];
            $day_all_data[$val['time']][$val['opt']]['num'] = $val['gold'];
            $day_all_data[$val['time']][$val['opt']]['rate'] = round($val['gold'] / $day_all_data[$val['time']]['num'], 4) * 100;
            $day_all_data[$val['time']][$val['opt']]['all_rate'] = round($val['gold'] / $all_gold_data[$val['opt']], 4) * 100;
            $day_all_data[$val['time']][$val['opt']]['all_num'] = $all_gold_data[$val['opt']];

        }
// print_r($day_all_data);exit;
        $up = "<span style='color:green'>↑</span>";
        $down = "<span style='color:red'>↓</span>";

        for ($i = $etime - 86400; $i >= $stime; $i = $i - 86400) {
            $date = date('Y-m-d', $i);
            $last_date = date('Y-m-d', $i - 86400);
            //所有类型补全
            // foreach ($type_data as $key => $val) {
            //     if (empty($day_all_data[$date][$key])) {
            //         $day_all_data[$date][$key]['name'] = $val['name'];
            //         $day_all_data[$date][$key]['num'] = 0;
            //         $day_all_data[$date][$key]['rate'] = 0;
            //         $day_all_data[$date][$key]['all_rate'] = 0;
            //         $day_all_data[$date][$key]['all_num'] = empty($all_gold_data[$key]) ? 0 : $all_gold_data[$key];
            //     }
            // }
            // if (empty($day_all_data[$date])) {
            //     foreach ($type_data as $key => $val) {
            //         $day_all_data[$date][$key]['name'] = $val['name'];
            //         $day_all_data[$date][$key]['num'] = 0;
            //         $day_all_data[$date][$key]['rate'] = 0;
            //         $day_all_data[$date][$key]['all_rate'] = 0;
            //         $day_all_data[$date][$key]['all_num'] = empty($all_gold_data[$key]) ? 0 : $all_gold_data[$key];
            //     }
            // }
            if ($day_all_data[$date]) {
                foreach ($day_all_data[$date] as $key => $val) {
                    if($i == $stime){
                        continue;
                    }
                    if ($key != 'num') {
                        if ($day_all_data[$last_date][$key]['num'] < $val['num']) {
                            $day_all_data[$date][$key]['num_status'] = $up;
                        } elseif ($day_all_data[$last_date][$key]['num'] > $val['num']) {
                            $day_all_data[$date][$key]['num_status'] = $down;
                        }
                        if ($day_all_data[$last_date][$key]['rate'] < $val['rate']) {
                            $day_all_data[$date][$key]['rate_status'] = $up;
                        } elseif ($day_all_data[$last_date][$key]['num'] > $val['num']) {
                            $day_all_data[$date][$key]['rate_status'] = $down;
                        }
                        if ($day_all_data[$last_date][$key]['all_rate'] < $val['all_rate']) {
                            $day_all_data[$date][$key]['all_rate_status'] = $up;
                        } elseif ($day_all_data[$last_date][$key]['all_rate'] > $val['all_rate']) {
                            $day_all_data[$date][$key]['all_rate_status'] = $down;
                        }
                    }

                }
            }
        }
      
            krsort($day_all_data);
            $assign['gold_list'] = $data_list;
    // print_r($data_list);exit;        
            $assign['day_gold_list'] = $day_all_data;
            $assign['shop_list'] = $log_shop_res;
            $assign['gold_type_list'] = $this->get_config_list(11);
          
            $assign['gold_type'] = $gold_type;
            $assign['op'] = $op == 2 ? Ext_Template::lang('消耗') : Ext_Template::lang('产出');
   
        $this->display('consumptionOutput_statistics.shtml', $assign);
    }

    //物品消耗统计
    public function output_goods(){
        $this->goods_statistics(2);
    }
    //物品产出统计
    public function input_goods(){
        $this->goods_statistics(1);
    }

    //物品产出消耗统计
    public function goods_statistics($op = 2)
    {
        $role_id = request('role_id', 'str');
       
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $type = request('type', 'str') ? request('type', 'str') : 'gid' ;

        $where = 'Where 1';
        $log_shop_where = 'Where 1';
        if ($role_id) {
            $where .= " and role_id = '$role_id' ";
            $log_shop_where .= " and role_id = '$role_id' ";
        }

        if ($start_time) {
            $stime = strtotime($start_time);
        } else {
            $start_time = date('Y-m-d', time()); //默认拿1天的数据
            $stime = strtotime($start_time);
        }
        if ($end_time) {
            $etime = strtotime($end_time) + 86400;
            if (($etime - $stime) > 86400 * 30) {
                $stime = $etime - 86400 * 1;
                $start_time = date('Y-m-d', $stime);
            }
        } else {
            $end_time = date('Y-m-d', $stime + 1 * 86400);
            $etime = strtotime($end_time) + 86400;
        }
        $_GET['start_time'] = $start_time;
        $where .= " and time >= $stime";

        $_GET['end_time'] = $end_time;
        $where .= " and time < $etime";

        
        // -- 操作消耗次数
        $sql = "select count(1) total_num from log_role_bags $where and op=$op";
        $total_num = $this->db->fetchone($sql);
        $sql = "select $type type,count(1) total_num from log_role_bags $where and op=$op GROUP BY $type order by total_num desc,type asc;";

        $count_list = $this->db->fetchRow($sql);
        foreach ($count_list as $key => $val) {
            $count_list[$key]['rate'] = round($val['total_num'] / $total_num['total_num'], 4) * 100;
        }

        // -- 操作消耗数量
        $sql = "select sum(num) total_num from log_role_bags $where and op=$op";
        $sum_num = $this->db->fetchone($sql);
        $sql = "select $type type,sum(num) total_num from log_role_bags $where and op=$op GROUP BY $type order by total_num desc,type asc;";
        $sum_list = $this->db->fetchRow($sql);
        foreach ($sum_list as $key => $val) {
            $sum_list[$key]['rate'] = round($val['total_num'] / $sum_num['total_num'], 4) * 100;
        }

        $assign['count_list'] = $count_list;
        $assign['sum_list'] = $sum_list;
        $assign['type_list'] = array('gid'=>Ext_Template::lang('物品变化'),'opt'=>Ext_Template::lang('操作类型'));
        $assign['type'] = $type;
        $assign['type_config_list'] = $type =='gid'?$this->get_config_list(1):$this->get_config_list(2);
        $assign['gold_type'] = $gold_type;
        $assign['op'] = $op == 2 ? Ext_Template::lang('消耗') : Ext_Template::lang('产出');
 
        $this->display('goods_statistics.shtml', $assign);
    }


}