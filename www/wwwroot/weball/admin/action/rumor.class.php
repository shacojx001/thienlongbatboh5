<?php
/**
 * 武侠游戏项
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_rumor extends action_superclass
{
    /**
     * 此处叫传闻!
     */
    public function rumor(){
        $assign = $this->get_server_rights();
        $where = ' where 1 and is_del = 0';

        $gid = request('gid','int');
        $sid = request('sid','int');

        $where .= $this->get_where_g($gid,$this->get_allgroup_list());
        if($sid){
            $where .= " and (FIND_IN_SET($sid,sids) or sids='')";
        }

        $total_record = $this->admindb->fetchOne("SELECT COUNT(1) as total from rumor_new $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from rumor_new $where order by id desc $limit";
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );

        $this->display('rumor.shtml', $assign);
    }

    public function rumor_action(){
        Auth::check_action(Auth::$RUMOR_MAN);
        $post['gid'] = request('gid','int','P');
        $post['sids'] = request('sids');
        $post['channels'] = request('channels');
        $post['interval'] = request('interval','int');
        $post['times'] = request('times','int');
        $post['begin_time'] = request('begin_time');
        $post['end_time'] = request('end_time');
        $post['begin_time'] = $post['begin_time']?strtotime($post['begin_time']):0;
        $post['end_time'] = $post['end_time']?strtotime($post['end_time']):0;
        $post['content'] = request('content','str');
        $post['type'] = request('rumorType','str');
        // $post['if_flow'] = request('if_flow','int');
// print_r($post);exit;
        if(!$post['gid']) alert('请选一个专服!');
        if(!$post['interval']) alert('发送间隔必须大于0秒');
        // if(!$post['times']) alert('发送次数必须大于0次');
        if(!$post['begin_time'] || $post['begin_time'] <= time()) alert('开始时间必须且大于当前时间!');
        if(!$post['end_time'] || $post['end_time'] <= $post['begin_time']) alert('结束时间必须且大于开始时间!');
        if(!$post['content']) alert('发送内容不能为空!');

        if($post['sids']){
            foreach ($post['sids'] as $k => $sid) {
                $sid = (int)$sid;
                if(!$sid){
                    unset($post['sids'][$k]);
                }else{
                    $post['sids'][$k] = $sid;
                }

            }
            if(empty($post['sids'])) {
                $post['sids'] = '';
            }else{
                sort($post['sids']);
                $post['sids'] = implode(',',$post['sids']);
            }
        }else{
            $serverlist = $this->get_allserver_list($post['gid']);
            $sids = array_keys($serverlist);
            $serverStr = implode(',',$sids);
            $post['sids'] = $serverStr;
        }

        if($post['channels']){
            foreach ($post['channels'] as $k => $channel) {
                $channel = (string)$channel;
                if(!$channel){
                    unset($post['channels'][$k]);
                }else{
                    $post['channels'][$k] = $channel;
                }

            }
            if(empty($post['channels'])){
                $post['channels'] = '';
            }else{
                sort($post['channels']);
            }

            foreach ($post['channels'] as $key => $val) {
                $channel_array[] = '{source,"'.$val.'"}';
            }
            $scope = "[".implode(",", $channel_array)."]";   

            $post['channels'] = implode(',',$post['channels']);
        }else{
            $scope = "world";
        }

        $sids = array();
        if($post['sids']){
            $sids = explode(',',$post['sids']);
        }
     
        if(!$sids){
            alert('没有可选游戏服!');
        }else{

            //写库
            $re = $this->admindb->insert('rumor_new',$post);

            $post['id'] = $re;
            $post['scope'] = $scope;
            $sql = $this->rumor_sql_reutrn($post);

            $res_list = '';
            foreach ($sids as $sid) {
                if($sid !=1) continue;
                //入游戏服库
                Config::gamedb($sid)->fetchOne($sql);
                $ret = Erlang::erl_rumor_all($sid);    
                $res_list[] = $sid.":".$ret;
            }
            $post['callback'] = $res_list ? implode(';',$res_list) : '';
            $sql = "UPDATE rumor_new SET callback= '{$post['callback']}' WHERE id={$post['id']} ";
            $this->admindb->query($sql);
            //更新通知服务端情况
            if($re){
                $sid_str = implode(',',$sids);
                Admin::adminLog("传闻发送:gid={$post['gid']};sid={$sid_str};channel={$post['channels']}");
                alert('发送成功!'.$post['callback']);
            }
            alert('发送异常!');
        }
    }
    //拼接sql
    public function rumor_sql_reutrn($post){
        $sql = "INSERT INTO rumor (
                        id,
                        type,
                        scope,
                        timing,
                        begin_time,
                        end_time,
                        `interval`,
                        times,
                        content,
                        valid
                    )
                    VALUES
                        (   '{$post['id']}',
                            '{$post['type']}',
                            '{$post['scope']}',
                            '1',
                            '{$post['begin_time']}',
                            '{$post['end_time']}',
                            '{$post['interval']}',
                            '{$post['times']}',
                            '{$post['content']}',
                            '1'
                        )";
        return $sql;                
    }

    /**
     * 删除公告
     */
    public function rumor_del(){
        $id = request('id','int');
        if(!$id){
            alert('缺少参数');
        }

        $sql = "select * from rumor_new where id = {$id}";
        $re = $this->admindb->fetchOne($sql);
        if(!$re){
            alert('找不到公告!');
        }
        $sids = explode(',', $re['sids']);
        $del_call = array();
        foreach ($sids as $v) {
            $sid = $v;
            //删除游戏服那记录
            $sql = "UPDATE  rumor SET valid = 0  WHERE id=$id ";
            Config::gamedb($sid)->fetchOne($sql);
            $res = Erlang::erl_rumor_del($sid);
            $del_call[] = $sid . ':' .$res;
        }
        $re['callback'] .= "|||".implode(';',$del_call);
        $sql = "update rumor_new set is_del=1,callback = '{$re['callback']}' where id = $id";
        $this->admindb->query($sql);
        
        alert('删除成功!'.$re['callback']);



    }

    /**
     * 公告获取服列表
     */
    public function ajax_allserver_list(){
        $gid = request('gid','int');
        $re = parent::ajax_allserver_list($gid);
        exit($re);
    }

    /**
     * 公告获取渠道列表
     */
    public function ajax_channel_list_select_by_gid(){
        $re = parent::ajax_channel_list_select_by_gid();
        exit($re);
    }

    //平台传闻
    function rumor_all()
    {
        //获取服务器列表权限
        $assign = $this->get_server_rights();
        $type = array(
            2 => '普通类型',
            3 => '开服公告',
        );
        $show = ' where 1';
        if($_SESSION['super'] != 'YES'){
            if ($_SESSION['channel']) {
                foreach ($_SESSION['channel'] as $key => $val) {
                    if (strpos($show, "FIND_IN_SET")) {
                        $show .= " OR  FIND_IN_SET('$val', channel_list) ";
                    } else {
                        $show .= " AND ( FIND_IN_SET('$val', channel_list) ";
                    }
                }
                $show .= ')';
                if($_SESSION['gids']){
                    $gshow = '';
                    foreach ($_SESSION['gids'] as $key => $val) {
                        if (strpos($gshow, "FIND_IN_SET")) {
                            $gshow .= " OR FIND_IN_SET('$val', gid_list) ";
                        } else {
                            $gshow .= " AND ( FIND_IN_SET('$val', gid_list) ";
                        }
                    }
                    $gshow .= ')';
                    $show .= $gshow;
                }
            }
        }

        if ($_REQUEST['show'] == 2) {//过期显示
            $show .= " and end_time< " . time();
        } else {//过期不显示
            $show .= " and end_time>" . time();
        }

        $sql = "SELECT id, type, timing, IF (begin_time,FROM_UNIXTIME(begin_time,'%Y-%m-%d %H:%i:%S'),0) AS begin_time,
                IF (end_time,FROM_UNIXTIME(end_time,'%Y-%m-%d %H:%i:%S'),0) AS end_time,
                   `interval`, times,content,valid,scope,channel_list,gid_list,error_info
                  FROM rumor $show and valid = 0 order by id desc";

        $res = $this->admindb->fetchRow($sql);
        $assign['type'] = $type;
        $assign['NoticeList'] = $res;
        $this->display('notice_all.shtml', $assign);


    }

    function rumor_all_insert()
    {
        Auth::check_action(Auth::$RUMOR_MAN);
        $begin_time = $_REQUEST['begin_time'] ? strtotime($_REQUEST['begin_time']) : 0;
        $end_time = $_REQUEST['end_time'] ? strtotime($_REQUEST['end_time']) : 0;
        $type = $_REQUEST['type'];
        $post['type'] = 0;
        if($type == 2 && $end_time && $end_time<time()){
            $post['type'] = 0;
            alert('结束时间不能小于当前时间');
        }
        if($type == 3){
            $post['type'] = 1;
            $begin_time = $end_time = 0;
        }
        $interval = (int)$_REQUEST['interval'];
        $content = str_replace("'", '', str_replace('"', '', $_REQUEST['content']));
        $times = (int)$_REQUEST['times'];
        if($interval<=0){
            alert('发送间隔必须大于0秒');
        }
        $gid_list_str = $_REQUEST['gid_list']?implode(',', $_REQUEST['gid_list']):'';
        if (empty($interval)) {
            alert("间隔时间不能为0！");
            exit;
        }
        if (empty($gid_list_str)) {
            alert('请选择一个专服！');
        }
        $channel_array = array();
        if (!empty($_REQUEST['channel'])) {
            foreach ($_REQUEST['channel'] as $key => $val) {
                $channel_array[] = (string)$val;
            }
            //$channel_array = implode(',',$channel_array);
        }else{
            $my_channel = $this->get_channel_list($gid_list_str);
            if(empty($my_channel)){
                alert('您没有可选渠道!');
            }
            $channel_array = array_keys($my_channel);
        }
        $channel_array = array_filter($channel_array);
        $channel_str = $channel_array?implode(',',$channel_array):'';

        $post['begint'] = $begin_time;
        $post['endt'] = $end_time;
        $post['interval'] = $interval;
        $post['times'] = $times;
        $post['content'] = $content;
        $post['channel_list'] = $channel_array;

        //查询专服下面所有正式服
        $sql = "SELECT id AS sid, sdb FROM adminserverlist WHERE gid IN ($gid_list_str) AND `default` = 1 AND UNIX_TIMESTAMP(NOW()) > otime";
        $sid_list = $this->admindb->fetchRow($sql);
        $res_list = array();//结果
        foreach ($sid_list as $key => $val) {
            if($val['sid']!=1) continue;
            $ret = Erlang::erl_rumor_all($val['sid']);
            //if($ret!='ok'){
                $res_list[] = $val['sid'].":".json_encode($ret);
            //}
        }
        $res_list = implode(';',$res_list);

        $sql = "INSERT INTO rumor ( `type`, begin_time, end_time, `interval`, times, content, gid_list, channel_list,  error_info)
                    VALUES  ('$type','$begin_time','$end_time','$interval','$times','$content','$gid_list_str','$channel_str','$res_list')";
        $this->admindb->query($sql);

        Admin::adminLog("传闻发送:type=$type;gid=$gid_list_str;channel=$channel_str");
        alert("提交成功");

    }

    //专服传闻删除
    function detele_rumor_all()
    {
        Auth::check_action(Auth::$RUMOR_MAN);
        $id = request('id','int');
        if (empty($id)) {
            alert("请选择需要删除的传闻！");
            exit;
        }

        $sql = "UPDATE rumor SET valid = 1 WHERE id ={$id}";
        Admin::adminLog("传闻删除:$id");
        $this->admindb->query($sql);
        alert("删除成功！");
        exit;

    }

    //传闻获取专服下的渠道
    function ajax_gid_channel()
    {
       $this->ajax_rumor_channel_list_checkbox();
    }




}