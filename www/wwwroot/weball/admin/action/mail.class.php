<?php
/**
 * 邮件
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_mail extends action_superclass
{
    /**
     * 邮件日志
     */
    public function log_system_mail()
    {

        $assign['act'] = 'log_system_mail';
        $where = " WHERE 1 ";
        $orderby = " order by create_time desc ";
        $url = "action_gateway.php?ctl=mail&act=log_system_mail";

        $role_id = $_GET['role_id'];
        if (!empty($role_id)) {
            $where .= " and a.role_id = '$role_id' ";
            $url .= "&role_id=$role_id";
        }

        $nickname = $_GET['nickname'];
        if (!empty($nickname)) {
            $where .= " and b.nick_name like '%$nickname%' ";
            $url .= "&nickname=$nickname";
        }

        $start_time = $_GET['start_time'];
        if (!empty($start_time)) {
            $starttime = strtotime($start_time);
            $where .= " AND a.create_time >= '$starttime' ";
            $url .= "&time=$start_time";
        }

        $end_time = $_GET['end_time'];
        if (!empty($end_time)) {
            $endtime = strtotime($end_time);
            $where .= " AND a.create_time <= '$endtime' ";
            $url .= "&time=$end_time";
        }

		$goods_id = $_GET['goods_id'];
		if(!empty($goods_id)){
			$where .= " and a.attachment like '%$goods_id%' ";
			$url .= "&goods_id=$goods_id";
		}

        $title = $_GET['title'];
        if (!empty($title)) {
            $where .= " and a.caption like '%$title%' ";
            $url .= "&title=$title";
        }
        $sql = "select count(*) as total from role_email a left join role_basic b on a.role_id=b.role_id $where";

        $total_record = $this->db->fetchOne($sql);
        if (!empty($total_record)) {
            $total_record = $total_record['total'];
        } else {
            $total_record = 0;
        }

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select a.role_id,b.nick_name as nickname,a.type,a.caption as title,a.content,a.is_read,a.attachment,
				CONCAT(
					if(is_read=1,'".Ext_Template::lang('已读')." ','".Ext_Template::lang('未读')." '),
					if(is_attachment_get=1,'".Ext_Template::lang('已领附件')."',''),
					if(is_deleted=1,'".Ext_Template::lang('已删除')." ','')
					) as state,a.create_time as time
				from role_email a left join role_basic b on a.role_id=b.role_id $where $orderby $limit";
        $list = $this->db->fetchRow($sql);

        //解析物品
        foreach ($list as &$val) {
            $val['str'] = Goods::explain_goods($val['attachment']);
        }

        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );
        $this->display('log_system_mail.shtml', $assign);
    }

    /**
     * 申请单服邮件-含物品
     */
    public function review_mail_list()
    {
        //获取服务器列表权限
        $assign = $this->get_server_rights();
        $assign['srv_group'] = $this->get_allgroup_list();
        $assign['gid'] = $gid = $_REQUEST['gid'];
        $assign['sid'] = $sid = $_REQUEST['sid'];
        if($gid){
            $assign['srv_list_bygroup'] = $this->get_allserver_list($_REQUEST['gid']);
            if($sid){
                $assign['role_list'] = $this->get_config_list('role_list', '' ,$sid);
                
                $assign['goodsNum'] = range(1,5);
            }
            // print_r(range(1,5));exit;
        }
        
        $assign["BindType"] = array(1 => Ext_Template::lang('已绑定'), 0 => Ext_Template::lang('无'));
        
        $this->display('review_mail.shtml', $assign);
        
        
    }


    /**
     * 单服邮件申请 提交
     */
    public function review_mail_action()
    {
// print_r($_REQUEST);exit;
        Auth::check_action(Auth::$MAIL_APPLY);
        $sid = $_POST['sid'];
        $gid = $_POST['gid'];

        if (empty($sid)) {
            Ext_Template::lang('请选择发送游戏服！');
            exit;
        }
        $lv = $_POST['lv'] ? "{lv,".$_POST['lv']."}" : "{lv,9999}";
        //如果存在渠道
        if ($_POST['channel'] == 'all' || $_POST['channel'] == '') {
            $source = "";
        } else {
            $source = "{$_POST['channel']}";
        }

        $title = $_POST['title'];
        $content = $_POST['content'];

        $goods = array();
        if($_POST['good']) {
            foreach ($_POST['good']['goods_id'] as $key => $val) {
                if(empty($val)) continue;
                $goods_id = $val;
                $goods_num = $_POST['good']['num'][$key]?$_POST['good']['num'][$key]:0;
                $is_bind = $_POST['good']['bind'][$key]?$_POST['good']['bind'][$key]:0;
                $time = strtotime($_POST['good']['overTime'][$key]) ? strtotime($_POST['good']['overTime'][$key]) : 0;
                $goods[] = "{"."$goods_id,$goods_num,$is_bind,$time}";
            }
            if(count($goods)>0){
                $post = "[{5,[" . implode(",", $goods) . "]}]";
            }else{
                $post = "[]";
            }
        }
        switch ($_POST['type']) {
            case 0: //指定玩家
                ///角色列表
                if ($_POST['role_id']) {
                    $id_list = "[" . $_POST['role_id'] . "]";
                } else {
                    echo Ext_Template::lang('请选择角色');exit;
                }

                //查看是否需要审核或者定时邮件
                list($code, $type, $timing) = Mail::check_audit($_POST['type']);

                if (!$code) {
                    Mail::send_mail_sid($type, $gid, $sid, $lv, $id_list, $title, $content, $post, '', $timing);
                    echo Ext_Template::lang('已经提交审核，请通知管理员审核发放！');
                    exit;
                }                   
                $result = Erlang::erl_send_mail_new($sid, $id_list, $title, $content, $post);
                Admin::adminLog("单服邮件:无需审核给指定玩家发邮件。",1);
                echo Ext_Template::lang('发送成功！' . $result); exit;
                break;
            case 1: //全部玩家
                //查看是否需要审核或者定时邮件
                list($code, $type, $timing) = Mail::check_audit($_POST['type']);
                if (!$code) {
                    Mail::send_mail_sid($type, $gid, $sid, $lv, $id_list, $title, $content, $post, '', $timing);
                    echo Ext_Template::lang('已经提交审核，请通知管理员审核发放！');
                    exit;
                }    

                $result = Erlang::erl_send_mail_all_new($sid, $lv, $title, $content, $post);
                // print_r($result);exit;
                Admin::adminLog("单服邮件:无需审核给全部玩家发邮件。",1);
                echo Ext_Template::lang('全部玩家发送成功！' . $result);
                break;
            case 2: //在线
                //查看是否需要审核或者定时邮件
                list($code, $type, $timing) = Mail::check_audit($_POST['type']);
                if (!$code) {
                    Mail::send_mail_sid($type, $gid, $sid, $lv, $id_list, $title, $content, $post, '', $timing);
                    echo Ext_Template::lang("已经提交审核，请通知管理员审核发放！");
                    exit;
                }    

                $result = Erlang::erl_send_mail_online_new($sid, $lv, $title, $content, $post, $source);
          
                Admin::adminLog("单服邮件:无需审核给在线玩家发邮件。",1);
                echo Ext_Template::lang('在线发送成功！' . $result);
                break;
            default:
                echo Ext_Template::lang('请选择发送类型');
                break;
        }
    }

    /**
     * 申请专服邮件
     */
    public function mail_sever_list()
    {
        $assign['srv_group'] = $this->get_allgroup_list();

        //存在gid,则把相应平台下的游戏服全部查出来
        $sever_list = $channel_list = array();
        $gid = request('gid','int');
        if ($gid) {
            $sever_list = $this->get_allserver_list($gid);
    // print_r($sever_list);exit;        
            // $channel_list = $this->get_channel_list($gid);
        }

        $assign['sever_list'] = $sever_list;
        $assign['gid'] = $gid;
        $assign['goodsNum'] = range(1,5);

        $this->display('mail_sever_list.shtml', $assign);
        

    }

    //单平台邮件发送
    public function mail_sever_list_send()
    {       
        Auth::check_action(Auth::$MAIL_APPLY);
        $sid_list = implode(',', $_POST['sid_list']);
        $gid = $_POST['gid'];

        if (empty($sid_list)) {
            alert(Ext_Template::lang('请选择发送游戏服！'));
            exit;
        }
        $lv = $_POST['lv'] ? "\{lv,".$_POST['lv']."\}" : "\{lv,9999\}";

        //如果存在渠道
        if ($_POST['channel'] == '') {
            $source = "";
        } else {
            $source = "{$_POST['channel']}";
        }

        $title = $_POST['title'];
        $content = $_POST['content'];

        $goods = array();
        if($_POST['good']) {
            foreach ($_POST['good']['goods_id'] as $key => $val) {
                if(empty($val)) continue;
                $goods_id = $val;
                $goods_num = $_POST['good']['num'][$key]?$_POST['good']['num'][$key]:0;
                $is_bind = $_POST['good']['bind'][$key]?$_POST['good']['bind'][$key]:0;
                $time = strtotime($_POST['good']['overTime'][$key]) ? strtotime($_POST['good']['overTime'][$key]) : 0;
                $goods[] = "{"."$goods_id,$goods_num,$is_bind,$time}";
            }
            if(count($goods)>0){
                $post = "[{5,[" . implode(",", $goods) . "]}]";
            }else{
                $post = "[]";
            }
        }

        switch ($_POST['type']) {
            case 3: //全部玩家
                Mail::send_mail_gid($_POST['type'], $gid, $sid_list, $lv, $title, $content, $post, '', $timing);
                echo Ext_Template::lang('已经提交审核，请通知管理员审核发放！');
                exit;
                break;
            case 4: //在线
                Mail::send_mail_gid($_POST['type'], $gid, $sid_list, $lv, $title, $content, $post, '', $timing);
                echo Ext_Template::lang('已经提交审核，请通知管理员审核发放！');
                exit;
                break;
            default:
                echo Ext_Template::lang('请选择发送类型');
                break;
        }


    }

    // 邮件审核_显示
    function audit_mail()
    {
// print_r($_REQUEST);       
        $_REQUEST['act'] = 'audit_mail_data';
        $kefu = $_REQUEST['kefu'] ? 'kefu=1&' : '';
        $assign['url'] = "/action_gateway.php?{$kefu}" . http_build_query($_REQUEST);
        if($_REQUEST['kefu']==1){
            $assign['edit'] = array('event'=>'','class'=>'disabled');
            $assign['del'] = array('event'=>'','class'=>'disabled');
        }else{
            $assign['edit'] = array('event'=>'lay-event="edit"','class'=>'normal');
            $assign['del'] = array('event'=>'lay-event="del"','class'=>'danger');
        }    
        $assign['kefu'] = $_REQUEST['kefu'];
        $assign['role_list'] = $this->get_config_list('role_list'); 
    
        $this->display('audit_mail.shtml', $assign);
    }

    function audit_mail_data(){
        //可以查看游戏服列表
        $where = " where 1 ";
     
        $sid_list = $this->get_allserver_bygid();
        if ($_REQUEST['gid']) {
            $where_sid = " AND gid=" . $_REQUEST['gid'];
        } else {
            $gid_list = $this->get_allgroup_list();
            $gids = array_keys($gid_list);
            if($gids){
                $where_in = " AND gid in (".implode(',',$gids).")";
            }
        }

        $statu = $_REQUEST['statu'] ? $_REQUEST['statu'] : 0;
        $where .= " AND statu =$statu ";

        if ($_REQUEST['goods_id']) {
            $where .= " AND post like '%{$_REQUEST['goods_id']}%' ";
        }
        $sql = "select count(1) as total from audit_mail   $where $where_sid $where_in ";
        $total_record = $this->admindb->fetchOne($sql);

        $count = $total_record['total'];
        $page = $_REQUEST['page']-1;
        $limit = "limit $page, {$_REQUEST['limit']}";
        $sql = "SELECT
                    id,
                    CASE type
                  WHEN 0 THEN
                    '".Ext_Template::lang('指定')."'
                  WHEN 1 THEN
                    '".Ext_Template::lang('全部')."'
                  WHEN 2 THEN
                    '".Ext_Template::lang('在线')."'
                  WHEN 3 THEN
                    '".Ext_Template::lang('专服全部')."'
                  WHEN 4 THEN
                    '".Ext_Template::lang('专服在线')."'
                  WHEN 8 THEN
                    '".Ext_Template::lang('VIP玩家')."'
                  WHEN 10 THEN
                    '".Ext_Template::lang('定时-指定')."'
                  WHEN 11 THEN
                    '".Ext_Template::lang('定时-全部')."'
                  WHEN 12 THEN
                    '".Ext_Template::lang('定时-在线')."'
                  WHEN 13 THEN
                    '".Ext_Template::lang('定时-全部')."'
                  WHEN 14 THEN
                    '".Ext_Template::lang('定时-在线')."'
                  END AS type,
                   gid,
                   if(sid=0&sid_list!='',sid_list,sid) as sid,
                   lv,
                   id_list,
                   title,
                   content,
                   post,
                  CASE statu
                    WHEN 0 THEN
                      '".Ext_Template::lang('未审核')."'
                    WHEN 1 THEN
                      '".Ext_Template::lang('已审核')."'
                    WHEN 2 THEN
                      '".Ext_Template::lang('拒绝')."'
                    END AS statu,
                   submiter,
                   auditer,
                   FROM_UNIXTIME(
                    submitimte,
                    '%Y-%m-%d %H:%i'
                  ) AS submitimte,

                  IF (
                    auditime != '',
                    FROM_UNIXTIME(auditime, '%Y-%m-%d %H:%i'),
                    0
                  ) AS auditime
                  FROM
                    audit_mail
                   $where $where_sid $where_in
                  ORDER BY
                    statu desc,id desc
                  $limit  ";
        $list = $this->admindb->fetchRow($sql);
// print_r($sql);exit;
        //写入缓存
        $goods_list = $this->get_config_list(1);
        foreach ($list as $key => $val) {
            $list[$key]['str'] = Goods::explain_goods($val['post'], $goods_list);
            // $list[$key]['str'] = $val['post'];
        }
        $sid_list = $this->get_config_list('sid_list'); 
        foreach ($list as $key => $val) {
            $cs = " ";
            foreach (explode(',', $val['sid']) as $k => $v) {
                if ($cs == " ") {
                    $cs .= $sid_list[$v];
                } else {
                    $cs .= "," . $sid_list[$v];
                }
            }
            $list[$key]['sid'] = $cs;
        }
        echo $this->echo_lay_json(0, 'ok', $list, $count);           
    }


    // 邮件审核
    function audit_mail_pass()
    {
    
        Auth::check_action(Auth::$MAIL_MAN);
        $id = $_GET['id'];
        if (empty($id)) {
            echo Ext_Template::lang('请选择审核邮件');
            exit;
        }

        // 查询需要审核的邮件
        $sql = "SELECT * FROM audit_mail WHERE id = $id ";
        $list = $this->admindb->fetchOne($sql);
        if ($list['statu'] == 1) {
            echo Ext_Template::lang('邮件已经发放');
            exit;
        }

        if ($list) {
            Admin::adminLog("审核邮件:通过$id",1);
            $sid = $list['sid'];
            $source = $list['source'];
            $id_list = $list['id_list'];
            $title = $list['title'];
            $content = $list['content'];
            $post = $list['post'];
            $sid_list = $list['sid_list'];
            $lv = $list['lv'];
            
            $time = time();
            $auditer = $_SESSION['username'];
            
            switch ($list['type']) {
                case 0: //指定玩家
                    $result = Erlang::erl_send_mail_new($sid, $id_list, $title, $content, $post);
                    $erl_re = json_encode($result);
                    //修改状态
                    $sql = "UPDATE audit_mail SET statu = 1, auditime = '$time', auditer = '$auditer',result= '$erl_re' WHERE id = $id";
                    $list = $this->admindb->query($sql);
                    echo Ext_Template::lang("发送成功！".$erl_re);exit;

                    break;
                case 1: //全部玩家
                    $result = Erlang::erl_send_mail_all_new($sid, $lv, $title, $content, $post, $source);
                    $erl_re = json_encode($result);
                    //修改状态
                    $sql = "UPDATE audit_mail SET statu = 1, auditime = '$time', auditer = '$auditer',result= '$erl_re' WHERE id = $id";
                    $list = $this->admindb->query($sql);
                    echo Ext_Template::lang("全部玩家发送成功！".$erl_re);exit;
                    
                    break;
                case 2: //在线
                    
                    $result = Erlang::erl_send_mail_online_new($sid, $lv, $title, $content, $post, $source);
                    $erl_re = json_encode($result);
                    //修改状态
                    $sql = "UPDATE audit_mail SET statu = 1, auditime = '$time', auditer = '$auditer',result= '$erl_re' WHERE id = $id";
                    $list = $this->admindb->query($sql);
                    echo Ext_Template::lang("在线发送成功！".$erl_re);exit;
                    break;
                case 3: //专服全部玩家
                    //循环游戏服列表
                    $erl_res = array();
                    foreach (explode(',', $sid_list) as $key => $val) {
                        $result = Erlang::erl_send_mail_all_new($val, $lv, $title, $content, $post, $source);
                        $erl_re = json_encode($result);
                        $erl_res[] = $val.':'.$erl_re;
                    }
                    $erl_res = implode(',',$erl_res);

                    //修改状态
                    $sql = "UPDATE audit_mail SET statu = 1, auditime = '$time', auditer = '$auditer',result= '$erl_res' WHERE id = $id";
                    $list = $this->admindb->query($sql);
                    echo Ext_Template::lang("全部玩家发送成功！".$erl_re);exit;
                    break;
                case 4: //专服在线
                    //循环游戏服列表
                    $erl_res = array();

                    foreach (explode(',', $sid_list) as $key => $val) {
                        
                        $result = Erlang::erl_send_mail_online_new($val, $lv, $title, $content, $post, $source);
                        $erl_re = json_encode($result);
                        $erl_res[] = $val.':'.$erl_re;
                    }
                    $erl_res = implode(',',$erl_res);
                    //修改状态
                    $sql = "UPDATE audit_mail SET statu = 1, auditime = '$time', auditer = '$auditer',result= '$erl_res' WHERE id = $id";
                    $list = $this->admindb->query($sql);
                    echo Ext_Template::lang("在线发送成功！".$erl_re);exit;
                    break;
                default:
                    echo Ext_Template::lang("邮件无效！");exit;
                    break;
            }
        }  //if结束

    }

    // 邮件审核_拒绝
    function audit_mail_refuse()
    {
        Auth::check_action(Auth::$MAIL_MAN);
        $id = $_GET['id'];
        if (empty($id)) {
            echo Ext_Template::lang("请选择邮件");exit;
        }
        // 查询需要审核的邮件
        $time = time();
        $auditer = $_SESSION['username'];
        $sql = "UPDATE audit_mail SET statu = 2, auditime = '$time', auditer = '$auditer' WHERE id = $id";
        $list = $this->admindb->query($sql);
        Admin::adminLog("审核邮件:拒绝$id",1);
        echo Ext_Template::lang("拒绝邮件成功！");exit;

    }

}