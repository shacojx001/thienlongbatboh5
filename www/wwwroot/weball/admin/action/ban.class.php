<?php
/**
 * 禁言，封号
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_ban extends action_superclass
{
    public $limit_type = array(
        0 => '5分钟',
        1 => '10分钟',
        2 => '30分钟',
        3 => '1小时',
        4 => '2小时',
        5 => '6小时',
        6 => '24小时',
        7 => '3天',
        8 => '1周',
    );
    public $limit_sec = array(
        0 => 300,
        1 => 600,
        2 => 1800,
        3 => 3600,
        4 => 7200,
        5 => 43200,
        6 => 86400,
        7 => 259200,
        8 => 604800,
    );

    public function __construct()
    {   
        parent::__construct();
        $this->smarty->assign('limit_type', $this->limit_type);
    }

    public function log_ban(){
        $where = " WHERE 1 ";
        $orderby = " order by id desc ";
        $url = "action_gateway.php?ctl=ban&act=log_ban";
        $admin = $_GET['admin'];
        if(!empty($admin)){
            $where .= " and admin = '$admin' ";
            $url .= "&admin=$admin";
        }

        $type = $_GET['type'];
        if($type != ''){
            $where .= " and type = '$type' ";
            $url .= "&type=$type";
        }

        $keyword = $_GET['keyword'];
        if(!empty($keyword)){
            $where .= " and object like '%$keyword%' ";
            $url .= "&keyword=$keyword";
        }

        $start_time = $_GET['start_time'];
        if(!empty($start_time)){
            $starttime = strtotime($start_time);
            $where .= " AND time >= '$starttime' ";
            $url .= "&start_time=$start_time";
        }

        $end_time = $_GET['end_time'];
        if(!empty($end_time)){
            $endtime = strtotime($end_time);
            $where .= " AND time <= '$endtime' ";
            $url .= "&end_time=$end_time";
        }

        $total_record = $this->db->fetchOne("SELECT COUNT(*) as total from log_ban $where");
        if(!empty($total_record)){
            $total_record = $total_record['total'];
        }else{
            $total_record = 0;
        }

        $per_page = 25;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select * from log_ban $where $orderby $limit";
        $list = $this->db->fetchRow($sql);
        $assign['list'] = $list;
        
        $assign['type'] = array(0=>Ext_Template::lang('角色封号'),1=>Ext_Template::lang('IP黑名单'),2=>Ext_Template::lang('角色解封'),3=>Ext_Template::lang('禁言'),'4'=>Ext_Template::lang('解除禁言'),5=>Ext_Template::lang('设备封禁'),6=>Ext_Template::lang('设备解封'),7=>Ext_Template::lang('IP解禁'),);
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );
        $this->display('log_ban.shtml',$assign);

    }

    function talk(){
        $assign['limit_type'] = $this->limit_type;
        $this->display('user_lim_talk.shtml',$assign);
    }

    //禁言
    function lim_talk()
    {
        $sid = request('sid','int');
        if(!$sid){
            $sid = $_SESSION['selected_sid'];
        }
        Auth::check_action(Auth::$BAN_TALK);
        $role_id = trim(preg_replace("/[^0-9\,]+/", "", $_REQUEST['ids']));
        $limit_time = trim($_REQUEST['lim_time']);
        $lim_time = time() + $this->limit_sec[$limit_time];

        $nameArr = array_filter(explode(',', $names));
        $re = User::talk_limit('limit_talk',$role_id,$lim_time,'后台禁言',$sid);

        alert($re['msg']);
    }

    //IP黑名单
    function ip_black_list()
    {

        $db = Ext_Mysql::getInstance();
        //封号IP
        $sql = "select * from ban_info where type=1";
        $list = $db->fetchRow($sql);
        if (!empty($list)) {
            $iplist = array();
            foreach ($list as $val) {
                $iplist[] = $val['value'];
            }
            asort($iplist);
            $this->smarty->assign('iplist', $iplist);
        }
        $this->smarty->display("./user_lim_ip_black.shtml");
    }

    //设备号
    function dev()
    {
        $db = Ext_Mysql::getInstance();
        //封号IP
        $sql = "select * from ban_info where type=3";
        $list = $db->fetchRow($sql);
        if (!empty($list)) {
            $iplist = array();
            foreach ($list as $val) {
                $iplist[] = $val['value'];
            }
            asort($iplist);
            $this->smarty->assign('iplist', $iplist);
        }
        $this->smarty->display("user_lim_dev.shtml");
    }

    /**
     * 设备号封禁
     */
    public function lim_dev(){
        Auth::check_action(Auth::$BAN_LOGIN);
        $devs = trim(str_replace(array('"',"'"), "", $_REQUEST['devs']));
        $devs = explode(',',$devs);
        $devs = array_filter($devs);

        $sid = request('sid','int');
        if(!$sid){
            $sid = $_SESSION['selected_sid'];
        }
        $msg = User::dev_limit('limit_device',$devs, '后台设备封禁',$sid);
        alert($msg['msg']);
    }

    //设备号解封
    function unlim_dev()
    {
        Auth::check_action(Auth::$BAN_LOGIN);
        $devs = trim(str_replace(array('"',"'"), "", $_REQUEST['devs']));
        $devs = explode(',',$devs);
        $devs = array_filter($devs);

        $sid = request('sid','int');
        if(!$sid){
            $sid = $_SESSION['selected_sid'];
        }

        $msg = User::dev_limit('unlimit_device',$devs, '后台解除设备封禁',$sid);
        alert($msg['msg']);
    }

    public function index()
    {
        $db = Ext_Mysql::getInstance();

        //封号账号
        //$sql = "select a.role_id,a.accname,b.nickname from role_login a left join role_low b on a.role_id=b.role_id where a.state = 1";
        $sql = "select role_id,acc_name as accname,nick_name as nickname from role_basic where state = 4";
        $list = $db->fetchRow($sql);
        if (!empty($list)) {
            $acclist = array();
            foreach ($list as $val) {
                $acclist[$val['role_id']] = $val['accname'] . '|' . $val['nickname'].'|'.$val['role_id'];
            }
            asort($acclist);
            $this->smarty->assign('acclist', $acclist);
        }
        $this->smarty->display("./user_lim.shtml");
    }

    //解除禁言
    function unlim_talk()
    {
        $sid = request('sid','int');
        if(!$sid){
            $sid = $_SESSION['selected_sid'];
        }
        Auth::check_action(Auth::$BAN_TALK);
        $ids = trim(preg_replace("/[^0-9\,]+/", "", $_REQUEST['ids']));
        $names = trim(preg_replace("/[\'\"]+/", "", $_REQUEST['names']));
        $idArr = array_filter(explode(',', $ids));
        $nameArr = array_filter(explode(',', $names));
        foreach ($idArr as $key => $val) {
            $re = User::talk_limit('unlimit_talk',$val,0,'后台解除禁言',$sid);
        }
        alert($re['msg']);
    }

    //封号
    function lim_login()
    {
        $sid = request('sid','int');
        if(!$sid){
            $sid = $_SESSION['selected_sid'];
        }
        Auth::check_action(Auth::$BAN_LOGIN);
        $ids = trim(preg_replace("/[^0-9\,]+/", "", $_REQUEST['ids']));
        $names = trim(preg_replace("/[\'\"]+/", "", $_REQUEST['names']));
       
        $reason = request('reason','str');
        $reason = $reason?$reason:'后台帐号封号';
          
        $re = User::user_limit('limit_login',$ids,$reason,$sid);

        alert($re['msg']);
    }

    //解封
    function unlim_login()
    {
        $sid = request('sid','int');
        if(!$sid){
            $sid = $_SESSION['selected_sid'];
        }
        Auth::check_action(Auth::$BAN_LOGIN);
        $ids = trim(preg_replace("/[^0-9\,]+/", "", $_REQUEST['ids']));
        $names = trim(preg_replace("/[\'\"]+/", "", $_REQUEST['names']));
        $idArr = array_filter(explode(',', $ids));
        $nameArr = array_filter(explode(',', $names));
        $re = User::user_limit('unlimit_login',$idArr,'后台帐号解封号',$sid);
        alert($re['msg']);

    }


    //同IP封号
    function lim_ip()
    {
        Auth::check_action(Auth::$BAN_LOGIN);
        $ip_list_str = trim(preg_replace("/[^0-9\,\.]+/", "", $_REQUEST['ip']));
        $ipArr = explode(',', $ip_list_str);

        $msg = User::ipLimit($ipArr,'ban_ip', '后台添加IP黑名单');
        alert($msg, 'action_gateway.php?ctl=ban&act=ip_black_list');
    }

    //同IP解封
    function unlim_ip()
    {
        Auth::check_action(Auth::$BAN_LOGIN);
        $ip_list_str = trim(preg_replace("/[^0-9\,\.]+/", "", $_REQUEST['ip']));
        $ipArr = explode(',', $ip_list_str);
        $msg = User::ipLimit($ipArr,'unban_ip', '后台删除IP黑名单');
        alert($msg, 'action_gateway.php?ctl=ban&act=ip_black_list');
    }

}