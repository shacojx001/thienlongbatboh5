<?php
/**
 * 活跃度
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_active extends action_superclass
{
    //开服升级统计 30天
    public function upgrade_on_opensevr(){
        $assign = array();

        $days = array();
        for($i=1;$i<=30;$i++){
            $days[] = $i;
        }
        $assign['days'] = $days;

        $sid = $_SESSION['selected_sid'];
        $is_pay = request('is_pay','int');//全部/付费/非付费
        $sql = "select * from `center_upgrade_on_opensevr` where sid = {$sid} order  by `day` asc";

        $list = array();
        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $stat_pay = json_decode($v['stat_pay'],true);
                $stat_no_pay = json_decode($v['stat_no_pay'],true);


                if($is_pay == 1){
                    if($stat_pay){
                        foreach ($stat_pay as $val) {
                            $list[$val['level']][$v['day']]['role_num'] = $val['c'];
                            $list[$val['level']][$v['day']]['rate'] = sprintf('%.2f',$val['c']/$v['total']*100) . '%';
                        }
                    }
                }elseif($is_pay == 2){
                    if($stat_no_pay){
                        foreach ($stat_no_pay as $val) {
                            $list[$val['level']][$v['day']]['role_num'] = $val['c'];
                            $list[$val['level']][$v['day']]['rate'] = sprintf('%.2f',$val['c']/$v['total']*100) . '%';
                        }
                    }
                }else{
                    $tmp = array();
                    if($stat_pay){
                        foreach ($stat_pay as $val) {
                            $tmp[$val['level']][$v['day']]['role_num'] = $val['c'];
                        }
                    }
                    if($stat_no_pay){
                        foreach ($stat_no_pay as $val) {
                            $tmp[$val['level']][$v['day']]['role_num'] = $tmp[$val['level']][$v['day']]['role_num']?($tmp[$val['level']][$v['day']]['role_num']+$val['c']):$val['c'];
                        }
                    }

                    foreach ($tmp as $k=>$item) {
                        foreach ($item as $kk=>$tm) {
                            $list[$k][$kk]['role_num'] = $tm['role_num'];
                            $list[$k][$kk]['rate'] = sprintf('%.2f', $tm['role_num']/$v['total']*100) . '%';
                        }
                    }
                    ksort($list);
                }

            }

        }
        $assign['list'] = $list;
        //var_dump($list);die;

        $this->display('upgrade_on_opensevr.shtml',$assign);
    }

    private function deal_stat_map($stat){
        $data = array();
        if($stat){
            foreach ($stat as $v) {
                $data[$v['level']] = $v['c'];
            }
        }

        return $data;
    }

    public function statistics_register(){
        $assign = array();

        $sid = $_SESSION['selected_sid'];
        $start_time = request('start_time');
        $end_time = request('end_time');
        $online = request('online','int');//全部/在线/离线
        $login = request('login','int');//全部/当天有登录/3天内有登录/7天内有登录
        $is_pay = request('is_pay','int');//全部/付费/非付费

        $where = ' where 1';
        if($sid){
            
            if($start_time){
                $where .= " and ctime > ".strtotime($start_time);
            }
            if($end_time){
                $where .= " and ctime<= ".strtotime($end_time);
            }
            if($online == 1){
                $where .= " and is_online = 1";
            }elseif($online == 2){
                $where .= " and is_online = 0";
            }
            
            if($login == 1){
                $today = strtotime(date('Y-m-d'));
                $where .= " and last_login_time> {$today}";
            }elseif($login == 2){
                $three_day = strtotime(date('Y-m-d')) - 86400 * 3;
                $where .= " and last_login_time> {$three_day}";
            }elseif($login == 3){
                $seven_day = strtotime(date('Y-m-d')) - 86400 * 7;
                $where .= " and last_login_time> {$seven_day}";
            }
            if($is_pay == 1){//付费
                $where .= " and first_recharge >0";
            }elseif($is_pay == 2){//非付费
                $where .= " and first_recharge = 0";
            }

            //总注册
            $sql = "select count(1) as c from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id $where";
// echo $sql;
            $re = Config::gamedb($sid)->fetchOne($sql);
            $total = $re['c']?$re['c']:0;

            if($total){
                //各职业注册分布
                $sql = "select b.career,count(1) as c from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id $where and b.career in (1,2,3,4) GROUP BY b.career";
// echo $sql;
                $re = Config::gamedb($sid)->fetchRow($sql);
                $career_arr = array();
                $div_width = 600;
                foreach ($re as $v) {
                    $v['stat'] = sprintf('%.2f',$v['c']/$total * 100) . '%';
                    $v['width'] = sprintf('%.2f', $div_width * $v['stat'] /100);
                    $career_arr[$v['career']] = $v;
                }

                //各等级注册分布
                $sql = "select level,count(1) as c from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id $where and level != 0 GROUP BY level";
                $re = Config::gamedb($sid)->fetchRow($sql);
                $level_arr = array();
                foreach ($re as $v) {
                    $v['stat'] = sprintf('%.2f',$v['c']/$total * 100) . '%';
                    $v['width'] = sprintf('%.2f', $div_width * $v['stat'] /100);
                    $level_arr[$v['level']] = $v;
                }
                $assign['total'] = $total;
                $assign['career_arr'] = $career_arr;
                $assign['level_arr'] = $level_arr;
                $assign['div_width'] = $div_width;

                // print_r($career_arr);exit;
            }

        }

        $this->display('statistics_register.shtml',$assign);
    }

    /**
     * 登陆统计
     */
    public function logincount_list()
    {
        $start_time = empty($_GET['start_time']) ? strtotime(date('Y-m-d')) - 86400 * 6 : strtotime($_GET['start_time']);  //开始时间，默认为6天前
        $end_time = empty($_GET['end_time']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime($_GET['end_time'] . '23:59:59');  //结束时间，默认为当前23:59
        $_REQUEST['start_time'] = date('Y-m-d', $start_time);
        $_REQUEST['end_time'] = date('Y-m-d', $end_time);
        $assign['ctime'] = empty($_GET['ctime']) ? '' : strtotime($_GET['ctime']);
        if (empty($_GET['ctime'])) {
            $_GET['ctime'] = date('Y-m-d', $end_time);
            $assign['ctime'] = strtotime($_REQUEST['end_time']);
        }

        if ($_SESSION['ad_sign']) {
            $ad_sign = $_SESSION['ad_sign'];
        }

        if (!$_SESSION['selected_sid']) {       //如果没选选择平台和服务器，提示选择
            echo '<script>alert("请选择平台和服务器！")</script>';
            exit;
        }
        $sid = $_SESSION['selected_sid'];     //获取当前选择的服务器id
        $assign['signarr'] = $this->get_sign_type_for_count($sid); //获取该服务器下所有的渠道

        if ($_GET['sign'] && $_GET['sign'] != 'all') {     //如果选择了渠道并且不是全部则将已选择的渠道放入session
            $ad_sign = $_GET['sign'];
            $_SESSION['ad_sign'] = $_GET['sign'];
        } elseif ($_GET['sign'] == 'all') {           //如果选择的是全部则清空session里的ad_sign
            $_SESSION['ad_sign'] = '';
            $ad_sign = '';
        }
        $cache = Ext_Memcached::getInstance('user');
        if (!empty($assign['ctime'])) {
            $cetime = $assign['ctime'] + 86399;
            $thour = array();
            for ($i = 0; $i < 24; $i++) {
                $thour[] = $i;
            }

            $assign['thour'] = json_encode($thour);
            //玩家数
            if ($ad_sign) {
                $sql = "select count(distinct(a.`role_id`)) as total,date_format(from_unixtime(`login_time`),'%k')as tdate from log_login a  where a.login_time >= '{$assign['ctime']}' and a.login_time <= '$cetime' and source ='$ad_sign' group by tdate order by a.login_time asc";
            } else {
                $sql = "select count(distinct(a.role_id)) as total,date_format(from_unixtime(`login_time`),'%k')as tdate from log_login a  where login_time >= '{$assign['ctime']}' and login_time <= '$cetime' group by tdate order by login_time asc";
            }
        
            if (!$crlist = $cache->fetch(md5($sql.$sid))) {
                $res = $this->db->fetchRow($sql);
                if (!empty($res)) {
                    $crlist = array();
                    $cr = array();
                    foreach ($res as $key => $val) {
                        $cr[$val['tdate']] = $val['total'];
                    }
                }
                foreach ($thour as $val) {
                    $crlist[] = !isset($cr[$val]) ? 0 : intval($cr[$val]);
                }
                $cache->store(md5($sql.$sid), $crlist, 300);
            }

            $assign['crlist'] = json_encode($crlist);
            //独立IP数
            if ($ad_sign) {
                $sql = "select count(distinct(a.ip)) as total,date_format(from_unixtime(`login_time`),'%k')as tdate from log_login a where a.login_time >= '{$assign['ctime']}' and a.login_time <= '$cetime' and source ='$ad_sign' group by tdate order by a.login_time asc";
            } else {
                $sql = "select count(distinct(ip)) as total,date_format(from_unixtime(`login_time`),'%k')as tdate from log_login where login_time >= '{$assign['ctime']}' and login_time <= '$cetime' group by tdate order by login_time asc";
            }
             
            if (!$cllist = $cache->fetch(md5($sql.$sid))) {
                $res = $this->db->fetchRow($sql);
                if (!empty($res)) {
                    $cllist = array();
                    $cl = array();
                    foreach ($res as $key => $val) {
                        $cl[$val['tdate']] = $val['total'];
                    }
                }
                foreach ($thour as $val) {
                    $cllist[] = !isset($cl[$val]) ? 0 : intval($cl[$val]);
                }
                $cache->store(md5($sql.$sid), $cllist, 300);
            }
            $assign['cllist'] = json_encode($cllist);
        }

        $tdate = array();
        for ($i = $start_time; $i <= $end_time; $i = $i + 86400) {
            $tdate[] = date('Y-m-d', $i);
        }
        $assign['tdatecount'] = count($tdate);
        //统计登录数
        if ($ad_sign) {
            $sql = "select count(1) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d')as tdate from log_login a LEFT JOIN role_create c on a.role_id=c.role_id where a.login_time >= '$start_time' and a.login_time <= '$end_time' and source ='$ad_sign' group by tdate order by a.login_time asc";
        } else {
            $sql = "select count(1) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d')as tdate from log_login where login_time >= '$start_time' and login_time <= '$end_time' group by tdate order by login_time asc";
        }

        if (!$lcount = $cache->fetch(md5($sql.$sid))) {
            $res = $this->db->fetchRow($sql);
            if (!empty($res)) {
                $lc = array();
                foreach ($res as $val) {
                    $lc[$val['tdate']] = $val['total'];
                }
                foreach ($tdate as $val) {
                    $lcount[] = !isset($lc[$val]) ? 0 : intval($lc[$val]);
                }
                $cache->store(md5($sql.$sid), $lcount, 300);
            }
        }
        //统计角色登录数
        if ($ad_sign) {
            $sql = "select count(distinct(a.`role_id`)) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d')as tdate from log_login a LEFT JOIN role_create c on a.role_id=c.role_id where a.login_time >= '$start_time' and a.login_time <= '$end_time' and source ='$ad_sign' group by tdate order by login_time asc";
        } else {
            $sql = "select count(distinct(`role_id`)) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d')as tdate from log_login where login_time >= '$start_time' and login_time <= '$end_time' group by tdate order by login_time asc";
        }

        if (!$rcount = $cache->fetch(md5($sql.$sid))) {
            $res = $this->db->fetchRow($sql);
            if (!empty($res)) {
                $rc = array();
                foreach ($res as $val) {
                    $rc[$val['tdate']] = $val['total'];
                }
                foreach ($tdate as $val) {
                    $rcount[] = !isset($rc[$val]) ? 0 : intval($rc[$val]);
                }
                $cache->store(md5($sql.$sid), $rcount, 300);
            }
        }
        //统计IP数
        if ($ad_sign) {
            $sql = "select count(distinct(a.ip)) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d')as tdate from log_login a LEFT JOIN role_create c on a.role_id=c.role_id where a.login_time >= '$start_time' and a.login_time <= '$end_time' and source ='$ad_sign' group by tdate order by a.login_time asc";
        } else {
            $sql = "select count(distinct(ip)) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d')as tdate from log_login where login_time >= '$start_time' and login_time <= '$end_time' group by tdate order by login_time asc";
        }

        if (!$lip = $cache->fetch(md5($sql.$sid))) {
            $res = $this->db->fetchRow($sql);
            if (!empty($res)) {
                $lic = array();
                foreach ($res as $val) {
                    $lic[$val['tdate']] = $val['total'];
                }
                foreach ($tdate as $val) {
                    $lip[] = !isset($lic[$val]) ? 0 : intval($lic[$val]);
                }
                $cache->store(md5($sql.$sid), $lip, 300);
            }
        }
        //老玩家IP数-之前有登陆过一次
        if ($ad_sign) {
            $sql = "select count(distinct(a.ip)) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d') as tdate from log_login a left join role_create c on a.role_id=c.role_id where login_time >= '$start_time' and login_time <= '$end_time' and c.ctime < unix_timestamp(date_format(from_unixtime(`login_time`),'%Y-%m-%d')) and source ='$ad_sign' group by tdate order by login_time asc";
        } else {
            $sql = "select count(distinct(a.ip)) as total,date_format(from_unixtime(`login_time`),'%Y-%m-%d') as tdate from log_login a left join role_create c on a.role_id=c.role_id where login_time >= '$start_time' and login_time <= '$end_time' and ctime < unix_timestamp(date_format(from_unixtime(`login_time`),'%Y-%m-%d')) group by tdate order by login_time asc";
        }

        if (!$oncelip = $cache->fetch(md5($sql.$sid))) {
            $res = $this->db->fetchRow($sql);
            if (!empty($res)) {
                $lic = array();
                foreach ($res as $val) {
                    $lic[$val['tdate']] = $val['total'];
                }
                foreach ($tdate as $val) {
                    $oncelip[] = !isset($lic[$val]) ? 0 : intval($lic[$val]);
                }
                $cache->store(md5($sql.$sid), $oncelip, 300);
            }
        }

        //老玩家IP数-当天跟昨天登录算老玩家
        $beforetime = $start_time - 86400;
        if ($ad_sign) {
            $sql = "select distinct(a.ip),date_format(from_unixtime(`login_time`),'%Y-%m-%d') as tdate from log_login a LEFT JOIN role_create c on a.role_id=c.role_id where login_time >= '$beforetime' and login_time <= '$end_time' and source ='$ad_sign'";
        } else {
            $sql = "select distinct(ip),date_format(from_unixtime(`login_time`),'%Y-%m-%d') as tdate from log_login where login_time >= '$beforetime' and login_time <= '$end_time'";
        }

        if (!$olip = $cache->fetch(md5($sql.$sid))) {
            $res = $this->db->fetchRow($sql);
            if (!empty($res)) {
                $iplist = array();
                foreach ($res as $key => $val) {
                    $iplist[$val['tdate']][$val['ip']] = $val['ip'];
                }

                $lic = array();
                foreach ($iplist as $key => $val) {
                    if (is_array($val) && is_array($iplist[date('Y-m-d', strtotime($key) - 86400)])) {
                        $lic[$key] = count(array_intersect_assoc($val, $iplist[date('Y-m-d', strtotime($key) - 86400)]));
                    }
                }
                foreach ($tdate as $val) {
                    $olip[] = !isset($lic[$val]) ? 0 : intval($lic[$val]);
                }
                $cache->store(md5($sql.$sid), $olip, 300);
            }
        }
    // print_r($crlist);exit;    
        $assign['selected_sign'] = $ad_sign;
        $assign['tdate'] = $tdate;
        $assign['jsdate'] = json_encode($tdate);
        $assign['lcount'] = json_encode($lcount);
        $assign['rcount'] = json_encode($rcount);
        $assign['lip'] = json_encode($lip);
        $assign['oncelip'] = json_encode($oncelip);
        $assign['olip'] = json_encode($olip);
        $this->display('logincount.shtml', $assign);
    }

    /**
     * 创角统计
     */
    public function reg_statistics()
    {
        $sql = "select channel_list from adminserverlist where id = " . $_SESSION['selected_sid'];
        $sinfo = $this->admindb->fetchOne($sql);       //获得该服务器下所有有玩家的渠道
        $signarr = explode(',', $sinfo['channel_list']);
        Ext_Debug::log($signarr);
        $condition = " AND server_id =".$_SESSION['selected_sid'];
        if (!empty($signarr)) {
            $assign['signarr'] = $signarr;
        }

        if (!empty($_GET['start_time'])) {
            $start_time = strtotime(request('start_time', 'str', 'G'));
            $condition .= " and a.reg_time>='$start_time' ";
        }
        if (!empty($_GET['end_time'])) {
            $end_time = strtotime(request('end_time', 'str', 'G'));
            $condition .= " and a.reg_time <='$end_time' ";
        }

        $assign = $this->get_statistics_by_signarr($signarr,$condition, $assign);
        $assign = $this->get_all_statistics('', $condition, $assign);

        $this->display('st_rg.shtml', $assign);

    }

    //获取各个渠道的统计数据
    private function get_statistics_by_signarr($signarr, $condition, $assign)
    {
        $signtotals = array();
        $signLvList = array();
        $signCareerList = array();

        foreach ($signarr as $sval) {

            $sql = "SELECT level as lv, count( *) as num FROM  role_basic a  where a.source ='$sval' $condition GROUP BY lv ASC ";
            $result = $this->db->fetchRow($sql);

            $signtotals[$sval] = 0;
            foreach ($result as $res) {
                $signtotals[$sval] += $res['num'];
                $signLvList[$sval][$res['lv']]['num'] = $res['num'];
            }
            if (!empty($signLvList[$sval])) {
                foreach ($signLvList[$sval] as &$Pval) {
                    $Pval['rate'] = round($Pval['num'] / $signtotals[$sval] * 100, 2);
                }
            }
            $sql = "SELECT career, count( * ) as num FROM `role_basic` a where a.source ='$sval' $condition GROUP BY career ";
            $result = $this->db->fetchRow($sql);

            foreach ($result as $res) {
                $signCareerList[$sval][$res['career']] = $res['num'];
            }

        }

        $assign['signtotals'] = $signtotals;
        $assign['signLvList'] = $signLvList;
        $assign['signCareerList'] = $signCareerList;
        $assign['signflag'] = 1;
        return $assign;
    }

    //获取全服的统计
    private function get_all_statistics($ad_sign,$condition, $assign)
    {


        if ($ad_sign) {
            $sql = "SELECT level as lv, count( *) as num FROM `role_basic` a where a.source ='$ad_sign' $condition GROUP BY lv ASC ";
        } else {
            $sql = "SELECT level as lv, count( *) as num FROM `role_basic` a where 1 $condition GROUP BY lv ASC ";
        }

        $result = $this->db->fetchRow($sql);
        $LvList = array();
        $totals = 0;
        foreach ($result as $res) {
            $totals += $res['num'];
            $LvList[$res['lv']]['num'] = $res['num'];
        }
        foreach ($LvList as &$Pval) {
            $Pval['rate'] = round($Pval['num'] / $totals * 100, 2);
        }
        if ($ad_sign) {
            $sql = "SELECT career, count( * ) as num FROM `role_basic` a where a.source ='$ad_sign' $condition GROUP BY career ";
        } else {
            $sql = "SELECT career, count( * ) as num FROM `role_basic` a where 1 $condition GROUP BY career ";
        }

        $result = $this->db->fetchRow($sql);
        $CareerList = array();
        foreach ($result as $res) {
            $CareerList[$res['career']] = $res['num'];
        }

        $assign['totals'] = $totals;
        $assign['LvList'] = $LvList;
        $assign['CareerList'] = $CareerList;
        return $assign;
    }


}