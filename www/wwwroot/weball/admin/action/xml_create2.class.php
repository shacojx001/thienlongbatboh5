<?php
/**
 * Excel 配置生成
 * @author
 */

require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_xml_create2 extends action_superclass
{
    public static $startTime = 0;
    public static $depth = array();

    public function cs()
    {   
         
        $work_dir = "/data2/www/vtnemo/dev/sh/optimizer";
        print_r($work_dir."</br>");
        print_r(ROOT_PATH.'sh/optimizer');exit;

        self::$startTime = microtime(true);

        $sql = "select * from base_xml where aid=354";
        $data = $this->admindb->fetchRow($sql);

        include(XML_PARSE_PATH."common.class.php");
        ini_set('memory_limit', '1024M');
        //缓冲输出
        header('X-Accel-Buffering: no');
        ini_set('max_execution_time', '0');
        foreach ($data as $key => $val) {
            $this->generate($val);
        }
    }


    //执行函数
    public function generate($confArgs)
    {
        echo "{$confArgs['comment']} 模块：</br>";
        $xmlFile = $confArgs['xmlfile'];
        $subTable = $confArgs['subtab'];
        $primaryKey = $confArgs['primary_key'];
        $aid = $confArgs['aid'];

        $data = $this->parse_xml($xmlFile, $subTable, $primaryKey);
print_r($data);exit;        
        $end = microtime(true);
        $time = $end - self::$startTime;
print_r('运行时间：'.$time);exit;
        if(!is_array($data)){
            return false;
        }
        if(count($data)<1){
            $this->echo_error("空数组"); 
        }

        $serverTpl = XML_PARSE_PATH."{$confArgs['template']}.svr.tpl.php";
        if ($confArgs['template'] && file_exists($serverTpl)) {
            $serverFile = SERVER_DATA_PATH."/conf_{$confArgs['template']}.erl";
            $serverSplit = SERVER_DATA_PATH."/{$confArgs['template']}/conf_{$confArgs['template']}_IndexNum.erl";
            $res = $this->gen_data_confs($serverFile, $serverTpl, $data, $serverSplit);
            if($res>0){
                echo "——服务端文件: $serverFile ok</br>";
                $this->insert_time($aid);
            } else {
                $this->echo_error("服务端文件: $serverFile 写入失败"); 
            }
        }

        $headerTpl = XML_PARSE_PATH."{$confArgs['template']}.hrl.tpl.php";
        if ($confArgs['template'] && file_exists($headerTpl)) {
            $headerFile = SERVER_HEADER_PATH."/{$confArgs['template']}.hrl";
            $res = $this->gen_data_confs($headerFile, $headerTpl, $data, '');
            if($res>0){
                echo "——服务端头文件: $headerFile ok</br>";
                $this->insert_time($aid);
            } else {
                $this->echo_error("服务端头文件: $headerFile 写入失败"); 
            }
        }

        $clientTpl = XML_PARSE_PATH."{$confArgs['template']}.cli.tpl.php";
        if ($confArgs['template'] && file_exists($clientTpl)) {
            $clientFile = CLIENT_DATA_PATH."/config_{$confArgs['template']}.lua"; 
            $clientSplit = CLIENT_DATA_PATH."/{$confArgs['template']}/config_{$confArgs['template']}_IndexNum.lua"; 
       
            $res = $this->gen_data_confs($clientFile, $clientTpl, $data, $clientSplit);
            if($res>0){
                //汉彬加的东西
                $work_dir = "/data2/www/vtnemo/dev/sh/optimizer";
                $opt_cmd = "/data2/www/vtnemo/dev/sh/optimizer/optimizer {$work_dir} {$clientFile}";
                exec($opt_cmd, $output);
                // print_r("<br>");
                // print_r("opt_cmd：".$opt_cmd);
                // print_r("<br>");
                // print_r($output);
                // print_r("<br>");
                //汉彬加的东西
                echo "——客户端文件: $clientFile ok</br>";
                $this->insert_time($aid);
            } else {
                $this->echo_error("客户端文件: $clientFile 写入失败"); 
            }
        }
        //输出内容
        ob_flush();
        flush();

        return true;
    }

    // 解析xml函数
    public function parse_xml($xmlFile, $tableFlag,  $primaryKey = null, $indexBeg = 0, $indexignore = 1)
    {
        // echo "开始解析XML文件：$xmlFile 的子表: $tableFlag</br>";
        // $xmlFile = XML_FILES_PATH.$xmlFile;
        $xmlFile = strtoupper(substr(PHP_OS,0,3))==='WIN'?iconv("utf-8","gbk",XML_FILES_PATH.$xmlFile) : XML_FILES_PATH.$xmlFile; 
        // $xmlFile = iconv("utf-8","gbk",XML_FILES_PATH.$xmlFile);
        if (file_exists($xmlFile)) {
            $xmlData = simplexml_load_file($xmlFile);
            $sheetNum = count($xmlData->Worksheet);
            $tableIndex = $sheetNum;
        
 
            if($sheetNum==0) {
                $this->echo_error($xmlFile." XML文件格式有问题，解析不出来，请重新生成！"); 
                return false;
            }

            //获取所有子表信息
            $tableList = array();
            for($i = 0; $i < $sheetNum; $i++) {
                $tableFlagCur = $xmlData->Worksheet[$i]->attributes('ss', TRUE)->Name;   
                $tableFlagCur = $this->object_to_array($tableFlagCur);
                $tableList[$i] = $tableFlagCur[0];
            }

            // 定位子表索引
            $tableList = array();
            if (is_int($tableFlag)) {//子表是数字
                // $tableIndex = $tableFlag;
                $tableList[$tableFlag] = $tableFlag;
            } else {
                //筛选出需要的子表
                $tableFlag = explode(",", $tableFlag); 
                //获取所有子表信息
                for($i = 0; $i < $sheetNum; $i++) {
                    $tableFlagCur = $xmlData->Worksheet[$i]->attributes('ss', TRUE)->Name;
                    $tableFlagCur = $this->object_to_array($tableFlagCur);
                    if(!in_array($tableFlagCur[0], $tableFlag)) continue;
                    $tableList[$i] = $tableFlagCur[0];
                }
            }
// print_r($xmlData);exit;            
            $res = array();
            foreach ($tableList as $tableIndex => $Name) {
                if ($tableIndex < $sheetNum) {
                    // 成功定位子表索引
                    $index = 0;
                    $keys = array();
                    $table = $xmlData->Worksheet[$tableIndex]->Table;
                    // 定位主键索引
                    foreach ($table->Row[$indexBeg] as $Cell) {
                        if (preg_match("/[A-Za-z0-9_]+/", $Cell->Data, $matchs) > 0) {
                            $keys[$matchs[0]] = $index;
                        }
                        $index++;
                    }
                     
                    $index = 0;
                    $rows = array();
                    // 复制表数据
                    foreach ($table->Row as $row) {
                        if($index == $indexignore) {//忽略备注项
                            $index++;
                            continue;
                        }
                        if ($index++ <= $indexBeg) {
                            continue;
                        }

                        $tmp = array();
                        $buff = array();
                        $offset = 0;

                        for ($i = 0; $i < count($row->Cell); $i++) {
                            $cellIndex = $row->Cell[$i]->attributes("ss", TRUE)->Index;
          
                            if ($cellIndex > 0) {
                                $offset = ($cellIndex - 1) - $i;
                            }
                            $val = (string)$row->Cell[$i]->Data;

                            if ($val == null) {//如果值为null时候,默认为''
                                $val = '';
                            }
                            $buff[$i + $offset] = trim($val);
                        }
                         
                        if(empty($buff[0]) && !is_numeric($buff[0]) ) {//第一个值为空数据，去掉
                            continue;
                        }    
                        // 格式化表数据
                        foreach ($keys as $key => $col) {
                            $tmp[$key] = isset($buff[$col]) ? $buff[$col] : "";
                        }

                        $rows[] = $tmp;
                    }

                    //检测主键重复
                    if($this->check_primaryKey($rows, $primaryKey)==false) {
                        return false;
                    }
                    
                } else {
                    // 定位子表索引失败，中止配置文件生成
                    $this->echo_error("XML文件: ".basename($xmlFile)." 不存在子表: $tableFlag ！"); 
                    return false;
                }
                $res[] = $this->array_to_hashmap2($rows,$primaryKey);
            }
print_r($res);exit;           
            //合并数组         
            $data = $res[0];
            for ($i=1; $i < count($res); $i++) { 
                foreach ($data as $key => $val) {
                    if($res[$i][$key]) {
                        $data[$key] = $val + $res[$i][$key];
                    }
                }
            }
          
            //打印data
            if($_GET['print']==1){
                   
                    $list = var_export($data,true);
                    echo "<style>code{font-size: 15px; font-family: initial;}</style>";
                    highlight_string($list);
                    exit;
            }
            //导表
            if($_GET['print'] == 'import_mysql') {
                $key_list = array();
                foreach ($xmlData->Worksheet->Table->Row[0]->Cell as $key => $val) {
                    $val = $this->object_to_array($val->Data);
                    $key_list[] = $val[0];
                }
                foreach ($xmlData->Worksheet->Table->Row[1]->Cell as $key => $val) {
                    $val = $this->object_to_array($val->Data);
                    $comment_list[] = $val[0];
                }
                $header = array_combine($comment_list, $key_list);
                // $header = $key_list;
              
                //导表
                $this->import_mysql($header, $data);

                alert('导表成功');
                
            }else {//服务端data
                unset($xmlData);
                unset($res);
                unset($tableList);
                return $data;
            }

        } else{
            $this->echo_error($xmlFile." XML文件不存在！"); 
            return false;
        }
    }

    /**
     * 对象 转 数组
     *
     * @param object $obj 对象
     * @return array
     */
    function object_to_array($obj) 
    {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }
     
        return $obj;
    }

    //检测重复主键
    public function check_primaryKey($data, $primaryKey = null)
    {        
        if($primaryKey == null) {
            return true;
        }
        $count = array();
        $primaryArray = explode(',', $primaryKey);
        foreach ($data as $key => $val) {
            foreach ($primaryArray as $k => $v) {
                $count[$key] = $count[$key] ? $count[$key].'_'.$val[$v] : $val[$v];
            }
        }
        
        $dif_count = array_unique($count);//去重
        if (count($count) != count($dif_count)) {   
            $result =  implode(',',array_diff_assoc($count, $dif_count));
            echo  $this->echo_error("主键 $primaryKey 有重复值：".$result); 
            return false;
        } else {
            return true;
        }
    }

    //将一个二维数组转换为 hashmap
    function array_to_hashmap2(& $arr, $keyField, $valueField = null)
    {
        $ret = array();
        if ($valueField) {
            foreach ($arr as $row) {
                $key_array = explode(",", $keyField);
                $key = '';
                foreach ($key_array as $k => $v) {
                    $key = $key == '' ? (string)$row[$v] : $key .'_'.(string)$row[$v];
                }    
                $ret[$key] = $row[$valueField];
            }
        } else {
            foreach ($arr as $row) {
                $key_array = explode(",", $keyField);
                $key = '';
                foreach ($key_array as $k => $v) {
                    $key = $key == '' ? (string)$row[$v] : $key .'_'.(string)$row[$v];
                }    
                $ret[$key] = $row;
            }
        }
        return $ret;
    }

}
