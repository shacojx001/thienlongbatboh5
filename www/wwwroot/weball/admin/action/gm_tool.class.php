<?php
/**
 * 命令配置
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_gm_tool extends action_superclass
{
    public $erl_type_list = array(
                        'int'=>'~i',//整形
                        'binary'=>'~b',//二进制
                        'string'=>'~s',//字符串
                        );
    
    public function gm_cs(){
        // $res = $this->erl_gm_list('update_filter_group',1,'[123456]');

        $res = $this->get_base_goods('color');

        print_r($res);exit;
    }

    public function gm_list_delete(){
        $id = $_REQUEST['id'];
        $sql = "delete from base_gm_list where id=$id";
        $this->admindb->query($sql);
        $this->clear_cache();
        alert('操作成功！');
    }


    public function gm_tool(){

        $res = $this->base_gm_list('id');

        $list = array();

        $type_list = $this->erl_type_list;
        
        foreach ($res as $key => $val) {

            $val['input_val'] = unserialize($val['input_val']);

            if($val['input_val']){
                $str_type = array();

                foreach ($val['input_val'] as $k => $v) {

                    $str_type[] = $type_list[$v['input_type']];   

                }

                $str_type = "[".implode(',', $str_type)."]";
            }else{
                $str_type = "[]";
            }

            $list[$val['id']] = array(
                                    'id'=>$val['id'],
                                    'type'=>$val['type'],
                                    'gm_name'=>$val['gm_name'],
                                    'gm'=>$val['gm'],
                                    'run_type'=>$val['run_type'],
                                    'module'=>$val['module'],
                                    'fun'=>$val['fun'],
                                    'input_val'=>$val['input_val'],
                                    'input_type_list'=>$str_type,
                                );
            
        }

        $assign['list'] = $list;
        $this->display(CENTER_TEMP . 'gm_tool.shtml', $assign);
    }

    //erl异步执行
    public function ajax_runing(){
        
        $sid = $_GET['sid'];
        $run_type = $_GET['run_type'];
        $module = $_GET['module'];
        $fun = $_GET['fun'];
        $input_type_list = $_GET['input_type_list'];

        $res = $_POST?$_POST:array();

        $post = array();
        foreach ($res as $key => $val) {//转类型
            
            if($val['type']=='int'){
                $post[] = intval($val['val']);
            }else{
                $post[] = $val['val'];
            }
            
        }
        
        if($run_type==3){//跨服中心
            $sid_info = $sid;
        }else{//1本服 2游戏服
            $gm_server_list = $this->get_server_list();
            $sid_info = $gm_server_list[$sid];
        }
        
        $erl = $this->erl_ling($sid,$run_type);

        $res = $erl->get($module, $fun, $input_type_list, array($post));
        // $res = 'ok';
        
        if(is_array($res)){
            $res = var_dump($res,true);
        }

        echo $sid_info.'结果:'.$res.'<br>';
    }

    public function get_server_list(){

        $cache = Ext_Memcached::getInstance('user');
        if(!$list = $cache->fetch('gm_server_list')){

            $sql = "select id,sdb from adminserverlist";
            $res = $this->admindb->fetchRow($sql);
            $list = array();
            foreach ($res as $key => $val) {
                $list[$val['id']] = $val['sdb'];
            }
            $cache->store('gm_server_list',$list,300);

        }

        return $list;
    }

    public function erl_ling($sid,$type){
        
        if($type==1 || $type==2){//游戏服

            $erl = Config::get_center_erl($sid);

            if(!erl || count($erl->error)>0){
             
              return $sid.'erl连接'.json_encode($erl).'出错'; 
            
            }

        }elseif($type==3){//跨服中心
            //查询跨服所需要的erlang节点
            $sql_res = "select kf_node_name,cookie from kf_server_list where server_name='{$sid}'";

            $res_erlang = $this->admindb->fetchOne($sql_res);
            
            $rows['erl_cookie'] = $res_erlang['cookie'];
           
            $rows['erl_node'] = $res_erlang['kf_node_name'];

            $erl = Ext_Erlang::getInstance(false, false, $rows, '');

            if(!erl || count($erl->error)>0){
             
              return $sid.'erl连接'.json_encode($erl).'出错'; 
            
            }
        }

        return $erl;
    }

    public function gm_list(){

        $res = $this->base_gm_list('id');

        $list = array();
        foreach ($res as $key => $val) {
            $list[$val['id']] = array(
                                    'id'=>$val['id'],
                                    'type'=>$val['type'],
                                    'gm_name'=>$val['gm_name'],
                                    'gm'=>$val['gm'],
                                    'run_type'=>$val['run_type'],
                                    'module'=>$val['module'],
                                    'fun'=>$val['fun'],
                                    'input_val'=>unserialize($val['input_val']),
                                    'remark'=>$val['remark'],
                                );
        }


        $run_type_list = array(1=>'本服',2=>'可选游戏服',3=>'跨服中心');
        
        $run_type_color = array(1=>'',2=>"style='background:#e0eaea'",3=>"style='background:#fff3f3'");

        $type_list = $this->erl_type_list;
        foreach ($list as $key => $val) {
            $str = "";

            if($val['input_val']){
                $str_type = array();

                foreach ($val['input_val'] as $k => $v) {

                    $list[$key]['input_list'] .= "$k=>{$v['input_name']}({$v['input_type']}),";
                    $str_type[] = $type_list[$v['input_type']];   

                }

                $str = implode(',',array_keys($val['input_val']));
                $str_type = implode(',', $str_type);
            }else{
                $str_type = "";
            }
            
           
            $list[$key]['run_type'] = $run_type_list[$val['run_type']];
            
            $list[$key]['module_fun'] = "'{$val['module']}' , '{$val['fun']}' , '[{$str_type}]' , array(array($str))";

            if($val['type']==1){

                $list[$key]['back_color'] = $run_type_color[$val['run_type']];
            }

            
            
        }

        $assign['header_list'] = array('ID','GM名','GM','类型','参数','调用方式','备注','');
           
        $assign['list'] = $list;

        $this->display(CENTER_TEMP . 'gm_list.shtml', $assign);


    }

    public function gm_list_add(){

        $id = $_GET['id'];

        //类型
        $assign['type_list'] = array(
                                1=>array(
                                    'name'=>'秘籍小工具',
                                    'str'=>"checked='checked'",
                                    ),
                                2=>array(
                                    'name'=>'内部使用',
                                    'str'=>"",
                                    ),
                                );
        //GM类型
        $assign['run_type_list'] = array(
                                    1=>array(
                                    'name'=>'本服',
                                    'str'=>"selected='selected'",
                                         ),
                                    2=>array(
                                    'name'=>'可选游戏服',
                                    'str'=>"",
                                         ),
                                    3=>array(
                                    'name'=>'跨服中心',
                                    'str'=>"",
                                         ),
                                    );

        if($id){
            $res = $this->base_gm_list('id',$id);

            $list = array();

            $list = array(
                        'id'=>$res['id'],
                        'gm_name'=>$res['gm_name'],
                        'gm'=>$res['gm'],
                        'module'=>$res['module'],
                        'fun'=>$res['fun'],
                        'input_val'=>unserialize($res['input_val']),
                        'remark'=>$res['remark'],
                    );
            $assign['type_list'] = $this->replace_state($assign['type_list'],$res['type'],"checked='checked'");
            $assign['run_type_list'] = $this->replace_state($assign['run_type_list'],$res['run_type'],"selected='selected'");
        }
        
        $assign['list'] = $list;

        $assign['erl_type_list'] = $this->erl_type_list;
// print_r($list);exit;
        $this->display(CENTER_TEMP . 'gm_list_add.shtml', $assign);
    }

    public function gm_save(){
        // print_r($_REQUEST);exit;

        $id = trim($_POST['id']);
        $type = trim($_POST['type']);
        $gm_name = trim($_POST['gm_name']);
        $gm = trim($_POST['gm']);
        $run_type = trim($_POST['run_type']);
        $module = trim($_POST['module']);
        $fun = trim($_POST['fun']);
        $remark = trim($_POST['remark']);
        $input_val = array();

        //查询新增的GM是否存在
        if($id){
            $where = " WHERE id!=$id AND gm='$gm' ";
        }else{
            $where = " WHERE gm='$gm' ";
        }
        
        $sql = "SELECT * FROM base_gm_list $where ";
        $res = $this->admindb->fetchRow($sql);

        if(count($res)>0){
            alert("调用名称已存在！");exit;
        }

        if(count($_POST['input_name'])>0){
            foreach ($_POST['input_name'] as $key => $val) {
                $input_val[$val] = array(
                                        'input_name'=>$_POST['input_val'][$key],
                                        'input_type'=>$_POST['input_type_list'][$key],
                                    );
            }
        }
        $input_val = serialize($input_val);

        if(empty($id)){
            $sql = "SELECT IFNULL(MAX(id), 0) + 1 AS id FROM base_gm_list";
            $res = $this->admindb->fetchOne($sql);

            $id = $res['id'];
        }

        $sql = "REPLACE INTO base_gm_list (
                        id,
                        type,
                        gm_name,
                        gm,
                        run_type,
                        module,
                        fun,
                        input_val,
                        remark
                    )
                    VALUES
                        (
                            '$id',
                            '$type',
                            '$gm_name',
                            '$gm',
                            '$run_type',
                            '$module',
                            '$fun',
                            '$input_val',
                            '$remark'
                        )";
        $this->admindb->query($sql);                
        
        $this->clear_cache();

        alert('编辑成功！','/action_gateway.php?ctl=gm_tool&act=gm_list');
    }

//替换函数 list 列表，index选中值，str替换值
    public function replace_state($list,$index,$str){
        
        foreach ($list as $key => $val) {
            if($key==$index){
                $list[$key]['str'] = $str;
            }else{
                unset($list[$key]['str']);
            }
        }

        return $list;
    }

//获取传入参数
    public function ajax_get_input_val(){
        
        $id = $_REQUEST['id'];

        $res = $this->base_gm_list('id',$id,'input_val');

        $input_val = unserialize($res);

        $str = "";
        if(count($input_val)>0){

            $str .= '<table>';
            $jq = "onkeyup=value=value.replace(/[^\d]/g,'')";
            
            foreach ($input_val as $key => $val) {

                if($val['input_type']=='int'){
                   
                    $jq_str = $jq;
                }else{
                    $jq_str = "";
                }
                
                $str .="<tr>
                <td style='width:15%;text-align: center;'>{$val['input_name']}({$val['input_type']})</td>
                <td><input type='text' name='$key' $jq_str input_type='{$val['input_type']}' autocomplete='off'></td>&nbsp;</tr>";

            }

            $str .= '</table>';
        }

        echo $str;

    }


//获取备注
    public function ajax_get_remark(){

        $id = $_REQUEST['id'];

        // $sql = "SELECT remark FROM base_gm_list WHERE id=$id";
        // $res = $this->admindb->fetchOne($sql);
        $res = $this->base_gm_list('id',$id,'remark');

        $str = "";
        if($res){

            $str .= $res;
            
        }

        echo $str;

    }


///ajax调用值
    public function ajax_get_server_list(){
        
        $type = $_REQUEST['type'];
        $str = "";
        if($type==1){//本服
            
            $info = $this->serverinfo();
            $str = "<input type='radio' id='sid' name='sid[]' value='{$info['id']}' checked='checked'>本服";
        }elseif($type==2){//服务器列表
            
            $sql = "SELECT l.id, sname, platform_name FROM adminserverlist l LEFT JOIN adminplatformlist p ON l.gid = p.gid WHERE `default` = 1";
            $res = $this->admindb->fetchRow($sql);

            $list = array();
            foreach ($res as $key => $val) {
                $list[$val['platform_name']][$val['id']] = $val['sname'];
            }

            $str = "<select id='sid' name='sid[]' multiple='multiple' style='height:400px;'>";

            foreach ($list as $key => $val) {
                $str .= "<optgroup label='$key'></optgroup>";
                foreach ($val as $kk => $vv) {
                    $str .="<option value='$kk'>$vv</option>";
                }
            }

            $str .= "</select>";
        
        }elseif($type==3){
            $sql = "SELECT kf_center FROM adminplatformlist WHERE kf_center!=''";
            $res = $this->admindb->fetchRow($sql);

            $str = "<select id='sid' name='sid[]' multiple='multiple' style='height:300px;'>";
            foreach ($res as $key => $val) {
                $str .="<option value='{$val['kf_center']}'>{$val['kf_center']}</option>";
            }
            $str .= "</select>";
        }
        
        echo $str;
    }


/*
base_gm_list缓存列
type id或gm
find type为ID值传ID值，type为gm则传gm值(不传则为整个列表)
field 取具体字段值
*/   
    public function base_gm_list($type,$find='',$field=''){

        $gm_type_list = array(
                        'id'=>'id_gm_list',
                        'gm'=>'gm_gm_list');

        //缓存
        $cache = Ext_Memcached::getInstance('user');
        if(!$list = $cache->fetch($gm_type_list[$type])){
            $sql = "SELECT * FROM base_gm_list  ORDER BY id DESC";
            $list = $this->admindb->fetchRow($sql);
// print_r(11111);exit;
            $id_gm_list = array();
            $gm_gm_list = array();
            foreach ($list as $key => $val) {
                $id_gm_list[$val['id']] = $val;
                $gm_gm_list[$val['gm']] = $val;
            }

            $cache->store('id_gm_list',$id_gm_list,300);
            $cache->store('gm_gm_list',$gm_gm_list,300);

            if($type=='id'){
                $list = $id_gm_list;
            }else{
                $list = $gm_gm_list;
            }
        } 

        if($find){
            if($field){
                return $list[$find][$field];
            }else{
                return $list[$find];
            }
            
        }else{
            return $list;
        }

    }
//清缓存
    public function clear_cache(){

        $cache = Ext_Memcached::getInstance('user');
        $cache->delete('id_gm_list');
        $cache->delete('gm_gm_list');

        return true;
    }


}//end class
