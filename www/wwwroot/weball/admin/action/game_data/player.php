<?php
/**
 * 玩家
 * User: wtf
 * Date: 18/5/4
 * Time: 下午2:42
 */
require_once dirname(__FILE__) . '/base.php';

class player extends base{

    public function handle()
    {
        exit('迷路了啊 小伙子@ !!!!');
    }

    //角色列表
    function role_list()
    {
        $url = "?ctl=game_data&act=player&method=role_list";

        $where = " WHERE 1 ";

        if ($_SESSION['channel']) {
            $str = implode(",", $_SESSION['channel']);
            $where .= " and c.source in ($str) ";
        }

        if ($_REQUEST['role_id']) {
            $where .= " AND b.role_id=" . $_REQUEST['role_id'];
        }

        if ($_REQUEST['name']) {
            $where .= " AND b.name like '%" . $_REQUEST['name'] . "%'";
        }

        if ($_REQUEST['accname']) {
            $where .= " AND b.accname='" . $_REQUEST['accname'] . "'";
        }

        if ($_REQUEST['reg_ip']) {
            $where .= " AND b.reg_ip='" . $_REQUEST['reg_ip'] . "'";
        }

        if ($_REQUEST['last_login_ip']) {
            $where .= " AND b.last_login_ip='" . $_REQUEST['last_login_ip'] . "'";
        }

        if ($_REQUEST['device']) {
            $where .= " AND a.device='" . $_REQUEST['device'] . "'";
        }

        if ($_REQUEST['lv_start'] && $_REQUEST['lv_end']) {
            $where .= " AND a.level>=" . $_REQUEST['lv_start'] . " AND a.level<=" . $_REQUEST['lv_end'];
        } else {
            if ($_REQUEST['lv_start']) {
                $where .= " AND a.level>=" . $_REQUEST['lv_start'];
            }

            if ($_REQUEST['lv_end']) {
                $where .= " AND a.level<=" . $_REQUEST['lv_end'];
            }
        }
        $time = time();
        if ($_REQUEST['kw_state'] == 'talk') {
            $where .= " AND ban_until > $time ";
        } elseif ($_REQUEST['kw_state'] == 'limit') {
            $where .= " AND a.state=4";
        } elseif ($_REQUEST['kw_state'] == 'gm') {
            $where .= " AND b.is_gm=1";
        }

        $sql = "select count(1) as total from role_basic a $where";
        $total_record = $this->gamedb->fetchOne($sql);
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        if ($_REQUEST['sortkey'] && $_REQUEST['sortval']) {
            $ORDER = $_REQUEST['sortkey'] . " " . $_REQUEST['sortval'];
        } else {
            $ORDER = " role_id DESC ";
        }
        
        $sql = "SELECT
                    b.role_id,
                    source,
                    b.`name`,
                    b.accname,
                    state,
                    b.`level`,
                    v.`level` vip_lv,
                    b.career,
                    gold,
                    bgold,
                    is_online,
                    FROM_UNIXTIME(last_login_time) last_login_time,
                    FROM_UNIXTIME(ctime) ctime,
                    ip,
                    last_login_ip,
                    c.device,
                    fight,
                    login_days,
                    FROM_UNIXTIME(ban_until) ban_until,
                    IF (ban_until > $time, 1, 0) ban_chat
                FROM
                    role_basic b
                LEFT JOIN role_create c ON b.role_id = c.role_id
                LEFT JOIN role_vip v ON b.role_id = v.role_id
                LEFT JOIN role_money m ON b.role_id = m.role_id  
                LEFT JOIN role_chat ch on b.role_id=ch.role_id
                $where order by $ORDER $limit";
        $res = $this->gamedb->fetchRow($sql);
// print_r($sql);
        $sql = "select id,title,'role_id' parameter from base_log";
        $log_list = $this->admindb->fetchRow($sql);
        foreach ($log_list as $key => $val) {
            if( $val['id'] == 7){
                $log_list[$key]['parameter'] = 'rm__role_id';
            }elseif( $val['id'] == 9 ){
                unset($log_list[$key]);
            }
        }
// print_r($sql);
        $assign['list'] = $res;
        $assign['log_list'] = $log_list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query(array_filter($_REQUEST)) . "&page=",
        );

        $this->display('game_data/role_list.shtml', $assign);

    }

    //查询同一设备，或同一IP，或者同一账号的玩家
    public function find_role_info()
    {
        $accname = request('accname', 'str');
        $ip = request('ip', 'str');
        $device_id = request('device_id', 'str');
        if ($accname || $ip || $device_id) {
            Auth::check_action(Auth::$SEARCH_ROLE_INFO);
            //游戏服列表
            $sql = "SELECT
                      id AS sid,
                      description
                    FROM
                      adminserverlist";
            $res = $this->admindb->fetchRow($sql);

            foreach ($res as $key => $val) {
                $sid_list[$val['sid']] = $val['description'];
            }

            $where = " WHERE 1 ";
            if ($accname) {
                $where .= " AND accname='$accname' ";
            }

            if ($ip) {
                $where .= " AND ip='$ip' ";
            }

            if ($device_id) {
                $where .= " AND device_id='$device_id' ";
            }

            $total_record = $this->centerdb->fetchOne("select count(1) as total from player  $where");

            $total_record = $total_record['total'];

            $per_page = 100;
            $total_page = ceil($total_record / $per_page);
            $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
            $cur_page = $cur_page < 1 ? 1 : $cur_page;
            $limit_start = ($cur_page - 1) * $per_page;
            $limit = " LIMIT {$limit_start}, $per_page ";

            $sql = "SELECT
                    accname,
                    role_id,
                    FROM_UNIXTIME(reg_time) AS reg_time,
                    FROM_UNIXTIME(last_login_time) AS last_login_time,
                    channel,
                    device_id,
                    nickname,
                    server_id,
                    last_login_server,
                    ip
                  FROM
                    player
                    $where $limit";
            $res = $this->centerdb->fetchRow($sql);

            foreach ($res as $key => $val) {
                $res[$key]['serverinfo'] = $sid_list[$val['server_id']];
                $res[$key]['last_serverinfo'] = $sid_list[$val['last_login_server']];
            }
            Admin::adminLog("查询同帐号同ip同设备号:{$where}");
            $assign['list'] = $res;

        }

        $url = "action_gateway.php?";
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_GET) . "&page=",
        );

        $this->display('game_data/role_info.shtml', $assign);
    }

    public function get_role_info()
    {

        $role_id = request('role_id', 'str');
        if ($role_id) {
            $time = time();
            $sql = "SELECT
                    b.role_id,
                    source,
                    b.`name`,
                    b.exp,
                    b.gender sex,
                    b.accname,
                    state,
                    b.`level`,
                    v.`level` vip_lv,
                    b.career,
                    fight,
                    gold,
                    bgold,
                    is_online,
                    FROM_UNIXTIME(last_login_time) last_login_time,
                    FROM_UNIXTIME(last_logout_time) last_logout_time,
                    FROM_UNIXTIME(ctime) ctime,
                    ip,
                    last_login_ip,
                    b.server_id,
                    m.coin,
                    m.fcoin,
                    m.cont,
                    m.credit,
                    c.device,
                    login_days,
                    floor(total_online/3600) total_online,
                    title_id,
                    if(expire_time!=0,FROM_UNIXTIME(expire_time),'永久') expire_time,
                    valid,
                    l.`name` as title_name,
                    FROM_UNIXTIME(ban_until) ban_until,
                    IF (ban_until > $time, 1, 0) ban_chat
                FROM
                    role_basic b
                LEFT JOIN role_create c ON b.role_id = c.role_id
                LEFT JOIN role_vip v ON b.role_id = v.role_id
                LEFT JOIN role_money m ON b.role_id = m.role_id  
                LEFT JOIN role_title_list l ON b.role_id = l.role_id
                LEFT JOIN role_chat ch on b.role_id=ch.role_id
                WHERE b.role_id = $role_id";
            $role_info_res = $this->gamedb->fetchOne($sql);
        
            $sql = "select count(1) num,sum(money) money from charge where role_id=$role_id";
            $res = $this->gamedb->fetchOne($sql);
            $title_list = $this->get_config_list(9);
      
            $role_info_res['title_id'] = $role_info_res['title_id'] ? $title_list[$role_info_res['title_id']] : '';

            $role_info_res['total_pay_cnt'] = $res['num']?$res['num']:0;
            $role_info_res['total_pay_money'] = $res['money']?$res['money']:0;

            echo json_encode($role_info_res);
        }
    }

    /**
     * 玩家背包
     */
    public function role_bag(){
        $url = '?ctl=game_data&act=player&method=role_bag';
        $assign = array();
        $where = ' where 1';
        $role_id = request('role_id','str');

        $item_id = request('item_id','int');
        if(!$role_id) {
            $role_id = -1;
            $item_id = -1;
        }
        if($role_id){
            $where .= ' and role_id = '.$role_id;
        }
        if($item_id){
            $where .= ' and item_tid = '.$item_id;
        }

        $sql = "SELECT
                        bag_id,
                        goods
                    FROM
                        role_bags
                    WHERE
                        role_id = $role_id ";
        $res = $this->db->fetchRow($sql);     
        $list = array();
        $goods_list = $this->get_config_list(1);

        foreach ($res as $key => $val) {
            preg_match_all('/(bind=>(\d+))/', $val['goods'], $re_bind);
            preg_match_all('/(expire=>(\d+))/', $val['goods'], $re_expire);
            preg_match_all('/(id=>(\d+))/', $val['goods'], $re_id);
            preg_match_all('/(num=>(\d+))/', $val['goods'], $re_num);
            preg_match_all('/(paris=>(\d+))/', $val['goods'], $re_paris);
            preg_match_all('/(pos=>(\d+))/', $val['goods'], $re_pos);   
            // print_r($re_id);exit;

            foreach ($re_id[2] as $kk => $vv) {
                $list[] = array(
                        'id'         => "($vv)".$goods_list[$vv],
                        'bind'       => $re_bind[2][$kk] == 1 ? Ext_Template::lang('绑定') : Ext_Template::lang('非绑'),
                        'expire'     => $re_expire[2][$kk] ? date("Y-m-d H:i:s",$re_expire[2][$kk]) : Ext_Template::lang('永久'),
                        'num'        => $re_num[2][$kk],
                        'paris'      => $re_paris[2][$kk],
                        'pos'        => $re_pos[2][$kk],
                        'bag_id'     => $val['bag_id'],
                        );
            }
        }          
  
        $assign['list'] = $list;
        $assign['role_info'] = $this->db->fetchOne("select * from role_basic where role_id=$role_id");
        $assign['role_list'] = $this->get_config_list('role_list');

        $this->display('game_data/role_bag.shtml',$assign);
    }


    /**
     * 更换职位
     */
    public function set_guild_member_pos(){
        Auth::check_action(Auth::$GUILD_POS);
        $pos = request('pos');
        $role_id = request('role_id');

        if(!$pos || !$role_id){
            alert(Ext_Template::lang('缺少参数'));
        }

        $re = Erlang::erl_set_guild_pos($_SESSION['selected_sid'],array('role_id'=>$role_id,'pos'=>$pos));
        $msg = is_array($re)?implode(',',$re):$re;
        Admin::adminLog('更换军盟成员职位|role_id:'.$role_id.',pos:'.$pos.';result:'.$msg);
        alert($re);
    }

    /**
     * 更换职位 修改战盟公告：lib_gm，change_guild_anno
     */
    public function ajax_change_guild_anno(){
        Auth::check_action(Auth::$GUILD_POS);
        $guild_id = request('guild_id');
        $content = request('content');

        if(!$guild_id || !$content){
            exit('缺少参数');
        }

        $re = Erlang::erl_change_guild_anno($_SESSION['selected_sid'],array('guild_id'=>$guild_id,'content'=>$content));
        $msg = is_array($re)?implode(',',$re):$re;
        Admin::adminLog('修改军盟公告|guild_id:'.$guild_id.',content:'.$content.';result:'.$msg);
        exit($re);
    }

    /**
     * 军盟列表
     */
    public function guild(){
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $guild_id = request('guild_id', 'str');
        $guild_name = request('guild_name', 'str');

        $url = "action_gateway.php?ctl=game_data&act=player&method=guild";
        $where = 'Where 1 ';
        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " And create_time >=$start_time";
            $url .='&start_time='.date('Y-m-d',$start_time);
        }
        if($end_time){
            $end_time = strtotime($end_time);
            $where .= " And create_time <".($end_time+86400);
            $url .='&end_time='.date('Y-m-d',$end_time);
        }
        if($guild_id){
            $where .= " And guild_id = '$guild_id'";
            $url .= "&guild_id=$guild_id";
        }
        if ($guild_name) {
            $where .= " And name = '$guild_name'";
            $url .= "&guild_name=$guild_name";
        }

        if ($_REQUEST['sortkey'] && $_REQUEST['sortval']) {
            $ORDER = $_REQUEST['sortkey'] . " " . $_REQUEST['sortval'];
        } else {
            $ORDER = " create_time DESC ";
        }


        $sql = "select count(1) as total from guild a $where";
        $total_record = $this->db->fetchOne($sql);
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        $sql = "select a.annoucement,a.guild_id,a.name,a.level,a.fund,a.fight_level,total_fight_level,FROM_UNIXTIME(a.create_time,'%Y-%m-%d %H:%i:%s') as create_time from guild a $where order by $ORDER $limit";
        $res = $this->db->fetchRow($sql);
        $str = '无';
        foreach($res as $key=>$val){
            if($val['fight_level']<100){
                switch ($val['fight_level'][0]){
                    case 1:$str='S组第'.$val['fight_level'][1].'名';break;
                    case 2:$str='A组第'.$val['fight_level'][1].'名';break;
                    case 3:$str='B组第'.$val['fight_level'][1].'名';break;
                    case 4:$str='C组第'.$val['fight_level'][1].'名';break;
                }
            }
            $res[$key]['fight_level'] = $str;
        }
        $assign['list'] = $res;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query(array_filter($_REQUEST)) . "&page=",
        );
        $this->display('game_data/guild_lists.shtml',$assign);
    }

    /**
     * 帮派成员
     */
    public function guild_member(){
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');
        $guild_id = request('guild_id','str');

        if(empty($guild_id)){
            alert(Ext_Template::lang('参数错误！'));
            
        }
        $where = 'Where 1 ';
        if ($start_time) {
            $start_time = strtotime($start_time);
            $where .= " And join_time >=$start_time";
        }
        if($end_time){
            $end_time = strtotime($end_time);
            $where .= " And join_time <".($end_time+86400);
        }
        $where .=" And guild_id=$guild_id";
        if ($_REQUEST['sortkey'] && $_REQUEST['sortval']) {
            $ORDER = $_REQUEST['sortkey'] . " " . $_REQUEST['sortval'];
        } else {
            $ORDER = " join_time DESC ";
        }
        $sql = "select a.role_id,b.nick_name,guild_id,FROM_UNIXTIME(join_time,'%Y-%m-%d %H:%i:%s') as join_time,pos,exchange_point,week_contribute,total_contribute from guild_member a left join role_basic b on a.role_id = b.role_id $where order by $ORDER";
        $res = $this->db->fetchRow($sql);
        $assign['list'] = $res;
        $this->display('game_data/guild_member.shtml',$assign);
    }

    /**
     * 获取帮派信息
     */
    public function ajax_get_guild_info(){
        $guild_id = request('guild_id','str');
        if($guild_id){
            $res = $this->get_guild_info($guild_id);
            echo json_encode($res);
        }
    }
}