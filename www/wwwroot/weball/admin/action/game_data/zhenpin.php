<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/2
 * Time: 上午10:49
 */
require_once dirname(__FILE__) . '/base.php';

class zhenpin extends base{

    /**
     * 获取珍品
     * @return array
     */
    public function get_item(){
        $list = array();
        $sql = "select * from base_config where type = 15";
        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['id']] = $v['item'];
            }
        }

        return $list;
    }
    // TODO: Implement handle() method.
    public function handle()
    {
        $assign = array();
        $where = ' where 1';
        $st = request('st');
        $et = request('et');
        if(!$st){
            $st = date('Y-m-d');
        }
        if(!$et){
            $et = $st;
        }
        $where .= " and log_time>=". strtotime($st);
        $where .= " and log_time<" . (strtotime($et)+86400);

        $type = request('type','int');
        if($type == 1){
            $where .= " and item_tid < 100000000";
        }elseif($type == 2){
            $where .= " and item_tid > 100000000";
        }

        $opt = request('opt','int');
        if($opt){
            $where .= " and opt = ".$opt;
        }

        $items = $this->get_item();
        $in = array_keys($items);

        if(!$in){
            alert('请策划童鞋先获取游戏配置-珍品表!');
        }
        $in = implode(',',$in);
        $sql = "select item_tid,change_type,count(item_tid) as c from log_item {$where} and item_tid in ({$in}) group by item_tid,change_type";

        $list = $tmp = array();
        $re = $this->gamedb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['item_tid']] = array(
                    'item_tid'=>$v['item_tid'],
                    'name'=>$items[$v['item_tid']],
                    '1'=>0,
                    '2'=>0,
                );

                $tmp[$v['item_tid']][$v['change_type']] = $v['c'];
            }
            foreach ($list as &$v) {
                if($tmp[$v['item_tid']][1]) $v['1'] = $tmp[$v['item_tid']][1];
                if($tmp[$v['item_tid']][2]) $v['2'] = $tmp[$v['item_tid']][2];
            }
        }
        $header = array('珍品ID','名称','产出数量','消耗数量');

        $assign['list'] = $list;
        $assign['head'] = $header;

        if(request('excel')){
            $this->export_to_excel($header,$list,'珍品统计');
        }
        $assign['st'] = $st;
        $assign['et'] = $et;

        $assign['opt'] = $this->get_opt();

        $this->display('game_data/zhenpin.shtml',$assign);
    }
}