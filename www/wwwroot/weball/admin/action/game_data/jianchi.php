<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/2
 * Time: 上午10:49
 */
require_once dirname(__FILE__) . '/base.php';

class jianchi extends base{

    public function handle()
    {
        // TODO: Implement handle() method.
        $assign = array();
        $where = ' where 1';
        $st = request('st');
        $et = request('et');
        if(!$st){
            $st = date('Y-m-d');
        }
        if(!$et){
            $et = $st;
        }
        $where .= " and log_time>=". strtotime($st);
        $where .= " and log_time<" . (strtotime($et)+86400);

        $sql = "select opt_detail , count(distinct role_id) as c,count(role_id) as c2 from log_sword_spirit {$where} and  opt_type = 1 group by opt_detail;";

        $list = $body = array();
        $total_num = 0;
        $re = $this->gamedb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $total_num += $v['c'];
                $list[$v['c']] = array(
                    'opt_detail'=>$v['opt_detail'],
                    'num'=>$v['c'],
                    'num_rate'=> 0 ,
                    'times'=>round($v['c2']/$v['c'],1),
                );
            }
            foreach ($list as &$v) {
                $v['num_rate'] = round($v['num']/$total_num,4) * 100 . '%';
                $v['width'] = $v['num']/$total_num * 500;
                $body[] = array(
                    $v['opt_detail'],$v['num_rate'],$v['num'],$v['times'],
                );
            }
        }
        krsort($list);
        $assign['list'] = $list;

        if(request('excel')){
            $header = array('活动类型','参与人数百分比','参与人数','人均参与次数');
            $this->export_to_excel($header,$body,'剑池统计');
        }
        $assign['st'] = $st;
        $assign['et'] = $et;

        $this->display('game_data/jianchi.shtml',$assign);
    }
}