<?php
/**
 * author fan
 * 游戏内数据之boss统计
 */
require_once dirname(__FILE__) . '/base.php';

class boss extends base
{
    public function handle()
    {
        exit('迷路了啊 小伙子@ !!!!');
    }
    /**
     * 获取BOSS配置
     * @return array
     */
    public function get_boss($scene = false){
        static $list = array();
        static $scene_list = array();

        if($list && !$scene) return $list;
        if($scene_list && $scene) return $scene_list;
        $sql = "select * from base_config where type = 12";
        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $v['attr'] = json_decode(trim($v['attr'],"\""),true);
                $list[$v['id']] = array(
                    'name'=>$v['item'],
                    'scene_id'=>$v['attr']['scene_id'],
                    'scene_name'=>$v['attr']['scene_name'],
                );
                $scene_list[$v['attr']['scene_id']] = $v['attr']['scene_name'];
            }
        }

        if($scene){
            return $scene_list;
        }

        return $list;
    }

    /**
     * 获取boss类型
     * @return array
     */
    public function get_boss_type(){
        $list = array();
        $sql = "select * from base_config where type = 13";
        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['id']]=$v['item'];
            }
        }

        return $list;
    }

    /**
     * 通过boss_id找场景
     * @param $boss_id
     * @return string
     */
    public function get_scene_by($boss_id){
        $list = $this->get_boss();
        return $list[$boss_id]['scene_name'];
    }

    /**
     * BOSS掉落统计
     */
    public function index()
    {
        $assign = array();
        $assign['boss_list'] = $this->get_boss();
        /**
         * 0白 1蓝 2紫 3橙 4红 5粉红
         */
        $assign['quality_list'] = array('1'=>'白装','2'=>'蓝装','3'=>'紫装','4'=>'橙装','5'=>'红装','6'=>'粉红');
        $assign['color_list'] = array('1'=>'#CCC','2'=>'blue','3'=>'purple','4'=>'orange','5'=>'red','6'=>'pink');
        $assign['star_list'] = array('1'=>'0星','2'=>'1星','3'=>'2星','4'=>'3星');

        $where = "where 1";
        $st = request('st');
        $et = request('et');
        $boss_id = request('boss_id');

        $quality = request('quality','int');
        $star = request('star','int');

        $assign['st'] = $st = $st?$st:date('Y-m-d');
        $assign['et'] = $et = $et?$et:date('Y-m-d');


        if($st) {
            $where .= ' and log_time >' .strtotime($st);
        }
        if($et){
            $where .= ' and log_time<='. (strtotime($et)+86400);
        }
        if($quality){
            $where .= ' and quality = '.($quality - 1);
        }
        if($star){
            $where .= ' and star = '. ($star-1);
        }
        if($boss_id){
            $where .= ' and boss_id='.$boss_id;
        }

        $sql = "select count(1) as c from (select 1 from log_world_boss {$where} and `op_type`=2 and item_tid>100000000 group by boss_id,item_tid,star) as tb";
        $re = $this->gamedb->fetchOne($sql);

        $total_record = $re['c']?$re['c']:0;
        $per_page = 500;
        $total_page = ceil($total_record / $per_page);
        $cur_page = ($page = request('page','int')) ? $page : 1;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";

        //备注: 一个item_id对应唯一quality
        $list = array();
        $sql = "select boss_id,boss_name,item_tid,item_name,quality,star,sum(amount) as s from log_world_boss {$where} and `op_type`=2 and item_tid>100000000 group by boss_id,item_tid,star $limit";
        $re = $this->gamedb->fetchRow($sql);
        if($re){
            $list = $re;
        }

        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($_SERVER['REQUEST_URI'], "?") === false) ? ($_SERVER['REQUEST_URI'] . "?page=") : (rtrim($_SERVER['REQUEST_URI'], "&") . "&page="),
        );

        $assign['list'] = $list;

        $this->display("game_data/boss_index.shtml", $assign);

    }

    public function red_orange_equip(){
        $assign = array();
        $assign['boss_scene_list'] = $boss_scene_list = $this->get_boss(true);
        $assign['boss_type_list'] = $this->get_boss_type();

        /**
         * 0白 1蓝 2紫 3橙 4红 5粉红
         */
        $assign['quality_list'] = array('1'=>'白装','2'=>'蓝装','3'=>'紫装','4'=>'橙装','5'=>'红装','6'=>'粉红');
        $assign['color_list'] = array('1'=>'#CCC','2'=>'blue','3'=>'purple','4'=>'orange','5'=>'red','6'=>'pink');
        $assign['star_list'] = array('1'=>'0星','2'=>'1星','3'=>'2星','4'=>'3星');

        $where = "where 1";
        $st = request('st');
        $et = request('et');
        $scene_id = request('scene_id','int');

        $assign['st'] = $st = $st?$st:date('Y-m-d');
        $assign['et'] = $et = $et?$et:date('Y-m-d');

        if($st) {
            $where .= ' and log_time >' .strtotime($st);
        }
        if($et){
            $where .= ' and log_time<='. (strtotime($et)+86400);
        }
        if($scene_id){
            $where .= ' and scene_id='.$scene_id;
        }
        //击杀统计
        $kill_by_boss_id = array();
        $kill_by_boss_type = array();
        $sql = "select boss_id,boss_type from log_world_boss {$where} and op_type=1";
        $re = $this->gamedb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $kill_by_boss_id[$v['boss_id']] += 1;
                $kill_by_boss_type[$v['boss_type']] += 1;
            }
        }
        //返回数据list
        $list_by_boss_type = $list_by_boss_id = array();
        $list_by_boss_type_total = $equip_by_boss_id_total = array();

        $for = $kill_by_boss_type;
        if($for){
            //红橙装统计
            $equip_by_boss_id = array();
            $equip_by_boss_type = array();
            $equip_by_boss_type_total = array();
            $sql = "select boss_id,boss_name,boss_type,boss_lv,scene_id,quality,star,sum(amount) as s from log_world_boss {$where} and op_type=2 and quality in (3,4) and star in (1,2) and item_tid>100000000 group by boss_id,quality,star;";

            $re = $this->gamedb->fetchRow($sql);
            if($re){
                foreach ($re as $v) {
                    $equip_by_boss_id[$v['boss_id']][$v['quality']][$v['star']] += $v['s'];
                    $equip_by_boss_id_total[$v['boss_id']][$v['quality']] += $v['s'];
                    $equip_by_boss_type[$v['boss_type']][$v['quality']][$v['star']] += $v['s'];
                    $equip_by_boss_type_total[$v['boss_type']][$v['quality']] += $v['s'];
                }

                //主循环获取boss_id统计列表
                foreach ($re as $v) {
                    $list_by_boss_id[$v['boss_id']]['boss_name'] = $v['boss_name'].'-'.$this->get_scene_by($v['boss_id']);
                    $list_by_boss_id[$v['boss_id']]['scene_id'] = $v['scene_id'];
                    $list_by_boss_id[$v['boss_id']]['boss_id'] = $v['boss_id'];
                    $list_by_boss_id[$v['boss_id']]['boss_lv'] = $v['boss_lv'];
                    $list_by_boss_id[$v['boss_id']]['boss_kill'] = $kill_by_boss_id[$v['boss_id']];
                    $list_by_boss_id[$v['boss_id']]['3_all'] = $equip_by_boss_id_total[$v['boss_id']][3]?:0;
                    $list_by_boss_id[$v['boss_id']]['3_1'] = $equip_by_boss_id[$v['boss_id']][3][1]?:0;
                    $list_by_boss_id[$v['boss_id']]['3_2'] = $equip_by_boss_id[$v['boss_id']][3][2]?:0;
                    $list_by_boss_id[$v['boss_id']]['4_all'] = $equip_by_boss_id_total[$v['boss_id']][4]?:0;
                    $list_by_boss_id[$v['boss_id']]['4_1'] = $equip_by_boss_id[$v['boss_id']][4][1]?:0;
                    $list_by_boss_id[$v['boss_id']]['4_2'] = $equip_by_boss_id[$v['boss_id']][4][2]?:0;
                }
            }
            //主循环获取boss类型统计列表
            foreach ($for as $boss_type => $line) {
                $list_by_boss_type[$boss_type]['kill'] = $line;
                $list_by_boss_type[$boss_type]['3_all'] = $equip_by_boss_type_total[$boss_type][3]?:0;
                $list_by_boss_type[$boss_type]['3_1'] = $equip_by_boss_type[$boss_type][3][1]?:0;
                $list_by_boss_type[$boss_type]['3_2'] = $equip_by_boss_type[$boss_type][3][2]?:0;
                $list_by_boss_type[$boss_type]['4_all'] = $equip_by_boss_type_total[$boss_type][4]?:0;
                $list_by_boss_type[$boss_type]['4_1'] = $equip_by_boss_type[$boss_type][4][1]?:0;
                $list_by_boss_type[$boss_type]['4_2'] = $equip_by_boss_type[$boss_type][4][2]?:0;
            }

            //计算总计
            foreach ($list_by_boss_type as $v) {
                $list_by_boss_type_total['kill_total'] += $v['kill'];
                $list_by_boss_type_total['3_all_total'] += $v['3_all'];
                $list_by_boss_type_total['3_1_total'] += $v['3_1'];
                $list_by_boss_type_total['3_2_total'] += $v['3_2'];
                $list_by_boss_type_total['4_all_total'] += $v['4_all'];
                $list_by_boss_type_total['4_1_total'] += $v['4_1'];
                $list_by_boss_type_total['4_2_total'] += $v['4_2'];
            }
            //计算占比
            foreach ($list_by_boss_type as &$v) {
                $v['kill_rate'] = $v['kill']? sprintf('%.2f',($v['kill']/$list_by_boss_type_total['kill_total'])*100).'%' : '0.00%';
                $v['3_all_rate'] = $v['3_all']? sprintf('%.2f',($v['3_all']/$list_by_boss_type_total['3_all_total'])*100).'%' : '0.00%';
                $v['3_1_rate'] = $v['3_1']? sprintf('%.2f',($v['3_1']/$list_by_boss_type_total['3_1_total'])*100).'%' : '0.00%';
                $v['3_2_rate'] = $v['3_2']? sprintf('%.2f',($v['3_2']/$list_by_boss_type_total['3_2_total'])*100).'%' : '0.00%';
                $v['4_all_rate'] = $v['4_all']? sprintf('%.2f',($v['4_all']/$list_by_boss_type_total['4_all_total'])*100).'%' : '0.00%';
                $v['4_1_rate'] = $v['4_1']? sprintf('%.2f',($v['4_1']/$list_by_boss_type_total['4_1_total'])*100).'%' : '0.00%';
                $v['4_2_rate'] = $v['4_2']? sprintf('%.2f',($v['4_2']/$list_by_boss_type_total['4_2_total'])*100).'%' : '0.00%';
            }


        }

        $assign['list_by_boss_type_total'] = $list_by_boss_type_total;
        $assign['list_by_boss_type'] = $list_by_boss_type;
        $assign['list_by_boss_id'] = $list_by_boss_id;


        $this->display("game_data/boss_red_orange_equip.shtml", $assign);
    }

    /**
     * boss掉落率
     */
    public function drop_rate(){
        $assign = array();
        $assign['boss_list'] = $boss_list = $this->get_boss();

        $where = "where 1";
        $st = request('st');
        $et = request('et');
        $boss_id = request('boss_id');

        $assign['st'] = $st = $st?$st:date('Y-m-d');
        $assign['et'] = $et = $et?$et:date('Y-m-d');

        if($st) {
            $where .= ' and log_time >' .strtotime($st);
        }
        if($et){
            $where .= ' and log_time<='. (strtotime($et)+86400);
        }
        if($boss_id){
            $where .= ' and boss_id='.$boss_id;
        }

        //击杀统计
        $kill_by_boss_id = array();
        $killer_by_boss_id = array();
        $sql = "select boss_id,killer_id,killer_name,log_time from log_world_boss {$where} and op_type=1";
        $re = $this->gamedb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $kill_by_boss_id[$v['boss_id']] += 1;
                $killer_by_boss_id[$v['boss_id']] .= date('Y-m-d H:i:s',$v['log_time']).$v['killer_name']."[{$v['killer_id']}]<br>";
            }
        }

        $list_by_boss_id = array();
        //红橙装统计
        $equip_by_boss_id = array();
        $sql = "select boss_id,boss_name,boss_type,boss_lv,scene_id,quality,star,item_tid,sum(amount) as s from log_world_boss {$where} and op_type=2 and quality in (3,4) and star in (1,2) and item_tid>100000000 group by boss_id,quality,star;";

        $re = $this->gamedb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $equip_by_boss_id[$v['boss_id']][$v['quality']][$v['star']] += $v['s'];
            }

            //主循环获取boss_id统计列表
            foreach ($re as $v) {
                $list_by_boss_id[$v['boss_id']]['boss_name'] = $boss_list[$v['boss_id']]['name'];
                $list_by_boss_id[$v['boss_id']]['boss_id'] = $v['boss_id'];
                $list_by_boss_id[$v['boss_id']]['equip_order'] = (int)substr($v['item_tid'],-4,-2).'阶';
                $list_by_boss_id[$v['boss_id']]['boss_lv'] = $v['boss_lv'];
                $list_by_boss_id[$v['boss_id']]['boss_kill'] = $k = $kill_by_boss_id[$v['boss_id']];
                $list_by_boss_id[$v['boss_id']]['3_1'] = ($t = $equip_by_boss_id[$v['boss_id']][3][1])?(sprintf('%.4f',$t/$k)*100).'%':0;
                $list_by_boss_id[$v['boss_id']]['3_2'] = ($t = $equip_by_boss_id[$v['boss_id']][3][2])?(sprintf('%.4f',$t/$k)*100).'%':0;
                $list_by_boss_id[$v['boss_id']]['4_1'] = ($t = $equip_by_boss_id[$v['boss_id']][4][1])?(sprintf('%.4f',$t/$k)*100).'%':0;
                $list_by_boss_id[$v['boss_id']]['4_2'] = ($t = $equip_by_boss_id[$v['boss_id']][4][2])?(sprintf('%.4f',$t/$k)*100).'%':0;

                $list_by_boss_id[$v['boss_id']]['3_1_c'] = $equip_by_boss_id[$v['boss_id']][3][1]?:0;
                $list_by_boss_id[$v['boss_id']]['3_2_c'] = $equip_by_boss_id[$v['boss_id']][3][2]?:0;
                $list_by_boss_id[$v['boss_id']]['4_1_c'] = $equip_by_boss_id[$v['boss_id']][4][1]?:0;
                $list_by_boss_id[$v['boss_id']]['4_2_c'] = $equip_by_boss_id[$v['boss_id']][4][2]?:0;
            }
        }

        if(request('excel')){
            /**
             * 导出excel
             */
            $headerArray = array('BOOS名称','掉落装备','BOOS_ID','BOSS等级','被击杀次数','击杀数据','橙装1星','橙装1星占比','橙装2星','橙装2星占比','红装1星','红装1星占比','红装2星','红装2星占比');
            $bodyArray = array();
            foreach ($list_by_boss_id as $v) {
                $bodyArray[] = array(
                    $v['boss_name'],$v['equip_order'],$v['boss_id'],$v['boss_lv'],$v['boss_kill'],
                    $killer_by_boss_id[$v['boss_id']],$v['3_1_c'],$v['3_1'],$v['3_2_c'],$v['3_2'],
                    $v['4_1_c'],$v['4_1'],$v['4_2_c'],$v['4_2']
                );
            }
            $this->export_to_excel($headerArray,$bodyArray,'BOSS掉落统计');
        }

        $assign['list'] = $list_by_boss_id;
        $assign['killer'] = $killer_by_boss_id;

        $this->display("game_data/boss_drop_rate.shtml", $assign);
    }


    /**
     * 按物品统计掉落
     */
    public function equip_by_item()
    {
        $assign = array();
        $assign['boss_list'] = $this->get_boss();
        /**
         * 0白 1蓝 2紫 3橙 4红 5粉红
         */
        $assign['quality_list'] = $quality_list = array('1'=>'白装','2'=>'蓝装','3'=>'紫装','4'=>'橙装','5'=>'红装','6'=>'粉红');
        $assign['color_list'] = array('1'=>'#CCC','2'=>'blue','3'=>'purple','4'=>'orange','5'=>'red','6'=>'pink');

        $where = "where 1";
        $st = request('st');
        $et = request('et');
        $boss_id = request('boss_id');

        $quality = request('quality','int');

        $assign['st'] = $st = $st?$st:date('Y-m-d');
        $assign['et'] = $et = $et?$et:date('Y-m-d');


        if($st) {
            $where .= ' and log_time >' .strtotime($st);
        }
        if($et){
            $where .= ' and log_time<='. (strtotime($et)+86400);
        }
        if($quality){
            $where .= ' and quality = '.($quality - 1);
        }

        if($boss_id){
            $where .= ' and boss_id='.$boss_id;
        }

        //备注: 一个item_id对应唯一quality
        $list = array();
        $sql = "select item_tid,item_name,quality,star,sum(amount) as s from log_world_boss {$where} and `op_type`=2 and item_tid>100000000 group by item_tid";
        $re = $this->gamedb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $v['equip_order'] = (int)substr($v['item_tid'],-4,-2).'阶';
                $list[] = $v;
            }
        }

        if(request('excel')){
            /**
             * 导出excel
             */
            $headerArray = array('物品名称','物品ID','装备阶数','装备属性','掉落数量');
            $bodyArray = array();
            foreach ($list as $v) {
                $bodyArray[] = array(
                    $v['item_name'],$v['item_tid'],$v['equip_order'],$quality_list[$v['quality']],$v['s'],
                );
            }
            $this->export_to_excel($headerArray,$bodyArray,'BOSS红橙装统计');
        }

        $assign['list'] = $list;

        $this->display("game_data/boss_equip_by_item.shtml", $assign);

    }

}




































































































