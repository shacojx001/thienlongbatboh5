<?php
/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/2
 * Time: 上午10:15
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';
abstract class base extends action_superclass{

    public $gamedb;
    public $admindb;
    public $centerdb;

    function __construct()
    {
        parent::__construct();
        $sid = $_SESSION['selected_sid'];
        $this->gamedb = Config::gamedb($sid);
        $this->admindb = Config::admindb();
        $this->centerdb = Config::centerdb();
    }
    /**
     * 抽象方法由子类实现
     * @return mixed
     */
    abstract function handle();

    /**
     * 获取opt
     * @return array
     */
    protected function get_opt(){
        $list = array();
        $sql = "select * from base_config where `type`=3";
        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['id']] = $v['item'];
            }
        }
        return $list;
    }

    /**
     * 获取帮派信息
     * @param $guild_id
     * @return array
     */
    protected function get_guild_info($guild_id){
        $sql = "select guild_id,name,annoucement,president_id,b.nick_name,creator_id,c.nick_name as create_name,
                  FROM_UNIXTIME(create_time,'%Y-%m-%d %H:%i:%s') as create_time,
                  a.level,member_num,fund,fight_level,storage_item_num,boss_food,total_fight_level,
                  open_boss_cnt,
                  FROM_UNIXTIME(open_boss_time,'%Y-%m-%d %H:%i:%s') as open_boss_time,
                  boss_max_hp,boss_cur_hp,
                  FROM_UNIXTIME(boss_refresh_time,'%Y-%m-%d %H:%i:%s') as boss_refresh_time,
                  league_win_cnt
                  from guild a left join role_base b on a.president_id = b.role_id left join role_base c on a.creator_id = c.role_id where guild_id = $guild_id";
        $res = $this->gamedb->fetchOne($sql);

        return $res;
    }
}