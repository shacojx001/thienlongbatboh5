<?php
/**
 * Created by PhpStorm.
 * User: jc
 * Date: 18/9/26
 * Time: 上午10:15
 */
!defined(ACTION_GATEWAY_PATH) or die ( 'no access!' );
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';
abstract class base extends action_superclass{

    public $gamedb;
    public $admindb;
    public $centerdb;

    function __construct()
    {
        parent::__construct();
        $sid = $_SESSION['selected_sid'];
        $this->gamedb = Config::gamedb($sid);
        $this->admindb = Config::admindb();
        $this->centerdb = Config::centerdb();
    }
    /**
     * 抽象方法由子类实现
     * @return mixed
     */
    abstract function handle();



}