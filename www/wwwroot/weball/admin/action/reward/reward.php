<?php
/**
 * 特殊日志需求
 * Date: 18/9/26
 */
!defined(ACTION_GATEWAY_PATH) or die ('no access!');
require_once dirname(__FILE__) . '/base.php';

class reward extends base
{

    public function handle(){

    }


//////奖金池
    public function reward_show(){
     //获取服务器列表权限
      $assign = $this->get_server_rights();

      $assign['show'] = count($assign['channel'])>=1?0:1;

      $assign['ctl'] = "reward";
      $assign['act'] = "reward";
      $assign['method'] = "reward_show";
      $url = "?ctl=" . $assign['ctl'] . "&act=" . $assign['act'] . "&method=" . $assign['method'];

      $assign['on'] = array(
            'gid_sid_select' => 1,
            'role_id' => 1,
            'nickname' => 1,
            'time' => 0,
  
      );

        $where = " WHERE 1 ";

        if ($_REQUEST['gid']) {
            //奖金池比例
            $sql = "SELECT
                     reward_rate
                FROM
                  adminplatformlist
                WHERE
                  gid=".$_REQUEST['gid'];
            $res = $this->admindb->fetchOne($sql);
            $rate = $res['reward_rate']?$res['reward_rate']/100:0;
        }


        if ($_REQUEST['role_id']) {
            $where .= " AND r.role_id =" . $_REQUEST['role_id'];
        }


        if ($_REQUEST['nickname']) {
            $where .= " AND r.name like '%" . $_REQUEST['nickname'] . "%'";
        }



        if($_REQUEST['sid']){
           $gid = $_REQUEST['gid'];
           $sid = $_REQUEST['sid'];

           //当天零点时间戳
           $stime = strtotime(date("Y-m-d"));
           
            if(count($_SESSION['channel'])>0){
              $channel_str = implode(",",$_SESSION['channel']);
              $where_channel = "and source in($channel_str)";
            }else{
              $where_channel = "";
            }

           //该渠道总充值
           $sql = "SELECT floor(sum(money*0.01)) AS money FROM center_charge WHERE gid=$gid $where_channel";

           $money = $this->admindb->fetchOne($sql);

           $sql = "select id as sid,otime from adminserverlist where gid=$gid";
           $sid_list = $this->admindb->fetchRow($sql,'sid');

            if($sid_list[$sid]['otime']>$stime){//新服
                $assign['is_new'] = 1;


                 //奖金池用了的奖金
                 $sql = "SELECT
                              sum(gold) AS gold
                            FROM
                              reward_audit_log
                            WHERE
                              gid = $gid
                            AND sid = $sid  ";
                 $res = $this->centerdb->fetchOne($sql);

                 $cost_gold = $res['gold']?$res['gold']:0;

                 $sql = "SELECT
                              (reward_today*10-$cost_gold) AS money
                            FROM
                              adminplatformlist
                            WHERE
                              gid = $gid";
                 $res = $this->admindb->fetchOne($sql);

                 $sql = "SELECT
                              reward_today
                            FROM
                              adminplatformlist
                            WHERE
                              gid = $gid";
                 $res_reward_today = $this->admindb->fetchOne($sql);
                 $assign['sum_gold'] = $res_reward_today['reward_today']*10;
           // print_r($res);exit;

            }
            else{//非当天新服
                 $assign['is_new'] = 0;


                 //奖金池用了的奖金
                 $sql = "SELECT
                              sum(gold) AS gold
                            FROM
                              reward_audit_log
                            WHERE
                              gid = $gid
                            AND is_new=0  ";
                 $res = $this->centerdb->fetchOne($sql);

                 $cost_gold = $res['gold']?$res['gold']:0;


                 $sql = "SELECT floor(SUM(money*0.01)*10*{$rate}-$cost_gold) AS money FROM center_charge WHERE gid=$gid $where_channel";

                 $res = $this->admindb->fetchOne($sql);
                 $assign['sum_gold'] = $money['money']*10*$rate;//总奖金

             }

//           $button = "<a href=/action_gateway.php?ctl=reward&act=reward&method=unset_newan&sid=33&role_id=fdfdf><button>取消内玩</button></a>";
           $symbol = '"';
           if($_REQUEST['role_id'] || $_REQUEST['nickname']){
               $sql = "SELECT
                       concat(
                          '<button onclick=synch(',
                          '{$symbol}',
                          r.role_id,
                          '{$symbol}',
                          ',',
                          '{$symbol}',
                          r.accname,
                          '{$symbol}',
                          ',',
                          '{$symbol}',
                          REPLACE(REPLACE(r.name, CHAR(10), ''), CHAR(13), ''),
                          '{$symbol}',
                          ',',
                          '{$symbol}',
                          cr.source,
                          '{$symbol}',
                          ')>添加</button>'
                        ) AS str,
                        r.role_id,
                        r.accname,
                        r.name,
                        r. LEVEL,
                        r.last_login_ip,
                        FROM_UNIXTIME(r.last_login_time),
                        IFNULL(0,sum(money)) AS money,

                    IF (
                        r.state = 1,
                        '封号',
                        '正常'
                    ) AS state
                    FROM
                        role_basic r
                    LEFT JOIN recharge c ON r.role_id = c.role_id
                    LEFT JOIN role_create cr ON r.role_id = cr.role_id
                      $where
                      GROUP BY
                        role_id";

               $list = Config::gamedb($_REQUEST['sid'])->fetchRow($sql);

               foreach ($list as $key => $val) {
                  $list[$key]['str'] = str_replace(array("'"), array(''), $val['str']);
                } 
// print_r($list); exit;
           }
           // else{//内玩
           //     $sql = "SELECT
           //             concat(
           //                '<button onclick=synch(',
           //                '{$symbol}',
           //                r.role_id,
           //                '{$symbol}',
           //                ',',
           //                '{$symbol}',
           //                r.acc_name,
           //                '{$symbol}',
           //                ',',
           //                '{$symbol}',
           //                r.nick_name,
           //                '{$symbol}',
           //                ',',
           //                '{$symbol}',
           //                r.source,
           //                '{$symbol}',
           //                ')>添加</button>'
           //              ) AS str,
           //              r.role_id,
           //              r.acc_name,
           //              r.nick_name,
           //              r.level,
           //              r.last_login_ip,
           //              FROM_UNIXTIME(r.last_login_time),
           //              sum(money) AS money,
           //              r.source,
           //              IF (
           //                r.state = 1,
           //                '<b style=color:red>封号</b>',
           //                '<b style=color:green>正常</b>'
           //              ) AS state,
           //              IF (
           //                r.is_inner = 1,
           //                '<b style=color:red>内玩</b>',
           //                '<b style=color:green>玩家</b>'
           //              ) AS use_market,
           //            concat('<a href=/action_gateway.php?ctl=reward&act=reward&method=unset_newan&sid=',$sid,'&role_id=',r.role_id,'><button>取消内玩</button></a>') as button

           //            FROM
           //              role_base r
           //            LEFT JOIN charge c ON r.role_id = c.role_id
           //            $where
           //            AND r.is_inner=1
           //            GROUP BY
           //              role_id";
             
           //    $list = Config::gamedb($_REQUEST['sid'])->fetchRow($sql);

           // }
                      
        }
// print_r($list);exit;

        $assign['sum_money'] = $money['money']?$money['money']:0;
        $assign['gold'] = $money['money']*10;
        $assign['rate'] = $rate;
        $assign['cost_gold'] = $cost_gold;


        $assign['list'] = $list;
        $assign['rank'] = array(
            '添加',
            '玩家ID',
            '玩家账号',
            '玩家名',
            '玩家等级',
            '玩家登录IP',
            '玩家最后登录时间',
            '玩家充值金额',
            '渠道',
            '玩家状态',
            // '是否内玩',
            '操作',
        );
      
      $assign['money'] = $res['money']?$res['money']:0; 
      $assign['user'] = $_SESSION['username'];

        $this->display('reward/reward_show.shtml', $assign);
  }

  //取消内玩
  function unset_newan(){
      $sid = $_GET['sid'];
      $role_id = intval($_GET['role_id']);
      $erl = $this->get_center_erl($sid);
      $res = $erl->get("lib_sell", "set_role_use_market_auth", '[~i,~i]', array(
              array(
                  $role_id,
                  1,
              )
      ));
      alert('修改成功！');
  }

  //奖金发放
  function reward_audit(){
      
      $role_id = $_REQUEST['role_id'];
      $accname = $_REQUEST['accname'];
      $nickname = $_REQUEST['nickname'];
      $channel = $_REQUEST['channel'];
      $gold = $_REQUEST['gold'];//元宝卡
      $sid = $_REQUEST['sid'];//服ID
      $gid = $_REQUEST['gid'];//gid
      $is_new = $_REQUEST['is_new'];//是否当天新服

      $user_name = $_REQUEST['user_name'];//使用者
      $ramk = $_REQUEST['ramk'];//申请者
      $auditer = $_SESSION['username'];//操作人

      if(empty($role_id) || empty($gold)){
         alert('请填写玩家ID或发送元宝卡！');
      }

      if(empty($sid)){
         alert('请选择游戏服！');
      }

      $sum_gold = 0;
      $begold = 0;
      //判断类型
      if($_GET['type']=='gold'){//元宝

          if( empty($gold['33020000']) &&
              empty($gold['33020001']) &&
              empty($gold['33020002']) &&
              empty($gold['33020003']) &&
              empty($gold['33020004']) &&
              empty($gold['33020005']) &&
              empty($gold['33020006']) &&
              empty($gold['33020007']) &&
              empty($gold['33020008']) &&
              empty($gold['33020009']) &&
              empty($gold['33020010']) &&
              empty($gold['33020011']) &&
              empty($gold['33020012'])
          ){
             alert('请填写发放物资数量！');exit;
          }

          $good = array();
          $sum_gold = 0;
          $post_insert = "";
          //发送物品组装
          foreach ($gold as $key => $val) {

              if($val){
                  $good[] = '{'.$key.','.$val.',1,0}';
              }

              if($key=='33020000' && $val){//18元至尊御魂宝箱·内
                $sum_gold = $sum_gold + 180*$val;
                $post_insert = $post_insert.'18元至尊御魂宝箱·内*'.$val.'<br>';

              }elseif($key=='33020001' && $val){//60元宝卡·内
                $sum_gold = $sum_gold + 60*$val;
                $post_insert = $post_insert.'60元宝卡·内*'.$val.'<br>';

              }elseif($key=='33020002' && $val){//300元宝卡·内
                $sum_gold = $sum_gold + 300*$val;
                $post_insert = $post_insert.'300元宝卡·内*'.$val;

              }elseif($key=='33020003' && $val){//680元宝卡·内
                $sum_gold = $sum_gold + 680*$val;
                $post_insert = $post_insert.'680元宝卡·内*'.$val;

              }elseif($key=='33020004' && $val){//1280元宝卡·内
                $sum_gold = $sum_gold + 1280*$val;
                $post_insert = $post_insert.'1280元宝卡·内*'.$val;

              }elseif($key=='33020005' && $val){//1980元宝卡·内
                $sum_gold = $sum_gold + 1980*$val;
                $post_insert = $post_insert.'1980元宝卡·内*'.$val;

              }elseif($key=='33020006' && $val){//3280元宝卡·内
                  $sum_gold = $sum_gold + 3280*$val;
                  $post_insert = $post_insert.'3280元宝卡·内*'.$val;

              }elseif($key=='33020007' && $val){//6480元宝卡·内
                  $sum_gold = $sum_gold + 6480*$val;
                  $post_insert = $post_insert.'6480元宝卡·内*'.$val;

              }elseif($key=='33020008' && $val){//10000元宝卡·内
                  $sum_gold = $sum_gold + 10000*$val;
                  $post_insert = $post_insert.'10000元宝卡·内*'.$val;

              }elseif($key=='33020009' && $val){//20000元宝卡·内
                  $sum_gold = $sum_gold + 20000*$val;
                  $post_insert = $post_insert.'20000元宝卡·内*'.$val;

              }elseif($key=='33020010' && $val){//30000元宝卡·内
                  $sum_gold = $sum_gold + 30000*$val;
                  $post_insert = $post_insert.'30000元宝卡·内*'.$val;

              }elseif($key=='33020011' && $val){//50000元宝卡·内
                  $sum_gold = $sum_gold + 50000*$val;
                  $post_insert = $post_insert.'50000元宝卡·内*'.$val;

              }elseif($key=='33020012' && $val){//100000元宝卡·内
                  $sum_gold = $sum_gold + 100000*$val;
                  $post_insert = $post_insert.'100000元宝卡·内*'.$val;

              }
          }



          ///查询发放奖金是否大于剩下奖金池奖金
          //奖金池比例
            $sql = "SELECT
                     reward_rate,
                     reward_today
                FROM
                  adminplatformlist
                WHERE
                  gid=".$_REQUEST['gid'];

            $res = $this->admindb->fetchOne($sql);

           if(count($_SESSION['channel'])>0){
              $channel_str = implode(",",$_SESSION['channel']);
              $where_channel = "and source in($channel_str)";
            }else{
              $where_channel = "";
            }

            //奖金池用了的奖金
           if($is_new==1){//新服
              $where = " AND sid=$sid";
           }else{
              $where = " AND is_new=0";
              $is_new=0;
           }
           $sql = "SELECT
                        sum(gold) AS gold
                      FROM
                        reward_audit_log
                      WHERE
                        gid = $gid
                      $where  ";
           $res = $this->centerdb->fetchOne($sql);

           $cost_gold = $res['gold']?$res['gold']:0;

           $sql = "SELECT
                            (reward_today*10-$cost_gold) AS money,
                            reward_rate
                          FROM
                            adminplatformlist
                          WHERE
                            gid = $gid";
           $res = $this->admindb->fetchOne($sql);

          if($is_new==1){//新服


          }else{//非新服

               $rate = $res['reward_rate']?$res['reward_rate']/100:0;

               $sql = "SELECT floor(SUM(money*0.01)*10*{$rate}-$cost_gold) AS money FROM center_charge WHERE gid=$gid $where_channel";

               $res = $this->admindb->fetchOne($sql);
          }

          $new_server_plus = 0;
          if( !empty($_REQUEST['new_server_plus']) ){
              $new_server_plus = intval($_REQUEST['new_server_plus']);
          }
          ///查询发放奖金是否大于剩下奖金池奖金
         if($sum_gold>$res['money']+$new_server_plus){
            alert('发放奖金超出额度！');exit;
         }

         //拼接
          $post = "[".implode(",",$good)."]";

      }else{//绑定元宝

          $begold = intval($_GET['begold']);
          if(empty($begold)){
            alert('请填写发放物资数量！');exit;
          }
        //拼接
          $post = "[{2,$begold}]";
      }
      
      $role_id_insert = intval($role_id);
      $role_id = "[".rtrim($role_id,',')."]";


      $title = '系统邮件';
      $content = '祝游戏愉快！';
      $time = time();

      $sql = "INSERT INTO reward_audit_log (
                  gid,
                  sid,
                  role_id,
                  accname,
                  nickname,
                  post,
                  gold,
                  begold,
                  user_name,
                  ramk,
                  auditer,
                  channel,
                  time,
                  is_new
                )
                VALUES
                  (
                    '$gid',
                    '$sid',
                    '$role_id_insert',
                    '{$accname}',
                    '{$nickname}',
                    '$post_insert',
                    '$sum_gold',
                    '$begold',
                    '$user_name',
                    '$ramk',
                    '$auditer',
                    '{$channel}',
                    '$time',
                    '$is_new' 
                  )";
      $this->centerdb->query($sql);


      $result_inner = Erlang::erl_gm_up_inner($sid, $role_id, 1 );

      //指定玩家
      //$post  [{33020000,1,1,0}]
      $result = Erlang::erl_send_mail_new($sid, $role_id, $title, $content, $post);

    // print_r($result);exit;
      alert('发送成功！修改为内玩：'.json_encode($result_inner).'  邮件发送: '.json_encode($result));

  }

  //奖金池日志
  function reward_audit_log(){

      $assign = $this->get_server_rights();
      $assign['on'] = array(
          'gid_sid_select' => 1,
          'role_id' => 1,
          'nickname' => 1,
          'time' => 1,
          'export' => 1,

      );

      $assign['ctl'] = "reward";
      $assign['act'] = "reward";
      $assign['method'] = "reward_audit_log";
      // 专服列表
      $sql = "SELECT
                    gid,
                    platform_name
                  FROM
                    adminplatformlist";
      $gid_list = $this->returnK($this->admindb->fetchRow($sql), "gid");

      //游戏服列表
      $sql = "SELECT
                    id AS sid,
                    gid,
                    description
                  FROM
                    adminserverlist
                  WHERE
                  `default` in(1,6)";
      $sid_list = $this->returnK($this->admindb->fetchRow($sql), "sid");

      $url = "?ctl=reward&act=reward&method=reward_audit_log";
      $where = " WHERE 1 ";

      if ($_REQUEST['gid']) {

          $where .= " AND gid =" . $_REQUEST['gid'];
      }

      if ($_REQUEST['sid']) {
          $where .= " AND sid =" . $_REQUEST['sid'];
      }

      if ($_REQUEST['role_id']) {
          $where .= " AND role_id =" . $_REQUEST['role_id'];
      }

      if ($_REQUEST['accname']) {
          $where .= " AND accname ='" . $_REQUEST['accname'] . "'";
      }

      if ($_REQUEST['nickname']) {
          $where .= " AND nickname like '%" . $_REQUEST['nickname'] . "%'";
      }

      if ($_GET['start_time'] && $_GET['end_time']) {
            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']);
            $where .= " AND time between $start_time AND $end_time ";
      }


      $total_record = $this->centerdb->fetchOne("SELECT COUNT(*) as total from reward_audit_log $where");
      $total_record = $total_record['total'];

      $per_page = 150;
      $total_page = ceil($total_record / $per_page);
      $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
      $cur_page = $cur_page < 1 ? 1 : $cur_page;
      $limit_start = ($cur_page - 1) * $per_page;
      $limit = " LIMIT {$limit_start}, $per_page ";
      $orderby = " order by  id desc ";


      $sql = "SELECT
                  gid,
                  sid,
                  role_id,
                  accname,
                  REPLACE(REPLACE(nickname, CHAR(10), ''), CHAR(13), ''),
                  channel,
                  gold,
                  post,
                  begold,
                  auditer,
                  user_name,
                  ramk,
                  FROM_UNIXTIME(time) AS time
                FROM
                  reward_audit_log
                $where
                $orderby

                  ";

      if($_REQUEST['export']!='导出'){
          $sql .= " $limit ";
          ini_set('memory_limit', '2048M');
      }

      $list = $this->centerdb->fetchRow($sql);
      foreach ($list as $key => &$val) {
//          $list[$key]['gid'] = $gid_list[$val['gid']]['platform_name'];
//          $list[$key]['sid'] = $sid_list[$val['sid']]['description'];
          $val['gid'] = $gid_list[$val['gid']]['platform_name'];
          $val['sid'] = $sid_list[$val['sid']]['description'];
      }
      $assign['rank'] = array(
            '专服',
            '游戏服',
            '角色ID',
            '角色账号',
            '角色名字',
            '渠道',
            '申请元宝数',
            '申请物资',
            '申请绑元数',
            '发放管理员',
            '申请人',
            '备注',
            '申请时间',
        );

      if($_REQUEST['export']=='导出'){
          // $this->export_txt_file( &$list,$assign['rank'], '奖金池发放日志');
      }

      $assign['list'] = $list;
      $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_GET) . "&page=",
        );
      
      $this->display('reward/template.shtml', $assign);
      // print_r($list);exit;            
  }

///修改奖金池比例
  function reward_rate_list(){
      $sql = "SELECT
                  gid,
                  platform_name,
                  reward_rate,
                  reward_today
                FROM
                  adminplatformlist";
      $res = $this->admindb->fetchRow($sql);
      
      $assign['list'] = json_encode($res);

      $this->display('reward/reward_rate_list.shtml', $assign);

  }

/////
    function reward_rate_update(){
        $gid = $_REQUEST['gid'];
        $reward_rate = $_REQUEST['reward_rate'];
        $reward_today = $_REQUEST['reward_today'];
        if(is_numeric($reward_rate) && is_numeric($reward_today)){
            $sql = "UPDATE adminplatformlist SET reward_rate=$reward_rate,reward_today=$reward_today WHERE gid=$gid";
            $this->admindb->query($sql);
            echo json_encode(array('msg'=>true));
        }else{
            echo json_encode(array('msg'=>false));
        }
    }



    public function reward_to_send_audit($data,$data_session){

        $role_id = $data['role_id'];
        $accname = $data['accname'];
        $nickname = $data['nickname'];
        $channel = $data['channel'];
        $gold = $data['gold'];//元宝卡
        $sid = $data['sid'];//服ID
        $gid = $data['gid'];//gid
        $is_new = $data['is_new'];//是否当天新服

        //当天零点时间戳
        $is_new = 0;
        $stime = strtotime(date("Y-m-d"));
        $sql = "select id as sid,otime from adminserverlist where gid=$gid";
        $sid_list = $this->admindb->fetchRow($sql,'sid');
        if($sid_list[$sid]['otime']>$stime){//新服
            $is_new = 1;
        }

        $user_name = $data['user_name'];//使用者
        $ramk = $data['ramk'];//申请者
        $auditer = $data_session['username'];//操作人

        if(empty($role_id) || empty($gold)){
            return array(false,'请填写玩家ID或发送元宝卡！');
        }

        if(empty($sid)){
            return array(false,'请选择游戏服！');
        }

        $sum_gold = 0;
        $begold = 0;
        //判断类型
        if($data['type']=='gold'){//元宝

            if( empty($gold['33020000']) &&
                empty($gold['33020001']) &&
                empty($gold['33020002']) &&
                empty($gold['33020003']) &&
                empty($gold['33020004']) &&
                empty($gold['33020005']) &&
                empty($gold['33020006']) &&
                empty($gold['33020007']) &&
                empty($gold['33020008']) &&
                empty($gold['33020009']) &&
                empty($gold['33020010']) &&
                empty($gold['33020011']) &&
                empty($gold['33020012'])
            ){
                return array(false,'请填写发放物资数量！');
            }

            $good = array();
            $sum_gold = 0;
            $post_insert = "";
            //发送物品组装
            foreach ($gold as $key => $val) {

                if($val){
                    $good[] = '{'.$key.','.$val.',1,0}';
                }

                if($key=='33020000' && $val){//18元至尊御魂宝箱·内
                    $sum_gold = $sum_gold + 180*$val;
                    $post_insert = $post_insert.'18元至尊御魂宝箱·内*'.$val.'<br>';

                }elseif($key=='33020001' && $val){//60元宝卡·内
                    $sum_gold = $sum_gold + 60*$val;
                    $post_insert = $post_insert.'60元宝卡·内*'.$val.'<br>';

                }elseif($key=='33020002' && $val){//300元宝卡·内
                    $sum_gold = $sum_gold + 300*$val;
                    $post_insert = $post_insert.'300元宝卡·内*'.$val;

                }elseif($key=='33020003' && $val){//680元宝卡·内
                    $sum_gold = $sum_gold + 680*$val;
                    $post_insert = $post_insert.'680元宝卡·内*'.$val;

                }elseif($key=='33020004' && $val){//1280元宝卡·内
                    $sum_gold = $sum_gold + 1280*$val;
                    $post_insert = $post_insert.'1280元宝卡·内*'.$val;

                }elseif($key=='33020005' && $val){//1980元宝卡·内
                    $sum_gold = $sum_gold + 1980*$val;
                    $post_insert = $post_insert.'1980元宝卡·内*'.$val;

                }elseif($key=='33020006' && $val){//3280元宝卡·内
                    $sum_gold = $sum_gold + 3280*$val;
                    $post_insert = $post_insert.'3280元宝卡·内*'.$val;

                }elseif($key=='33020007' && $val){//6480元宝卡·内
                    $sum_gold = $sum_gold + 6480*$val;
                    $post_insert = $post_insert.'6480元宝卡·内*'.$val;

                }elseif($key=='33020008' && $val){//10000元宝卡·内
                    $sum_gold = $sum_gold + 10000*$val;
                    $post_insert = $post_insert.'10000元宝卡·内*'.$val;

                }elseif($key=='33020009' && $val){//20000元宝卡·内
                    $sum_gold = $sum_gold + 20000*$val;
                    $post_insert = $post_insert.'20000元宝卡·内*'.$val;

                }elseif($key=='33020010' && $val){//30000元宝卡·内
                    $sum_gold = $sum_gold + 30000*$val;
                    $post_insert = $post_insert.'30000元宝卡·内*'.$val;

                }elseif($key=='33020011' && $val){//50000元宝卡·内
                    $sum_gold = $sum_gold + 50000*$val;
                    $post_insert = $post_insert.'50000元宝卡·内*'.$val;

                }elseif($key=='33020012' && $val){//100000元宝卡·内
                    $sum_gold = $sum_gold + 100000*$val;
                    $post_insert = $post_insert.'100000元宝卡·内*'.$val;

                }
            }



            ///查询发放奖金是否大于剩下奖金池奖金
            //奖金池比例
            $sql = "SELECT
                     reward_rate,
                     reward_today
                FROM
                  adminplatformlist
                WHERE
                  gid=".$data['gid'];

            $res = $this->admindb->fetchOne($sql);

            if(count($data_session['channel'])>0){
                $channel_str = implode(",",$data_session['channel']);
                $where_channel = "and source in($channel_str)";
            }else{
                $where_channel = "";
            }

            //奖金池用了的奖金
            if($is_new==1){//新服
                $where = " AND sid=$sid";
            }else{
                $where = " AND is_new=0";
                $is_new=0;
            }
            $sql = "SELECT
                        sum(gold) AS gold
                      FROM
                        reward_audit_log
                      WHERE
                        gid = $gid
                      $where  ";
            $res = $this->centerdb->fetchOne($sql);

            $cost_gold = $res['gold']?$res['gold']:0;

            $sql = "SELECT
                            (reward_today*10-$cost_gold) AS money,
                            reward_rate
                          FROM
                            adminplatformlist
                          WHERE
                            gid = $gid";
            $res = $this->admindb->fetchOne($sql);

            if($is_new==1){//新服


            }else{//非新服

                $rate = $res['reward_rate']?$res['reward_rate']/100:0;

                $sql = "SELECT floor(SUM(money*0.01)*10*{$rate}-$cost_gold) AS money FROM center_charge WHERE gid=$gid $where_channel";

                $res = $this->admindb->fetchOne($sql);
            }

            $new_server_plus = 0;
            if( !empty($data['new_server_plus']) ){
                $new_server_plus = intval($data['new_server_plus']);
            }
            ///查询发放奖金是否大于剩下奖金池奖金
            if($sum_gold>$res['money']+$new_server_plus){
                return array(false,'发放奖金超出额度！');
            }

            //拼接
            $post = "[".implode(",",$good)."]";

        }else{//绑定元宝

            $begold = intval($data['begold']);
            if(empty($begold)){
                return array(false,'请填写发放物资数量！');
            }
            //拼接
            $post = "[{2,$begold}]";
        }

        $role_id_insert = intval($role_id);
        $role_id = "[".rtrim($role_id,',')."]";


        $title = '系统邮件';
        $content = '祝游戏愉快！';
        $time = time();

        $sql = "INSERT INTO reward_audit_log (
                  gid,
                  sid,
                  role_id,
                  accname,
                  nickname,
                  post,
                  gold,
                  begold,
                  user_name,
                  ramk,
                  auditer,
                  channel,
                  time,
                  is_new
                )
                VALUES
                  (
                    '$gid',
                    '$sid',
                    '$role_id_insert',
                    '{$accname}',
                    '{$nickname}',
                    '$post_insert',
                    '$sum_gold',
                    '$begold',
                    '$user_name',
                    '$ramk',
                    '$auditer',
                    '{$channel}',
                    '$time',
                    '$is_new'
                  )";
        $is_insert = $this->centerdb->query($sql);


        $result_inner = Erlang::erl_gm_up_inner($sid, $role_id, 1 );

        //指定玩家
        //$post  [{33020000,1,1,0}]
        $result = Erlang::erl_send_mail_new($sid, $role_id, $title, $content, $post);

        // print_r($result);exit;
        return array(true,'发送成功！修改为内玩：'.json_encode($result_inner).'  邮件发送: '.json_encode($result));

    }


    //导表
    function export_txt_file($list,$headerArray, $title)
    {
        Auth::check_action(Auth::$REWARD_LOG_EXPORT);
        Admin::adminLog("导出日志-:$title");
        ini_set('memory_limit', '2048M');

        $data = '';
        $data .= implode("\t", $headerArray);
        $data .= "\r\n";
        foreach ($list as $key => $val) {
            $line = array();
            foreach ($val as $k => $v) {
                $line[] = $v;
            }
            $data .= implode("\t", $line);
            $data .= "\r\n";
        }
        $file_n = '日志导出'.$title.'.txt';
        $file_name = ROOT_DIR.'log'.DIRECTORY_SEPARATOR.$file_n;
        file_put_contents( $file_name, $data);

        $data = file_get_contents($file_name);
        header("content-type:application/octet-stream");
        header('Content-Disposition: attachment; filename=' . $file_n );
        header("Content-Transfer-Encoding: binary");
        echo $data;
        exit;
    }

    public function reward_sh(){

        $assign = $this->get_server_rights();
        $assign['on'] = array(
            'gid_sid_select' => 1,
            'role_id' => 1,
            'nickname' => 1,
            'time' => 1,
            'export' => 1,
            'state' => 1,
        );
        $assign['ctl'] = "reward";
        $assign['act'] = "reward";
        $assign['method'] = "reward_sh";



        // 专服列表
        $sql = "SELECT
                    gid,
                    platform_name
                  FROM
                    adminplatformlist";
        $gid_list = $this->returnK($this->admindb->fetchRow($sql), "gid");

        //游戏服列表
        $sql = "SELECT
                    id AS sid,
                    gid,
                    description
                  FROM
                    adminserverlist
                  WHERE
                  `default` in(1,6)";
        $sid_list = $this->returnK($this->admindb->fetchRow($sql), "sid");

        $url = "?ctl=reward&act=reward&method=reward_audit_log";
        $where = " WHERE 1 ";
        if ($_REQUEST['state']) {
            $where .= " AND state =" . $_REQUEST['state'];
        }

        if ($_REQUEST['gid']) {
            $where .= " AND gid =" . $_REQUEST['gid'];
        }

        if ($_REQUEST['sid']) {
            $where .= " AND sid =" . $_REQUEST['sid'];
        }

        if ($_REQUEST['role_id']) {
            $where .= " AND role_id =" . $_REQUEST['role_id'];
        }

        if ($_REQUEST['accname']) {
            $where .= " AND accname ='" . $_REQUEST['accname'] . "'";
        }

        if ($_REQUEST['nickname']) {
            $where .= " AND nickname like '%" . $_REQUEST['nickname'] . "%'";
        }

        if ($_GET['start_time'] && $_GET['end_time']) {
            $start_time = strtotime($_GET['start_time']);
            $end_time = strtotime($_GET['end_time']);
            $where = " AND time between $start_time AND $end_time ";
        }


        $total_record = $this->centerdb->fetchOne("SELECT COUNT(*) as total from reward_apply $where");
        $total_record = $total_record['total'];

        $per_page = 150;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by id desc,state asc ";


        $sql = "SELECT
                  id,
                  gid,
                  sid,
                  role_id,
                  accname,
                  nickname,
                  channel,
                  item,
                  begold,
                  user_name,
                  ramk,
                  FROM_UNIXTIME(time) AS time,
                  state
                FROM
                  reward_apply
                $where
                $orderby

                  ";

        if($_REQUEST['export']!='导出'){
            $sql .= " $limit ";
        }

        $list = $this->centerdb->fetchRow($sql);
        foreach ($list as $key => $val) {
            $list[$key]['gid'] = $gid_list[$val['gid']]['platform_name'];
            $list[$key]['sid'] = $sid_list[$val['sid']]['description'];
        }
        $assign['rank'] = array(
            'id',
            '专服',
            '游戏服',
            '角色ID',
            '角色账号',
            '角色名字',
            '渠道',
            '申请物资',
//            '申请元宝数',
            '申请绑元数',
            '申请人',
            '备注',
            '申请时间',
            '状态',
            '操作',
        );
        if($_REQUEST['export']=='导出'){
            $this->export_txt_file( $list,$assign['rank'], '奖金池申请日志');
        }

        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => $url . http_build_query($_GET) . "&page=",
        );

        $this->display('reward/reward_sh.shtml', $assign);
    }


    public function reward_sh_action(){
        $id = request('id','int');
        $state = request('state','int');
        if(empty($id)||empty($state)){
            exit(json_encode(array('ste'=>400,'msg'=>'参数错误')));
        }
        $sql = "select * from reward_apply where id=$id";
        $data = $this->centerdb->fetchOne($sql);
        if(empty($data)){
            exit(json_encode(array('ste'=>400,'msg'=>'参数错误!')));
        }
        //$state==3拒绝
        if( $state==3 ){
            $sql = "update reward_apply set state=$state where id=$id";
            $is_ok = $this->centerdb->query($sql);
            if(empty($is_ok)){
                exit(json_encode(array('ste'=>400,'msg'=>'操作失败!')));
            }
            exit(json_encode(array('ste'=>200,'msg'=>'操作成功')));
        }

        $sql = "update reward_apply set state=4 where id=$id";
        $this->centerdb->query($sql);

        //发送奖金池操作
        $sendData = array(
            'role_id' => $data['role_id'],
            'accname' => $data['accname'],
            'nickname' => $data['nickname'],
            'channel' => $data['channel'],
            'gold' => json_decode($data['item'],true),
            'sid' => $data['sid'],
            'gid' => $data['gid'],
            'is_new' => $data['is_new'],
            'ramk' => $data['ramk'],
            'type' => $data['type'],
            'new_server_plus' => 0,
            'begold' => $data['begold'],
            'user_name' => $data['user_name'],
        );
        $dataSession = array(
            'username'=>$_SESSION['username'],
            'channel'=>$_SESSION['channel'],
        );
        $is_ok = $this->reward_to_send_audit($sendData,$dataSession);
        if($is_ok[0]!==true){
            exit(json_encode(array('ste'=>400,'msg'=>$is_ok[1])));
        }
        //发送奖金池操作

        $sql = "update reward_apply set state=$state where id=$id";
        $is_ok = $this->centerdb->query($sql);
        if(empty($is_ok)){
            exit(json_encode(array('ste'=>400,'msg'=>'操作失败!')));
        }
        exit(json_encode(array('ste'=>200,'msg'=>'操作成功')));
    }


////class end
}