<?php
/**
 * 错误日志
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_error extends action_superclass
{
	public function log_client_error(){
		$where = " WHERE 1 ";
		$orderby = " order by id desc ";
		$url = "action_gateway.php?ctl=error&act=log_client_error";
		
		$typeid = $_GET['typeid'];
		if(!empty($typeid)){
			$where .= " and id = '$typeid' ";
			$url .= "&typeid=$typeid";
		}
		
		$total_record = Config::centerdb()->fetchOne("SELECT COUNT(*) as total from log_client_error $where");
  		if(!empty($total_record)){
  			$total_record = $total_record['total'];
  		}else{
	        $total_record = 0;
  		}
	
	    $per_page = 30;
		$total_page = ceil($total_record / $per_page);
		$cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
		$cur_page = $cur_page < 1 ? 1 : $cur_page;
		$limit_start = ($cur_page - 1) * $per_page;
		$limit = " LIMIT {$limit_start}, $per_page ";
			
		$sql = "select * from log_client_error $where $orderby $limit";
		$list = Config::centerdb()->fetchRow($sql);
		$assign['list'] = $list;
		$assign['pages'] = array(
				    			"curpage" => $cur_page,
						        "totalpage" => $total_page,
						        "totalnumber" => $total_record,
						        "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
                              );
		$this->display('log_client_error.shtml',$assign);
	}
	public function clean_client_error(){
		$sql = "truncate table log_client_error";
		Config::centerdb()->query($sql);
		alert(Ext_Template::lang('清空成功'));
	}
}