<?php
/**
 * 系统
 */
require_once ACTION_GATEWAY_PATH . 'action/superclass.php';

class action_system extends action_superclass
{
    /**
     * 后台切换服用
     */
    public function saveurl()
    {
        $url = trim($_POST['url']);
        $_SESSION['toggle_admin_return_url'] = $url;
        exit;
    }
    // 顶部游戏服搜索框
    public function findServer()
    {   
        $name = $_REQUEST['name'];
    
        if(empty($name)){echo false;exit;}
     
        $where = '1';
        if ($_SESSION['channel'] && $_SESSION['super'] != 'YES') {
            foreach ($_SESSION['channel'] as $key => $val) {
                if (strpos($where, "FIND_IN_SET")) {
                    $where .= " OR  FIND_IN_SET('$val', channel_list) ";
                } else {
                    $where .= " AND ( FIND_IN_SET('$val', channel_list) ";
                }
            }
            $where .= ')';
        }
        $sql = "select * from adminserverlist where $where and (description LIKE '%$name%' or id LIKE '%$name%' or sdb LIKE '%$name%' )";
        $rst = $this->admindb->fetchRow($sql);

        if($rst){
            $today = strtotime(date("Y-m-d"));
            $tomorrow = $today + 86400;
            $yesterday = $today - 86400;
            $postnatal = $today + 86400*2;

            foreach ($rst as $v) {
                $color = '';
                if($this->server_color[$v['default']]){
                    $color = $this->server_color[$v['default']];
                }
                if($v['otime']>time()){//未到开服时间
                    $color = 10;
                }
                
                $isTodya = '';
                if($v['otime']>$today && $v['otime']<$tomorrow){//今天新服
                    $isTodya = "<b style='color:red;'>今</b>";
                }elseif($v['otime']>$yesterday && $v['otime']<$today){
                    $isTodya = "<b style='color:#ef6565;'>昨</b>";
                }elseif($v['otime']>$tomorrow && $v['otime']<$postnatal){
                    $isTodya = "<b style='color:#efc2c2;'>明</b>";
                }

                $main = '';
                $hefu = '';
                if($v['main_id'] >0 && $v['srv_num'] > 0){
                    $hefu = "[合{$v['main_id']}]";
                    $main = $v['main_id'];
                }
                if($v['main_id'] == 0 && $v['srv_num'] > 0){
                    $hefu = '[合]';
                }

                $html .= "<button data-main='{$main}' data-sid='{$v['id']}' type='button' class='js-sid btn btn-default btn-xs' style='background-color: {$color};float:left;margin-right:10px;margin-bottom: 4px;'>{$v['id']} {$v['description']} {$v['sdb']}  {$hefu}  {$isTodya}</button>";
            }
        }
      
        if($html){
            echo $html;exit;
        }
        echo '结果为空';
    }

    public function titleReturn(){
        
        $name = $_REQUEST['keyword'];
        
        //超级管理员，返回所有菜单
        if ($_SESSION['super'] == 'YES') {
            $where = "";
        }else{
            $group_row = $admindb->fetchOne("SELECT menuids FROM admingroup WHERE gid={$gid}");
            $where = " and kid IN({$group_row['menuids']}) ";
        }

        $sql = "select kid,filename,name from adminkind where pid!=0 and name like '%$name%' and `show`='YES' $where ";
        $res = $this->admindb->fetchRow($sql);
        $data = array();    
        //正常栏目
        if(count($res)>0){
            foreach ($res as $key => $val) {
                $data[] = array(
                        'value'=>$val['kid'],
                        'name'=>$val['name'],
                        );
                if($val['name'] == '日志列表'){
                    $logID = $val['kid'];
                }
            }
        }
        //日志
        $sql = "select id,title from base_log where title like '%$name%' ";
        $res = $this->admindb->fetchRow($sql);
        if(count($res)>0){
            foreach ($res as $key => $val) {
                $data[] = array(
                        'value'=>'8888'.$val['id'],
                        'name'=>$val['title'],
                        );
            }
        }

        echo json_encode(array('code'=>0, 'msg'=>'success', 'data'=>$data));
    }

    /**
     * 后台切换数据库
     *
     * @return bool
     */
    public function ajax_toggle_admin()
    {
        $sid = 0;
        if (isset($_GET['sid']) && !empty($_GET['sid'])) {
            $sid = intval($_GET['sid']);
        }

        if (!empty($sid)) {
            $sql = "select `gid`,`sdb`,`description`,`otime`,`db_host` as `host`,`db_user` as `user`,`db_pwd` as `pwd`,`db_port` as `port` from `adminserverlist` where id = {$sid} limit 1";
            $toggle_db_val = $this->admindb->fetchOne($sql);
            if (empty($toggle_db_val)) {
                exit(json_encode(array('flag' => 0, 'msg' => 'Notice: The server no exist!')));
            } else {
                if ($_SESSION['super'] != 'YES' && $_SESSION['gids'] && !in_array($toggle_db_val['gid'], $_SESSION['gids'])) {
                    exit(json_encode(array('flag' => 0, 'msg' => 'Notice: Permission denied to the server')));
                }
            }
            //切换数据库
            $_SESSION['selected_sid'] = $sid;//增加全局的sid   cyh添加
            $_SESSION['admin_db_link'] = $toggle_db_val['sdb'];

            //检查数据库连接
            $check = telnet($toggle_db_val['host'],$toggle_db_val['port']);
            if(!$check){
                $flag = 1;
                $alert = '';
            }else{
                $check = Config::mysql_line_check($toggle_db_val);
                if (true === $check) {
                    $flag = 1;
                    $alert = '';
                } else {
                    $alert = 'Notice: Can not connect to db of ' . $toggle_db_val['sdb'] . ",Please choose other or perfect the Configure first!";
                    $flag = 2;
                }
            }

            $game_title = isset($toggle_db_val['description']) ? $toggle_db_val['description'] : 0;
            if (!empty($_SESSION['toggle_admin_return_url'])) {
                $showmsg = array('flag' => $flag, 'msg' => $_SESSION['toggle_admin_return_url'], 'page_title' => $game_title, 'otime' => date('Y-m-d H:i:s', $toggle_db_val['otime']), 'alert' => $alert);
                exit(json_encode($showmsg));
            } else {
                $showmsg = array('flag' => $flag, 'msg' => 'default.php', 'page_title' => $game_title, 'alert' => $alert);
                exit(json_encode($showmsg));
            }

        } else {
            $showmsg = array('flag' => 0, 'msg' => '无有效的服务器列表');
            exit(json_encode($showmsg));
        }
    }

    // 跨服中心查询
    public function kf_sql()
    {
        global $system_config;
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
        $list = $HRpc_client->getServerByPid($system_config['my_pid']);
        $assign['kf_list'] = $list;
        $this->display('kf_sql.shtml', $assign);
    }

    // 跨服中心查询 -- 执行sql
    public function kf_sql_action()
    {
        ini_set('memory_limit', '2048M');
        $useDb = $_REQUEST['use_db'];
        $output = $_REQUEST['output'];
        $sql = stripcslashes($_POST['sql']);
        $sql = str_replace(array('insert', 'delete', 'update', 'replace', 'into'), array('', '', '', '', ''), $sql);
        if (!strpos($sql, 'limit') && !strpos($sql, 'LIMIT')) {
            $sql .= " ;";
        }
        $db = Config::kfdb($useDb);
       
        $rows = $db->fetchRow($sql);
        if (!empty($rows)) {
            switch ($output) {
                case "txt":
                    $data = "";
                    $line1 = array_keys($rows[0]);
                    $data .= implode("\t", $line1);
                    $data .= "\r\n";
                    foreach ($rows as $key => $val) {
                        $line = array();
                        foreach ($val as $k => $v) {
                            $line[] = $v;
                        }
                        $data .= implode("\t", $line);
                        $data .= "\r\n";
                    }
                    header("content-type:application/octet-stream");
                    header('Content-Disposition: attachment; filename=' . uniqid() . '.txt');
                    header("Content-Transfer-Encoding: binary");
                    echo $data;
                    exit;
                default:
                    $table = "<table border=1>";
                    $table .= "<tr>";
                    foreach ($rows[0] as $rk => $rv) {
                        $table .= "<td>$rk</td>";
                    }
                    $table .= "</tr>";
                    foreach ($rows as $key => $val) {
                        $table .= "<tr>";
                        foreach ($val as $k => $v) {
                            $table .= "<td>$v</td>";
                        }
                        $table .= "</tr>";
                    }
                    $table .= "</table>";
                    echo $table;
                    echo "<br>";
                    echo "<a href='javascript:;' onclick='history.back();'>返回</a>";
            }
        } else {
            echo '查询不到结果&nbsp;';
            echo "<a href='javascript:;' onclick='history.back();'>返回</a>";
        }
    }

    // 秘籍 -- 显示页面
    public function sql()
    {
        $this->display('sql.shtml', $assign);
    }

    // 秘籍 -- 执行sql
    public function sql_action()
    {
        ini_set('memory_limit', '2048M');
        $useDb = $_REQUEST['use_db'];
        $output = $_REQUEST['output'];
        $sql = stripcslashes($_POST['sql']);
        $sql = str_replace(array('insert', 'delete', 'update', 'replace', 'into'), array('', '', '', '', ''), $sql);
        if (!strpos($sql, 'limit') && !strpos($sql, 'LIMIT')) {
            // $sql .= " limit 0,100;";
            $sql .= " ;";
        }
        $rows = array();
        if ($useDb == "admin") {
            $db = Ext_Mysql::getInstance('admin');
        } elseif ($useDb == "allcenter") {
            $db = Ext_Mysql::getInstance('allcenter');
        } else {
            $db = $this->db;
        }

        $rows = $db->fetchRow($sql);

        if (!empty($rows)) {
            switch ($output) {
                case "txt":
                    $i = 1;
                    $data = "";
                    $line1 = array_keys($rows[0]);
                    $data .= implode("\t", $line1);
                    $data .= "\r\n";
                    foreach ($rows as $key => $val) {
                        $line = array();
                        foreach ($val as $k => $v) {
                            $line[] = $v;
                        }
                        $data .= implode("\t", $line);
                        $data .= "\r\n";
                    }
                    header("content-type:application/octet-stream");
                    header('Content-Disposition: attachment; filename=' . uniqid() . '.txt');
                    header("Content-Transfer-Encoding: binary");
                    echo $data;
                    exit;
                default:
                    $i = 1;
                    $table = "<table border=1>";
                    $table .= "<tr>";
                    foreach ($rows[0] as $rk => $rv) {
                        $table .= "<td>$rk</td>";
                    }
                    $table .= "</tr>";
                    foreach ($rows as $key => $val) {
                        $table .= "<tr>";
                        foreach ($val as $k => $v) {
                            $table .= "<td>$v</td>";
                        }
                        $table .= "</tr>";
                    }
                    $table .= "</table>";
                    echo $table;
                    echo "<br>";
                    echo "<a href='javascript:;' onclick='history.back();'>返回</a>";
            }
        } else {
            echo '查询不到结果&nbsp;';
            echo "<a href='javascript:;' onclick='history.back();'>返回</a>";
        }
    }

    /**
     * 获取游戏基础数据(通过js调用)
     */

    public function js_get_base_config()
    {
        $url = request('url', 'str');
        $type = request('type', 'int');

        $encrypt_list = array(
            'type' => $type
        );
        $ticket = $this->encrypt($encrypt_list);
        $url .= "?type=$type&ticket=$ticket";
        $respond = Http::httpGet($url);
        $data = json_decode($respond, true);

        if ($data['info'] != 1) {
            echo urldecode(json_encode(array('info' => '-2', 'data' => urlencode($data['data']))));
            exit;
        }

        $code = 0;
        switch ($type) {
            case 1:
                $res = $data['data'];
             
                $item_type = $res[0]['item_type'];

                $attr = addslashes(json_encode($res[0]['attr']));
                
                $sql = "replace into base_config(`id`,`type`,`item`,`item_type`,`attr`) values (" . $res[0]['id'] . ",$type,'" . $res[0]['name'] . "',$item_type, '$attr')";
            // print_r($sql);exit;
                foreach ($res as $key => $value) {
                    $item_type = $value['item_type'];
                    $attr = addslashes(json_encode($value['attr']));
                    
                    $sql .= ",(" . $value['id'] . ",$type,'" . $value['name'] . "',$item_type, '$attr')";
                }
                $code = $this->admindb->query($sql);
                break;
            case 2:
            case 3:
            case 4:
            case 6:
                $res = $data['data'];
                $save = array();
                $this->admindb->query("delete from base_config where `type` = {$type}");
                $sql = "replace into base_config(`id`,`type`,`item`,`item_type`,`attr`) VALUES";
                if ($res) {
                    foreach ($res as $v) {
                        //$v['attr'] = json_encode($v['attr'],JSON_UNESCAPED_UNICODE);// php5.3不支持
                        $v['attr'] = is_array($v['attr'])?json_encode($v['attr']):$v['attr'];
                        $save[] = "('{$v['id']}','{$type}','{$v['item']}','{$v['item_type']}','{$v['attr']}')";
                    }
                    $sql .= implode(',', $save);
                    $code = $this->admindb->query($sql);
                }
                break;
            case 5:
                $res = $data['data'];
                $save = array();
                $this->admindb->query("delete from base_config where `type` = {$type}");
                $sql = "replace into base_config(`id`,`type`,`item`,`item_type`,`attr`) VALUES";
                if ($res) {
                    foreach ($res as $v) {
                        foreach ($v['attr'] as &$in) {
                            $in['desc'] = urlencode($in['desc']);
                        }
                        $v['attr'] = json_encode($v['attr']);// php5.3不支持
                        // $v['attr'] = is_array($v['attr'])?json_encode($v['attr']):$v['attr'];
                        $save[] = "('{$v['id']}','{$type}','{$v['item']}','{$v['item_type']}','{$v['attr']}')";
                    }
                // print_r($save);exit;    
                    $sql .= implode(',', $save);
                    $code = $this->admindb->query($sql);
                }
                break;    
            case 7:
                $res = $data['data'];
              
                $save = array();
                $sql = "replace into base_log(`id`,`title`,`pid`,`sql`,`searchList`,`replaceList`,`order`,`page`) VALUES";
                if ($res) {
                    foreach ($res as $v) {
                        $v['sql'] = addslashes($v['sql']);
                        $save[] = "('{$v['id']}','{$v['title']}','{$v['pid']}','{$v['sql']}','{$v['searchList']}','{$v['replaceList']}','{$v['order']}','{$v['page']}')";
                    }
                    $sql .= implode(',', $save);
                   // print_r($sql);exit;
                    $code = $this->admindb->query($sql);
                }
                $cache = Ext_Memcached::getInstance('user');
                $cache->delete('base_log_list');
                $cache->delete('logTableList');
                break;
 
            case 8:
                $res = $data['data'];
              
                $save = array();
                $sql = "replace into base_gm_list(`id`,`type`,`gm_name`,`gm`,`run_type`,`module`,`fun`,`input_val`,`remark`) VALUES";
                if ($res) {
                    foreach ($res as $v) {
                        $v['sql'] = addslashes($v['sql']);
                        $save[] = "('{$v['id']}','{$v['type']}','{$v['gm_name']}','{$v['gm']}','{$v['run_type']}','{$v['module']}','{$v['fun']}','{$v['input_val']}','{$v['remark']}')";
                    }
                    $sql .= implode(',', $save);
                    // print_r($sql);exit;
                    $code = $this->admindb->query($sql);
                }
                $cache = Ext_Memcached::getInstance('user');
                $cache->delete('id_gm_list');
                $cache->delete('gm_gm_list');
                break;
            case 14:
                $res = $data['data'];
              
                $save = array();
                $sql = "replace into adminkind(`kid`, `name`, `pid`, `filename`, `show`, `ctime`, `mtime`, `ksort`, `system`, `kr`, `vn`, `thai`) VALUES";
                if ($res) {
                    foreach ($res as $v) {
                        $save[] = "('{$v['kid']}','{$v['name']}','{$v['pid']}','{$v['filename']}','{$v['show']}','{$v['ctime']}','{$v['mtime']}','{$v['ksort']}','{$v['system']}','{$v['kr']}','{$v['vn']}','{$v['thai']}')";
                    }
                    $sql .= implode(',', $save);
                    $code = $this->admindb->query($sql);
                }
                if ($_SESSION['super'] == 'YES') {
                    $mc_key = Key::$gm_m['menu_all'];
                } else {
                    $mc_key = sprintf(Key::$gm_m['menu_gid'], $_SESSION['gid']);
                }
                $mc = Ext_Memcached::getInstance('user');
                $mc->delete($mc_key);
                break;
     
            case 9:
            case 10:
            case 11:
            case 12:
                $res = $data['data'];
                $res[0]['item_type'] = $res[0]['item_type'] ? $res[0]['item_type'] : 0;
                $sql = "replace into base_config(`id`,`type`,`item`,`item_type`) values (" . $res[0]['id'] . ",$type,'" . $res[0]['name'] . "'," . $res[0]['item_type'] . ")";
                foreach ($res as $key => $value) {
                    $item_type = $value['item_type'] ? $value['item_type'] : 0;
                    $sql .= ",(" . $value['id'] . ",$type,'" . $value['name'] . "',$item_type)";
                }
                $code = $this->admindb->query($sql);
                break;
            default:
                $res = $data['data'];
                $res[0]['item_type'] = $res[0]['item_type'] ? $res[0]['item_type'] : 0;
                $sql = "replace into base_config(`id`,`type`,`item`,`item_type`) values (" . $res[0]['id'] . ",$type,'" . $res[0]['name'] . "'," . $res[0]['item_type'] . ")";
                foreach ($res as $key => $value) {
                    $item_type = $value['item_type'] ? $value['item_type'] : 0;
                    $sql .= ",(" . $value['id'] . ",$type,'" . $value['name'] . "',$item_type)";
                }
                // print_r($sql);exit;
                $code = $this->admindb->query($sql);    
        
        }
        if ($code >= 1) {
            echo urldecode(json_encode(array('info' => '1', 'data' => urlencode('获取成功'))));
            exit;
        } else {
            echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('获取失败'))));
            exit;
        }

    }

    /**
     * 游戏基础数据
     */
    public function base_config()
    {

        $type = request('type', 'int');
        $type = $type ? $type : 1;
        $id = request('id', 'int');
        $name = request('name', 'str');

        $cur_page = empty($_GET['page']) ? 1 : request('page', 'int');

        $where = "where 1";
        if ($type) {
            $where .= " and type = $type";
        }
        if ($id) {
            $where .= " and id = $id";
        }
        if ($name) {
            $where .= " and item = '$name'";
        }
        $limit = "limit ";
        $per_page = 50;
        if ($cur_page) {
            $startRow = ($cur_page - 1) * $per_page;
            $limit .= "$startRow,$per_page";
        } else {
            $limit .= "0,50";
        }

        $sql = "select id,item from base_config $where order by id $limit";
        $res = $this->admindb->fetchRow($sql);
        $assign['list'] = $res;
        $assign['type_record'] = $this->base_config_list;

        $sql = "select count(*) as total from base_config $where";
        $total_record = $this->admindb->fetchOne($sql);
        $total = $total_record['total'];
        $total_page = ceil($total / $per_page);

        $url = "action_gateway.php?ctl=system&act=base_config&type=$type&id=$id&name=$name";

        $this->smarty->assign(
            array(
                "pages" => array(
                    "curpage" => $cur_page,
                    "totalpage" => $total_page,
                    "totalnumber" => $total,
                    "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
                )
            )
        );
        $this->display("get_base_config.shtml", $assign);
    }

    public function system_config()
    {

        $sql = "select * from system_config";
        $res = $this->admindb->fetchRow($sql);
        $assign['list'] = $res;
        $this->display("system_config.shtml", $assign);
    }

    public function update_system_config()
    {

        $value = request('conf_value', 'str');
        $name = request('conf_name', 'str');
        $desc = request('conf_desc', 'str');

        if (empty($value) || empty($name) || empty($desc)) {
            alert('参数有误');
            exit;
        }

        $value = htmlspecialchars($value);
        $value = addslashes($value);
        $name = htmlspecialchars($name);
        $name = addslashes($name);
        $desc = htmlspecialchars($desc);
        $desc = addslashes($desc);

        $sql = "update system_config set value = '$value',`desc` = '$desc' where name = '$name'";
        $code = $this->admindb->query($sql);
        if ($code >= 1) {
            alert('修改成功');
        } else {
            alert('修改失败');
        }

    }

    public function js_add_system_config()
    {

        $value = request('conf_value', 'str');
        $name = request('conf_name', 'str');
        $desc = request('conf_desc', 'str');

        if (empty($value) || empty($name) || empty($desc)) {
            echo urldecode(json_encode(array('info' => '-6', 'data' => urlencode('参数有误'))));
            exit;
        }

        $value = htmlspecialchars($value);
        $value = addslashes($value);
        $name = htmlspecialchars($name);
        $name = addslashes($name);
        $desc = htmlspecialchars($desc);
        $desc = addslashes($desc);

        $sql = "insert ignore into system_config value ('$name','$value','$desc')";
        $code = $this->admindb->query($sql);
        if ($code >= 1) {
            echo urldecode(json_encode(array('info' => '1', 'data' => urlencode('增加成功'))));
        } else {
            echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('增加失败'))));
        }

    }

    public function js_del_system_config()
    {

        $name = request('conf_name', 'str');

        if (empty($name)) {
            echo urldecode(json_encode(array('info' => '-6', 'data' => urlencode('参数有误'))));
            exit;
        }
        $sql = "delete from system_config where name = '$name'";
        $code = $this->admindb->query($sql);
        if ($code >= 1) {
            echo urldecode(json_encode(array('info' => '1', 'data' => urlencode('刪除成功'))));
        } else {
            echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('刪除失败'))));
        }
    }

    function encrypt($data)
    {
        $encrypt_key = "fynemo123456";
        ksort($data);
        $ticket_str = '';
        foreach ($data as $key => $value) {
            $ticket_str .= '&' . $key . '=' . $value;
        }
        $ticket = md5($ticket_str . $encrypt_key);
        return $ticket;
    }

    /**
     * 缓存管理
     */
    public function mc_manage()
    {
        $assign = array();
        $this->display('mc_manage.shtml', $assign);
    }

    public function mc_manage_act()
    {
        $key = request('key');
        if (empty($key)) {
            alert('非法请求!');
        }
        switch ($key) {
            case 'all':
                if ($_SESSION['super'] != 'YES') {
                    alert('你没权限操作!');
                }
                $mc = Ext_Memcached::getInstance('user');
                $mc->flush();
                $mc = Ext_Memcached::getInstance('api');
                $mc->flush();
                alert('ok!');
                break;
            case 'server_list':
                Auth::check_action(Auth::$CACHE_API);
                $accname = request('accname', 'str');
                if (!$accname) {
                    alert('缺少帐号!');
                }
                $mc_key = sprintf(Key::$api['notice'], $accname);
                $mc = Ext_Memcached::getInstance('api');
                $mc->delete($mc_key);
                alert('ok!');
                break;
            case 'notice':
                Auth::check_action(Auth::$CACHE_API);
                $channel = request('channel', 'str');
                if (!$channel) {
                    alert('缺少渠道号!');
                }
                $mc_key = sprintf(Key::$api['notice'], $channel);
                $mc = Ext_Memcached::getInstance('api');
                $mc->delete($mc_key);
                alert('ok!');
                break;
            case 'menu':
                Auth::check_action(Auth::$CACHE_GM);
                if ($_SESSION['super'] == 'YES') {
                    $mc_key = Key::$gm_m['menu_all'];
                } else {
                    $mc_key = sprintf(Key::$gm_m['menu_gid'], $_SESSION['gid']);
                }
                $mc = Ext_Memcached::getInstance('user');
                $mc->delete($mc_key);
                alert('ok!');
                break;
            case 'rights':
                Auth::check_action(Auth::$CACHE_GM);
                if ($_SESSION['super'] == 'YES') {
                    alert('ok!');
                } else {
                    $mc_key = sprintf(Key::$gm_m['rights_gid'], $_SESSION['gid']);
                    $mc = Ext_Memcached::getInstance('user');
                    $mc->delete($mc_key);
                    alert('ok!');
                }
                break;
        }
    }


    //定时热更--服务端
    public function hot_update_erl_list()
    {
        $password = "20180101";
        $secret = $_GET['secret'];
        if($secret != md5($password)) {
            echo "<script>
			var memo = prompt('请输入确认密码');
			if(memo === null || !memo || memo == undefined){
				alert('答案为空');

			}else{
				if(memo == '".$password."'){
						location.href='action_gateway.php?ctl=system&act=hot_update_erl_list&secret=".md5($password)."';
				}else{
					alert('答案错误');
				}
			}
			</script>";
            exit;
        }
        $start_time = request('start_time', 'str');
        $end_time = request('end_time', 'str');

        $url = "?ctl=system&act=hot_update_erl_list";
        $where = " WHERE 1 ";

        if (!empty($start_time)) {
            $starttime = strtotime($start_time);
            $where .= " AND runtime >= '$starttime' ";
            $url .= "&start_time=$start_time";
        }
        if (!empty($end_time)) {
            $endtime = strtotime($end_time . "23:59:59");
            $where .= " AND runtime <= '$endtime' ";
            $url .= "&end_time=$end_time";
        }

        $total_record = $this->admindb->fetchOne("SELECT COUNT(*) as total FROM hot_update_erl_list $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by id desc ";

        $sql = "select * from hot_update_erl_list $where $orderby $limit";
        $list = $this->admindb->fetchRow($sql);
        if (!empty($list)) {
            foreach ($list as $key => $val) {
                $list[$key]['content'] = str_replace(' ', '<br>', $val['content']);
            }
        }
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );
        $assign['filearr'] = array();
        $this->display('hot_update_erl_list.shtml', $assign);
    }

    //定时热更--服务端 修改和插入
    public function add_hot_update_erl()
    {
        $id = request('id', 'int');
        $file = request('file', 'arr');
        if (empty($file)) {
            alert('热更文件为空');
        }
        if (count($file) > 5) {
            alert('热更文件数量大于5');
        }
        $content = join(' ', $file);
        $runtime = time();
        $sql = "select finishtime from hot_update_erl_list order by runtime desc limit 1";
        $rinfo = $this->admindb->fetchOne($sql);
        if ($runtime - $rinfo['finishtime'] < 300) {
            alert("热更时间距离最大更新时间间隔不能小于5分钟");
        }
        if (!empty($id)) {
            $sql = "update hot_update_erl_list set content='$content',runtime='$runtime' where id='$id'";
        } else {
            $sql = "insert into hot_update_erl_list(content,runtime)values('$content','$runtime')";
        }
        $this->admindb->query($sql);
        if ($this->admindb->getAffectRows()) {
            alert("操作成功");
        }
    }

    //定时热更--服务端 删除热更计划
    public function del_hot_update_erl()
    {
        $id = request('id', 'int');
        $this->admindb->query("delete from hot_update_erl_list where id = '$id'");
        if ($this->admindb->getAffectRows()) {
            alert("删除成功");
        }
        alert("删除失败");
    }

    //定时热更--服务端 获取实时热更日志
    public function get_hot_update_erl_log()
    {

        $start_line = request('start_line', 'int');
        $erl_compile = ROOT_PATH . 'log/hot_erl_all_server.txt';
        if (is_file($erl_compile)) {
            $arr = getFileLines($erl_compile, $start_line, 1000);
            $line = $arr['line'];
            $serverlog = $arr['content'];

        }
        echo json_encode(array('serverlog' => nl2br($serverlog), 'line' => $line));
        exit;
    }

    //定时热更--服务端 开始热更
    public function start_hot_update_erl()
    {
        $id = request('id', 'int');
        if (empty($id)) {
            echo 'ID为空';
            exit;
        }
        $time = time();
        $sql = "select * from hot_update_erl_list where id='$id'";
        $info = $this->admindb->fetchOne($sql);
        if (empty($info['content'])) {
            echo '更新内容不能为空';
            exit;
        }

        system('echo "正在等待热更，1分钟内将执行，请稍等..." >' . ROOT_PATH . 'log/hot_erl_all_server.txt', $ret);
        system("echo 1 >" . ROOT_PATH . "log/hot_erl_all_server_flag.txt", $ret);
        system("echo " . $info['content'] . " >" . ROOT_PATH . "log/hot_erl_all_server_content.txt", $ret);


        $this->admindb->query("update hot_update_erl_list set finishtime = '$time' where id='{$info['id']}'");
        echo '热更中，5分钟后才能进行下一次热更';
        exit;
    }

    //定时热更--客户端
    public function hot_update_client_list()
    {
        $password = "20180101";
        $secret = $_GET['secret'];
        if($secret != md5($password)) {
            echo "<script>
			var memo = prompt('请输入确认密码');
			if(memo === null || !memo || memo == undefined){
				alert('答案为空');

			}else{
				if(memo == '".$password."'){
						location.href='action_gateway.php?ctl=system&act=hot_update_client_list&secret=".md5($password)."';
				}else{
					alert('答案错误');
				}
			}
			</script>";
            exit;
        }

        $url = "?ctl=system&act=hot_update_client_list";
        $where = " WHERE 1 ";

        $total_record = $this->admindb->fetchOne("SELECT COUNT(*) as total FROM hot_update_client_list $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($_REQUEST['page']) ? (int)$_REQUEST['page'] : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by id desc ";

        $sql = "select * from hot_update_client_list $where $orderby $limit";
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );

        $this->display('hot_update_client_list.shtml', $assign);
    }

    //定时热更--客户端 修改和插入
    public function add_host_update_client()
    {
        $id = request('id', 'int');
        $phone = request('phone', 'str');
        $runtime = strtotime(request('runtime', 'str'));
        $time = time();
        if (empty($runtime)) {
            alert('热更时间不能为空');
        }
        if (!empty($id)) {
            $sql = "update hot_update_client_list set runtime = '$runtime',phone='$phone' where id='$id'";
        } else {
            $sql = "insert into hot_update_client_list(addtime,runtime,phone)values('$time','$runtime','$phone')";
        }
        $this->admindb->query($sql);
        if ($this->admindb->getAffectRows()) {
            alert("操作成功");
        }
    }

    //定时热更--客户端 删除热更计划
    public function del_hot_update_client()
    {
        $id = request('id', 'int');
        $this->admindb->query("delete from hot_update_client_list where id = '$id'");
        if ($this->admindb->getAffectRows()) {
            alert("删除成功");
        }
        alert("删除失败");
    }

    //定时热更--客户端 开始热更
    public function start_hot_update_client()
    {

        $id = request('id', 'int');
        if (empty($id)) {
            echo 'ID为空';
            exit;
        }

        $sql = "select * from hot_update_client_list where id='$id'";
        $info = $this->admindb->fetchOne($sql);
        if (empty($info)) {
            echo '更新内容不能为空';
            exit;
        }
        if ($info['phone'] != 'ios') {//不等于苹果
            system('echo "正在等待热更，1分钟内将执行，请稍等..." >' . ROOT_PATH . 'log/hot_client_all_server_android.txt', $ret);
            system("echo 1 >" . ROOT_PATH . "log/hot_client_all_server_flag_android.txt", $ret);
        } else {
            system('echo "正在等待热更，1分钟内将执行，请稍等..." >' . ROOT_PATH . 'log/hot_client_all_server_ios.txt', $ret);
            system("echo 1 >" . ROOT_PATH . "log/hot_client_all_server_flag_ios.txt", $ret);
        }
        $time = time();
        $this->admindb->query("update hot_update_client_list set finishtime = '$time' where id='{$id}'");
        echo '热更中，5分钟后再进行下一次热更';
        exit;
    }

    //定时热更--客户端 获取实时热更日志
    public function get_hot_update_client_log()
    {
        $start_line = request('start_line1', 'int');
        $start_line2 = request('start_line2', 'int');

        //client编译标识文件 和 编译日志文件
        $client_compile_android = ROOT_PATH . 'log/hot_client_all_server_android.txt';
        $client_compile_ios = ROOT_PATH . 'log/hot_client_all_server_ios.txt';

        // 安卓客户端
        if (is_file($client_compile_android)) {
            $arr = getFileLines($client_compile_android, $start_line, 1000);
            $android_line = $arr['line'];
            $clientlog_android = $arr['content'];
        }

        // iOS客户端
        if (is_file($client_compile_ios)) {
            $arr = getFileLines($client_compile_ios, $start_line2, 1000);
            $ios_line = $arr['line'];
            $clientlog_ios = $arr['content'];
        }
        echo json_encode(array('clientlog_android' => nl2br($clientlog_android), 'android_line' => $android_line, 'clientlog_ios' => nl2br($clientlog_ios), 'ios_line' => $ios_line));
        exit;
    }

    //定时停更--测试服
    public function update_test_server()
    {
        $password = "20180101";
        $secret = $_GET['secret'];
        if($secret != md5($password)) {
            echo "<script>
			var memo = prompt('请输入确认密码');
			if(memo === null || !memo || memo == undefined){
				alert('答案为空');

			}else{
				if(memo == '".$password."'){
						location.href='action_gateway.php?ctl=system&act=update_test_server&secret=".md5($password)."';
				}else{
					alert('答案错误');
				}
			}
			</script>";
            exit;
        }
        $cur_page = request('page', 'int');

        $url = "?ctl=system&act=update_test_server";
        $where = " WHERE 1 ";

        $total_record = $this->admindb->fetchOne("SELECT COUNT(*) as total FROM update_test_server $where");
        $total_record = $total_record['total'];

        $per_page = 30;
        $total_page = ceil($total_record / $per_page);
        $cur_page = isset($cur_page) ? $cur_page : 1;
        $cur_page = $cur_page < 1 ? 1 : $cur_page;
        $limit_start = ($cur_page - 1) * $per_page;
        $limit = " LIMIT {$limit_start}, $per_page ";
        $orderby = " order by id desc ";

        $sql = "select * from update_test_server $where $orderby $limit";
        $list = $this->admindb->fetchRow($sql);
        $assign['list'] = $list;
        $assign['pages'] = array(
            "curpage" => $cur_page,
            "totalpage" => $total_page,
            "totalnumber" => $total_record,
            "url" => (strpos($url, "?") === false) ? ($url . "?page=") : (rtrim($url, "&") . "&page="),
        );

        $this->display('update_test_server.shtml', $assign);
    }

    //定时停更--测试服 修改和插入
    public function add_update_test_server()
    {

        $id = request('id', 'int');
        $runtime = request('runtime', 'str');
        $time = time();
        if (empty($runtime)) {
            alert('停更时间不能为空');
        }
        $runtime = strtotime($runtime);
        if (!empty($id)) {
            $sql = "update update_test_server set runtime = '$runtime' where id='$id'";
        } else {
            $sql = "insert into update_test_server(addtime,runtime) values('$time','$runtime')";
        }
        $this->admindb->query($sql);
        if ($this->admindb->getAffectRows()) {
            alert("操作成功");
        }
    }

    //定时停更--测试服 删除热更计划
    public function del_update_test_server()
    {
        $id = request('id', 'int');
        $this->admindb->query("delete from update_test_server where id = '$id'");
        if ($this->admindb->getAffectRows()) {
            alert("删除成功");
        }
        alert("删除失败");
    }

    //定时停更--测试服 开始
    public function start_update_test_server()
    {
        $id = request('id', 'int');
        if (empty($id)) {
            echo 'ID为空';
            exit;
        }
        system('echo "正在等待停更，1分钟内将执行，请稍等..." >' . ROOT_PATH . 'log/update_test_server.txt', $retval);
        system('echo 1 >' . ROOT_PATH . 'log/update_test_server_flag.txt', $retval);
        $time = time();
        $this->admindb->query("update update_test_server set finishtime = '$time' where id='{$id}'");
        echo '执行成功，正在准备更新测试服';
        exit;
    }

    //定时停更--测试服 获取实时数据
    public function get_update_test_server_log()
    {

        $start_line = request('start_line', 'int');
        //erl编译标识文件 和 编译日志文件
        $erl_compile = ROOT_PATH . 'log/update_test_server.txt';

        if (is_file($erl_compile)) {
            $arr = getFileLines($erl_compile, $start_line, 1000);
            $line = $arr['line'];
            $serverlog = $arr['content'];
        }
        echo json_encode(array('serverlog' => nl2br($serverlog), 'line' => $line));
        exit;
    }


    /**
     * 接口调试
     */
    public function api_show()
    {
        $api = trim(request('api'));
        $ps = $_POST;
        unset($ps['api']);
        if ($ps && $api) {
            include ADMIN_PATH . 'wx/config.php';
            $ps['time'] = time();
            $ps['ticket'] = w_encrypt($ps);
            $query = urldecode(http_build_query($ps));
            $api = 'http://' . $_SERVER['SERVER_NAME'] . '/wx/' . $api . '.php?' . $query;
            $result = Http::httpGet($api);
            exit(json_encode(array('api' => $api, 'result' => $result)));
        }
        $this->display('api_show.shtml', array());
    }

    /**
     * check api
     */
    public function check_api()
    {
        $api = request('api');
        switch ($api) {
            case 'api_active':
                exit('channel=');
                break;
            case 'api_server';
                exit('channel=&accname=&device_id=&ios_version=-1');
                break;
            case 'api_order';
                exit('sid=&role_id=&accname=&channel=&money=&recharge_id=&product_id=-1');
                break;
            case 'api_verify';
                exit('token=&game_id=&channel=');
                break;
            case 'api_card';
                exit('sid=&role_id=&card_no=');
                break;
        }
    }

    public function get_custom_data()
    {   
        $otime = time();
        // Ext_Debug::log($otime);
        $server_sql = "SELECT s.id,platform_name,s.description FROM adminserverlist s  LEFT JOIN adminplatformlist p ON s.gid = p.gid WHERE `default` = 1 AND otime<$otime ORDER BY p.gid ASC, s.id ASC ";
        $data_sql = "select role_id,accname,`name` from role_basic where 1";
        $res = $this->admindb->fetchRow($server_sql);
        $s_list = array();
        foreach ($res as $key => $val) {
            $s_list[$val['platform_name']][$val['id']] = $val['description'];
        }
// print_r($s_list);exit;        
        $assign['s_list'] = $s_list;
        $assign['server_sql'] = $server_sql;
        $assign['data_sql'] = $data_sql;
        $assign['type'] = 2;
        $this->display('get_custom_data.shtml', $assign);
    }

    public function run_custom_data()
    {
        Auth::check_action(Auth::$RUN_CUSTOM_DATA);
        global $server_list_info;

        $time = time();
        $server_sql = $_POST['server_sql'];
        $data_sql = $_POST['data_sql'];
        $server_arr = $_POST['server_check'];
        $ids = trim($_POST['data_ids']);
        $field = $_POST['field'];
        if (empty($server_sql)&&empty($server_arr)) {   //若sql跟列表为空则默认为sql查
            $server_sql = "SELECT s.id,platform_name,s.description FROM adminserverlist s  LEFT JOIN adminplatformlist p ON s.gid = p.gid WHERE `default` = 1 AND otime<$time ORDER BY p.gid ASC, s.id ASC ";
            $assign['type'] = 1;
        }elseif($server_sql&&empty($server_arr)){   //若列表查询为空sql不为空则sql查询
            $s_res = $this->admindb->fetchRow($server_sql);
            $assign['type'] = 1;
        }else{      //若两个值都存在在则默认为列表选择
            $s_res = array();
            $sids= array();
            global $server_list_info;
            foreach($server_arr as $k => $v){
                $server = array();
                $server['id'] = $sids[] = $v;
                $server['platform_name'] = $server_list_info[$v]['gid'];
                $server['description'] = $server_list_info[$v]['description'];
                array_push($sids,$arr[0]);
                array_push($s_res,$server);
            }
            $assign['sids'] = $sids;
            $assign['type'] = 2;
        }

        if (empty($data_sql)) {
            alert('sql不能为空！');
        }
        if($ids&&$field){
        
            if(strpos($ids,',') !== false){
                $arr1 = explode(',',$ids);
            }else{
                $arr1 = explode("\r\n",$ids);
            }
           
            $arr2 = array();
        
            foreach($arr1 as $k => $v){
                $str = "'".$v."'";
                array_push($arr2,$str);
            }
            $sql_where = "and $field in (".implode(',',$arr2).')';
  
            $query_data_sql = str_replace('$data_ids',$sql_where,$data_sql);
        }else{
            $query_data_sql = $data_sql;
        }

        if (count($s_res) > 0) {

            $all_data = array();
            foreach ($s_res as $key => $value) {
                $sid = $value['id'];
                $platform_name = $value['platform_name'];
                $desc = $value['description'];
                $serverInfo = array('平台'=>$platform_name, '服名'=>$desc);
                $res = Config::gamedb($sid)->fetchRow($query_data_sql);

                if (count($res) > 0) {
                    $data = array();
                    foreach ($res as $k => $v) {
                        $data[] = array_merge($serverInfo, $v);
                    }
                    // print_r($data);exit;
                    $all_data = array_merge($all_data,$data);
                }else {       
                    $error = mysql_error();
                    $error = empty($error)?'数据为空！':$error;
                    $assign['error'] = $error;
                }
            }
// print_r($sids);exit;
            if (count($all_data) > 0) {
                expTxt($all_data);
            }
        }else{
            $error = mysql_error($this->admindb->getConn());
            $error = empty($error)?'数据为空！':$error;
            $assign['error'] = $error;
        }
        $all_server_sql = "SELECT s.id,platform_name,s.description FROM adminserverlist s  LEFT JOIN adminplatformlist p ON s.gid = p.gid WHERE `default` = 1 AND otime<$time ORDER BY p.gid ASC, s.id ASC ";
        $res = $this->admindb->fetchRow($all_server_sql);
        $s_list = array();
        foreach ($res as $key => $val) {
            $s_list[$val['platform_name']][$val['id']] = $val['description'];
        }
        $assign['s_list'] = $s_list;
        $assign['server_sql'] = $server_sql;
        $assign['data_sql'] = $data_sql;
        $this->display('get_custom_data.shtml', $assign);
    }

    /**
     * 检测游戏服数据库连接
     */
    public function check_mysql_connect(){
        global $server_list_info;
        $result = array();
        foreach ($server_list_info as $key=>$v) {
            if(in_array($v['default'],array(1,4)) && $v['main_id'] == 0){
                
                $telnet_class = $connect_class = "red";

                $result[$key]['id'] = $v['id'];
                $result[$key]['sdb'] = $v['sdb'];
                $result[$key]['telnet'] = "Telnet fail!";
                $result[$key]['result'] = "Connect fail";
                $result[$key]['color'] = "green";

                $sTime = microtime(true);
                $check = telnet($v['db_host'],$v['db_port']);
                $eTime = microtime(true);
                $telnet_time = $eTime-$sTime;
                $telnet_time = round($telnet_time,3);

                $result[$key]['telnet_time'] = round($telnet_time,3);

                if($check){
                    $telnet_class = 'green';
                    $telnet_result = "Telnet Success, ip:{$v['db_host']} port:{$v['db_port']}, time {$telnet_time} s!";
                    $result[$key]['telnet'] = $telnet_result;
                    $sTime = microtime(true);
                    
                    $check = @Config::mysql_line_check(array('host'=>$v['db_host'],'port'=>$v['db_port'],'pwd'=>$v['db_pwd'],'user'=>$v['db_user'],'sdb'=>$v['sdb']));
                    $eTime = microtime(true);
                    $connect_time = $eTime-$sTime;
                    $connect_time = round($connect_time,3);
                    if(true === $check){
                        $connect_class = 'green';
                        
                        $result[$key]['result'] = "Connect Success, time {$connect_time} s!";
                    }elseif( 0 === $check){
                        $result[$key]['color'] = "red";
                        
                        $result[$key]['result'] = "missing params, plz check!";
                    }elseif( 1 === $check){
                        $result[$key]['color'] = "red";
                        
                        $result[$key]['result'] = "may be mysql pwd error!";
                    }elseif( 2 === $check){
                        $result[$key]['color'] = "red";
                        
                        $result[$key]['result'] = "can not select the db {$v['sdb']}!";
                    }

                }else{
                    $result[$key]['color'] = "red";
                }

            }
        }

        // print_r($result);exit;
        $assign['data'] = json_encode(array_values($result));
        $this->display('wx_info/check_mysql_connect.shtml', $assign);
        
    }

    /**
     * 检测erlang连接
     */
    public function check_erl_connect(){
        global $server_list_info;
        $data = array();
        foreach ($server_list_info as $key=>$v) {
            if(in_array($v['default'],array(1,4)) && $v['main_id'] == 0){
                $data[$key]['id'] = $v['id'];
                $data[$key]['sdb'] = $v['sdb'];
                $data[$key]['telnet'] = telnet($v['db_host'],$v['db_port']);
                $data[$key]['mysql'] = @Config::mysql_line_check(array('host'=>$v['db_host'],'port'=>$v['db_port'],'pwd'=>$v['db_pwd'],'user'=>$v['db_user'],'sdb'=>$v['sdb']));
                if(!is_bool($data[$key]['mysql']) && $data[$key]['mysql']!=true) continue;

                $sql = "select * from base_game";
                $erlinfo = Config::gamedb($v['id'])->fetchRow($sql);
           
                $rows = array();
                foreach ($erlinfo as $val) {
                    if ($val['cf_name'] == 'erl_cookie') {
                        $rows['erl_cookie'] = $val['cf_value'];
                    }
                    if ($val['cf_name'] == 'erl_node') {
                        $rows['erl_node'] = $val['cf_value'];
                    }
                }
                $data[$key]['erl_cookie'] = $rows['erl_cookie'];
                $data[$key]['erl_node'] = $rows['erl_node'];
         
                if(empty($rows['erl_cookie'])||empty($rows['erl_node'])){
                    $data[$key]['msg'] = "node或cookie为空";
                }else{
                    $link = peb_connect($rows['erl_node'], $rows['erl_cookie'], 5000);
                    if($link && is_resource($link)){
                        $data[$key]['msg'] = 'success';
                        peb_close($link);
                    }else{
                        $data[$key]['msg'] = peb_errorno().", errormsg:".peb_error();
                    }

                }
            }
        }

        // print_r($data);exit;
        $assign['data'] = json_encode(array_values($data));
        $this->display('wx_info/check_erl_connect.shtml', $assign);
    }

    /**
     * 检查超级跨服中心数据连接
     */
    public function check_super_kf_center(){
        $serlist = Model::get_kf_server_super_list();
        ob_start();
        foreach ($serlist as $v) {
            $telnet_result = "Telnet fail!";
            $connect_result = "Connect fail";
            $telnet_class = $connect_class = "red";

            $sTime = microtime(true);
            $check = telnet($v['db_host'],$v['db_port']);
            $eTime = microtime(true);
            $telnet_time = $eTime-$sTime;
            $telnet_time = round($telnet_time,3);
            if($check){
                $telnet_class = 'green';
                $telnet_result = "Telnet Success, ip:{$v['db_host']} port:{$v['db_port']}, time {$telnet_time} s!";
                $sTime = microtime(true);
                $check = Config::mysql_line_check(array('host'=>$v['db_host'],'port'=>$v['db_port'],'pwd'=>$v['db_pwd'],'user'=>$v['db_user'],'sdb'=>$v['db_name']));
                $eTime = microtime(true);
                $connect_time = $eTime-$sTime;
                $connect_time = round($connect_time,3);
                if(true === $check){
                    $connect_class = 'green';
                    $connect_result = "Connect Success, time {$connect_time} s!";
                }elseif( 0 === $check){
                    $connect_result .= ", missing params, plz check!";
                }elseif( 1 === $check){
                    $connect_result .= ", may be mysql pwd error!";
                }elseif( 2 === $check){
                    $connect_result .= ", can not select the db {$v['db_name']}!";
                }
            }else{
                $telnet_result .= ", ip:{$v['db_host']} port:{$v['db_port']}!";
            }

            echo "服{$v['id']} <span style='color:{$telnet_class}'>$telnet_result </span> | <span style='color:{$connect_class}'>$connect_result</span><br>";
            ob_flush();
            flush();
        }
    }

    /**
     * 检测erlang连接
     */
    public function check_super_erl_connect(){
        global $system_config;
        $HRpc_client = Ext_HRpc::get_cli_instance($system_config['rpc_kf_server']);
        $re = $HRpc_client->getServerByPid();
        if(!$re){
            exit('获取跨服失败!');
        }
        foreach ($re as $v) {
            $msg = "Server {$v['node']}";
            if(!telnet($v['db_host'],$v['db_port']) || !Config::mysql_line_check(array('host'=>$v['db_host'],'port'=>$v['db_port'],'pwd'=>$v['db_pwd'],'user'=>$v['db_user'],'sdb'=>$v['db_name']))){
                $msg .= " Connect db fail!<br>";
            }else{
                $rows = array();
                $rows['erl_cookie'] = $v['cookie'];
                $rows['erl_node'] = $v['node'];
                $msg .= " Node:{$rows['erl_node']}, Cookie:{$rows['erl_cookie']}";
                if(empty($rows['erl_cookie'])||empty($rows['erl_node'])){
                    $msg .= " missing params node or cookie!<br>";
                }else{
                    $link = peb_connect($rows['erl_node'], $rows['erl_cookie'], 5000);
                    if($link && is_resource($link)){
                        $msg .= "  <span style='color:green'>Success</span><br>";
                        peb_close($link);
                    }else{
                        $msg .= "  <span style='color:red'>errorno: ".peb_errorno().", errormsg:".peb_error()."</span><br>";
                    }
                }
            }
            echo $msg;
        }

    }

    /**
     * 检测游戏服base_kf
     */
    public function check_base_kf(){

        $kf_list = Model::get_kf_server_super_list();
        $data = array();
        if($kf_list){
            foreach ($kf_list as $v) {
                $data[$v['node']] = $v['node'];
            }
        }
        $select = Form::select('kf_center',$data);
        $assign['select_html'] = $select;

        $kfCenter = request('kf_center','str');
        $str = array();
        if($kfCenter){
            $list = Kf::rpc_get_server_base_kf($kfCenter);
            if($list){
                foreach ($list as $v) {
                    if($kfCenter == $v['name']){
                        $msg = "<span style='color:green;'>匹配</span>";
                    }else{
                        $msg = "<span style='color:red;'>不匹配!!!</span>";
                    }
                    $str[] = "服{$v['node_id']} {$v['name']} {$v['cookie']} {$msg}";
                }
            }
        }

        $assign['msg'] = implode('<br>',$str);

        $this->display('kf_check_ser_kf.shtml', $assign);
    }
}