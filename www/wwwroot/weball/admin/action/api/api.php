    <?php
/**
 * author fan
 * LTV 生命周期价值
 */
require_once dirname(__FILE__) . '/base.php';

class api extends base
{
    public function handle()
    {
        exit(Ext_Template::lang('迷路了啊 小伙子@ !!!!'));
    }

    //服务器列表展示
    public function api_server()
    {   
        global $system_config;
        $sql = "select channel_str as channel from channel_list GROUP BY channel_str";
        $res = $this->admindb->fetchRow($sql);
        
        if($system_config['admin_url']){
            $url = $system_config['admin_url'].'vt/api_server.php';  
        }else{
            $url = 'http://'.$_SERVER['HTTP_HOST'].'/vt/api_server.php';
        }

        if($_REQUEST['channel'] && $_REQUEST['accname'] && $_REQUEST['device_id']){
            $encrypt = array();
            $encrypt['channel'] = trim($_REQUEST['channel']);
            $encrypt['accname'] = trim($_REQUEST['accname']);
            $encrypt['device_id'] = trim($_REQUEST['device_id']);
            $encrypt['ios_version'] = trim($_REQUEST['ios_version']);
            $encrypt['time'] = time();
            $encrypt['ticket'] = $this->w_encrypt($encrypt);

            $post = http_build_query($encrypt);
            $url = $url.'?'.$post;
       
            $data = json_decode(file_get_contents($url),true);
        // print_r($data);exit;    
            if(count($data['data']['zone_list'])>0){
                $zone_list = array();
                foreach ($data['data']['zone_list'] as $key => $val) {
                    $zone_list[$val['name']] = $val['list'];
                }
                foreach ($zone_list as $key => $val) {
                    $zone_list[$key] = array_chunk($val, 2);
                }
                $data['data']['zone_list'] = $zone_list;
            }
            
            $serverlist = $data['data']['server_list'];
            //服务器列表处理
            list($server_list, $recom) = $this->serverlist($data['data']['server_list']);
        // print_r($data);exit;    
            //角色列表处理
// print_r($data);exit;          
            $data['data']['person_list'] = $this->person_list($data['data']['person_list'], $serverlist);
        }
        // print_r($data);exit;
        $assign['channel_list'] = $res;
        $assign['url'] = $url;
        $assign['data'] = $data;
        $assign['recom'] = $recom;
        $assign['server_list'] = $server_list;
       
        $this->display("api/api_server.shtml", $assign);
    }

    //角色列表处理
    function person_list($data, $serverlist)
    {
        //角色列表处理
        if(count($data)>0){
            $list = array();
            $time = strtotime(date("Y-m-d"));
            foreach ($data as $key => &$val) {
                
                $last_login_time = $val['last_login_time'] > $time ? "<span style='color:red;'>".date('Y-m-d H:i:s', $val['last_login_time'])."</span>"  : date('Y-m-d H:i:s', $val['last_login_time']);
                $reg_time = $val['reg_time'] > $time ? "<span style='color:red;'>".date('Y-m-d H:i:s', $val['reg_time'])."</span>" : date('Y-m-d H:i:s', $val['reg_time']);

                $list[] = array(
                    'nickname' => $val['nickname'],
                    'role_id' => $val['role_id'],
                    'level' => $val['level'],
                    'last_login_server' => $serverlist[$val['server_id']]['description'],
                    'last_login_time' => $last_login_time,
                    'server_id' => $serverlist[$val['server_id']]['description'],
                    'reg_time' => $reg_time,
                    
                    );
               
            }
        }
        return $list;
    }

    //服务器列表处理
    function serverlist($data)
    {
        //服务器列表处理
        if(count($data)>0){
            $recom = array();
            $time = strtotime(date("Y-m-d"));
            foreach ($data as $key => $val) {
                $color = "color:#3cf33c;";
                if( $val['hot_state'] === '0' ){
                
                    $color = "color:red;";
                }
                if( $val['default'] == 4 ){
                    $color = "color:#FFFF;";
                }

                if( $val['recom_state'] == 1 ){//推荐服
                    $recom[] = $val;
                }
                $data[$key]['otime'] = $val['otime']>$time ? "<span style='color:red;'>".date('Y-m-d H:i:s', $val['otime']).'</span>' : date('Y-m-d H:i:s', $val['otime']);
                $data[$key]['color'] = $color;
            }
            $recom = count($recom)>0 ? reset($recom) : reset($data);

            // $data = array_chunk($data, 2, true);
        }

        return array($data, $recom);
    }

    //加密模拟
    function w_encrypt($data){
        //加密key
        $key = '2c41a3ddd23d1a1f711b7f37df4d515a';
        //去掉ticket参数
        unset($data['ticket']);
        //参数键名按字母表顺序排序
        ksort($data);
        //组装签名串 a=1&b=2.key
        $ticket_str = urldecode(http_build_query($data));
        //md5加密
        $ticket = md5($ticket_str . $key);
        //打印日志
        $msg = 'OK';
        if($ticket != $_REQUEST['ticket']){
            $msg = 'FAIL';
        }
        return $ticket;
    }


    //日志显示
    public function api_log()
    {
        if($_GET['path']) {
            $path = $_GET['path'];
        } else {
            $path = LOG_DIR . 'api';
        }
        
        $list = $this->get_file_list($path);
        $assign['list'] = $list;
        $assign['path'] = $path;
        $this->display("api/api_log.shtml", $assign);
    }

    //删除十天前日志
    public function delete_log()
    {
        $path = LOG_DIR . 'api';
        $this->del_file($path);
        alert(Ext_Template::lang('清理成功！'));
    }


    //日志查看
    public function api_log_show()
    {
        $path = $_GET['path'];
        $content = file_get_contents($path);
        echo "<style>code{font-size: 15px; font-family: initial;}</style>";  
        highlight_string($content);
    }

    //返回文件列表
    public function get_file_list($path)
    {
    	$res = scandir($path);
    	$list = array();
    	$file_array = array();
        $dir_array = array();
        foreach ($res as $key => $val) {
    		if( $val == '.' ) continue;
            $info = $this->get_file_info($path .'/'. $val);

            if( $info['type'] == 'file') {//文件
                $name = $val;
                $url = $path .'/'. $val;
                $button = '查看';
                $method = 'api_log_show';
                $target = '_blank';
            }elseif ( $val == '..' ) {//上一级
                $val = '';
                $name = '上一级';
                $url = dirname($path);
                $button = '上一级';
                $method = 'api_log';
                $target = '';
                $class = 'background-color: red; border: red;';
            } else {//文件夹
                $name = $val;
                $url = $path .'/'. $val;
                $button = '下一级';
                $method = 'api_log';
                $target = '';
                $class = '';
            }
    		
            $info2 = array(
                    'path' => $url,
                    'file' => $val,
                    'name'=> $name, 
                    'button'=> "<a href='/action_gateway.php?ctl=api&act=api&method=$method&path=".$url."' target=$target ><input style='$class' type=button value=$button></a>",
                    );
            if($info['type'] == 'file') {
                $file_array[] = array_merge($info2, $info);
            } else {
                $dir_array[] = array_merge($info2, $info);
            }
    		
    	}
    
    	$list = array_merge($dir_array, $file_array);
    	return $list;
    }

    //获取文件信息
    public function get_file_info($path)
    {
    	$info = array(
    			'size' => filesize($path),
    			'type' => filetype($path),
    			'date' => date('Y-m-d H:i:s', filemtime($path)),
    			'power' => substr(sprintf("%o", fileperms($path)),-4),
    			);
    	return $info;
    }

    public function i_array_column($input, $columnKey, $indexKey=null){
        if(!function_exists('array_column')){
            $columnKeyIsNumber  = (is_numeric($columnKey))?true:false;
            $indexKeyIsNull            = (is_null($indexKey))?true :false;
            $indexKeyIsNumber     = (is_numeric($indexKey))?true:false;
            $result                         = array();
            foreach((array)$input as $key=>$row){
                if($columnKeyIsNumber){
                    $tmp= array_slice($row, $columnKey, 1);
                    $tmp= (is_array($tmp) && !empty($tmp))?current($tmp):null;
                }else{
                    $tmp= isset($row[$columnKey])?$row[$columnKey]:null;
                }
                if(!$indexKeyIsNull){
                    if($indexKeyIsNumber){
                        $key = array_slice($row, $indexKey, 1);
                        $key = (is_array($key) && !empty($key))?current($key):null;
                        $key = is_null($key)?0:$key;
                    }else{
                        $key = isset($row[$indexKey])?$row[$indexKey]:0;
                    }
                }
                $result[$key] = $tmp;
            }
            return $result;
        }else{
            return array_column($input, $columnKey, $indexKey);
        }
    }

    /**
     * 删除目录及目录下所有文件或删除指定文件
     * @param $path   str  待删除目录路径
     * @param $delDir int  是否删除目录，1或true删除目录，0或false则只删除文件保留目录（包含子目录）
     * @return bool 返回删除状态
     */
    public function del_file($path, $delDir = TRUE)
    {
        if (empty($path)) {
            return '删除路径为空';
        }
        if (!file_exists($path)) {
            return $path . ' 不存在';
        }
        $time_10 = strtotime(date("Y-m-d"))-86400*10;

        $handle = opendir($path);
        if ($handle) {
            while (false !== ($item = readdir($handle))) {
                if ($item != "." && $item != ".."){
                    //小于10天前创建，则删除
                    if(filemtime("$path/$item") < $time_10){
                        
                        is_dir("$path/$item") ? $this->del_file("$path/$item", $delDir) : unlink("$path/$item");
                    }
                }
            }
            closedir($handle);
            if ($delDir)
                return rmdir($path);
        } else {
            if (file_exists($path)) {
                if(filemtime($path) < $time_10){
                    
                    return unlink($path);
                }
                
            } else {
                return FALSE;
            }
        }
    }


}




































































































