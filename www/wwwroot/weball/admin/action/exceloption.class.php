<?php
/**
 * Excel 配置生成
 * @author 严利宏
 */

require_once ACTION_GATEWAY_PATH . 'action/superclass.php';
require_once ACTION_GATEWAY_PATH . 'header.php';

class action_exceloption extends action_superclass
{
	// 不进行Excel导表的数据表
	protected $special_table_without_import_db = array(
			'base_log',			// (特殊)日志表
			'base_exceloption',	// (特殊)配置表
			// 'base_scene', 		// 场景-用场景编辑器导表
			// 'base_skill',		// 技能配置-原来的php配置
			'base_skill_normal_special_effect',  
			'base_skill_special_effect',
			// 'base_task',		// 任务配置-原来的php配置
			// 'base_talk',		// 对话配置-原来的php配置
			// 'base_npc',			// npc配置-使用原来的php配置

		);

	// 不进行配置生成的数据表
	protected $special_table_without_create_config = array(
			'base_log',			// (特殊)日志表
			'base_exceloption',	// (特殊)配置表
			'base_goods_subtype',
			'base_boss_reward',
			'base_kfboss_reward',
			'base_skill_normal_special_effect',  
			'base_skill_special_effect',
			'base_drop_mon_list',
			'base_treasure_info',
			'base_christmas_treasure_info'
		);

	// 需要使用特殊的EXCEL表导入方法的数据表
	protected $special_table_use_importdb2 = array(
			'base_scene'  		// 场景
	);
     

	// **************
	// 界面显示
	public function option(){
		$group_infos = $this->db->fetchRow("select id,comment from base_exceloption where pid=0 order by convert(comment using gbk) asc;");
		$configgroup = array(0=>'我就是主归属');
		if(is_array($group_infos)){
			foreach ($group_infos as $group) {
				$configgroup[$group['id']] = $group['comment'];
			}
		}

		$config_infos = $this->db->fetchRow("SELECT b.*, p.ksort as ksort2 FROM base_exceloption AS b LEFT JOIN base_exceloption AS p ON b.pid=p.id WHERE b.pid!=0 ORDER BY convert(p.comment using gbk) asc, b.ksort asc, convert(b.comment using gbk) asc;"); 
		$config = array();
		if(is_array($config_infos)){
			foreach ($config_infos as $item) {
				$config[$item['pid']][$item['id']] = $item;
			}
		}

		// 是否导入稳定服中
		$base_option_flag = trim(file_get_contents("base_option_flag.txt"));

		$this->smarty->assign('base_option_flag',$base_option_flag);
		$this->smarty->assign('configgroup',$configgroup);
		$this->smarty->assign('config',$config);
// print_r($config);exit;	
		$this->smarty->display("./exceloption.shtml");
	}

	// **************
	// 界面操作
	public function option_action(){
		$type = trim($_GET['type']);
		$is_svn_commit = false;
		ini_set('max_execution_time','0');
		if(isset($type) && !empty($type)) {
			$info = $this->db->fetchOne("select * from base_exceloption where id='$type'");		
			$is_svn_commit = $this->create_one($info['table_name'], $info['path'], $info['comment']);
			
		}else{
			$infos = $this->db->fetchRow("select * from base_exceloption where pid!=0"); 
			$_SESSION['base_exceloption_infos'] = $infos;
			echo "<script>document.location.href='./base_exceloption_create_all.php';</script>";
			//log_write("create all","w");
			$is_svn_commit = false; // 在base_exceloption_create_all.php 中提交svn
			// if (is_array($infos)){
			// 	foreach ($infos as $row){
			// 		$ret = $this->create_one($row['table_name'], $row['path'], $row['comment']);
			// 		$is_svn_commit = $is_svn_commit || $ret;
			// 	}
			// }
		}
		// 提交svn
		if($is_svn_commit) $this->svn_commit_all_config();
	}

	// 勾选生成
	public function option_action_check(){
		
		if(!$_POST['type']){
			alert('请选择需要生成的文件');exit;
		}
		$is_svn_commit = false;
         
		foreach ($_POST['type'] as $key => $val) {
			$type = $val;
			$info = $this->db->fetchOne("select * from base_exceloption where id='$type'"); 
		
			$is_svn_commit = $this->create_one($info['table_name'], $info['path'], $info['comment']);
		}
		
		// 提交svn
		if($is_svn_commit) $this->svn_commit_all_config();
	}

	public function svn_commit_all_config(){
		echo "生成成功！<br> ";
		include_once 'action/svn.class.php';
		$svn_class = new action_svn();
		echo "<br><br>erl文件正在提交svn...<br>";
		$svn_class->svn_commit_erl();
		echo "<br><br>hrl文件正在提交svn...<br>";
		$svn_class->svn_commit_hrl();
		// echo "<br><br>xml文件正在提交svn...<br>";
		// $svn_class->svn_commit_xml();
		echo "<br><br>lua文件正在提交svn...<br>";
		$svn_class->svn_commit_lua();
	}

	public function create_one($table_name, $path, $comment){
		// 入库
		if ($this->import_db($table_name, $path)){
			// 生成配置
			if($this->create_config($table_name)){
				return true;
			}else{
				echo "生成{$comment}配置失败!!!!!<br>";
			}
		}else{
			echo "{$comment}导入表{$table_name}失败";
		}
		return false;
	}

	// **************
	//  生成配置文件，在对应的xxx.class.php中的create_config函数中执行对应逻辑
	private function create_config($table_name){
		if(in_array($table_name, $this->special_table_without_create_config)){
			return true;
		}else{
			$control = substr($table_name, 5); // 去掉base_
			$class_file = ACTION_GATEWAY_PATH . "action/erl/{$control}.class.php";
// print_r($class_file);exit;			
			if(!file_exists($class_file)){
				print_r("file not exists:".$class_file);
				return true;
			}
			require_once $class_file;
			$class = "action_{$control}";
			$method = 'create_config'; // 必须是create_config函数
			$action_instance = new $class;
			if (!method_exists($action_instance, $method))
			{
				alert("类{$class}的方法{$method}不存在", '');
			}
			return $action_instance->$method();
		}
	}

	// **************
	//  Excel数据导入数据库
	private function import_db($table, $path){
		if(in_array($table, $this->special_table_without_import_db)){
			//特殊表不处理，如场景表
			return true;
		}else if(in_array($table, $this->special_table_use_importdb2)){
			return $this->import_db2($table, $path);
		}else{
			ini_set('memory_limit','2048M');
			error_reporting(E_ALL ^ E_NOTICE);
			set_include_path(PATH_SEPARATOR . ROOT_DIR.'admin/Classes'. PATH_SEPARATOR . get_include_path());
			require_once("Classes/PHPExcel.php");
			$objPHPExcel = new PHPExcel();
			$objReader = PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format
			// $objReader = PHPExcel_IOFactory::createReader('Excel2007'); 
			
			$file =  EXCEL_CONFIG_PATH . $path;
			//echo "$file";
			if(is_readable($file) == false)
			  	chmod($file, 0644);

			try{
				$objPHPExcel = $objReader->load($file);
			}catch(Exception $e){
				
				echo "<br>read $file error!!!!!!!!    ";
				echo '<br>Message: ' .$e->getMessage();
				exit();
			};
// print_r($objPHPExcel);exit;
			$sql = "INSERT INTO {$table}(";
			//$objPHPExcel->getSheetCount()
			for ($SheetIndex = 0; $SheetIndex < 1; ++$SheetIndex) {
				$data = $objPHPExcel->getSheet($SheetIndex); 
				$cols = $data->getHighestColumn();//竖行数,从1开始
				$rows = $data->getHighestRow(); //横行数,从1开始
				$cols = PHPExcel_Cell::columnIndexFromString($cols);//总列数

				// 去掉后面空白的列
				$colscount = 0;
				for($j = 0;$j<$cols;$j++){//计算实际有多和列，防止空白列
					$realcols = $data->getCellByColumnAndRow($j, 1)->getValue();
					if(!isset($realcols)) continue;
					$colscount ++;
				}
				$cols = $colscount;

				if($SheetIndex==0){
					for($j = 0;$j<$cols;$j++){
						$fields = mysql_real_escape_string($data->getCellByColumnAndRow($j, 1)->getValue());
						$sql .= "`$fields`";
						if($j < $cols-1) $sql .= ",";
					}
					$sql .= ")VALUES";
				}

				// 不合法的标签跳过
				if(!$this->check_sheet_normal($data, $cols, $table)){
					print_r("Excel标签不匹配DB");
					continue;
				}
				for ($i = 3; $i <= $rows; $i++){//从第三行开始，第一行作为字段名 第二行是描述
					if(mysql_real_escape_string($data->getCellByColumnAndRow(0, $i)->getValue()) == '') continue;
					$sql .= "(";
					for($j = 0;$j<$cols;$j++){
						$value = mysql_real_escape_string($data->getCellByColumnAndRow($j, $i)->getValue());
						$sql .= "'{$value}'";
						if($j < $cols-1) $sql .= ",";
					}
					$sql .= "),";
				}
			}
			$sql = rtrim($sql,',');

			// 可能Excel表中无数据
			if(substr($sql, -6) == "VALUES")
				$sql = "DELETE FROM {$table}";
			
			$sqltruncate = "DELETE FROM {$table}";
			$sqllist = array(
			0 => $sqltruncate,
			1 => $sql,	
			);
			// echo $sql;
			$conn = $this->db->transaction($sqllist);
			if($conn != 1){
				echo '<span style="color:red;">导入失败，请拉到最下面查看出错问题</span><br>'.$conn;
				//alert("导入失败");
			}
			return $conn == 1;
		}
	}

	// **************
	//  Excel数据导入数据库 -- 方法2
	private function import_db2($table, $path){
		ini_set('memory_limit','2048M');
		error_reporting(E_ALL ^ E_NOTICE);
		set_include_path(PATH_SEPARATOR . ROOT_DIR.'admin/Classes'. PATH_SEPARATOR . get_include_path());
		require_once("Classes/PHPExcel.php");
		$objPHPExcel = new PHPExcel();
		$objReader = PHPExcel_IOFactory::createReader('Excel5');//use excel2007 for 2007 format 
		$file =  EXCEL_CONFIG_PATH . $path;
		//echo "$file";
		$sqllist = array();
		if(is_readable($file) == false)
		  	chmod($file, 0644);

		try{
			$objPHPExcel = $objReader->load($file);
		}catch(Exception $e){
			echo "<br>read $file error!!!!!!!!    ";
			echo '<br>Message: ' .$e->getMessage();
			exit();
		};
		$sql = "";
		for ($SheetIndex = 0; $SheetIndex < $objPHPExcel->getSheetCount(); ++$SheetIndex) {
			$data = $objPHPExcel->getSheet($SheetIndex); 
			$cols = $data->getHighestColumn();//竖行数,从1开始
			$rows = $data->getHighestRow(); //横行数,从1开始
			$cols = PHPExcel_Cell::columnIndexFromString($cols);//总列数
			$array0 = array();

			// 去掉后面空白的列
			$colscount = 0;
			for($j = 0;$j<$cols;$j++){//计算实际有多和列，防止空白列
				$realcols = $data->getCellByColumnAndRow($j, 1)->getValue();
				if(!isset($realcols)) continue;
				$colscount ++;
			}
			$cols = $colscount;

			if($SheetIndex==0){
				for($j = 0;$j<$cols;$j++){
					$s0 = "";
					$fields = mysql_real_escape_string($data->getCellByColumnAndRow($j, 1)->getValue());
					$s0 .= "`$fields`=";
					array_push($array0, $s0);
					// if($j < $cols-1) $sql .= ",";
				}
				// $sql .= ")VALUES";
			}

			// 不合法的标签跳过
			// if(!$this->check_sheet_normal($data, $cols, $table))
			// 	continue;

			for ($i = 3; $i <= $rows; $i++){//从第三行开始，第一行作为字段名 第二行是描述
				if(mysql_real_escape_string($data->getCellByColumnAndRow(0, $i)->getValue()) == '') continue;
				$sql = "UPDATE `{$table}` SET ";
				$sceneid = "";
				for($j = 0;$j<$cols;$j++){
					$value = mysql_real_escape_string($data->getCellByColumnAndRow($j, $i)->getValue());
					if ($j == 0) $sceneid .= " WHERE `id` = '{$value}'";
					$sql .= $array0[$j];
					$sql .= "'{$value}'";
					if($j < $cols-1) $sql .= ",";
				}
				$sql .= $sceneid;
				array_push($sqllist,$sql);
			}
		}
        //print_r($sqllist);
		// 可能Excel表中无数据
		//if($sql == "") return true;

		//$sql = rtrim($sql,';');
		//$sqllist = explode(";", $sql);

		// $sqltruncate = "SELECT * FROM {$table}";
		// $sqllist = array(
		// 	0 => $sqltruncate,
		// 	1 => $sqltruncate,
		// 	2 => $sqltruncate,	
		// );
		$conn = $this->db->transaction($sqllist);
		if($conn != 1){
			echo '<span style="color:red;">导入失败，请拉到最下面查看出错问题</span><br>'.$conn;
		} else {
			return true;
		}
	}

	
	private function check_sheet_normal($data, $cols, $table){
		//  检查第一列是不是数据字段名
		$frs = $this->db->fetchrow("SHOW FULL FIELDS FROM $table");
		$fieldarr = array();
		foreach ($frs as $fv){
			$fieldarr[$fv['Field']] = true;
		}
		if($cols == 0) return false;
		for($j = 0;$j<$cols;$j++){
			$value = mysql_real_escape_string($data->getCellByColumnAndRow($j, 1)->getValue());
			//echo " aaaa {$value} aaaa <br>";
			if(!isset($fieldarr[$value])){
				print_r($value);
				return false;			
			}
		}
		return true;	
	}

	// **************
	//  导到稳定服
	public function copy_to_base_server_action(){
		file_put_contents("base_option_flag.txt", '1');
		$type = trim($_GET['type']);
		$is_svn_commit = false;
		ini_set('max_execution_time','0');
		if(isset($type) && !empty($type)) {
			$info = $this->db->fetchOne("select * from base_exceloption where id='$type'"); 
			$is_svn_commit = $this->copy_to_base_server_action_one($info['table_name'], $info['path']);
		}else{
			$infos = $this->db->fetchRow("select * from base_exceloption where pid!=0"); 

			$this->copy_to_base_server_action_one('base_log', '');
			$this->copy_to_base_server_action_one('base_exceloption', '');

			// $_SESSION['base_exceloption_infos'] = $infos;
			// echo "<script>document.location.href='./base_exceloption_create_all.php';</script>";
			// //write("create all","w");
			// $is_svn_commit = false; // 在base_exceloption_create_all.php 中提交svn

			if (is_array($infos)){
				foreach ($infos as $row){
					$ret = $this->copy_to_base_server_action_one($row['table_name'], $row['path']);
					$is_svn_commit = $is_svn_commit || $ret;
				}
			}
		}
		// 提交svn
		// if($is_svn_commit) $this->svn_commit_all_base_excel();
        echo "{$row['table_name']}表 导表完成<br>";
		file_put_contents("base_option_flag.txt", '0');
	}

	public function copy_to_base_server_action_one($table, $path){
		// if(in_array($table, $this->special_table_without_import_db)){
			//没有Excel的表直接进行数据库导表
			echo "{$table}表 直接进行数据库导表<br>";
			system("mysqldump -u fynemo -pfynemo123456 fynemo_dev {$table} > dump.sql");
			system("mysql -u fynemo -pfynemo123456 fynemo_base < dump.sql");
			system("rm dump.sql");
			return true;
		// }else{
		// 	// 复制Excel过去稳定服目录
		// 	echo "{$table}表 复制Excel到稳定服目录<br>";
		// 	$DevExcelRoot = "/data/www/fynemo/dev/doc/Excel数据配置";
		// 	$BaseExcelRoot = "/data/www/fynemo/base/doc/Excel数据配置";
		// 	$srcfile = $DevExcelRoot . $path;
		// 	$desfile = $BaseExcelRoot . $path;
		// 	$basedir = $BaseExcelRoot . dirname($path);
		// 	// 创建文件夹
		// 	if(is_dir($basedir)){
		// 		copy($srcfile, $desfile);
		// 	}else{
		// 		mkdir($basedir, 0777, true);
		// 		copy($srcfile, $desfile);
		// 	}

		// 	return true;
		// }
	}

	public function svn_commit_all_base_excel(){
		echo "提交稳定服Excel<br>";

		$dir = "/data/www/fynemo/base/doc/Excel数据配置/";
	    $version =  svn_update($dir);
	    echo '更新版本：'.$version.'<br>';
	    echo '<pre>';
	    $list = svn_status($dir);
	    // print_r($list);
	    echo '</pre>';
	    echo '<br>';
	    if(!empty($list)){
	        foreach ($list as $key => $val) {
	            if($val['text_status'] == 2){
	               echo $val['path'].' Add '.var_dump(svn_add($val['path'])).'<br>';
	            }elseif($val['text_status'] == 8){
	               echo $val['path'].' Edit <br>';
	            }
	        }
	    }
	    echo '<br>';
	    
	    $res = svn_commit('开发服导入', array($dir));
	    print_r($res);
	    if($res[0] == -1){
	        echo '无内容提交';
	    }else{
	        echo '提交版本号：'.$res[0];
	    }

	}

	// **************
	//  导出Excel模版
	public function export_tpl_excel(){
		$type = trim($_GET['type']);
		if(isset($type) && !empty($type)) {
			$info = $this->db->fetchOne("select * from base_exceloption where id='$type'"); 
			$this->export_one($info['table_name']);
		}else{
			echo "不支持全部导出！！！";
		}
	}

	private function export_one($table){
		$rows = $this->db->fetchrow("SELECT * FROM $table ");
		$list = array();
		foreach ($rows as $val){
			foreach ($val as $k => $v){
				$list[$k][] = $v;
			}
		}
		
		$fieldsql = "SHOW FULL FIELDS FROM $table";
		$frs = $this->db->fetchrow($fieldsql);
		$fieldarr = array();
		foreach ($frs as $fv){
			$fieldarr[$fv['Field']] = $fv['Comment'];
		}
		ini_set('memory_limit','2048M');
		require_once("Classes/PHPExcel.php");
		$objPHPExcel = new PHPExcel();
		$i = 0;
		$j = 3;
		
		if(!empty($list)){
			foreach($list as $key => $val){
				$fields = $key;
				$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i,1, $fields);
				if(isset($fieldarr[$key])){
					$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i,2, $fieldarr[$key]);
				}

			    foreach ($val as $v){
			    	$objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i,$j++, trim($v));
			    }
			    $i++;
			    $j = 3;
			}
		}else{
			foreach ($fieldarr as $key => $val) {
	            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i,1, $key);
	            $objPHPExcel->setActiveSheetIndex(0)->setCellValueByColumnAndRow($i,2, $val);
	            $i++;
            }
		}
		
		$objPHPExcel->getActiveSheet()->setTitle($table);
		$excelName = $table.'.xls';
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel; charset=utf-8');  
		header("Content-Disposition: attachment; filename=".$excelName);
		header('Cache-Control: max-age=0');
		header("Pragma: no-cache");
		$objWriter->save('php://output');
	}

	// **************
	//  增加配置
	public function add_config(){
		$id = $_REQUEST['id'];
		$pid = $_REQUEST['pid'];
		$comment = $_REQUEST['comment'];
		$table_name = $_REQUEST['table_name'];
		$path = $_REQUEST['path'];
		$ksort = $_REQUEST['ksort'];
		if(!isset($id) || $id == ''){
			$sql = "insert into base_exceloption(pid,comment,table_name,path,ksort)values('$pid','$comment','$table_name','$path','$ksort')";
			$this->db->query($sql);
			alert('');
		}else{
			$sql = "replace into base_exceloption(id,pid,comment,table_name,path,ksort)values('$id','$pid','$comment','$table_name','$path','$ksort')";
			$this->db->query($sql);
			alert('');
		}
	}
	// **************
	//  删除配置
	public function del_config(){
		$id = $_REQUEST['id'];
		if($id == '') alert('ID为空');
		$this->db->query("delete from base_exceloption where id = '$id'");
		alert("");
	}

	// **************
	//  显示配置内容
	public function option_show(){
		$type = trim($_GET['type']);
		if(isset($type) && !empty($type)) {
			$info = $this->db->fetchOne("select * from base_exceloption where id='$type'"); 
			$table = $info['table_name'];
			$lxr_log_info = $this->db->fetchOne("select id from base_log where `table`='{$table}'");
			if(isset($lxr_log_info) && !empty($lxr_log_info)){
				$url = "base_log_show.php?base_log_id=$lxr_log_info[id]";
				if (isset($url)){ 
					Header("HTTP/1.1 303 See Other"); 
					Header("Location: $url"); 
					exit; //from www.w3sky.com 
				} 
			}else{
				$_GET['type'] = $table;
				include_once 'action/erl.class.php';
				$erl_class = new action_erl();
				$erl_class->erl_api();
			}
			//ctl=erl&act=erl_api&type=base_activity_info
		}else{
			echo "不支持查看全部！！！";
		}
	}

}