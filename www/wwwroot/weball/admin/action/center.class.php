<?php
class action_center {
    /**
     * 入口方法
     */
    public function __call($method,$arg){
        $controller = request('ctl','str');
        $class = request('act','str');
        $class_file = ACTION_GATEWAY_PATH . 'action/' . $controller . '/' .$class . '.php';
        if(!file_exists($class_file)){
            exit('404~');
        }
        include $class_file;
        if(class_exists($class)){
            $method = request('method','str')?:'handle';
            try{
                $method = new ReflectionMethod($class, $method);
                if ($method->isPublic()) {
                    $class = new ReflectionClass($class);
                    $instance  = $class->newInstance();
                    $method->invoke($instance);
                }else{
                    exit('迷路了啊 小伙子@ !');
                }
            }catch(ReflectionException $e){
                Ext_Debug::log('msg:'.$e->getMessage().';file:'.$e->getFile().';line:'.$e->getLine(),'error');
                exit('迷路了啊 小伙子@ !');
            }
        }else{
            exit('迷路了啊 小伙子@ !');
        }
    }
}