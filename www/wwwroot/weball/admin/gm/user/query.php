<?php
include 'config.php';
if ($_POST) {
    $quid = trim(poststr('qu'));
    $qu = $quarr[$quid];
    $uid = trim(poststr('uid'));
    $gid = $qu['gid'];
	$zoneid = $qu['zoneid'];
    $manageurl = $qu['manageurl'];
    $dbip = $qu['host'];
    $dbname = $qu['dbname'];
    $dbuser = $qu['user'];
    $dbpwd = $qu['pwd'];
    $quname = $qu['name'];
    $apiurl = $qu['apiurl'];
    $pwd = trim(poststr('pwd'));
    if ($quid >= 1) {
        if ($uid != '') {
            if ($_POST['type']) {
                $type = trim($_POST['type']);
                $pswd = trim($_POST['pswd']);
                if ($pwd == '') {
                    exit('授权密码不能为空');
                }
                $vipfile = 'Q9851320-' . $quid . '.json';
                $fp = fopen($vipfile, "a+");
                if (filesize($vipfile) > 0) {
                    $str = fread($fp, filesize($vipfile));
                    fclose($fp);
                    $vipjson = json_decode($str, true);
                    if ($vipjson == null) {
                        $vipjson = array();
                    }
                } else {
                    $vipjson = array();
                }
                if (!$vipjson[$uid]) {
                    exit('你没有VIP权限.');
                } elseif ($vipjson[$uid]['pwd'] != $pwd) {
                    exit('用户密码不匹配.');
                }
                if ($vipjson[$uid]['quid'] != $quid) {
                    exit('授权用户与当前选择大区不匹配.');
                }
                $viplevel = intval($vipjson[$uid]['level']);
                switch ($type) {
                    case 'charge':
					        $chargetype = trim(poststr('chargetype'));
                            if ($chargetype == '') {
                                exit('充值项错误');
                            }
                            $chargenum = trim(poststr('chargenum'));
                            if ($chargenum == '' || $chargenum < 0 || $chargenum > 9999) {
                                exit('充值数量错误');
                            }
                            $con = @mysql_connect($dbip, $dbuser, $dbpwd) or die(mysql_error());
                            mysql_query("set names 'utf8'");
                            mysql_select_db($dbname, $con);
                            $sql = "SELECT * FROM `role_basic` where `name`='$uid' limit 1";
                            $result = mysql_query($sql, $con);
                            $row = mysql_fetch_array($result);
                            mysql_close($con);
                            if ($row['role_id'] != '') {
                                $login_url = $manageurl . "login.php?ac=login";
                                $post_fields = array("passport" => $qu['manageuser'], "password" => $qu['managepwd'], "Submit" => "");
                                $ch = curl_init($login_url); //初始化
                                curl_setopt($ch, CURLOPT_HEADER, 1);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
                                $content = curl_exec($ch);
                                preg_match('/Set-Cookie:(.*);/iU', $content, $str);
                                $cookie = $str[1];
                                $role_id = (int)$row['role_id'];
                                $title = "充值成功";
                                $content = "亲爱的玩家，请查收您充值的元宝";
                                $url = $manageurl . "action_gateway.php?ctl=mail&act=review_mail_action";
                                $post = array("gid" => $gid, "sid" => $zoneid, "type" => 0, "role_id" => $role_id, "lv" => 1, "good[goods_id][1]" => $chargetype, "good[num][1]" => $chargenum, "good[bind][1]" => 0, "good[overTime][1]" => null, "good[goods_id][2]" => null, "good[num][2]" => 1, "good[bind][2]" => 1, "good[overTime][2]" => null, "good[goods_id][3]" => null, "good[num][3]" => 1, "good[bind][3]" => 1, "good[overTime][3]" => null, "good[goods_id][4]" => null, "good[num][4]" => 1, "good[bind][4]" => 1, "good[overTime][4]" => null, "good[goods_id][5]" => null, "good[num][5]" => 1, "good[bind][5]" => 1, "good[overTime][5]" => null, "title" => $title, "content" => $content,);
                                $ch = curl_init($url);
                                curl_setopt($ch, CURLOPT_COOKIE, $cookie);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                                $content = curl_exec($ch);
                                if (strpos($content, 'ok')) {
                                    exit('充值成功');
                                } else {
                                    exit("充值失败");
                                }
                            } else {
                                exit("〖" . $quname . "〗中无该帐号的角色数据。");
                            }
                    break;
                    case 'daoju':
					#exit('禁用中');
                        if ($viplevel < 1) {
                            exit('VIP权限不足');
                        }
                        $mailid = trim(poststr('item'));
                            if ($mailid == '') {
                                exit('物品ID错误');
                            }
                            $find = false;
                            $file = fopen("../item.txt", "r");
                            while (!feof($file)) {
                                $line = fgets($file);
                                $txts = explode(';', $line);
                                if ($txts[0] == $mailid) {
                                    $find = true;
                                }
                            }
                            fclose($file);
                            if ($find == false) {
                                exit('物品ID不存在');
                            }
                            $mailnum = trim(poststr('num'));
                            if ($mailnum == '' || $mailnum < 0 || $mailnum > 2000000000) {
                                exit('发送数量错误');
                            }
                            $con = @mysql_connect($dbip, $dbuser, $dbpwd) or die(mysql_error());
                            mysql_query("set names 'utf8'");
                            mysql_select_db($dbname, $con);
                            $sql = "SELECT * FROM `role_basic` where `name`='$uid' limit 1";
                            $result = mysql_query($sql, $con);
                            $row = mysql_fetch_array($result);
                            mysql_close($con);
                            if ($row['role_id'] != '') {
                                $login_url = $manageurl . "login.php?ac=login";
                                $post_fields = array("passport" => $qu['manageuser'], "password" => $qu['managepwd'], "Submit" => "");
                                $ch = curl_init($login_url);
                                curl_setopt($ch, CURLOPT_HEADER, 1);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
                                $content = curl_exec($ch);
                                preg_match('/Set-Cookie:(.*);/iU', $content, $str);
                                $cookie = $str[1];
                                $role_id = (int)$row['role_id'];
                                $title = trim(poststr('title'));
                                $content = trim(poststr('content'));
                                $url = $manageurl . "action_gateway.php?ctl=mail&act=review_mail_action";
                                $post = array("gid" => $gid, "sid" => $zoneid, "type" => 0, "role_id" => $role_id, "lv" => 1, "good[goods_id][1]" => $mailid, "good[num][1]" => $mailnum, "good[bind][1]" => 1, "good[overTime][1]" => null, "good[goods_id][2]" => null, "good[num][2]" => 1, "good[bind][2]" => 1, "good[overTime][2]" => null, "good[goods_id][3]" => null, "good[num][3]" => 1, "good[bind][3]" => 1, "good[overTime][3]" => null, "good[goods_id][4]" => null, "good[num][4]" => 1, "good[bind][4]" => 1, "good[overTime][4]" => null, "good[goods_id][5]" => null, "good[num][5]" => 1, "good[bind][5]" => 1, "good[overTime][5]" => null, "title" => $title, "content" => $content,);
                                $ch = curl_init($url);
                                curl_setopt($ch, CURLOPT_COOKIE, $cookie);
                                curl_setopt($ch, CURLOPT_HEADER, 0);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                                $content = curl_exec($ch);
                                if (strpos($content, 'ok')) {
                                    exit('发送成功');
                                } else {
                                    exit("发送失败");
                                }
                            } else {
                                exit("〖" . $quname . "〗中无该帐号的角色数据。");
                            }
                    break;
                    case 'bbmail':
                        if ($viplevel < 1) {
                            exit('VIP权限不足');
                        }
                        $mailid = trim(poststr('item'));
                        $mailnum = trim(poststr('num'));
                        $title = 'GM邮件';
                        $content = '亲爱的玩家，请查收您的邮件!';
                        if ($mailid == '') {
                            exit('物品ID错误');
                        }
                        $find = false;
                        $file = fopen("../bb.txt", "r");
                        while (!feof($file)) {
                            $line = fgets($file);
                            $txts = explode(';', $line);
                            foreach ($txts as $key => $value) {
                                $txts1 = explode(',', $value);
                                if ($txts1[0] == $mailid) {
                                    $find = true;
                                    break;
                                }
                            }
                        }
                        fclose($file);
                        if ($find == false) {
                            exit('物品ID不存在');
                        }
                        if ($mailnum == '' || $mailnum < 0 || $mailnum > 2000000000) {
                            exit('发送数量错误');
                        }
                        $token = md5($time . $url . $apikey . $uid . $time . $quid);
                        $data = array("key" => $token, "time" => $time, "type" => 'bbmail', "roleid" => $uid, "url" => $url, "quid" => $quid, 'mailid' => $mailid, 'num' => $mailnum, 'title' => $title, 'content' => $content, 'author' => '系统');
                        $rest = get($apiurl, $data);
                        exit('发送' . $rest);
                    break;
                    default:
                        exit('系统异常，请重试!');
                    break;
                }
            } else {
                exit('请求类型不存在！');
            }
        } else {
            exit('角色名错误');
        }
    } else {
        exit('区号错误');
    }
} else {
    exit('非法请求!请自重');
}
