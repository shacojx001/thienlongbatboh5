<?php
/**
 * 业务处理入口文件
 * 
 * 旧做法：一个模块的业务处理，单独一个文件或拆分成几个文件，这样会导致文件过多，查找和维护都不方便
 * 新做法：所有业务都通过该入口，再通过参数分发到具体的业务处理文件夹action/下面
 * 			菜单的链接地址需要这样填写，如：action_gateway.php?ctl=user&act=list
 * 			其中ctl的值对应为action文件夹下面的类文件， act为类下面的方法
 */

define("ACTION_GATEWAY_PATH", dirname(__FILE__) . '/');
require_once ACTION_GATEWAY_PATH . 'header.php';

$control = isset($_REQUEST['ctl']) ? htmlspecialchars(trim($_REQUEST['ctl'])) : '';
$action = isset($_REQUEST['act']) ? htmlspecialchars(trim($_REQUEST['act'])) : '';

if (empty($control) || empty($action))
{
	alert(Ext_Template::lang('参数错误'), '');
}

$class_file = ACTION_GATEWAY_PATH . "action/{$control}.class.php";

require_once $class_file;
$class = "action_{$control}";
$method = $action;
$action_instance = new $class;
if (!is_callable(array($action_instance,$method)))
{
	alert("类{$class}的方法{$method}不存在", '');
}
$action_instance->$method();
exit;
