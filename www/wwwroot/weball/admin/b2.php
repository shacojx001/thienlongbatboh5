<?php
$data = array(
	'op'		=>'1', 
	'no'		=> 'order_' . time(). mt_rand(100, 999),
	'game'		=> $_GET['game'],
	'zone'		=> $_GET['zone'],
	'guser'		=> $_GET['username'],
	'realpay'	=> $_GET['amount'],
	'paytype'	=> $_GET['paytype']
);

$ch = curl_init('https://tlbb.autopayez.com/index.php');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 对认证证书来源的检查
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); // 从证书中检查SSL加密算法是否存在
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);

echo $result;

