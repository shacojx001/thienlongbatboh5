<?php
/**
 * 初始化接口
 */
require '../bs2.php';
require './config.php';
$ios_version = request('ios_version');//审核版本号
$params['channel'] = $channel = request('channel');//渠道号
$params['time'] = $time = request('time'); //时间戳 10位
$ticket = request('ticket');
//验证参数
if(!$channel || !$ticket || !$time){
   
    return_json(-1,array());
}
//print_r(w_encrypt($params));
//验证ticket
if(w_encrypt($params) != $ticket){
    
    return_json(-7,array());
}
//验证platform
$gid = $channel_list[$channel]['gid'];

if(!$gid){
   
    return_json(-8,array());
}
/* 专服标识 */
$data['platform'] = $gid;
/* game_id */
$data['game_id'] = $channel_list[$channel]['game_id'] ? $channel_list[$channel]['game_id'] : 0;
/* is_pay */
$data['is_pay'] = $channel_list[$channel]['is_pay'];
/* 全局url */
$data['admin_url'] = "http://117.4.244.81:82/";
/* 资源地址 */
$data['res_url'] = $gid_list_info[$gid]['res_url'];
/* 资源版本号获取地址 */
$data['version_url'] = $gid_list_info[$gid]['version_url'];
/* sdk登陆验证接口 */
$data['verify_url'] = 'vt/api_verify.php';
/* 获取服务器列表 */
$data['server_url'] = 'vt/api_server.php';
/* 下单接口 */
$data['order_url'] = 'vt/api_order.php';
/* 创建角色接口 */
$data['reg_url'] = 'vt/api_insert_reg.php';
/* 更新角色接口 */
$data['update_url'] = 'vt/api_update_info.php';
/* 公告接口 */
$data['notice_url'] = 'vt/api_notice.php';
/* 客户端报错接口 */
$data['error_url'] = 'vt/api_client_error.php';
/* 礼包接口 */
$data['card_url'] = 'vt/api_card.php';
/* 设备流失统计 */
$data['pos_url'] = 'vt/api_pos.php';


/* 图片上传接口 */
$data['photo_upload_url'] = 'wx/show/head_photo_upload.php';

/* 图片显示接口 */
$data['photo_show_url'] = PIC_SHOW;

if ($ios_version && in_array($ios_version, $channel_list[$channel]['ios_version'])) {
    /* 全局url */
    $data['admin_url'] = "http://117.4.244.81:82/";
}

return_json(1,$data);
