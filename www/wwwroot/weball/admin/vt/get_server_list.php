<?php
/**
 *
 * 切换服务器：
 */
require '../bs2.php';
require './config.php';

$channel = request('channel');//渠道号
$accname = request('accname');//平台帐号
$ios_version = request('ios_version');//审核版本号
$ticket = request('ticket');//加密
$device_id = request('device_id');//设备号

//加密列表
if ($ios_version) {
    $encrypt_list = array(
        'channel' => $channel,
        'accname' => $accname,
        'ios_version' => $ios_version,
    );
} else {
    $encrypt_list = array(
        'channel' => $channel,
        'accname' => $accname,
    );
}

if (empty($channel) || empty($accname) ||empty($device_id)) {
    echo urldecode(json_encode(array('info' => '-1', 'data' => urlencode('参数不全'))));
    exit;//参数不齐
}

if ($ticket != encrypt($encrypt_list)) {
    echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('加密不对'))));
    exit;//加密不对
}

unset($encrypt_list);

//支付开关
if ($channel_list[$channel]['is_pay'] == 1) {
    $is_pay = 1;
} else {
    $is_pay = 0;
}
//白名单
if (isset($ip_list[$channel]) && in_array($accname,$ip_list[$channel])) {
    $white_list = 1;
} else {
    $white_list = 0;
}

//是否IOS审核服
if ($ios_version && $ios_version == $channel_list[$channel]['ios_version']) {
    $data = array(
        '1' => array(
            "id" => "104",
            "description" => urlencode("天下无双"),
            "game_ip" => "jyjh.ios.gznemo.com",
            "game_port" => "7510",
            "otime" => "1467873474",
            "visible_time" => "1471957001",
            "hot_state" => "0",
            "recom_state" => "0",
            "default" => "1",
            "repair_ctime" => "1467873666",
            "repair_etime" => "1467960068",
            "stop_service_describe" => "",
            "server_num" => "1",
            "shenhe" => "1",
        ),
    );

} else {//非IOS审核
    $sid_list = array();
    foreach ($server_list_info as $key => $val) {
        if (is_numeric(strpos($val['channel_list'], $channel))) {
            $sid_list[$key] = $val;
        }
    }
    $data = array();
    //判断是否测试名单
    if ($white_list == 1) {
        foreach ($sid_list as $key => $val) {
            if ($val['default'] == 1 || $val['default'] == 0 || $val['default'] == 4 || $val['default'] == 6) {
                $data[$key]['server_id'] = $val['id'];
                $data[$key]['description'] = urlencode(preg_replace('/^([0-9]+)/', '', $val['description']));
                $data[$key]['game_ip'] = urlencode($val['game_ip']);
                $data[$key]['game_port'] = $val['game_port'];
                $data[$key]['otime'] = $val['otime'];
                $data[$key]['visible_time'] = $val['visible_time'];
                $data[$key]['hot_state'] = $val['hot_state'];
                $data[$key]['recom_state'] = $val['recom_state'];
                $data[$key]['default'] = $val['default']; //状态
                $data[$key]['repair_ctime'] = $val['repair_ctime']; //维护开始
                $data[$key]['repair_etime'] = $val['repair_etime']; //维护结束
                $data[$key]['stop_service_describe'] = urlencode($val['stop_service_describe']); //停服描述
            }
        }

    } else {
        //玩家账号
        foreach ($sid_list as $key => $val) {
            if ($val['visible_time'] >= time()) continue;
            if ($val['default'] == 1 || $val['default'] == 4 || $val['default'] == 6) {
                $data[$key]['server_id'] = $val['id'];
                $data[$key]['description'] = urlencode(preg_replace('/^([0-9]+)/', '', $val['description']));
                $data[$key]['game_ip'] = urlencode($val['game_ip']);
                $data[$key]['game_port'] = $val['game_port'];
                $data[$key]['otime'] = $val['otime'];
                $data[$key]['visible_time'] = $val['visible_time'];
                $data[$key]['hot_state'] = $val['hot_state'];
                $data[$key]['recom_state'] = $val['recom_state'];
                $data[$key]['default'] = $val['default']; //状态
                $data[$key]['repair_ctime'] = $val['repair_ctime']; //维护开始
                $data[$key]['repair_etime'] = $val['repair_etime']; //维护结束
                $data[$key]['stop_service_describe'] = urlencode($val['stop_service_describe']); //停服描述
            }
        }
    }

}//是否IOS审核服
$cache = Ext_Memcached::getInstance("api");
$key = sprintf(Key::$api['server_list'],$accname);
if (!$player = $cache->fetch($key)) {
    $db = Ext_Mysql::getInstance('allcenter');
    $sql = "select role_id,accname,last_login_server,last_login_time,`nickname`,server_id,reg_time,`level`,channel from player where accname='$accname' order by last_login_time desc";
    $player = $db->fetchRow($sql);
    $cache->store($key, $player, 3600);
}
// 写入缓存end


$person_list = array();
if ($player) {
    foreach ($player as $k => $val) {
        $person_list[$k]['role_id'] = $val['role_id'];
        $person_list[$k]['accname'] = urlencode($val['accname']);
        $person_list[$k]['last_login_server'] = $val['last_login_server'];
        $person_list[$k]['last_login_time'] = $val['last_login_time'];
        $person_list[$k]['server_id'] = $val['server_id'];
        $person_list[$k]['reg_time'] = $val['reg_time'];
        $person_list[$k]['nickname'] = urlencode(str_replace('"', '', $val['nickname']));
        $person_list[$k]['level'] = $val['level'];
    }
}

/**
 * 分区获取
 */
$zone_list = array();
$zone_file = CONFIG_DIR . 'zone.cfg.php';
if (is_file($zone_file)) {
    $re = include $zone_file;
    if (is_array($re)) {
        $arr = array(
        );
        $gid = 0;
        foreach ($arr as $k => $v) {
            if (in_array($channel, $v)) {
                $gid = $k;
                break;
            }
        }
        $gid = $gid ? $gid : $channel_list[$channel]['gid'];
        if (isset($re[$gid])) {
            $zone_list = $re[$gid];
            foreach ($zone_list as &$v) {
                $v['name'] = urlencode($v['name']);
            }
        }
    }
}


echo urldecode(json_encode(array('info' => '1', 'white_list' => $white_list, 'is_pay' => $is_pay, 'time' => time(), 'data' => array('zone_list' => $zone_list, 'server_list' => $data, 'person_list' => $person_list))));
