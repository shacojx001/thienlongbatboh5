<?php
/**
 *
 * 错误上报接口
 */

require '../bs2.php';
require './config.php';
//初始化code
$code = 1;

$records = '"' . preg_replace('/"/', "", request('content')) . '"';//错误内容
$channel = request('channel','str');//平台号
$server_id = request('server_id','int');//服id
$role_id = request('role_id','int');//角色id
$nick_name = request('nick_name');//角色名
$level = request('level','int');//角色等级
$content = request('content');//内容
$time = request('time');
$ticket = request('ticket');//加密
$ip = get_ip();
$db = Ext_Mysql::getInstance('allcenter');


$_time = time();

//加密列表
$encrypt_list = array(
    'level' => $level,
    'nick_name' => $nick_name,
    'role_id' => $role_id,
    'server_id' => $server_id,
    'content' => $content,
    'channel' => $channel,
    'time' => $time,
);
$nick_name = urldecode($nick_name);
$records = urldecode($records);
if (empty($records) || empty($channel)||empty($ticket)||empty($time)) {
    $code = -1;
   
    return_json($code,array());
}

if ($ticket != w_encrypt($encrypt_list)) {
    $code = -7;
   
    return_json($code,array());
}


$cache = Ext_Memcached::getInstance("api");

$db = Ext_Mysql::getInstance('allcenter');

$key = crc32($records);

$cur_num = 0;
if (!$num = $cache->fetch($key)) {
    $sql = "select num from log_client_error where id=$key";
    $res = $db->fetchRow($sql);

    $cur_num = $res['num'] + 1;
    $cache->store($key, $cur_num, 7 * 86400);
} else {

    $cur_num = $num + 1;
    $cache->store($key, $cur_num, 7 * 86400);
}

$nick_name = addslashes($nick_name);
$sql = "REPLACE into `log_client_error`(id, `time`,`records`,`channel`,`ip`,`num`, `role_id`,`server_id`,`nick_name`,`level`) values($key,'$_time',$records,'$channel','$ip', $cur_num,$role_id,$server_id,'$nick_name',$level)";

$success = $db->query($sql);

if ($success == 1) {
    $code = 1;
   
    return_json($code,array());
} else {
    $code = -12;
   
    return_json($code,array());
}