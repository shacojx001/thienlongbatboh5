<?php
/**
 * 插入注册信息
 */
require '../bs2.php';
require 'config.php';

$channel = request('channel','str');//平台ID
$accname = request('accname','str');//平台帐号
$id = request('id','int');//游戏服id
$device_id = request('device_id','str');//设备ID
$os = request('os','str');//系统
$nickname = urldecode(request('nickname'));//角色名
$ticket = request('ticket');//加密
$role_id = request('role_id','int');

// 判断参数完整性
if (empty($role_id) || empty($channel) || empty($accname) || empty($id) || empty($device_id) || empty($os) || empty($nickname) || empty($ticket)) {
    echo urldecode(json_encode(array('info' => '-1', 'data' => urlencode('参数不全'))));
    exit;
}

// 判断服务器器是否正确
if (empty($server_list_info[$id])) {
    echo urldecode(json_encode(array('info' => '-3', 'data' => urlencode('游戏服不存在'))));
    exit;
}
$gid = (int)$server_list_info[$id]['gid'];

// 判断ticket是否正确
$encrypt_list = array('channel' => $channel,
    'accname' => $accname,
    'id' => $id,
    'device_id' => $device_id,
    'os' => $os,
    'nickname' => urlencode($nickname),
    'role_id' => $role_id,
);
if ($ticket != encrypt($encrypt_list)) {
    echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('加密不对'))));
    exit;
}
unset($encrypt_list);

// 限制每分钟的访问次数
$cache = Ext_Memcached::getInstance("api");
if (!$num = $cache->fetch($ticket)) {
    $num = 1;
    $cache->store($ticket, $num, 60);
} else {
    $num = $num + 1;
    $cache->store($ticket, $num, 60);
    if ($num > 2) {
        echo urldecode(json_encode(array('info' => '-6', 'data' => urlencode('访问频繁'))));
        exit;
    }
}

$nickname = addslashes($nickname);
$ip = get_ip();
$time = time();
$db = Ext_Mysql::getInstance('allcenter');
$sql = "insert into `player`(`channel`,`accname`,`last_login_server`,`nickname`,`device_id`,`os`,`reg_time`,`server_id`,`last_login_time`,`ip`,`role_id`,`gid`,`last_login_channel`) values('$channel','$accname','$id','$nickname','$device_id','$os','$time','$id','$time','$ip','$role_id','$gid','$channel')";
$success = $db->query($sql);

if ($success) {
    //设备表统计用
    $sql = "insert ignore into `player_device`(`device_id`,`accname`,`reg_time`,`channel`,`os`,`nickname`,`gid`,`server_id`,`ip`,`role_id`)
            values('$device_id','$accname','$time','$channel','$os','$nickname','$gid','$id','$ip','$role_id')";
    $db->query($sql);
    //帐号表统计用
    $sql = "insert ignore into `player_accname`(`device_id`,`accname`,`reg_time`,`channel`,`os`,`nickname`,`gid`,`server_id`,`ip`,`role_id`)
            values('$device_id','$accname','$time','$channel','$os','$nickname','$gid','$id','$ip','$role_id')";
    $db->query($sql);
    echo urldecode(json_encode(array('info' => '1', 'data' => urlencode('插入成功'))));
    exit;
} else {
    $log_dir = LOG_DIR.'api/';
    if(!is_dir($log_dir)){
        $re = mkdir($log_dir, 0777, true);
    }
    //记录失败日志
    file_put_contents($log_dir.'fail_insert_role.'.date('Ymd'),date('Y-m-d H:i:s').' sql:'.$sql."\n");
    echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('插入失败'))));
    exit;
}	  