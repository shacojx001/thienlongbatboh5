<?php
/**
 * 接口参数配置
 */

// 打印信息
function write_log($text, $type = 'a')
{
    $filename = 'log_usr_api.txt';
    $text = "\r\n++++++++++++++++++++++++++++++++++++++++++\r\n"
        . date('Y-m-d H:i:s') . "--" . time() . "\r\n"
        . print_r($text, true);

    if (@$fp = fopen($filename, $type)) {
        flock($fp, 2);
        fwrite($fp, $text);
        fclose($fp);
        return true;
    } else {
        return false;
    }

}

/**
 * 连接各服后台数据库
 */
function centerdb($sid)
{
    global $all_server_bysid;

    if (!isset($all_server_bysid[$sid])) {
        echo "server is empty";
        exit;
    }
    return Ext_Mysql::getInstance('center', $all_server_bysid[$sid]);
}

/**
 *   PHP请求post
 */
function send_post($url, $post_data)
{

    $postdata = http_build_query($post_data);
    $options = array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-type:application/x-www-form-urlencoded',
            'content' => $postdata,
            'timeout' => 15 // 超时时间（单位:s）
        )
    );
    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    return $result;
}

/**
 * ticket加密(请求字段字母排序拼接加字符串加密) 中文要url_encode
 * @param $data
 * @return string
 */
function encrypt($data)
{
    $encrypt_key = "fynemo123456";

    //按键名排序,ticket参数不参与计算签名
    unset($data['ticket']);
    unset($data['PHPSESSID']);
    unset($data['menuck']);
    //参数键名按字母表顺序排序
    ksort($data);
    //组装签名串
    $ticket_str = '';
    foreach ($data as $key => $value) {
        // $ticket_str .= $ticket_str == '' ? $key.'='.$value : '&'.$key.'='.$value;
        $ticket_str .= '&' . $key . '=' . $value;
    }

    $ticket = md5($ticket_str . $encrypt_key);
    //file_put_contents('./ticket_log.txt', "\r\n" . date('Y-m-d H:i:s') . $ticket_str . $encrypt_key . '---' . $ticket);

// print_r($ticket);exit;	
    return $ticket;
}

function w_encrypt($data){
    //加密key
    $key = '2c41a3ddd23d1a1f711b7f37df4d515a';
    //去掉ticket参数
    unset($data['ticket']);
    //参数键名按字母表顺序排序
    ksort($data);
    //组装签名串 a=1&b=2.key
    $ticket_str = urldecode(http_build_query($data));
    //md5加密
    $ticket = md5($ticket_str . $key);
    //打印日志
    $msg = 'OK';
    if($ticket != $_REQUEST['ticket']){
        $msg = 'FAIL';
    }
    //file_put_contents('./ticket_log.txt', "\r\n" . date('Y-m-d H:i:s') .'|'.$msg.'|'.$_SERVER['REQUEST_URI'].'|ticket_str:'. $ticket_str . $key . '---' . $ticket.'---your ticket:'.$_REQUEST['ticket'],FILE_APPEND);

    return $ticket;
}

$msgCode= array(
    0=>'fail',
    1=>'success',
    -1 =>'missing_parameter',
    -2 =>'frequent_operation',
    -3 =>'signature_error',
    -4 =>'role_cannot_be_found',
    -5 =>'create_order_fail',
    -6 =>'platform_cannot_be_found',
    -7 =>'error_ticket',
    -8 =>'error_parameter_channel',
    -9 =>'error_sdk_request',
    -10 =>'error_server',
    -11 =>'insert_role_fail',
    -12 =>'insert_exception_error',
    -13 =>'card_no_empty',
    -14 =>'time_not_up',
    -15 =>'be_overdue',
    -16 =>'lack_of_rank',
    -17 =>'out_of_commission',
    -18 =>'received_the_same',
    -19 =>'can`t_received',
    -20 =>'error_product_id',
    -21 =>'error_money',
    -22 =>'blacklist',
);

function return_json($code,$data = array(), $msg = ''){
    global $msgCode;
    
    list($log_dir, $url) = get_file_name();
    $log_file = $log_dir.'echo.txt';
    $ip = get_ip();
    $msgM = $msg ? $msg : $msgCode[$code];
    $content = "\r\n++++++++++++++++++++++++++++++++++++++++++\r\n"
        . date('Y-m-d H:i:s') . " -- ".$ip. "\r\n"
        .'code:'.$code. " -- ". 'msg:'.$msgM. "\r\n"
        .'data:'.json_encode($data). "\r\n"
        .'url: '.$url. "\r\n"
        .'?'.urldecode(http_build_query($_POST)).''.urldecode(http_build_query($_GET))
        ."\r\n\r";
    file_put_contents($log_file,$content,FILE_APPEND);    

    echo json_encode(array('info'=>$code,'data'=>$data,'msg'=>$msgM));
    exit();
}

//给渠道接口输出用的
function return_input($data){
    list($log_dir, $url) = get_file_name(false);
    $log_file = $log_dir.'echo.txt';       
    $ip = get_ip();
    $content = "\r\n++++++++++++++++++++++++++++++++++++++++++\r\n"
        . date('Y-m-d H:i:s') . " -- ".$ip. "\r\n"
        .'data:'.$data. "\r\n"
        .'url: '.$url. "\r\n"
        .'?'.urldecode(http_build_query($_POST)).''.urldecode(http_build_query($_GET))
        ."\r\n\r";
    file_put_contents($log_file,$content,FILE_APPEND);    

    echo $data;
    exit();
}

//获取写入路径
function get_file_name($api = true){
    if($api){
        $php_self = substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'],'/')+1);
    }else{
        $php_self = ltrim($_SERVER['PHP_SELF'], "/");
        $php_self = substr($php_self,strpos($php_self, '/')+1);
    }
     
    $php_self = strstr($php_self,'.',true);
    $log_dir = ROOT_DIR."/log/api/".date('Ymd')."/$php_self/";
  
    if(!is_dir($log_dir)){
       mkdir($log_dir, 0777, true);
    }

    $path = parse_url($_SERVER["REQUEST_URI"]);
    $url = $_SERVER['HTTP_HOST'].$path['path'];

    return array($log_dir, $url); 
}


function api_log(){
    global $msgCode;
    list($log_dir, $url) = get_file_name();

    $log_file = $log_dir.'ask.txt';
    $ip = get_ip();

    $content = "\r\n++++++++++++++++++++++++++++++++++++++++++\r\n"
        . date('Y-m-d H:i:s') . "--".$ip. "\r\n"
        .'url: '.$url. "\r\n"
        .'?'.urldecode(http_build_query($_POST)).''.urldecode(http_build_query($_GET))
        ."\r\n\r";
    file_put_contents($log_file,$content,FILE_APPEND);

}

api_log();
