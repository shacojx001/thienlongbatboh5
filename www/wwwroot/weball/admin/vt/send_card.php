<?php
/**
 * 礼包发放接口
 * 2017-11-16
 */
require '../bs2.php';
include_once './config.php';

class auto_sync_internal_role
{
    private $admindb;
    private $allcenter;

    public function __construct()
    {
        $this->admindb = Ext_Mysql::getInstance('admin');
        $this->allcenter = Ext_Mysql::getInstance('allcenter');
    }

    public function sync_internal_role()
    {
        $sid = request('sid', 'int', 'GP');
        $role_id = request('role_id', 'str', 'GP');
        $card_no = request('card_no', 'str', 'GP');
        $uptime = request('time', 'int', 'GP');
        $sign = request('sign', 'str', 'GP');

        //参数为空
        if (empty($sid) || empty($role_id) || empty($card_no) || empty($sign) || empty($uptime)) {
            $this->exit_echo(-1);
        }

        // 限制每分钟的访问次数
        $mk = "LB" . $sign;
        $cache = Ext_Memcached::getInstance("api");
        if (!$num = $cache->fetch($mk)) {
            $num = 1;
            $cache->store($mk, $num, 5);
        } else {
            $this->exit_echo(-2);
            exit;
        }

        $data = array('sid' => $sid,
            'role_id' => $role_id,
            'card_no' => $card_no,
            'time' => $uptime);
//        print_r(encrypt($data));
//        echo "<br />";
//        print_r(encrypt($data1));
        //签名错误
        if (encrypt($data) != $sign) {
            $this->exit_echo(-10);
        }

        $res = $this->get_by_role_id($sid, $role_id);
        //角色不存在
        if (empty($res)) {
            $this->exit_echo(-11);
        }
        $lv = $res['level'];
        $nickname = $res['nick_name'];
        $channel = $res['source'];
        $accname = $res['acc_name'];
        $time = time();

        $sql = "select * from cdkey_list where card_no='$card_no' ";
        $res = $this->allcenter->fetchOne($sql);

        //礼包不存在
        if (empty($res)) {
            $this->exit_echo(-3);
        }
        //先判断可以领取时间，过期时间
        if ($time < $res['can_use_time']) {
            $this->exit_echo(-4);
        }
        //判断失效时间
        if ($res['card_expire'] != 0 && $time > $res['card_expire']) {
            $this->exit_echo(-5);
        }

        //领取等级
        if ($lv < $res['lv_limit']) {
            $this->exit_echo(-6);
        }

        //判断是否同一渠道
        if ($res['channel'] && !in_array($channel, explode(',', $res['channel']))) {
            $this->exit_echo(-7);
        }

        //同账号重复使用0为不可以,1为可以
        $this->the_same_accname($res['repeat_flag'], $res['gift_id'], $accname, $res['interval'], $sid);

        //同角色重复使用0为不可以,1为可以
        $this->the_same_role_id($res['the_same_role_id'], $res['gift_id'], $role_id, $res['interval'], $accname);


        //所有检验都过了
        $id_list = "[" . $role_id . "]";
        $title = "礼包";
        $content = "礼包";
        $post = "[{" . $res['gift_id'] . ",1,1,0}]";

        Erlang::erl_send_mail_new($sid, $id_list, $title, $content, $post);
        //使用后删除该礼包
        if ($res['type'] != 1) {
            $sql = "DELETE FROM cdkey_list WHERE card_no = '$card_no'";
            $this->allcenter->query($sql);
        }

        //插入使用记录
        $sql = "INSERT INTO cdkey_log (
                card_no,
                `interval`,
                card_expire,
                can_use_time,
                card_time,
                gift_id,
                repeat_flag,
                lv_limit,
                channel,
                platf,
                the_same_role_id,
                sid,
                role_id,
                accname,
                nickname,
                time
              )
              VALUES
                (
                  '{$res['card_no']}',
                  '{$res['interval']}',
                  '{$res['card_expire']}',
                  '{$res['can_use_time']}',
                  '{$res['card_time']}',
                  '{$res['gift_id']}',
                  '{$res['repeat_flag']}',
                  '{$res['lv_limit']}',
                  '{$res['channel']}',
                  '{$res['platf']}',
                  '{$res['the_same_role_id']}',
                  '{$sid}',
                  '{$role_id}',
                  '{$accname}',
                  '{$nickname}',
                  '{$time}'
                )";
        $this->allcenter->query($sql);

        $this->exit_echo(1);
        // echo "领取成功";
    }//function end

    public function exit_echo($code)
    {
        $msg = array(
            1 => '成功',
            -1 => '有参数为空',
            -2 => '领取过于频繁',
            -3 => '礼包号不存在',
            -4 => '未到领取时间',
            -5 => '礼包已过期',
            -6 => '您的等级未达到领取该礼包的要求',
            -7 => '该礼包你不能使用',
            -8 => '该类型礼包同一账号只能领取一次',
            -9 => '该类型礼包同一角色只能领取一次',
            -10 => '参数有误',
            -11 => '角色不存在',
            -12 => '该类型礼包同一账号只能同服领取'
        );

        $msgCode = array(
            1 => 'success',
            -1 => 'missing_parameter',
            -2 => 'receive_frequent',
            -3 => 'card_no_empty',
            -4 => 'time_is_up',
            -5 => 'be_overdue',
            -6 => 'not_required',
            -7 => 'out_of_commission',
            -8 => 'received_the_same',
            -9 => 'received_the_same',
            -10 => 'parameter error',
            -11 => 'role is not exit',
            -12 => 'can not received'
        );
        $ip = get_ip();
        //正式服日志
        $log_dir = LOG_DIR . 'api/';
        if (!is_dir($log_dir)) {
            mkdir($log_dir, 0777, true);
        }
        //记录失败日志
        file_put_contents($log_dir . 'send_card_.' . date('Y-m-d'), date('Y-m-d H:i:s') . '`' . $ip . '`' . $code . '`' . $msgCode[$code] . '`' . $msg[$code] . urldecode(http_build_query($_REQUEST)) . "\r\n", FILE_APPEND);
//        file_put_contents('./send_card_.' . date('Y-m-d') . '.txt', date('Y-m-d H:i:s') . '`' . $ip . '`' . $code . '`' . $msgCode[$code] . '`' . $msg[$code] . urldecode(http_build_query($_REQUEST)) . "\r\n", FILE_APPEND);

        exit($msgCode[$code]);
    }


    //同账号重复使用0为不可以,1为可以
    public function the_same_accname($the_same_accname, $gift_id, $accname, $interval, $sid)
    {
        $sql = "select `time`,`sid` from cdkey_log where gift_id='{$gift_id}' and accname='$accname' order by id desc";
        //查询该账号使用该礼包类型记录
        $use_info = $this->allcenter->fetchOne($sql);
        if ($the_same_accname == 1 || $the_same_accname == 2) {
            //存在 使用过
            if ($use_info) {
                switch ($the_same_accname) {
                    case 1: {
                        //上次领取时间限制 同类型使用时间限制(小时)0为无限制
                        if ($interval == 0) {
                            return 1;
                        }
                        //(当前时间-上次使用时间)/3600>限制时间即可
                        $cs = time() - $use_info['time'] - $interval * 3600;
                        if ($cs > 0) {//超过限制时间可领取
                            return 1;
                        } else {//未超过
                            $this->exit_echo(-4);
                        }
                        break;
                    }
                    case 2: {
                        $last_sid = $use_info['sid'];
                        if ($sid == $last_sid) {
                            return 1;
                        } else {
                            $this->exit_echo(-12);
                        }
                        break;
                    }
                }
            } else {//未使用过
                return 1;
            }
        } else {
            if ($use_info) {
                $this->exit_echo(-8);
            } else {
                return 1;
            }
        }
    }//function end


    //同账号重复使用0为不可以,1为可以
    public function the_same_role_id($the_same_role_id, $gift_id, $role_id, $interval, $accname)
    {
        //查询该账号使用该礼包类型记录
        $sql = "select `time` from cdkey_log where gift_id='{$gift_id}' and role_id='$role_id' and accname='$accname' order by id desc";
        $use_info = $this->allcenter->fetchOne($sql);
        if ($the_same_role_id == 1) {
            //存在 使用过
            if ($use_info) {
                //上次领取时间限制 同类型使用时间限制(小时)0为无限制
                if ($interval == 0) {
                    return 1;
                }
                //(当前时间-上次使用时间)/3600>限制时间即可
                $cs = time() - $use_info['time'] - $interval * 3600;
                if ($cs > 0) {//超过限制时间可领取
                    return 1;
                } else {//未超过
                    $this->exit_echo(-4);
                }
            } else {//未使用过
                return 1;
            }
        } else {//
            if ($use_info) {
                $this->exit_echo(-9);
            } else {
                return 1;
            }
        }
    }//function end

    public function get_by_role_id($sid, $role_id)
    {
        global $server_list_info;
        if (!isset($server_list_info[$sid])) {
            $this->exit_echo(-10);
        }
        $sdb = $server_list_info[$sid]['sdb'];
        $db = Ext_Mysql::getInstance("center", $sdb);
        $sql = "select acc_name,level,nick_name,source from role_base where role_id='$role_id'";
        return $db->fetchOne($sql);
    }


}


$syncclass = new auto_sync_internal_role();
$syncclass->sync_internal_role();