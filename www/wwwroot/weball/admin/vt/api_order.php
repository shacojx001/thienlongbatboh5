<?php
/**
 * 下单接口
 * Created by PhpStorm.
 * User: fanweiting
 * Date: 2017/7/25
 * Time: 19:34
 */

include_once '../bs2.php';
include_once './config.php';

$encrypt['sid'] = $sid = request('sid','int');//服ID
$encrypt['role_id'] = $role_id = request('role_id','int'); // 游戏角色id
$encrypt['accname'] = $accname = request('accname','str'); // 帐号
$encrypt['channel'] = $channel = request('channel','str'); // 渠道号
$encrypt['product_id'] = $product_id = request('product_id','str'); //产品id
$encrypt['money'] = $money = request('money','int'); //money 单位分
$encrypt['time'] = $time = request('time','int'); //请求时间
// $encrypt['recharge_id'] = $recharge_id = request('recharge_id','int'); //请求时间
$ticket = request('ticket'); //签名

$type = request('type','int');
$type = 1 == $type ? $type : 0; //1 为测试

//舒适化
$code = 1;
if(empty($accname) || empty($ticket) ||empty($sid)||empty($role_id)||empty($channel)||empty($money)||empty($time)){
    $code = -1;
   
    return_json($code,array());
}

// 限制每分钟的访问次数
$cache = Ext_Memcached::getInstance("user");
if (!$num = $cache->fetch($ticket)) {
    $num = 1;
    $cache->store($ticket, $num, 60);
} else {
    $num = $num + 1;
    $cache->store($ticket, $num, 60);
    if ($num > 10) {
        $code = -2;
      
        return_json($code,array());
    }
}

if ($ticket != w_encrypt($encrypt)) {
    $code = -7;
  
    return_json($code,array());
}

//验证product_id
$products_file = CONFIG_DIR.'product_cfg.php';
if(is_file($products_file)) {
    include $products_file;
    $channel_product_info = $product_list[$channel];
    if($channel_product_info && (!$product_id || !$channel_product_info[$product_id] || $channel_product_info[$product_id]['money'] * 100 != $money)){
        $code = -20;
       
        return_json($code,array());
    }
}

//验证金额
if($money < 100){
    $code = -21;
  
    return_json($code,array());
}



$gid = $channel_list[$channel]['gid'];
if(!$gid){
    $code = -6;
   
    return_json($code,array());
}

$orderId = '';
//查询玩家是否存在
$sql = "select accname acc_name,`name` nick_name,`level` from role_basic where role_id='{$role_id}' and accname='{$accname}'";
$info = centerdb($sid)->fetchOne($sql);

if($info){
    //创建订单
    $gold = $money/100 * 10;
    $orderId = Order::makeOrderNo(8);
    $data = array(
        'gid'=>$gid,
        'sid'=>$sid,
        'channel'=>$channel,
        'role_id'=>$role_id,
        'nickname'=>$info['nick_name'],
        'lv' => $info['level'],
        'accname'=>$info['acc_name'],
        'money' =>$money,
        'gold' => $gold,
        'product_id'=>$product_id,
        'ctime'=>time(),
        'type'=>$type,
    );
    $re = Order::createOrder($orderId,$data);
    if(!$re){
        $code = -5;
        
        return_json($code,array());
    }
}else{
    $code = -4;
   
    return_json($code,array());
}

$data = array();
if($code == 1){
    
    $data = array('order_id'=>$orderId);
}
return_json($code,$data);
