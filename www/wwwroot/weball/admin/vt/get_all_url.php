<?php
/**
 * 初始化接口
 */
require '../bs2.php';

$channel = request('channel');//渠道号
$gid = $channel_list[$channel]['gid'];
$data = array(
    'admin_url' => urlencode("http://183.6.169.170:7511/"),
    'res_url' => urlencode($gid_list_info[$gid]['res_url']),
    'version_url' => urlencode($gid_list_info[$gid]['version_url']),
    'game_id' => $channel_list[$channel]['game_id'],
);

echo urldecode(json_encode($data));
exit;
     