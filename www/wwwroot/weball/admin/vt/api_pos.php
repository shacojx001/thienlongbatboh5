<?php
/**
 * 设备流失
 * User: wtf
 * Date: 18/5/22
 * Time: 下午12:58
 */
require '../bs2.php';
require './config.php';
include LIB_DIR . '/Tools/Redis.php';
$params['channel']  = request('channel','str');//位置
$params['pos']  = request('pos','int');//位置
$params['dev']  = request('dev','str');//设备号
$params['time'] = request('time','int'); //时间戳 10位
$ticket = request('ticket');

foreach ($params as $param) {
    if(empty($params)){
       
        return_json(-1,array());
    }
}

if(!$ticket || w_encrypt($params) != $ticket){
 
    return_json(-7,array());
}

//队列方式入库
//$redis = Redis_Server::getInstance();
//if($redis->get_conn()){
//    $re = $redis->push('api_device_loss_key',$params);
//}else{
//    //数据库方式
//    $re = Config::centerdb()->insert('device_loss',$params);
//}
Config::centerdb()->insert('device_loss',$params);
return_json(1,array());
