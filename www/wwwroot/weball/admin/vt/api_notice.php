<?php
/**
 *
 * 获取公告：
 */
require '../bs2.php';
require './config.php';

//初始化code
$code = 1;


$encrypt['channel'] = $channel = request('channel');//平台ID
$encrypt['time'] = $time = request('time');
$ticket = request('ticket');//加密
if(empty($channel)||empty($time)||empty($ticket)){
    $code = -1;
  
    return_json($code,array());
}

if ($ticket != w_encrypt($encrypt)) {
    $code = -7;
  
    return_json($code,array());
}

// print_r($gid_list);exit;
$cache = Ext_Memcached::getInstance("api");
$key = sprintf(Key::$api['notice'],$channel);
if (!$notice = $cache->fetch($key)) {
    $gid = $channel_list[$channel]['gid'];
    $db = Ext_Mysql::getInstance('allcenter');
    $sql = " select title,content,time from notice where gid=$gid";
    $notice = $db->fetchOne($sql);

    $cache->store($key, $notice, 86400);
}

$data = array(
    'time' => $notice['time'],
    'content' => $notice['content'],
    'title' => $notice['title'],
);

return_json($code,$data);
