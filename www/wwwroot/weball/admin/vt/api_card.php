<?php
/**
 * 礼包发放接口
 * 2017-11-16
 */
require '../bs2.php';
include_once './config.php';

class auto_sync_internal_role
{
    private $admindb;
    private $allcenter;

    public function __construct()
    {
        $this->admindb = Ext_Mysql::getInstance('admin');
        $this->allcenter = Ext_Mysql::getInstance('allcenter');
    }

    public function sync_internal_role()
    {
        $sid = request('sid', 'int', 'GP');
        $role_id = request('role_id', 'str', 'GP');
        $card_no = request('card_no', 'str', 'GP');
        $uptime = request('time', 'int', 'GP');
        $sign = request('ticket', 'str', 'GP');

        //参数为空
        if (empty($sid) || empty($role_id) || empty($card_no) || empty($sign) || empty($uptime)) {
            $code = -1;
           
            return_json($code,array());
        }

        // 限制每分钟的访问次数
        $mk = "LB" . $sign;
        $cache = Ext_Memcached::getInstance("api");
        if (!$num = $cache->fetch($mk)) {
            $num = 1;
            $cache->store($mk, $num, 5);
        } else {
            $code = -2;
            
            return_json($code,array());
        }

        $data = array('sid' => $sid,
            'role_id' => $role_id,
            'card_no' => $card_no,
            'time' => $uptime);

        //签名错误
        if (w_encrypt($data) != $sign) {
            $code = -7;
            
            return_json($code,array());
        }

        $res = $this->get_by_role_id($sid, $role_id);
        //角色不存在
        if (empty($res)) {
            $code = -4;
            
            return_json($code,array());
        }
        $lv = $res['level'];
        $nickname = $res['nick_name'];
        $channel = $res['source'];
        $accname = $res['acc_name'];
        $time = time();

        $sql = "select * from cdkey_list where card_no='$card_no' ";
        $res = $this->allcenter->fetchOne($sql);

        //礼包不存在
        if (empty($res)) {
            $code = -13;
            
            return_json($code,array());
        }
        //先判断可以领取时间，过期时间
        if ($time < $res['can_use_time']) {
            $code = -14;
           
            return_json($code,array());
        }
        //判断失效时间
        if ($res['card_expire'] != 0 && $time > $res['card_expire']) {
            $code = -15;
            
            return_json($code,array());
        }

        //领取等级
        if ($lv < $res['lv_limit']) {
            $code = -16;
          
            return_json($code,array());
        }

        //判断是否同一渠道
        if ($res['channel'] && !in_array($channel, explode(',', $res['channel']))) {
            $code = -17;
          
            return_json($code,array());
        }

        //同账号重复使用0为不可以,1为可以
        $this->the_same_accname($res['repeat_flag'], $res['gift_id'], $accname, $res['interval'], $sid,$res['cgift_type']);

        //同角色重复使用0为不可以,1为可以
        $this->the_same_role_id($res['the_same_role_id'], $res['gift_id'], $role_id, $res['interval'], $accname,$res['cgift_type']);

        $gf = $this->admindb->fetchOne("select item from base_config where id = ".$res['gift_id']." and type = 3" );

        if(empty($gf)){
            $gf['item'] = '礼包';
        }
        //所有检验都过了
        $id_list = "[" . $role_id . "]";
        $title = $gf['item'];
        $content = $gf['item'];
        
        $card_expire = $res['card_expire'] ? $res['card_expire']:0;
        $post = "[{5,[{".$res['gift_id'].",1,1,".$card_expire."}]}]";

//        print_r($post);   [{5,[{16010001,1,1,0}]}]
        Erlang::erl_send_mail_new($sid, $id_list, $title, $content, $post);
        //使用后删除该礼包
        if ($res['type'] != 1) {
            $sql = "DELETE FROM cdkey_list WHERE card_no = '$card_no'";
            $this->allcenter->query($sql);
        }

        //插入使用记录
        $sql = "INSERT INTO cdkey_log (
                card_no,
                `interval`,
                card_expire,
                can_use_time,
                card_time,
                gift_id,
                repeat_flag,
                lv_limit,
                channel,
                platf,
                the_same_role_id,
                sid,
                role_id,
                accname,
                nickname,
                time,
                cgift_type
              )
              VALUES
                (
                  '{$res['card_no']}',
                  '{$res['interval']}',
                  '{$res['card_expire']}',
                  '{$res['can_use_time']}',
                  '{$res['card_time']}',
                  '{$res['gift_id']}',
                  '{$res['repeat_flag']}',
                  '{$res['lv_limit']}',
                  '{$res['channel']}',
                  '{$res['platf']}',
                  '{$res['the_same_role_id']}',
                  '{$sid}',
                  '{$role_id}',
                  '{$accname}',
                  '{$nickname}',
                  '{$time}',
                  '{$res['cgift_type']}'
                )";
        $this->allcenter->query($sql);

        $code = 1;
      
        return_json($code,array());
        // echo "领取成功";
    }//function end

    //同账号重复使用0为不可以,1为可以
    public function the_same_accname($the_same_accname, $gift_id, $accname, $interval, $sid,$cgift_type)
    {
        $sql = "select `time`,`sid` from cdkey_log where gift_id='{$gift_id}' and accname='$accname' and cgift_type = $cgift_type order by id desc";
        //查询该账号使用该礼包类型记录
        $use_info = $this->allcenter->fetchOne($sql);
        if ($the_same_accname == 1 || $the_same_accname == 2) {
            //存在 使用过
            if ($use_info) {
                switch ($the_same_accname) {
                    case 1: {
                        //上次领取时间限制 同类型使用时间限制(小时)0为无限制
                        if ($interval == 0) {
                            return 1;
                        }
                        //(当前时间-上次使用时间)/3600>限制时间即可
                        $cs = time() - $use_info['time'] - $interval * 3600;
                        if ($cs > 0) {//超过限制时间可领取
                            return 1;
                        } else {//未超过
                            $code = -14;
                         
                            return_json($code,array());
                        }
                        break;
                    }
                    case 2: {
                        $last_sid = $use_info['sid'];
                        if ($sid == $last_sid) {
                            return 1;
                        } else {
                            $code = -19;
                          
                            return_json($code,array());
                        }
                        break;
                    }
                }
            } else {//未使用过
                return 1;
            }
        } else {
            if ($use_info) {
                $code = -18;
           
                return_json($code,array());
            } else {
                return 1;
            }
        }
    }//function end


    //同账号重复使用0为不可以,1为可以
    public function the_same_role_id($the_same_role_id, $gift_id, $role_id, $interval, $accname,$cgift_type)
    {
        //查询该账号使用该礼包类型记录
        $sql = "select `time` from cdkey_log where gift_id='{$gift_id}' and role_id='$role_id' and accname='$accname' and cgift_type=$cgift_type order by id desc";
        $use_info = $this->allcenter->fetchOne($sql);
        if ($the_same_role_id == 1) {
            //存在 使用过
            if ($use_info) {
                //上次领取时间限制 同类型使用时间限制(小时)0为无限制
                if ($interval == 0) {
                    return 1;
                }
                //(当前时间-上次使用时间)/3600>限制时间即可
                $cs = time() - $use_info['time'] - $interval * 3600;
                if ($cs > 0) {//超过限制时间可领取
                    return 1;
                } else {//未超过
                    $code = -14;
                 
                    return_json($code,array());
                }
            } else {//未使用过
                return 1;
            }
        } else {//
            if ($use_info) {
                $code = -18;
            
                return_json($code,array());
            } else {
                return 1;
            }
        }
    }//function end

    public function get_by_role_id($sid, $role_id)
    {
        global $server_list_info;
        if (!isset($server_list_info[$sid])) {
            $code = -1;
       
            return_json($code,array());
        }
        $sdb = $server_list_info[$sid]['sdb'];
        $db = Ext_Mysql::getInstance("center", $sdb);
        $sql = "select b.accname acc_name,level,b.name nick_name,source from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id where b.role_id='$role_id'";
        
        return $db->fetchOne($sql);
    }


}


$syncclass = new auto_sync_internal_role();
$syncclass->sync_internal_role();