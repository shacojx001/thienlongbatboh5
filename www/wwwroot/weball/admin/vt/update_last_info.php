<?php
/**
 * 更新玩家最后登陆信息
 * 日期 2017-10-30
 * wtf
 */
require '../bs2.php';
require './config.php';

$role_id = request('role_id','int'); // 角色ID
$channel = request('channel','str'); // 平台号
$accname = request('accname','str'); // 平台帐号
$last_login_server = request('last_login_server','int'); // 最后登录游戏服
$server_id = request('server_id','int'); // 注册游戏服
$level = request('level','int');
$nickname = urldecode(request('nickname'));
$ticket = request('ticket');       // 加密
$ip = get_ip();
// 判断参数完整性
if(empty($role_id) || empty($channel) || empty($accname)|| empty($server_id) || empty($last_login_server)|| empty($nickname) ||empty($ticket)){
    echo urldecode(json_encode(array('info'=>'-1','data'=>urlencode ('参数不全'))));
    exit;
}

// 判断ticket是否正确

$encrypt_list = array('role_id'=>$role_id,
                      'channel'=>$channel,
                      'accname'=>$accname,
                      'last_login_server'=>$last_login_server,
                      'server_id'=>$server_id,
                      'level'=>$level,
                      'nickname'=>urlencode($nickname),
                    );
if($ticket != encrypt($encrypt_list)){
    echo urldecode(json_encode(array('info'=>'-5','data'=>urlencode ('加密不对'))));
    exit;
}
unset($encrypt_list);
   
// 限制每分钟的访问次数
$cache = Ext_Memcached::getInstance("api");
if(!$num = $cache->fetch($ticket)) {  
    $num = 1;
    $cache->store($ticket, $num, 60);
}else{
    $num = $num + 1;
    $cache->store($ticket, $num, 60);
    if($num > 5) {
      echo urldecode(json_encode(array('info'=>'-6','data'=>urlencode ('访问频繁'))));
      exit;
    }
}

//更新角色
$nickname = addslashes($nickname);
$now_time = time();
$sql = "update player set last_login_time=$now_time,last_login_server='$last_login_server',ip='$ip',`level`= $level,`nickname` = '$nickname' where  accname='$accname' and `server_id`='$server_id' and role_id='$role_id'";
$db = Ext_Mysql::getInstance('allcenter');
$success = $db->query($sql);
if($db->getAffectRows()>0){
    //更新缓存
    $key = sprintf(Key::$api['server_list'],$accname);
    $player = $cache->fetch($key);
    if(!$player){
        $sql = "select role_id,accname,last_login_server,last_login_time,`nickname`,server_id,reg_time,`level`,channel from player where accname='$accname' order by last_login_time desc";
        $player = $db->fetchRow($sql);

        $cache->store($key, $player, 3600);
    }else{
        foreach ($player as $k => $v){
            if($v['server_id'] == $server_id && $v['accname'] == $accname && $v['role_id']==$role_id){
                $now_time = time();
                $player[$k]['last_login_server'] = "$last_login_server";
                $player[$k]['last_login_time'] = "$now_time";
                $player[$k]['level'] = "$level";
                $player[$k]['ip'] = "$ip";
                $player[$k]['nickname'] = "$nickname";
            }
        }
        $cache->store($key, $player, 3600);
    }
    echo urldecode(json_encode(array('info'=>'1','data'=>urlencode ('更新成功'))));exit;
}else{
    echo urldecode(json_encode(array('info'=>'1','data'=>urlencode ('没有记录更新'))));exit;
}



  



   
   
