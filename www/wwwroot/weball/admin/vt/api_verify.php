<?php
/**
 * 验证code接口
 * User: wtf
 * Date: 17/12/5
 * Time: 下午3:26
 */
require '../bs2.php';
require './config.php';

//初始化code
$code = 1;
/*
 * 获取参数
 */
$data['token'] = $token = request('token');//从游戏客户端拿到的code ,有效期10分钟 ,code使用一次即失效
$data['game_id'] = $game_id = request('game_id');
$data['channel'] = $channel = request('channel');
$data['time'] = $time = request('time');
$ticket = request('ticket');

/**
 * 验证参数
 */
if(!$token || !$channel||!$time){
    $code = -1;
  
    return_json($code,array());
}

if(w_encrypt($data) != $ticket){
    $code = -7;
   
    return_json($code,array());
}

$sdk_mark = $channel_list[$channel]['sdk_mark']?$channel_list[$channel]['sdk_mark']:'';
$sdk_key = $channel_list[$channel]['pay_key']?$channel_list[$channel]['pay_key']:'';
$sdk_url = $channel_list[$channel]['sdk_url']?$channel_list[$channel]['sdk_url']:'';
$sdk_file = './'.$sdk_mark.'/api_verify.php';
if(!is_file($sdk_file)){
    $code = -19;
    
    return_json($code,array());
}

$re = $accname = $third_accname = '';
$data = array();
/*
 * 在各自文件里做sdk验证
 */
include_once $sdk_file;
if($code == 1){
    $data = array('accname'=>$accname,'third_accname'=>$third_accname);
}

return_json($code,$data);