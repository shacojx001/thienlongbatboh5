<?php
/**
 * 
 * 头像上传
 */

include_once '../../bs2.php';

include_once '../config.php';

file_put_contents('head_photo_upload.txt',$_REQUEST['picture']);

list($server_id, $role_id, $ticket, $filename, $path, $picture) = get_request();

$post = array(
            'path'      => $path,
            'filename'  => $filename,
            'picture'   => $picture,
	    );

$url = PIC_WEB.'upload.php';

$res = send_post($url,$post);

if(is_numeric($res)){
	  insert_center($server_id, $role_id, $path.$filename);			
   echo 'ok';exit;
}else{
   echo $res;exit;
}

//入库
function  insert_center($server_id, $role_id, $path)
{	
	global $server_list_info;
	$serverInfo = $server_list_info[$server_id];
	$gid = $serverInfo['gid'];
	$state = PIC_AUDIT;
	$time = time();
	
	// $msg = json_encode(Erlang::erl_user_icon($server_id, $role_id, $state));

	$sql = "REPLACE INTO `tdnemo_admin_dev`.`center_role_photo` (
				`gid`,
				`sid`,
				`role_id`,
				`state`,
				`url`,
				ctime,
				msg
			)
			VALUES
				(
					{$gid},
					{$server_id},
					'{$role_id}',
					{$state},
					'{$path}',
					$time,
					'{$msg}'
				);";			
	Ext_Mysql::getInstance('allcenter')->query($sql);
	//
				
	return true;
}

//获取参数
function get_request()
{
	$post = array('platform', 'server_id', 'role_id', 'ticket', 'picture');
	foreach ($post as $key => $val) {
		if(empty($_REQUEST[$val])){
			echo "$val is empty";exit;
		}
	}
	if(get_ticket($_REQUEST) != $_REQUEST['ticket']){
		echo 'ticket error';exit;
	}
	if(strlen($_REQUEST['picture'])<10){
		echo 'picture is empty';exit;
	}
	// 2 审核；3 审核通过
	$path = ($_REQUEST['role_id']%100).'/';
	$data = array(
			$_REQUEST['server_id'],
			$_REQUEST['role_id'],
			$_REQUEST['ticket'],
			$_REQUEST['role_id'].'.jpg',
			$path,
			base64_decode($_REQUEST['picture']),
		);
	//测试
	// $data = array(
	// 		$_REQUEST['server_id'],
	// 		$_REQUEST['role_id'],
	// 		$_REQUEST['ticket'],
	// 		$_REQUEST['role_id'].'.jpg',
	// 	 	$path,
	// 		base64_decode(file_get_contents("a.txt")),
	// 	);
	

	return $data;
}

//加密
function get_ticket($post)
{
	return md5($post['platform'].$post['server_id'].$post['role_id'].'xqjnemo123456');
}

//end


