<?php
/**
 * 
 * 图片上传
 */

list($path, $filename, $picture) = get_request();

if(!is_dir($path)){
  mkdir($path,0770,true);
}

echo create_img($path, $filename, $picture);
exit;

//创建图片
function create_img($path, $filename, $picture)
{   
    $imgUrl = $path.$filename;
    //文件写入 
    $file = fopen($imgUrl, "w");//打开文件准备写入 
    $result = fwrite($file, $picture);//写入 
    fclose($file);//关闭
    show($imgUrl);
    return $result;
}

//修改成大小图
function show($imgUrl)
{
    $imgUrlMin = str_replace('.jpg', '_min.jpg', $imgUrl);
    //小图
    resizejpg($imgUrl, $imgUrlMin, 100, 100);
    //大图
    resizejpg($imgUrl, $imgUrl, 320, 320);
}


//获取参数
function get_request()
{
    $post = array('path', 'filename', 'picture');
    foreach ($post as $key => $val) {
     if(empty($_REQUEST[$val])){
         echo "$val is empty";exit;
     }
    }
    $data = array(
        $_REQUEST['path'],
        $_REQUEST['filename'],
        $_REQUEST['picture'],
    );
  //测试
  // $data = array(
  //       'wxnemo/',
  //       '17779008633408.jpg',
  //       base64_decode(file_get_contents("b.txt")),
  //   );
  return $data;
}

//图片大小处理
function resizejpg($imgsrc,$imgdst,$imgwidth,$imgheight)
{ 
    //$imgsrc jpg格式图像路径 $imgdst jpg格式图像保存文件名 $imgwidth要改变的宽度 $imgheight要改变的高度
    //取得图片的宽度,高度值
    $arr = getimagesize($imgsrc);                     
    // header("Content-type: image/jpg");
    
    $imgWidth = $imgwidth;
    $imgHeight = $imgheight;
    // Create image and define colors
    $imgsrc = imagecreatefromjpeg($imgsrc);
    $image = imagecreatetruecolor($imgWidth, $imgHeight);  //创建一个彩色的底图  
    imagecopyresampled($image, $imgsrc, 0, 0, 0, 0,$imgWidth,$imgHeight,$arr[0], $arr[1]);
    ///$image 图片资源  $imgdst 图片保存路径 
    imagejpeg($image, $imgdst);
    imagedestroy($image);
}


//同步云服务器
function put($key){
  //存储空间名
  $bucket = "fengyun2";
  //上传至存储空间后的文件名称(请不要和API公私钥混淆)
  $key    = $key;
  //待上传文件的本地路径
  $file   = $key;

  //该接口适用于0-10MB小文件,更大的文件建议使用分片上传接口
  list($data, $err) = UCloud_PutFile($bucket, $key, $file);
  if ($err) {
    return  $key ." :error: " . $err->ErrMsg . "\n code: " . $err->Code;
  }
  return $key." :ETag: " . $data['ETag'] . "\n";
}