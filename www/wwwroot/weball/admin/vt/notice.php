<?php
/**
 *
 * 获取公告：
 */
require '../bs2.php';

require 'config.php';

$channel = request('channel');//平台ID
$ticket = request('ticket');//加密
// 写入缓存start    

//加密列表
$encrypt_list = array(
    'channel' => $channel,
);

if ($ticket != encrypt($encrypt_list)) {
    echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('加密不对'))));
    exit;//加密不对
}

unset($encrypt_list);
// print_r($gid_list);exit;
$cache = Ext_Memcached::getInstance("api");

$key = sprintf(Key::$api['notice'],$channel);

if (!$notice = $cache->fetch($key)) {

    $gid = $gid_list[$channel]['gid'];

    $db = Ext_Mysql::getInstance('allcenter');
    $sql = " select title,content,time from notice where gid=$gid";
    $notice = $db->fetchOne($sql);

    $cache->store($key, $notice, 86400);
}

$data = array(
    'time' => $notice['time'],
    'content' => urlencode($notice['content']),
    'title' => urlencode($notice['title']),
);


echo urldecode(json_encode(array('info' => '1', 'data' => $data)));
