<?php
/**
 * 获取服务器列表：
 */
require '../bs2.php';
require './config.php';

//初始化code
$code = 1;



$encrypt['channel'] = $channel = request('channel');//渠道号
$encrypt['accname'] = $accname = request('accname');//平台帐号

$encrypt['ticket'] = $ticket = request('ticket');//加密
$encrypt['device_id'] = $device_id = request('device_id');//设备号
$encrypt['time'] = $time = request('time');//请求时间戳

foreach ($encrypt as $key => $val) {
    if(empty($val)){
        $code = -1;
        return_json($code,array(), $key.' empty');
    }
}

$encrypt['ios_version'] = $ios_version = request('ios_version');//审核版本号

if(w_encrypt($encrypt) != $ticket){
    $code = -7;
    return_json($code,array());
}

//白名单，黑名单
list($white_list, $msg) = ban_login();
if($white_list == -1){
    $code = -22;
    return_json($code, array(), $msg);
}

//是否IOS审核服
if ($ios_version && $ios_version == $channel_list[$channel]['ios_version']) {
    $data = get_server_shenheInfo();

} else {//非IOS审核
    $sid_list = array();
    foreach ($server_list_info as $key => $val) {
        $arr = explode(',',$val['channel_list']);
        if (in_array($channel,$arr)){
            $sid_list[$key] = $val;
        }
    }
    $data = array();
    //判断是否测试名单
    $time = time();
    foreach ($sid_list as $key => $val) {
        if (check_white_serverList($white_list, $val, $time)) {
            $data[$key] = get_server_info($val);
        }
    }       
}
/**
 * 角色列表获取
 */
$person_list = get_player_list($accname);
/**
 * 分区获取
 */
$zone_list = get_zone_list($sid_list, $channel);

$data = array('is_white'=>$white_list,'zone_list' => $zone_list, 'server_list' => $data, 'person_list' => $person_list);

//专家登录
// send_mail($accname,get_ip(),$channel);

return_json($code,$data);

//分区处理
function get_zone_list($sid_list, $channel){
    $zone_list = array();
    $zone_file = CONFIG_DIR . 'zone.cfg.php';
  
    if (is_file($zone_file)) {
        $re = include $zone_file;
        $zone_list_data = $re[$channel] ? $re[$channel] : $re['tpl'];
        // print_r($zone_list_data);exit;    
        if($zone_list_data){
            // 过滤已选中结果
            $server_key_list = get_zone_array($zone_list_data, $sid_list);
            // print_r($server_key_list);exit;   
            $num_key = 1;
            foreach ($zone_list_data as $key => $val) {
                if(is_array($val)) continue;
                if(count($server_key_list)==0){
                    unset($zone_list_data[$key]);
                    continue;
                }
                
                if(strstr($key, '%')){//%拆分区
                    foreach (array_chunk($server_key_list, $val) as $kk => $vv) {
                        $num_key_tk = str_replace('%', $num_key, $key);
                        $zone_list_data[$num_key_tk] = $vv;
                        $num_key = $num_key + 1;
                    }
                    unset($server_key_list);
                    unset($zone_list_data[$key]);
                }else{
                    $zone_list_data[$key] = array_slice($server_key_list, 0 , $val, true);
                    array_splice($server_key_list, 0, $val);
                }
                
            }

            //客户端要求的处理  
            foreach ($zone_list_data as $key => $val) {
                $zone_list[] = array(
                                'name'=>$key,
                                'list'=>$val,
                                );
            }
            //客户端要求的处理  
        }
        
        
    }
    // $zone_list = array();
    return $zone_list;
}

//抽出分组里边的数组
function get_zone_array($zoneData, $sid_list){
    $filter_array = array();
    foreach ($zoneData as $key => $val) {
        if(is_array($val)){
            $filter_array = array_merge($filter_array,$val);
        }
        
    }
    if(count($filter_array)>0){
        foreach ($filter_array as $key => $val) {
            unset($sid_list[$val]);
        }
    }
    return array_keys($sid_list);
}

//白名单服务器列表判断
function check_white_serverList($white_list, $val, $time)
{   
    if( $white_list == 1 ){
        $result = ($val['default'] == 1 || $val['default'] == 0 || $val['default'] == 4 || $val['default'] == 6);
    } else {  
        $result = !($val['visible_time'] >= $time) && ($val['default'] == 1 || $val['default'] == 4 || $val['default'] == 6);
    }
    return $result;
}

//游戏服列表
function get_server_info($val)
{
    $data = array(
            'server_id' => $val['id'],
            'description' => $val['description'],
            'game_ip' => $val['game_ip'],
            'game_port' => $val['game_port'],
            'otime' => $val['otime'],
            'visible_time' => $val['visible_time'],
            'hot_state' => $val['hot_state'],
            'recom_state' => $val['recom_state'],
            'default' => $val['default'], //状态
            'stop_service_describe' => $val['stop_service_describe'], //停服描述
        );
    return $data;
}

//审核服
function get_server_shenheInfo()
{
    $data = array(
        '1' => array(
            "id" => "104",
            "description" => "天下无双",
            "game_ip" => "jyjh.ios.gznemo.com",
            "game_port" => "7510",
            "otime" => "1467873474",
            "visible_time" => "1471957001",
            "hot_state" => "0",
            "recom_state" => "0",
            "default" => "1",
            "stop_service_describe" => "",
            "server_num" => "1",
            "shenhe" => "1",
        ),
    );
    return $data;
}

//获取角色列表
function get_player_list($accname)
{
    $cache = Ext_Memcached::getInstance("api");
    $key = sprintf(Key::$api['server_list'],$accname);
    if (!$player = $cache->fetch($key)) {
        $db = Ext_Mysql::getInstance('allcenter');
        $sql = "select role_id,accname,last_login_server,last_login_time,REPLACE(nickname,'\"','') nickname,server_id,reg_time,`level`,channel from player where accname='$accname' order by last_login_time desc";
        $player = $db->fetchRow($sql);
        $cache->store($key, $player, 3600);
    }
    return $player;
}

//白名单，黑名单
function ban_login()
{   
    $ban_file = CONFIG_DIR . 'ban.cfg.php';
    $code = 0;
    $msg = '';
    if (is_file($ban_file)) {
        include_once $ban_file;
        $channel = request('channel');
        $accname = request('accname');
        $device_id = request('device_id');
        $ip = get_ip();
        // 0 正常；1 名单；-1 黑名单
        
        //账号白名单
        if (isset($accname_white_list[$channel]) && in_array($accname,$accname_white_list[$channel])) {
            $code = 1;
        } 
        //IP白名单
        if(isset($ip_white_list) && in_array($ip, $ip_white_list)){
            $code = 1;
        }
        //IP黑名单
        if(isset($ip_ban_list) && in_array($ip, $ip_ban_list)){
            $code = -1;
            $msg = "IP：$ip 禁止登录";
        }
        //设备黑名单
        if(isset($device_ban_list) && in_array($device_id, $device_ban_list)){
            $code = -1;
            $msg = "设备：$device_id 禁止登录";
        }
        
    }
    return array($code, $msg);
}

//专家服发邮件用的
function send_mail($accname, $ip, $channel){
        if($ip=='124.207.11.41'){
            return false;
        }
        $title = '专家服登录';
        $content = "当前时间：".date('Y-m-d H:i:s',time())."<br/><br/><br/>";

        $content .= '<table>
                    <tr>
                        <td bgcolor="#dde7ee" nowrap="nowrap"><b>账号</b></td>
                        <td bgcolor="#dde7ee" nowrap="nowrap"><b>IP</b></td>
                        <td bgcolor="#dde7ee" nowrap="nowrap"><b>渠道号</b></td>
                    </tr>';
        $content .= '<tr bgcolor="#FFFFFF">
                                    <td nowrap="nowrap">'.$accname.'</td>
                                    <td nowrap="nowrap">'.$ip.'</td>
                                    <td nowrap="nowrap">'.$channel.'</td>
                                </tr>';
        $content .= '</table>';

        $receive_user  = array(
            '138118901@qq.com',
            '3003876087@qq.com',
            '3003803199@qq.com',
        );
        $rootPath = dirname(dirname(dirname(__FILE__)));
        require $rootPath . '/lib/Ext/PHPMailer/sendEmail.php';

        $sendEmail = new sendEmail();
        $ste = $sendEmail->send(
            'smtp.exmail.qq.com',
            465,
            $title,
            $content,
            $receive_user,
            "cjx@gznemo.com",
            "JgapmojtEa4FgHS2",
            $title,
            'ssl'
        );
        return $ste;
    }
