<?php
/**
 *  封号禁言接口
 *
    接口地址 http://manager.xqj.linygame.com//wx/linyou/api_ban.php
    POST方式
    参数
    type:
        limit_login(封号)
        unlimit_login(解封号)
        limit_talk(禁言)
        unlimit_talk(解禁言)
    返回结果:
    ok成功,其他为失败
    sid:20001 服id
    role_id:123 角色id
    second: 10  禁言秒数 int type等于limit_talk/unlimit_talk时有效,其他为空
    sign: abc 签名 key=11aec243c8007acb6285d5c98c87e0df

    PHP签名说明：
    $params = $_POST;
    unset($params['sign']);
    ksort($params);
    sign=md5(http_build_query($params) . $key);
 *
 *
 */
include(dirname(dirname(dirname(dirname(__FILE__)))) . "/admin/action/superclass.php");
require '../../bs2.php';
require '../config.php';

class ban extends action_superclass
{

    public function __construct()
    {
    }

    /**
     * 执行
     */
    function run(){
        $code = 0;
        $msg = array(
            1=>'ok',
            -1=>'缺少参数',
            -2=>'签名错误',
            -3=>'操作类型错误',
            -4=>'找不到该服',
            -5=>'封禁失败',
            -6=>'ip格式错误',
        );
        $key = "11aec243c8007acb6285d5c98c87e0df";
        $sign = $_POST['sign'];
        $p['sid'] = $_POST['sid'];
        $p['role_id'] = $_POST['role_id'];
        $p['type'] = $_POST['type'];
        $p['second'] = $_POST['second'];

        if (!$p['sid']) {
            $code = -1;
        }
        if($code == 0){
            $role_id = intval($p['role_id']);
            $sid = $p['sid'];
            $p = array_filter($p);
            ksort($p);
            if(md5(urldecode(http_build_query($p)).$key) != $sign){
                $code = -2;
            }
        }
        if($code == 0){
            switch( $p['type'] ){
                case 'limit_login'://封号
                    if (!$p['role_id']) {
                        $code = -1;
                        break;
                    }

                    $result = Erlang::limit_login(array($role_id),$sid);
                    if($result){
                        $code = 1;
                    }else{
                        $code = -5;
                    }
                    break;
                case 'unlimit_login'://解封号
                    if (!$p['role_id']) {
                        $code = -1;
                        break;
                    }

                    $result = Erlang::unlimit_login(array($role_id),$sid);
                    if($result){
                        $code = 1;
                    }else{
                        $code = -5;
                    }
                    break;
                case 'limit_talk'://禁言
                    if (!$p['second']||!$p['role_id']) {
                        $code = -1;
                        break;
                    }
                    $time = $p['second'];
                    $time =intval($time);

                    $result = Erlang::talk_limit($role_id,$time,$sid);

                    if($result){
                        $min = (int)($time/60);
                        $description = "麟游接口禁言:对玩家禁言{$min}分钟";
                        $t = time();
                        $sql = "insert into log_ban(type,object,description,time,admin)values(3,'$role_id','$description','$t','接口')";
                        Config::gamedb($sid)->query($sql);

                        $code = 1;
                    }else{
                        $code = -5;
                    }
                    break;
                case 'unlimit_talk'://解禁言
                    if (!$p['role_id']) {
                        $code = -1;
                        break;
                    }

                    $result = Erlang::unlimit_talk($role_id,$sid);
                    if($result){
                        $description = "麟游接口解除禁言:$role_id";
                        $t = time();
                        Config::gamedb($sid)->query("insert into log_ban(type,object,description,time,admin)values(4,'$role_id','$description','$t','接口'");
                        $code = 1;
                    }else{
                        $code = -5;
                    }
                    break;

                default:
                    $code = -3;
                    break;
            }
        }
        //$ip = get_ip();
        //file_put_contents(LOG_DIR.'ban/'.date('Ymd').".txt",date('Y-m-d H:i:s').'`'.$ip.'`'.$code.'`'.$msg[$code].'`'.$p['type'].'`'.urldecode(http_build_query($_POST))."\r\n",FILE_APPEND);

        exit($msg[$code]);
    }



}

$syncclass = new ban();
$syncclass->run();