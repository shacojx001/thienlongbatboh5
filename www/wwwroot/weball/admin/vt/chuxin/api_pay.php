<?php
/**
 * 回调通知接口
 * Created by PhpStorm.
 * User: fanweiting
 * Date: 2017/7/25
 * Time: 19:34
 */
include_once '../../bs2.php';
include_once '../config.php';


/* orderId=3180128&game=xqj&amount=600&uid=01dfc31d5584169e1828dbf186a06cdb&zone=1&goodsId=&payTime=1513764204&payChannel=&payExt=821620641503170026&sign=6e922da372e93be9ddf946ed66fa96c4
game
orderId 订单号
amount 商品总价 分
uid 用户标识
zone 区服ID
goodsId  商品ID
payTime 平台支付完成时间
payChannel 支付方式 支付宝
payExt 扩展信息
sign md5(game+orderId+amount+uid+zone+goodsId+payTime+payChannel+payExt+’#’+secretKey)
*/
$secretKey = '3b90c4c4fdbe8a6a905dadc9e279c389';
$data['game'] = $game = request('game');
$data['orderId'] = $orderId = request('orderId');
$data['amount'] = $amount = request('amount');
$data['uid'] = $uid = request('uid');
$data['zone'] = $zone = request('zone');
$data['goodsId'] = $goodsId = request('goodsId');
$data['payTime'] = $payTime = request('payTime');
$data['payChannel'] = $payChannel = request('payChannel');
$data['payExt'] = $payExt = request('payExt');
$sign = request('sign');

//初始化code
$code = 0;
$msgCode = array(
    1=>'success',
    -1 =>'missing_parameter',
    -2 =>'signature_error',
    -3 =>'check_order_fail1',
    -4 =>'check_order_fail2',
    -5 =>'check_order_fail3',
    -6 =>'server_cannot_be_found',
    -7 =>'order_insert_fail',
    -8 =>'ip limit',
    -9 =>'uid or role error',
    -10 => 'error_money',
);



//校验参数
if(empty($game)||empty($orderId)||empty($amount)||empty($uid)||empty($zone)||empty($payTime)||empty($payExt)||empty($sign)){
    $code = -1;
}

//验证签名
if($code == 0){
    $_sign_str = "{$game}{$orderId}{$amount}{$uid}{$zone}{$goodsId}{$payTime}{$payChannel}{$payExt}#{$secretKey}";
    $_sign = md5($_sign_str);
    if($sign != $_sign){
        $code = -2;
    }
}
if($code == 0){
    //验证订单
    $ptime = intval($payTime);
    $order_id = addslashes($orderId);
    $cp_orderId = $payExt;
    if(!preg_match("/^\d*$/",$cp_orderId)){
        $code = -4;
    }

    //游戏业务逻辑
    if($code == 0 ){
        $sql = "SELECT order_id,sid,channel,role_id,nickname,accname,lv,money,gold,state,product_id,`type` FROM `order` where order_id='{$cp_orderId}'";
        $info = Ext_Mysql::getInstance('allcenter')->fetchOne($sql);
        if(!$info){
            $code = -5;
        }

        if($code == 0 && $info['state'] == 1){
            $code = 1;
            Erlang::erl_charge($info['sid'],$order_id);
        }

        if($code == 0 && !isset($all_server_bysid[$info['sid']])) {
            $code = -6;
        }

        if($code == 0 && ($uid != $info['accname'])){
            $code = -9;
        }

        if($code == 0 && $amount != $info['money']){
            $code = -10;
        }
  
        if($code == 0){
            //查询游戏服订单是否重复
            $sql = "SELECT count(pay_no) AS num FROM charge WHERE pay_no = '{$order_id}'";
            $is_save = centerdb($info['sid'])->fetchOne($sql);
 
            //更新字段
            $updates = array(
                'pay_no'=>$order_id,
                'state'=>1,
                'ptime'=>$ptime,
            );

            $update_re = 0;
            if($is_save['num']){
                $code = 1;
                //如果走到这里说明center订单表没更新成功，直接更新即可
                $update_re = Order::updateOrder($cp_orderId,$updates);
                Erlang::erl_charge($info['sid'],$order_id);
            }else{

                //插入游戏服发货表
                $type = $info['type']==1?2:1;
             
                $sql = "INSERT INTO recharge (`type`,product_id,pay_no,accname,role_id,`name`,money,gold,ctime,state,`level`,source,cp_no,remark,rmb)
VALUES ('{$type}','{$info['product_id']}','{$order_id}','{$info['accname']}','{$info['role_id']}','{$info['nickname']}','{$info['money']}','{$info['gold']}',{$ptime},0,{$info['lv']},'{$info['channel']}','{$cp_orderId}','{$payExt}','{$info['money']}') ";
                $success = centerdb($info['sid'])->query($sql);

                if($success){
                    //更新center订单表
                    $update_re = Order::updateOrder($cp_orderId,$updates);
                    if($update_re){
                        $code = 1;
                        Erlang::erl_charge($info['sid'],$order_id);
                    }
                }else{
                    $code = -7;
                }
            }
        }
    }
}
$msg = date('Y-m-d H:i:s').'`'.$ip.'`'.$code.'`'.$msgCode[$code];

$data = array('orderId'=>$orderId,'amount'=>$amount,'game'=>$game,'zone'=>$zone,'uid'=>$uid);
return_input(json_encode(array('errno'=>$code==1?1000:0,'errmsg'=>$msgCode[$code],'data'=>$data)));

/*
 * {
    "errno": int, //状态值，1000：充值成功; 1001：签名错误; -1008：参数错误; 1108：订单重复。
    "errmsg": string,//状态提示信息，成功时为空
    "data": {
        "orderId": //麟游订单号
        "amount": //商品总价，单位（分）
        "game": //游戏名缩写
        "zone": //区服ID
        "uid": //第三方平台用户标识
    }
}

 */

   
   
   
