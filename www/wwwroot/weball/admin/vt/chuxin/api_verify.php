<?php
/**
 * 验证code接口
 * User: wtf
 * Date: 17/12/5
 * Time: 下午3:26
 */

//测试地址：http://lijinling.g.linnyou.com/api/index
//正式地址：http://g.linnyou.com/api/index
$url = 'http://g.chuxinhudong.com/api/index';
//$url = 'http://g.linnyou.com/api/index';
$key = '036e245eb8317cef';

$sign = md5($token.$key);
$param = "data={$token}&sign={$sign}";
$re = Http::httpPost($url,$param);

/**
 * 处理结果
 */
$json = json_decode($re,true);
if(!isset($json['errno'])||$json['errno']!=1000){
    $code = 'error_sdk_request';
    
    return_input($code);
}else{
    $accname = $json['data']['uid'];
    $third_accname = '';
}

/*
 {

    "errno": int, //状态值，1000：登陆成功，1001：签名错误，-1008：参数错误，1003：登陆验证失败。
    "errmsg":string,//状态提示信息，成功时为空
    "data": {
		"game"://游戏名缩写
		"zone"://区服ID,无值时候返回空
		"uid": //用户标识
    }

}

 */
