<?php
/**
 * 回调通知接口
 * Created by PhpStorm.
 * User: fanweiting
 * Date: 2017/7/25
 * Time: 19:34
 */
include_once '../../bs2.php';
include_once '../config.php';

$log_dir = ROOT_DIR."log/";
function pay_log($file='',$msg=''){
    if(!$file) return false;
    $dir = dirname($file);
    if(!is_dir($dir)){
        mkdir($dir, 0777, true);
    }
    file_put_contents($file,$msg.'`'.urldecode(http_build_query($_POST)).'`'.urldecode(http_build_query($_GET))."\r\n",FILE_APPEND);
    return true;
}
/*
game_id	是	int	游戏id
server_id	是	int	服ID
role_id	是	int	角色ID
cp_order_num	是	string	CP方订单号
pt_order_num	是	string	平台订单号
total_fee	是	int	订单金额，单位为分
uid	是	int	用户ID
time	是	int	当前时间戳，10位
ext	否	string	透传参数，下单时传入的参数原样回传
sign	是	string	加密串 规则：请查看下方备注，KEY值请联系商务获取

*/
$key = 'd723917a7a200e821812b4635cff417e';//测试
//$key = '193d21a4eef22bd168fc318bed276162';//正式
$data['game_id'] = $game_id = request('game_id','int');
$data['cp_order_num'] = $cp_order_num = request('cp_order_num','str');
$data['pt_order_num'] = $pt_order_num = request('pt_order_num','str');
$data['server_id'] = $server_id = request('server_id','int');
$data['uid'] = $accname = request('uid','str');
$data['role_id'] = $role_id = request('role_id','int');
$data['total_fee'] = $money = request('total_fee','int');
$data['time'] = $time = request('time','int');
$data['ext'] = $ext = request('ext','str');
$sign = request('sign');

//初始化code
$code = 0;
$msgCode = array(
    1=>'success',
    -1 =>'missing_parameter',
    -2 =>'signature_error',
    -3 =>'check_order_fail',
    -4 =>'check_order_fail',
    -5 =>'check_order_fail',
    -6 =>'server_cannot_be_found',
    -7 =>'order_insert_fail',
    -8 =>'ip limit',
    -9 =>'uid or role error',
    -10 => 'error_money',
);

//白名单
$white_ip = array(
    '124.250.53.77',
    '124.250.53.76',
    '124.250.53.75',
);
$ip = get_ip();
if ( !in_array($ip,$white_ip)){
    //$code = -8;
}

//校验参数
if(empty($cp_order_num)||empty($pt_order_num)||empty($server_id)||empty($accname)||empty($role_id)||empty($money)||empty($time)||empty($sign)){
    $code = -1;
}

//验证签名
if($code == 0){
    ksort($data);
    $_sign = md5($key.http_build_query($data).$key);
    if($sign != $_sign){
        $code = -2;
    }
}
if($code == 0){
    //验证订单
    $ptime = intval($time);
    $order_id = addslashes($pt_order_num);
    $cp_orderId = $cp_order_num;
    if(!preg_match("/^\d*$/",$cp_orderId)){
        $code = -4;
    }

    //游戏业务逻辑
    if($code == 0 ){
        $sql = "SELECT order_id,sid,channel,role_id,nickname,accname,lv,money,gold,state,product_id,conf_id,`type` FROM `order` where order_id='{$cp_orderId}'";
        $info = Ext_Mysql::getInstance('allcenter')->fetchOne($sql);
        if(!$info){
            $code = -5;
        }

        if($code == 0 && $info['state'] == 1){
            $code = 1;
            Erlang::erl_charge($info['sid'],$order_id);
        }

        if($code == 0 && !isset($all_server_bysid[$info['sid']])) {
            $code = -6;
        }

        if($code == 0 && ($accname != $info['accname'] || $role_id != $info['role_id'])){
            $code = -9;
        }

        if($code == 0 && $money != $info['money']){
            $code = -10;
        }

        if($code == 0){
            //查询游戏服订单是否重复
            $sql = "SELECT count(pay_no) AS num FROM charge WHERE pay_no = '{$order_id}'";
            $is_save = centerdb($info['sid'])->fetchOne($sql);

            //更新字段
            $updates = array(
                'pay_no'=>$order_id,
                'state'=>1,
                'ptime'=>$ptime,
            );

            $update_re = 0;
            if($is_save['num']){
                $code = 1;
                //如果走到这里说明center订单表没更新成功，直接更新即可
                $update_re = Order::updateOrder($cp_orderId,$updates);
                Erlang::erl_charge($info['sid'],$order_id);
            }else{
                //插入游戏服发货表
                $itime = time();
                $type = $info['type']==1?2:1;
                $sql = "INSERT INTO charge (`type`,pay_no,accname,role_id,nickname,money,gold,ctime,state,lv,source,cp_no,product_id,conf_id,itime,ext)
VALUES ('{$type}','{$order_id}','{$info['accname']}','{$info['role_id']}','{$info['nickname']}','{$info['money']}','{$info['gold']}',{$ptime},0,{$info['lv']},'{$info['channel']}','{$cp_orderId}','{$info['product_id']}',{$info['conf_id']},'{$itime}','') ";
                $success = centerdb($info['sid'])->query($sql);
                if($success){
                    //更新center订单表
                    $update_re = Order::updateOrder($cp_orderId,$updates);
                    if($update_re){
                        $code = 1;
                        Erlang::erl_charge($info['sid'],$order_id);
                    }
                }else{
                    $code = -7;
                }
            }
            if($update_re==0){
                //更新center订单失败
                //TODO 记录日志
                $msg = date('Y-m-d H:i:s').'`'.$code.'`'.$cp_orderId.'`'.$order_id;
                pay_log($log_dir.'charge/pay_fail.'.date('Ymd').'.log',$msg);
            }
        }
    }
}
$msg = date('Y-m-d H:i:s').'`'.$ip.'`'.$code.'`'.$msgCode[$code];
pay_log($log_dir.'charge/pay_log.'.date('Ymd').'.log',$msg);

exit(json_encode(array('state'=>$code==1?1:0,'msg'=>$msgCode[$code])));



   
   
   
