<?php
/**
 * 插入注册信息
 */
require '../bs2.php';
require './config.php';

//初始化code
$code = 1;
$encrypt['channel'] = $channel = request('channel','str');//平台ID
$encrypt['accname'] = $accname = request('accname','str');//平台帐号
$encrypt['sid'] = $id = request('sid','int');//游戏服id
$encrypt['device_id'] = $device_id = request('device_id','str');//设备ID
$encrypt['os'] = $os = request('os','str');//系统
$encrypt['nickname'] = request('nickname');//角色名，用于签名
$nickname = request('nickname');//角色名
$encrypt['role_id'] = $role_id = request('role_id','int');
$encrypt['time'] = $time = request('time');
$ticket = request('ticket');//加密

// 判断参数完整性
if (empty($role_id) || empty($channel) || empty($accname) || empty($id) || empty($device_id) || empty($os) || empty($nickname) || empty($ticket)) {
    $code = -1;
   
    return_json($code,array());
}

if ($ticket != w_encrypt($encrypt)) {
    $code = -7;
  
    return_json($code,array());
}
// 判断服务器器是否正确
if (empty($server_list_info[$id])) {
    $code = -10;
   
    return_json($code,array());
}
$gid = (int)$server_list_info[$id]['gid'];

// 限制每分钟的访问次数
$cache = Ext_Memcached::getInstance("api");
if (!$num = $cache->fetch($ticket)) {
    $num = 1;
    $cache->store($ticket, $num, 60);
} else {
    $num = $num + 1;
    $cache->store($ticket, $num, 60);
    if ($num > 2) {
        $code = -2;
      
        return_json($code,array());
    }
}
$nickname = urldecode($nickname);
$nickname = addslashes($nickname);
$ip = get_ip();
$time = time();
$db = Ext_Mysql::getInstance('allcenter');
$sql = "insert into `player`(`channel`,`accname`,`last_login_server`,`nickname`,`device_id`,`os`,`reg_time`,`server_id`,`last_login_time`,`ip`,`role_id`,`gid`,`last_login_channel`) values('$channel','$accname','$id','$nickname','$device_id','$os','$time','$id','$time','$ip','$role_id','$gid','$channel')";
$success = $db->query($sql);

if ($success) {
    //清缓存
    $key = sprintf(Key::$api['server_list'],$accname);
    $cache->delete($key);
    //设备表统计用
    $sql = "insert ignore into `player_device`(`device_id`,`accname`,`reg_time`,`channel`,`os`,`nickname`,`gid`,`server_id`,`ip`,`role_id`)
            values('$device_id','$accname','$time','$channel','$os','$nickname','$gid','$id','$ip','$role_id')";
    $db->query($sql);
    //帐号表统计用
    $sql = "insert ignore into `player_accname`(`device_id`,`accname`,`reg_time`,`channel`,`os`,`nickname`,`gid`,`server_id`,`ip`,`role_id`)
            values('$device_id','$accname','$time','$channel','$os','$nickname','$gid','$id','$ip','$role_id')";
    $db->query($sql);
    $code = 1;
    return_json($code,array());
} else {
    $code = -11;
 
    return_json($code,array());
}	  