<?php
/**
 * 更新玩家最后登陆信息
 * 日期 2017-10-30
 * wtf
 */
require '../bs2.php';
require './config.php';
//初始化code
$code = 1;

$encrypt['role_id'] = $role_id = request('role_id','int'); // 角色ID
$encrypt['channel'] = $channel = request('channel','str'); // 平台号
$encrypt['accname'] = $accname = request('accname','str'); // 平台帐号
$encrypt['server_id'] = $server_id = request('server_id','int'); // 注册游戏服
$encrypt['level'] = $level = request('level','int');
$encrypt['nickname'] = $nickname = request('nickname');//角色名，用于签名
$encrypt['time'] = $time = request('time','int');
$ticket = request('ticket');       // 加密
$ip = get_ip();

// 判断参数完整性
if(empty($role_id) || empty($channel) || empty($accname)|| empty($server_id) || empty($last_login_server)|| empty($nickname) ||empty($ticket)|| empty($time)){
    $code = -1;
    
    return_json($code,array());
}
// 判断ticket是否正确
if($ticket != w_encrypt($encrypt)){
    $code = -7;
    
    return_json($code,array());
}
unset($encrypt_list);
   
// 限制每分钟的访问次数
$cache = Ext_Memcached::getInstance("api");
if(!$num = $cache->fetch($ticket)) {  
    $num = 1;
    $cache->store($ticket, $num, 60);
}else{
    $num = $num + 1;
    $cache->store($ticket, $num, 60);
    if($num > 5) {
        $code = -2;
       
        return_json($code,array());
    }
}

//更新角色
$nickname = urldecode($nickname);
$nickname = addslashes($nickname);
$now_time = time();
$sql = "update player set last_login_time=$now_time,ip='$ip',`level`= $level,`nickname` = '$nickname' where  accname='$accname' and `server_id`='$server_id' and role_id='$role_id' and channel='$channel'";
$db = Ext_Mysql::getInstance('allcenter');
$success = $db->query($sql);
if($db->getAffectRows()>0){
    //更新缓存
    $key = sprintf(Key::$api['server_list'],$accname);
    $player = $cache->fetch($key);
    if(!$player){
        $sql = "select role_id,accname,last_login_server,last_login_time,`nickname`,server_id,reg_time,`level`,channel from player where accname='$accname' and channel='$channel' order by last_login_time desc";
        $player = $db->fetchRow($sql);

        $cache->store($key, $player, 3600);
    }else{
        foreach ($player as $k => $v){
            if($v['server_id'] == $server_id && $v['accname'] == $accname && $v['role_id']==$role_id){
                $now_time = time();
                $player[$k]['last_login_server'] = "$server_id";
                $player[$k]['last_login_time'] = "$now_time";
                $player[$k]['level'] = "$level";
                $player[$k]['ip'] = "$ip";
                $player[$k]['nickname'] = "$nickname";
            }
        }
        $cache->store($key, $player, 3600);
    }
    $code = 1;
    return_json($code,array());
}else{
    $code = 0;
    
    return_json($code,array());
}



  



   
   
