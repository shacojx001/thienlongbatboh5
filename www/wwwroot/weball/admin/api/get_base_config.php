<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/29
 * Time: 10:14
 */

/**
 * 初始化接口
 */
require '../bs2.php';
require '../vt/config.php';
header("Content-type:text/html;charset=utf-8");

$ip =getIP();

$db_table_type = array(
    '1'=>'base_table_goods',//物品表
    '2'=>'base_table_op_type',//操作类型
    '3'=>'base_table_goods',//礼包表 type=15
    '4'=>'base_table_activity',//跨服活动
    '5'=>'base_table_operation_activity_index',//运营活动
    '6'=>'base_table_activity',//跨服运营活动
    '7'=>'base_log',//日志列表
    '8'=>'base_gm_list',//GM工具
    '9'=>'base_table_title',//称号列表
    '10'=>'base_table_shop',//商店类型
    '11'=>'base_table_money',//货币类型
    '12'=>'base_table_recharge',//货币类型
    '13'=>'base_table_bag',//背包类型
);


$type = request('type','str');

if(!$type){
    echo urldecode(json_encode(array('info' => '-7', 'data' => urlencode('type不能为空'))));
    exit;
}

if(empty($db_table_type[$type])){
    echo urldecode(json_encode(array('info' => '-7', 'data' => urlencode('type类型不存在'))));
    exit;
}
$table_name = $db_table_type[$type];
$admin = Ext_Mysql::getInstance('admin');
switch ($type){
    case 1:
        $sql = "select * from $table_name ";
        $res = $admin->fetchRow($sql);
        $list = array();
        foreach($res as $key => $value){
            $list[] = array(
                    'name' => urlencode ($value['name']),
                    'id' => $value['id'],
                    'item_type' => $value['type'],
                    'attr' =>$value,
                    );
        }
        break;
    case 2:
        $sql = "select `value` as id, `desc` as `name` from $table_name";
        $res = $admin->fetchRow($sql);
        $list = array();
        if($res){
            foreach ($res as $v) {
                $list[] = array(
                    'id'=>$v['id'],
                    'item'=>$v['name'],
                    'item_type'=>0,
                    'attr'=>'',
                );
            }
        }
        break;
    case 3:
        $sql = "select id,name from $table_name where type=15";
        $res = $admin->fetchRow($sql);
        $list = array();
        if($res){
            foreach ($res as $v) {
                $list[] = array(
                    'id'=>$v['id'],
                    'item'=>$v['name'],
                    'item_type'=>0,
                    'attr'=>'',
                );
            }
        }
        break;    
    

    case 4://跨服活动类型
        $sql = "select id,name from $table_name where type=6";
        $res = $admin->fetchRow($sql);
        $list = array();
        if($res){
            foreach ($res as $v) {
                $list[] = array(
                    'id'=>$v['id'],
                    'item'=>$v['name'],
                    'item_type'=>0,
                    'attr'=>'',
                );
            }
        }     
        break;  
    case 5:
        $sql = "select * from $table_name";
        $res = $admin->fetchRow($sql);

        $list = array();
        if($res){
            $actDateInfo = array();
            foreach ($res as $v) {
                
                $actDateInfo[$v['act_id']][$v['index']] = array(
                        'act_id'=>$v['act_id'],
                        'index_id'=>$v['index'],
                        'is_default'=>$v['act_id'],
                        'desc'=>$v['desc'],
                        'default'=>$v['default'],
                    );
            }
            foreach ($res as $v) {
                $list[$v['act_id']] = array(
                    'id'=>$v['act_id'],
                    'item'=>$v['name'],
                    'item_type'=>$v['is_kf'],
                    'attr'=>$actDateInfo[$v['act_id']],
                );
            }
        }       
        break;   
    case 6://跨服运营活动
        $sql = "select id,name from $table_name where type=7";
        $res = $admin->fetchRow($sql);
        $list = array();
        if($res){
            foreach ($res as $v) {
                $list[] = array(
                    'id'=>$v['id'],
                    'item'=>$v['name'],
                    'item_type'=>0,
                    'attr'=>'',
                );
            }
        }     
        break;          
    case 7://综合日志
    case 8://GM工具
        $sql = "select * from $table_name ";
        $list = $admin->fetchRow($sql);
        break;
    case 10:
        $sql = "select id,name,price_type from $table_name";
        $res = $admin->fetchRow($sql);
        $list = array();
        if($res){
            foreach ($res as $v) {
                $list[] = array(
                    'id'=>$v['id'],
                    'name'=>$v['name'],
                    'item_type'=>$v['price_type'],
                    'attr'=>'',
                );
            }
        }     
        break;      
    case 12:
        $sql = "select product_id id,concat(rmb,'元->',gold,'元宝') `name` from $table_name";
        $res = $admin->fetchRow($sql);
        $list = array();
        foreach($res as $key => $value){
            $list[] = array(
                    'name' => urlencode ($value['name']),
                    'id' => $value['id'],
                    );
        }
        break;         
    default:
        $sql = "select id,name from $table_name";
        $res = $admin->fetchRow($sql);
        $list = array();
        foreach($res as $key => $value){
            $list[] = array(
                    'name' => urlencode ($value['name']),
                    'id' => $value['id'],
                    );
        }
        break;   

}

if(!empty($list)){
    echo urldecode(json_encode(array('info' => '1', 'data' => $list)));
    exit;
}else{
    echo urldecode(json_encode(array('info' => '-5', 'data' => urlencode('获取失败，请先导表入库'))));
    exit;
}




