<?php
//file_put_contents('./order.log',date('Y-m-d H:i:s').'`'.urldecode(http_build_query($_GET))."\r\n",FILE_APPEND);
include_once '../bs2.php';

$post['appid'] = request('appid','int');
$post['start_time'] = request('start_time','int');
$post['end_time'] = request('end_time','int');
$post['time'] = request('time','int');
$post['ticket'] = request('ticket','str');

$json = array('info'=>0,'data'=>array(),'msg'=>'');
foreach ($post as $key => $val) {
    if(empty($val)){
        echo_json(-2,$key.' empty');
    }
}

if($post['ticket']!=w_encrypt($post)){
    echo_json(-1,'ticket error');
}

$allcenter = Ext_Mysql::getInstance('admin');
$where = " WHERE ctime BETWEEN {$post['start_time']} AND {$post['end_time']}";

$sql = "SELECT
					gid,
					sid,
					source,
					pay_no,
					cp_no,
					accname,
					role_id,
					nickname,
					lv,
					'cny' currency,
					money,
					gold,
					ctime
				FROM
					center_charge
				$where	";
$data = $allcenter->fetchRow($sql);
if($data){
    foreach ($data as $k=>$v) {
        $data[$k]['nickname'] = urlencode($v['nickname']);
    }
}

echo_json(1,'ok',$data);

function w_encrypt($post){
    $key = '2319be7988235310c07b89bf3a0ef451';
    unset($post['ticket']);
    ksort($post);
    $ticket_str = urldecode(http_build_query($post));
    $ticket = md5($ticket_str.$key);
    return $ticket;
}

function echo_json($info,$msg = '',$data = array()){
    $json['info'] = $info;
    $json['msg'] =  $msg;
    $json['data'] = $data;
    echo json_encode($json);exit;
}