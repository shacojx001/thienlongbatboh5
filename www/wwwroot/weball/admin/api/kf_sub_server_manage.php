<?php
require_once '../bs2.php';
error_reporting(0);
ini_set('display_errors', 0);
ini_set('always_populate_raw_post_data',-1);
class kf_sub_server_manage{
    public $admindb;
    public function __construct()
    {
        $this->admindb = Config::admindb();
    }

    public function platform(){
        $sql = "select * from adminplatformlist where 1 order by id";
        return $this->admindb->fetchRow($sql);
    }

    /**
     * 设置平台对应跨服中心
     * @param $gid
     * @param $node
     * @return bool|mysqli_result
     */
    public function setPlatformKf($gid,$nodeInfo){
        $gid = (int)$gid;
        if(!$gid){
            return false;
        }
        clean_xss($nodeInfo);
        $sql = "update adminplatformlist set kf_center = '{$nodeInfo['node']}' where gid = '{$gid}'";
        $re = $this->admindb->query($sql);
        if($re){
            return $this->setServerKf($gid,$nodeInfo);
        }
        return false;
    }

    /**
     * 设置专服下的游戏服base_kf表 指定使用哪个kf
     * @param $gid
     * @param $nodeInfo
     * @return array|bool
     */
    private function setServerKf($gid,$nodeInfo){
        //获取gid下所有服
        //修改器base_kf表
        if(!$gid) return false;
        $list = Model::get_server_list('id,otime',"gid = {$gid} and main_id = 0");
        $res = array();
        if($list){
            $save = array(
                'node_id'=>'100',
                'name'=>$nodeInfo['node'],
                'cookie'=>$nodeInfo['cookie'],
            );
            foreach ($list as $v) {
                $re =  Config::gamedb($v['id'])->insertOrUpdate('base_kf',$save);
                if(!$re){
                    $res[] = $v['id'];
                }
            }
        }else{
            return true;
        }

        return $res;
    }

    /**
     * 获取跨服中心下的所有服
     * @param $kfCenter
     * @return array|bool
     */
    public function getServerList($kfCenter,$fields = '*',$where='1'){
        if(!$kfCenter){
            return false;
        }
        clean_xss($kfCenter);
        $gids = array();
        $platform = Model::get_platform('gid',"kf_center='{$kfCenter}'");
        if(!$platform){
            return false;
        }
        foreach ($platform as $v) {
            $gids[] = $v['gid'];
        }
        $gids = implode(',',$gids);
        $where = "gid in ({$gids}) and `default` in (1,4) and main_id = 0 and ".$where;
        $list = Model::get_server_list($fields,$where);
        if($list){
            foreach ($list as $k=>$v) {
                $erlInfo = $this->getSerErlang($v['id']);
                $list[$k]['server_node'] = $erlInfo['server_node'];
                $list[$k]['server_cookies'] = $erlInfo['server_cookies'];
            }
        }

        return $list;
    }

    protected function getSerErlang($sid){
        $info = array();
        $sql = "select cf_name,cf_value from base_game where cf_name in ('erl_node','erl_cookie')";
        $res = Config::gamedb($sid)->fetchRow($sql);
        if($res){
            foreach ($res as $re) {
                if($re['cf_name']=='erl_node'){
                    $info['server_node'] = $re['cf_value'];
                }
                if($re['cf_name']=='erl_cookie'){
                    $info['server_cookies'] = $re['cf_value'];
                }
            }
        }

        return $info;
    }

    /**
     * 获取游戏服的base_kf
     * @param $kfCenter
     * @return array|bool
     */
    public function getServerKf($kfCenter){
        if(!$kfCenter){
            return false;
        }
        clean_xss($kfCenter);
        $gids = array();
        $platform = Model::get_platform('gid',"kf_center='{$kfCenter}'");
        if(!$platform){
            return false;
        }
        foreach ($platform as $v) {
            $gids[] = $v['gid'];
        }
        $gids = implode(',',$gids);
        $where = "gid in ({$gids}) and `default` in (1,4) and main_id = 0 ";
        $list = Model::get_server_list('id',$where);
        if($list){
            foreach ($list as $k=>$v) {
                $kfInfo = $this->getSerBaseKf($v['id']);
                $list[$k]['name'] = $kfInfo['name'];
                $list[$k]['cookie'] = $kfInfo['cookie'];
            }
        }
        return $list;
    }

    /**
     * 获取服的base_kf
     * @param $sid
     * @return array
     */
    protected function getSerBaseKf($sid){
        $info = array();
        $sql = "select `name`,`cookie` from base_kf where id = 100";
        $res = Config::gamedb($sid)->fetchRow($sql);
        if($res){
            foreach ($res as $re) {
                $info['name'] = $re['name'];
                $info['cookie'] = $re['cookie'];
            }
        }

        return $info;
    }

    /**
     * 获取游戏服的战力
     * @param $kfCenter
     * @return array|bool
     */
    public function getServerPower($kfCenter){
        if(!$kfCenter){
            return false;
        }
        clean_xss($kfCenter);
        $gids = array();
        $platform = Model::get_platform('gid',"kf_center='{$kfCenter}'");
        if(!$platform){
            return false;
        }
        foreach ($platform as $v) {
            $gids[] = $v['gid'];
        }
        $gids = implode(',',$gids);
        $sql = "select sid,`value` from center_server_power where gid in ({$gids})";

        return Config::admindb()->fetchRow($sql);
    }

}

$client_ip = get_ip();
if(!in_array($client_ip,array('127.0.0.1'))){
//    exit('no access');
}

$kf_server = new kf_sub_server_manage();
$HRpc_server = Ext_HRpc::get_ser_instance();
$HRpc_server->addFunction(array($kf_server,'platform'));
$HRpc_server->addFunction(array($kf_server,'setPlatformKf'));
$HRpc_server->addFunction(array($kf_server,'getServerList'));
$HRpc_server->addFunction(array($kf_server,'getServerKf'));
$HRpc_server->addFunction(array($kf_server,'getServerPower'));
$HRpc_server->start();

/*
CREATE TABLE `kf_server_super` (
`node` varchar(128) NOT NULL COMMENT '跨服节点',
  `cookie` varchar(128) NOT NULL COMMENT '通信cookie',
  `db_name` varchar(50) NOT NULL DEFAULT '' COMMENT '服务库名',
  `db_host` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器host',
  `db_port` int(11) NOT NULL DEFAULT '3306' COMMENT '数据库端口',
  `db_user` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库用户名',
  `db_pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库密码',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '发行商ID',
  UNIQUE KEY `node` (`node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='所有跨服服务器';
*/