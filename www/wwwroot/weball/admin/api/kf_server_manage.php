<?php
require_once '../bs.php';
error_reporting(0);
ini_set('display_errors', 0);
ini_set('always_populate_raw_post_data',-1);
class kf_server_manage{
    public $admindb;
    public function __construct()
    {
        $this->admindb = Config::admindb();
    }

    /**
     * 新增跨服中心
     * @param array $info
     * @return bool
     */
    public function editServer($info = array()){
        if(empty($info)){
            return false;
        }
        clean_xss($info,true);
        $save = array(
            'node'=>$info['node'],
            'cookie'=>$info['cookie'],
            'db_name'=>$info['db_name'],
            'db_host'=>$info['db_host'],
            'db_user'=>$info['db_user'],
            'db_pwd'=>$info['db_pwd'],
            'pid'=>$info['pid'],
        );
        return $this->admindb->insertOrUpdate('kf_server_super',$save);
    }

    /**
     * 获取跨服中心
     * @param $node
     * @return array|bool|null
     */
    public function getServerByNode($node){
        if(!$node){
            return false;
        }
        clean_xss($node,true);
        $sql = "select * from kf_server_super where node = '{$node}'";
        return $this->admindb->fetchOne($sql);
    }

    /**
     * 获取跨服中心
     * @param $node
     * @return array|bool|null
     */
    public function getServerByPid($pid = 0){
        clean_xss($pid,true);
        $where = ' where status = 1';
        if($pid){
            $where .= " and pid = '{$pid}'";
        }
        $sql = "select * from kf_server_super $where";
        return $this->admindb->fetchRow($sql);
    }


    public function getServerInfoByNode($kfCenter){
        clean_xss($kfCenter,true);
        $where = ' where 1';
        if($kfCenter){
            $where .= " and node = '{$kfCenter}'";
        }
        $sql = "select * from kf_server_super $where";
        return $this->admindb->fetchOne($sql);
    }
}

$client_ip = get_ip();
if(!in_array($client_ip,array('127.0.0.1'))){
//    exit('no access');
}

$kf_server = new kf_server_manage();
$HRpc_server = Ext_HRpc::get_ser_instance();
$HRpc_server->addFunction(array($kf_server,'editServer'));
$HRpc_server->addFunction(array($kf_server,'getServerByNode'));
$HRpc_server->addFunction(array($kf_server,'getServerByPid'));
$HRpc_server->addFunction(array($kf_server,'getServerInfoByNode'));
$HRpc_server->start();

/*
CREATE TABLE `kf_server_super` (
`node` varchar(128) NOT NULL COMMENT '跨服节点',
  `cookie` varchar(128) NOT NULL COMMENT '通信cookie',
  `db_name` varchar(50) NOT NULL DEFAULT '' COMMENT '服务库名',
  `db_host` varchar(50) NOT NULL DEFAULT '' COMMENT '服务器host',
  `db_port` int(11) NOT NULL DEFAULT '3306' COMMENT '数据库端口',
  `db_user` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库用户名',
  `db_pwd` varchar(50) NOT NULL DEFAULT '' COMMENT '数据库密码',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '发行商ID',
  UNIQUE KEY `node` (`node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='所有跨服服务器';
*/