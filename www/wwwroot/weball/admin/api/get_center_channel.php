<?php
include_once '../bs2.php';

$post['appid'] = request('appid','int');
$post['time'] = request('time','int');
$post['ticket'] = request('ticket','str');

$json = array('info'=>0,'data'=>array(),'msg'=>'');
foreach ($post as $key => $val) {
    if(empty($val)){
        echo_json(-2,$key.' empty');
    }
}

if($post['ticket']!=w_encrypt($post)){
    echo_json(-1,'ticket error');
}

$admin = Ext_Mysql::getInstance('admin');

$sql = "SELECT channel_str source,channel_name name FROM channel_list";
$data = $admin->fetchRow($sql);

echo_json(1,'ok',$data);

function w_encrypt($post){
    $key = '2319be7988235310c07b89bf3a0ef451';
    unset($post['ticket']);
    ksort($post);
    $ticket_str = urldecode(http_build_query($post));
    $ticket = md5($ticket_str.$key);
    return $ticket;
}

function echo_json($info,$msg = '',$data = array()){
    $json['info'] = $info;
    $json['msg'] =  $msg;
    $json['data'] = $data;
    echo json_encode($json);exit;
}