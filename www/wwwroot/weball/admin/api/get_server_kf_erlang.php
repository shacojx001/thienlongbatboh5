<?php
require '../bs2.php';

$sid = request('sid','int');
if(!$sid) exit();

$gid = substr($sid,0,-4) . '0000';
if(!$gid_list_info[$gid]) exit();

$kfCenter = $gid_list_info[$gid]['kf_center'];
if(!$kfCenter) exit();

$conf = include CONFIG_DIR . 'db_conf_kf.php';
$cookie = $conf[$kfCenter]['cookie'];
exit("100\t$kfCenter\t$cookie");