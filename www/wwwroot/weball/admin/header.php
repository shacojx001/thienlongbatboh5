<?php
/**
 * 后台头部文件
 */
require('./bs.php');

/*
 * 接管PHP错误处理机制 2017年1月11日
 */
if(Ext_Debug::check()){
	$debug = new Ext_Debug();
	set_error_handler(array($debug,'error'));
	register_shutdown_function(array($debug,'shutdown'));
	set_exception_handler(array($debug,'exception'));
}

/**
 * 检查权限
 */
Admin::checkUser();

/**
 * 初始化smarty
 */
$smarty = Ext_Template::getInstance();
$smarty->registerObject("smartyLang",$smarty);
/**
 * 初始化DB
 */
$db = Ext_Mysql::getInstance();
$admindb = Ext_Mysql::getInstance("admin");

/**
 * 系统配置
 */
$sys_conf = $admindb->fetchRow("select * from system_config");
$system_config = array();
foreach($sys_conf as $val){
	$system_config[$val['name']] = $val['value'];
}
$smarty->assign("system_config", $system_config);

/**
 * 游戏基本信息
 */
$tmp = $db->fetchRow("select * from `base_game`");
$base_info = array();
foreach($tmp as $val)
{
    $base_info[$val['cf_name']] = $val['cf_value'];;
}
$MAIL_FLAG = $base_info['mail_flag'];
$smarty->assign("base_info", $base_info);

//解析用户的信息
$userInfo = array(
	'username' => $_SESSION['username'],
	'super' => $_SESSION['super'],
	'userrights' => $_SESSION['grights'],
	'gid' => $_SESSION['gid'],
);
$smarty->assign("userInfo", $userInfo);

