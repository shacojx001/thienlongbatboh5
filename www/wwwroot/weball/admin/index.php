<?php
/**
 * 后台头部文件
 */
require_once './header.php';

/**
 * 左侧菜单
 */
$menu = Auth::get_group_menu($userInfo['gid']);
$menuLog = Auth::get_menu_log();
$smarty->assign('menu',$menu);
$smarty->assign('menuLog',$menuLog);
/**
 * 网页头部
 */
$superclas = new action_superclass();
$srv_group = $superclas->get_allgroup_list();

$server_option = $superclas->server_list_for_header($_SESSION['gid'],true);
$defaultserver = $superclas->get_server_byid($_SESSION['selected_sid'],6);
//服状态颜色
$server_color = $superclas->server_color;
//服状态 <!--------九-零一-起-玩w-ww-.-9-0-17-5-.-c-om------------>
$status = array(0 => Ext_Template::lang('测试服'), 1 => Ext_Template::lang('正式服'), 2 => Ext_Template::lang('计划开服'), 3 => Ext_Template::lang('计划封测'), 4 => Ext_Template::lang('维护中'), 5 => Ext_Template::lang('关服'), 6 => Ext_Template::lang('合服'),10 => Ext_Template::lang('未到开服时间'));
$langList = array(
		'zh-cn'=>Ext_Template::lang('中文'),
		'kr'=>Ext_Template::lang('韩国'),
		'vn'=>Ext_Template::lang('越南'),
		'thai'=>Ext_Template::lang('泰国'),
		);

$smarty->assign("srv_group", $srv_group);
$smarty->assign("server_option", $server_option);
$smarty->assign("defaultserver", $defaultserver);
$smarty->assign("gid", $defaultserver['gid']);
$smarty->assign("server_color", $server_color);
$smarty->assign("status", $status);
$smarty->assign("langList", $langList);

// print_r($_SESSION);exit;
$smarty->display("index.shtml");
