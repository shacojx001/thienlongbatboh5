<?php
/**
 * 后台用 - admin
 * 启动文件
 */
define("DEBUG", false);

if($_SERVER['SERVER_NAME'] == 'www.tdnemo.com' || $_SERVER['SERVER_NAME'] == 'dev.tdnemo.com')
{
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
}else{
    error_reporting(0);
    //ini_set('display_startup_errors', 0);
    ini_set('display_errors', 0);
}


error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);

ini_set("session.cookie_httponly", 1);
ini_set('magic_quotes_runtime', 0);

/*服务器域名地址*/
define("COOKIEDOMAIN", $_SERVER['HTTP_HOST']);

define("PUBLICWEBROOT", 'http://' . $_SERVER['HTTP_HOST'] . "/static/");
define("SERVER_ADDR",$_SERVER['SERVER_NAME']);

/****session****/
//@ini_set("session.save_handler", "memcache");
//ini_set("session.save_path", "tcp://127.0.0.1:11211");
//ini_set('session.gc_maxlifetime',10800);
//设置登录超时时间
session_set_cookie_params(86400*3); 
session_start();
$_SESSION['lang'] = isset($_GET['lang'])?$_GET['lang']:($_SESSION['lang']?$_SESSION['lang']:'zh-cn');

header('Content-Type: text/html; charset=utf-8');//字符集
date_default_timezone_set("Asia/Shanghai");//时区

define("ROOT_PATH", dirname(dirname(dirname(__FILE__))) . "/");
define("ADMIN_PATH", dirname(__FILE__) . "/");
// erl路径
define("ERL_DATA_DIR", ROOT_PATH . "game/src/config/generate/");
// hrl路径
define("HRL_DATA_DIR", ROOT_PATH . "game/include/");
// erl路径 -- 场景
define("ERL_DATA_DIR_SCENE", ROOT_PATH . "game/src/config/generate/scene/");
// erl_config路径
define("ERL_CONFIG_DATA_DIR", ROOT_PATH . "game/config/");
// lua路径
define("LUA_CONFIG_DIR", ROOT_PATH . "lua_new/scripts/config/");
// lua script路径
define("LUA_SCRIPT_DIR", ROOT_PATH . "lua_new/scripts/config/sceneinfo/");
// lua sqlite路径
define("LUA_SQLITE_DIR", ROOT_PATH . "cdn/client/luaconfig/");
// xml路径
define("XML_DATA_DIR", ROOT_PATH . "cdn/flash/assets/config/create/");
// Lua路径
define("LUA_DATA_DIR", ROOT_PATH . "client/editor/Assets/_Scripts/config/");
// 根路径
define('ROOT_DIR', dirname(dirname(__FILE__)) . "/");
// web路径
define("WEBROOT_DIR", dirname(dirname(__FILE__)) . "/html/");
// 库路径，将设置为 include path
define('LIB_DIR', ROOT_DIR . 'lib/');
// 服务路径，将作为 AMF 调用发布
define('SERVICE_DIR', ROOT_DIR . 'service/');
// 映射对象路径，暂时不用
define('SERVICE_VO_DIR', SERVICE_DIR . 'vo/');
// 日志
define('LOG_DIR', ROOT_DIR . 'log/');
// 模块目录
define('MODULE_DIR', ROOT_DIR . 'module/');
// 游戏数据缓冲目录
define('CACHE_DIR', ROOT_DIR . 'cache/');
// 配置文件目录
define('CONFIG_DIR', ROOT_DIR . 'config/');
//模板目录
define('TPL_DIR', ADMIN_PATH . '/view/');
//模板组件
define('TPL_MOM', ADMIN_PATH . '/view/'.$_SESSION['lang'].'/common/');
//中央后台模板名
define('CENTER_TEMP', 'center/');
//Excel配置目录
define('EXCEL_CONFIG_PATH', ROOT_PATH . "doc/Excel数据配置");

define('THIS_TIME', time());

//XMl配置目录
define('XML_FILES_PATH', ROOT_PATH . "doc/xml_config");
//php模版
define('XML_PARSE_PATH', ROOT_PATH . "xml_parser/");
//场景数据目录
define('SCENE_DATA_PATH', ROOT_PATH . "xml_parser/scene_data/");
//生成服务端文件
define('SERVER_DATA_PATH', ROOT_PATH . "game/src/config");
//生成服务端头文件
define('SERVER_HEADER_PATH', ROOT_PATH . "game/include");
//生成客户端文件
define('CLIENT_DATA_PATH', ROOT_PATH . "client/config");

// 图片上传服务器
define("PIC_WEB", 'http://183.6.169.170:7511/wx/show/');
// 图片显示服务器
define("PIC_SHOW", 'http://183.6.169.170:7511/wx/show/');
// 图片审核
define("PIC_AUDIT", 3);


set_include_path( PATH_SEPARATOR . ROOT_DIR.'lib'
    . PATH_SEPARATOR . ROOT_DIR.'lib/Tools'
    . PATH_SEPARATOR . ROOT_DIR.'service'
    . PATH_SEPARATOR . get_include_path());

//@include_once CONFIG_DIR.'security.php';
include_once CONFIG_DIR . 'servers.cfg.php';
include_once LIB_DIR . 'Helper/String.php';
include_once LIB_DIR . 'Helper/Array.php';
spl_autoload_register("autoload");
