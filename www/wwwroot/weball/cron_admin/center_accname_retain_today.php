<?php
/**
 * 今天的留存
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/26
 * Time: 14:36
 */

date_default_timezone_set("Asia/Shanghai");//时区
file_put_contents('./crontab.log',date('Y-m-d H:i:s')."|center_role_retain_today\n",FILE_APPEND);
include 'center_accname_retain.php';

$a = new auto_center_accname_retain();
if(isset($argv[1])){
    $a->date = $argv[1];
}else{
    $a->date = date('Y-m-d');
}
$a->log_accname_retain_channel();

echo "center_role_retain_today run !";