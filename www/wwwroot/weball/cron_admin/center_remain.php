<?php
/**
 * LTV /usr/local/php/bin/php center_remain.php today:accname:channel
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */
include(dirname(dirname(__FILE__)) . "/admin/bs2.php");
isset($argv[1]) ? $param = $argv[1] : die('param error, example this [accname:channel]');
list($date, $by) = explode(':', $param);
if (!$date || !$by) {
    die('param error, example this [accname:channel]');
}

//付费帐号留存
if ('paid_accname' == $by) {
    include 'remain/paid_accname.php';
    $strategy = new center_remaining_paid_accname();
}
//新增付费帐号留存
elseif('newly_paid_accname' == $by){
    include 'remain/newly_paid_accname.php';
    $strategy = new center_remaining_newly_paid_accname();
}
//角色留存 按服按渠道
elseif('role_channel_server' == $by){
    include 'remain/role_channel_server.php';
    $strategy = new role_channel_server();
    if('today' == $date){
        $strategy->date = date('Y-m-d');
        $strategy->run();
    }elseif('everyday'==$date){
        $strategy->date = date('Y-m-d',strtotime(date('Y-m-d'))-86400);
        $strategy->run();
    }elseif('old'==$date){
        $st = strtotime('2017-12-24');
        $st = strtotime('2018-09-22');
        $now = time();
//        $now = strtotime('2018-09-20 01:00:00');
        for ($i = $st; $i < $now; $i = $i + 86400) {
            $strategy->date = date('Y-m-d',$i);
            $strategy->run();
        }
    }
    die;
}

//当天每几分钟跑一次
if ('today' == $date) {
    $time = strtotime(date('Y-m-d'));
    $strategy->run($time);
} //每天凌晨跑一次
elseif ('everyday' == $date) {
    $time = strtotime(date('Y-m-d')) - 86400;
    $strategy->run($time);
} //跑旧数据
elseif ('old' == $date) {
    $st = strtotime('2017-12-24');
    $now = time();
    for ($i = $st; $i < $now; $i = $i + 86400) {
        $strategy->run($i);
    }
}