<?php
/**
 * 汇总 /usr/local/php/bin/php center_collect.php today:all
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */
include(dirname(dirname(__FILE__)) . "/admin/bs2.php");
isset($argv[1]) ? $param = $argv[1] : die('param error, example this [时间:策略]');
list($date, $by) = explode(':', $param);
if (!$date || !$by) {
    die('param error, example this [时间:策略]');
}

//所有汇总
if ('all' == $by) {
    include 'center/total.php';
    $strategy = new total();
}
//汇总 按专服
elseif('gid' == $by ){
    include 'center/total_gid.php';
    $strategy = new total_gid();
}
//汇总 按渠道
elseif('channel' == $by ){
    include 'center/total_channel.php';
    $strategy = new total_channel();
}
//在线汇总
elseif('online' == $by ){
    include 'center/online.php';
    $strategy = new online();
}

//当天每几分钟跑一次
if ('today' == $date) {
    $time = strtotime(date('Y-m-d'));
    $strategy->run($time);
} //每天凌晨跑一次
elseif ('everyday' == $date) {
    $strategy->run();
}