<?php
/**
 * 每日付费帐号留存统计 平价 源 码 网 www.p j ym w.com
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/17
 * Time: 14:59
 */

class center_remaining_paid_accname
{
    private $db;
    private $tb;
    public function __construct()
    {
        $this->tb = 'center_remaining_paid_accname';
        //$this->db = Ext_Mysql::getInstance('admin');
        $this->db = Config::admindb();
    }

    /**
     * 运行
     * $param $date 统计日期
     */
    public function run($time){
        echo 'start:'.date('Y-m-d',$time)."\n";

        $otime = $this->server_open_time();
        if($time < $otime){
            return;
        }
        //1、昨天付费用户数 insert
        $paid_num = $this->daily_pay_num($time);
        $save = array('time'=>$time,'pay_num'=>$paid_num);
        $this->db->insertOrUpdate($this->tb,$save);

        //2、前90天每天付费用户在昨天登录数
        for($day=1;$day<=90;$day++){//前天、大前天、大大前天。。。
            $paid_time = $time-86400*$day;
            //开服前的时间不用统计
            if($paid_time < $otime) break;

            $login_num = $this->login_count_of_paid_num($time,$paid_time);

            //3、update 那天付费用户数再昨天登录数
            $this->update_daily_num($paid_time,$day,$login_num);
            echo $day."|";
        }
        echo "\nend:".date('Y-m-d',$time)."\n";
    }

    /**
     * 求某天付费用户在当天登陆数
     * @param $login_time
     * @param $paid_time
     * @return int
     */
    public function login_count_of_paid_num($login_time,$paid_time){
        //登陆时间为昨天
        $login_time_end = $login_time+86400;
        $where_login = " time >= {$login_time} and time < {$login_time_end}";

        //在付费时间为轮询时间
        $paid_time_end = $paid_time + 86400;
        $where_paid = "ctime>={$paid_time} and ctime<{$paid_time_end}";

        $sql = "select count(distinct(`acc_name`)) as c from center_role_login
                    where {$where_login} and `acc_name` in
                    (select distinct(`accname`) from center_charge
                    where {$where_paid})";
        $re = $this->db->fetchOne($sql);
        $login_num = $re['c']?$re['c']:0;

        return $login_num;
    }

    /**
     * 返回游戏服开服时间
     * @param int $sid
     * @return mixed
     */
    public function server_open_time(){
        global $server_list_info;

        $sid = min(array_keys($server_list_info));
        $otime = $server_list_info[$sid]['otime'];

        return strtotime(date('Y-m-d', $otime));
    }

    /**
     * 某天充值人数
     * @param $date
     * @return int
     */
    public function daily_pay_num($date){
        $et = $date+86400;
        $sql = "select count(1) as c from center_charge where ctime>$date and ctime<$et";
        $re = $this->db->fetchOne($sql);
        return $re['c']?$re['c']:0;
    }

    /**
     * 某天的记录
     * @param $date
     * @return array
     */
    public function daily_info($date){
        $sql = "select * from {$this->tb} where `time` = '{$date}'";
        $re = $this->db->fetchOne($sql);
        return $re ? $re : array();
    }

    /**
     * 更新某天的数据
     * @param $date //付费日志
     * @param $day //第n天
     * @param $login_num //登陆数
     */
    public function update_daily_num($date,$day,$login_num){
        $save = array('time'=>$date);
        //查看当天是否有记录
        $info = $this->daily_info($date);
        if($info){
            $login_data = json_decode($info['login_data'],true);
            $login_data[$day] = $login_num;
            $login_data = json_encode($login_data);
            $save['login_data'] = $login_data;
        }else{
            $save['pay_num'] = $this->daily_pay_num($day);
            $save['login_data'] = json_encode(array($day=>$login_num));
        }
        $this->db->insertOrUpdate($this->tb,$save);
    }


}


