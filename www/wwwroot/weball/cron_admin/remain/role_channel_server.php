<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/17
 * Time: 14:59
 */

class role_channel_server
{
    public $date;
    private $admindb;

    public function __construct()
    {
        //$this->admindb = Ext_Mysql::getInstance('admin');
        $this->admindb = Config::admindb();
    }

    public function run()
    {
        echo "start:".$this->date."\n";
        global $server_list_info;
        $now = time();
        foreach ($server_list_info as $key => $value) {
            if( in_array($value['default'],array(1)) && $value['otime'] < $now){
                $sid = $value['id'];
                $gid = $value['gid'];
                $role_info_sql = "select reg_time from center_role where sid = $sid order by reg_time asc";
                $role_info = $this->admindb->fetchone($role_info_sql);
                $open_time = $role_info['reg_time'] - $role_info['reg_time'] % 86400 - 60 * 60 * 8;//开服时间格式为时间戳
                //各个时间
                $day_json = array(
                    'openTime' => $open_time,
                    'dayTime' => $this->get_zore_point(0),
                    'oneTime' => $this->get_zore_point(1),
                    'towTime' => $this->get_zore_point(2),
                    'threeTime' => $this->get_zore_point(3),
                    'fourTime' => $this->get_zore_point(4),
                    'fiveTime' => $this->get_zore_point(5),
                    'sixTime' => $this->get_zore_point(6),
                    'sevenTime' => $this->get_zore_point(7),
                    'eightTime' => $this->get_zore_point(8),
                    'fourteenTime' => $this->get_zore_point(14),
                    'fifteenTime' => $this->get_zore_point(15),
                    'thirtyTime' => $this->get_zore_point(30),
                    'thirtyoneTime' => $this->get_zore_point(31),
                    'fortyfiveTime' => $this->get_zore_point(45),
                    'fortysixTime' => $this->get_zore_point(46),
                    'sixtyTime' => $this->get_zore_point(60),
                    'sixtyoneTime' => $this->get_zore_point(61),
                    'seventyfiveTime' => $this->get_zore_point(75),
                    'seventysixTime' => $this->get_zore_point(76),
                    'ninetyTime' => $this->get_zore_point(90),
                    'ninetyoneTime' => $this->get_zore_point(91),
                );


                $login_fields = array(
                    'oneTime' => 'one_login',
                    'towTime' => 'one_login',
                    'threeTime' => 'two_login',
                    'fourTime' => 'three_login',
                    'fiveTime' => 'four_login',
                    'sixTime' => 'five_login',
                    'sevenTime' => 'six_login',
                    'eightTime' => 'seven_login',
                    'fourteenTime' => 'fourteen_login',
                    'fifteenTime' => 'fourteen_login',
                    'thirtyTime' => 'thirty_login',
                    'fortyfiveTime' => 'fortyfive_login',
                    'sixtyTime' => 'sixty_login',
                    'seventyfiveTime' => 'seventyfive_login',
                    'ninetyTime' => 'ninety_login',
                );
                #开始统计
                $this->create_date($day_json, $login_fields, $sid, $gid);
            }
        }

        $str = date('Y-m-d H:i:s')." \r\n";
        $file_path = dirname(__FILE__).DIRECTORY_SEPARATOR.'role_channel_server_log.log';
        file_put_contents($file_path,$str,FILE_APPEND);

    }

    # 获得某天的00:00:00
    function get_zore_point($day = 0)
    {
        if($this->date){
            $cur_time = strtotime($this->date)+86400;
        }else{
            $cur_time = strtotime(date('Y-m-d'))+86400;   //算今天内的
        }
        //auto_center_role_retain::log_info($cur_time);
        $zore_point = $cur_time - 86400 * $day;
        return $zore_point;
    }

    function create_date($day_json, $login_fields, $sid, $gid)
    {
        // 在前一天登陆
        $login_where = " and a.time >= " . $day_json['oneTime'] . " and a.time < " . $day_json['dayTime'] . " and b.channel!='' ";
        $group = " GROUP BY channel,sid ";
        // 前一天注册人数
        $sql = "select count(*) as reg_num,channel from center_role where sid=" . $sid . " and gid=" . $gid . " and reg_time >= " . $day_json['oneTime'] . " and reg_time < " . $day_json['dayTime'] . "  and channel!=''  " . $group;
        $reginfo = $this->admindb->fetchRow($sql);
        $ctime = $day_json['oneTime'];
        // 如果有人注册则插入
        foreach ($reginfo as $key) {
            $reg_num = $key['reg_num'];
            $channel = $key['channel'];
            // 验证数据库是否有记录
            $result = $this->admindb->fetchone("SELECT COUNT(*) as t FROM center_role_retain_channel WHERE time='" . $ctime . "' and channel = '" . $channel . "' and sid = " . $sid . " and gid = " . $gid);
            // 无记录则插入记录
            if ($result['t'] == 0) {
                $sql = "replace into center_role_retain_channel(`time`,`channel`,`reg_num`,`sid`,`gid`)values('" . $ctime . "','" . $channel . "'," . $reg_num . "," . $sid . "," . $gid . ")";
                $this->admindb->query($sql);
            } else {
                $sql = "update center_role_retain_channel set reg_num = " . $reg_num . " where time = '" . $ctime . "' and channel = '" . $channel . "' and sid = " . $sid . " and gid = " . $gid;
                $this->admindb->query($sql);
            }
        }
        //开始统计几天前注册在前天登陆的
        $last_item = 'oneTime';
        $login_group = " GROUP BY b.channel,b.sid,b.gid ";
        foreach ($login_fields as $item => $value) {
            if ($item == 'oneTime' or $item == 'fourteenTime' or $item == 'thirtyTime') {  //此三个时间是不需要统计的
                $last_item = $item;  //记录上一个时间
                continue;
            }
            //如果时间大于开服时间，则统计
            if ($day_json['openTime'] <= $day_json[$item]) {
                $low_time = $day_json[$last_item];   //计算的时间  如前两天注册的前一天登陆，这个前一天
                $high_time = $day_json[$item];       //计算时间的前一天        这个为前两天
                $ctime = $day_json[$item];
                //获取前几天注册并在昨天登陆的人数和渠道
                $sql = "select count(distinct(a.`role_id`)) as t,b.channel,b.sid,b.gid from center_role_login a left join center_role b on a.role_id=b.role_id where 1 " . $login_where . " and b.reg_time >= " . $high_time . " and b.reg_time < " . $low_time . $login_group;
                $data_info = $this->admindb->fetchRow($sql);

                // 如果有数据则更新记录
                foreach ($data_info as $data => $field_value) {
                    $login = $field_value['t'];
                    $channel = $field_value['channel'];
                    $c_sid = $field_value['sid'];//服务器id
                    $c_gid = $field_value['gid'];//平台id

                    //查看是否已拥有此记录
                    $check_sql = "select 1 from center_role_retain_channel where time='" . $ctime . "' and channel = '" . $channel . "' and sid = " . $c_sid . " and gid = " . $c_gid . "";
                    $res = $this->admindb->fetchrow($check_sql);
                    //如果已拥有此记录这更新数据，没有则插入数据（插入默认注册人数为0）
                    if ($res) {
                        $sql = "update center_role_retain_channel set $value='" . $login . "' where time='" . $ctime . "' and channel = '" . $channel . "' and sid = " . $c_sid . " and gid = " . $c_gid . "";
                    } else {
                        $sql = "replace into center_role_retain_channel(`$value`,`time`,`channel`,`reg_num`,`sid`,`gid`)values($login,$ctime,'" . $channel . "',0,$c_sid,$c_gid)";
                    }
                    $this->admindb->query($sql);
                    //提交
                }
                $last_item = $item;
            }
        }
    }

}

