<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/17
 * Time: 14:59
 */

include(dirname(dirname(__FILE__)) . "/admin/bs2.php");

class auto_center_accname_retain
{
    public $date;
    private $admindb;

    public function __construct()
    {
        $this->admindb = Ext_Mysql::getInstance('admin');
    }

    public function log_accname_retain_channel()
    {
        //各个时间
        $day_json = array(
            'dayTime' => $this->get_zore_point(0),
            'oneTime' => $this->get_zore_point(1),
            'towTime' => $this->get_zore_point(2),
            'threeTime' => $this->get_zore_point(3),
            'fourTime' => $this->get_zore_point(4),
            'fiveTime' => $this->get_zore_point(5),
            'sixTime' => $this->get_zore_point(6),
            'sevenTime' => $this->get_zore_point(7),
            'eightTime' => $this->get_zore_point(8),
            'fourteenTime' => $this->get_zore_point(14),
            'fifteenTime' => $this->get_zore_point(15),
            'thirtyTime' => $this->get_zore_point(30),
            'thirtyoneTime' => $this->get_zore_point(31),
        );
        $login_fields = array(
            'oneTime' => 'one_login',
            'towTime' => 'one_login',
            'threeTime' => 'two_login',
            'fourTime' => 'three_login',
            'fiveTime' => 'four_login',
            'sixTime' => 'five_login',
            'sevenTime' => 'six_login',
            'eightTime' => 'seven_login',
            'fourteenTime' => 'fourteen_login',
            'fifteenTime' => 'fourteen_login',
            'thirtyTime' => 'thirty_login',
            'thirtyoneTime' => 'thirty_login',
        );
        #开始统计
        $this->create_date($day_json, $login_fields);


    }

    # 获得某天的00:00:00
    function get_zore_point($day = 0)
    {
        if ($this->date) {
            $cur_time = strtotime($this->date) + 86400;
        } else {
            $cur_time = strtotime(date('Y-m-d')) + 86400;   //算今天内的
        }
        //auto_center_role_retain::log_info($cur_time);
        $zore_point = $cur_time - 86400 * $day;
        return $zore_point;
    }

    function create_date($day_json, $login_fields)
    {
        // 在前一天登陆
        $login_where = " and a.time >= " . $day_json['oneTime'] . " and a.time < " . $day_json['dayTime'] . "";
        $group = " GROUP BY acc_name ";
        // 前一天注册人数
        $sql = "select count(*) as reg_num,acc_name from center_accname where 1 and reg_time >= " . $day_json['oneTime'] . " and reg_time < " . $day_json['dayTime'] . " " . $group;
        $reginfo = $this->admindb->fetchRow($sql);
        $ctime = $day_json['oneTime'];
        // 如果有人注册则插入
        foreach ($reginfo as $key) {
            $reg_num = $key['reg_num'];
            $accname = $key['acc_name'];
            // 验证数据库是否有记录
            $result = $this->admindb->fetchone("SELECT COUNT(*) as t FROM center_accname_retain WHERE time='" . $ctime . "' and accname = '" . $accname . "'");
            // 无记录则插入记录
            if ($result['t'] == 0) {
                $sql = "replace into center_accname_retain(`time`,`accname`,`reg_num`)values('" . $ctime . "','" . $accname . "'," . $reg_num . ")";
                $this->admindb->query($sql);
            } else {
                $sql = "update center_accname_retain set reg_num = " . $reg_num . " where time = '" . $ctime . "' and accname = '" . $accname . "'";
                $this->admindb->query($sql);
            }
        }
        //开始统计几天前注册在前天登陆的
        $last_item = 'oneTime';
        $login_group = " GROUP BY b.acc_name ";
        foreach ($login_fields as $item => $value) {
            if ($item == 'oneTime' or $item == 'fourteenTime' or $item == 'thirtyTime') {  //此三个时间是不需要统计的
                $last_item = $item;  //记录上一个时间
                continue;
            }
            //如果时间大于开服时间，则统计
            $low_time = $day_json[$last_item];   //计算的时间  如前两天注册的前一天登陆，这个前一天
            $high_time = $day_json[$item];       //计算时间的前一天        这个为前两天
            $ctime = $day_json[$item];
            //获取前几天注册并在昨天登陆的账号
            $sql = "select count(distinct a.acc_name) as t,b.acc_name from center_role_login a left join center_accname b on a.acc_name=b.acc_name where 1 " . $login_where . " and b.reg_time >= " . $high_time . " and b.reg_time < " . $low_time .$login_group;
            $data_info = $this->admindb->fetchRow($sql);

            // 如果有数据则更新记录
            foreach ($data_info as $data => $field_value) {
                $login = $field_value['t'];
                $accname = $field_value['acc_name'];

                //查看是否已拥有此记录
                $check_sql = "select 1 from center_accname_retain where time='" . $ctime . "' and accname = '" . $accname . "'";
                $res = $this->admindb->fetchrow($check_sql);
                //如果已拥有此记录这更新数据，没有则插入数据（插入默认注册人数为0）
                if ($res) {
                    $sql = "update center_accname_retain set $value='" . $login . "' where time='" . $ctime . "' and accname = '" . $accname . "'";
                } else {
                    $sql = "replace into center_accname_retain (`$value`,`time`,`accname`,`reg_num`)values($login,$ctime,'" . $accname . "',0)";
                }
                $this->admindb->query($sql);
                //提交
            }
            $last_item = $item;
        }

    }

    public static function log_info($data)
    {
        $log_dir = LOG_DIR . 'cron/';
        if (!is_dir($log_dir)) {
            $re = mkdir($log_dir, 0777, true);
        }
        file_put_contents($log_dir . 'center_accname_retain.' . date('Y-m-d'), date('Y-m-d H:i:s') . '`' . print_r($data, true) . "\n", FILE_APPEND);

    }

}

