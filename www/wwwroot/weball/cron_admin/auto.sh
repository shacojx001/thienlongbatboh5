#!/bin/sh

# 定时执行清除php的脚本（当前的shell脚本是每分钟执行一次的）
#cd $(dirname `/usr/sbin/lsof -p $$ | gawk '$4 =="255r"{print $NF}'`)
cd `dirname $0`
function run
{
	#---------------- 每n分钟执行一次x.php(0-59) run_by_min file.php 1 ---------------------#
    run_by_min center_collect.php 10 today:channel #今天分渠道汇总
    run_by_min center_collect.php 10 today:gid #今天分专服汇总
    run_by_min center_collect.php 10 today:all #今天汇总
    run_by_min center_collect.php 10 today:online #今天在线统计

    run_by_min center_accname_retain_today.php 10 #今天的留存 即将废弃 重写

    run_by_min center_remain.php 60 today:paid_accname  #今天付费帐号留存
    run_by_min center_remain.php 60 today:newly_paid_accname #今天新增付费帐号留存
    run_by_min center_remain.php 60 today:role_channel_server #今天角色留存 按渠道、服分组

    run_by_min center_ltv.php 60 today:accname:channel #当天统计帐号LTV 按渠道分组
    run_by_min center_ltv.php 60 today:role:channel #当天统计角色LTV 按渠道分组
    run_by_min center_ltv.php 60 today:role:channel_sid #当天统计角色LTV 按渠道/服分组

	#run_by_min chat_monitor_linyou.php 1 #麟游聊天监控发送 wtf
    #run_by_min auto_watch_monitor.php 2  # 聊天日志监控

	#---------------- 除去指定时间,每分钟执行一次 run_by_min_m file.php 5 23 50 1800  --------#

	#---------------- 每n小时执行一次 run_by_hour file.php(0-23) ---------------------------#

	#---------------- 定时定分执行 run_at_time file.php 3 2  -------------------------------#

    run_at_time center_accname_retain_lastday.php 1 16 #每日玩家留存统计 即将废弃 重写

    run_at_time center_collect.php 0 10 everyday:all #每日汇总
    run_at_time center_collect.php 0 12 everyday:channel #每日分渠道汇总
    run_at_time center_collect.php 0 14 everyday:gid #每日分专服汇总
    run_at_time center_collect.php 0 16 everyday:online #每日在线统计

    run_at_time center_remain.php 0 18 everyday:paid_accname #每日付款帐号留存
    run_at_time center_remain.php 0 20 everyday:newly_paid_accname #每日新增付款帐号留存
    run_at_time center_remain.php 0 30 everyday:role_channel_server #每日角色留存 按渠道按服分组

    run_at_time center_ltv.php 0 20 everyday:accname:channel # 每日统计帐号LTV 按渠道分组
    run_at_time center_ltv.php 0 22 everyday:role:channel # 每日统计角色LTV 按渠道分组
    run_at_time center_ltv.php 0 24 everyday:role:channel_sid # 每日统计角色LTV 按渠道、服分组

    run_at_time kf_sys.php 5 2 single #串渠道跨服自动一服一组
    run_at_time kf_sys.php 5 6 cross #串渠道跨服自动跨组
}

# 所有每分要执行的脚本
function run_by_min
{
	sleep 1
	# 当前分钟数
	min=`date +%M`

	# 传入的变量 file为要执行的文件 t为时间 p为php参数
	local file=${1}
	local t=${2}
	local p=${3}

	# 每t分钟执行一次
	b=$(( $min % $t ))
	if [ ${b} -eq 0 ] ; then 
		   /usr/local/php/bin/php $file $p
	fi
}

function run_by_min_m
{
        sleep 1
        # 当前分钟数
        data=`date +%Y-%m-%d`
        min=`date +%M`
        hour=`date +%H`

        echo $data
        # 传入的变量 file为要执行的文件 t为时间
        local file=${1}
        local t=${2}

        local ehour=${3}
        local emin=${4}
        local elasting=${5}

        local TimeStampB=`date -d$data\ $ehour:$emin:'0' +%s`
        local TimeStampE=`expr $TimeStampB + $elasting`

        local TimeStampB2=`expr $TimeStampB - 86400`
        local TimeStampE2=`expr $TimeStampE - 86400`


        # 每t分钟执行一次
        local NowTimeStamp=`date +%s`
        b=$(( $min % $t ))
        #[ ${b} -eq 0 ] && 
        #[![[${NowTimeStamp} -ge ${TimeStampB}] && [${NowTimeStamp} -le ${TimeStampE}]]] && [![[${NowTimeStamp} -ge ${TimeStampB2}] && [${NowTimeStamp} -le ${TimeStampE2}]]]
        if [ ${b} -eq 0 ] && (! ([ ${NowTimeStamp} -ge ${TimeStampB} ] && [ ${NowTimeStamp} -le ${TimeStampE} ]) )  && (! ( [ ${NowTimeStamp} -ge ${TimeStampB2} ] && [ ${NowTimeStamp} -le ${TimeStampE2} ] ) ); then
                   /usr/local/php/bin/php $file
        fi
}

# 所有每小时要执行的脚本
function run_by_hour
{
	sleep 1
	# 当前小时数
	hour=`date +%H`

	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}

	# 每t分钟执行一次
	b=$(( $hour % $t ))
	if [ ${b} -eq 0 ] ; then 
		   /usr/local/php/bin/php $file 
	fi
}

# 定点定时执行
function run_at_time
{
	sleep 1
	# 当前小时数
	hour=`date +%H`
	# 当前分钟数
	min=`date +%M`

	# 传入的变量 file为要执行的文件 h 为小时 m 分钟 p为php参数
	local file=${1}
	local h=${2}
	local m=${3}
	local p=${4}

	if [[ ${hour} -eq ${h} && ${min} -eq ${m} ]] ; then
		/usr/local/php/bin/php $file $p
	fi
}

run
