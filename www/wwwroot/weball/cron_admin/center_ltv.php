<?php
/**
 * LTV /usr/local/php/bin/php center_ltv.php today:accname:channel
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */
include(dirname(dirname(__FILE__)) . "/admin/bs2.php");
isset($argv[1]) ? $param = $argv[1] : die('param error, example this [accname:channel]');
list($date, $by, $group) = explode(':', $param);
if (!$date || !$by || !$group) {
    die('param error, example this [accname:channel]');
}

//帐号ltv 按渠道
if ('accname' == $by && 'channel' == $group) {
    include 'ltv/ltv_accname_channel.php';
    $strategy = new ltv_accname_channel();
}
//角色ltv 按渠道
elseif('role' == $by && 'channel' == $group){
    include 'ltv/ltv_role_channel.php';
    $strategy = new ltv_role_channel();
}
//角色ltv 按服按渠道
elseif('role' == $by && 'channel_sid' == $group){
    include 'ltv/ltv_role_channel_sid.php';
    $strategy = new ltv_role_channel_sid();
}

//当天每几分钟跑一次
if ('today' == $date) {
    $time = strtotime(date('Y-m-d'));
    $strategy->run($time);
} //每天凌晨跑一次
elseif ('everyday' == $date) {
    $time = strtotime(date('Y-m-d')) - 86400;
    $strategy->run($time);
} //跑旧数据
elseif ('old' == $date) {
    $st = strtotime('2017-12-24');
    $now = time();
    for ($i = $st; $i < $now; $i = $i + 86400) {
        $strategy->run($i);
    }
}