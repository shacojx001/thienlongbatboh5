<?php
/**
 * 全服汇总统计
 * 每天00:10跑数据
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */

class online
{
    private $adminDb;
    public function __construct()
    {
        //$this->adminDb = Ext_Mysql::getInstance('admin');
        $this->adminDb = Config::admindb();
    }

    /**
     * 多天跑
     */
    public function multi_run($start_date = '',$end_date = ''){
        if(empty($start_date)){
            exit('缺少开始日期:年-月-日');
        }
        //开始时间 0点
        $t = strtotime($start_date);
        $st = strtotime(date('Y-m-d',$t));

        //结束时间默认当天零点
        $et = strtotime(date('Y-m-d',time()));
        if($end_date){
            $t = strtotime($end_date);
            $et = strtotime(date('Y-m-d',$t));
        }

        if( ($et - $st) < 86400){
            exit('结束日期比开始日期至少大1天!');
        }

        for($i = $st; $i <= $et; $i+=86400){
            echo "日期:". date("Y-m-d",$i)."\n";
            echo "数据:\n";
            $this->run($i);
        }

    }

    /**
     * 单天跑
     */
    public function run( $time = 0 ){
        if($time){
            $day_st = $time;
        } else{
            //当天零点
            $day_st = strtotime(date('Y-m-d',time())) - 86400;
        }

        echo "start:".date('Y-m-d',$day_st)."\n";

        $day_et = $day_st + 86400;

        // 分渠道统计当天在线
        $data = array();
        $sql = "select gid,sid,channel,max(online) as s from center_online_channel where time>{$day_st} and time<{$day_et} and channel != '' group by gid,sid,channel;";
        $re = $this->adminDb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $data[] = array(
                    'time'=>$day_st,
                    'gid'=>$v['gid'],
                    'sid'=>$v['sid'],
                    'channel'=>$v['channel'],
                    'online'=>$v['s'],
                );
            }
            foreach ($data as $v) {
                $this->adminDb->insert('center_total_online_channel',$v,true);
            }
        }

        // 分服统计当天在线
        $data = array();
        $sql = "select gid,sid,max(s) as s from (select gid,sid,time,sum(online) as s from center_online_channel where time>{$day_st} and time<{$day_et} and channel != '' group by gid,sid,time) a group by gid,sid;";
        $re = $this->adminDb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $data[] = array(
                    'time'=>$day_st,
                    'gid'=>$v['gid'],
                    'sid'=>$v['sid'],
                    'online'=>$v['s'],
                );
            }
            foreach ($data as $v) {
                $this->adminDb->insert('center_total_online',$v,true);
            }
        }
    }

}
