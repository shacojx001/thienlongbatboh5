<?php
/**
 * 分渠道汇总统计
 * 每天00:10跑数据
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */

class total_channel
{
    private $adminDb;
    public function __construct()
    {
        //$this->adminDb = Ext_Mysql::getInstance('admin');
        $this->adminDb = Config::admindb();
    }

    /**
     * 多天跑
     */
    public function multi_run($start_date = '',$end_date = ''){
        if(empty($start_date)){
            exit('缺少开始日期:年-月-日');
        }
        //开始时间 0点
        $t = strtotime($start_date);
        $st = strtotime(date('Y-m-d',$t));

        //结束时间默认当天零点
        $et = strtotime(date('Y-m-d',time()));
        if($end_date){
            $t = strtotime($end_date);
            $et = strtotime(date('Y-m-d',$t));
        }

        if( ($et - $st) < 86400){
            exit('结束日期比开始日期至少大1天!');
        }

        for($i = $st; $i <= $et; $i+=86400){
            echo "日期:". date("Y-m-d",$i)."\n";
            echo "数据:\n";
            $this->run($i);
        }

    }

    /**
     * 单天跑
     */
    public function run( $time = 0 ){
        if($time){
            $day_st = $time;
        } else{
            //当天零点
            $day_st = strtotime(date('Y-m-d',time())) - 86400;
        }
        echo "start:".date('Y-m-d',$day_st)."\n";
        $day_et = $day_st + 86400;

        /*
          `DAU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录角色数',
          `DAO_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录老角色数',
          `DNU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增角色数',
          `DAU` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录帐号数',
          `DAO` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录老帐号数',
          `DNV` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增设备数',
          `DNU` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增帐号数',
          `DPU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天付费人数(按角色且去重)',
          `DPU` int(11) NOT NULL DEFAULT '0' COMMENT '当天付费人数(按帐号且去重)',
          `DPUS` int(11) NOT NULL DEFAULT '0' COMMENT '当天付费次数(按帐号不去重)',
          `DPT` int(11) NOT NULL DEFAULT '0' COMMENT '当天总付费(单位分)',
          `DNPU_R` int(11) NOT NULL DEFAULT '0' COMMENT '注册当天新增付费人数(按角色去重)',
          `DNPU` int(11) NOT NULL DEFAULT '0' COMMENT '注册当天新增付费人数(按帐号去重)',
          `DNPT_R` int(11) NOT NULL DEFAULT '0' COMMENT '注册当天新增总付费(按角色)',
          `DNPT` int(11) NOT NULL DEFAULT '0' COMMENT '注册当天新增总付费(按帐号)',
        */
        $data = array();

        //`DAU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录角色数',
        $sql = "select channel,count(DISTINCT role_id) as c from center_role_login where time >={$day_st} and time < {$day_et} and channel != '' group by channel;";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DAU_R'] = $v['c'];
            }
        }

        //`DAO_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录老角色数',
        $sql = "select a.channel,count(DISTINCT a.role_id) as c from center_role_login a left join center_role b on a.role_id = b.role_id where a.time >={$day_st} and a.time < {$day_et} and b.reg_time<{$day_st} and a.channel != '' group by a.channel;";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DAO_R'] = $v['c'];
            }
        }

        // `DNU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增角色数',
        $sql = "select channel,count(role_id) as c from center_role where reg_time>={$day_st} and reg_time<{$day_et} group by channel;";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DNU_R'] = $v['c'];
            }
        }

        //`DAU` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录帐号数',
        $sql = "select channel,count(DISTINCT acc_name) as c from center_role_login where time >={$day_st} and time < {$day_et} and channel !='' group by channel";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DAU'] = $v['c'];
            }
        }

        //`DAO` int(11) NOT NULL DEFAULT '0' COMMENT '当天登录老帐号数',
        $sql = "select a.channel,count(DISTINCT a.acc_name) as c from center_role_login a left join center_accname b on a.`acc_name`=b.`acc_name` where a.time >={$day_st} and a.time < {$day_et} and b.reg_time <{$day_st} and a.channel !='' group by a.channel";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DAO'] = $v['c'];
            }
        }

        //`DNV` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增设备数',
        $sql = "select channel,count(device) as c from center_device where reg_time>={$day_st} and reg_time<{$day_et} group by channel";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DNV'] = $v['c'];
            }
        }

        //`DNU` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增帐号数',
        $sql = "select channel,count(acc_name) as c from center_accname where reg_time>={$day_st} and reg_time<{$day_et} group by channel";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['channel']]['DNU'] = $v['c'];
            }
        }

        //`DPU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天付费角色数(去重)',
        $sql = "select source,count(DISTINCT role_id) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by source;";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DPU_R'] = $v['c'];
            }
        }

        //`DPU` int(11) NOT NULL DEFAULT '0' COMMENT '当天付费帐号数(去重)',
        $sql = "select source,count(DISTINCT accname) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DPU'] = $v['c'];
            }
        }

        //`DPUS` int(11) NOT NULL DEFAULT '0' COMMENT '当天付费次数(不去重)',
        $sql = "select source,count(accname) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DPUS'] = $v['c'];
            }
        }

        //`DPT` int(11) NOT NULL DEFAULT '0' COMMENT '当天总付费(单位分)',
        $sql = "select source,sum(money) as s from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DPT'] = $v['s'];
            }
        }

        //`DNPU_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增付费角色数',
        $sql = "select a.source,count(DISTINCT a.role_id) as c from center_charge a left join center_role b on a.role_id=b.role_id where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DNPU_R'] = $v['c'];
            }
        }


        //`DNPU` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增帐号付费数',
        $sql = "select a.source,count(DISTINCT a.accname) as c from center_charge a left join center_accname b on a.accname=b.acc_name where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DNPU'] = $v['c'];
            }
        }

        //`DNPT_R` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增角色总付费',
        $sql = "select a.source,sum(money) as s from center_charge a left join center_role b on a.role_id=b.role_id where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DNPT_R'] = $v['s'];
            }
        }

        //`DNPT` int(11) NOT NULL DEFAULT '0' COMMENT '当天新增帐号总付费',
        $sql = "select a.source,sum(money) as s from center_charge a left join center_accname b on a.accname=b.acc_name where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.source";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['source']]['DNPT'] = $v['s'];
            }
        }

        if($data){
            foreach ($data as $channel => $cv) {
                $cv['channel'] = $channel;
                $cv['time'] = $day_st;
                $this->adminDb->insert('center_total_channel',$cv,true);
            }
        }

    }
}

