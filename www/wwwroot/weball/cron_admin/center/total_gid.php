<?php
/**
 * 全服汇总统计
 * 每天00:10跑数据
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */

class total_gid
{
    private $adminDb;
    public function __construct()
    {
        //$this->adminDb = Ext_Mysql::getInstance('admin');
        $this->adminDb = Config::admindb();
    }

    /**
     * 多天跑
     */
    public function multi_run($start_date = '',$end_date = ''){
        if(empty($start_date)){
            exit('缺少开始日期:年-月-日');
        }
        //开始时间 0点
        $t = strtotime($start_date);
        $st = strtotime(date('Y-m-d',$t));

        //结束时间默认当天零点
        $et = strtotime(date('Y-m-d',time()));
        if($end_date){
            $t = strtotime($end_date);
            $et = strtotime(date('Y-m-d',$t));
        }

        if( ($et - $st) < 86400){
            exit('结束日期比开始日期至少大1天!');
        }

        for($i = $st; $i <= $et; $i+=86400){
            echo "日期:". date("Y-m-d",$i)."\n";
            echo "数据:\n";
            $this->run($i);
        }

    }

    /**
     * 单天跑
     */
    public function run( $time = 0 ){
        if($time){
            $day_st = $time;
        } else{
            //当天零点
            $day_st = strtotime(date('Y-m-d',time())) - 86400;
        }
        echo "start:".date('Y-m-d',$day_st)."\n";
        $day_et = $day_st + 86400;

        $D = array(
            'DAU_R' => 0, //当天登录角色数
            'DAO_R' => 0, //当天登录老角色数
            'DNU_R' => 0, //当天新增角色数
            'DAU' => 0, //当天登录帐号数
            'DAO' => 0, //当天登录老帐号数
            'DNV' => 0, //当天新增设备数
            'DNU' => 0,  //当天新增帐号数
            'DPU' => 0, //当天付费人数(按帐号且去重)
            'DPUS' => 0, //当天付费次数(按帐号不去重)
            'DPT' => 0, //当天总付费(单位分)
            'DNPU' => 0, //注册当天新增付费人数(按帐号去重)
            'DNPT' => 0, //注册当天新增总付费(按帐号去重)
            'DPU_R'=>0, //'当天付费人数(按角色且去重)',
            'DNPU_R'=>0,// COMMENT '注册当天新增付费人数(按角色去重)',
            'DNPT_R'=>0,// '注册当天新增总付费(按角色)',
        );
        $data = array();
        //当天登录角色数
        $sql = "select gid,count(DISTINCT role_id) as c from center_role_login where time >={$day_st} and time < {$day_et} group by gid;";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DAU_R'] = $v['c'];
            }
        }

        //当天登录老角色数
        $sql = "select a.gid,count(DISTINCT a.role_id) as c from center_role_login a left join center_role b on a.role_id=b.role_id where a.time >={$day_st} and a.time < {$day_et} and b.reg_time<{$day_st} group by a.gid;";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DAO_R'] = $v['c'];
            }
        }
        //当天新增角色数
        $sql = "select gid,count(role_id) as c from center_role where reg_time >={$day_st} and reg_time < {$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNU_R'] = $v['c'];
            }
        }

        //当天登录帐号数
        $sql = "select gid,count(DISTINCT acc_name) as c from center_role_login where time >={$day_st} and time < {$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DAU'] = $v['c'];
            }
        }

        //当天登录老帐号数
        $sql = "select a.gid,count(DISTINCT a.acc_name) as c from center_role_login a left join center_accname b on a.`acc_name`=b.`acc_name` where a.time >={$day_st} and a.time < {$day_et} and b.reg_time <{$day_st} group by a.gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DAO'] = $v['c'];
            }
        }

        //当天新增帐号数
        $sql = "select gid,count(acc_name) as c from center_accname where reg_time>={$day_st} and reg_time<{$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNU'] = $v['c'];
            }
        }

        //当天新增设备数
        $sql = "select gid,count(device) as c from center_device where reg_time>={$day_st} and reg_time<{$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNV'] = $v['c'];
            }
        }

        //'DPU' => 0, //当天付费人数(按帐号且去重)
        $sql = "select gid,count(DISTINCT `accname`) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DPU'] = $v['c'];
            }
        }

        //'DPUS' => 0, //当天付费次数(按帐号不去重)
        $sql = "select gid,count( accname ) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DPUS'] = $v['c'];
            }
        }

        //'DPT' => 0, //当天总付费(单位分)
        $sql = "select gid,sum(money) as s from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DPT'] = $v['s'];
            }
        }

        //'DNPU' => 0, //注册当天新增付费人数(按帐号去重)
        $sql = "select a.gid,count(DISTINCT a.accname) as c from center_charge a left join center_accname b on a.accname=b.acc_name where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNPU'] = $v['c'];
            }
        }

        //'DNPT' => 0, //注册当天新增总付费(按帐号去重)
        $sql = "select a.gid,sum(money) as s from center_charge a left join center_accname b on a.accname=b.acc_name where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNPT'] = $v['s'];
            }
        }

        //'DPU_R'=>0, //'当天付费人数(按角色且去重)',
        $sql = "select gid,count(DISTINCT role_id) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et} group by gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DPU_R'] = $v['c'];
            }
        }

        //'DNPU_R'=>0,// COMMENT '注册当天新增付费人数(按角色去重)',
        $sql = "select a.gid,count(DISTINCT a.role_id) as c from center_charge a left join center_role b on a.role_id=b.role_id where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNPU_R'] = $v['c'];
            }
        }

        //'DNPT_R'=>0,// '注册当天新增总付费(按角色)',
        $sql = "select a.gid,sum(money) as s from center_charge a left join center_role b on a.role_id=b.role_id where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et} group by a.gid";
        $re = $this->adminDb->fetchRow($sql);
        if($re) {
            foreach ($re as $v) {
                $data[$v['gid']]['DNPT_R'] = $v['s'];
            }
        }

        if($data){
            foreach ($data as $gid => $gv) {
                $gv['gid'] = $gid;
                $gv['time'] = $day_st;
                $this->adminDb->insert('center_total_gid',$gv,true);
            }
        }
    }

}
