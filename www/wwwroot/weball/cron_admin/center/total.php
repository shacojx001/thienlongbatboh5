<?php
/**
 * 全服汇总统计
 * 每天00:10跑数据
 * User: wtf
 * Date: 17/11/18
 * Time: 上午9:47
 */

class total
{
    private $adminDb;
    public function __construct()
    {
        //$this->adminDb = Ext_Mysql::getInstance('admin');
        $this->adminDb = Config::admindb();
    }

    /**
     * 多天跑
     */
    public function multi_run($start_date = '',$end_date = ''){
        if(empty($start_date)){
            exit('缺少开始日期:年-月-日');
        }
        //开始时间 0点
        $t = strtotime($start_date);
        $st = strtotime(date('Y-m-d',$t));

        //结束时间默认当天零点
        $et = strtotime(date('Y-m-d',time()));
        if($end_date){
            $t = strtotime($end_date);
            $et = strtotime(date('Y-m-d',$t));
        }

        if( ($et - $st) < 86400){
            exit('结束日期比开始日期至少大1天!');
        }

        for($i = $st; $i <= $et; $i+=86400){
            echo "date:". date("Y-m-d",$i)."\n";
            echo "data:\n";
            $this->run($i);
        }

    }

    /**
     * 单天跑
     */
    public function run( $time = 0 ){
        if($time){
            $day_st = $time;
        } else{
            //当天零点
            $day_st = strtotime(date('Y-m-d',time())) - 86400;
        }
        echo "start:".date('Y-m-d',$day_st)."\n";
        $day_et = $day_st + 86400;

        $D = array(
            'DAU_R' => 0, //当天登录角色数
            'DAO_R' => 0, //当天登录老角色数
            'DNU_R' => 0, //当天新增角色数
            'DAU' => 0, //当天登录帐号数
            'DAO' => 0, //当天登录老帐号数
            'DNV' => 0, //当天新增设备数
            'DNU' => 0,  //当天新增帐号数
            'DPU' => 0, //当天付费人数(按帐号且去重)
            'DPUS' => 0, //当天付费次数(按帐号不去重)
            'DPT' => 0, //当天总付费(单位分)
            'DNPU' => 0, //注册当天新增付费人数(按帐号去重)
            'DNPT' => 0, //注册当天新增总付费(按帐号去重)
            'DPU_R'=>0, //'当天付费人数(按角色且去重)',
            'DNPU_R'=>0,// COMMENT '注册当天新增付费人数(按角色去重)',
            'DNPT_R'=>0,// '注册当天新增总付费(按角色)',
        );

        //当天登录角色数
        $sql = "select count(DISTINCT role_id) as c from center_role_login where time >={$day_st} and time < {$day_et};";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DAU_R'] = $re['c'];

        //当天登录老角色数
        $sql = "select count(DISTINCT a.role_id) as c from center_role_login a left join center_role b on a.role_id=b.role_id where a.time >={$day_st} and a.time < {$day_et} and b.reg_time<{$day_st};";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DAO_R'] = $re['c'];

        //当天新增角色数
        $sql = "select count(role_id) as c from center_role where reg_time >={$day_st} and reg_time < {$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DNU_R'] = $re['c'];

        //当天登录帐号数
        $sql = "select count(DISTINCT acc_name) as c from center_role_login where time >={$day_st} and time < {$day_et};";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DAU'] = $re['c'];

        //当天登录老帐号数
        $sql = "select count(DISTINCT a.acc_name) as c from center_role_login a left join center_accname b on a.`acc_name`=b.`acc_name` where a.time >={$day_st} and a.time < {$day_et} and b.reg_time <{$day_st}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DAO'] = $re['c'];

        //当天新增帐号数
        $sql = "select count(acc_name) as c from center_accname where reg_time>={$day_st} and reg_time<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DNU'] = $re['c'];

        //当天新增设备数
        $sql = "select count(device) as c from center_device where reg_time>={$day_st} and reg_time<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DNV'] = $re['c'];

        //当天付费人数DPU
        $sql = "select count(DISTINCT accname) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DPU'] = $re['c'];

        //当天付费次数DPUS
        $sql = "select count(accname) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DPUS'] = $re['c'];

        //当天总付费(单位分) DPT
        $sql = "select sum(money) as s from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['s']) $D['DPT'] = $re['s'];

        //注册当天新增付费人数(按帐号去重) DNPU
        $sql = "select count(DISTINCT a.accname) as c from center_charge a left join center_accname b on a.accname=b.acc_name where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DNPU'] = $re['c'];

        //注册当天新增总付费(按帐号去重) DNPT
        $sql = "select sum(money) as s from center_charge a left join center_accname b on a.accname=b.acc_name where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['s']) $D['DNPT'] = $re['s'];

        //'DPU_R'=>0, //'当天付费人数(按角色且去重)',
        $sql = "select count(DISTINCT role_id) as c from center_charge where `type`=1 and ctime>={$day_st} and ctime<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DPU_R'] = $re['c'];

        //'DNPU_R'=>0,// COMMENT '注册当天新增付费人数(按角色去重)',
        $sql = "select count(DISTINCT a.role_id) as c from center_charge a left join center_role b on a.role_id=b.role_id where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);
        if($re['c']) $D['DNPU_R'] = $re['c'];

        //'DNPT_R'=>0,// '注册当天新增总付费(按角色)',
        $sql = "select sum(money) as s from center_charge a left join center_role b on a.role_id=b.role_id where a.`type`=1 and a.ctime>={$day_st} and a.ctime<{$day_et} and b.reg_time>={$day_st} and b.reg_time<{$day_et}";
        $re = $this->adminDb->fetchOne($sql);

        if($re['s']) $D['DNPT_R'] = $re['s'];

        $D['time'] = $day_st;

        //file_put_contents('./tmp.log',json_encode($D)."\n",FILE_APPEND);


        $this->adminDb->insert('center_total',$D,true);
    }

}


