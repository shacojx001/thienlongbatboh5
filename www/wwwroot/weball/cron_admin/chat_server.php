<?php
include_once (dirname(dirname(__FILE__))."/admin/bs2.php");
class chat_server {

    public $server;
    public function run() {
        global $argv;
        swoole_set_process_name("/usr/local/php/bin/php {$argv[0]} server");
        $this->server = new swoole_websocket_server("0.0.0.0", 9501);
        $this->server->set(array(
            'worker_num' => 2,
        ));
        $this->server->on('open', function (swoole_websocket_server $server, $frame) {
            //echo "server: handshake success with fd{$frame->fd}\n";
            chat_server::_log("server: handshake success with fd{$frame->fd}");
        });

        $this->server->on('WorkerStart', function(swoole_websocket_server $server, $worker_id) {
            chat_server::_log('WorkerStart|worker_id:'.$worker_id);
            global $argv;
            swoole_set_process_name("/usr/local/php/bin/php {$argv[0]} worker");
        });

        $this->server->on('message', function (swoole_websocket_server $server, $frame) {
            //echo "receive from {$frame->fd}:{$frame->data},opcode:{$frame->opcode},fin:{$frame->finish}\n";
            $rec = json_decode($frame->data,true);
            $data = chat_server::get_message($rec);
            $server->push($frame->fd, json_encode(array('fd'=>$frame->fd,'content'=>json_encode($data))));
        });
        $this->server->on('close', function ($ser, $fd) {
            //echo "client {$fd} closed\n";
            chat_server::_log("client {$fd} closed");
        });
        //$this->server->on('request', function ($request, $response) {
            // 接收http请求从get获取message参数的值，给用户推送
            // $this->server->connections 遍历所有websocket连接用户的fd，给所有用户推送
            //foreach ($this->server->connections as $fd) {
                //$this->server->push($fd, $request->get['message']);
            //}
        //});

        $this->server->start();
    }

    public static function get_message($params){
        $where = '1 and channel_id!=1';
        $gid = intval($params['gid']);
        $sid = intval($params['sid']);
        $chat_channel = intval($params['chat_channel']);
        $role_id = intval(trim($params['role_id']));
        $log_id = intval($params['log_id']);
        $keywords = addslashes(trim($params['keywords']));
        if($gid){
            $where .= ' and gid = '.$gid;
        }
        if($sid){
            $where .= ' and sid = '.$sid;
        }
        if($chat_channel){
            $where .= ' and channel_id = '.$chat_channel;
        }
        if($role_id){
            $where .= ' and send_role_id = '.$role_id;
        }
        if($log_id){
            $where .=' and id >'.$log_id;
        }else{
            $time = time()-5;
            $where .=' and log_time>='.$time;
        }

        $sql = "select id,sid,gid,channel_id,guild_id,guild_name,
                send_role_id,send_nick_name,send_accname,message,
                rec_nick_name,rec_role_id,
                from_unixtime(log_time,'%Y-%m-%d %H:%i:%s') as log_time
                from chat where $where";

        $db = Ext_Mysql::getInstance("admin");
        $res = $db->fetchRow2($sql);
        if($res){
            $keywords = array_filter(explode('|',$keywords));
            if(count($keywords)>0){
                foreach ( $res as &$v) {
                    $v['map'] = '';
                    foreach ($keywords as $keyword) {
                        if(strpos($v['message'],$keyword) !== false) {
                            $v['map'] .= '&nbsp;[<span style="color:red;">'.$keyword.'</span>]';
                        }
                    }

                }
            }
        }

        return $res?$res:array();
    }

    public static function _log($msg){
        file_put_contents('./chat_server.log',date('Y-m-d H:i:s').'|'.$msg."\n",FILE_APPEND);
    }

    /**
     * @Desc 停止swoole服务
     */
    public function stop()
    {
        $spid = pcntl_fork();
        if ($spid) {
            $pname = get_class().".php";
            $this->killProcessByName($pname);
        }
        chat_server::_log('Server Stop!!!');
    }

    /**
     * 杀死所有进程
     * @param $name
     * @param int $signo
     * @return string
     */
    private function killProcessByName($name, $signo = 9)
    {
        $cmd = 'ps -ef |grep "'.$name.'" | grep -v "grep"| awk \'{print $2}\'|xargs kill -'.$signo;
        return exec($cmd);
    }
}

$server = new chat_server();
$action = isset($argv[1])?$argv[1]:''; //获取命令行参数
switch ($action) {
    case 'start':
        $server->run();
        break;
    case 'stop':
        $server->stop();
        break;
    default:
        exit('Valid Command List:start | stop'."\n");
}