<?php
/**
 * ltv 帐号唯一 渠道分组
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/17
 * Time: 14:59
 */


include(dirname(__FILE__) . "/base.php");

class ltv_accname_channel extends base
{
    public function __construct()
    {
        $this->tb = 'center_ltv_accname_channel';
        $this->admindb = Config::admindb();
    }

    /**
     * 求某天的注册数 return [['group'=>'reg_num'],[...]];
     */
    public function reg_num($time)
    {
        $list = array();
        $et = $time + 86400;
        $sql = "select channel,count(acc_name) as c from center_accname where reg_time >= {$time} and reg_time < {$et} group by channel";
        $re = $this->admindb->fetchRow($sql);

        if($re){
            foreach ($re as $v) {
                $list[$v['channel']] = $v['c'];
            }
        }
        return $list;
    }

    /**
     * 求某天注册玩家在第N天充值 return [['group'=>[pay_num,daily_money,total_money]],[...]]
     */
    public function reg_pay($reg_time, $time)
    {
        $list = array();
        $reg_time_end = $reg_time + 86400;
        $time_end = $time + 86400;


        //到当天为止总充值
        $where = "b.reg_time>={$reg_time} and b.reg_time<{$reg_time_end} and a.ctime<{$time_end}";
        $sql =  "select a.source,sum(a.money) as s
                from center_charge a left join center_accname b on a.`accname` = b.`acc_name`
                where {$where} group by a.source";

        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['source']] = array(0,0,$v['s']/100);
            }
        }

        //第N天充值
        $where = "b.reg_time>={$reg_time} and b.reg_time<{$reg_time_end} and a.ctime>={$time} and a.ctime<{$time_end}";
        $sql = "select a.source,count(distinct a.`accname`) as num, sum(a.money) as s
                from center_charge a left join center_accname b on a.`accname` = b.`acc_name`
                where {$where} group by a.source";

        $re = $this->admindb->fetchRow($sql);
        if($re){
            foreach ($re as $v) {
                $list[$v['source']][0] = $v['num'];
                $list[$v['source']][1] = $v['s']/100;
            }
        }

        return $list;
    }


}


