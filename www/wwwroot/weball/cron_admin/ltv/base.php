<?php

/**
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/8
 * Time: 下午12:34
 */
abstract class base
{

    protected $group;
    protected $admindb;
    protected $tb;

    public function run($time)
    {
        echo 'start:' . date('Y-m-d', $time) . "\n";

        $otime = $this->server_open_time();
        if ($time < $otime) {
            return;
        }

        $reg_num = $this->reg_num($time);
        if ($reg_num) {
            //分渠道
            foreach ($reg_num as $group => $num) {
                $save = array('time' => $time, 'reg_num' => $num, 'group' => $group);
                $this->admindb->insertOrUpdate($this->tb, $save);
            }
        }

        //2、前90天每天注册用户在昨天充值
        for ($day = 1; $day <= 90; $day++) {//前天、大前天、大大前天。。。
            $reg_time = $time - 86400 * ($day-1);
            //开服前的时间不用统计
            if ($reg_time < $otime) break;

            $info = $this->reg_pay($reg_time, $time);
            if ($info) {
                //分渠道
                foreach ($info as $group => $v) {
                    //3、update 那天注册用户数在昨天登录数
                    $this->update_daily_num($reg_time, $day, $group, $v);
                }
            }

            echo $day . "|";
        }
        echo "\nend:" . date('Y-m-d', $time) . "\n";
    }

    /**
     * 返回游戏服开服时间
     * @param int $sid
     * @return int
     */
    protected function server_open_time()
    {
        global $server_list_info;

        $sid = min(array_keys($server_list_info));
        $otime = $server_list_info[$sid]['otime'];

        return strtotime(date('Y-m-d', $otime));
    }

    /**
     * 更新某天的数据
     * @param $date //注册时间
     * @param $day //第n天
     * @param $channel //渠道
     * @param $login_num //充值
     */
    protected function update_daily_num($reg_time, $day, $group, $info)
    {
        $save = array('time' => $reg_time, 'group' => $group);
        //查看当天是否有记录
        $_info = $this->daily_info($reg_time,$group);
        if ($_info) {
            $data = json_decode($_info['data'], true);
            $data[(int)$day] = array((int)$info[0], (int)$info[1], (int)$info[2]);
            $data = json_encode($data);
            $save['data'] = $data;
        } else {
            $reg_info = $this->reg_num($reg_time);
            $save['reg_num'] = $reg_info[$group];
            $data[(int)$day] = array((int)$info[0], (int)$info[1], (int)$info[2]);
            $save['data'] = json_encode($data);
        }

        $this->admindb->insertOrUpdate($this->tb, $save);
    }

    /**
     * 某天的记录
     * @param $date
     * @param $group
     * @return array
     */
    protected function daily_info($date,$group)
    {
        $sql = "select * from {$this->tb} where `time` = '{$date}' and `group` = '{$group}'";
        $re = $this->admindb->fetchOne($sql);
        return $re ? $re : array();
    }

    /**
     * 求某天的注册数 return [['group'=>'reg_num'],[...]];
     */
    protected abstract function reg_num($time);

    /**
     * 求某天注册玩家在第N天充值 return [['group'=>[pay_num,money]],[...]]
     */
    protected abstract function reg_pay($reg_time, $time);
}