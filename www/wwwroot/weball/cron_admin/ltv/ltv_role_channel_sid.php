<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/12/07
 * Time: 14:59
 */

class ltv_role_channel_sid
{
    public $date;
    private $admindb;

    public function __construct()
    {
        //$this->admindb = Ext_Mysql::getInstance('admin');
        $this->admindb = Config::admindb();
    }

    public function run($time)
    {
        echo 'start:' . date('Y-m-d', $time) . "\n";
        $day_end = $time + 86400;
        //当天注册的总人数
        $sql = "select count(*) as reg_total ,date(FROM_UNIXTIME(reg_time)) AS time,channel,gid,sid from center_role where reg_time >=$time and reg_time < $day_end group by time,channel,gid,sid";
        $total_res = $this->admindb->fetchRow($sql);

        //将查询的总人数与当天注册并充值的人数结合
        foreach ($total_res as $key => $val) {
            $total_count = $val['reg_total'];
            $channel = $val['channel'];
            $sid = $val['sid'];
            $sql = "replace into center_channel_return_money (gid,sid,reg_time,channel,reg_num)
                  values(" . $val['gid'] . "," . $sid . "," . strtotime($val['time']) . ",'" . $channel . "'," . $total_count . ")";
            //插入一条今天注册的人数
            $code = $this->admindb->query($sql);
        }

        //查询今天充值的记录，用用户注册的日期为分组
        $sql = "select count(distinct a.role_id) as total,sum(a.money) as money,date(FROM_UNIXTIME(reg_time)) AS time,channel ,a.gid,a.sid from center_charge a
                    left join center_role b on a.role_id = b.role_id
                    where ctime >=$time and ctime < $day_end and b.role_id is not NULL
                    group by time,channel,a.gid,a.sid";
        $res = $this->admindb->fetchRow($sql);

        foreach ($res as $key => $val) {
            $reg_time = strtotime($val['time']);    //注册日期
            $lday = ($day_end - $reg_time) / 86400;        //差距的天数
            $sum_money = $val['money'] / 100;           //充值金额
            $sum_num = $val['total'];           //充值的人数
            $sql = "select day_money,all_money from center_channel_return_money where gid = " . $val['gid'] . "  and sid =  " . $val['sid'] . " and channel = '" . $val['channel'] . "' and reg_time = " . $reg_time . " limit 1";
            $isExit = $this->admindb->fetchOne($sql);       //查看记录是否存在
            if ($isExit) {
                if ($lday <= 90) {     //如果小于90天则将记录保存入json
                    $all_money = $isExit['all_money'];
                    $arr = json_decode($isExit['day_money'], true);
                    $arr[$lday]['money'] = $sum_money;
                    $all_money += $sum_money;
                    $arr[$lday]['num'] = $sum_num;
                    $json = json_encode($arr);
                    $sql = "update center_channel_return_money set day_money = '" . $json . "',all_money ='" . $all_money . "'  where gid = " . $val['gid'] . " and sid = " . $val['sid'] . " and channel = '" . $val['channel'] . "' and reg_time = " . $reg_time;
                    $this->admindb->query($sql);
                } else {      //如果大于90天则将总金额累加，其他的不做记录
                    $all_money = $isExit['all_money'];
                    $all_money += $sum_money;
                    $sql = "update center_channel_return_money set all_money ='" . $all_money . "' where gid = " . $val['gid'] . " and sid =  " . $val['sid'] . " and channel = '" . $val['channel'] . "'  and reg_time = " . $reg_time;
                    $this->admindb->query($sql);
                }
            }

        }
    }

}
