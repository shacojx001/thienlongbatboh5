<?php
/**
 * 跨服分组 新服进入
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/2
 * Time: 下午3:07
 */

include dirname(dirname(__FILE__)).'/admin/bs2.php';
class kf_sys
{
    public $admindb;
    public function __construct()
    {
        $this->admindb = Config::admindb();
    }

    
    /**
     * 专服玩法不齐，补上模版玩法
     */
    public function add_tpl($list){
        $sql = "select * from kf_super_group_config where new_server = 1 and node ='tpl' ";
        $tpl_res = $this->admindb->fetchRow($sql);
        $tpl_list = array();
        foreach ($tpl_res as $key => $val) {
            $tpl_list[$val['act_id']] = $val;
        }

        $data = array();
        foreach ($list as $key => $val) {
            $data[$val['node']][$val['act_id']] = $val;
        }

        foreach ($data as $key => $val) {
            foreach (array_diff_key($tpl_list, $val) as $k => $v) {
                $data[$key][$k] = $v;
            }
        }

        $list = array();
        foreach ($data as $key => $val) {
            foreach ($val as $kk => $vv) {
                $vv['node'] = $key;
                $list[] = $vv;
            }
        }

        return $list;
    }

    /**
     * 玩法类 单服组加入其它组
     */
    public function dispatch_group_cron(){
        $sql = "select * from kf_super_group_config where new_server != 2 and node !='tpl'";
        $list = $this->admindb->fetchRow($sql);
        $list = $this->add_tpl($list);
      
        if($list){
            foreach ($list as $v) {
                if(!$v['node']||!$v['act_id']||!$v['type']||!$v['open_days']||!$v['new_server']||!$v['group_num']){
                    continue;
                }
                if($v['new_server'] == 2){
                    continue;
                }
                print($v['node'].','.$v['act_id']."\n");
                $ste = Kf::dispatch_group($v['node'],$v);
                if(empty($ste[0])||$ste[0]!==true){
                    $msg = '|分组失败：'.$ste[1];
                }else{
                    $msg = '|分组成功';
                }
                file_put_contents('./dispatch_group_single_cron.log',date('Y-m-d H:i:s')."|cross start|{$v['node']}|{$v['act_id']}$msg\r\n",FILE_APPEND);

            }
            print('OK!');
        }else{
            print('activity empty!');
        }
    }

    /**
     * 玩法类 一服一组
     */
    public function dispatch_group_single_cron(){
        $allKfCenter = Model::get_kf_server_super_list();
        $activity = Kf::get_act_wf();
        if($activity && $allKfCenter){
            foreach ($allKfCenter as $v) {
                foreach ($activity as $act) {
                    $sql = "select `single_server` from kf_super_group_config where `node` = '{$v['node']}' and act_id='{$act['id']}'";
                    $re = Config::admindb()->fetchOne($sql);
                    //如果设置了 不自动一服一组,则忽略
                    print($v['node'].','.$act['id'].','.$re['single_server']."\n");
                    if($re['single_server'] && $re['single_server'] == 2){
                        continue;
                    }
                    file_put_contents('./dispatch_group_single_cron.log',date('Y-m-d H:i:s')."|single start|{$v['node']}|{$act['id']}\n",FILE_APPEND);
                    Kf::dispatch_group_single_cron($v['node'],$act['id']);
                }
            }
        }

        print('OK!');
    }
}
isset($argv[1]) ? $param = $argv[1] : die('param error, example this [cross|single]');

$sys = new kf_sys();
if('cross' == $param){
    $sys->dispatch_group_cron();
}elseif('single' == $param){
    $sys->dispatch_group_single_cron();
}
