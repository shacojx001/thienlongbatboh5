<?php
/**
 * 获取redis队列中的聊天日志。
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/27
 * Time: 12:34
 */
include_once(dirname(dirname(__FILE__)) . "/admin/bs2.php");
include_once(dirname(dirname(__FILE__)) . "/lib/Tools/Redis.php");

class auto_get_chat_log
{


    public function get_redis_data()
    {

        while (true) {
            $redis = Redis_Server::getInstance();
            $json_data = $redis->pop(CHAT_LOG_KEY);  //将队列中的日志信息出队
            print_r($json_data);
            if (!empty($json_data)) {
                $data = json_decode($json_data, true);  //将json解析
                $insert_sql = "insert into chat (log_time,channel_id,guild_id,guild_name,send_role_id,send_nick_name,rec_role_id,rec_nick_name,message,gid,sid,send_accname,send_paid,send_ip,send_mac,send_net,rec_accname,rec_paid,send_lv,rec_lv) values";
                $all_value_arr = array();
                foreach ($data as $log) {
                    $log_time = $log['log_time'];
                    $channel_id = $log['channel_id'];
                    $guild_id = $log['guild_id'];
                    $guild_name = urldecode($log['guild_name']);
                    $send_role_id = $log['send_role_id'];
                    $send_nick_name = urldecode($log['send_nick_name']);
                    $rec_role_id = $log['rec_role_id'];
                    $rec_nick_name = urldecode($log['rec_nick_name']);
                    $message = urldecode($log['message']);
                    $gid = $log['gid'];
                    $sid = $log['sid'];
                    $send_accname = $log['send_accname'];
                    $send_paid = $log['send_paid'];
                    $send_ip = $log['send_ip'];
                    $send_mac = $log['send_mac'];
                    $send_net = $log['send_net'];
                    $rec_accname = $log['rec_accname'];
                    $rec_paid = $log['rec_paid'];
                    $send_lv = $log['send_lv'];
                    $rec_lv = $log['rec_lv'];
                    $str = "('$log_time','$channel_id','$guild_id','$guild_name','$send_role_id','$send_nick_name','$rec_role_id','$rec_nick_name','$message','$gid','$sid','$send_accname','$send_paid','$send_ip','$send_mac','$send_net','$rec_accname','$rec_paid','$send_lv','$rec_lv')";
                    array_push($all_value_arr, $str);
                }
                $all_value_str = implode(',', $all_value_arr);
                $insert_sql = $insert_sql . $all_value_str;
                $admindb = Ext_Mysql::getInstance('admin');
                if (Ext_Mysql::reset_connect($admindb->get_error())) {     //判断数据库连接是否断开，并重新连接执行sql
                    $admindb = Ext_Mysql::getInstance('admin');
                    file_put_contents(LOG_DIR . 'get_chat_log_error.txt', 'mysql connet fail---reconnet-----' . date('Y-m-d-H') . "\r\n", FILE_APPEND);
                }
                try{
                    $result = $admindb->query($insert_sql);
                }catch(Exception $e){//判断数据库连接是否异常,并重新连接执行sql
                    $msg = $e->getMessage();
                    file_put_contents(LOG_DIR . 'get_chat_log_error.txt', 'mysql query fail------reconnet---requery---' . date('Y-m-d-H') . $msg."\r\n", FILE_APPEND);
                    Ext_Mysql::reset_connect($msg);
                    $admindb = Ext_Mysql::getInstance('admin');
                    $result = $admindb->query($insert_sql);
                }

                file_put_contents(LOG_DIR . 'get_chat_log.txt', 'get redis to db success-----' . date('Y-m-d-H') . '----result---' . $result . "\r\n", FILE_APPEND);
            } else {
                sleep(3);
            }
//            sleep(3);
        }
    }
}

$a = new auto_get_chat_log();
$a->get_redis_data();