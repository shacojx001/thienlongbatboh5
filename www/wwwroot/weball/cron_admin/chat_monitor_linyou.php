<?php
/**
 * 麟游聊天监控专用
 * Created by PhpStorm.
 * User: wtf
 * Date: 18/5/2
 * Time: 下午3:07
 */

include dirname(dirname(__FILE__)).'/admin/bs2.php';
class chat {
    /**
     * @var
     */
    protected $current_id = 0;

    /**
     * @var string
     */
    protected $api = 'http://gchat.svc.linnyou.com/chat/getchat/xqj';

    /**
     * run
     */
    public function run(){
        exec("ps -ef | grep 'chat_monitor_linyou' | grep -v grep",$output,$return_var);
        if(count($output)>1){
            exit('task is already running!');
        }
        while (true) {
            $count = $this->handle();
            if($count>0){
                sleep(1);
            }else{
                sleep(5);
            }
        }
    }

    /**
     * 日志处理
     * @return int
     */
    public function handle(){
        $where = " where 1";
        if(0 != $this->current_id){
            $where .= " and id > ".$this->current_id;
        }
        $sql = "select * from chat $where order by id desc limit 10";

        $send = array();
        $db = Ext_Mysql::getInstance('admin');
        $re = $db->fetchRow2($sql);
        if($re){
            foreach ($re as $v) {
                $send[] = array(
                    'sId'=>$v['send_accname'],
                    'sGid'=>$v['send_role_id'],
                    'sNick'=>$v['send_nick_name'],
                    'sPay'=>$v['send_paid'],
                    'sLelvel'=> $v['send_lv'],
                    'rId'=>$v['rec_accname'],
                    'rGid'=>$v['rec_role_id'],
                    'rNick'=>$v['rec_nick_name'],
                    'rPay'=>$v['rec_paid'],
                    'rLelvel'=>$v['rec_lv'],
                    'content'=>$v['message'],
                    'ip'=>$v['send_ip'],
                    'deviceMac'=>$v['send_mac'],
                    'wifi'=>$v['send_net'],
                    'source'=>$v['gid'],
                    'serverNum'=>$v['sid'],
                    'time'=>$v['log_time'],
                );
                $this->current_id = $v['id']>$this->current_id?$v['id']:$this->current_id;
            }
        }
        if($send && !CHAT_DEBUG){
            $this->curl_post($send);
        }

        if(CHAT_DEBUG){
            echo "count:".count($send);
            echo "\t";
            echo $this->current_id;
            echo "\t";
            echo $sql;
            echo PHP_EOL;
        }

        return count($send);
    }

    /**
     * socket发送
     */
    public function curl_post($content,$timeout=3)
    {
        $post_data = "data=".json_encode($content);
//        var_export($post_data);
        //初始化
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $this->api);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //设置post方式提交
        curl_setopt($curl, CURLOPT_POST, 1);
        //设置post数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        //设置超时
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        //执行命令
        $output = curl_exec($curl);
        //关闭URL请求
        curl_close($curl);

        return $output;
    }
}
define('CHAT_DEBUG',false);
$obj = new chat();
$obj->run();