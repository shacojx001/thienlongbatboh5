<?php
/**
 * 监控
 */
include(dirname(dirname(__FILE__)) . "/admin/bs.php");
include(dirname(dirname(__FILE__)) . "/admin/action/superclass.php");
include(dirname(dirname(__FILE__)) . "/config/servers.cfg.php");

class auto_sync_internal_role extends action_superclass
{
    private static $cleanstatus;

    public function __construct()
    {
        $this->admindb = Ext_Mysql::getInstance('admin');
        //$this->allcenter = Ext_Mysql::getInstance('allcenter');
    }

    /**
     * 监控
     */
    public function sync_internal_role()
    {
        global $server_list_info;


        $time = time();
        $slist = array();
        $total = array();

        foreach ($server_list_info as $key => $val) {
            if ($val['default'] == 1 && $val['otime'] < time() && $val['game_ip']) {
                $slist[$key]['sid'] = $val['id'];

                $slist[$key]['gid'] = $val['gid'];

                $slist[$key]['url'] = $val['url'];

                $slist[$key]['ip'] = $val['game_ip'];

            }

            if ($val['default'] != 6 && $val['game_ip']) {
                $total[$key]['ip'] = $val['game_ip'];
            }
        }
        //算机器上总服数
        foreach ($total as $key => $val) {
            $arrt[$val['ip']]['all_server_num'] = $arrt[$val['ip']]['all_server_num'] + 1;
        }

        foreach ($slist as $key => $val) {
            $url = $val['url'] . "js/auto_monitor.txt";

            $res = json_decode(file_get_contents($url));

            $arr[$val['ip']]['ip'] = $val['ip'];

            $arr[$val['ip']]['load'] = str_replace("\n", "", $res->load);

            $arr[$val['ip']]['space_all'] = str_replace("\n", "", $res->space_all);

            $arr[$val['ip']]['space_use'] = str_replace("\n", "", $res->space_use);

            $arr[$val['ip']]['mem_all'] = ceil(str_replace("total\n", "", $res->mem_all) / 1024) . "M";

            $arr[$val['ip']]['mem_free'] = ceil(str_replace("free\n", "", $res->mem_free) / 1024) . "M";

            $arr[$val['ip']]['server_num'] = $arr[$val['ip']]['server_num'] + 1;

            $arr[$val['ip']]['online_num'] = $arr[$val['ip']]['online_num'] + $res->s_num;

            $arr[$val['ip']]['online_top_num'] = $arr[$val['ip']]['online_top_num'] + $res->top_num;

            $arr[$val['ip']]['all_server_num'] = $arrt[$val['ip']]['all_server_num'];

        }

        ///////服务器在线值对比上次值
        $sql = "SELECT
					ip,time,online_num
				FROM
					(
						SELECT
							ip,
							time,
							online_top_num as online_num
						FROM
							log_monitor
            WHERE 
              ip !=''
						ORDER BY
							time DESC
					) a
				GROUP BY
	  				ip";
        $result = $this->admindb->fetchRow($sql);

        foreach ($result as $key => $val) {
            ///整个服务器对比上次值
            //判断时间是否同一天
            //同一天
            if (date('Y-m-d', $val['time']) == date('Y-m-d', $time)) {
                //上次值小，则取当值
                if ($arr[$val['ip']]['online_num'] && $arr[$val['ip']]['online_num'] > $val['online_num']) {

                    $arr[$val['ip']]['online_top_num'] = $arr[$val['ip']]['online_num'];

                } else {//上次值大，或者不存在则取当前值

                    $arr[$val['ip']]['online_top_num'] = $val['online_num'];

                }

            } else {

                $arr[$val['ip']]['online_top_num'] = $arr[$val['ip']]['online_num'];

            }

        }
///////服务器在线值对比上次值

        $sql = "INSERT INTO log_monitor (
            ip,
            time,
            mem_free,
            server_num,
            online_num,
            online_top_num,
            space_use,
            mem_all,
            `load`,
            space_all,
            all_server_num
          )
          VALUES ";


        foreach ($arr as $key => $val) {

            $ip = $val['ip'];
            $mem_free = $val['mem_free'];
            $server_num = $val['server_num'];
            $online_num = $val['online_num'];
            $online_top_num = $val['online_top_num'];
            $space_use = $val['space_use'];
            $mem_all = $val['mem_all'];
            $load = $val['load'];
            $space_all = $val['space_all'];
            $all_server_num = $val['all_server_num'];

            $sql_input .= "(
            							'$ip',
            							'$time',
            							'$mem_free',
            							'$server_num',
            							'$online_num',
            							'$online_top_num',
            							'$space_use',
            							'$mem_all',
            							'$load',
            							'$space_all',
                          '$all_server_num'
            						),";

        }

        $sql_input = rtrim($sql_input, ",");
        $sql = $sql . $sql_input;
        // print_r($sql);exit;
        $this->admindb->query($sql);


// file_put_contents($file, $content,FILE_APPEND);// 这个函数支持版本(PHP 5)


    }//fun end
}

$syncclass = new auto_sync_internal_role();
$syncclass->sync_internal_role();

?>
