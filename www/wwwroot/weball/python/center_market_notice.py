#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from basemodule import basemodule
from logger import logger
import os
import time
base = basemodule()

def center_market_notice(fun,host,port,user,pwd,db,sid,gid,otime,srv_num):
        #print host,port,user,pwd,db,sid,gid,os.getpid()
        try:
            #每小时统计一次市场交易
            gamedb = base.game_link(host,port,user,pwd,db)
            admindb = base.admin_link()
            cur_time = int(time.time())
            zore_point = base.today()
            next_zore_point = base.today() + 86400
            limit_price = 2000

            sql = "select sum(price) as price,nick_name,seller_id from log_market a " \
                  "left join role_base b on a.seller_id = b.role_id  " \
                  "where log_time >=%s and log_time < %s group by seller_id having sum(price) >= %d"\
                  % (zore_point,next_zore_point,limit_price)
            # print sql
            res = gamedb.fetchrow(sql)
            for item in res:
                insert_sql = "replace into center_market_notice(gid,sid,seller_id,seller_name,price,time) values(%d,%d,%d,'%s',%d,%d)" \
                             % (gid,sid,item['seller_id'],item['nick_name'],item['price'],zore_point)
                admindb.query(insert_sql)


        except  Exception, e:
            print e
            print db,'error'
        finally:
            admindb.commit()
            gamedb.close()
            admindb.close()
            log = logger('center_market_notice','success','info').info('pid:%s, host:%s, db:%s Finish' % (os.getpid(),host,db))
            print '%s %s Finish' % (host,db)

if __name__ == '__main__':
        base.multiprocess(center_market_notice,'center_market_notice')



