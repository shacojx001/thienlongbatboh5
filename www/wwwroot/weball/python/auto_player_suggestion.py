#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from basemodule import basemodule
from logger import logger
import os

base = basemodule()

def player_suggestion(fun,host,port,user,pwd,db,sid,gid,otime,srv_num):

    try:
        gamedb = base.game_link(host, port, user, pwd, db)
        admindb = base.admin_link()
        centersql = "select src_time from center_last_insert where sid = %d and `src_table` = 'suggestion' order by src_time desc limit 1" % (sid)
        srcinfo = admindb.fetchone(centersql)
        if srcinfo:
            src_time = int(srcinfo['src_time']) - 600
        else:
            src_time = 0
        limit = 2000
        i = 0
        last_insert = 0
        while True:
            # 角色表
            arr = []
            sql = "select role_id,nickname,role_lv,combat_power,star,content,commit_time from suggestion where commit_time >= %d order by commit_time asc limit %d,%d " % (
            src_time, i, limit)
            row = gamedb.fetchrow(sql)
            if row:
                for res in row:
                    arr.append("(%d,%d,%d,'%s',%d,%d,%d,'%s',%d)" % (
                        gid,sid,res['role_id'],res['nickname'],res['role_lv'],res['combat_power'],res['star'],res['content'],res['commit_time']))
                    last_insert = res['commit_time']
                insert_sql_value = ','.join(arr)
                # 角色表
                insert_sql = "replace into player_suggestion(gid,sid,role_id,nickname,role_lv,combat_power,star,content,commit_time) values" + insert_sql_value
                # print insert_sql
                admindb.execute(insert_sql)

            else:
                break
            i += limit
        if last_insert:
            if src_time == 0:
                last_sql = "insert into center_last_insert (sid,src_table,src_time) values(%d,'suggestion','%s')" % (sid, last_insert)
                # print last_sql
                admindb.execute(last_sql)
            else:
                last_sql = "update center_last_insert set src_time= %d where sid=%d and src_table='suggestion'" % (last_insert, sid)
                # print last_sql
                admindb.execute(last_sql)

    except Exception, e:
        print e
        log = logger('log_player_suggestion', 'sync.all.log', 'error').error('%s %s error:%s' % (host, db, e))
    finally:
        admindb.commit()
        gamedb.close()
        admindb.close()
        logger('log_player_suggestion', 'sync.all.log', 'info').info(
            'pid:%s, host:%s, db:%s Finish' % (os.getpid(), host, db))

if __name__ == '__main__':
        base.multiprocess(player_suggestion,'player_suggestion')
