#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from basemodule import basemodule
from logger import logger
import os

base = basemodule()

def center_server_power(fun,host,port,user,pwd,db,sid,gid,otime,srv_num):
        #print host,port,user,pwd,db,sid,gid,os.getpid()
        try:
            #开服服测试
            # if sid != 1:
            #    return
            gamedb = base.game_link(host,port,user,pwd,db)
            admindb = base.admin_link()

            # 数据源表
            sql = "select fight combat_power from role_basic order by fight desc limit 5";
            row = gamedb.fetchrow(sql)
            # print row
            power = 0
            if row:
                for res in row:
                    power += pow(res['combat_power'],2)
                power = int(pow(power * 5, 0.5))
                #print power
                insert_sql = "insert into center_server_power (gid,sid,`value`) values ('%d','%d','%d') ON DUPLICATE KEY UPDATE `value` = '%d'" \
                % (gid,sid,power,power)
                admindb.execute(insert_sql)
        except:
            print db,'error'
        finally:
            admindb.commit()
            gamedb.close()
            admindb.close()
            log = logger('center_server_power','success','info').info('pid:%s, host:%s, db:%s Finish' % (os.getpid(),host,db))
            print '%s %s Finish' % (host,db)

if __name__ == '__main__':
        base.multiprocess(center_server_power,'center_server_power')



