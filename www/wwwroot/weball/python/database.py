#!/usr/bin/env python2.7
import copy
import MySQLdb
import MySQLdb.constants
import MySQLdb.converters
import MySQLdb.cursors
import itertools
from logger import logger
from time import time

class Connection(object):
    """A lightweight wrapper around MySQLdb DB-API connections.

    The main value we provide is wrapping rows in a dict/object so that
    columns can be accessed by name. Typical usage:

        db = database.Connection("localhost", "mydatabase")

    Cursors are hidden by the implementation, but other than that, the methods
    are very similar to the DB-API.

    We explicitly set the timezone to UTC and the character encoding to
    UTF-8 on all connections to avoid time zone and encoding errors.
    """
    def __init__(self, host, port,database, user=None, password=None):
        self.host = host
        self.database = database
        #ANSI,TRADITIONAL
        args = dict(use_unicode=True, charset="utf8",
                    db=database, init_command='SET time_zone = "+8:00"',
                    sql_mode="ANSI")
        if user is not None:
            args["user"] = user
        if password is not None:
            args["passwd"] = password
        
        args["host"] = host
        args["port"] = int(port)

        self._db = None
        self._db_args = args
        self.logging = logger('MYSQL','mysql.log')
        #print 'log init starting..'
        try:
            self.reconnect()
        except:
            self.logging.error("Cannot connect to MySQL on %s,database: %s" % (self.host,self.database))
            exit(0)

    def __del__(self):
        self.close()

    def close(self):
        """Closes this database connection."""
        if self._db is not None:
            self._db.close()
            self._db = None
            self.logging.close()
            #print 'log init end..'

    def commit(self):
        if self._db is not None:
            try:
                self._db.ping()
            except:
                self.reconnect()
            try:
                self._db.commit()
            except Exception,e:
                self._db.rollback()
                self.logging.exception("Can not commit %s" % (e))

    def rollback(self):
        if self._db is not None:
            try:
                self._db.rollback()
            except Exception,e:
                self.logging.error("Can not rollback")

    def reconnect(self):
        """Closes the existing database connection and re-opens it."""
        self.close()
        self._db = MySQLdb.connect(**self._db_args)
        self._db.autocommit(False)

    def query(self, query, *parameters):
        """Returns a row list for the given query and parameters."""
        cursor = self._cursor()
        try:
            return self._execute(cursor, query, parameters)
        finally:
            cursor.close()

    def execute(self, query, *parameters):
        """Executes the given query, returning the lastrowid from the query."""
        cursor = self._cursor()
        try:
            self._execute(cursor, query, parameters)
            return cursor.lastrowid
        finally:
            cursor.close()

    def fetchone(self,query, *parameters):
        """Executes the given query, returning the count value from the query."""
        cursor = self._cursor()
        try:
            cursor.execute(query, parameters)
            return cursor.fetchone()
        finally:
            cursor.close()

    def fetchrow(self,query, *parameters):
        cursor = self._cursor()
        try:
            cursor.execute(query, parameters)
            return cursor.fetchall()
        finally:
            cursor.close()

    def insert(self,table,**datas):
        '''
        Executes the given parameters to an insert SQL and execute it
        '''
        return Insert(self,table)(**datas)

    def replace(self,table,**datas):
        '''
        Executes the given parameters to an replace SQL and execute it
        '''
        return Replace(self,table)(**datas)

    def update(self,table,where,**datas):
        '''
        Executes the given parameters to an update SQL and execute it
        '''
        return Update(self,table,where)(**datas)

    def executemany(self, query, parameters):
        """Executes the given query against all the given param sequences.

        We return the lastrowid from the query.
        """
        cursor = self._cursor()
        try:
            cursor.executemany(query, parameters)
            return cursor.lastrowid
        finally:
            cursor.close()

    def _cursor(self):
        if self._db is None: self.reconnect()
        try:
            self._db.ping()
        except:
            self.reconnect()
        return self._db.cursor(cursorclass = MySQLdb.cursors.DictCursor)

    def _execute(self, cursor, query, parameters):
        try:
            return cursor.execute(query, parameters)
        except:
            self.logging.error("_execute,Error connecting to MySQL on %s" % (self.host))
            self.close()
        
class Insert:
    '''
    Insert Query Generator
    '''
    def __init__(self,db,tablename):
        self.db=db
        self.tablename=tablename

    def __call__(self,**fileds):
        columns=fileds.keys()
        _prefix="".join(['INSERT INTO `',self.tablename,'`'])
        _fields=",".join(["".join(['`',column,'`']) for column in columns])
        _values=",".join(["%s" for i in range(len(columns))])
        _sql="".join([_prefix,"(",_fields,") VALUES (",_values,")"])
        _params=[fileds[key] for key in columns]
        return self.db.execute(_sql,*tuple(_params))

class Replace:
    '''
    Replace Query Generator
    '''
    def __init__(self,db,tablename):
        self.db=db
        self.tablename=tablename

    def __call__(self,**fileds):
        columns=fileds.keys()
        _prefix="".join(['REPLACE INTO `',self.tablename,'`'])
        _fields=",".join(["".join(['`',column,'`']) for column in columns])
        _values=",".join(["%s" for i in range(len(columns))])
        _sql="".join([_prefix,"(",_fields,") VALUES (",_values,")"])
        _params=[fileds[key] for key in columns]
        return self.db.execute(_sql,*tuple(_params))

class Update:
    '''
    Update Query Generator
    '''
    def __init__(self,db,tablename,where):
        self.db=db
        self._tablename=tablename
        self._where=where
    
    def __call__(self,**fields):
        if len(fields)<1:
            #raise OperationalError,"Must have unless 1 field to update"
            pass
        _params=[]
        _cols=[]
        _where = []
        for f in fields:
            _cols.append("`%s`='%s'" % (f,fields[f]))
        _sql_slice=["UPDATE ",self._tablename," SET ",",".join(_cols)]
        if self._where:
            _sql_slice.append(" WHERE ")
            for w in self._where:
                _where.append("`%s`='%s'" % (w,self._where[w]))
            _sql_slice.append(' AND '.join(_where))
        _sql="".join(_sql_slice)
        #print _sql
        return self.db.execute(_sql)
