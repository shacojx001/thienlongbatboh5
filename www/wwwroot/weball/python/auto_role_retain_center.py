#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from basemodule import basemodule
import os
import time
import sys
from logger import logger
from collections import OrderedDict

base = basemodule()


#各个游戏服务器的统计
def log_role_retain_channel(fun, host, port, user, pwd, db, sid, gid, otime, srv_num):
    try:
        gamedb = base.game_link(host, port, user, pwd, db)
        admindb = base.admin_link()
        role_info_sql = "select reg_time from role_base order by reg_time asc";
        role_info = gamedb.fetchone(role_info_sql)
        open_time = role_info['reg_time'] - role_info['reg_time'] % 86400 - 60 * 60 * 8   #开服时间格式为时间戳
        #各个时间
        day_json = {
            'openTime':     open_time,
            'dayTime':      get_zore_point(0),
            'oneTime':      get_zore_point(1),
            'towTime':      get_zore_point(2),
            'threeTime':    get_zore_point(3),
            'fourTime':     get_zore_point(4),
            'fiveTime':     get_zore_point(5),
            'sixTime':      get_zore_point(6),
            'sevenTime':    get_zore_point(7),
            'eightTime':    get_zore_point(8),
            'fourteenTime': get_zore_point(14),
            'fifteenTime':  get_zore_point(15),
            'thirtyTime':   get_zore_point(30),
            'thirtyoneTime':get_zore_point(31),
            }
        #各个时间对应的字段
        login_fields = OrderedDict()
        login_fields['oneTime'] = 'one_login',
        login_fields['towTime'] = 'one_login',
        login_fields['threeTime'] = 'two_login',
        login_fields['fourTime'] = 'three_login',
        login_fields['fiveTime'] = 'four_login',
        login_fields['sixTime'] = 'five_login',
        login_fields['sevenTime'] = 'six_login',
        login_fields['eightTime'] = 'seven_login',
        login_fields['fourteenTime'] = 'fourteen_login',
        login_fields['fifteenTime'] = 'fourtteen_login',
        login_fields['thirtyTime'] = 'thirty_login',
        login_fields['thirtyoneTime'] = 'thirty_login',
        #开始统计
        create_date(day_json,login_fields,gamedb,sid,gid,admindb)

    except Exception, e:
        print e
        log = logger('log_role_retain_channel', 'sync.all.log', 'error').error('%s %s error:%s' % (host, db, e))
    finally:
        gamedb.close()
        admindb.close()
        logger('log_role_retain_channel', 'sync.all.log', 'info').info(
            'pid:%s, host:%s, db:%s Finish' % (os.getpid(), host, db))
        print '%s %s Finish' % (host, db)

# 获得某天的00:00:00
def get_zore_point(day):
    if len(sys.argv) == 2:
        cur_time = isVaildDate(sys.argv[1])
        zore_point = (cur_time - cur_time % 86400) - 86400 * day - 60 * 60 * 8
    else:
        zore_point = base.today() - 86400*day

    return zore_point

def isVaildDate(date):
    try:
        date = date+" 10:00:00"
        timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
        return int(time.mktime(timeArray))
    except:
        print("date error")


def create_date(day_json,login_fields,gamedb,sid,gid,admindb):
    # 在前一天登陆
    login_where = " and a.time >= %s and a.time < %s and b.source!='' "%(str(day_json['oneTime']),str(day_json['dayTime']))
    group = " GROUP BY source "
    # 前一天注册人数
    sql = "select count(*) as reg_num,source from role_base where reg_time >= %s and reg_time < %s and last_login_time>0 and source!='' %s "%(str(day_json['oneTime']),str(day_json['dayTime']),group)
    reginfo = gamedb.fetchrow(sql)
    # 获取Y-m-d H:M:S时间格式
    # time_local = time.localtime(day_json['oneTime'])
    # ctime = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
    ctime = day_json['oneTime']
    # 如果有人注册则插入
    for key in reginfo:
        reg_num = int(key['reg_num'])
        channel = key['source']
        # 验证数据库是否有记录
        result = admindb.fetchone("SELECT COUNT(*) as t FROM center_role_retain_channel WHERE time='%s' and channel = '%s' and sid = %d and gid = %d "%(ctime,channel,sid,gid))
        # 无记录则插入记录
        if result['t'] == 0:
            sql = "replace into center_role_retain_channel(`time`,`channel`,`reg_num`,`sid`,`gid`)values('%s','%s',%d,%d,%d)"%(ctime,channel,reg_num,sid,gid)
            admindb.query(sql)
            admindb.commit()
        else:
            sql = "update center_role_retain_channel set reg_num = %d where time = '%s' and channel = '%s' and sid = %d and gid = %d"%(reg_num,ctime,channel,sid,gid);
            admindb.query(sql)
            admindb.commit()

    #开始统计几天前注册在前天登陆的
    last_item = 'oneTime'
    login_group = " GROUP BY b.source "
    for item in login_fields:
        if item == 'oneTime' or item == 'fourteenTime' or item == 'thirtyTime':  #此三个时间是不需要统计的
            last_item = item  #记录上一个时间
            continue
        #如果时间大于开服时间，则统计
        if day_json['openTime'] <= day_json[item]:
            low_time = str(day_json[last_item])   #计算的时间  如前两天注册的前一天登陆，这个前一天
            high_time = str(day_json[item])       #计算时间的前一天        这个为前两天
            # 获取Y-m-d H:M:S时间格式
            # time_local = time.localtime(day_json[item])
            # ctime = time.strftime("%Y-%m-%d %H:%M:%S", time_local)
            ctime = day_json[item]
            #获取前几天注册并在昨天登陆的人数和渠道
            sql = "select count(distinct(a.`role_id`)) as t,b.source from log_login a left join role_base b on a.role_id=b.role_id where 1 %s and b.reg_time >= %s and b.reg_time < %s %s"%(login_where,high_time,low_time,login_group)
            data_info = gamedb.fetchrow(sql);
            # print sql
            # print data_info

            # 如果有数据则更新记录
            for data in data_info:
                login = data['t'];
                channel = data['source'];


                #查看是否已拥有此记录
                check_sql = "select 1 from center_role_retain_channel where time='%s' and channel = '%s' and sid = %d and gid = %d"%(ctime,channel,sid,gid)
                res = admindb.fetchrow(check_sql)
                #如果已拥有此记录这更新数据，没有则插入数据（插入默认注册人数为0）
                if res:
                    sql = "update center_role_retain_channel set %s='%s' where time='%s' and channel = '%s' and sid = %d and gid = %d"%(
                        login_fields[item][0], login, ctime, channel,sid,gid)
                else:
                    sql = "replace into center_role_retain_channel(`%s`,`time`,`channel`,`reg_num`,`sid`,`gid`)values('%s','%s','%s',%d,%d,%d)"%(
                        login_fields[item][0], login,ctime,channel,0,sid,gid)
                admindb.query(sql);
                #提交
                admindb.commit()
        last_item = item

if __name__ == '__main__':
    base.multiprocess(log_role_retain_channel, 'log_role_retain_channel')

