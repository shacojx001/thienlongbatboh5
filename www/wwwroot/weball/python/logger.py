#!/usr/bin/env python2.7
import logging

class logger(object):

    def __init__(self,name=None,fname=None,level=None):
        if name == None:
            self.name = 'root'
        else:
            self.name = name

        if fname == None:
            self.fname = 'python_log'
        else:
            self.fname = fname

        if level == None:
            self.level = logging.WARN
        elif level == 'info':
            self.level = logging.INFO
        elif level == 'debug':
            self.level = logging.DEBUG
        else:
            self.level = logging.WARN

        self.filename = '../log/'+self.fname+'.txt'
        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        self._log = logging.getLogger(self.name)
        self._log.setLevel(self.level)
        

    def __del__(self):
         logging.shutdown()

    def handle(self):
        self._fh = logging.FileHandler(self.filename)
        self._fh.setLevel(self.level)
        self._ch = logging.StreamHandler()
        self._ch.setLevel(self.level)
        self._fh.setFormatter(self.formatter)
        self._ch.setFormatter(self.formatter)
        self._log.addHandler(self._fh)
        # self._log.addHandler(self._ch)

    def debug(self,msg):
        try:
            self.handle()
            return self._log.debug(msg)
        finally:
            self.remove()

    def info(self,msg):
        try:
            self.handle()
            return self._log.info(msg)
        finally:
            self.remove()
    
    def error(self,msg):
        try:
            self.handle()
            return self._log.error(msg)
        finally:
            self.remove()

    def warn(self,msg):
        try:
            self.handle()
            return self._log.warn(msg)
        finally:
            self.remove()
            
    def exception(self,msg):
        try:
            self.handle()
            return self._log.exception(msg)
        finally:
            self.remove()

    def remove(self):
        self._log.removeHandler(self._fh)
        self._log.removeHandler(self._ch)

    def close(self):
        logging.shutdown()

   
