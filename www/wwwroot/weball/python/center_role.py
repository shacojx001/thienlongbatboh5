#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from basemodule import basemodule
from logger import logger
import os

base = basemodule()

def center_role(fun,host,port,user,pwd,db,sid,gid,otime,srv_num):
        #print host,port,user,pwd,db,sid,gid,os.getpid()
        try:
            gamedb = base.game_link(host,port,user,pwd,db)
            admindb = base.admin_link()

            centersql = "select src_time from center_last_insert where sid = '%d' and `src_table` = 'role_base' order by src_time desc limit 1" % (sid)
            srcinfo = admindb.fetchone(centersql)
            if srcinfo:
                src_time = int(srcinfo['src_time']) - 600
            else:
                src_time = 0
            limit = 2000
            i = 0
            last_insert = 0
            while True:
                # 角色表
                arr = []
                # sql = "select role_id,acc_name,source,reg_time,reg_ip,device from role_base where reg_time >= %d and acc_name != '' order by reg_time asc limit %d,%d " % (src_time,i,limit)
                sql = "SELECT b.role_id, b.accname, source, ctime, ip, c.device FROM role_basic b LEFT JOIN role_create c ON b.role_id = c.role_id WHERE ctime >= %d ORDER BY ctime ASC Limit %d,%d " % (src_time,i,limit)
                row = gamedb.fetchrow(sql)
                if row:
                    for res in row:
                        arr.append("('%d','%d','%d','%s','%s','%d','%s','%s')" % (gid,sid,res['role_id'],res['accname'],res['source'],res['ctime'],res['ip'],res['device']))
                        last_insert = res['ctime']
                    insert_sql_value = ','.join(arr)
                    # 角色表
                    insert_sql = "insert ignore into center_role(gid,sid,role_id,acc_name,channel,reg_time,reg_ip,device) values" + insert_sql_value
                    admindb.execute(insert_sql)
                    # 帐号表
                    insert_sql = "insert ignore into center_accname(gid,sid,role_id,acc_name,channel,reg_time,reg_ip,device) values" + insert_sql_value
                    admindb.execute(insert_sql)
                    # 设备表
                    insert_sql = "insert ignore into center_device(gid,sid,role_id,acc_name,channel,reg_time,reg_ip,device) values" + insert_sql_value
                    admindb.execute(insert_sql)
                else:
                    break
                i += limit
            if last_insert:
                if src_time == 0:
                    last_sql = "insert into center_last_insert (sid,src_table,src_time) values('%d','role_base','%d')" % (sid,last_insert)
                    #print last_sql
                    admindb.execute(last_sql)
                else:
                    last_sql = "update center_last_insert set src_time= '%d' where sid='%d' and src_table='role_base'" % (last_insert,sid)
                    #print last_sql
                    admindb.execute(last_sql)
        except:
            print db,'error'
        finally:
            admindb.commit()
            gamedb.close()
            admindb.close()
            log = logger('center_role','success','info').info('pid:%s, host:%s, db:%s Finish' % (os.getpid(),host,db))
            print '%s %s Finish' % (host,db)

if __name__ == '__main__':
        base.multiprocess(center_role,'center_role')



