from basemodule import basemodule
from logger import logger
import json
import os
from collections import OrderedDict

base = basemodule()


def center_upgrade_on_opensevr(fun, host, port, user, pwd, db, sid, gid, otime, srv_num):
    try:
        gamedb = base.game_link(host, port, user, pwd, db)
        admindb = base.admin_link()
        today = base.today()

        otime = base.startdaytime(otime)
        
        if today < otime:
            return

        day_nums = (today - otime)/86400

        if day_nums > 30:
            return
       
        stat_no_pay = stat_pay = []
        total_role = 0
        sql = "select count(1) as c from role_basic where `level` !=0"
        res = gamedb.fetchone(sql)

        if res:
            total_role = res['c']

            sql = "select count(1) as c,`level` from role_basic where `level` !=0 and first_recharge > 0 group by `level`"
            res = gamedb.fetchrow(sql)
            # print sql
            # print res
            if res:
                stat_pay = json.dumps(res)
            else:
                stat_pay = ''
            sql = "select count(1) as c,`level` from role_basic where `level` !=0 and first_recharge = 0 group by `level`"
            res = gamedb.fetchrow(sql)

            if res:
                stat_no_pay = json.dumps(res)
            else:
                stat_no_pay = ''
       
        sql = "insert into center_upgrade_on_opensevr (sid,day,total,stat_no_pay,stat_pay)values('%d','%d','%d','%s','%s')" % (sid,day_nums,total_role,stat_no_pay,stat_pay)
        # print sql
        admindb.query(sql)

    except:
        print db,'error'
    finally:
        admindb.commit()
        gamedb.close()
        admindb.close()
        log = logger('center_upgrade_on_opensevr','success','info').info('pid:%s, host:%s, db:%s Finish' % (os.getpid(),host,db))
        print '%s %s Finish' % (host,db)

if __name__ == '__main__':
    base.multiprocess(center_upgrade_on_opensevr, 'center_upgrade_on_opensevr')
