#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
import multiprocessing
import urlparse
import ConfigParser
import sys
from logger import logger
from database import Connection
from datetime import *
import time
import os
import MySQLdb

class basemodule(object):

    def __init__(self):
        pass
    
    def center_link(self):
        try:
            config = ConfigParser.ConfigParser()
            config.readfp(open('mysql.ini','rb'))
            centerdb = Connection(config.get('center','host'),\
                                  int(config.get('center','port')),\
                                  config.get('center','db'),\
                                  config.get('center','user'),\
                                  config.get('center','password'))
            return centerdb
        except:
            self.errorlog("%s:%d,%s connect center link error" % (config.get('center','host'),int(config.get('center','port')),config.get('center','db')))
            print 'center link error'
            exit(0)

    def admin_link(self):
        try:
            cursor = {}
            config = ConfigParser.ConfigParser()
            config.readfp(open('mysql.ini','rb'))
            admindb = Connection(config.get('admin','host'),\
                                  int(config.get('admin','port')),\
                                  config.get('admin','db'),\
                                  config.get('admin','user'),\
                                  config.get('admin','password'))
            return admindb
        except:
            self.errorlog("%s:%d,%s connect admin link error" % (config.get('admin','host'),int(config.get('admin','port')),config.get('admin','db')))
            print 'admin link error'
            exit(0)

    def game_link(self,host,port,user,pwd,db):
        try:
            gamedb = Connection(host,port,db,user,pwd)
            return gamedb
        except:
            self.errorlog("%s:%d,%s connect sql error" % (host,port,db))
            print '%s %s game link error' % (host,db)
            exit(0)

    def truncate(self,table):
        centerdb = self.center_link()
        centerdb.query("TRUNCATE TABLE %s" % (table))
        centerdb.close()
        

    def getserverlist(self):
            try:
                admindb = self.admin_link()
                row = admindb.fetchrow("SELECT id,url,db_host,db_port,db_user,db_pwd,sdb,gid,otime,srv_num FROM adminserverlist WHERE `default`=1 and `state`=0")
                if row:
                    slist = {}
                    for s in row:
                        url=urlparse.urlparse(s['url'])
                        host = url.netloc
                        slist[s['id']] = {'sid':s['id'],'host':s['db_host'],'port':s['db_port'],'user':s['db_user'],\
                                          'pwd':s['db_pwd'],'db':s['sdb'],'gid':s['gid'],'otime':s['otime'],\
                                          'srv_num':s['srv_num']}
                    return slist
            finally:
                admindb.close()

    def getglist(self):
            try:
                admindb = self.admin_link()
                row = admindb.fetchrow("SELECT gid,gname FROM admingroup WHERE groupbackground='cooperate'")
                if row:
                    glist = {}
                    for s in row:
                        glist[s['gid']] = s['gname']
                    return glist
            finally:
                admindb.close()

    def getslist(self):
            try:
                admindb = self.admin_link()
                row = admindb.fetchrow("SELECT id,sname FROM adminserverlist WHERE `default`=1 and `state`=0")
                if row:
                    slist = {}
                    for s in row:
                        slist[s['id']] = s['sname']
                    return slist
            finally:
                admindb.close()

    #单进程
    def runmultiprocess(self,fun,host,port,user,pwd,db,sid,gid,otime,srv_num):
            plist = []
            proc = multiprocessing.Process(target=fun,args=(fun,host,port,user,pwd,db,sid,gid,otime,srv_num))
            plist.append(proc)
            for proc in plist:
                proc.start()
            for proc in plist:
                proc.join()

    def start_process(self):
        print 'Starting',multiprocessing.current_process().name
    
    def multiprocess(self,func,fname,t_truncate = ''):
        s = self.check_cron_state(fname)
        if s:
            exit(0)
        lis = self.getserverlist()
        if lis:
            self.cron_state(fname,1)
            pool_size=multiprocessing.cpu_count()
            pool = multiprocessing.Pool(processes=pool_size,initializer=self.start_process())
            reslist = []
            resdb = []
            if t_truncate:
                self.truncate(t_truncate)
            for i in lis:
                res = pool.apply_async(func,(func,lis[i]['host'],lis[i]['port'],lis[i]['user'],lis[i]['pwd'],lis[i]['db'],\
                                               lis[i]['sid'],lis[i]['gid'],lis[i]['otime'],lis[i]['srv_num']))
                reslist.append(res)
                resdb.append(lis[i]['db'])
                #try:
                    #res.get(5)
                #except multiprocessing.TimeoutError:
                #self.errorlog("%s:%d,%s TimeoutError" % (lis[i]['host'],lis[i]['port'],lis[i]['db']))
                # exit(0)
            pool.close()
            #pool.terminate()
            pool.join()
            succ = 0
            fail = 0
            #print resdb
            #succlis = []
            faillis = []
            for i in reslist:
                if i.successful():
                    succ = succ+1
                    #succlis.append(resdb[local])
                else:
                    fail = fail+1
                    local = reslist.index(i)
                    faillis.append(resdb[local])

            logger('crontab','sync.crontab.time.success','info').info("%s,total:%s,success:%s,fail:%s,faillis:%s" % (fname,len(lis),succ,fail,faillis))
            #if res.successful():
            #    print 'successful'
            #else:
            #    print 'fail'
            last_result = "total:%s,success:%s,fail:%s,faillis:%s" % (len(lis),succ,fail,faillis)
            self.cron_state(fname,0,last_result)
        else:
            print 'serverlist not exist!'

    def cron_state(self,cname,state,last_result=''):
        admin_db = self.admin_link()
        if state:
            sql = "INSERT INTO python_cron_state (cname,state)VALUES('%s','%s') ON DUPLICATE key UPDATE state='%s',start_time='%s'" % (cname,state,state,int(time.time()))
        else:
            sql = "UPDATE python_cron_state set state='%s',end_time='%s',times=times+1,last_result='%s' WHERE cname='%s'" % (state,int(time.time()),MySQLdb.escape_string(last_result),cname)
        admin_db.execute(sql)
        admin_db.commit()
        admin_db.close()

    def check_cron_state(self,cname):
        admin_db = self.admin_link()
        sql = "SELECT state FROM python_cron_state WHERE cname='%s'" % (cname)
        s = admin_db.fetchone(sql)
        admin_db.commit()
        admin_db.close()
        if s:
            return s['state']
        else:
            return 0
                
    def errorlog(self,reason):
        cursor = self.admin_link()
        sql = "INSERT INTO python_error_log (`text`,`time`) VALUES('%s','%d')" % (reason,int(time.time()))
        cursor.execute(sql)
        cursor.close()

    def time2date(self,value,format = '%Y-%m-%d %H:%M:%S'):
        if isinstance(value,int) != True:
            value = int(value)
        value = time.localtime(value)
        dt = time.strftime(format, value)
        return dt

    def date2time(self,dt,type = 1):
        if type == 1:
            format = '%Y-%m-%d %H:%M:%S'
        else:
            format = '%Y-%m-%d'
        #time.strptime(dt, '%Y-%m-%d %H:%M:%S')
        s = time.mktime(time.strptime(dt, format))
        return int(s)

    def today(self):
        return self.date2time(datetime.now().strftime('%Y-%m-%d 00:00:00'))

    def strtotime(self,timeformat):
        return self.date2time(datetime.now().strftime(timeformat))

    def startdaytime(self,timestamp):
        return self.date2time(date.fromtimestamp(timestamp).strftime('%Y-%m-%d 00:00:00'))

    def date(self,timestamp):
        return date.fromtimestamp(timestamp)
    
    


        

