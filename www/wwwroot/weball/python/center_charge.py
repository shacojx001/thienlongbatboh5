#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from basemodule import basemodule
from logger import logger
import os

base = basemodule()

def center_charge(fun,host,port,user,pwd,db,sid,gid,otime,srv_num):
        #print host,port,user,pwd,db,sid,gid,os.getpid()
        try:
            gamedb = base.game_link(host,port,user,pwd,db)
            admindb = base.admin_link()

            # 数据源表
            src_table = 'charge'

            centersql = "select src_time from center_last_insert where sid = '%d' and `src_table` = '%s' order by src_time desc limit 1" % (sid, src_table)
            srcinfo = admindb.fetchone(centersql)
            if srcinfo:
                src_time = int(srcinfo['src_time']) - 600
            else:
                src_time = 0
            limit = 2000
            i = 0
            last_insert = 0
            while True:
                # 角色表
                arr = []
                sql = "select * from recharge where ctime >= %d and type = 8 order by ctime asc limit %d,%d" % (src_time,i,limit)
                row = gamedb.fetchrow(sql)
                if row:
                    for res in row:
                        arr.append("('%d','%d','%s','%s','%s','%d','%s','%d','%s','%d','%d','%d','%d','%d','%s','%s')" \
                                   % (gid,sid,res['pay_no'],res['cp_no'],res['accname'],res['role_id'],res['name'],res['level'],\
                                      res['source'],res['rmb'],res['gold'],res['ctime'],res['ctime'],res['type'],res['product_id'],res['remark']))
                        last_insert = res['ctime']
                    insert_sql_value = ','.join(arr)
                    # 充值表
                    insert_sql = "insert ignore into center_charge(gid,sid,pay_no,cp_no,accname,role_id,nickname,lv,source,money,gold,ctime,itime,type,product_id,ext) values" + insert_sql_value
                    admindb.execute(insert_sql)
                else:
                    break
                i += limit
            if last_insert:
                if src_time == 0:
                    last_sql = "insert into center_last_insert (sid,src_table,src_time) values('%d','%s','%d')" % (sid,src_table,last_insert)
                    #print last_sql
                    admindb.execute(last_sql)
                else:
                    last_sql = "update center_last_insert set src_time= '%d' where sid='%d' and src_table='%s'" % (last_insert,sid,src_table)
                    #print last_sql
                    admindb.execute(last_sql)
        except:
            print db,'error'
        finally:
            admindb.commit()
            gamedb.close()
            admindb.close()
            log = logger('center_charge','success','info').info('pid:%s, host:%s, db:%s Finish' % (os.getpid(),host,db))
            print '%s %s Finish' % (host,db)

if __name__ == '__main__':
        base.multiprocess(center_charge,'center_charge')



