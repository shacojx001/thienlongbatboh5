from basemodule import basemodule
from logger import logger
import json
import os
from collections import OrderedDict

base = basemodule()


def auto_role_level_retain(fun, host, port, user, pwd, db, sid, gid, otime, srv_num):
    try:
        gamedb = base.game_link(host, port, user, pwd, db)
        admindb = base.admin_link();
        today = base.today()
        date_time = base.today() - 86400
        one_day_time = [date_time - 86400, date_time - 86400 * 2]
        three_day_time = [date_time - 86400 * 3, date_time - 86400 * 4]
        sevent_day_time = [date_time - 86400 * 7, date_time - 86400 * 8]
        sql_pay = "select " \
                  "max(a.logout_time) as time ,max(a.role_lv) as lv,a.role_id " \
                  "from log_logout a left join charge b " \
                  "on a.role_id = b.role_id " \
                  "where b.role_id is not null " \
                  "group by role_id order by lv asc"
        all_sql = "select count(distinct a.role_id) as all_count from role_base a left join charge b on a.role_id = b.role_id  where b.role_id is not null and a.reg_time<%d" % (today)

        get(sql_pay, all_sql,date_time, gid, sid, gamedb, admindb, one_day_time, three_day_time, sevent_day_time, 1)

        sql_no_pay = "select " \
                  "max(a.logout_time) as time ,max(a.role_lv) as lv,a.role_id " \
                  "from log_logout a left join charge b " \
                  "on a.role_id = b.role_id " \
                  "where b.role_id is null " \
                  "group by role_id order by lv asc"
        all_sql = "select count(distinct a.role_id) as all_count from role_base a left join charge b on a.role_id = b.role_id  where b.role_id is null and a.reg_time<%d" % (today)
        get(sql_no_pay,all_sql,date_time, gid, sid, gamedb, admindb, one_day_time, three_day_time, sevent_day_time, 2)


    except Exception, e:
        print e
        log = logger('auto_role_level_retain', 'sync.all.log', 'error').error('%s %s error:%s' % (host, db, e))
    finally:
        admindb.commit()
        gamedb.close()
        admindb.close()
        logger('compute_role_level_retain', 'sync.all.log', 'info').info(
            'pid:%s, host:%s, db:%s Finish' % (os.getpid(), host, db))
        print '%s %s Finish' % (host, db)

def get(sql,all_sql,date_time,gid,sid,gamedb,admindb,one_day_time,three_day_time,sevent_day_time,type):
    res = gamedb.fetchrow(sql)
    one_lv_count = OrderedDict()
    three_lv_count = OrderedDict()
    sevent_lv_count = OrderedDict()
    lv_count = OrderedDict()
    for data in res:
        time = data['time']
        lv = data['lv']
        if lv in lv_count:
            lv_count[lv] += 1
        else:
            lv_count[lv] = 1
        if time < one_day_time[0]:
            if lv in one_lv_count:
                one_lv_count[lv] += 1
            else:
                one_lv_count[lv] = 1
        if time < three_day_time[0]:
            if lv in three_lv_count:
                three_lv_count[lv] += 1
            else:
                three_lv_count[lv] = 1
        if time < sevent_day_time[0]:
            if lv in sevent_lv_count:
                sevent_lv_count[lv] += 1
            else:
                sevent_lv_count[lv] = 1
    one_json = json.dumps(one_lv_count)
    three_json = json.dumps(three_lv_count)
    sevent_json = json.dumps(sevent_lv_count)
    lv_count_json = json.dumps(lv_count)

    all_count_res = gamedb.fetchone(all_sql)
    all_count = all_count_res['all_count']

    sql = "insert into center_lv_retain(time,gid,sid,one_day,three_day,sevent_day,all_count,lv_all_count,type) " \
          "values(%d,%d,%d, '%s', '%s', '%s',%d, '%s',%d)" % (
              date_time, gid, sid, one_json, three_json, sevent_json, all_count, lv_count_json,type)
    admindb.query(sql);


if __name__ == '__main__':
    base.multiprocess(auto_role_level_retain, 'auto_role_level_retain')
