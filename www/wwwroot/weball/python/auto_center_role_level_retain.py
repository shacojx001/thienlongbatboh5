#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from basemodule import basemodule
import time
import sys
import os
from logger import logger
from collections import OrderedDict

base = basemodule()


def compute_role_level_retain(fun, host, port, user, pwd, db, sid, gid, otime, srv_num):
    try:
        # 连接数据库
        gamedb = base.game_link(host, port, user, pwd, db)
        admindb = base.admin_link();
        #昨天
        day = get_zore_point(1);

        # 时间范围
        day_list = OrderedDict()
        # 前天
        day_list['one_day'] = get_zore_point(2)
        # 前4天
        day_list['three_day'] = get_zore_point(4)
        day_list['seven_day'] = get_zore_point(8)

        # 查询各个等级的总数
        #level_total_sql = "select count(*) as total ,level ,last_login_time as time from role_base a where reg_time < %s group by level order by level asc" % (day+86400);
        level_total_sql = "select count(1) as total ,level ,last_login_time as time from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id  where ctime < %s group by level order by level asc" % (day+86400);
        # print level_total_sql
        # 查询所有的角色
        #one_day_total_sql = "select count(*) as total ,level ,last_login_time as time from role_base a where last_login_time < %s and reg_time < %s group by level order by level asc" % (
        #day_list['one_day'],day+86400)
        one_day_total_sql = "select count(1) as total ,level ,last_login_time as time from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id  where last_login_time < %s and ctime < %s group by level order by level asc" % (
        day_list['one_day'],day+86400)
        # print one_day_total_sql
        #three_day_total_sql = "select count(*) as total ,level ,last_login_time as time from role_base a where last_login_time < %s and reg_time < %s group by level order by level asc" % (
        #day_list['three_day'],day+86400)
        three_day_total_sql = "select count(1) as total ,level ,last_login_time as time from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id  where last_login_time < %s and ctime < %s group by level order by level asc" % (
        day_list['three_day'],day+86400)
        # print three_day_total_sql
        #seven_day_total_sql = "select count(*) as total ,level ,last_login_time as time from role_base a where last_login_time < %s and reg_time < %s group by level order by level asc" % (
        #day_list['seven_day'],day+86400)
        seven_day_total_sql = "select count(1) as total ,level ,last_login_time as time from role_basic b LEFT JOIN role_create c on b.role_id=c.role_id  where last_login_time < %s and ctime < %s group by level order by level asc" % (
        day_list['seven_day'],day+86400)
        # print seven_day_total_sql

        res = gamedb.fetchrow(level_total_sql);
        one_day_res = gamedb.fetchrow(one_day_total_sql);
        three_day_res = gamedb.fetchrow(three_day_total_sql);
        seven_day_res = gamedb.fetchrow(seven_day_total_sql);

        # 将各个等级的总数进行排序并按范围排序
        level_range_list = sort_res(res)
        # 统计各个等级一天没有登陆的人数
        one_day_list = sort_res(one_day_res)
        # 统计各个等级三天没有登陆的人数
        three_day_list = sort_res(three_day_res)
        # 统计各个等级7天没有登陆的人数
        seven_day_list = sort_res(seven_day_res)
        # 开始插入数据
        for data in level_range_list:
            if (one_day_list.has_key(data)!=True):
                one_total = 0
            else:
                one_total = one_day_list[data][0];
            if (three_day_list.has_key(data)!=True):
                three_total = 0
            else:
                three_total = three_day_list[data][0];
            if (seven_day_list.has_key(data)!=True):
                seven_total = 0
            else:
                seven_total = seven_day_list[data][0];
            # print level_range_list[data][0]
            insert_sql = "replace into center_role_level_retain (time,gid,sid,level_range,total,one_not_login,three_not_login,seven_not_login)" \
                         "values(%s,%d,%d,'%s',%d,%d,%d,%d)" \
                         % (day, gid, sid, level_range_list[data][1], level_range_list[data][0], one_total, three_total,
                            seven_total)
            res = admindb.query(insert_sql)
    except Exception, e:
        print e
        log = logger('compute_role_level_retain', 'sync.all.log', 'error').error('%s %s error:%s' % (host, db, e))
    finally:
        admindb.commit()
        gamedb.close()
        admindb.close()
        logger('compute_role_level_retain', 'sync.all.log', 'info').info(
            'pid:%s, host:%s, db:%s Finish' % (os.getpid(), host, db))
        print '%s %s Finish' % (host,db)

def get_zore_point(day):
    if len(sys.argv) == 2:
        cur_time = isVaildDate(sys.argv[1])
        zore_point = (cur_time - cur_time % 86400) - 86400 * day - 60 * 60 * 8
    else:
        zore_point = base.today() - 86400*day

    return zore_point


def isVaildDate(date):
    try:
        date = date + " 10:00:00"
        timeArray = time.strptime(date, "%Y-%m-%d %H:%M:%S")
        return int(time.mktime(timeArray))
    except:
        print("date error")


# 确定数字的范围（20为一个范围）
def check_range(index):
    if index == 0:
        return 1, 20

    a = 1
    if index % 20 == 0:
        a = index / 20
        if a == 1:
            b = 1
        else:
            b = (a - 1) * 20 + 1

    else:
        a = (index - index % 20) / 20
        b = a * 20 + 1

    c = b + 19

    return (b, c)


def sort_res(res):
    level_range_list = OrderedDict()
    if len(res) == 0:
        return level_range_list

    # 初始值
    total = 0;
    s, b = check_range(res[0]['level'])
    # print s,b
    # 该范围的key
    last_range = "%d~%d" % (s, b)
    last_s = s;
    last_b = b;
    for count in res:
        # 等级为0则不进行统计
        # print last_range
        if count['level'] == 0:
            continue
        # 获得该等级所在的范围
        s, b = check_range(count['level'])
        # print s,b
        # 该范围的key
        range_ = "%d~%d" % (s, b)
        # 是否循环完一个等级范围
        if last_range != range_:
            level_range = [];
            level_range.append(total)
            level_range.append(last_range)
            level_range.append(last_s)
            level_range.append(last_b)
            level_range_list[last_range] = level_range
            total = 0
        total += count['total']
        last_range = "%d~%d" % (s, b)

    level_range = [];
    level_range.append(total)
    level_range.append(range_)
    level_range.append(s)
    level_range.append(b)
    level_range_list[range_] = level_range

    # 返回结果
    # print level_range_list
    return level_range_list


if __name__ == '__main__':
    base.multiprocess(compute_role_level_retain, 'compute_role_level_retain')
