#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
from basemodule import basemodule
from logger import logger
import os

base = basemodule()

def center_role_login(fun,host,port,user,pwd,db,sid,gid,otime,srv_num):
        #print host,port,user,pwd,db,sid,gid,os.getpid()
        try:
            gamedb = base.game_link(host,port,user,pwd,db)
            admindb = base.admin_link()

            # 数据源表
            src_table = 'log_login'

            centersql = "select src_time from center_last_insert where sid = '%d' and `src_table` = '%s' order by src_time desc limit 1" % (sid,src_table)
            srcinfo = admindb.fetchone(centersql)
            if srcinfo:
                src_time = int(srcinfo['src_time']) - 600
            else:
                src_time = 0
            limit = 2000
            i = 0
            last_insert = 0
            while True:
                arr = []
                sql = "select accname,source,l.role_id,l.career,level,device,l.ip,login_time time from log_login l LEFT JOIN role_create c on l.role_id=c.role_id where login_time >= %d order by login_time asc limit %d,%d " % (src_time,i,limit)
                row = gamedb.fetchrow(sql)
                if row:
                    for res in row:
                        if res['device'] == None:
                            res['device'] = ''
                        arr.append("('%d','%d','%d','%s','%s','%d','%d','%d','%s','%s')" % (gid,sid,res['role_id'],res['accname'],res['source'],res['time'],res['career'],res['level'],res['ip'],res['device']))
                        last_insert = res['time']

                    insert_sql_value = ','.join(arr)
                    insert_sql = "insert ignore into center_role_login(gid,sid,role_id,acc_name,channel,time,career,level,ip,device) values" + insert_sql_value
                    admindb.execute(insert_sql)
                else:
                    break
                i += limit
            print last_insert
            if last_insert:
                if src_time == 0:
                    last_sql = "insert into center_last_insert (sid,src_table,src_time) values('%d','%s','%d')" % (sid,src_table,last_insert)
                    admindb.execute(last_sql)
                else:
                    last_sql = "update center_last_insert set src_time= '%d' where sid='%d' and src_table='%s'" % (last_insert,sid,src_table)
                    admindb.execute(last_sql)
        except:
            print db,'error'
        finally:
            admindb.commit()
            gamedb.close()
            admindb.close()
            log = logger('center_role_login','success','info').info('pid:%s, host:%s, db:%s Finish' % (os.getpid(),host,db))
            print '%s %s Finish' % (host,db)

if __name__ == '__main__':
        base.multiprocess(center_role_login,'center_role_login')



