#!/bin/sh
# 定时执行Python的脚本（当前的shell脚本是每分钟执行一次的）
cd $(dirname `/usr/sbin/lsof -p $$ | gawk '$4 =="255r"{print $NF}'`)
function run
{
	# 每n分钟执行一次x.py(00-59)
	run_by_min center_online_channel.py 2   #每2分钟抓一次在线人数(修复)
	run_by_min center_role.py 5 #每5分钟抓一次角色表(修复)
	run_by_min center_role_login.py 5 #每5分钟抓一次登录日志(修复)
	# run_by_min auto_player_suggestion.py 2 	#每2分钟抓意见反馈
	run_by_min center_charge.py 5 #没5分钟抓一次(等待修复)

	# 除去指定时间,每n分钟执行一次 例：file.py 10 23 50 1800


	# 每n小时执行一次x.py(0-23)
	#run_by_hour center_market_notice.py 1 #每小时统计一次市场交易预警

	# 定时定分执行x.py
	run_at_time auto_center_role_level_retain.py 1 10 #玩家按等级统计流失(修复)
	#run_at_time center_new_level_retain.py 1 20
	#run_at_time auto_role_retain_center.py 1 14     #留存统计

    run_at_time center_upgrade_on_opensevr.py 0 2 #开服升级分布(修复)

    run_at_time center_server_power.py 0 4 #各服战力情况(修复)
}

# 所有每分要执行的脚本
function run_by_min
{
	sleep 1
	# 当前分钟数
	min=`date +%M`
	min=$(( 10#$min ))
	
	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}
	
	# 每t分钟执行一次
	if [ $t == 0 -a $min == $t ]; then
		python $file &
	elif [ $t != 0 -a $(( ${min} % $t )) == 0 ]; then
		python $file &
	fi
}



function run_by_min_m
{
        sleep 1
        # 当前分钟数
        data=`date +%Y-%m-%d`
        min=`date +%M`
        hour=`date +%H`

        echo $data
        # 传入的变量 file为要执行的文件 t为时间
        local file=${1}
        local t=${2}

        local ehour=${3}
        local emin=${4}
        local elasting=${5}

        local TimeStampB=`date -d$data\ $ehour:$emin:'0' +%s`
        local TimeStampE=`expr $TimeStampB + $elasting`

        local TimeStampB2=`expr $TimeStampB - 86400`
        local TimeStampE2=`expr $TimeStampE - 86400`


        # 每t分钟执行一次
        local NowTimeStamp=`date +%s`
        b=$(( $min % $t ))
        #[ ${b} -eq 0 ] && 
        #[![[${NowTimeStamp} -ge ${TimeStampB}] && [${NowTimeStamp} -le ${TimeStampE}]]] && [![[${NowTimeStamp} -ge ${TimeStampB2}] && [${NowTimeStamp} -le ${TimeStampE2}]]]
        if [ ${b} -eq 0 ] && (! ([ ${NowTimeStamp} -ge ${TimeStampB} ] && [ ${NowTimeStamp} -le ${TimeStampE} ]) )  && (! ( [ ${NowTimeStamp} -ge ${TimeStampB2} ] && [ ${NowTimeStamp} -le ${TimeStampE2} ] ) ); then
                   python $file
        fi
}

# 所有每小时要执行的脚本
function run_by_hour
{
	sleep 1
	# 当前小时数
	hour=`date +%H`

	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}

	# 每t分钟执行一次
	b=$(( $hour % $t ))
	if [ ${b} -eq 0 ] ; then 
		   python $file &
	fi
}

# 定点定时执行
function run_at_time
{
	sleep 1
	# 当前小时数
	hour=`date +%H`
	# 当前分钟数
	min=`date +%M`

	# 传入的变量 file为要执行的文件 h 为小时 m 分钟
	local file=${1}
	local h=${2}
	local m=${3}

	if [[ ${hour} -eq ${h} && ${min} -eq ${m} ]] ; then
		   python $file &
	fi
}

run
