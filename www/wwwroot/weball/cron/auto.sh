#!/bin/sh

# 定时执行清除php的脚本（当前的shell脚本是每分钟执行一次的）
cd $(dirname `/usr/sbin/lsof -p $$ | gawk '$4 =="255r"{print $NF}'`)
function run
{
	# 每n分钟执行一次x.php(0-59) run_by_min file.php 1

    # 除去指定时间,每分钟执行一次 run_by_min_m file.php 5 23 50 1800

    # 每n小时执行一次 run_by_hour file.php(0-23)

    # 定时定分执行 run_at_time file.php 3 2

}

# 所有每分要执行的脚本
function run_by_min
{
	sleep 1
	# 当前分钟数
	min=`date +%M`

	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}

	# 每t分钟执行一次
	b=$(( $min % $t ))
	if [ ${b} -eq 0 ] ; then 
		   php $file
	fi
}


function run_by_min_m
{
        sleep 1
        # 当前分钟数
        data=`date +%Y-%m-%d`
        min=`date +%M`
        hour=`date +%H`

        echo $data
        # 传入的变量 file为要执行的文件 t为时间
        local file=${1}
        local t=${2}

        local ehour=${3}
        local emin=${4}
        local elasting=${5}

        local TimeStampB=`date -d$data\ $ehour:$emin:'0' +%s`
        local TimeStampE=`expr $TimeStampB + $elasting`

        local TimeStampB2=`expr $TimeStampB - 86400`
        local TimeStampE2=`expr $TimeStampE - 86400`


        # 每t分钟执行一次
        local NowTimeStamp=`date +%s`
        b=$(( $min % $t ))
        #[ ${b} -eq 0 ] && 
        #[![[${NowTimeStamp} -ge ${TimeStampB}] && [${NowTimeStamp} -le ${TimeStampE}]]] && [![[${NowTimeStamp} -ge ${TimeStampB2}] && [${NowTimeStamp} -le ${TimeStampE2}]]]
        if [ ${b} -eq 0 ] && (! ([ ${NowTimeStamp} -ge ${TimeStampB} ] && [ ${NowTimeStamp} -le ${TimeStampE} ]) )  && (! ( [ ${NowTimeStamp} -ge ${TimeStampB2} ] && [ ${NowTimeStamp} -le ${TimeStampE2} ] ) ); then
                   php $file
        fi
}

# 所有每小时要执行的脚本
function run_by_hour
{
	sleep 1
	# 当前小时数
	hour=`date +%H`

	# 传入的变量 file为要执行的文件 t为时间
	local file=${1}
	local t=${2}

	# 每t分钟执行一次
	b=$(( $hour % $t ))
	if [ ${b} -eq 0 ] ; then 
		   php $file 
	fi
}

# 定点定时执行
function run_at_time
{
	sleep 1
	# 当前小时数
	hour=`date +%H`
	# 当前分钟数
	min=`date +%M`

	# 传入的变量 file为要执行的文件 h 为小时 m 分钟
	local file=${1}
	local h=${2}
	local m=${3}

	if [[ ${hour} -eq ${h} && ${min} -eq ${m} ]] ; then
		   php $file
	fi
}

run
