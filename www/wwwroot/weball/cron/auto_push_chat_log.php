<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/1/26
 * Time: 14:58
 */

include_once("bs.php");
include_once(dirname(dirname(__FILE__)) . "/lib/Tools/Redis.php");


class auto_push_chat_log
{
    private $last_read_date;        //记录上次读取的日期
    private $last_read_hour;        //记录上次读取的小时数
    private $read_line = 1;          //读取的行数
    private $line_file_dir;         //防止漏读，重读，将实时读取的行数写入到文件中，每次切换小时，切换天，重启都先读取这个文件。

    public function __construct()
    {
        // $this->line_file_dir = ROOT_PATH.'php/cron/line_file.txt';
        // $this->log_file_dir = ROOT_PATH.'php/cron/chat_log_error.txt';
        //本地
        $this->line_file_dir = ROOT_PATH.'server_game/cron/line_file.txt';
        $this->log_file_dir = ROOT_PATH.'server_game/cron/chat_log_error.txt';
    }

    public function start_push($date,$read_old)
    {
        $sql = "select cf_name,cf_value from base_game where cf_name = 'gid' OR cf_name = 'sid'";
        $db = Ext_Mysql::getInstance('game');       //获取服务器信息
        $res = $db->fetchRow($sql);

        $gid = 0;
        $sid = 0;
        if($res == null){        //如果服没开就直接退出
            print_r('----------server---not---open---------');
            exit();
        }
        print_r($res);
        foreach ($res as $val) {
            if ($val['cf_name'] == 'gid') {
                $gid = $val['cf_value'];
            }
            if ($val['cf_name'] == 'sid') {
                $sid = $val['cf_value'];
            }
        }     
        $this->get_line();
        if($read_old){
            print_r('------------start---push---old----data-------');
            $this->read_and_push_old($date,$gid,$sid);  //跑旧数据
        }else{
            print_r('------------start---push---realtime----data-------');
            $this->read_and_push_real_time($gid,$sid);    //跑实时数据
        }
    }

    public function read_and_push_old($date,$gid,$sid){        //跑旧数据
        //直接读取该天下的所有文件，不考虑实时性
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $now_date = $year . '_' . $month . '_' . $day;    //这样的格式跟服务器上的格式相同
        $now_hour = date('H');
//        $dir = "/home/chatlogs/$date/";
        $dir = ROOT_PATH."game/log/chat/";
        for ($i = 0; $i < 24; $i++) {
            $redis = Redis_Server::getInstance();
            $file_name = 'chatlog_' . $date . '_' . $i . '.txt';      //文件名
            $file_path = $dir . $file_name;                       //文件路径
            print_r(array($file_path,$now_date,$date,$now_hour,$i));
            if ($date.'_'.$i < $now_date.'_'.$now_hour) {     //
                $data = $this->read_and_push($file_path,$gid, $sid);
                if ($data) {
                    try{                 // catch到redis的异常。有异常则写入到日志文件里
                        $index = $redis->push(CHAT_LOG_KEY, $data);
                        if($index ==0){
                            file_put_contents($this->log_file_dir,'Error push redis----'.$file_path.'-------'.$data."\r\n",FILE_APPEND);
                        }
                    }catch (Exception $e){
                        file_put_contents($this->log_file_dir,'Wrong redis----'.$file_path.'-------'.$data."\r\n",FILE_APPEND);
                        exit;
                    }
                }
            }
        }
    }

    public function read_and_push($file_path, $gid, $sid)       //读取整个文件  跑旧数据用
    {

        if (is_file($file_path)) {
            $data = array();
            $fp = fopen($file_path, "r");
            while (!feof($fp)) {
                $str = fgets($fp);//逐行读取。如果fgets不写length参数，默认是读取1k。
                if($str == ''){
                    continue;
                }
                $log = $this->format_data($str,$gid,$sid);
                array_push($data, $log);
            }
            return json_encode($data);
        }
    }

    public function read_and_push_real_time($gid, $sid)        //获取实时数据
    {
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $now_date = $year . '_' . $month . '_' . $day;
        $now_hour = date('H');
        $this->last_read_date = $now_date;
        $this->last_read_hour = $now_hour;
        $n = 1;
        while (true) {
            //获取当前时间
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $now_date = $year . '_' . $month . '_' . $day;
            $now_hour = date('H');
            $file_name = 'chat_' . $now_date . '_' . $now_hour . '.txt';
            $dir = ROOT_PATH."game/log/chat/";
            $file_path = $dir . $file_name;
            $redis = Redis_Server::getInstance();
            print_r(array($file_path,$this->last_read_date,$now_date,$this->last_read_hour,$now_hour,$this->read_line)); //打印

            if ($this->last_read_date.'_'.$this->last_read_hour == $now_date.'_'.$now_hour) {        //相同小时直接读取同一个文件
              print_r('----read----data---');
              
                $this->read_data($file_path,$redis,$gid,$sid);
              
              print_r('end');exit;  
            } else{      //不同小时数先读取上次读取的文件，再读取当前小时数的文件
                print_r('----read----data---diff--data--');
                $last_dir = ROOT_PATH."game/log/chat/$this->last_read_date/";
                $last_file_name = 'chat_' . $this->last_read_date . '_' . $this->last_read_hour . '.txt';
                $last_file_path = $last_dir.$last_file_name;
                $this->read_data($last_file_path,$redis,$gid,$sid,false);
print_r(22222);exit;
                $this->read_line = 1;
                $this->last_read_date = $now_date;
                $this->last_read_hour = $now_hour;
                $this->read_data($file_path,$redis,$gid,$sid);
            }
            sleep(1);
        }
    }

    public function read_data($file_path,$redis,$gid,$sid,$flag=true)
    {
        if (is_file($file_path)) {
            $result = get_file_for_one_line($file_path, $this->read_line);  //获取指定行到文件末尾的数据
        
            if (!empty($result['data'])) {      //如果无数据则不做操作
                $data = array();
                foreach ($result['data'] as $val) {         //读取
                    $log = $this->format_data($val,$gid,$sid);
                    array_push($data, $log);
                } 
                try{
                    $index = $redis->push(CHAT_LOG_KEY, json_encode($data));
print_r("\r\n");
print_r($data);
print_r("\r\n");
print_r('$index：',$index);
                    if($index ==0){
                        file_put_contents($this->log_file_dir,'Error push redis--'.$file_path.'--'.$this->read_line.'--'.$result['line'].'--'.json_encode($data)."\r\n",FILE_APPEND);
                    }
                }catch(Exception $e){
                    file_put_contents($this->log_file_dir,'Wrong redis-----'.$file_path.'-----'.$this->read_line.'----'.$result['line'].json_encode($data)."\r\n",FILE_APPEND);
                    exit;
                }
                $this->read_line  = $result['line']+1;

                if($flag){
                    $this->set_line();
                }
            }
        }
    }

    public function format_data($str, $gid, $sid)
    {
        $str = trim($str);
        $data = json_decode($str, true);
        //时间戳、频道、帮派id、帮派名字、发送者角色id、发送者昵称、发送者账号、渠道、发送者是否付费玩家、接收者角色id、接收者昵称、接收者账号、聊天内容、发送者IP、发送者等级、接受者等级       
        $data['gid'] = $gid;
        $data['sid'] = $sid;
        return $data;
    }

    //开始跑数据获取上次读取的行数跟日期
    public function get_line(){
        if(is_file($this->line_file_dir)){    //如果文件不存在则从第一行开始读取
            $fp = fopen($this->line_file_dir, "r");
            while (!feof($fp)) {
                $line = fgets($fp);
                $arr = explode('_',$line);
                if($arr[0] == date('Y-m-d-H')){   //查看记录到的行数是不是在当前小时，如果不是则直接从第一行开始读取
                    $this->read_line = $arr[1];
                }else{
                    $this->read_line = 1;
                }
                break;
            }
            fclose($fp);
        }else{
            $this->read_line = 1;
        }
    }

    //写入行数到文件
    public function set_line(){
        $line = date('Y-m-d-H').'_'.$this->read_line;
        $fp = fopen($this->line_file_dir, "w");
        fwrite($fp,$line);
        fclose($fp);
    }

}

$a = new auto_push_chat_log();
if (isset($argv[1])) {      //如果输入有参数，则使用参数的日期
    $date = $argv[1];
    $read_old = true;
} else {     //默认为当前天
    $year = date('Y');
    $month = date('m');
    $day = date('d');
    $date = $year . '_' . $month . '_' . $day;
    $read_old = false;
}
// print_r($date);exit;
$a->start_push($date,$read_old);