<?php
/**
 * 聊天监控守护进程
 *
 * Created by PhpStorm.
 */
$file =  dirname(__FILE__).'/auto_push_chat_log.php';
if(empty($argv[1])){
    $re = exec("ps -ef | grep '$file' | grep -v grep",$output,$return_var);
    if(!$re){
        echo "starting...\n";
        exec("/usr/local/php/bin/php $file >/dev/null 2>&1 &",$output,$return_var);
        echo "OK~";
    }else{
        echo "deamon is already running!";
    }
}else if($argv[1] == 'stop'){
    $re = exec("ps -ef | grep '$file' | grep -v grep",$output,$return_var);
    if(!$re){
        echo "deamon is not running!";
    }else{
        exec('ps -ef |grep "'.$file.'" | grep -v "grep"| awk \'{print $2}\'|xargs kill -9',$output,$return_var);
         //exec("/usr/local/php/bin/php $file >null &",$output,$return_var);
    }
}else if($argv[1] == 'restart'){
    $re = exec("ps -ef | grep '$file' | grep -v grep",$output,$return_var);
    if(!$re){
        echo "starting...\n";
        exec("/usr/local/php/bin/php $file >/dev/null 2>&1 &",$output,$return_var);
        echo "OK~";
    }else{
        exec('ps -ef |grep "'.$file.'" | grep -v "grep"| awk \'{print $2}\'|xargs kill -9',$output,$return_var);
        exec("/usr/local/php/bin/php $file >/dev/null 2>&1 &",$output,$return_var);
    }
}


