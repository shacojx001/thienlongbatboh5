<?php
/**
 * 游戏服渠道数据汇总5分钟1次
 */
include(dirname(dirname(__FILE__))."/html/bs.php");
$db = Ext_Mysql::getInstance('game');

       $arr = array();

       $strat_time = strtotime(date("Y-m-d"));

       $yesterday = strtotime(date("Y-m-d"))-60*60*24;

       $url = dirname(dirname(__FILE__))."/html/js";  


       $file  = $url.'/auto_monitor.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个
       
       //打开文件，取最大在线跟时间戳

       $diff = json_decode(file_get_contents($file));


	   //当前在线
	   $sql = " SELECT
						count(*) AS num
					FROM
						role_login
					WHERE
						online_flag = 1
				"; 
	   $res = $db->fetchOne($sql); 
       
       //判断时间跟最大值
       if($diff->time==$strat_time && $res['num']>$diff->top_num){
       	   
       	   $arr['top_num'] =  $res['num']; 
       	   
       }elseif($diff->time<$strat_time){

       	   $arr['top_num'] =  $res['num']; 

       }
       else{
       	   $arr['top_num'] = $diff->top_num;
       } 

       ///昨日最高在线
       $sql = "SELECT
                            max(total) AS total
                     FROM
                            log_online
                     WHERE
                            `timestamp` >= $yesterday
                     AND `timestamp` <= $strat_time ";
       $res2 = $db->fetchOne($sql);              

       $arr['s_num'] =  $res['num'];  

       $arr['yesterday_num'] =  $res2['total'];  

       $arr['time'] =  strtotime(date("Y-m-d"));//当天零点  

       $arr['load'] = `top -b -n 1|awk -F':|,' '/load average/ {print $8}' `; 

       $arr['space_all'] = `df -h|awk '/sda3/ {print $2}'`; 

       $arr['space_use'] = `df -h|awk '/sda3/ {print $3}'`; 

       $arr['mem_all'] = `top -b -n 1|awk -F':|,' '/Mem/ {print $2}'`; 

       $arr['mem_free'] = `free |awk '/+ buffers/ {print $3}'`;

       

	   $content =  json_encode($arr); 
		    
	   file_put_contents($file, $content);// 这个函数支持版本(PHP 5) 


echo "auto_monitor.php文件执行时间：".date('Y-m-d H:i:s')."\r\n";
?>
