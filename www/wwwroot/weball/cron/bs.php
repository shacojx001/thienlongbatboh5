<?php
/**
 * 后台用 - admin
 * 启动文件
 */
//define("DEBUG", true);

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);


//header('Content-Type: text/html; charset=utf-8');//字符集
date_default_timezone_set("Asia/Shanghai");//时区


define("ROOT_PATH", dirname(dirname(dirname(__FILE__))) . "/");

define('ROOT_DIR', dirname(dirname(__FILE__))."/");
// web路径


define('LIB_DIR', ROOT_DIR . 'lib/');
// 服务路径，将作为 AMF 调用发布


define('LOG_DIR', ROOT_DIR . 'log/');
// 模块目录


// 配置文件目录
define('CONFIG_DIR', ROOT_DIR . 'config/');

define("CHAT_LOG_KEY",'chat_log_key_vtnemo');


set_include_path( PATH_SEPARATOR . ROOT_DIR.'lib'
				. PATH_SEPARATOR . ROOT_DIR.'lib/Tools'
				. PATH_SEPARATOR . ROOT_DIR.'service'
			    . PATH_SEPARATOR . get_include_path());


// include_once CONFIG_DIR.'servers.cfg.php';
include_once LIB_DIR.'Helper/String.php';
spl_autoload_register("autoload");
