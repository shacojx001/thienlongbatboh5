<?php
/**
 * 全服数据抓取脚本
 */
include(dirname(dirname(__FILE__)) . "/admin/bs.php");
include(dirname(dirname(__FILE__)) . "/admin/action/superclass.php");
include(dirname(dirname(__FILE__)) . "/config/servers.cfg.php");

class auto_get_custom_data extends action_superclass
{
    private static $cleanstatus;

    public function __construct()
    {
        $this->admindb = Ext_Mysql::getInstance('admin');
        //$this->allcenter = Ext_Mysql::getInstance('allcenter');
    }

    /**
     * 全服数据抓取脚本
     */
    public function run()
    {
        ini_set('memory_limit', '2048M');
        ////////游戏服选择
                $time = time();//当前时间戳
                $sql = "SELECT
                            s.id,
                            p.platform_name,
                            s.description
                        FROM
                            adminserverlist s
                        LEFT JOIN adminplatformlist p ON s.gid = p.gid
                        WHERE
                            `default` = 1
                        AND otime<$time
                        AND s.gid=34
                        ORDER BY
                            p.gid ASC,
                            s.id ASC";

        ////////游戏服选择
        $res2 = $this->admindb->fetchRow($sql);
        $list = array();
        $data = "";
        $sign = '"';
        foreach ($res2 as $key => $val) {
        ////////查询具体数据
                    $sql = "SELECT
            m.guild_id 帮会ID,
            `name` 帮会名,
            count(role_id) 帮会人数
        FROM
            guild_member m
        LEFT JOIN guild g ON m.guild_id = g.id
        where guild_id!=0
        GROUP BY
            guild_id
        ORDER BY
            count(role_id) DESC";
        ////////查询具体数据
            $res = Config::gamedb($val['id'])->fetchRow($sql);

            if (count($res) > 0) {
                foreach ($res as $k => $v) {

                    $base =
                        array(
                            '平台' => $val['platform_name'],
                            '服名' => $val['description'],
                        );
                    $list[] = array_merge($base, $v);
                }

            }
        }
        $line1 = array_keys($list[0]);
        $data .= implode("\t", $line1);
        $data .= "\r\n";
        foreach ($list as $key => $val) {
            $line = array();
            foreach ($val as $k => $v) {
                $line[] = $v;
            }
            $data .= implode("\t", $line);
            $data .= "\r\n";

            // sleep(4);
        }
        file_put_contents(ROOT_DIR . 'cron/data_mining.txt', $data);

        echo 'ok';
        exit;

    }
}

$auto = new auto_get_custom_data();
$auto->run();
?>