<?php

include('bs.php');

function log_write($text, $type = 'a'){
    if (!is_dir(ROOT_DIR))
    {
        return false;
    }
    $filename = ROOT_DIR.'write.txt';
    $text = "\r\n++++++++++++++++++++++++++++++++++++++++++\r\n"
        . date('Y-m-d H:i:s') . "\r\n"
        . print_r($text, true);

    if (@$fp = fopen($filename, $type))
    {
        flock($fp, 2);
        fwrite($fp, $text);
        fclose($fp);
        return true;
    }
    else
    {
        return false;
    }

}


//erl编译标识文件 和 编译日志文件
$erl_compile_flag = ROOT_PATH . 'flag/flag_compile_erl.txt';
$erl_compile_log = ROOT_PATH . 'log/log_compile_erl.txt';
$flag = file_get_contents($erl_compile_flag);
// print_r($erl_compile_log);exit;
//隔5秒请求一次编译日志进度内容，包括Erlang和flash的日志
if($_REQUEST['ac'] == 'get_compile_log'){

    $serverlog = file_get_contents($erl_compile_log);
    // $flag = file_get_contents($erl_compile_flag);
    echo json_encode(array('flag'=>$flag, 'serverlog'=>nl2br($serverlog)));
    exit;
}
// print_r(trim(file_get_contents($erl_compile_flag)));exit;
//编译erlang按钮显示检查：检查是否在编译Erlang中，是的话按钮灰掉
$erl_flag = trim(file_get_contents($erl_compile_flag));
if ($erl_flag == "1") {
    $compile_erlang_text = "编译Erlang中...";
    $quick_compile_erlang_text = "编译Erlang中...";
    $hot_erlang_text = "编译Erlang中...";
    $gcode_text = "编译Erlang中...";
    $compile_erlang_button = "disabled='disabled'";
}else if($erl_flag == "2"){
    $compile_erlang_text = "快速编译Erlang中...";
    $quick_compile_erlang_text = "快速编译Erlang中...";
    $hot_erlang_text = "快速编译Erlang中...";
    $gcode_text = "快速编译Erlang中...";
    $compile_erlang_button = "disabled='disabled'";
}else if($erl_flag == "3"){
    $compile_erlang_text = "热更中...";
    $quick_compile_erlang_text = "热更中...";
    $hot_erlang_text = "热更中...";
    $gcode_text = "热更中...";
    $compile_erlang_button = "disabled='disabled'";
}else if($erl_flag == "4"){
    $compile_erlang_text = "生成协议中...";
    $quick_compile_erlang_text = "生成协议中...";
    $hot_erlang_text = "生成协议中...";
    $gcode_text = "生成协议中...";
    $compile_erlang_button = "disabled='disabled'";
}else {
    $compile_erlang_text = "编译Erlang";
    $quick_compile_erlang_text = "快速编译Erlang";
    $hot_erlang_text = "热更Erlang";
    $gcode_text = "生成协议文件";
    $compile_erlang_button = "";
}

if (isset($_REQUEST['flag_compile']) && intval($_REQUEST['flag_compile'])) {
    // 编译重启Erlang
    $sys = trim(file_get_contents($erl_compile_flag));
    if ($sys != "0") {
        header("Location:builder.php");
        exit();
    } else {
        file_put_contents($erl_compile_flag, '1');
        system("sh " . ROOT_PATH . "sh/compile_erl.sh >> $erl_compile_log &");
        header("Location:builder.php");
        exit();
    }
} else if (isset($_REQUEST['flag_quick_compile']) && intval($_REQUEST['flag_quick_compile'])) {
    // 快速编译重启Erlang
    $sys = trim(file_get_contents($erl_compile_flag));
    if ($sys != "0") {
        header("Location:builder.php");
        exit();
    } else {
        file_put_contents($erl_compile_flag, '2');
        system("sh " . ROOT_PATH . "sh/quick_compile_erl.sh >> $erl_compile_log &");
        header("Location:builder.php");
        exit();
    }
} else if (isset($_REQUEST['flag_hot']) && intval($_REQUEST['flag_hot'])) {
    // 热更服务端代码
    $sys = trim(file_get_contents($erl_compile_flag));
    if ($sys != "0") {
        header("Location:builder.php");
        exit();
    } else {
        file_put_contents($erl_compile_flag, '3');
	system("sh " . ROOT_PATH . "sh/hot_erl.sh >> $erl_compile_log &");
        header("Location:builder.php");
        exit();
    }
} else if (isset($_REQUEST['flag_prot']) && intval($_REQUEST['flag_prot'])) {
    // 生成协议文件
    $sys = trim(file_get_contents($erl_compile_flag));
    if ($sys != "0"){
        header("Location:builder.php");
        exit();
    } else{
        file_put_contents($erl_compile_flag, '4');
        system("sh " . ROOT_PATH . "sh/gprot.sh >> $erl_compile_log &");
        header("Location:builder.php");
        exit();
    }
} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<title>3D T2手游--开发服编译</title>
<style type="text/css">
    h1{font:bold 16px/28px arial,'宋体'; color:#333; background:#f4f4f4; margin:0 0 20px 0; padding:0;}
    body{font:12px/20px arial,'宋体';}
    #main{margin: auto 0;width: 100%; height:150px;}
    #wrapper{float:left;width:20%; height:400px; margin:10px; border:1px solid #eee; padding:2px; text-align:center;}
    #admin{width:100%; height:700px; margin:10px auto; border:1px solid #eee; padding:2px; text-align:center;}
    #serverlog{float:left;width:70%; height:400px; margin:10px; border:1px solid #eee; padding:2px;}
    #servercontent{width:100%; height:365px;overflow-y:scroll;margin-top:5px;}
</style>
<script src="./js/jquery.js" type="text/javascript"></script>
<script type="text/javascript">
var flag = <?php echo $flag; ?>;
// alert(aa)
$(document).ready(function(){
    var  ClentTimerOne = setTimeout("get_compile_log()", 1000);
    var ClentTimer = setInterval("get_compile_log()",10000);
    var ClentTimer2 = setInterval("get_compile_log2()",20000);
});


function get_compile_log(){
    $.ajax({
        url:'builder.php',
        dataType:'json',
        type:'post',
        data:'ac=get_compile_log&time='+Math.random(),
        success:function(d){
            $('#servercontent').html(d.serverlog);
            $("#servercontent").scrollTop(1000000);
        }
    });
}

function get_compile_log2(){
    $.ajax({
        url:'builder.php',
        dataType:'json',
        type:'post',
        data:'ac=get_compile_log&time='+Math.random(),
        success:function(d){
            if(d.flag!=flag){
                location.reload()
            }
        }
    });
}
</script>
</head>
<body>
<div>
    <div id="serverlog">
        <h1 style="text-align:center;margin:0;">Erlang编译日志</h1>
        <div id="servercontent"></div>
    </div>
    <div id="wrapper">
        <h1>编译器</h1>
        <form id="f3" method="post">
            <input type="hidden" id="flag_compile" name="flag_compile" value="0" />
            <input type="hidden" id="flag_quick_compile" name="flag_quick_compile" value="0" />
            <input type="hidden" id="flag_hot" name="flag_hot" value="0" />
            <input type="hidden" id="flag_prot" name="flag_prot" value="0" />
            <input type="submit" value="<?php echo $compile_erlang_text ?>" <?php echo $compile_erlang_button ?> onclick="this.value='编译Erlang中...'; document.getElementById('flag_compile').value=1; document.getElementById('f3').submit(); this.disabled=true;" /><br />
            <input type="submit" value="<?php echo $quick_compile_erlang_text ?>" <?php echo $compile_erlang_button ?> onclick="this.value='快速编译Erlang中...'; document.getElementById('flag_quick_compile').value=1; document.getElementById('f3').submit(); this.disabled=true;" /><br />
            <input type="submit" value="<?php echo $hot_erlang_text ?>" <?php echo $compile_erlang_button ?> onclick="this.value='热更Erlang中...'; document.getElementById('flag_hot').value=1; document.getElementById('f3').submit(); this.disabled=true;" /><br />
            <input type="submit" value="<?php echo $gcode_text ?>" <?php echo $compile_erlang_button ?> onclick="this.value='生成协议中...'; document.getElementById('flag_prot').value=1; document.getElementById('f3').submit(); this.disabled=true;" /></br></br>
            <a href="/down_ebin.php">ebin压缩下载</a>
        </form>
    </div>
</div>
</body>
</html>
<?php }?>
