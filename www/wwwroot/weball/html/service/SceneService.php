<?php
/*
 * Author: 逍遥重生, MSN: gamekinga@hotmail.com qq:13931430
 * 2009-5-28
 * 场景
 */

/*return array(
            "id" => 1000,
            "name" => "场景1000",
            "type" => 0,
            "x" => 11,
            "y" => 22,
            "requirement" => array(),
            "elem" => array(
                array(
                	"id" =>1,
                	"name" => "出入口1",
                	"x" => 33,
                    "y" => 44,
                ),
                array(
                	"id" =>1,
                	"name" => "出入口1",
                	"x" => 33,
                    "y" => 44,
                ),
            ),
            "npc" => array(
                array(
                	"id" =>100,
                    "x" => 33,
                	"y" => 44
                )
            ),
            "mon" => array(
                array(
                	"id" =>100,
                    "x" => 33,
                	"y" => 44
                )
            )
        );*/
function makeInsertSqlFromArray($arr, $table)
{
    $str1 = '';
    $str2 = '';
    foreach ($arr as $k => $v) {
        $str1 .= "`{$k}`,";
        $str2 .= "'{$v}',";
    }

    $str = "INSERT INTO `{$table}` (" . trim($str1, ', ') . ") VALUES (" . trim($str2, ', ') . ")";
    return $str;
}

class SceneService {
  private $kf = 'game';//默认连到开发服
	/*
	 * NPC信息
	 */
    public function getNpcInfo() {
    	$db = Ext_Mysql::getInstance($this->kf);
    	$npc_info = $db->fetchRow("select id,name,image,realm,icon,func from base_npc");
    	return $npc_info;
    }
    
	public function MyEncode($array) {
		return addcslashes(json_encode($array), '\\');
	}
    
    /*
	 * mon信息
	 */
	public function getMonInfo() {
    	$db = Ext_Mysql::getInstance($this->kf);
    	$npc_info = $db->fetchRow("select id,name,lv from base_mon");
    	return $npc_info;
    }
    
	public function getSceneInfo(){
		$id = $_REQUEST['id'];
		$db = Ext_Mysql::getInstance($this->kf);
        $data = $db->fetchOne("select * from base_scene where id={$id}");
        if(!is_array($data)){
            return NULL;
        }
        $data['id'] = intval($data['id']);
        $data['type'] = intval($data['type']);
        $data['x'] = intval($data['x']);
        $data['y'] = intval($data['y']);
        $data['requirement'] = json_decode($data['requirement']);
        $data['elem'] = json_decode($data['elem']);
        $data['npc'] = json_decode($data['npc']);
        $data['mon'] = json_decode($data['mon']);
        $data['jump'] = json_decode($data['jump']);
        $data['mask_id'] = intval($data['mask_id']);
        return $data;
    }
    
	public function getAllSceneInfo(){
		$db = Ext_Mysql::getInstance($this->kf);
        $allData = $db->fetchRow("select `id`,`name` from base_scene order by id");
        if(count($allData) < 1){
            return NULL;
        }
        return $allData;
    }
    
    public function saveScene() {
        //$data = sstripslashes($_POST['data']);
        $data = $_POST['data'];
        $data = json_decode($data);
        $data = get_object_vars($data);
        $data['id'] = intval($data['id']);
        $data['type'] = intval($data['type']);
        $data['x'] = intval($data['x']);
        $data['y'] = intval($data['y']);
        $data['requirement'] = $this->MyEncode($data['requirement']);
        //$data['jump'] = $this->MyEncode($data['jump']);
        $data['elem'] = $this->MyEncode($data['elem']);
        $data['npc'] = $this->MyEncode($data['npc']);
        $data['mon'] = $this->MyEncode($data['mon']);
        // $data['sound'] = $this->MyEncode($data['sound']);
        // $data['safe'] = json_encode($data['safe']);
        $data['width'] = intval($data['width']);
        $data['height'] = intval($data['height']);
        $data['mask_id'] = intval($data['mask_id']);
        $sql = makeInsertSqlFromArray($data, "base_scene");
        $sql = str_replace("INSERT ","replace ", $sql);
        print_r($sql);
        Ext_Mysql::getInstance($this->kf)->query($sql);
        $rmd5 = $data['id'];
        return $rmd5;
    }

    public function saveSceneMask() {
		print_r("WO QU NI MA");
        //$data = sstripslashes($_POST['data']);
        $data = $_POST['data'];
		print_r($data);
        $data = json_decode($data);
        $data = get_object_vars($data);
        $data['id'] = intval($data['id']);
        $sql = makeInsertSqlFromArray($data, "base_scene_mask");
        $sql = str_replace("INSERT ","replace ", $sql);
        Ext_Mysql::getInstance($this->kf)->query($sql);
        // $return = array();
        // $return = Ext_Mysql::getInstance($this->kf)->fetchOne("select mask from base_scene_mask where id={$data['id']}");
        $rmd5 = $data['id'];
        return $rmd5;
    }
    
    /**
     * 导入到表mon
     */
    public function import() {
    	$db = Ext_Mysql::getInstance($this->kf);
    	$db->query("truncate table `mon`");
    	$db->query("truncate table `npc`");
//    	$db->query("truncate table `elem`");
    	$data = $db->fetchRow("select * from base_scene", 'id');
    	if (is_array($data)) {
    		foreach($data as $scene) {
    			//导入场景元素
//	    		if ($scene['elem']) {
//		    		$all = json_decode($scene['elem']);
//		    		if (is_array($all))
//		    		foreach($all as $v) {
//		    			$v = get_object_vars($v);
//		    			if (!isset($data[$v['id']]))
//		    				continue;
//		    			$ones = array();
//		    			$ones['name'] = $data[$v['id']]['name'];
//		    			$ones['x'] = $v['x'];
//		    			$ones['y'] = $v['y'];
//		    			$ones['sid'] = $v['id'];
//		    			$ones['scene'] = $scene['id'];
//		    			$sql = makeInsertSqlFromArray($ones, "elem");
//		    			$db->query($sql);
//		    		}
//    			}
    			//导入NPC
//	    		if ($scene['npc']) {
//		    		$all = json_decode($scene['npc']);
//		    		if (is_array($all))
//		    		foreach($all as $v) {
//		    			$v = get_object_vars($v);
//		    			if (!$ones = $db->fetchOne("select * from `base_npc` where `id` = ".$v['id']))
//		    				continue;
//		    			$ones['nid'] = $v['id'];
//		    			$ones['x'] = $v['x'];
//		    			$ones['y'] = $v['y'];
//		    			$ones['scene'] = $scene['id'];
//		    			$sql = makeInsertSqlFromArray($ones, "npc");
//		    			$db->query($sql);
//		    		}
//    			}
//    			//导入MON
//    			if ($scene['mon']) {
//		    		$all = json_decode($scene['mon']);
//		    		if (is_array($all))
//		    		foreach($all as $v) {
//		    			$v = get_object_vars($v);
//		    			if (!$ones = $db->fetchOne("select * from `base_mon` where `id` = ".$v['id']))
//		    				continue;
//						$ones['mid'] = $v['id'];
//		    			$ones['d_x'] = $v['x'];
//		    			$ones['d_y'] = $v['y'];
//		    			$ones['scene'] = $scene['id'];
//		    			unset($ones['id']);
//		    			$sql = makeInsertSqlFromArray($ones, "mon");
//		    			$db->query($sql);
//		    		}
//    			}
    		}
    	}
    	return true;	
    }
}

