<?php
/**
 * 查看日志文件
 */
/*
if(!($_SERVER['SERVER_NAME'] == 'devlog.zwx.com' || $_SERVER['SERVER_NAME'] == 'wdlog.zwx.com')) {
    die();
}
*/

if ($_SERVER['SERVER_NAME'] == 'devlog.vtnemo.com') {
	$log_path = '/data2/www/vtnemo/dev/game/log/'; 
} else {
	$log_path = '/data/www/zwx/wd/game/logs/'; 
}

$act = isset($_GET['act']) ? trim($_GET['act']) : '';

if (empty($act)) {	//显示列表
	$date = date("Y_n_j");
	$file1 = "cls_alarm_{$date}.txt";
	$file2 = "errlog_{$date}.txt";
	$file3 = "sys_alarm_{$date}.txt";
	echo "
	<a href='http://" . $_SERVER['SERVER_NAME'] . "/game_log.php?act=clean'> >> 清理日志 << </a><br />
	<a href='http://" . $_SERVER['SERVER_NAME'] . "/{$file1}'>{$file1}</a><br />
	<a href='http://" . $_SERVER['SERVER_NAME'] . "/{$file2}'>{$file2}</a><br />
	<a href='http://" . $_SERVER['SERVER_NAME'] . "/{$file3}'>{$file3}</a>
	";
} else if ($act == 'clean') {	//清除日志
	if (is_dir($log_path)) {
		if ($dh = opendir($log_path)) {
			while (($file = readdir($dh)) !== false) {
				if ($file == '.' || $file == '..' || $file == 'game_log.php') continue;
				unlink($log_path . $file);
			}
			closedir($dh);
		}
	}
	echo "清理成功";
}
