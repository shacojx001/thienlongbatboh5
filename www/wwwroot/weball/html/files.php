<?php
if(!($_SERVER['SERVER_NAME'] == 'www.fynemo.com' || $_SERVER['SERVER_NAME'] == 'dev.fynemo.com')) {
    die();
}

$assetsPath = 'flash/assets';
$gamePath = 'flash';
//$path = 'c:/360Downloads/';
function get_filetree_scandir($path){ 
    $result = array();   
    $temp = array();
    if (!is_dir($path)||!is_readable($path)) 
        return null; //检测目录有效性   
    $allfiles = scandir($path); //获取目录下所有文件与文件夹  
    foreach ($allfiles as $filename) { //遍历一遍目录下的文件与文件夹    
        if (in_array($filename,array('.','..')))
            continue; //无视 . 与 .. 
        if($filename == ".svn") continue; 
		if($filename == "scene") continue; 
        $fullname = $path.'/'.$filename; //得到完整文件路径     
        if (is_dir($fullname)) { //是目录的话继续递归      
            $temp= get_filetree_scandir($fullname); //递归开始
            $result = array_merge($result,$temp);    
        }    
        else {      
            $result[] = $fullname; //如果是文件，就存入数组    

        }  
    }  
    return $result; 
}
$result = get_filetree_scandir($assetsPath);
$result[] = $gamePath.'/GameLoader.swf';
header('Content-Type: text/xml');
print_r("<?xml version='1.0' encoding='utf-8'?>\n");
print_r("<files>");
foreach( $result as $sfile){
    //$sfile = iconv("gb2312","UTF-8",$sfile);
    $file_time= date ("YmdHis", filemtime($sfile));
    if(strpos($sfile,"GameLoader.swf")>0){
      $sfile = "GameLoader.swf";
    }else{
        $sfile = substr($sfile,strpos($sfile,'assets'));
    } 
    $sfile = str_replace("//","/",$sfile) ;
    print_r("<file date='".$file_time."' path='".$sfile."' />\n"); 
}
print_r("</files>");
 
?> 

