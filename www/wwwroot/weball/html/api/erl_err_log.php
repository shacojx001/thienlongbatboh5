<?php
function my_dir($dir) {
    $files = array();
    if(@$handle = opendir($dir)) { //注意这里要加一个@，不然会有warning错误提示：）
        while(($file = readdir($handle)) !== false) {
            if($file != ".." && $file != "." && $file != ".svn") { //排除根目录；
                if(is_dir($dir."/".$file)) { //如果是子文件夹，就进行递归
                    $files[$file] = my_dir($dir."/".$file);
                } else { //不然就将文件的名字存入数组；
                    $filesize = filesize($dir."/".$file);
                    $files[] = array(
                        'size' => $filesize,
                        'file' => $file
                    );
                }

            }
        }
        closedir($handle);
        return $files;
    }
}

$file = '';
$dir = dirname(dirname(dirname(dirname(__FILE__))))."/game/log/";
$tmp = dirname(__FILE__).'/tmp';
if(isset($_GET['file'])) $file = $_GET['file'];
if($file){
    $des = $dir.$file;
    if(!is_file($des)){
        exit('日志文件不存在!');
    }
    if(!is_dir($tmp)){
        $re = mkdir($tmp, 0777, true);
        if(!$re) exit('创建临时文件夹失败!');
    }else{
        exec("rm -rf $tmp/*",$a,$b);
    }
    exec("\cp $des $tmp",$a,$b);
    if($b !== 0) exit('复制log文件到临时文件夹失败!');
    exit('success');

}else{
    $files = my_dir($dir);
    exit(json_encode($files));
}