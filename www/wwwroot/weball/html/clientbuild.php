<?php 
include(dirname(dirname(dirname(__FILE__)))."/php/html/bs.php");

// 设置执行时间没有限制
set_time_limit(0);

// 设置版本号
$flashVersion=20151231;

// 替换assetsConfig.xml中的version和key项
$assetsConfigPath = CDN_CLIENT_DIR . '/flash/assets/config/assetConfig.xml';
$xml = simplexml_load_file($assetsConfigPath); 
$content = file_get_contents($assetsConfigPath);
$time = time();
$out = preg_replace('/<key>(.*?)<\/key>/',"<key>{$time}</key>", $content);
$out = preg_replace('/<version>(.*?)<\/version>/',"<version>$flashVersion</version>", $out);
$size = file_put_contents($assetsConfigPath, $out);

// 拷贝生成带版本号的主swf文件
$mainSwfFile=CDN_CLIENT_DIR . "/flash/main.swf";
$mainxxFile=CDN_CLIENT_DIR . "/flash/main".$flashVersion.".xx";
$file_get_contents=file_get_contents($mainSwfFile,"w");
$fp=fopen($mainxxFile,"w");
fwrite($fp, $file_get_contents);
fclose($fp);

// 打包config目录下的配置
$configPath=CDN_CLIENT_DIR . '/flash/assets/config';
$configFile=$configPath."/config".$flashVersion.".xx";
// (1) 遍历config目录下的文件过滤出.xmlbak文件进行打包
$allConfigfiles = get_filetree_scandir($configPath); 
$fp=fopen($configFile,"w");
foreach ($allConfigfiles as $filename) {     
	//$fullname = $configPath.'/'.$filename; 
	$fullname = $filename;
	// 如果是bak文件
	if (strpos($filename,".xmlbak") > 0 ) {       
		$result[] = $filename; //如果是文件，就存入数组 
		$content  = file_get_contents($fullname);
		$filename = substr($fullname, strrpos($fullname, "/")+1);
		$filename = substr($filename,0,strlen($filename)-3);
		//print_r($filename."<br>");
		writeStr($fp,$filename);
		fwrite($fp, pack('N',strlen($content)));
		fwrite($fp, $content);
	}    
}
fclose($fp);
// (2) 进行压缩
$file_get_contents=file_get_contents($configFile,"w");
$compressed = gzcompress($file_get_contents, 9);
// (3) 回写文件
$fp=fopen($configFile,"w");
fwrite($fp, $compressed);
fclose($fp);

// 打包assets和common目录下的资源。格式[path,mdate,size],额外字段["key",16位加密串]
$gamePath   = CDN_CLIENT_DIR . '/flash';
$fileTimeFile=$gamePath."/fileTime.xx";
$fp = fopen($fileTimeFile, 'w');
writeStr($fp,"key");
writeStr($fp,"7YnELt8MmA4jVED7");
// (1) 打包assets下的资源
$assetsPath = CDN_CLIENT_DIR . '/flash/assets';
$result = get_filetree_scandir($assetsPath);
foreach( $result as $sfile) {
    $file_time  = date ("U", filemtime($sfile));
	$file_size  = filesize($sfile);
    $sfile      = substr($sfile,strpos($sfile,'assets')+6);    
    $sfile      = str_replace("//","/",$sfile) ;
	writeStr($fp,$sfile);
	fwrite($fp, pack('N',$file_time));
	fwrite($fp, pack('N',$file_size));
}
// (2) 打包common下的资源
$commonPath = CDN_CLIENT_DIR . '/common';
$result = get_filetree_scandir($commonPath);
foreach( $result as $sfile) {
    $file_time = date ("U", filemtime($sfile));
	$file_size = filesize($sfile);
	$sfile = substr($sfile,strpos($sfile,'common')); 
    $sfile = str_replace("//","/",$sfile) ;
	writeStr($fp,$sfile);
	fwrite($fp, pack('N',$file_time));
	fwrite($fp, pack('N',$file_size));
}
//gzcompress($fp,9)
fclose($fp);
// (3) 进行压缩
$file_get_contents=file_get_contents($fileTimeFile,"w");
$compressed = gzcompress($file_get_contents, 9);
// (4) 回写内容
$fp=fopen($fileTimeFile,"w");
fwrite($fp, $compressed);
fclose($fp);

// 提交SVN
$dir=CDN_CLIENT_DIR . "/flash";
$comment="服务器提交";
svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_USERNAME, '');
svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_PASSWORD, '');
svn_update(realpath($dir));
$svn_status=svn_status(realpath($dir));
foreach( $svn_status as $addfile)
{
	if($addfile["text_status"]==2)
	{
		svn_add($addfile["path"]);
	}
	
}
svn_commit("server commit", array(realpath($dir)));

print_r("编译配置成功!客户端版本号 $flashVersion ，配置版本号 $time ！");
print_r(date("h:i:s A",$time));


function get_filetree_scandir($path){ 
    $result = array();   
    // 检测目录有效性 
    if (!is_dir($path)||!is_readable($path)) 
        return null;   
    // 获取目录下所有文件与文件夹
    $allfiles = my_scandir($path);   
    // 遍历一遍目录下的文件与文件夹
    foreach ($allfiles as $filename) {   
    	// 忽略.与..目录  
        if (in_array($filename,array('.','..')))
            continue; 
        // 忽略.svn目录
        if($filename == ".svn") 
        	continue; 
		if (is_file($filename) && !preg_match("/\/sceneatfres\//", $filename) && !preg_match("/\/scenejpgres\//", $filename))
			$result[] = $filename; //如果是文件，就存入数组   
    }  
    return $result; 
}

function writeStr($fp,$str)
{
	$len=strlen($str);
	fwrite($fp, pack('n',$len));
	$strarr=str_split($str);
	foreach ($strarr as $v)
	{
		
		fwrite($fp, pack('H*',bin2hex($v)));
	}

}
