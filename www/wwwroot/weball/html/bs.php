<?php
/**
 * 管理后台头文件
 */

define("DEBUG", true);
//Report all errors directly to the screen for simple diagnostics in the dev environment
if($_SERVER['SERVER_NAME'] == 'www.tdnemo.com' || $_SERVER['SERVER_NAME'] == 'dev.tdnemo.com' || (isset($_GET['GE_DEBUG']) && $_GET['GE_DEBUG'] == 'open'))
{
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_startup_errors', 1);
    ini_set('display_errors', 1);
}else{
    error_reporting(0);
    //ini_set('display_startup_errors', 0);
    ini_set('display_errors', 0);
}

@ini_set('magic_quotes_runtime', 0);

/*服务器域名地址*/
$server = $_SERVER[''];
define("COOKIEDOMAIN", $_SERVER['SERVER_NAME']);
define("PUBLICWEBROOT", 'http://'.$_SERVER['SERVER_NAME']."/");

ini_set("session.cookie_httponly", 1);
header('P3P: CP=CAO PSA OUR');
@ini_set("session.save_handler", "memcache");
ini_set("session.save_path", "tcp://127.0.0.1:11211");
ini_set('session.gc_maxlifetime',10800);
session_start();
header('Content-Type: text/html; charset=utf-8');//字符集
date_default_timezone_set("Asia/Shanghai");//时区

// 顶级目录，下面有as, cdn, game(erl目录), log(erl生成的日志，及两个处理日志的php文件), php, sh(开发服或稳定服相关sh)
define("ROOT_PATH", dirname(dirname(dirname(__FILE__)))."/");
//cdn路径
define("CDN_CLIENT_DIR", ROOT_PATH . "cdn");
// erl生成数据路径
define("ERL_DATA_DIR", ROOT_PATH . "game/src/config/generate/");
// erl路径 -- 场景
define("ERL_DATA_DIR_SCENE", ROOT_PATH . "game/src/config/generate/scene/");
//erl 日志路径
define("ERL_LOGS_DIR", ROOT_PATH . "game/logs");
//flash模块路径
define("CDN_FLASH_MODULES_DIR", ROOT_PATH . "cdn/flash/modules/");
// 根路径
define('ROOT_DIR', dirname(dirname(__FILE__))."/");
// web路径
define("WEBROOT_DIR", ROOT_DIR . "html/");
// 库路径，将设置为 include path
define('LIB_DIR', ROOT_DIR . 'lib/');
// 服务路径，将作为 AMF 调用发布
define('SERVICE_DIR', ROOT_DIR . 'service/');
// 映射对象路径，暂时不用
define('SERVICE_VO_DIR', SERVICE_DIR . 'vo/');
// 日志
define('LOG_DIR', ROOT_DIR . 'log/');
// 模块目录
define('MODULE_DIR', ROOT_DIR . 'module/');
// 游戏数据缓冲目录
define('CACHE_DIR', ROOT_DIR . 'cache/');
// 配置文件目录
define('CONFIG_DIR', ROOT_DIR . 'config/');
//模板目录
define('TPL_DIR', ROOT_DIR . 'view/');

set_include_path( PATH_SEPARATOR . ROOT_DIR . 'lib'
				. PATH_SEPARATOR . ROOT_DIR . 'lib/Tools'
			    . PATH_SEPARATOR . get_include_path());

@include_once CONFIG_DIR . 'security.php';
include_once LIB_DIR . 'Helper/String.php';
spl_autoload_register("autoload");
