<?php
if(!($_SERVER['SERVER_NAME'] == 'www.tdnemo.com' || $_SERVER['SERVER_NAME'] == 'dev.tdnemo.com')) {
    die();
}

require 'bs.php';

//用post方式提交
$m = $_GET['service'];
$a = $_GET['method'];

if(empty($m))exit('service not found');
if(empty($a))exit('method not found');

if(file_exists('./service/' .$m.".php")){
	require_once './service/' .$m.".php";
	if(class_exists($m)){
		$control = new $m();
	}else{
		exit('service not found');
	}
	//note 不允许访问私有方法 
	if(method_exists($control, $a) && $a{0} != '_') {
		//note 统一json格式输出
		echo json_encode($control->$a());
//		d($control->$a());
	} else {
		exit('method not found');
	}
}else{
	exit('service not found');
}